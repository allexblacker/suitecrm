<?php

$app_list_strings['_type_dom'][''] = '';
$app_list_strings['_type_dom']['Analyst'] = 'Analyst';
$app_list_strings['_type_dom']['Competitor'] = 'Mitbewerber';
$app_list_strings['_type_dom']['Customer'] = 'Kunden';
$app_list_strings['_type_dom']['Integrator'] = 'Integrator';
$app_list_strings['_type_dom']['Investor'] = 'Investor';
$app_list_strings['_type_dom']['Other'] = 'Andere';
$app_list_strings['_type_dom']['Partner'] = 'Partner';
$app_list_strings['_type_dom']['Press'] = 'Presse';
$app_list_strings['_type_dom']['Prospect'] = 'Zielkontakt ';
$app_list_strings['_type_dom']['Reseller'] = 'Fachhändler';
