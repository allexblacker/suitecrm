<?php

$app_list_strings['Array_priority_dom']['P1'] = 'Hoch';
$app_list_strings['Array_priority_dom']['P2'] = 'Mittel';
$app_list_strings['Array_priority_dom']['P3'] = 'Niedrig';
$app_list_strings['Array_resolution_dom'][''] = '';
$app_list_strings['Array_resolution_dom']['Accepted'] = 'Akzeptiert';
$app_list_strings['Array_resolution_dom']['Closed'] = 'Abgeschlossen';
$app_list_strings['Array_resolution_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['Array_resolution_dom']['Invalid'] = 'Ungültig';
$app_list_strings['Array_resolution_dom']['Out of Date'] = 'Abgelaufen';
$app_list_strings['Array_status_dom']['Assigned'] = 'Zugewiesen';
$app_list_strings['Array_status_dom']['Closed'] = 'Abgeschlossen';
$app_list_strings['Array_status_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['Array_status_dom']['New'] = 'Neue';
$app_list_strings['Array_status_dom']['Pending Input'] = 'Rückmeldung ausstehend';
$app_list_strings['Array_status_dom']['Rejected'] = 'Abgelehnt';
$app_list_strings['Array_type_dom']['Administration'] = 'Administration';
$app_list_strings['Array_type_dom']['Product'] = 'Produkt';
$app_list_strings['Array_type_dom']['User'] = 'Benutzer';
