<?php

$mod_strings['LBL_ASSIGNED_TO_ID'] = 'Zugewiesen an:';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Benutzer:';
$mod_strings['LBL_ASSIGNED_USER'] = 'Zugew. Benutzer:';
$mod_strings['LBL_CREATED_BY'] = 'Erstellt von:';
$mod_strings['LBL_DATE_CREATED'] = 'Erstellungsdatum:';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum:';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum:';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung:';
$mod_strings['LBL_EDIT_BUTTON'] = 'Bearbeiten';
$mod_strings['LBL_LAST_MODIFIED'] = 'Zuletzt geändert:';
$mod_strings['LBL_MODIFIED_BY'] = 'Zuletzt geändert von:';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_NUMBER'] = 'Nummer:';
$mod_strings['LBL_PRIORITY'] = 'Priorität:';
$mod_strings['LBL_REMOVE'] = 'Entfernen';
$mod_strings['LBL_RESOLUTION'] = 'Lösung';
$mod_strings['LBL_STATUS'] = 'Status:';
$mod_strings['LBL_SUBJECT'] = 'Betreff:';
$mod_strings['LBL_SYSTEM_ID'] = 'System ID:';
$mod_strings['LBL_TYPE'] = 'Typ:';
$mod_strings['LBL_WORK_LOG'] = 'Arbeitsprotokoll:';
