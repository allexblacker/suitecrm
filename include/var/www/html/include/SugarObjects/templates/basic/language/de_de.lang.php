<?php

$mod_strings['LBL_CREATED'] = 'Erstellt von';
$mod_strings['LBL_CREATED_ID'] = 'Erstellt von Id';
$mod_strings['LBL_CREATED_USER'] = 'Erstellt von Benutzer';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_EDIT_BUTTON'] = 'Bearbeiten';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_LIST_NAME'] = 'Name';
$mod_strings['LBL_MODIFIED'] = 'Geändert von';
$mod_strings['LBL_MODIFIED_ID'] = 'Geändert von Id';
$mod_strings['LBL_MODIFIED_NAME'] = 'Geändert von Name';
$mod_strings['LBL_MODIFIED_USER'] = 'Geändert von Benutzer';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_REMOVE'] = 'Entfernen';
