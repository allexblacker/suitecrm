<?php

$app_list_strings['Array_category_dom'][''] = '';
$app_list_strings['Array_category_dom']['Knowledege Base'] = 'Wissensdatenbank';
$app_list_strings['Array_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['Array_category_dom']['Sales'] = 'Verkauf';
$app_list_strings['Array_status_dom']['Active'] = 'Aktiv';
$app_list_strings['Array_status_dom']['Draft'] = 'Entwurf';
$app_list_strings['Array_status_dom']['Expired'] = 'Abgelaufen';
$app_list_strings['Array_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['Array_status_dom']['Pending'] = 'Ausstehend';
$app_list_strings['Array_status_dom']['Under Review'] = 'Wird überprüft';
$app_list_strings['Array_subcategory_dom'][''] = '';
$app_list_strings['Array_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['Array_subcategory_dom']['Marketing Collateral'] = 'Werbematerial';
$app_list_strings['Array_subcategory_dom']['Product Brochures'] = 'Produktbroschüren';
