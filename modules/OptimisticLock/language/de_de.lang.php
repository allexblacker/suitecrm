<?php

$mod_strings['LBL_ACCEPT_DATABASE'] = 'Datenbank akzeptieren';
$mod_strings['LBL_ACCEPT_YOURS'] = 'Akzeptiere Ihre';
$mod_strings['LBL_CONFLICT_EXISTS'] = 'Ein Konflikt existiert wegen - ';
$mod_strings['LBL_IN_DATABASE'] = 'In der Datenbank';
$mod_strings['LBL_NO_LOCKED_OBJECTS'] = 'Keine gesperrten Objekte';
$mod_strings['LBL_RECORDS_MATCH'] = 'Übereinstimmung von Datensätzen';
$mod_strings['LBL_YOURS'] = 'Ihre';
