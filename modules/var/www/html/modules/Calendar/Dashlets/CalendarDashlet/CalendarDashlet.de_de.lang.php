<?php

$dashletStrings['CalendarDashlet']['LBL_CONFIGURE_TITLE'] = 'Titel';
$dashletStrings['CalendarDashlet']['LBL_CONFIGURE_VIEW'] = 'Ansicht';
$dashletStrings['CalendarDashlet']['LBL_DESCRIPTION'] = 'Kalender dashlet';
$dashletStrings['CalendarDashlet']['LBL_SAVE_BUTTON_LABEL'] = 'Speichern';
$dashletStrings['CalendarDashlet']['LBL_TITLE'] = 'Meine Kalender';
$dashletStrings['CalendarDashlet']['LBL_VIEW_DAY'] = 'Tag';
$dashletStrings['CalendarDashlet']['LBL_VIEW_MONTH'] = 'Monat';
$dashletStrings['CalendarDashlet']['LBL_VIEW_WEEK'] = 'Woche';
