<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings['LBL_ASSIGNED_TO_ID'] = 'Toegewezen gebruiker-ID';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Gebruiker';
$mod_strings['LBL_ID'] = 'Id';
$mod_strings['LBL_DATE_ENTERED'] = 'Datum aangemaakt';
$mod_strings['LBL_DATE_MODIFIED'] = 'Datum gewijzigd';
$mod_strings['LBL_MODIFIED'] = 'Gewijzigd door';
$mod_strings['LBL_MODIFIED_ID'] = 'Gewijzigd door ID';
$mod_strings['LBL_MODIFIED_NAME'] = 'Gewijzigd door naam';
$mod_strings['LBL_CREATED'] = 'Aangemaakt door';
$mod_strings['LBL_CREATED_ID'] = 'Gemaakt door ID';
$mod_strings['LBL_DESCRIPTION'] = 'Omschrijving';
$mod_strings['LBL_DELETED'] = 'Verwijderd';
$mod_strings['LBL_NAME'] = 'Naam';
$mod_strings['LBL_CREATED_USER'] = 'Aangemaakt door gebruiker';
$mod_strings['LBL_MODIFIED_USER'] = 'Gewijzigd door gebruiker';
$mod_strings['LBL_LIST_NAME'] = 'Naam';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Gebieden lijst';
$mod_strings['LBL_MODULE_NAME'] = 'Gebieden';
$mod_strings['LBL_MODULE_TITLE'] = 'Gebieden';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mijn gebieden';
$mod_strings['LNK_NEW_RECORD'] = 'Aanmaken gebied';
$mod_strings['LNK_LIST'] = 'Overzicht gebieden';
$mod_strings['LNK_IMPORT_JJWG_AREAS'] = 'Importeer gebieden';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Zoek gebieden';
$mod_strings['LBL_HISTORY_SUBPANEL_TITLE'] = 'Overzicht geschiedenis';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Activiteiten';
$mod_strings['LBL_JJWG_AREAS_SUBPANEL_TITLE'] = 'Gebieden';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nieuwe gebieden';
$mod_strings['LBL_CITY'] = 'Stad';
$mod_strings['LBL_STATE'] = 'Provincie';
$mod_strings['LBL_COUNTRY'] = 'Land';
$mod_strings['LBL_COORDINATES'] = 'Coordinaten';
$mod_strings['LBL_LIST_ASSIGNED_USER'] = 'Gebruiker';
$mod_strings['LBL_AREA_MAP'] = 'Gebiedskaart';

$mod_strings['LBL_AREA_EDIT_TITLE'] = 'Aanmaken gebiedsinstructies:';
$mod_strings['LBL_AREA_EDIT_DESC_1'] = 'Klik links op de map, in de richting van de klok, voor het maken van markeerpunten voor het te selecteren gebied.';
$mod_strings['LBL_AREA_EDIT_DESC_2'] = 'Klik op het eerste markeerpunt om het polygoon gebied te sluiten.';
$mod_strings['LBL_AREA_EDIT_RESET'] = 'Herstellen';
$mod_strings['LBL_AREA_EDIT_USE_AREA_COORDINATES'] = 'Gebruik gebied coordinaten';
$mod_strings['LBL_AREA_EDIT_COORDINATE_RESULTS'] = 'Coordinaat Resultaten (lng,lat,elv):';
$mod_strings['LBL_JJWG_MAPS_JJWG_AREAS_FROM_JJWG_MAPS_TITLE'] = 'Kaarten';
