<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
    'LBL_ASSIGNED_TO_ID' => '担当ユーザID',
    'LBL_ASSIGNED_TO_NAME' => '担当ユーザ',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => '作成日',
    'LBL_DATE_MODIFIED' => '更新日',
    'LBL_MODIFIED' => '更新者',
    'LBL_MODIFIED_ID' => '更新ID',
    'LBL_MODIFIED_NAME' => '更新者',
    'LBL_CREATED' => '作成者',
    'LBL_CREATED_ID' => '生成ID',
    'LBL_DESCRIPTION' => '詳細',
    'LBL_DELETED' => '削除済み',
    'LBL_NAME' => 'タイトル',
    'LBL_CREATED_USER' => '生成ユーザ',
    'LBL_MODIFIED_USER' => '更新ユーザ',
    'ACCOUNT_REMOVE_PROJECT_CONFIRM' => 'プロジェクトからこの取引先を削除して良いですか？',
    'ERR_DELETE_RECORD' => '取引先を削除するにはレコード番号を指定する必要があります。',
    'LBL_ACCOUNT_NAME' => 'タイトル',
    'LBL_ACCOUNT' => '会社:',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動',
    'LBL_ADDRESS_INFORMATION' => '住所情報',
    'LBL_ANNUAL_REVENUE' => '年間売上:',
    'LBL_ANY_ADDRESS' => '住所:',
    'LBL_ANY_EMAIL' => 'Eメール:',
    'LBL_ANY_PHONE' => '電話:',
    'LBL_RATING' => '格付:',
    'LBL_ASSIGNED_USER' => 'アサイン先',
    'LBL_BILLING_ADDRESS_CITY' => '請求先市区町村:',
    'LBL_BILLING_ADDRESS_COUNTRY' => '請求先国:',
    'LBL_BILLING_ADDRESS_POSTALCODE' => '請求先郵便番号:',
    'LBL_BILLING_ADDRESS_STATE' => '請求先都道府県:',
    'LBL_BILLING_ADDRESS_STREET_2' => '請求先住所2:',
    'LBL_BILLING_ADDRESS_STREET_3' => '請求先住所3:',
    'LBL_BILLING_ADDRESS_STREET_4' => '請求先住所4:',
    'LBL_BILLING_ADDRESS_STREET' => '請求先番地その他:',
    'LBL_BILLING_ADDRESS' => '請求先住所:',
    'LBL_ACCOUNT_INFORMATION' => '取引先情報',
    'LBL_CITY' => '市区町村:',
    'LBL_CONTACTS_SUBPANEL_TITLE' => '取引先担当者',
    'LBL_COUNTRY' => '国:',
    'LBL_DEFAULT_SUBPANEL_TITLE' => '取引先',
    'LBL_DESCRIPTION_INFORMATION' => '詳細情報',
    'LBL_DUPLICATE' => '重複の可能性がある取引先',
    'LBL_EMAIL' => 'Email:',
    'LBL_EMPLOYEES' => '従業員:',
    'LBL_FAX' => 'FAX',
    'LBL_INDUSTRY' => '業界:',
    'LBL_LIST_ACCOUNT_NAME' => '取引先',
    'LBL_LIST_CITY' => '市',
    'LBL_LIST_EMAIL_ADDRESS' => '電子メール',
    'LBL_LIST_PHONE' => '電話',
    'LBL_LIST_STATE' => '州・県',
    'LBL_LIST_WEBSITE' => 'Webサイト',
    'LBL_MEMBER_OF' => '親会社:',
    'LBL_MEMBER_ORG_FORM_TITLE' => '子会社',
    'LBL_MEMBER_ORG_SUBPANEL_TITLE' => '子会社',
    'LBL_OTHER_EMAIL_ADDRESS' => 'その他電子メール:',
    'LBL_OTHER_PHONE' => 'その他電話:',
    'LBL_OWNERSHIP' => '企業形態:',
    'LBL_PARENT_ACCOUNT_ID' => '親取引先ID',
    'LBL_PHONE_ALT' => 'その他の電話:',
    'LBL_PHONE_FAX' => '会社ファックス:',
    'LBL_PHONE_OFFICE' => '会社電話:',
    'LBL_PHONE' => '電話:',
    'LBL_POSTAL_CODE' => '郵便番号:',
    'LBL_PUSH_BILLING' => '請求する',
    'LBL_PUSH_SHIPPING' => '出荷する',
    'LBL_SAVE_ACCOUNT' => '取引先保存',
    'LBL_SHIPPING_ADDRESS_CITY' => '出荷先市区町村:',
    'LBL_SHIPPING_ADDRESS_COUNTRY' => '出荷先国:',
    'LBL_SHIPPING_ADDRESS_POSTALCODE' => '出荷先郵便番号:',
    'LBL_SHIPPING_ADDRESS_STATE' => '出荷先都道府県:',
    'LBL_SHIPPING_ADDRESS_STREET_2' => '出荷先住所 2',
    'LBL_SHIPPING_ADDRESS_STREET_3' => '出荷先住所 3',
    'LBL_SHIPPING_ADDRESS_STREET_4' => '出荷先住所 4',
    'LBL_SHIPPING_ADDRESS_STREET' => '出荷先番地その他:',
    'LBL_SHIPPING_ADDRESS' => '出荷先住所:',
    'LBL_STATE' => '都道府県:', //For address fields
    'LBL_TEAMS_LINK' => 'チーム',
    'LBL_TICKER_SYMBOL' => '証券コード:',
    'LBL_TYPE' => 'タイプ:',
    'LBL_USERS_ASSIGNED_LINK' => '担当ユーザ一覧',
    'LBL_USERS_CREATED_LINK' => '作成ユーザ一覧',
    'LBL_USERS_MODIFIED_LINK' => '更新ユーザ一覧',
    'LBL_VIEW_FORM_TITLE' => '取引先ビュー',
    'LBL_WEBSITE' => 'Webサイト:',
    'LNK_ACCOUNT_LIST' => '取引先',
    'LNK_NEW_ACCOUNT' => '取引先作成',
    'MSG_DUPLICATE' => 'Creating this account may potentially create a duplicate account. You may either select an account from the list below or you may click on Save to continue creating a new account with the previously entered data.',
    'MSG_SHOW_DUPLICATES' => 'Creating this account may potentially create a duplicate account. You may either click on Save to continue creating this new account with the previously entered data or you may click Cancel.',
    'NTC_COPY_BILLING_ADDRESS' => '請求先住所を出荷先住所にコピー',
    'NTC_COPY_BILLING_ADDRESS2' => '出荷先へコピー',
    'NTC_COPY_SHIPPING_ADDRESS' => '出荷先住所を請求先住所にコピー',
    'NTC_COPY_SHIPPING_ADDRESS2' => '請求先へコピー',
    'NTC_DELETE_CONFIRMATION' => '本当にこのレコードを削除してよいですか?',
    'NTC_REMOVE_ACCOUNT_CONFIRMATION' => '本当にこのレコードを削除して良いですか？',
    'NTC_REMOVE_MEMBER_ORG_CONFIRMATION' => '本当にこのレコードを子会社から削除して良いですか？',
    'LBL_LIST_FORM_TITLE' => 'Invoices List',
    'LBL_MODULE_NAME' => '請求',
    'LBL_MODULE_TITLE' => 'Invoices: Home',
    'LBL_HOMEPAGE_TITLE' => 'My Invoices',
    'LNK_NEW_RECORD' => 'Create Invoice',
    'LNK_LIST' => 'View Invoices',
    'LBL_SEARCH_FORM_TITLE' => 'Search Invoices',
    'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
    'LBL_AOS_INVOICES_SUBPANEL_TITLE' => '請求',
    'LBL_NEW_FORM_TITLE' => 'New Invoice',
    'LBL_TERMS_C' => 'Terms',
    'LBL_APPROVAL_ISSUE' => 'Approval Issues',
    'LBL_APPROVAL_STATUS' => 'Approval Status',
    'LBL_BILLING_ACCOUNT' => '取引先',
    'LBL_BILLING_CONTACT' => '取引先担当者',
    'LBL_EXPIRATION' => 'Valid Until',
    'LBL_INVOICE_NUMBER' => 'Invoice Number',
    'LBL_OPPORTUNITY' => '商談名',
    'LBL_SHIPPING_ACCOUNT' => 'Shipping Account',
    'LBL_TEMPLATE_DDOWN_C' => 'Invoice Templates',
    'LBL_SHIPPING_CONTACT' => 'Shipping Contact',
    'LBL_STAGE' => 'Quote Stage',
    'LBL_TERM' => 'Payment Terms',
    'LBL_SUBTOTAL_AMOUNT' => 'サブタイトル',
    'LBL_DISCOUNT_AMOUNT' => '割引',
    'LBL_TAX_AMOUNT' => '税金',
    'LBL_SHIPPING_AMOUNT' => '配送',
    'LBL_TOTAL_AMT' => '合計',
    'VALUE' => 'タイトル',
    'LBL_EMAIL_ADDRESSES' => 'Eメールアドレス',
    'LBL_LINE_ITEMS' => '１行項目',
    'LBL_GRAND_TOTAL' => 'Grand Total',
    'LBL_QUOTE_NUMBER' => 'Quote Number',
    'LBL_QUOTE_DATE' => 'Quote Date',
    'LBL_INVOICE_DATE' => 'Invoice Date',
    'LBL_DUE_DATE' => '期限',
    'LBL_STATUS' => 'ステータス',
    'LBL_INVOICE_STATUS' => 'Invoice Status',
    'LBL_PRODUCT_QUANITY' => 'Quantity',
    'LBL_PRODUCT_NAME' => '商品',
    'LBL_PART_NUMBER' => 'Part Number',
    'LBL_PRODUCT_NOTE' => 'ノート',
    'LBL_PRODUCT_DESCRIPTION' => '詳細',
    'LBL_LIST_PRICE' => '一覧',
    'LBL_DISCOUNT_TYPE' => 'タイプ',
    'LBL_DISCOUNT_AMT' => '割引',
    'LBL_UNIT_PRICE' => 'Sale Price',
    'LBL_TOTAL_PRICE' => '合計',
    'LBL_VAT' => '税金', //VAT
    'LBL_VAT_AMT' => 'Tax Amount', //VAT
    'LBL_ADD_PRODUCT_LINE' => 'Add Product Line',
    'LBL_SERVICE_NAME' => 'サービス',
    'LBL_SERVICE_LIST_PRICE' => '一覧',
    'LBL_SERVICE_PRICE' => 'Sale Price',
    'LBL_SERVICE_DISCOUNT' => '割引',
    'LBL_ADD_SERVICE_LINE' => 'Add Service Line ',
    'LBL_REMOVE_PRODUCT_LINE' => '削除',
    'LBL_PRINT_AS_PDF' => 'PDFを印刷',
    'LBL_EMAIL_INVOICE' => 'Email Invoice',
    'LBL_LIST_NUM' => 'Num',
    'LBL_PDF_NAME' => 'Invoice',
    'LBL_EMAIL_NAME' => 'Invoice for',
    'LBL_NO_TEMPLATE' => 'ERROR\nNo templates found. If you have not created an Invoice template, go to the PDF templates module and create one',
    'LBL_SUBTOTAL_TAX_AMOUNT' => 'Subtotal + Tax',//pre shipping
    'LBL_EMAIL_PDF' => 'Email PDF',
    'LBL_ADD_GROUP' => 'Add Group',
    'LBL_DELETE_GROUP' => 'Delete Group',
    'LBL_GROUP_NAME' => 'グループ名',
    'LBL_GROUP_TOTAL' => 'Group Total',
    'LBL_SHIPPING_TAX' => 'Shipping Tax',
    'LBL_SHIPPING_TAX_AMT' => 'Shipping Tax',
    'LBL_IMPORT_LINE_ITEMS' => 'Import Line Items',
    'LBL_SUBTOTAL_AMOUNT_USDOLLAR' => 'Subtotal (Default Currency)',
    'LBL_DISCOUNT_AMOUNT_USDOLLAR' => 'Discount (Default Currency)',
    'LBL_TAX_AMOUNT_USDOLLAR' => 'Tax (Default Currency)',
    'LBL_SHIPPING_AMOUNT_USDOLLAR' => 'Shipping (Default Currency)',
    'LBL_TOTAL_AMT_USDOLLAR' => 'Total (Default Currency)',
    'LBL_SHIPPING_TAX_USDOLLAR' => 'Shipping Tax (Default Currency)',
    'LBL_SHIPPING_TAX_AMT_USDOLLAR' => 'Shipping Tax (Default Currency)',
    'LBL_GRAND_TOTAL_USDOLLAR' => 'Grand Total (Default Currency)',
    'LBL_INVOICE_TO' => 'Invoice To',
    'LBL_AOS_LINE_ITEM_GROUPS' => '項目グループ',
    'LBL_AOS_PRODUCT_QUOTES' => 'Product Quotes',
    'LBL_AOS_QUOTES_AOS_INVOICES' => 'Quotes: Invoices',
);
?>
