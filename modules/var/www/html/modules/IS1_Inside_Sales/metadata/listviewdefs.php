<?php
$module_name = 'IS1_Inside_Sales';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'CALL_C' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_CALL_C',
    'width' => '10%',
    'default' => false,
  ),
  'CALL_TO_ACTION' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Call_to_Action',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_DATE_TIME' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'created_date_time',
    'width' => '10%',
    'default' => false,
  ),
  'CUSTOMER_FUNNEL' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Customer_Funnel',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DECISION_MAKER' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Decision_Maker',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'IST_APPT_SET' => 
  array (
    'type' => 'date',
    'label' => 'IST_APPT_set',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'NOTES' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Notes',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'ROLE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Role',
    'width' => '10%',
    'default' => false,
  ),
  'TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Type',
    'width' => '10%',
    'default' => false,
  ),
);
?>
