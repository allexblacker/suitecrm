<?php

$mod_strings['ERR_CRON_SYNTAX'] = 'Ungültige Cron Syntax';
$mod_strings['ERR_DELETE_RECORD'] = 'Um diesen Datensatz zu löschen, muss eine Datensatznummer angegeben werden';
$mod_strings['LBL_ADV_OPTIONS'] = 'Erw. Optionen';
$mod_strings['LBL_ALL'] = 'Jeden Tag';
$mod_strings['LBL_ALWAYS'] = 'Immer';
$mod_strings['LBL_AND'] = ' und ';
$mod_strings['LBL_AODINDEXUNINDEXED'] = 'Indexiere nicht indizierter Dokumente';
$mod_strings['LBL_AODOPTIMISEINDEX'] = 'Advanced OpenDiscovery Index optimieren';
$mod_strings['LBL_AORRUNSCHEDULEDREPORTS'] = 'Geplanten Bericht ausführen';
$mod_strings['LBL_AT'] = 'um';
$mod_strings['LBL_AT_THE'] = 'Am';
$mod_strings['LBL_BASIC_OPTIONS'] = 'Basis Setup';
$mod_strings['LBL_CATCH_UP'] = 'Ausführen, wenn versäumt';
$mod_strings['LBL_CATCH_UP_WARNING'] = 'Deaktivieren, wenn die Ausführung dieses Jobs länger als einen Augenblick dauert.';
$mod_strings['LBL_CLEANJOBQUEUE'] = 'Jobs Warteschlange leeren';
$mod_strings['LBL_CRONTAB_EXAMPLES'] = 'Das oben stehende verwendet die Standard Crontab Notation.';
$mod_strings['LBL_CRONTAB_SERVER_TIME_POST'] = '). Bitte stellen Sie die Ausführungszeiten entsprechend ein.';
$mod_strings['LBL_CRONTAB_SERVER_TIME_PRE'] = 'Die Cron-Spezifierungen laufen basierend auf der Zeitzone des Servers (';
$mod_strings['LBL_CRON_INSTRUCTIONS_LINUX'] = 'Konfiguration eines crontab Eintrages';
$mod_strings['LBL_CRON_INSTRUCTIONS_WINDOWS'] = 'Konfiguration des Windows Terminplaners';
$mod_strings['LBL_CRON_LINUX_DESC1'] = 'Um die SuiteCRM Scheduler auszuführen, bearbeiten Sie die Crontab-Datei des Webservers mit diesem Befehl:';
$mod_strings['LBL_CRON_LINUX_DESC2'] = '... und füge die folgende Zeile der crontab-Datei hinzu: ';
$mod_strings['LBL_CRON_LINUX_DESC3'] = 'Sie sollten dies erst nach Abschluss der Installation machen.';
$mod_strings['LBL_CRON_LINUX_DESC'] = 'Hinweis: Um den SuiteCRM Zeitplaner auszuführen, fügen Sie die folgende Zeile zu Ihrer crontab Datei hinzu:';
$mod_strings['LBL_CRON_WINDOWS_DESC'] = 'Hinweis: Um den SuiteCRM Zeitplaner auszuführen, erstellen Sie eine Stapeldatei, die Sie mit Windows Geplante Aufgaben ausführen. Die Stapeldatei sollte folgende Kommandos enthalten:';
$mod_strings['LBL_DATE_TIME_END'] = 'Enddatum &  Zeit';
$mod_strings['LBL_DATE_TIME_START'] = 'Startdatum & Zeit';
$mod_strings['LBL_DAY_OF_MONTH'] = 'Datum';
$mod_strings['LBL_DAY_OF_WEEK'] = 'Tag';
$mod_strings['LBL_EVERY'] = 'alle';
$mod_strings['LBL_EVERY_DAY'] = 'Jeden Tag';
$mod_strings['LBL_EXECUTE_TIME'] = 'Ausführungszeit';
$mod_strings['LBL_FRI'] = 'Freitag';
$mod_strings['LBL_FROM'] = 'Von';
$mod_strings['LBL_HOUR'] = ' Stunden';
$mod_strings['LBL_HOURS'] = 'h';
$mod_strings['LBL_HOUR_SING'] = ' Stunde';
$mod_strings['LBL_IN'] = 'In';
$mod_strings['LBL_INTERVAL'] = 'Intervall';
$mod_strings['LBL_JOB'] = 'Auftrag';
$mod_strings['LBL_JOBS_SUBPANEL_TITLE'] = 'Aktive Jobs';
$mod_strings['LBL_JOB_URL'] = 'Job URL';
$mod_strings['LBL_LAST_RUN'] = 'Letzte erfolgreiche Durchführung';
$mod_strings['LBL_LIST_EXECUTE_TIME'] = 'Wird gestartet am:';
$mod_strings['LBL_LIST_JOB_INTERVAL'] = 'Intervall:';
$mod_strings['LBL_LIST_LIST_ORDER'] = 'Geplante Aufgaben:';
$mod_strings['LBL_LIST_NAME'] = 'Geplante Aufgabe:';
$mod_strings['LBL_LIST_RANGE'] = 'Bereich:';
$mod_strings['LBL_LIST_REMOVE'] = 'Entfernen:';
$mod_strings['LBL_LIST_STATUS'] = 'Status:';
$mod_strings['LBL_LIST_TITLE'] = 'Aufgaben Liste:';
$mod_strings['LBL_MINS'] = 'Min';
$mod_strings['LBL_MINUTES'] = ' Minuten ';
$mod_strings['LBL_MIN_MARK'] = 'Minuten nach';
$mod_strings['LBL_MODULE_NAME'] = 'SuiteCRM Zeitplaner';
$mod_strings['LBL_MODULE_TITLE'] = 'Geplante Aufgaben';
$mod_strings['LBL_MON'] = 'Montag';
$mod_strings['LBL_MONTH'] = 'Monat';
$mod_strings['LBL_MONTHS'] = 'Monate';
$mod_strings['LBL_NAME'] = 'Name des Jobs';
$mod_strings['LBL_NEVER'] = 'Nie';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Neuer Termin';
$mod_strings['LBL_NO_PHP_CLI'] = 'Wenn Ihr Host keine PHP-Binary zur Verfügung stellt, können Sie Ihre Jobs mittels wget oder curl starten:<br>for wget: <b>*    *    *    *    *    wget --quiet --non-verbose /cron.php > /dev/null 2>&1</b><br>for curl: <b>*    *    *    *    *    curl --silent /cron.php > /dev/null 2>&1"';
$mod_strings['LBL_OFTEN'] = ' So oft wie möglich.';
$mod_strings['LBL_ON_THE'] = 'Am';
$mod_strings['LBL_OOTB_BOUNCE'] = 'Unzustellbare Kampagnen E-Mails verarbeiten (Nacht)';
$mod_strings['LBL_OOTB_CAMPAIGN'] = 'E-Mail Kampagnen-Massenaussendung (Nacht)';
$mod_strings['LBL_OOTB_CLEANUP_QUEUE'] = 'Job-Warteschlange leeren';
$mod_strings['LBL_OOTB_IE'] = 'Eingehende E-Mail Konten überprüfen';
$mod_strings['LBL_OOTB_LUCENE_INDEX'] = 'Lucene Indexierung durchführen';
$mod_strings['LBL_OOTB_OPTIMISE_INDEX'] = 'AOD Indexierung optimieren';
$mod_strings['LBL_OOTB_PRUNE'] = 'Datenbank am 1. des Monats säubern';
$mod_strings['LBL_OOTB_REMOVE_DOCUMENTS_FROM_FS'] = 'Entfernung von Dokumenten aus dem Dateisystem';
$mod_strings['LBL_OOTB_REPORTS'] = 'Aufgabenberichte verarbeiten';
$mod_strings['LBL_OOTB_SEND_EMAIL_REMINDERS'] = 'E-Mail Erinnerungsbenachrichtigungen ausführen';
$mod_strings['LBL_OOTB_SUGARFEEDS'] = 'SuiteCRMFeed Tabellen löschen';
$mod_strings['LBL_OOTB_TRACKER'] = 'Benutzerverlauf löschen';
$mod_strings['LBL_OOTB_WORKFLOW'] = 'Workflow-Aufgaben verarbeiten';
$mod_strings['LBL_PERENNIAL'] = 'andauernd';
$mod_strings['LBL_PERFORMFULLFTSINDEX'] = 'Volltextsuche Systemindexierung';
$mod_strings['LBL_POLLMONITOREDINBOXES'] = 'Eingehende E-Mail Konten überprüfen';
$mod_strings['LBL_POLLMONITOREDINBOXESAOP'] = 'AOP überwachten Posteingang abrufen';
$mod_strings['LBL_POLLMONITOREDINBOXESFORBOUNCEDCAMPAIGNEMAILS'] = 'Unzustellbare Kampagnen E-Mails verarbeiten (Nacht)';
$mod_strings['LBL_PROCESSAOW_WORKFLOW'] = 'AOW-Workflows';
$mod_strings['LBL_PRUNEDATABASE'] = 'Datenbank am 1. des Monats säubern';
$mod_strings['LBL_RANGE'] = 'bis';
$mod_strings['LBL_REFRESHJOBS'] = 'Jobs aktualisieren';
$mod_strings['LBL_REMOVEDOCUMENTSFROMFS'] = 'Entfernung von Dokumenten aus dem Dateisystem';
$mod_strings['LBL_RUNMASSEMAILCAMPAIGN'] = 'E-Mail Kampagnen-Massenaussendung (Nacht)';
$mod_strings['LBL_SAT'] = 'Samstag';
$mod_strings['LBL_SCHEDULER'] = 'Geplante Aufgabe:';
$mod_strings['LBL_SCHEDULER_TIMES'] = 'Scheduler-Zeiten';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Geplante Aufgabe suchen';
$mod_strings['LBL_SENDEMAILREMINDERS'] = 'E-Mail Erinnerungs Benachrichtigungen senden';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_SUN'] = 'Sonntag';
$mod_strings['LBL_THU'] = 'Donnerstag';
$mod_strings['LBL_TIME_FROM'] = 'Aktiv von';
$mod_strings['LBL_TIME_TO'] = 'Aktiv bis';
$mod_strings['LBL_TOGGLE_ADV'] = 'Zeige erweiterte Optionen';
$mod_strings['LBL_TOGGLE_BASIC'] = 'Basisoptionen anzeigen';
$mod_strings['LBL_TRIMSUGARFEEDS'] = 'SuiteCRM Feed Tabellen löschen';
$mod_strings['LBL_TRIMTRACKER'] = 'Benutzerverlauf löschen';
$mod_strings['LBL_TUE'] = 'Dienstag';
$mod_strings['LBL_UPDATE_TRACKER_SESSIONS'] = 'Tracker_sessions-Tabelle aktualisieren';
$mod_strings['LBL_WARN_CURL'] = 'Warnung:';
$mod_strings['LBL_WARN_CURL_TITLE'] = 'cURL Warnung:';
$mod_strings['LBL_WARN_NO_CURL'] = 'Im System sind die cURL-bibliotheken im PHP-Modul nicht eingeschaltet/kompiliert (--with-curl=/path/to/curl_library). Bitte kontaktieren Sie den Administrator. Ohne die cURL-Funktionalität kann der Scheduler nicht ausgeführt werden.';
$mod_strings['LBL_WED'] = 'Mittwoch';
$mod_strings['LNK_LIST_SCHEDULED'] = 'Geplante Jobs';
$mod_strings['LNK_LIST_SCHEDULER'] = 'Geplante Aufgaben';
$mod_strings['LNK_NEW_SCHEDULER'] = 'Neue Aufgabe';
$mod_strings['NTC_DELETE_CONFIRMATION'] = 'Sind Sie sicher, dass Sie diesen Eintrag löschen wollen?';
$mod_strings['NTC_LIST_ORDER'] = 'Setzen Sie die Reihenfolge, in der dieser Plan in der Terminplaner Auswahlliste erscheinen soll';
$mod_strings['NTC_STATUS'] = 'Zum Entfernen dieses Plans vom Auswahlmenü des Terminplaners setzen Sie den Status auf inaktiv';
$mod_strings['SOCK_GREETING'] = '0';
