<?php

$dashletStrings['PipelineBySalesStageDashlet']['LBL_DESCRIPTION'] = 'Vertikales Balkendiagramm der Verkaufsphase-Pipeline';
$dashletStrings['PipelineBySalesStageDashlet']['LBL_REFRESH'] = 'Diagramm aktualisieren';
$dashletStrings['PipelineBySalesStageDashlet']['LBL_TITLE'] = 'Pipeline nach Verkaufsphasen';
