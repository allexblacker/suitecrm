<?php

$dashletStrings['ModuleActivityDashlet']['LBL_ACTIVITY_SCALE'] = 'Activität Skala';
$dashletStrings['ModuleActivityDashlet']['LBL_ACTIVITY_UNITS'] = 'erzeugte Datensätze';
$dashletStrings['ModuleActivityDashlet']['LBL_DESCRIPTION'] = 'Horizontales Balkendiagramm über den benutzergenerierten Datensätzen pro Modul';
$dashletStrings['ModuleActivityDashlet']['LBL_MODULES'] = 'Module';
$dashletStrings['ModuleActivityDashlet']['LBL_REFRESH'] = 'Diagramm Aktualisierung';
$dashletStrings['ModuleActivityDashlet']['LBL_TITLE'] = 'Modul Aktivität';
$dashletStrings['ModuleActivityDashlet']['LBL_TOTAL_ACTIVITY'] = 'Alle Aktivitäten für den Zeitraum';
