<?php

$dashletStrings['OutcomeByMonthDashlet']['LBL_DESCRIPTION'] = 'Diagramm der monatlichen Ergebnisse';
$dashletStrings['OutcomeByMonthDashlet']['LBL_REFRESH'] = 'Diagramm aktualisieren';
$dashletStrings['OutcomeByMonthDashlet']['LBL_TITLE'] = 'Resultat nach Monat';
