<?php

$dashletStrings['MyPipelineBySalesStageDashlet']['LBL_DESCRIPTION'] = 'Vertikales Balkendiagramm meiner Verkaufsphase-Pipeline';
$dashletStrings['MyPipelineBySalesStageDashlet']['LBL_REFRESH'] = 'Diagramm aktualisieren';
$dashletStrings['MyPipelineBySalesStageDashlet']['LBL_TITLE'] = 'Meine Pipeline nach Verkaufsphasen';
