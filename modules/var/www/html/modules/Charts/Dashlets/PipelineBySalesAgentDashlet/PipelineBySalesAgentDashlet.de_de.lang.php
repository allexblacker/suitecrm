<?php

$dashletStrings['PipelineBySalesAgentDashlet']['LBL_DESCRIPTION'] = 'Balkendiagramm für Vertriebsphase';
$dashletStrings['PipelineBySalesAgentDashlet']['LBL_REFRESH'] = 'Diagramm Aktualisierung';
$dashletStrings['PipelineBySalesAgentDashlet']['LBL_TITLE'] = 'Prozess in der Vertriebsphase';
