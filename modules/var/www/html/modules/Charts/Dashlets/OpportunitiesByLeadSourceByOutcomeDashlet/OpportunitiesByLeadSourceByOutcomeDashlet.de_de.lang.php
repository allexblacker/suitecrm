<?php

$dashletStrings['OpportunitiesByLeadSourceByOutcomeDashlet']['LBL_DESCRIPTION'] = 'Horizontal gestapelt Diagramm der Chancen nach Kundenkontakt-Quelle nach Ergebnis';
$dashletStrings['OpportunitiesByLeadSourceByOutcomeDashlet']['LBL_REFRESH'] = 'Diagramm aktualisieren';
$dashletStrings['OpportunitiesByLeadSourceByOutcomeDashlet']['LBL_TITLE'] = 'Alle Verkaufschancen nach Quelle und Ergebnis';
