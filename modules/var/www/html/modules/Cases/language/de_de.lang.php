<?php

$mod_strings['ERR_DELETE_RECORD'] = 'Um diesen Datensatz zu löschen, muss eine Datensatznummer angegeben werden';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Firmen';
$mod_strings['LBL_ACCOUNT_ID'] = 'Firmen ID:';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Firmenname:';
$mod_strings['LBL_ACCOUNT_NAME_MOD'] = 'Firmenname Mod';
$mod_strings['LBL_ACCOUNT_NAME_OWNER'] = 'Firmenname Besitzer';
$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Aktivitäten';
$mod_strings['LBL_ADD_CASE_FILE'] = 'Datei hinzufügen';
$mod_strings['LBL_AOP_CASE_ATTACHMENTS'] = 'Anhänge:';
$mod_strings['LBL_AOP_CASE_EVENTS'] = 'Fall-Ereignisse';
$mod_strings['LBL_AOP_CASE_UPDATES'] = 'Fall-Updates';
$mod_strings['LBL_AOP_CASE_UPDATES_THREADED'] = 'Fall-Aktualisierung angestoßen';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Zugewiesen an';
$mod_strings['LBL_ASSIGNED_USER_NAME_MOD'] = 'Zugewiesener Benutzername Mod';
$mod_strings['LBL_ASSIGNED_USER_NAME_OWNER'] = 'Zugewiesener Benutzername Besitzer';
$mod_strings['LBL_ATTACH_NOTE'] = 'Notiz anhängen';
$mod_strings['LBL_BUGS_SUBPANEL_TITLE'] = 'Fehler';
$mod_strings['LBL_CASE'] = 'Fall:';
$mod_strings['LBL_CASE_ATTACHMENTS_DISPLAY'] = 'Fall-Anhänge:';
$mod_strings['LBL_CASE_INFORMATION'] = 'Übersicht';
$mod_strings['LBL_CASE_NUMBER'] = 'Fallnummer:';
$mod_strings['LBL_CASE_SUBJECT'] = 'Fall Betreff:';
$mod_strings['LBL_CASE_UPDATES_COLLAPSE_ALL'] = 'Alles ausblenden';
$mod_strings['LBL_CASE_UPDATES_EXPAND_ALL'] = 'Alles einblenden';
$mod_strings['LBL_CASE_UPDATE_FORM'] = 'Anhang-Formular aktualisieren';
$mod_strings['LBL_CLEAR_CASE_DOCUMENT'] = 'Dokument leeren';
$mod_strings['LBL_CONTACTS_SUBPANEL_TITLE'] = 'Kontakte';
$mod_strings['LBL_CONTACT_CASE_TITLE'] = 'Kontakt:';
$mod_strings['LBL_CONTACT_CREATED_BY'] = 'Erstellt von';
$mod_strings['LBL_CONTACT_CREATED_BY_NAME'] = 'Erstellt von Kontakt';
$mod_strings['LBL_CONTACT_HISTORY_SUBPANEL_TITLE'] = 'E-Mails des verbundenen Kontakts';
$mod_strings['LBL_CONTACT_NAME'] = 'Kontaktname:';
$mod_strings['LBL_CONTACT_ROLE'] = 'Rolle:';
$mod_strings['LBL_CREATED_BY_NAME_MOD'] = 'Erstellt von Name Mod';
$mod_strings['LBL_CREATED_BY_NAME_OWNER'] = 'Erstellt von Besitzer des Namens';
$mod_strings['LBL_CREATED_USER'] = 'Erstellter Benutzer';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Fälle';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung:';
$mod_strings['LBL_DOCUMENTS_SUBPANEL_TITLE'] = 'Dokumente';
$mod_strings['LBL_EXPORT_ASSIGNED_USER_ID'] = 'Zugewiesene Benutzer ID';
$mod_strings['LBL_EXPORT_ASSIGNED_USER_NAME'] = 'Zugewiesener Benutzername';
$mod_strings['LBL_EXPORT_CREATED_BY'] = 'Erstellt von ID';
$mod_strings['LBL_EXPORT_CREATED_BY_NAME'] = 'Erstellt von Benutzername';
$mod_strings['LBL_EXPORT_MODIFIED_USER_ID'] = 'Geändert von ID';
$mod_strings['LBL_EXPORT_TEAM_COUNT'] = 'Team Anzahl';
$mod_strings['LBL_FILENANE_ATTACHMENT'] = 'Dateianhang';
$mod_strings['LBL_HISTORY_SUBPANEL_TITLE'] = 'Verlauf';
$mod_strings['LBL_INTERNAL'] = 'Internes Update';
$mod_strings['LBL_INVITEE'] = 'Kontakte';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Firmenname';
$mod_strings['LBL_LIST_ASSIGNED'] = 'Zugewiesen an';
$mod_strings['LBL_LIST_ASSIGNED_TO_NAME'] = 'Zugewiesene Benutzer';
$mod_strings['LBL_LIST_CLOSE'] = 'Schließen';
$mod_strings['LBL_LIST_DATE_CREATED'] = 'Erstellungsdatum';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Fälle Liste';
$mod_strings['LBL_LIST_LAST_MODIFIED'] = 'Zuletzt geändert am';
$mod_strings['LBL_LIST_MY_CASES'] = 'Meine offenen Fälle';
$mod_strings['LBL_LIST_NUMBER'] = 'Nr.';
$mod_strings['LBL_LIST_PRIORITY'] = 'Priorität';
$mod_strings['LBL_LIST_STATUS'] = 'Status';
$mod_strings['LBL_LIST_SUBJECT'] = 'Betreff';
$mod_strings['LBL_MEMBER_OF'] = 'Firma';
$mod_strings['LBL_MODIFIED_BY_NAME_MOD'] = 'Geändert von Name Mod';
$mod_strings['LBL_MODIFIED_BY_NAME_OWNER'] = 'Name des Eigentümers geändert';
$mod_strings['LBL_MODIFIED_USER'] = 'Geänderter Benutzer';
$mod_strings['LBL_MODIFIED_USER_NAME'] = 'Geänderter Benutzername';
$mod_strings['LBL_MODIFIED_USER_NAME_MOD'] = 'Geänderter Benutzername Mod';
$mod_strings['LBL_MODIFIED_USER_NAME_OWNER'] = 'Geänderter Benutzername Besitzer';
$mod_strings['LBL_MODULE_NAME'] = 'Fälle';
$mod_strings['LBL_MODULE_TITLE'] = 'Fälle: Startseite';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Neue Anfrage';
$mod_strings['LBL_NO_CASE_UPDATES'] = 'Zu diesem Fall gibt es keine Aktualisierungen';
$mod_strings['LBL_NO_SUGGESTIONS'] = 'No Suggestions';
$mod_strings['LBL_NUMBER'] = 'Nummer:';
$mod_strings['LBL_PORTAL_VIEWABLE'] = 'Portal sichtbar';
$mod_strings['LBL_PRIORITY'] = 'Priorität:';
$mod_strings['LBL_PROJECTS_SUBPANEL_TITLE'] = 'Projekte';
$mod_strings['LBL_PROJECT_SUBPANEL_TITLE'] = 'Projekte';
$mod_strings['LBL_REMOVE_CASE_FILE'] = 'Entferne Datei';
$mod_strings['LBL_RESOLUTION'] = 'Lösung:';
$mod_strings['LBL_RESOLUTION_BUTTON'] = 'Resolution';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Fälle suchen';
$mod_strings['LBL_SELECT_CASE_DOCUMENT'] = 'Dokument auswählen';
$mod_strings['LBL_SELECT_EXTERNAL_CASE_DOCUMENT'] = 'Externe Datei';
$mod_strings['LBL_SELECT_INTERNAL_CASE_DOCUMENT'] = 'Internes CRM-Dokument';
$mod_strings['LBL_STATUS'] = 'Status:';
$mod_strings['LBL_SUBJECT'] = 'Betreff:';
$mod_strings['LBL_SUGGESTION_BOX'] = 'Suggestions';
$mod_strings['LBL_SUGGESTION_BOX_REL'] = 'Relevance';
$mod_strings['LBL_SUGGESTION_BOX_STATUS'] = 'Status';
$mod_strings['LBL_SUGGESTION_BOX_TITLE'] = 'Title';
$mod_strings['LBL_SYSTEM_ID'] = 'System ID';
$mod_strings['LBL_TEAM_COUNT_MOD'] = 'Team Anzahl Mod';
$mod_strings['LBL_TEAM_COUNT_OWNER'] = 'Team Anzahl Besitzer';
$mod_strings['LBL_TEAM_NAME_MOD'] = 'Team Name Mod';
$mod_strings['LBL_TEAM_NAME_OWNER'] = 'Team Namenbesitzer';
$mod_strings['LBL_TOOL_TIP_BODY'] = 'Body: ';
$mod_strings['LBL_TOOL_TIP_BOX_TITLE'] = 'KnowledgeBase Suggestions';
$mod_strings['LBL_TOOL_TIP_INFO'] = 'Additional Info: ';
$mod_strings['LBL_TOOL_TIP_TITLE'] = 'Title: ';
$mod_strings['LBL_TOOL_TIP_USE'] = 'Use as: ';
$mod_strings['LBL_TYPE'] = 'Typ';
$mod_strings['LBL_UPDATE_TEXT'] = 'Aktualisieren des Textes';
$mod_strings['LBL_WORK_LOG'] = 'Arbeitsprotokoll';
$mod_strings['LNK_CASE_LIST'] = 'Fälle anzeigen';
$mod_strings['LNK_IMPORT_CASES'] = 'Fälle importieren';
$mod_strings['LNK_NEW_CASE'] = 'Fall erstellen';
$mod_strings['NTC_REMOVE_FROM_BUG_CONFIRMATION'] = 'Möchten Sie diesen Fall wirklich vom Fehler entfernen?';
$mod_strings['NTC_REMOVE_INVITEE'] = 'Möchten Sie diesen Kontakt wirklich von diesem Fall entfernen?';
