<?php

$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Benutzer';
$mod_strings['LBL_CALLBACK_URL'] = 'Callback URL';
$mod_strings['LBL_CONSUMER'] = 'Verbrauchername';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_LIST_DELETE'] = 'Löschen von Token';
$mod_strings['LBL_OAUTH_AUTHORIZE'] = 'Token autorisieren';
$mod_strings['LBL_OAUTH_CONSUMERREQ'] = 'Token von Verbraucher autorisieren <b>%s</b>?';
$mod_strings['LBL_OAUTH_DISABLED'] = 'OAuth Unterstützung nicht aktiviert. Die PHP oauth Erweiterung könnte fehlen. Bitte kontaktieren Sie Ihren Administrator.';
$mod_strings['LBL_OAUTH_REQUEST'] = 'Anfordern von Tokens';
$mod_strings['LBL_OAUTH_ROLE'] = 'Token-Rolle';
$mod_strings['LBL_OAUTH_VALIDATION'] = 'Verifizierungs-Code';
$mod_strings['LBL_SECRET'] = 'Geheim';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_TOKEN_TS'] = 'Token TS';
$mod_strings['LBL_TS'] = 'Zeitstempel';
$mod_strings['LBL_TSTATE'] = 'TState';
$mod_strings['LBL_VERIFY'] = 'Überprüfen';
