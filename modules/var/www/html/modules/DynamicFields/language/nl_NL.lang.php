<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LNK_NEW_CALL' => 'Nieuw telefoongesprek',
    'LNK_NEW_MEETING' => 'Nieuwe afspraak',
    'LNK_NEW_TASK' => 'Nieuwe taak',
    'LNK_NEW_NOTE' => 'Nieuwe notitie of bijlage',
    'LNK_NEW_EMAIL' => 'E-mail archiveren',
    'LNK_CALL_LIST' => 'Telefoongesprekken',
    'LNK_MEETING_LIST' => 'Afspraken',
    'LNK_TASK_LIST' => 'Taken',
    'LNK_NOTE_LIST' => 'Notities',
    'LNK_EMAIL_LIST' => 'E-mails',
    'LBL_ADD_FIELD' => 'Veld toevoegen:',
    'LBL_SEARCH_FORM_TITLE' => 'Zoek module ',
    'COLUMN_TITLE_NAME' => 'Veldnaam',
    'COLUMN_TITLE_DISPLAY_LABEL' => 'Label weergeven',
    'COLUMN_TITLE_LABEL_VALUE' => 'Labelwaarde',
    'COLUMN_TITLE_LABEL' => 'Systeemlabel',
    'COLUMN_TITLE_DATA_TYPE' => 'Gegevenstype',
    'COLUMN_TITLE_MAX_SIZE' => 'Maximale grootte',
    'COLUMN_TITLE_HELP_TEXT' => 'Helptekst',
    'COLUMN_TITLE_COMMENT_TEXT' => 'Reactie tekst',
    'COLUMN_TITLE_REQUIRED_OPTION' => 'Verplicht veld',
    'COLUMN_TITLE_DEFAULT_VALUE' => 'Standaardwaarde',
    'COLUMN_TITLE_FRAME_HEIGHT' => 'IFrame Hoogte',
    'COLUMN_TITLE_HTML_CONTENT' => 'HTML',
    'COLUMN_TITLE_URL' => 'Standaard URL',
    'COLUMN_TITLE_AUDIT' => 'Audit',
    'COLUMN_TITLE_MIN_VALUE' => 'Min. waarde',
    'COLUMN_TITLE_MAX_VALUE' => 'Max. waarde',
    'COLUMN_TITLE_LABEL_ROWS' => 'Rijen',
    'COLUMN_TITLE_LABEL_COLS' => 'Kolommen',
    'COLUMN_TITLE_DISPLAYED_ITEM_COUNT' => '# Items weergegeven',
    'COLUMN_TITLE_AUTOINC_NEXT' => 'Automatisch ophogen volgende waarde',
    'COLUMN_DISABLE_NUMBER_FORMAT' => 'Uitschakelen formatering',
    'COLUMN_TITLE_ENABLE_RANGE_SEARCH' => 'Inschakelen reeks zoeken',
    'LBL_DROP_DOWN_LIST' => 'Drop-down lijst',
    'LBL_RADIO_FIELDS' => 'Radio velden',
    'LBL_MULTI_SELECT_LIST' => 'Multi selectie lijst',
    'COLUMN_TITLE_PRECISION' => 'Precisie',
    'LBL_MODULE' => 'Module',
    'COLUMN_TITLE_MASS_UPDATE' => 'Bulk update',
    'COLUMN_TITLE_IMPORTABLE' => 'Importeerbaar',
    'COLUMN_TITLE_DUPLICATE_MERGE' => 'Dubbelen samenvoegen',
    'LBL_LABEL' => 'Omschrijving',
    'LBL_DATA_TYPE' => 'Data type',
    'LBL_DEFAULT_VALUE' => 'Standaardwaarde',
    'ERR_RESERVED_FIELD_NAME' => "Gereserveerd keywoord",
    'ERR_SELECT_FIELD_TYPE' => 'Selecteer een veldtype',
    'ERR_FIELD_NAME_ALREADY_EXISTS' => 'Veldnaam bestaat al',
    'LBL_BTN_ADD' => 'Toevoegen',
    'LBL_BTN_EDIT' => 'Bewerken',
    'LBL_GENERATE_URL' => 'URL genereren',
    'LBL_CALCULATED' => 'Berekende waarde',
    'LBL_FORMULA' => 'Formule',
    'LBL_DYNAMIC_VALUES_CHECKBOX' => 'Afhankelijke',
    'LBL_BTN_EDIT_VISIBILITY' => 'Zichtbaarheid bewerken',
    'LBL_LINK_TARGET' => 'Open link in',
    'LBL_IMAGE_WIDTH' => 'Breedte',
    'LBL_IMAGE_HEIGHT' => 'Hoogte',
    'LBL_IMAGE_BORDER' => 'Grens',
    'COLUMN_TITLE_VALIDATE_US_FORMAT' => 'US-formaat',
    'LBL_DEPENDENT' => 'Afhankelijke',
    'LBL_VISIBLE_IF' => 'Zichtbaar Als',
    'LBL_ENFORCED' => 'Gedwongen',
    'LBL_HELP' => 'Hulp' /*for 508 compliance fix*/,
    'COLUMN_TITLE_INLINE_EDIT_TEXT' => 'Bewerken in regel',
    'COLUMN_TITLE_PARENT_ENUM' => 'Bovenliggend Dropdown',
);


?>
