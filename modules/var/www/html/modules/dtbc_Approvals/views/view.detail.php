<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');
require_once('modules/dtbc_Approvals/ApprovalsHelper.php');
require_once("modules/AOW_WorkFlow/aow_utils.php");

class dtbc_ApprovalsViewDetail extends ViewDetail
{	
	private $partMap = array();

	public function __construct() {
		global $mod_strings;

		$this->partMap[ApprovalsParts::entry] =  $mod_strings['LBL_ENTRY_CRITERIAS'];
		$this->partMap[ApprovalsParts::initial] = $mod_strings['LBL_INITIAL_SUBMISSION_ACTIONS'];
		$this->partMap[ApprovalsParts::step] = $mod_strings['LBL_APPROVAL_STEPS'];
		$this->partMap[ApprovalsParts::approval] = $mod_strings['LBL_APPROVAL_ACTIONS'];
		$this->partMap[ApprovalsParts::rejection] = $mod_strings['LBL_REJECTION_ACTIONS'];
		$this->partMap[ApprovalsParts::recall] = $mod_strings['LBL_RECALL_ACTIONS'];
	}

	private function createButton($params, $label, $confirmLabel = '') {
		global $mod_strings;
		
		$confirmPart = "";
		if (!empty($confirmLabel)) 
			$confirmPart = "onclick='return confirm(\"" . $mod_strings[$confirmLabel] . "\");'";
		
		return "<a href='index.php?module=dtbc_Approvals&$params' $confirmPart>" . $mod_strings[$label] . "</a>";
	}
		
	private function createEditActionButton($dbRow, $action) {
		return $this->createButton("action=$action&action_id=" . $dbRow['id'], 'LBL_EDIT');
	}
	
	private function createRemoveActionButton($dbRow, $type) {
		return $this->createButton("action=remove_action&type=$type&action_id=" . $dbRow['id'], 'LBL_REMOVE', 'LBL_REALLY_DELETE');
	}
	
	private function createEditCriteriaButton($dbRow) {
		return $this->createButton("action=new_criteria&criteria_id=" . $dbRow['id'], 'LBL_EDIT');
	}
	
	private function createRemoveCriteriaButton($dbRow) {
		return $this->createButton("action=remove_criteria&criteria_id=" . $dbRow['id'], 'LBL_REMOVE', 'LBL_REALLY_DELETE');
	}
	
	private function createEditStepButton($dbRow) {
		return $this->createButton("action=new_step&step_id=" . $dbRow['id'], 'LBL_EDIT');
	}
	
	private function createRemoveStepButton($dbRow) {
		return $this->createButton("action=remove_step&step_id=" . $dbRow['id'], 'LBL_REMOVE', 'LBL_REALLY_DELETE');
	}
	
	private function createEditStepCriteriaButton($dbRow, $criteriaId) {
		$criteriaPart = empty($criteriaId) ? "" : "&criteria_id=" . $criteriaId;
		return $this->createButton("action=new_criteria$criteriaPart&approval_id=" . $dbRow['approval_id'] . "&approval_step_id=" . $dbRow['id'], 'LBL_SET_CRITERIA');
	}
	
	private function createStepUpButton($dbRow) {
		return $this->createButton("action=step_up&step_id=" . $dbRow['id'], 'LBL_UP');
	}
	
	private function createStepDownButton($dbRow) {
		return $this->createButton("action=step_down&step_id=" . $dbRow['id'], 'LBL_DOWN');
	}
	
	private function prepareStepRow($dbRow, $users, $securityGroups) {
		global $mod_strings;

		$cells = array();
		
		$db = DBManagerFactory::getInstance();
		
		$criteriaCell = "";
		$criteria = $db->fetchOne("SELECT * FROM dtbc_approvals_criterias WHERE approval_step_id = " . $db->quoted($dbRow['id']));
		
		if (!empty($criteria['id'])) {
			$criteriaLines = array();
			$criteriaLineQuery = "SELECT * FROM dtbc_approvals_criteria_lines WHERE approval_criteria_id = " . $db->quoted($criteria['id']) . " ORDER BY `order`";
			$criteriaLineResponse = $db->query($criteriaLineQuery);
			
			$formula = $criteria['formula'];
			$count = 0;
			while($row = $db->fetchByAssoc($criteriaLineResponse)) {
				$formula = str_replace("{C$count}", "[" . $this->getRealModule($row) . "." . $this->formatFieldName($row) . " " . $this->formatOperator($row) . " " . $this->formatValue($row) . "]", $formula);
				$count++;
			}
			
			$criteriaCell = $formula . " " . $mod_strings['LBL_ELSE'] . " " . $mod_strings['LBL_BEHAVIOURS_ARRAY'][$criteria['behaviour']];
		}
		
		$approversQuery = "SELECT * FROM dtbc_approvals_approvers WHERE approval_step_id = " . $db->quoted($dbRow['id']);
		$approversResponse = $db->query($approversQuery);
		
		$approvers = array();
		while($row = $db->fetchByAssoc($approversResponse)) {
			$approvers []= $row['type'] == "user" ? $mod_strings['LBL_USER'] . ": " . $users[$row['value']] : $mod_strings['LBL_GROUP'] . ": " . $securityGroups[$row['value']];
		}
		
		$buttons = $this->createEditStepButton($dbRow) . " | " . $this->createEditStepCriteriaButton($dbRow, $criteria['id']) . " | " . $this->createRemoveStepButton($dbRow);
		$buttons .= " | " . $this->createStepUpButton($dbRow) . " | " . $this->createStepDownButton($dbRow);
		
		$cells []= $buttons;
		$cells []= intval($dbRow['order']) + 1;
		$cells []= $dbRow['name'];
		$cells []= $dbRow['description'];
		$cells []= implode("<br/>", $approvers);
		$cells []= $criteriaCell;
		
		return $cells;
	}
	
	private function prepareEntryRow($dbRow) {
		$cells = array();
		
		$db = DBManagerFactory::getInstance();
		
		$criteriaLines = array();
		$criteriaLineQuery = "SELECT * FROM dtbc_approvals_criteria_lines WHERE approval_criteria_id = " . $db->quoted($dbRow['id']) . " ORDER BY `order`";
		$criteriaLineResponse = $db->query($criteriaLineQuery);
		
		$formula = $dbRow['formula'];
		$count = 0;
		while($row = $db->fetchByAssoc($criteriaLineResponse)) {
			$formula = str_replace("{C$count}", "[" . $this->getRealModule($row) . "." . $this->formatFieldName($row) . " " . $this->formatOperator($row) . " " . $this->formatValue($row) . "]", $formula);
			$count++;
		}
		
		$cells []= $this->createEditCriteriaButton($dbRow) . " | " . $this->createRemoveCriteriaButton($dbRow);
		$cells []= $formula;
		
		return $cells;
	}
	
	private function getRealModule($row) {
		$module = $this->bean->target_module;
		
		if($this->bean->target_module != $row['path'])
			$module = getRelatedModule($this->bean->target_module, $row['path']);
		
		return $module;
	}
	
	private function formatFieldName($row) {
		return getModuleFields($this->getRealModule($row), 'DetailView', $row['field']);
	}
	
	private function formatOperator($row) {
		global $app_list_strings;
		return $app_list_strings['aow_operator_list'][$row['operator']];
	}
	
	private function formatValue($row) {
		return getModuleField($this->getRealModule($row), $row['field'], $row['field'], 'DetailView', $row['value']);
	}
	
	private function prepareActionRow($dbRow) {
		global $mod_strings;

		$cells = array();
		
		$editAction = $dbRow['type'] == 'fieldupdate' ? "new_field_update" : "new_email_alert";
		
		$cells []= $this->createEditActionButton($dbRow, $editAction) . " | " . $this->createRemoveActionButton($dbRow, $dbRow['type']);
		$cells []= $mod_strings['LBL_ACTION_TYPE_ARRAY'][$dbRow['type']];
		
		if ($dbRow['type'] == 'fieldupdate')
			$cells []= $mod_strings['LBL_UPDATE'] . " " . $dbRow['field'] . " - " . $dbRow['value'];
		else 
			$cells []= $mod_strings['LBL_EMAIL_ALERT'];
		
		$cells []= $dbRow['description'];
		
		return $cells;
	}
	
	private function createActionHeaders() {
		global $mod_strings;
		return array($mod_strings['LBL_MANAGE'], $mod_strings['LBL_TYPE'], $mod_strings['LBL_ACTION'], $mod_strings['LBL_DESCRIPTION']);
	}
	
	private function createActionpart($id, $uiRows) {
		return array(
				"id" => $id,
				"name" => $this->partMap[$id],
				"headers" => $this->createActionHeaders(),
				"rows" => empty($uiRows[$id]) ? "" : $uiRows[$id],
			   );
	}
	
	public function display(){
		global $mod_strings;

		ApprovalsHelper::recreateEmailTemplateList();
		
		parent::display();
		
		$db = DBManagerFactory::getInstance();
		
		$actionsQuery = "SELECT * FROM dtbc_approvals_actions WHERE approval_id = " . $db->quoted($this->bean->id);
		$response = $db->query($actionsQuery);
		
		$uiRows = array();
		
		while($row = $db->fetchByAssoc($response)) {
			$uiRows[$row['approval_part_id']] []= $this->prepareActionRow($row);
		}
		
		$criteriasQuery = "SELECT * FROM dtbc_approvals_criterias WHERE approval_part_id = " . $db->quoted(ApprovalsParts::entry) . " AND approval_id = " . $db->quoted($this->bean->id);
		$response = $db->query($criteriasQuery);	

		while($row = $db->fetchByAssoc($response)) {
			$uiRows[$row['approval_part_id']] []= $this->prepareEntryRow($row);
		}
		
		$usersQuery = "SELECT id, first_name, last_name FROM users WHERE deleted = 0 ORDER BY first_name";
		$usersResponse = $db->query($usersQuery);
			
		$users = array();
		while($row = $db->fetchByAssoc($usersResponse)) {
			$users[$row['id']] = $row['first_name'] . " " . $row['last_name'];
		}
		
		$securityGroupsQuery = "SELECT id, name FROM securitygroups WHERE deleted = 0 ORDER BY name";
		$securityGroupsResponse = $db->query($securityGroupsQuery);
			
		$securityGroups = array();
		while($row = $db->fetchByAssoc($securityGroupsResponse)) {
			$securityGroups[$row['id']] = $row['name'];
		}
		
		$stepsQuery = "SELECT * FROM dtbc_approvals_steps WHERE approval_id = " . $db->quoted($this->bean->id) . " ORDER BY `order`";
		$response = $db->query($stepsQuery);	

		while($row = $db->fetchByAssoc($response)) {
			$uiRows[ApprovalsParts::step] []= $this->prepareStepRow($row, $users, $securityGroups);
		}	
		
		$sections = array(
			array(
				"id" => ApprovalsParts::entry,
				"name" => $this->partMap[ApprovalsParts::entry],
				"headers" => array($mod_strings['LBL_MANAGE'], $mod_strings['LBL_CRITERIA']),
				"rows" => empty($uiRows[ApprovalsParts::entry]) ? "" : $uiRows[ApprovalsParts::entry],
		    ),
			$this->createActionpart(ApprovalsParts::initial, $uiRows),
			array(
				"id" => ApprovalsParts::step,
				"name" => $this->partMap[ApprovalsParts::step],
				"headers" => array($mod_strings['LBL_MANAGE'], $mod_strings['LBL_ORDER'], $mod_strings['LBL_NAME'], $mod_strings['LBL_DESCRIPTION'], $mod_strings['LBL_APPROVERS'], $mod_strings['LBL_CRITERIA']),
				"rows" => empty($uiRows[ApprovalsParts::step]) ? "" : $uiRows[ApprovalsParts::step],
		    ),
			$this->createActionpart(ApprovalsParts::approval, $uiRows),
			$this->createActionpart(ApprovalsParts::rejection, $uiRows),
			$this->createActionpart(ApprovalsParts::recall, $uiRows),
		);
		
		$smarty = new Sugar_Smarty();
		
		$smarty->assign('sections', $sections);
		$smarty->assign('beanId', $this->bean->id);
		$smarty->assign('mod', $mod_strings);
		
		$smarty->display('modules/dtbc_Approvals/tpls/ApprovalsPanels.tpl');
	}
	
	private function displayEditIcon() {
		echo "<a class='edit-link' title='$label' href='$href'>
				<img src='themes/SolarEdgeP/images/edit_inline.gif' border='0' alt='$label'>
			  </a>";
	}
}
