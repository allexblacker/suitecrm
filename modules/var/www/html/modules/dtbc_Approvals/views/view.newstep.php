<?php
if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once("modules/AOW_WorkFlow/aow_utils.php");
require_once('modules/dtbc_Approvals/ApprovalsHelper.php');

class ViewNewStep extends SugarView
{    
	public function display()
	{
		global $mod_strings;

		if (empty($_REQUEST['approval_id']) && empty($_REQUEST['step_id']))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$db = DBManagerFactory::getInstance();
		
		$step = null;
		if (!empty($_REQUEST['step_id'])) {
			$stepQuery = "SELECT * FROM dtbc_approvals_steps WHERE id = " . $db->quoted($_REQUEST['step_id']);
			$step = $db->fetchOne($stepQuery);
		}
		
		$stepId = empty($step) ? $_REQUEST['step_id'] : $step['id'];
		$approvalId = empty($step) ? $_REQUEST['approval_id'] : $step['approval_id'];
				
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($approvalId);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$usersQuery = "SELECT id, first_name, last_name FROM users WHERE deleted = 0 ORDER BY first_name";
		$usersResponse = $db->query($usersQuery);
			
		$users = array();
		while($row = $db->fetchByAssoc($usersResponse)) {
			$users[$row['id']] = $row['first_name'] . " " . $row['last_name'];
		}
		
		$securityGroupsQuery = "SELECT id, name FROM securitygroups WHERE deleted = 0 ORDER BY name";
		$securityGroupsResponse = $db->query($securityGroupsQuery);
			
		$securityGroups = array();
		while($row = $db->fetchByAssoc($securityGroupsResponse)) {
			$securityGroups[$row['id']] = $row['name'];
		}
		
		$approversQuery = "SELECT * FROM dtbc_approvals_approvers WHERE approval_step_id = " . $db->quoted($stepId);
		$approversResponse = $db->query($approversQuery);
		
		$approvers = array();
		while($approversRow = $db->fetchByAssoc($approversResponse)) {
			$approvers []= array(
				"type" => $approversRow['type'],
				"value" => $approversRow['value'],
				"displayValue" => $approversRow['type'] == 'user' ? $users[$approversRow['value']] : $securityGroups[$approversRow['value']],
			);
		}
				
		$smarty = new Sugar_Smarty();
		
		$smarty->assign('stepId', $stepId);
		$smarty->assign('beanId', $approvalBean->id);
		
		$smarty->assign('name', empty($step) ? "" : $step['name']);
		$smarty->assign('description', empty($step) ? "" : $step['description']);
		$smarty->assign('approval_method', empty($step) ? "" : $step['approval_method']);
		
		$smarty->assign('users', $users);
		$smarty->assign('groups', $securityGroups);
		$smarty->assign('approvalMethods', $mod_strings['LBL_APPROVAL_METHOD_ARRAY']);
		$smarty->assign('mod', $mod_strings);
		
		$smarty->assign('approvers', $approvers);
		
		$smarty->display('modules/dtbc_Approvals/tpls/NewStep.tpl');
    }
}
