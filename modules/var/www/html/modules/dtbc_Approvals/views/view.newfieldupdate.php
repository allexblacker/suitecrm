<?php
if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once("modules/AOW_WorkFlow/aow_utils.php");
require_once('modules/dtbc_Approvals/ApprovalsHelper.php');

class ViewNewFieldUpdate extends SugarView
{    
	public function display()
	{
		global $mod_strings;

		if (empty($_REQUEST['action_id']) && empty($_REQUEST['approval_id']))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$action = null;
		if (!empty($_REQUEST['action_id'])) {
			$db = DBManagerFactory::getInstance();
			
			$actionQuery = "SELECT * FROM dtbc_approvals_actions WHERE id = " . $db->quoted($_REQUEST['action_id']). " ";
			$action = $db->fetchOne($actionQuery);
		}
		
		$approvalId = empty($action) ? $_REQUEST['approval_id'] : $action['approval_id'];
		$approvalPartId = empty($action) ? $_REQUEST['approval_part_id'] : $action['approval_part_id'];
		$approvalStepId = empty($action) ? $_REQUEST['approval_step_id'] : $action['approval_step_id'];
		
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($approvalId);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');

        $fields = getModuleFields($approvalBean->target_module);
				
		$smarty = new Sugar_Smarty();
		$smarty->assign('beanId', $approvalBean->id);
		$smarty->assign('actionId', $_REQUEST['action_id']);
		$smarty->assign('partId', $approvalPartId);
		$smarty->assign('stepId', $approvalStepId);
		$smarty->assign('fields', $fields);
		$smarty->assign('target_module', $approvalBean->target_module);
		$smarty->assign('mod', $mod_strings);
		
		$smarty->assign('name', empty($action) ? "" : $action['name']);
		$smarty->assign('description', empty($action) ? "" : $action['description']);
		$smarty->assign('field', empty($action) ? "" : $action['field']);
		$smarty->assign('value', empty($action) ? "" : $action['value']);
		
		$smarty->display('modules/dtbc_Approvals/tpls/NewFieldUpdate.tpl');
    }
}
