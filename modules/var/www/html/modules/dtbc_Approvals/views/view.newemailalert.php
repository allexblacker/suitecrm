<?php
if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('modules/dtbc_Approvals/ApprovalsHelper.php');

class ViewNewEmailAlert extends SugarView
{    
	public function display()
	{
		global $mod_strings;

		if (empty($_REQUEST['action_id']) && empty($_REQUEST['approval_id']))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');

		$db = DBManagerFactory::getInstance();
	
		$action = null;
		if (!empty($_REQUEST['action_id'])) {
			$actionQuery = "SELECT * FROM dtbc_approvals_actions WHERE id = " . $db->quoted($_REQUEST['action_id']);
			$action = $db->fetchOne($actionQuery);
		}
		
		$approvalId = empty($action) ? $_REQUEST['approval_id'] : $action['approval_id'];
		$approvalPartId = empty($action) ? $_REQUEST['approval_part_id'] : $action['approval_part_id'];
		$approvalStepId = empty($action) ? $_REQUEST['approval_step_id'] : $action['approval_step_id'];
		
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($approvalId);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		ApprovalsHelper::recreateEmailTemplateList();
		$templates = ApprovalsHelper::getEmailTemplateList();
		
		$usersQuery = "SELECT id, first_name, last_name FROM users WHERE deleted = 0";
		$usersResponse = $db->query($usersQuery);
		
		$users = array();
		while($row = $db->fetchByAssoc($usersResponse)) {
			$users[$row['id']] = $row['first_name'] . " " . $row['last_name'];
		}
		
		$recipientsQuery = "SELECT * FROM dtbc_approvals_recipients WHERE approval_action_id = " . $db->quoted($_REQUEST['action_id']);
		$recipientsResponse = $db->query($recipientsQuery);
		
		$recipients = array();
		while($recipientRow = $db->fetchByAssoc($recipientsResponse)) {
			$recipients []= array(
				"type" => $recipientRow['type'],
				"value" => $recipientRow['value'],
				"displayValue" => $recipientRow['type'] == 'user' ? $users[$recipientRow['value']] : $recipientRow['value'],
			);
		}
		
		$smarty = new Sugar_Smarty();
		
		$smarty->assign('beanId', $approvalBean->id);
		$smarty->assign('actionId', $_REQUEST['action_id']);
		$smarty->assign('partId', $approvalPartId);
		$smarty->assign('stepId', $approvalStepId);
		$smarty->assign('target_module', $approvalBean->target_module);
		$smarty->assign('templates', $templates);
		$smarty->assign('types', $mod_strings['LBL_RECIPIENT_TYPE_ARRAY']);
		$smarty->assign('mod', $mod_strings);
		
		$smarty->assign('name', empty($action) ? "" : $action['name']);
		$smarty->assign('description', empty($action) ? "" : $action['description']);
		$smarty->assign('template', empty($action) ? "" : $action['template']);
		
		$smarty->assign('recipients', $recipients);
		
		$smarty->display('modules/dtbc_Approvals/tpls/NewEmailAlert.tpl');
    }
}
