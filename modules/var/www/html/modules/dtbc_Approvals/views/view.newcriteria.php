<?php
if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once("modules/AOW_WorkFlow/aow_utils.php");
require_once('modules/dtbc_Approvals/ApprovalsHelper.php');

class ViewNewCriteria extends SugarView
{    
	public function display()
	{
		global $mod_strings;
		
		if (empty($_REQUEST['approval_id']) && empty($_REQUEST['criteria_id']))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$db = DBManagerFactory::getInstance();
		
		$criteria = null;
		if (!empty($_REQUEST['criteria_id'])) {
			$criteriaQuery = "SELECT * FROM dtbc_approvals_criterias WHERE id = " . $db->quoted($_REQUEST['criteria_id']);
			$criteria = $db->fetchOne($criteriaQuery);
		}
		
		$criteriaId = empty($criteria) ? $_REQUEST['criteria_id'] : $criteria['id'];
		$approvalId = empty($criteria) ? $_REQUEST['approval_id'] : $criteria['approval_id'];
		$approvalPartId = empty($criteria) ? (empty($_REQUEST['approval_part_id']) ? "" : $_REQUEST['approval_part_id']) : $criteria['approval_part_id'];
		$approvalStepId = empty($criteria) ? (empty($_REQUEST['approval_step_id']) ? "" : $_REQUEST['approval_step_id']) : $criteria['approval_step_id'];
		
		$criteriaLines = array();
		if (!empty($criteria)) {
			$criteriaLineQuery = "SELECT * FROM dtbc_approvals_criteria_lines WHERE approval_criteria_id = " . $db->quoted($criteria['id']) . " ORDER BY `order`";
			$criteriaLineResponse = $db->query($criteriaLineQuery);
			
			while($row = $db->fetchByAssoc($criteriaLineResponse)) {
				$criteriaLines []= $row;
			}
		}
		
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($approvalId);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
				
		$smarty = new Sugar_Smarty();
		
		$smarty->assign('criteriaId', $criteriaId);
		$smarty->assign('beanId', $approvalBean->id);
		$smarty->assign('partId', $approvalPartId);
		$smarty->assign('stepId', $approvalStepId);

		$smarty->assign('criteriaLines', $criteriaLines);
		$smarty->assign('formula', empty($criteria) ? "" : $criteria['formula']);
		$smarty->assign('target_module', $approvalBean->target_module);
		
		$smarty->assign('behaviour', empty($criteria) ? "approve" : $criteria['behaviour']);
		$smarty->assign('behaviour_options', $mod_strings['LBL_BEHAVIOURS_ARRAY']);
		$smarty->assign('mod', $mod_strings);
		
		$smarty->display('modules/dtbc_Approvals/tpls/NewCriteria.tpl');
    }
}
