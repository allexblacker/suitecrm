{literal}
<style>
#criteriaTable td {
	padding: 10px;
}

#criteriaTable th {
	padding: 6px 10px;
	text-align: center;
}

.idTd {
	font-weight: bold;
}
</style>
{/literal}

<div class="moduleTitle" style="margin-bottom: 30px;">
	<h2 class="module-title-text">{$mod.LBL_CRITERIA}</h2>
	<div class="clear"></div>
</div>

<form action="index.php" method="POST" name="EditView" id="EditView">
	<input type="hidden" name="module" id="module" value="dtbc_Approvals" />
	<input type="hidden" name="action" id="action" value="save_criteria" />
	<input type="hidden" name="criteria_id" id="criteria_id" value="{$criteriaId}" />
	<input type="hidden" name="approval_id" id="approval_id" value="{$beanId}" />
	<input type="hidden" name="approval_part_id" id="approval_part_id" value="{$partId}" />
	<input type="hidden" name="approval_step_id" id="approval_step_id" value="{$stepId}" />
	
	<div id="EditView_tabs">
		<div class="panel-content" style="margin-bottom: 30px;">
			<div class="panel panel-default">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_CRITERIAS}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<table id="criteriaTable" style="border-collapse: collapse; display: none;">
									<thead>
										<tr>
											<th>{$mod.LBL_ID}</th>
											<th>{$mod.LBL_MODULE}</th>
											<th>{$mod.LBL_FIELD}</th>
											<th>{$mod.LBL_OPERATOR}</th>
											<th>{$mod.LBL_VALUE}</th>
											<th>{$mod.LBL_REMOVE}</th>
										</tr>
									</thead>
									<tbody id="criteriaHolder">
										
									</tbody>
								</table>
								<input type="button" value="{$mod.LBL_NEW_CRITERIA}" onclick="javascript:addCriteriaLine();" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-content" style="margin-bottom: 30px;">
			<div class="panel panel-default">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_FORMULA}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<input type="text" value="{$formula}" id="formula" name="formula" style="width: 100%;" /><br/>
								<div style="margin-top: 10px; font-weight: bold;">{$mod.LBL_EXAMPLE_FORMULA}: {literal}({C0} OR {C1}) AND {C2}</div>{/literal}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{if $stepId}
		<div class="panel-content">
			<div class="panel panel-default">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_IF_CRITERIA_NOT_MET}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<select id="behaviour" name="behaviour">
									{html_options options=$behaviour_options selected=$behaviour}
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
		{/if}
	</div>
	
	<input type="submit" value="{$mod.LBL_SAVE}" style="margin-top: 20px;" />
	<input type="button" value="{$mod.LBL_CANCEL}" style="margin-left: 10px;" onclick='window.location.href="index.php?module=dtbc_Approvals&action=DetailView&record={$beanId}"' />
</form>

{literal}
<script>

var currentMax = 0;
var flow_rel_modules = new Array();
var flow_fields = new Array();

function recalculateIds() {
	var $rows = $("#criteriaHolder > tr");
	
	for (var i = 0; i < $rows.length; i++) {
		$($rows[i]).find('td.idTd').html("{C" + i + "}");
	}
	
	if ($rows.length == 0) {
		$('#criteriaTable').hide();
	} else {
		$('#criteriaTable').show();
	}
}

function addCriteriaLine(path, field, operator, value) {
	var $table = $("#criteriaHolder");
	
	field = field == undefined ? "" : field;
	operator = operator == undefined ? "" : operator;
	value = value == undefined ? "" : value;

	var $row = $("<tr line='" + currentMax + "'>");
	
	var $idTd = $("<td class='idTd'></td>");	
	
	var $pathSelect = $("<select name='path[" + currentMax + "]' id='path[" + currentMax + "]'>" + flow_rel_modules + "</select>");
	var $pathTd = $("<td class='path'></td>");
	$pathTd.append($pathSelect);
	
	var $fieldSelect = $("<select name='field[" + currentMax + "]' id='field[" + currentMax + "]'></select>");
	var $fieldTd = $("<td class='field'></td>");
	$fieldTd.append($fieldSelect);
	
	var $operatorTd = $("<td name='operator[" + currentMax + "]' id='operator[" + currentMax + "]' class='operator'></td>");
	var $valueTd = $("<td name='value[" + currentMax + "]' id='value[" + currentMax + "]' class='value'></td>");
	var $removeTd = $("<td><input type='button' value='{/literal}{$mod.LBL_REMOVE}{literal}' onclick='javascript:removeRow(this);' /></td>");
	
	$row.append($idTd);
	$row.append($pathTd);
	$row.append($fieldTd);
	$row.append($operatorTd);
	$row.append($valueTd);
	$row.append($removeTd);
	
	$table.append($row);
	
	currentMax++;
	
	recalculateIds();
	
	if (path != undefined) {
		$pathSelect.val(path);
		onPathChange($pathSelect, path, field, operator, value);
	}
		
	$fieldSelect.change(function() {
		onFieldChange(this);
	});
	
	$pathSelect.change(function() {
		onPathChange(this);
	});
	
	if (path == undefined)
		onPathChange($pathSelect);
}

function onPathChange(elem, pathValue, fieldValue, operatorValue, valueValue) {
	var $elem = $(elem);
	var $row = $elem.closest('tr');
	var $fieldTd = $row.find('td.field');
	
	var value = $elem.val();
	var line = $row.attr("line");
	
	var callback = {
		success: function(result) {
			var $field = $fieldTd.find('select');
			$field.html(result.responseText);
			
			if (fieldValue != undefined)
				onFieldChange($field, fieldValue, operatorValue, valueValue)
		}
	}

	YAHOO.util.Connect.asyncRequest ("GET", "index.php?module=AOW_WorkFlow&action=customGetModuleFields&view=EditView&aow_module={/literal}{$target_module}{literal}&rel_field="+value+"&aow_value="+pathValue,callback);
}

function onFieldChange(elem, fieldValue, operatorValue, valueValue) {
	var $elem = $(elem);
	var $row = $elem.closest('tr');
	var $operatorTd = $row.find('td.operator');
	var $valueTd = $row.find('td.value');
	var $pathTd = $row.find('td.path');

	var fieldTypeCallback = {
		success: function(result) {
			$valueTd.get(0).innerHTML = result.responseText;
			SUGAR.util.evalScript(result.responseText);
			//enableQS(true);
		},
		failure: function(result) {
			$valueTd.get(0).innerHTML = '';
		}
	}
	
	var operatorCallback = {
		success: function(result) {
			$operatorTd.get(0).innerHTML = result.responseText;
			SUGAR.util.evalScript(result.responseText);
			//enableQS(true);
		},
		failure: function(result) {
			$operatorTd.get(0).innerHTML = '';
		}
	}
	
	if (fieldValue != undefined)
		$elem.val(fieldValue);
	
	var path = $pathTd.find('select').val();
	var value = $elem.val();
	var line = $row.attr("line");

	var fieldOperatorUrl = "index.php?module=AOW_WorkFlow&action=getModuleOperatorField&view=EditView&aow_module={/literal}{$target_module}{literal}&aow_fieldname=" + value + "&aow_newfieldname=operator[" + line + "]&rel_field=" + path;
	var fieldTypeUrl = "index.php?module=AOW_WorkFlow&action=customGetModuleFieldType&view=EditView&aow_module={/literal}{$target_module}{literal}&aow_fieldname=" + value + "&aow_newfieldname=value[" + line + "]&rel_field=" + path;
	
	if (operatorValue != undefined)
		fieldOperatorUrl += "&aow_value=" + operatorValue;
		
	if (valueValue != undefined)
		fieldTypeUrl += "&aow_value=" + valueValue;
	
	YAHOO.util.Connect.asyncRequest ("GET", fieldOperatorUrl, operatorCallback);
	YAHOO.util.Connect.asyncRequest ("GET", fieldTypeUrl, fieldTypeCallback);
}

function removeRow(elem) {
	if (confirm("{/literal}{$mod.LBL_REALLY_DELETE}{literal}") == true) {
		$(elem).closest('tr').remove();
		recalculateIds();
	}
}

$(function() {
	var pathsForModuleCallback = {
		success: function(result) {
			flow_rel_modules = result.responseText;
			
			{/literal}
			{if $criteriaLines}
				{foreach from=$criteriaLines item=criteriaLine}
					addCriteriaLine("{$criteriaLine.path}", "{$criteriaLine.field}", "{$criteriaLine.operator}", "{$criteriaLine.value}");
				{/foreach}
			{else}
				addCriteriaLine();
			{/if}
			{literal}
			
			recalculateIds();
		}
	}

    YAHOO.util.Connect.asyncRequest ("GET", "index.php?module=AOW_WorkFlow&action=getModuleRelationships&aow_module={/literal}{$target_module}{literal}", pathsForModuleCallback);
});

</script>
{/literal}
