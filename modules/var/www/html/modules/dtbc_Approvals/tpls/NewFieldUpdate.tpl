<div class="moduleTitle" style="margin-bottom: 30px;">
	<h2 class="module-title-text">{$mod.LBL_FIELD_UPDATE}</h2>
	<div class="clear"></div>
</div>

<form action="index.php" method="POST" name="EditView" id="EditView">
	<input type="hidden" name="module" id="module" value="dtbc_Approvals" />
	<input type="hidden" name="action" id="action" value="save_field_update" />
	<input type="hidden" name="action_id" id="action_id" value="{$actionId}" />
	<input type="hidden" name="approval_id" id="approval_id" value="{$beanId}" />
	<input type="hidden" name="approval_part_id" id="approval_part_id" value="{$partId}" />
	<input type="hidden" name="approval_step_id" id="approval_step_id" value="{$stepId}" />
	
	<div id="EditView_tabs">
		<div class="panel-content">
			<div class="panel panel-default">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_BASIC}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_NAME}:<span class="required">*</span></div>
								<div class="col-xs-12 col-sm-8 edit-view-field" type="name">
									<input type="text" name="name" id="name" size="30" maxlength="255" value="{$name}" title="" />
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_DESCRIPTION}:</div>
								<div class="col-xs-12 col-sm-8 edit-view-field " type="text" colspan="3">
									<textarea id="description" name="description" rows="6" cols="80" title="" tabindex="">{$description}</textarea>
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_MODULE}:</div>
								<div class="col-xs-12 col-sm-2 label" style="font-weight: normal; padding-left: 0;">{$target_module}</span></div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_FIELD}:<span class="required">*</span></div>
								<div class="col-xs-12 col-sm-8 edit-view-field" type="name" field="name">
									<select name="field" id="field" onchange="javascript:onFieldChange(this, false);">
										{$fields}
									</select>
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_VALUE}:</div>
								<div class="col-xs-12 col-sm-8 edit-view-field" id="valueHolder">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<input type="submit" value="{$mod.LBL_SAVE}" style="margin-top: 20px;" />
	<input type="button" value="{$mod.LBL_CANCEL}" style="margin-left: 10px;" onclick='window.location.href="index.php?module=dtbc_Approvals&action=DetailView&record={$beanId}"' />
</form>

{literal}
<script>
	function onFieldChange(elem, initial) {
		var value = $(elem).val();

		var callback = {
			success: function(result) {
				document.getElementById('valueHolder').innerHTML = result.responseText;
				SUGAR.util.evalScript(result.responseText);
				enableQS(true);
			},
			failure: function(result) {
				document.getElementById('valueHolder').innerHTML = '';
			}
		}

		var url = "index.php?module=AOW_WorkFlow&action=getModuleFieldType&view=EditView&aow_module={/literal}{$target_module}{literal}&aow_fieldname=" + value + "&aow_newfieldname=value&rel_field={/literal}{$target_module}{literal}";

		if (initial === true)
			url += "&aow_value={/literal}{$value}{literal}";
		
		YAHOO.util.Connect.asyncRequest ("GET", url, callback);
	}
	
	$(function() {
		$("#field").val('{/literal}{$field}{literal}');
		onFieldChange($("#field"), true);
	});
</script>
{/literal}



























