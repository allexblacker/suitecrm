<br/>

<ul class='noBullet' id='subpanel_list'>
	{foreach item=section from=$sections}
	<li class='noBullet' id='{$section.id}' style='margin-bottom: 20px;'>
		<div class='panel panel-default sub-panel'>
			<div class='panel-heading panel-heading-collapse'>
				<div style='border-rad'>
					<div style='padding-left: 10px; float: left;'>{$section.name}</div>
					{if $section.id eq 'entry'}
						<div style='padding-right: 10px; float: right;'>
							{if $section.rows}
							{else}
								<input type='button' value='{$mod.LBL_SET_CRITERIA}' onclick='window.location.href="index.php?module=dtbc_Approvals&action=new_criteria&approval_id={$beanId}&approval_part_id={$section.id}"' />
							{/if}
						</div>
					{elseif $section.id eq 'step'}
						<div style='padding-right: 10px; float: right;'>
							<input type='button' value='{$mod.LBL_NEW_STEP}' onclick='window.location.href="index.php?module=dtbc_Approvals&action=new_step&approval_id={$beanId}&approval_part_id={$section.id}"' />
						</div>
					{else}
						<div style='padding-right: 10px; float: right;'>
							<input type='button' value='{$mod.LBL_NEW_FIELD_UPDATE}' onclick='window.location.href="index.php?module=dtbc_Approvals&action=new_field_update&approval_id={$beanId}&approval_part_id={$section.id}"' />
						</div>
						<div style='padding-right: 10px; float: right;'>
							<input type='submit' value='{$mod.LBL_NEW_EMAIL_ALERT}' onclick='window.location.href="index.php?module=dtbc_Approvals&action=new_email_alert&approval_id={$beanId}&approval_part_id={$section.id}"' />
						</div>
					{/if}
					<div style='clear: both;'>&nbsp;</div>
				</div>
			</div>
			<div class='panel-body panel-collapse collapse in' id='subpanel_contacts' aria-expanded='true' style=''>
				<div class='tab-content'>
					{if $section.rows}
					<table cellpadding='0' cellspacing='0' border='0' class='list view table-responsive subpanel-table footable footable-24 breakpoint-lg' style='display: table;'>
						<thead>
							<tr class='footable-header'>
								{foreach item=header from=$section.headers}
								<th class='footable-last-visible' style='display: table-cell;'>
									{$header}
								</th>
								{/foreach}
							</tr>
						</thead>
						<tbody>
							{foreach item=row from=$section.rows}
								{if $smarty.foreach.foo.index is even} 
									<tr class='evenListRowS1'>
								{else} 
									<tr class='oddListRowS1'>
								{/if}
								
								{foreach item=cell from=$row}
									<td style='display: table-cell;'>
										{$cell}
									</td>
								{/foreach}
								
								</tr>
							{/foreach}
						</tbody>
					</table>
					{else}
					<div style="padding-left: 25px; padding-top: 25px;">
						{if $section.id eq 'entry'}
							{$mod.LBL_NO_ENTRY_CRITERIAS}
						{elseif $section.id eq 'step'}
							{$mod.LBL_NO_STEPS}
						{else}
							{$mod.LBL_NO_ACTIONS}
						{/if}
					</div>
					{/if}
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
