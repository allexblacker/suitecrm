{literal}
<style>
#recipientsTable td {
	padding: 0px 20px;
}

#recipientsTable th {
	padding: 6px 10px;
	text-align: center;
}
</style>
{/literal}

<div class="moduleTitle" style="margin-bottom: 30px;">
	<h2 class="module-title-text">{$mod.LBL_EMAIL_ALERT}</h2>
	<div class="clear"></div>
</div>

<form action="index.php" method="POST" name="EditView" id="EditView">
	<input type="hidden" name="module" id="module" value="dtbc_Approvals" />
	<input type="hidden" name="action" id="action" value="save_email_alert" />
	<input type="hidden" name="action_id" id="action_id" value="{$actionId}" />
	<input type="hidden" name="approval_id" id="approval_id" value="{$beanId}" />
	<input type="hidden" name="approval_part_id" id="approval_part_id" value="{$partId}" />
	<input type="hidden" name="approval_step_id" id="approval_step_id" value="{$stepId}" />
	
	<div id="EditView_tabs">
		<div class="panel-content">
			<div class="panel panel-default">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_BASIC}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_NAME}:<span class="required">*</span></div>
								<div class="col-xs-12 col-sm-8 edit-view-field" type="name">
									<input type="text" name="name" id="name" size="30" maxlength="255" value="{$name}" title="" />
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_DESCRIPTION}:</div>
								<div class="col-xs-12 col-sm-8 edit-view-field " type="text" colspan="3">
									<textarea id="description" name="description" rows="6" cols="80" title="" tabindex="">{$description}</textarea>
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_MODULE}:</div>
								<div class="col-xs-12 col-sm-2 label" style="font-weight: normal; padding-left: 0;">{$target_module}</span></div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_EMAIL_TEMPLATE}:<span class="required">*</span></div>
								<div class="col-xs-12 col-sm-8 edit-view-field" type="name" field="name">
									<select name="template" id="template">
										{html_options options=$templates selected=$template}
									</select>
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_RECIPIENTS}:</div>
								<div class="col-xs-12 col-sm-8">
									<select name="type" id="type" onchange="javascript:onTypeChange(this);" style="float: left; margin-right: 20px; line-height: 40px; min-height: 40px; margin-top: 10px;">
										{html_options options=$types}
									</select> 
									<div id="valueHolder" style="float: left;"></div>
									<div id="buttonHolder" style="float: left; margin-top: 10px; margin-left: 10px;">
										<input type='button' value='{$mod.LBL_ADD}' onclick='javascript:setupAndAddRecipientLine()' id='addButton' />
									</div>
									<div style="clear: both;"></div>
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label"></div>
								<div class="col-xs-12 col-sm-8">
									<table id="recipientsTable" style="border-collapse: collapse;">
										<thead>
											<tr>
												<th>{$mod.LBL_TYPE}</th>
												<th>{$mod.LBL_VALUE}</th>
												<th>{$mod.LBL_REMOVE}</th>
											</tr>
										</thead>
										<tbody id="recipientsHolder">
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<input type="submit" value="{$mod.LBL_SAVE}" style="margin-top: 20px;" />
	<input type="button" value="{$mod.LBL_CANCEL}" style="margin-left: 10px;" onclick='window.location.href="index.php?module=dtbc_Approvals&action=DetailView&record={$beanId}"' />
</form>

{literal}
<script>

var currentMax = 0;

function onTypeChange(elem) {
	var value = $(elem).val();
	
	$("#valueHolder").hide();
	$("#valueHolder").css("margin-top", value == "email" ? "8px": "0px");
	
	if (value == "creator" || value == "assigned") {
		return;
	}
	
	$("#addButton").hide();

	var callback = {
		success: function(result) {
			document.getElementById('valueHolder').innerHTML = result.responseText;
			SUGAR.util.evalScript(result.responseText);
			enableQS(true);
			
			$("#valueHolder").show();
			$("#addButton").show();
		},
		failure: function(result) {
			document.getElementById('valueHolder').innerHTML = '';
		}
	}
	
	var field = value == "email" ? "name" : "assigned_user_name";

	var url = "index.php?module=AOW_WorkFlow&action=getModuleFieldType&view=EditView&aow_module=dtbc_Approvals&aow_fieldname=" + field + "&aow_newfieldname=data&rel_field=dtbc_Approvals";
	
	YAHOO.util.Connect.asyncRequest ("GET", url, callback);
};

function removeRow(elem) {
	if (confirm("{/literal}{$mod.LBL_REALLY_DELETE}{literal}") == true) {
		$(elem).closest('tr').remove();
	}
}

function addRecipientLine(type, value, displayValue) {
	var $table = $("#recipientsHolder");
	
	var $typeHiddenInput = $("<input type='hidden' id='emailType[" + currentMax + "]' name='emailType[" + currentMax + "]' value='" + type + "'>");
	var $valueHiddenInput = $("<input type='hidden' id='emailValue[" + currentMax + "]' name='emailValue[" + currentMax + "]' value='" + value + "'>");
	
	var $row = $("<tr>");
	var $typeTd = $("<td>" + $('#type > option[value=' + type + ']').text() + "</td>");
	var $valueTd = $("<td>" + displayValue + "</td>");
	var $removeTd = $("<td><input type='button' value='{/literal}{$mod.LBL_REMOVE}{literal}' onclick='javascript:removeRow(this);' /></td>");
	
	$row.append($typeTd);
	$row.append($valueTd);
	$row.append($removeTd);
	$row.append($typeHiddenInput);
	$row.append($valueHiddenInput);
	
	$table.append($row);
	
	currentMax++;
}

function setupAndAddRecipientLine() {
	var type = $("#type").val();
	
	var value = "";
	var displayValue = "";
	switch (type) {
		case "user":
			value = $("#data").val();
			displayValue = $("#data_display").val();
			break;
		case "email":
			value = displayValue = $("#data").val();
			break;
		default:
			value = displayValue = "-";
			break;
	}

	addRecipientLine(type, value, displayValue);
};

$(function(){
	{/literal}
	
	{foreach from=$recipients item=recipient}
		addRecipientLine("{$recipient.type}", "{$recipient.value}", "{$recipient.displayValue}");
	{/foreach}
	
	{literal}
});

</script>
{/literal}

