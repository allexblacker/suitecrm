<?php

$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung:';
$mod_strings['LBL_GROUP_NAME'] = 'Gruppenname:';
$mod_strings['LBL_LIST_TITLE'] = 'Gruppen';
$mod_strings['LBL_MODULE_NAME'] = 'Gruppen';
$mod_strings['LBL_TEAM'] = 'Team:';
$mod_strings['LNK_ALL_GROUPS'] = 'Alle Gruppen';
$mod_strings['LNK_CONVERT_USER'] = 'Benutzer in Gruppe konvertieren';
$mod_strings['LNK_NEW_GROUP'] = 'Gruppe erstellen';
