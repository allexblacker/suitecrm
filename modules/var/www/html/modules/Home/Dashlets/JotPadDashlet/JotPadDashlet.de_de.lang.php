<?php

$dashletStrings['JotPadDashlet']['LBL_CONFIGURE_HEIGHT'] = 'Höhe (1 - 300)';
$dashletStrings['JotPadDashlet']['LBL_CONFIGURE_TITLE'] = 'Titel';
$dashletStrings['JotPadDashlet']['LBL_DBLCLICK_HELP'] = 'Doppelklicken Sie unten, um zu bearbeiten.';
$dashletStrings['JotPadDashlet']['LBL_DEFAULT_TEXT'] = '';
$dashletStrings['JotPadDashlet']['LBL_DESCRIPTION'] = 'Ein Dashlet für Ihre Notizen';
$dashletStrings['JotPadDashlet']['LBL_SAVED'] = 'Gespeichert.';
$dashletStrings['JotPadDashlet']['LBL_SAVING'] = 'Speichere JotPad ...';
$dashletStrings['JotPadDashlet']['LBL_TITLE'] = 'JotPad';
