<?php

$dashletStrings['InvadersDashlet']['LBL_DBLCLICK_HELP'] = 'A und D zum Bewegen, S um zu feuern.';
$dashletStrings['InvadersDashlet']['LBL_DESCRIPTION'] = 'Ein bisschen was zum Spielen';
$dashletStrings['InvadersDashlet']['LBL_GAME_OVER'] = 'Game Over<br>(Klicken, um nochmal zu spielen)';
$dashletStrings['InvadersDashlet']['LBL_START'] = 'Klicken, um zu beginnen';
$dashletStrings['InvadersDashlet']['LBL_TITLE'] = 'Eindringlinge!';
