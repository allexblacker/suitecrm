<?php

$mod_strings['LBL_ADDING'] = 'Hinzufügen für';
$mod_strings['LBL_ALLOW_ALL'] = 'Alle';
$mod_strings['LBL_ALLOW_NONE'] = 'Kein(e)';
$mod_strings['LBL_ALLOW_OWNER'] = 'Besitzer';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_NO_ACCESS'] = 'Sie haben keine Berechtigung, $module zu erstellen';
$mod_strings['LBL_REDIRECT_TO_HOME'] = 'Weiterleitung zur Startseite in';
$mod_strings['LBL_ROLE'] = 'Rolle';
$mod_strings['LBL_ROLES_SUBPANEL_TITLE'] = 'Benutzerrollen';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Suchen';
$mod_strings['LBL_SECONDS'] = 'Sekunden';
$mod_strings['LBL_USERS_SUBPANEL_TITLE'] = 'Benutzer';
$mod_strings['LIST_ROLES'] = 'Liste Rollen';
$mod_strings['LIST_ROLES_BY_USER'] = 'Rollen nach Benutzern auflisten';
