<?php

$mod_strings['LBL_ASSIGNED_TO_ID'] = 'Zugewiesene Benutzer ID';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Benutzer';
$mod_strings['LBL_CONSKEY'] = 'Nachfrager Schlüssel';
$mod_strings['LBL_CONSSECRET'] = 'Nachfrager Geheimnis';
$mod_strings['LBL_CREATED'] = 'Erstellt von';
$mod_strings['LBL_CREATED_ID'] = 'Erstellt von Id';
$mod_strings['LBL_CREATED_USER'] = 'Erstellt von Benutzer';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'OAuth Schlüssel';
$mod_strings['LBL_LIST_NAME'] = 'Schlüsselname';
$mod_strings['LBL_MODIFIED'] = 'Geändert von';
$mod_strings['LBL_MODIFIED_ID'] = 'Geändert von Id';
$mod_strings['LBL_MODIFIED_NAME'] = 'Geändert von Name';
$mod_strings['LBL_MODIFIED_USER'] = 'Geändert von Benutzer';
$mod_strings['LBL_MODULE_NAME'] = 'OAuth Schlüssel';
$mod_strings['LBL_MODULE_TITLE'] = 'OAuth Schlüssel';
$mod_strings['LBL_NAME'] = ' Schlüsselname des Verbrauchers';
$mod_strings['LBL_TOKENS'] = 'Token';
$mod_strings['LNK_LIST'] = 'OAuth Schlüssel anzeigen';
$mod_strings['LNK_NEW_RECORD'] = 'OAuth Schlüssel erstellen';
