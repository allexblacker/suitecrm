<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
    'LBL_ASSIGNED_TO_ID' => 'Toegewezen gebruiker-ID',
    'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => 'Datum creatie',
    'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
    'LBL_MODIFIED' => 'Gewijzigd door',
    'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
    'LBL_MODIFIED_NAME' => 'Gewijzigd door naam',
    'LBL_CREATED' => 'Aangemaakt door',
    'LBL_CREATED_ID' => 'Gemaakt door ID',
    'LBL_DESCRIPTION' => 'Toelichting',
    'LBL_DELETED' => 'Verwijderd',
    'LBL_NAME' => 'Naam',
    'LBL_NUMBER' => 'Num', //PR 3296
    'LBL_CREATED_USER' => 'Aangemaakt door gebruiker',
    'LBL_MODIFIED_USER' => 'Gewijzigd door gebruiker',
    'LBL_LIST_FORM_TITLE' => 'Producten offertes lijst',
    'LBL_MODULE_NAME' => 'Producten aanbiedingen',
    'LBL_MODULE_TITLE' => 'Product offertes: Home',
    'LBL_HOMEPAGE_TITLE' => 'Mijn offertes',
    'LNK_NEW_RECORD' => 'Maak offerte',
    'LNK_LIST' => 'Product offertes',
    'LBL_SEARCH_FORM_TITLE' => 'Zoek product offertes',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'Bekijk historie',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activiteiten',
    'LBL_AOS_PRODUCTS_SUBPANEL_TITLE' => 'Product offertes',
    'LBL_NEW_FORM_TITLE' => 'Nieuwe Product offerte',
    'LBL_PRODUCT_NAME' => 'Naam',
    'LBL_PRODUCT_NUMBER' => 'Num', //PR 3296
    'LBL_PRODUCT_QTY' => 'Hoeveelheid',
    'LBL_PRODUCT_COST_PRICE' => 'Kostprijs',
    'LBL_PRODUCT_LIST_PRICE' => 'Adviesprijs',
    'LBL_PRODUCT_UNIT_PRICE' => 'Prijs per eenheid',
    'LBL_PRODUCT_DISCOUNT' => 'Korting',
    'LBL_PRODUCT_DISCOUNT_AMOUNT' => 'Kortingsbedrag',
    'LBL_PART_NUMBER' => 'Onderdeel Nummer',
    'LBL_PRODUCT_DESCRIPTION' => 'Omschrijving',
    'LBL_DISCOUNT' => 'Type korting',
    'LBL_VAT_AMT' => 'BTW-bedrag',
    'LBL_VAT' => 'BTW',
    'LBL_PRODUCT_TOTAL_PRICE' => 'Totale prijs',
    'LBL_PRODUCT_NOTE' => 'Toelichting',
    'Quote' => '',
    'LBL_AOS_PRODUCTS_QUOTES_SUBPANEL_TITLE' => 'Product offertes',
    'LBL_FLEX_RELATE' => 'Gerelateerd aan',
    'LBL_PRODUCT' => 'Producten',

    'LBL_SERVICE_MODULE_NAME' => 'Diensten',
    'LBL_SERVICE_NUMBER' => 'Num', //PR 3296
    'LBL_LIST_NUM' => 'Num',
    'LBL_PARENT_ID' => 'Hoofd ID',
    'LBL_GROUP_NAME' => 'Groep',
	'LBL_GROUP_DESCRIPTION' => 'Omschrijving', //PR 3296
    'LBL_PRODUCT_COST_PRICE_USDOLLAR' => 'Kostprijs (standaard valuta)',
    'LBL_PRODUCT_LIST_PRICE_USDOLLAR' => 'Advies prijs (standaard valuta)',
    'LBL_PRODUCT_UNIT_PRICE_USDOLLAR' => 'Eenheidsprijs (standaard valuta)',
    'LBL_PRODUCT_TOTAL_PRICE_USDOLLAR' => 'Totaal prijs (standaard valuta)',
    'LBL_PRODUCT_DISCOUNT_USDOLLAR' => 'Korting (Standaard valuta)',
    'LBL_PRODUCT_DISCOUNT_AMOUNT_USDOLLAR' => 'Kortingsbedrag (standaard valuta)',
    'LBL_VAT_AMT_USDOLLAR' => 'BTW bedrag (standaard valuta)',
    'LBL_PRODUCTS_SERVICES' => 'Product / Dienst',
    'LBL_CONTRACT_ACCOUNT' => 'Contracten',
    'LBL_PRODUCT_ID' => 'Artikel-ID',

    'LBL_AOS_CONTRACTS' => 'Contracten',
    'LBL_AOS_INVOICES' => 'Facturen',
    'LBL_AOS_PRODUCTS' => 'Producten',
    'LBL_AOS_QUOTES' => 'Offertes',
);
?>
