<?php
$module_name = 'AP1_Alternative_Part';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'alternative_part_name',
            'label' => 'Name',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'bin',
            'label' => 'Bin',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'priority',
            'label' => 'Priority',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'sn1_serial_number_ap1_alternative_part_name',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'stock_level',
            'label' => 'Stock_Level',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'warehouse',
            'label' => 'Warehouse',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
