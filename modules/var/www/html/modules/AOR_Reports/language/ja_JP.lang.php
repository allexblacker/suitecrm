<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_ASSIGNED_TO_ID' => '担当ユーザID',
    'LBL_ASSIGNED_TO_NAME' => '担当ユーザ',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => '作成日',
    'LBL_DATE_MODIFIED' => '更新日',
    'LBL_MODIFIED' => '更新者',
    'LBL_MODIFIED_ID' => '更新ID',
    'LBL_MODIFIED_NAME' => '更新者',
    'LBL_CREATED' => '作成者',
    'LBL_CREATED_ID' => '生成ID',
    'LBL_DESCRIPTION' => '詳細',
    'LBL_DELETED' => '削除済み',
    'LBL_NAME' => '名前',
    'LBL_CREATED_USER' => '生成ユーザ',
    'LBL_MODIFIED_USER' => '更新ユーザ',
    'LBL_LIST_NAME' => '名前',
    'LBL_EDIT_BUTTON' => '編集',
    'LBL_REMOVE' => '削除',
    'LBL_LIST_FORM_TITLE' => 'Reports List',
    'LBL_MODULE_NAME' => 'レポート',
    'LBL_MODULE_TITLE' => 'レポート',
    'LBL_HOMEPAGE_TITLE' => 'My Reports',
    'LNK_NEW_RECORD' => 'Create Report',
    'LNK_LIST' => 'View Reports',
    'LNK_IMPORT_AOR_REPORTS' => 'Import Reports',
    'LBL_SEARCH_FORM_TITLE' => 'Search Reports',
    'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動',
    'LBL_AOR_REPORTS_SUBPANEL_TITLE' => 'レポート',
    'LBL_NEW_FORM_TITLE' => 'New Reports',
    'LBL_REPORT_MODULE' => 'Report Module',
    'LBL_GRAPHS_PER_ROW' => 'Charts per row',
    'LBL_FIELD_LINES' => 'Display Fields',
    'LBL_ADD_FIELD' => 'フィールドの追加',
    'LBL_CONDITION_LINES' => 'Conditions',
    'LBL_ADD_CONDITION' => 'Add Condition',
    'LBL_EXPORT' => 'エクスポート',
    'LBL_DOWNLOAD_PDF' => 'Download PDF',
    'LBL_ADD_TO_PROSPECT_LIST' => 'ターゲットリストに追加',
    'LBL_AOR_MODULETREE_SUBPANEL_TITLE' => 'Module tree',
    'LBL_AOR_FIELDS_SUBPANEL_TITLE' => 'フィールド',
    'LBL_AOR_CONDITIONS_SUBPANEL_TITLE' => 'Conditions',
    'LBL_TOTAL' => '合計',
    'LBL_AOR_CHARTS_SUBPANEL_TITLE' => 'チャート一覧',
    'LBL_ADD_CHART' => 'Add chart',
    'LBL_ADD_PARENTHESIS' => 'Drop parenthesis',
    'LBL_CHART_TITLE' => 'タイトル',
    'LBL_CHART_TYPE' => 'タイプ',
    'LBL_CHART_X_FIELD' => 'X Axis',
    'LBL_CHART_Y_FIELD' => 'Y Axis',
    'LBL_AOR_REPORTS_DASHLET' => 'レポート',
    'LBL_DASHLET_TITLE' => 'タイトル',
    'LBL_DASHLET_REPORT' => 'Report',
    'LBL_DASHLET_CHOOSE_REPORT' => 'Please choose a report',
    'LBL_DASHLET_SAVE' => 'セーブする',
    'LBL_DASHLET_CHARTS' => 'チャート一覧',
    'LBL_DASHLET_ONLY_CHARTS' => 'Only show charts',
    'LBL_AOR_SCHEDULED_REPORTS_AOR_REPORTS_FROM_AOR_SCHEDULED_REPORTS_TITLE' => 'スケジュール済みレポート一覧',
    'LBL_UPDATE_PARAMETERS' => '更新',
    'LBL_PARAMETERS' => 'Parameters',
    'LBL_TOOLTIP_DRAG_DROP_ELEMS' => 'Drag and drop elements into field or condition area',
    'LBL_MAIN_GROUPS' => 'Main Group:',
    'LBL_CHAR_UNNAMED_DEFAULT_TITLE' => 'Unnamed Chart',
    'LBL_REPORT' => 'Report',
);
