<?php

$mod_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Aktivitäten';
$mod_strings['LBL_ASSIGNED_TO_ID'] = 'Zugewiesene Benutzer ID';
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Zugewiesen an';
$mod_strings['LBL_Alert_SUBPANEL_TITLE'] = 'Warnung';
$mod_strings['LBL_CREATED'] = 'Erstellt von';
$mod_strings['LBL_CREATED_ID'] = 'Erstellt von ID';
$mod_strings['LBL_CREATED_USER'] = 'Erstellt von Benutzer';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_EDIT_BUTTON'] = 'Bearbeiten';
$mod_strings['LBL_HISTORY_SUBPANEL_TITLE'] = 'Verlauf ansehen';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Meine Warnungen';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_IS_READ'] = 'gelesen';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Warnliste';
$mod_strings['LBL_LIST_NAME'] = 'Name';
$mod_strings['LBL_MODIFIED'] = 'Geändert von';
$mod_strings['LBL_MODIFIED_ID'] = 'Geändert von ID';
$mod_strings['LBL_MODIFIED_NAME'] = 'Geändert von Name';
$mod_strings['LBL_MODIFIED_USER'] = 'Geändert von Benutzer';
$mod_strings['LBL_MODULE_NAME'] = 'Warnung';
$mod_strings['LBL_MODULE_TITLE'] = 'Warnung';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Neue Warnung';
$mod_strings['LBL_REMOVE'] = 'Entfernen';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Warnungen suchen';
$mod_strings['LBL_TYPE'] = 'Typ';
$mod_strings['LNK_IMPORT_Alert'] = 'Warnungen importieren';
$mod_strings['LNK_LIST'] = 'Warnungen ansehen';
$mod_strings['LNK_NEW_RECORD'] = 'Warnung erstellen';
