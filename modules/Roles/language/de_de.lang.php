<?php

$mod_strings['LBL_ALLOWED_MODULES'] = 'Zulässige Module: ';
$mod_strings['LBL_ASSIGN_MODULES'] = 'Module bearbeiten:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Rollen';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung: ';
$mod_strings['LBL_DISALLOWED_MODULES'] = 'Gesperrte Module:';
$mod_strings['LBL_LANGUAGE'] = 'Sprache: ';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Rollenliste';
$mod_strings['LBL_MODULES'] = 'Module';
$mod_strings['LBL_MODULE_NAME'] = 'Rollen';
$mod_strings['LBL_MODULE_TITLE'] = 'Rollen: Startseite';
$mod_strings['LBL_NAME'] = 'Name: ';
$mod_strings['LBL_ROLE'] = 'Rolle: ';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Rollensuche';
$mod_strings['LBL_USERS'] = 'Benutzer';
$mod_strings['LBL_USERS_SUBPANEL_TITLE'] = 'Benutzer';
$mod_strings['LNK_NEW_ROLE'] = 'Neue Rolle';
$mod_strings['LNK_ROLES'] = 'Rollen';
