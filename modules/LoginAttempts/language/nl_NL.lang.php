<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 * New module PR 2953
 */

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen gebruiker-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_SECURITYGROUPS' => 'Security Groepen',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groepen',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Aanmaakdatum',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door naam',
  'LBL_CREATED' => 'Aangemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DESCRIPTION' => 'Omschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Aangemaakt door gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Bewerken',
  'LBL_REMOVE' => 'Verwijderen',
  'LBL_LIST_FORM_TITLE' => 'Lijst met login pogingen',
  'LBL_MODULE_NAME' => 'Login pogingen',
  'LBL_MODULE_TITLE' => 'Login pogingen',
  'LBL_HOMEPAGE_TITLE' => 'Mijn login pogingen',
  'LNK_NEW_RECORD' => 'Nieuwe login pogingen',
  'LNK_LIST' => 'Bekijk login pogingen',
  'LNK_IMPORT_LOGINATTEMPTS' => 'Importeer login pogingen',
  'LBL_SEARCH_FORM_TITLE' => 'Zoek login pogingen',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Overzicht Geschiedenis',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activiteiten',
  'LBL_LOGINATTEMPTS_SUBPANEL_TITLE' => 'Login pogingen',
  'LBL_NEW_FORM_TITLE' => 'Nieuwe login pogingen',
  'LBL_USERNAME' => 'Gebruikersnaam',
  'LBL_RECORD_USER_ID' => 'Gebruikers-ID',
  'LBL_IP_ADDRESS' => 'IP-adres',
  'LBL_SUCCESS' => 'Succesvol',
);
