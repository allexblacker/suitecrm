<?php

$mod_strings['LBL_AUDITED_FIELDS'] = 'Auditierte Felder in diesem Modul:';
$mod_strings['LBL_CHANGE_LOG'] = 'Änderungsprotokoll';
$mod_strings['LBL_CREATED_BY'] = 'Geändert von';
$mod_strings['LBL_FIELD_NAME'] = 'Feld';
$mod_strings['LBL_LIST_DATE'] = 'Änderungsdatum';
$mod_strings['LBL_NEW_VALUE'] = 'Neuer Wert';
$mod_strings['LBL_NO_AUDITED_FIELDS_TEXT'] = 'In diesem Modul sind keine überprüften Felder';
$mod_strings['LBL_OLD_NAME'] = 'Alter Wert';
