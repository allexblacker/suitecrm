<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => '担当ユーザID',
  'LBL_ASSIGNED_TO_NAME' => '担当ユーザ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '生成ID',
  'LBL_PDF_COMMENT' => 'Full PDF text', //PR 3532
  'LBL_DESCRIPTION' => '本文',
  'LBL_HEADER' => 'Header',
  'LBL_FOOTER' => 'Footer',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '生成ユーザ',
  'LBL_MODIFIED_USER' => '更新ユーザ',
  'LBL_LIST_FORM_TITLE' => 'PDF Templates List',
  'LBL_MODULE_NAME' => 'PDF テンプレート',
  'LBL_MODULE_TITLE' => 'PDF Templates: Home',
  'LBL_HOMEPAGE_TITLE' => 'My PDF Templates',
  'LNK_NEW_RECORD' => 'Create PDF Template',
  'LNK_LIST' => 'View PDF Templates',
  'LBL_SEARCH_FORM_TITLE' => 'Search PDF Templates',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動',
  'LBL_AOS_PDF_TEMPLATES_SUBPANEL_TITLE' => 'PDF テンプレート',
  'LBL_NEW_FORM_TITLE' => 'New PDF Template',
  'LBL_TYPE' => 'タイプ',
  'LBL_ACTIVE' => '有効なアカウント',
  'LBL_DEFAULT_TEMPLATE' => 'Default Template',
  'LBL_BUTTON_INSERT' => '挿入',
  'LBL_WARNING_OVERWRITE' => 'Warning this will overwrite you current Work',
  'LBL_INSERT_FIELDS' => 'Insert Fields',
  
  'LBL_SAMPLE' => 'Load Sample',
  'LBL_PAGE' => 'ページ',
  'LBL_PREPARED_FOR' => 'Prepared For',
  'LBL_PREPARED_BY' => 'Prepared By',
  'LBL_QUOTE_SAMPLE' => 'Quote Sample',
  'LBL_INVOICE_SAMPLE' => 'Invoice Sample',
  'LBL_ACCOUNT_SAMPLE' => 'Account Sample',
  'LBL_CONTACT_SAMPLE' => 'Contact Sample',
  'LBL_LEAD_SAMPLE' => 'Lead Sample',
  'LBL_ANY_STREET' => 'Any Street',
  'LBL_ANY_TOWN' => 'Any Town',
  'LBL_ANY_WHERE' => 'Any Where',
  
  'LBL_QUOTE_GROUP_SAMPLE' => 'Quote Group Sample',
  'LBL_INVOICE_GROUP_SAMPLE' => 'Invoice Group Sample',
  'LBL_MARGIN_LEFT' => 'Margin Left',
  'LBL_MARGIN_RIGHT' => 'Margin Right',
  'LBL_MARGIN_TOP' => 'Margin Top',
  'LBL_MARGIN_BOTTOM' => 'Margin Bottom',
  'LBL_MARGIN_HEADER' => 'Margin Header',
  'LBL_MARGIN_FOOTER' => 'Margin Footer',
  'LBL_EDITVIEW_PANEL1' => 'Margins',
  'LBL_DETAILVIEW_PANEL1' => 'Margins',
  'LBL_PAGE_SIZE' => 'Page Size',
  'LBL_ORIENTATION' => 'Orientation',
);
?>
