<div style='width:100%'>
<form name='configure_{$id}' action="index.php" method="post" onSubmit='return SUGAR.dashlets.postForm("configure_{$id}", SUGAR.mySugar.uncoverPage);'>
<input type='hidden' name='id' value='{$id}'>
<input type='hidden' name='module' value='Home'>
<input type='hidden' name='action' value='ConfigureDashlet'>
<input type='hidden' name='to_pdf' value='true'>
<input type='hidden' name='configure' value='true'>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="edit view" align="center">
<tr>
    <td scope='row'>{$maxRecordsLBL}</td>
    <td>
    	<select name="prospectsMaxRecords" size='1'>
            {html_options options=$prospectsMaxRecordsOptions selected=$prospectsMaxRecords}
		</select>
    </td>
</tr>
<tr>
    <td scope='row'>{$daysOfHistoryLBL}</td>
    <td>
    	<select name="prospectsDaysOfHistory" size='1'>
            {html_options options=$prospectsDaysOfHistoryOptions selected=$prospectsDaysOfHistory}
		</select>
    </td>
</tr>
<tr>
    <td scope='row'>{$minimumScoreLBL}</td>
    <td>
    	<select name="prospectsMinimumScore" size='1'>
            {html_options options=$prospectsMinimumScoreOptions selected=$prospectsMinimumScore}
		</select>
    </td>
</tr>
<tr>
    <td scope="row" colspan="2"><b>Prospect Targets</b></td>
</tr>
<tr>
    <td scope="row">{$includeLeadsLBL}</td>
    <td>
    	<input type="checkbox" name="prospectsIncludeLeads" {if $prospectsIncludeLeads}checked{/if}>
    </td>
</tr>
<tr>
    <td scope="row">{$includeOpportunitiesLBL}</td>
    <td>
    	<input type="checkbox" name="prospectsIncludeOpportunities" {if $prospectsIncludeOpportunities}checked{/if}>
    </td>
</tr>
<tr>
    <td scope="row">{$includeContactsLBL}</td>
    <td>
    	<input type="checkbox" name="prospectsIncludeContacts" {if $prospectsIncludeContacts}checked{/if}>
    </td>
</tr>
<tr>
    <td scope="row" colspan="2"><b>Behavior Types</b></td>
</tr>
<tr>
    <td scope="row">{$includeSendsLBL}</td>
    <td>
    	<input type="checkbox" name="prospectsIncludeSends" {if $prospectsIncludeSends}checked{/if}>
    </td>
</tr>
<tr>
    <td scope="row">{$includeOpensLBL}</td>
    <td>
    	<input type="checkbox" name="prospectsIncludeOpens" {if $prospectsIncludeOpens}checked{/if}>
    </td>
</tr>
<tr>
    <td align="right" colspan="2">
        <input type="submit" class="button" value="{$saveLBL}">
   	</td>
</tr>
</table>
</form>
</div>
	