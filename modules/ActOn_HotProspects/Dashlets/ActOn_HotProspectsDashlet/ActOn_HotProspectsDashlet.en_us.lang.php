<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 ********************************************************************************/

$dashletStrings['ActOn_HotProspectsDashlet'] = array('LBL_TITLE'            => 'Act-On Hot Prospects',
													 'LBL_DESCRIPTION'      => 'A dashlet to display Act-On Leads',
													 'LBL_PROSPECTS_DASHLET_TITLE' => 'My Act-On Hot Prospects',
													 'LBL_CONFIGURE_TITLE'  => 'Configuration' );
?>