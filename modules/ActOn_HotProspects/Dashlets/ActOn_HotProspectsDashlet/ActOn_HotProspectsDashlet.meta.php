<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 ********************************************************************************/

 
global $app_strings;

$dashletMeta['ActOn_HotProspectsDashlet'] = array(
                                      'title'		=> 'LBL_PROSPECTS_DASHLET_TITLE',
                                      'description' => 'LBL_PROSPECTS_DASHLET_TITLE',
                                      'icon'        => 'themes/default/images/icon_Meetings_32.gif',
                                      'category'    => 'Module Views');
