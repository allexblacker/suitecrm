<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once('include/Dashlets/Dashlet.php');

class ActOn_HotProspectsDashlet extends Dashlet
{
	var $recordCategories;
	var $historyCategories;
	var $scoreCategories;
    var $defaultURL = 'http://api.actonsoftware.com';
    var $defaultKey = 'XXXXXX';
    var $maxRecords = 20;
    var $daysOfHistory = 7;
	var $minimumScore = 10;
	var $includeSends = true;
    var $includeOpens = true;
	var $includeLeads = true;
	var $includeOpportunities = true;
	var $includeContacts = true;
	
    protected $allowed_schemes = array("http", "https");

    function ActOn_HotProspectsDashlet($id, $options = null) {
        parent::Dashlet($id);
        $this->isConfigurable = true;
        $this->hasScript = false;  // if dashlet has javascript attached to it

        if(!empty($options['titleLabel']))
		{
        	$this->title = translate($options['titleLabel'], 'ActOn_HotProspects');
        }
		else
		{
	        if(empty($options['title']))
			{
	            $this->title = translate('LBL_HOMEPAGE_TITLE', 'ActOn_HotProspects');
	        }
			else
			{
	            $this->title = $options['title'];
	        }
        }

		if(!empty($options['maxRecords']))
		{
	        $this->maxRecords = $options['maxRecords'];
		}

		if(!empty($options['daysOfHistory']))
		{
	        $this->daysOfHistory = $options['daysOfHistory'];
		}

		if(!empty($options['minimumScore']))
		{
	        $this->minimumScore = $options['minimumScore'];
		}

		//Targets
		if(isset($options['includeLeads']) && $options['includeLeads'])
		{
	        $this->includeLeads = true;
		}
		else
		{
	        $this->includeLeads = false;
		}
		if(isset($options['includeOpportunities']) && $options['includeOpportunities'])
		{
	        $this->includeOpportunities = true;
		}
		else
		{
	        $this->includeOpportunities = false;
		}
		if(isset($options['includeContacts']) && $options['includeContacts'])
		{
	        $this->includeContacts = true;
		}
		else
		{
	        $this->includeContacts = false;
		}

		//Behavior Types
		if(isset($options['includeSends']) && $options['includeSends'])
		{
	        $this->includeSends = true;
		}
		else
		{
	        $this->includeSends = false;
		}

		if(isset($options['includeOpens']) && $options['includeOpens'])
		{
	        $this->includeOpens = true;
		}
		else
		{
			$this->includeOpens = false;
		}

		$this->recordCategories = array(5 => "5", 10 => "10", 20 => "20");
		$this->historyCategories = array(1 => "Today", 7 => "One Week", 14 => "Two Weeks", 30 => "One Month", 60 => "Two Months");
		$this->scoreCategories = array(1 => "1", 2 => "2", 3 => "3", 4 => "4", 5 => "5", 6 => "6", 7 => "7", 8 => "8", 9 => "9", 10 => "10", 20 => "20", 30 => "30", 40 => "40", 50 => "50", 60 => "60", 70 => "70", 80 => "80", 90 => "90", 100 => "100");

        $this->height = 300;
        $this->autoRefresh = false;
    }

    function displayOptions()
	{
        global $app_strings;
		$ss = new Sugar_Smarty();
		$ss->assign('maxRecordsLBL', translate('LBL_PROSPECTS_DASHLET_RECORDS', 'ActOn_HotProspects'));
		$ss->assign('prospectsMaxRecordsOptions', $this->recordCategories);
		$ss->assign('prospectsMaxRecords', $this->maxRecords);
		$ss->assign('daysOfHistoryLBL', translate('LBL_PROSPECTS_DASHLET_HISTORY', 'ActOn_HotProspects'));
		$ss->assign('prospectsDaysOfHistoryOptions', $this->historyCategories);
		$ss->assign('prospectsDaysOfHistory', $this->daysOfHistory);
		$ss->assign('minimumScoreLBL', translate('LBL_PROSPECTS_DASHLET_MINSCORE', 'ActOn_HotProspects'));
		$ss->assign('prospectsMinimumScoreOptions', $this->scoreCategories);
		$ss->assign('prospectsMinimumScore', $this->minimumScore);

		//Prospect Types
		$ss->assign('includeLeadsLBL', translate('LBL_PROSPECTS_DASHLET_LEADS', 'ActOn_HotProspects'));
		$ss->assign('prospectsIncludeLeads', $this->includeLeads);
		$ss->assign('includeOpportunitiesLBL', translate('LBL_PROSPECTS_DASHLET_OPPORTUNITIES', 'ActOn_HotProspects'));
		$ss->assign('prospectsIncludeOpportunities', $this->includeOpportunities);
		$ss->assign('includeContactsLBL', translate('LBL_PROSPECTS_DASHLET_CONTACTS', 'ActOn_HotProspects'));
		$ss->assign('prospectsIncludeContacts', $this->includeContacts);

		//Behavior Types
		$ss->assign('includeSendsLBL', translate('LBL_PROSPECTS_DASHLET_SENDS', 'ActOn_HotProspects'));
		$ss->assign('prospectsIncludeSends', $this->includeSends);
		$ss->assign('includeOpensLBL', translate('LBL_PROSPECTS_DASHLET_OPENS', 'ActOn_HotProspects'));
		$ss->assign('prospectsIncludeOpens', $this->includeOpens);
		
		$ss->assign('id', $this->id);
        $ss->assign('height', $this->height);
        $ss->assign('saveLBL', $app_strings['LBL_SAVE_BUTTON_LABEL']);
        $ss->assign('clearLBL', $app_strings['LBL_CLEAR_BUTTON_LABEL']);
        return  $ss->fetch('modules/ActOn_HotProspects/Dashlets/ActOn_HotProspectsDashlet/ActOn_HotProspectsDashletOptions.tpl');
    }

    function saveOptions($req)
	{
        $options = array();
        if(isset($req['prospectsMaxRecords']))
		{
            $options['maxRecords'] = $req['prospectsMaxRecords'];
        }
        if(isset($req['prospectsDaysOfHistory']))
		{
            $options['daysOfHistory'] = $req['prospectsDaysOfHistory'];
        }
        if(isset($req['prospectsMinimumScore']))
		{
            $options['minimumScore'] = $req['prospectsMinimumScore'];
        }

		//Prospect Types
        if(isset($req['prospectsIncludeLeads']))
		{
            $options['includeLeads'] = true;
        }
		else
		{
            $options['includeLeads'] = false;
		}
        if(isset($req['prospectsIncludeOpportunities']))
		{
            $options['includeOpportunities'] = true;
        }
		else
		{
            $options['includeOpportunities'] = false;
		}
        if(isset($req['prospectsIncludeContacts']))
		{
            $options['includeContacts'] = true;
        }
		else
		{
            $options['includeContacts'] = false;
		}

		//Behavior Types
		if(isset($req['prospectsIncludeSends']))
		{
            $options['includeSends'] = true;
        }
		else
		{
            $options['includeSends'] = false;
		}
        if(isset($req['prospectsIncludeOpens']))
		{
            $options['includeOpens'] = true;
        }
		else
		{
            $options['includeOpens'] = false;
		}

		return $options;
    }

    function display()
	{
		global $current_user;
		$actonConnector = SourceFactory::getSource('ext_rest_acton');
		$actonConfig = $actonConnector->getConfig();

		if(!function_exists("curl_init"))
		{
            $GLOBALS['log']->fatal("REST call failed - no cURL!");
            return false;
        }

		// 1. Do we need to retrieve a new sessionToken?
		$sessionToken = $actonConnector->getSessionToken($actonConfig);
		if($sessionToken != null)
		{
			$url = $actonConfig['properties']['url'] . '/acton/sugar/prospectsJSON.jsp';		
			$postArgs = array('sessionToken' => urlencode($sessionToken), 'owner' => urlencode($current_user->email1), 'userId' => urlencode($current_user->id), 'username' => urlencode($current_user->user_name), 'name' => urlencode($current_user->name), 'recordCount' => $this->maxRecords, 'searchDays' => $this->daysOfHistory, 'lowerBound' => $this->minimumScore, 'includeLeads' => $this->includeLeads, 'includeOpportunities' => $this->includeOpportunities, 'includeContacts' => $this->includeContacts, 'includeSends' => $this->includeSends, 'includeOpens' => $this->includeOpens);
			$postArgsQuery = http_build_query($postArgs);

			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postArgsQuery);
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			$GLOBALS['log']->debug("HTTP client call: $url -> $postArgs");
			$response = curl_exec($curl);
			if($response === false)
			{
				$curl_errno = curl_errno($curl);
				$curl_error = curl_error($curl);
				$GLOBALS['log']->error("HTTP client: cURL call failed: error $curl_errno: $curl_error");
				return parent::display() . "Unable to communicate with server. Please check your Act-On Connector setup, or click refresh in a moment.";
			}
			$GLOBALS['log']->debug("HTTP client response: $response");
			curl_close($curl);

			if(trim($response) == "[]")
			{
				return parent::display() . "No Hot Prospects data currently available for this SugarCRM user. Consider increasing your Behavior Score window.";
			}
			else
			{
				$result = json_decode($response, true);
				if($result != NULL)
				{
					$html = '<table class="list view" width="100%" border="0"><tbody><tr><td colspan="10" align="right"></td></tr><tr height="20"><td><b>Name</b></td><td><b>Email Address</b></td><td><b>Company</b></td><td align="middle"><b>Lead Score</b></td><td><b>Last Activity</b></td></tr><tr><td colspan="10" align="right" style="border-bottom: 1px solid #C0C0C0"></td></tr>';
					foreach($result as $prospect)
					{
						$html .= '<tr>';

						$module = "Contacts";
						if($prospect['Type'] == "lead")
							$module = "Leads";
						else if($prospect['Type'] == "prospect")
							$module = "Prospects";

						if($prospect['ExternalId'] != '')
						{
							$html .= '<td scope="row">' . '<a href="index.php?action=DetailView&module=' . $module . '&record=' .  $prospect['ExternalId'] . '">'. $prospect['First Name'] . ' ' . $prospect['Last Name'] . '</a></td>';
						}
						else
						{
							$html .= '<td scope="row">' . $prospect['First Name'] . ' ' . $prospect['Last Name'] . '</td>';
						}

						$html .= '<td scope="row">' . $prospect['Email'] . '</td>';
						$html .= '<td scope="row">' . $prospect['Company'] . '</td>';
						$html .= '<td scope="row" align="middle">' . $prospect['Score'] . '</td>';
						$html .= '<td scope="row">' . date("D, M d", ($prospect['LastActivity'] / 1000)) . '</td>';
						$html .= '</tr></tbody>';
					}
					$html .= "</table>";
					return parent::display() . $html;
				}
				else
				{
					return parent::display() . "Unable to authenticate. Please check your Act-On Connector setup.";
				}
			}
		}
		else
		{
			return parent::display() . "Unable to authenticate. Please check your Act-On Connector setup.";
		}
    }
	
	/*
	function displayScript()
	{
		$ss = new Sugar_Smarty();
		$ss->assign('prospectsMaxRecords', $this->maxRecords);
		$ss->assign('prospectsDaysOfHistory', $this->daysOfHistory);
		$ss->assign('prospectsMinimumScore', $this->minimumScore);
		$ss->assign('prospectsIncludeSends', $this->includeSends);
		$ss->assign('prospectsIncludeOpens', $this->includeOpens);
        return  $ss->fetch('modules/ActOn_HotProspects/Dashlets/ActOn_HotProspectsDashlet/ActOn_HotProspectsDashletScript.tpl');
	}
	*/
}
