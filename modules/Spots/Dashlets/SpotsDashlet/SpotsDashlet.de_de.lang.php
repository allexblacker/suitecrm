<?php

$dashletStrings['SpotsDashlet']['LBL_CONFIGURE_TITLE'] = 'Bezeichnung';
$dashletStrings['SpotsDashlet']['LBL_DESCRIPTION'] = 'Spot-Report Ihrer Daten';
$dashletStrings['SpotsDashlet']['LBL_NAME'] = 'Name';
$dashletStrings['SpotsDashlet']['LBL_NO_SPOTS_SELECTED'] = 'Keine Spots für Anzeige ausgewählt';
$dashletStrings['SpotsDashlet']['LBL_SAVED'] = 'Gespeichert';
$dashletStrings['SpotsDashlet']['LBL_SAVING'] = 'Speichere Spots ...';
$dashletStrings['SpotsDashlet']['LBL_SHOW_UI'] = 'Zeige Benutzeroberfläche ';
$dashletStrings['SpotsDashlet']['LBL_SPOTS_POINTED_DELETED'] = 'Dieser Spot wurde gelöscht';
$dashletStrings['SpotsDashlet']['LBL_SPOTS_TO_LOAD'] = 'Verfügbare Spots';
$dashletStrings['SpotsDashlet']['LBL_TITLE'] = 'Spots';
