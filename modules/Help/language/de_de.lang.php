<?php

$mod_strings['ERR_DELETE_RECORD'] = 'Um diesen Datensatz zu löschen, muss eine Datensatznummer angegeben werden';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Firmenliste';
$mod_strings['LBL_MODULE_NAME'] = 'Firmen';
$mod_strings['LBL_MODULE_TITLE'] = 'Firma: Startseite';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Firma erstellen';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Firmensuche';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Firma erstellen';
$mod_strings['LNK_NEW_CALL'] = 'Anruf erstellen';
$mod_strings['LNK_NEW_CASE'] = 'Fall erstellen';
$mod_strings['LNK_NEW_CONTACT'] = 'Kontakt erstellen';
$mod_strings['LNK_NEW_EMAIL'] = 'E-Mail archivieren';
$mod_strings['LNK_NEW_MEETING'] = 'Neues Meeting';
$mod_strings['LNK_NEW_NOTE'] = 'Notiz oder Anhang erstellen';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Verkaufschance erstellen';
$mod_strings['LNK_NEW_TASK'] = 'Aufgabe erstellen';
