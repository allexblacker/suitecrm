<?php
$module_name = 'S1_Site';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'ACCOUNT_NAME' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'Account_Name',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'CURRENCY' => 
  array (
    'type' => 'varchar',
    'label' => 'Currency',
    'width' => '10%',
    'default' => false,
  ),
  'CURRENT_SOFTWARE_VERSION' => 
  array (
    'type' => 'varchar',
    'label' => 'Current_Software_Version',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DEFAULT_PANEL_MODEL' => 
  array (
    'type' => 'varchar',
    'label' => 'default_panel_model',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'ERROR_LOG' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Error_log',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'EXISTING_SITE' => 
  array (
    'type' => 'decimal',
    'label' => 'Existing_Site',
    'width' => '10%',
    'default' => false,
  ),
  'FIELD_MAGNITUDE' => 
  array (
    'type' => 'varchar',
    'label' => 'field_magnitude',
    'width' => '10%',
    'default' => false,
  ),
  'FIELD_TYPE' => 
  array (
    'type' => 'varchar',
    'label' => 'field_type',
    'width' => '10%',
    'default' => false,
  ),
  'FIRST_TELEMETRY_DATE' => 
  array (
    'type' => 'date',
    'label' => 'First_Telemetry_Date',
    'width' => '10%',
    'default' => false,
  ),
  'GUID' => 
  array (
    'type' => 'varchar',
    'label' => 'guid',
    'width' => '10%',
    'default' => false,
  ),
  'INSTALLATION_DATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'installation_date',
    'width' => '10%',
    'default' => false,
  ),
  'INSTALLER' => 
  array (
    'type' => 'varchar',
    'label' => 'Installer',
    'width' => '10%',
    'default' => false,
  ),
  'IS_PUBLIC' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'is_public',
    'width' => '10%',
  ),
  'INTERNAL_NOTES' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Internal_notes',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'LAST_REPORTING_TIME' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'Last_reporting_time',
    'width' => '10%',
    'default' => false,
  ),
  'LAST_TELEMETRY_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Last_Telemetry_Date',
    'width' => '10%',
    'default' => false,
  ),
  'LATITUDE' => 
  array (
    'type' => 'varchar',
    'label' => 'latitude',
    'width' => '10%',
    'default' => false,
  ),
  'LAYOUT_FILE_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'layout_file_name',
    'width' => '10%',
    'default' => false,
  ),
  'LOCATION_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'location_id',
    'width' => '10%',
    'default' => false,
  ),
  'LONGITUDE' => 
  array (
    'type' => 'varchar',
    'label' => 'longitude',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'MODULE_TYPE' => 
  array (
    'type' => 'varchar',
    'label' => 'Module_type',
    'width' => '10%',
    'default' => false,
  ),
  'MONITORING_ACCOUNT_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'monitoring_account_id',
    'width' => '10%',
    'default' => false,
  ),
  'MONITORING_CREATION_TIME' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'Monitoring_creation_time',
    'width' => '10%',
    'default' => false,
  ),
  'NOTES' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'notes',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'MONITORING_SITE_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'Monitoring_Site_ID',
    'width' => '10%',
    'default' => false,
  ),
  'MONITORING_SITE_LAST_UPDATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'Monitoring_site_last_update',
    'width' => '10%',
    'default' => false,
  ),
  'PANEL_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Panel_Type',
    'width' => '10%',
    'default' => false,
  ),
  'ROOF_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Roof_Type',
    'width' => '10%',
    'default' => false,
  ),
  'PEAK_POWER_KW_H_PEAK' => 
  array (
    'type' => 'decimal',
    'label' => 'Peak_power_kw_h_peak',
    'width' => '10%',
    'default' => false,
  ),
  'PHONE_NUMBER' => 
  array (
    'type' => 'phone',
    'label' => 'Phone_number',
    'width' => '10%',
    'default' => false,
  ),
  'PUBLIC_MODULES' => 
  array (
    'type' => 'varchar',
    'label' => 'public_modules',
    'width' => '10%',
    'default' => false,
  ),
  'SERVICE_INSTRUCTION' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Service_instruction',
    'width' => '10%',
  ),
  'SITE_ADDITIONAL_INFO' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Site_Additional_Info',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'SITE_ADDRESS' => 
  array (
    'type' => 'varchar',
    'label' => 'Site_Address',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_CITY' => 
  array (
    'type' => 'varchar',
    'label' => 'Site_City',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_COUNTRY' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Site_Country',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_IS_MONITORED' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Site_is_Monitored',
    'width' => '10%',
  ),
  'SITE_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'Name',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_STATE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Site_State',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_ZIP' => 
  array (
    'type' => 'varchar',
    'label' => 'Site_zip',
    'width' => '10%',
    'default' => false,
  ),
  'SOLAR_FIELD_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'solar_field_id',
    'width' => '10%',
    'default' => false,
  ),
  'SPECIAL_SERVICE_INSTRUCTION' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'special_service_instruction',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'STATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'Status',
    'width' => '10%',
    'default' => false,
  ),
  'URL_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'url_name',
    'width' => '10%',
    'default' => false,
  ),
  'WORKPLAN_REMARKS' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Workplan_Remarks',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
);
?>
