<?php

$mod_strings['AOS_PRODUCT_QUOTES'] = 'Zeilenelemente';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_DISCOUNT_AMOUNT'] = 'Rabatt';
$mod_strings['LBL_DISCOUNT_AMOUNT_USDOLLAR'] = 'Rabatt (Währung)';
$mod_strings['LBL_GROUP_TOTAL'] = 'Gesamtsumme';
$mod_strings['LBL_GROUP_TOTAL_USDOLLAR'] = 'Gruppensumme (Währung)';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_LIST_NUM'] = 'Nummer';
$mod_strings['LBL_MODULE_NAME'] = 'Gruppen';
$mod_strings['LBL_NAME'] = 'Gruppenname';
$mod_strings['LBL_PARENT_ID'] = 'Eltern-ID';
$mod_strings['LBL_SUBTOTAL_AMOUNT'] = 'Zwischensumme';
$mod_strings['LBL_SUBTOTAL_AMOUNT_USDOLLAR'] = 'Zwischensumme (Währung)';
$mod_strings['LBL_SUBTOTAL_TAX_AMOUNT'] = 'Zwischensumme + Steuer';
$mod_strings['LBL_SUBTOTAL_TAX_AMOUNT_USDOLLAR'] = 'Zwischensumme + Steuer (Standardwährung)';
$mod_strings['LBL_TAX_AMOUNT'] = 'Steuer';
$mod_strings['LBL_TAX_AMOUNT_USDOLLAR'] = 'Steuer (Währung)';
$mod_strings['LBL_TOTAL_AMT'] = 'Gesamt';
$mod_strings['LBL_TOTAL_AMT_USDOLLAR'] = 'Gesamt (Währung)';
