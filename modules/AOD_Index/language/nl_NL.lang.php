<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen gebruiker-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Ingevoerd',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door naam',
  'LBL_CREATED' => 'Aangemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DESCRIPTION' => 'Omschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Aangemaakt door gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_LIST_FORM_TITLE' => 'Index Lijst',
  'LBL_MODULE_NAME' => 'Indexeer',
  'LBL_MODULE_TITLE' => 'Indexeer',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Index',
  'LNK_NEW_RECORD' => 'Creeër Index',
  'LNK_LIST' => 'Bekijk Index',
  'LNK_IMPORT_AOD_INDEX' => 'Importeer Index',
  'LBL_SEARCH_FORM_TITLE' => 'Zoek Index',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Bekijk Geschiedenis',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activiteiten',
  'LBL_AOD_INDEX_SUBPANEL_TITLE' => 'Indexeer',
  'LBL_NEW_FORM_TITLE' => 'Nieuwe Index',
  'LBL_LAST_OPTIMISED' => 'Geoptimaliseerd op',
  'LBL_LOCATION' => 'Locatie',
  'LBL_SEARCH_DOCUMENTS' => 'Zoek Documenten',
  'LBL_SEARCH_BUTTON' => 'Zoek',
  'LBL_SEARCH_QUERY_PLACEHOLDER' => 'Invoer zoeken...',
  'LBL_INDEX_STATS' => 'Indexeer statistiek',
  'LBL_OPTIMISE_NOW' => "Optimaliseer Nu",
  'LBL_TOTAL_RECORDS' => 'Totaal aantal records',
  'LBL_INDEXED_RECORDS' => 'Geindexeerde records',
  'LBL_UNINDEXED_RECORDS' => 'Niet Geindexeerde records',
  'LBL_FAILED_RECORDS' => 'Mislukte records',
  'LBL_INDEX_FILES' => 'Index aantal Bestanden',
  'LBL_SEARCH_RESULT_SCORE' => 'Zoek Score',
  'LBL_SEARCH_RESULT_MODULE' => 'Module',
  'LBL_SEARCH_RESULT_NAME' => 'Naam',
  'LBL_SEARCH_RESULT_DATE_CREATED' => 'Datum Gecreëerd',
  'LBL_SEARCH_RESULT_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_SEARCH_RESULT_EMPTY' => 'Geen resultaat',
  'LBL_SEARCH_RESULT_SUMMARY' => 'Samenvattig',
  'LBL_FAILED_INDEX_MODULE' => 'Module',
  'LBL_FAILED_INDEX_NAME' => 'Naam',
  'LBL_FAILED_INDEX_DATE' => 'Datum',
  'LBL_FAILED_INDEX_ERROR' => 'Fout',
  'LBL_NEVER_OPTIMISED' => 'Nooit',
  'LBL_USE_AOD_SEARCH' => 'Geavanceerd zoeken',
  'LBL_USE_VANILLA_SEARCH' => 'Standaard zoeken',
);
