<?php

$connector_strings['LBL_LICENSING_INFO'] = '<table border="0" cellspacing="1"><tr><td valign="top" width="35%" class="dataLabel">Dies ist der URL für den Company Insider Plug-in, der verwendet wird, um LinkedIn&#169; Firmeninformatinen anzuzeigen. Sie brauchen den URL nicht zu verändern, wenn LinkedIn&#169; den URL für den Plug-In nicht verändert.</td></tr></table>';
$connector_strings['LBL_NAME'] = 'Name des Unternehmens';
$connector_strings['company_url'] = 'URL';
$connector_strings['oauth_consumer_key'] = 'API-Schlüssel';
$connector_strings['oauth_consumer_secret'] = 'Geheimer Schlüssel';
