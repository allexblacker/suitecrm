<?php

$connector_strings['LBL_ENGAGE'] = 'Sprechen Sie neue Kunden an';
$connector_strings['LBL_ENGAGE_SUB'] = 'mit interessanten Themen';
$connector_strings['LBL_GET_STARTED'] = 'Jetzt beginnen!';
$connector_strings['LBL_OPP'] = 'Finden Sie neue Verkaufschancen';
$connector_strings['LBL_OPP_SUB'] = 'um mehr Kunden zu erreichen';
$connector_strings['LBL_REFERRAL'] = 'Erhalten Sie Empfehlungen';
$connector_strings['LBL_REFERRAL_SUB'] = 'für wichtige Entscheidungsträger';
$connector_strings['LBL_TOS0'] = 'Ich akzeptiere die InsideView';
$connector_strings['LBL_TOS1'] = 'Nutzungsbedingungen';
$connector_strings['LBL_TOS3'] = 'Datenschutzrichtlinien';
$connector_strings['iv_description0'] = 'Beim Klick auf ""Jetzt beginnen"" akzeptieren Sie InsideViews""';
