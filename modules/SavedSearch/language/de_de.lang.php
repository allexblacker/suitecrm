<?php

$mod_strings['LBL_ASCENDING'] = 'Aufsteigend';
$mod_strings['LBL_CREATED_BY'] = 'Erstellt von';
$mod_strings['LBL_DELETE_BUTTON_TITLE'] = 'Gespeicherte Suche löschen';
$mod_strings['LBL_DELETE_CONFIRM'] = 'Möchten Sie die gewählte gespeicherte Suche wirklich löschen?';
$mod_strings['LBL_DESCENDING'] = 'Absteigend';
$mod_strings['LBL_DIRECTION'] = 'Richtung:';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lister meiner gespeicherten Suchen ';
$mod_strings['LBL_LIST_MODULE'] = 'Modul';
$mod_strings['LBL_LIST_NAME'] = 'Name';
$mod_strings['LBL_MODIFY_CURRENT_FILTER'] = 'Aktuellen Filter anpassen';
$mod_strings['LBL_MODIFY_CURRENT_SEARCH'] = 'Suche ändern';
$mod_strings['LBL_MODULE_TITLE'] = 'Meine gespeicherten Suchen';
$mod_strings['LBL_ORDER_BY_COLUMNS'] = 'Sortieren nach Spalte:';
$mod_strings['LBL_PREVIOUS_SAVED_SEARCH'] = 'Früher gespeicherte Suchen:';
$mod_strings['LBL_PREVIOUS_SAVED_SEARCH_HELP'] = 'Eine bestehende gespeicherte Suche bearbeiten oder löschen.';
$mod_strings['LBL_SAVE_BUTTON_TITLE'] = 'Aktuelle Suche speichern';
$mod_strings['LBL_SAVE_SEARCH_AS'] = 'Suche speichern als:';
$mod_strings['LBL_SAVE_SEARCH_AS_HELP'] = 'Dies speichert Ihre Anzeigeeinstellungen und Filter im Reiter Erweiterte Suche.';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Meine gespeicherten Suchen : Suche';
$mod_strings['LBL_UPDATE_BUTTON_TITLE'] = 'Aktualisieren der gespeicherten Suche';
