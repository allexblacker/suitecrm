<?php

$mod_strings['ERR_DELETE_CONFIRM'] = 'Möchten Sie diese Dokumentversion löschen?';
$mod_strings['ERR_DELETE_LATEST_VERSION'] = 'Die letzte Version eines Dokuments kann nicht gelöscht werden.';
$mod_strings['ERR_DOC_VERSION'] = 'Dokumentversion';
$mod_strings['ERR_FILENAME'] = 'Dateiname';
$mod_strings['LBL_ACTIVE_DATE'] = 'Veröffentlichungsdatum';
$mod_strings['LBL_CHANGE_LOG'] = 'Änderungsprotokoll';
$mod_strings['LBL_CREATED_BY_NAME'] = 'Erstellt von';
$mod_strings['LBL_CURRENT_DOC_VERSION'] = 'Letzte Version:';
$mod_strings['LBL_DET_CREATED_BY'] = 'Erstellt von:';
$mod_strings['LBL_DET_DATE_CREATED'] = 'Erstellungsdatum:';
$mod_strings['LBL_DOCUMENT'] = 'Verknüpftes Dokument';
$mod_strings['LBL_DOC_ID'] = 'ID Dokument Quelle';
$mod_strings['LBL_DOC_NAME'] = 'Dokumentname:';
$mod_strings['LBL_DOC_TYPE'] = 'Quelle';
$mod_strings['LBL_DOC_URL'] = 'URL Dokument Quelle';
$mod_strings['LBL_DOC_VERSION'] = 'Version:';
$mod_strings['LBL_EXPIRATION_DATE'] = 'Ablaufdatum';
$mod_strings['LBL_FILENAME'] = 'Datei:';
$mod_strings['LBL_FILE_EXTENSION'] = 'Dateierweiterung';
$mod_strings['LBL_LATEST_REVISION'] = 'Letzte Version';
$mod_strings['LBL_MIME'] = 'MIME-Typ';
$mod_strings['LBL_MODULE_NAME'] = 'Dokumentversion';
$mod_strings['LBL_REVISION'] = 'Version';
$mod_strings['LBL_REVISIONS'] = 'Versionen';
$mod_strings['LBL_REVISION_NAME'] = 'Versionsnummer';
$mod_strings['LBL_REV_LIST_CREATED'] = 'Erstellt von';
$mod_strings['LBL_REV_LIST_ENTERED'] = 'Erstellungsdatum:';
$mod_strings['LBL_REV_LIST_FILENAME'] = 'Datei';
$mod_strings['LBL_REV_LIST_LOG'] = 'Änderungsprotokoll';
$mod_strings['LBL_REV_LIST_REVISION'] = 'Version';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Dokumente suchen';
$mod_strings['LNK_DOCUMENT_LIST'] = 'Dokumente anzeigen';
$mod_strings['LNK_NEW_DOCUMENT'] = 'Dokument erstellen';
$mod_strings['LNK_NEW_MAIL_MERGE'] = 'Serienbrief';
