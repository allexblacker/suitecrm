<?php

$mod_strings['ERR_CALL'] = 'Kann diese Funktion nicht aufrufen: %s';
$mod_strings['ERR_CURL'] = 'Kein CURL - Kann URL Jobs nicht durchführen';
$mod_strings['ERR_FAILED'] = 'Unerwarteter Fehler, bitte überprüfen Sie die Logs von SuiteCRM und php.';
$mod_strings['ERR_JOBTYPE'] = 'Unbekannter Jobtyp: %s';
$mod_strings['ERR_JOB_FAILED_VERBOSE'] = 'Job %1$s (%2$s) im CRON Lauf fehlgeschlagen';
$mod_strings['ERR_NOSUCHUSER'] = 'Benutzer ID %s nicht gefunden';
$mod_strings['ERR_NOUSER'] = 'Für diesen Job wurde keine Benutzer ID angegeben';
$mod_strings['ERR_PHP'] = '%s [%d]: %s in %s auf Zeile %d';
$mod_strings['ERR_TIMEOUT'] = 'Erzwungener Fehler durch Zeitablauf';
$mod_strings['LBL_CLIENT'] = 'Besitzer';
$mod_strings['LBL_DATA'] = 'Job Daten';
$mod_strings['LBL_EXECUTE_TIME'] = 'Ausführungszeit';
$mod_strings['LBL_FAIL_COUNT'] = 'Fehlschläge';
$mod_strings['LBL_INTERVAL'] = 'Minimaler Intervall zwischen Versuchen';
$mod_strings['LBL_MESSAGE'] = 'Nachrichten';
$mod_strings['LBL_NAME'] = 'Name des Jobs';
$mod_strings['LBL_PERCENT'] = 'Prozent erledigt';
$mod_strings['LBL_REQUEUE'] = 'Bei Fehlschlag wiederholen';
$mod_strings['LBL_RESOLUTION'] = 'Ergebnis';
$mod_strings['LBL_RETRY_COUNT'] = 'Maximale Wiederholungen';
$mod_strings['LBL_SCHEDULER_ID'] = 'Zeitplaner';
$mod_strings['LBL_STATUS'] = 'Job-Status';
