<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Datum aangemaakt',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door naam',
  'LBL_CREATED' => 'Aangemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DESCRIPTION' => 'Omschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Aangemaakt door gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Bewerken',
  'LBL_REMOVE' => 'Verwijderen',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen gebruiker-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_LIST_FORM_TITLE' => 'Grafieken lijst',
  'LBL_MODULE_NAME' => 'Grafieken',
  'LBL_MODULE_TITLE' => 'Grafieken',
  'LBL_HOMEPAGE_TITLE' => 'Mijn grafieken',
  'LNK_NEW_RECORD' => 'Aanmaken grafieken',
  'LNK_LIST' => 'Overzicht grafieken',
  'LNK_IMPORT_AOR_CHARTS' => 'Importeer grafieken',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken grafieken',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Overzicht Geschiedenis',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activiteiten',
  'LBL_AOR_CHARTS_SUBPANEL_TITLE' => 'Grafieken',
  'LBL_NEW_FORM_TITLE' => 'Nieuwe grafieken',
);
