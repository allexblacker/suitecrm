<?php

$dashletStrings['OpportunitiesByLeadSourceDashlet']['LBL_DESCRIPTION'] = 'Tortendiagramm der Verkaufschancen nach Quelle';
$dashletStrings['OpportunitiesByLeadSourceDashlet']['LBL_REFRESH'] = 'Diagramm aktualisieren';
$dashletStrings['OpportunitiesByLeadSourceDashlet']['LBL_TITLE'] = 'Alle Verkaufschancen nach Quelle';
