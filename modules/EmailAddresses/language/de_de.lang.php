<?php

$mod_strings['LBL_DATE_CREATE'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Löschen';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'E-Mail Adresse';
$mod_strings['LBL_EMAIL_ADDRESS_CAPS'] = 'E-Mail Adresse Großbuchstaben';
$mod_strings['LBL_EMAIL_ADDRESS_ID'] = 'ID';
$mod_strings['LBL_INVALID_EMAIL'] = 'Ungültige E-Mail Adresse:';
$mod_strings['LBL_OPT_OUT'] = 'E-Mails abgemeldet';
