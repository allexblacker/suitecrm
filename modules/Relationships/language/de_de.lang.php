<?php

$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_ID'] = 'Beziehungs ID';
$mod_strings['LBL_JOIN_KEY_LHS'] = 'Join Schlüssel LHS';
$mod_strings['LBL_JOIN_KEY_RHS'] = 'Join Schlüssel RHS';
$mod_strings['LBL_JOIN_TABLE'] = 'Join Tabellenname';
$mod_strings['LBL_LHS_KEY'] = 'LHS Schlüsselname';
$mod_strings['LBL_LHS_MODULE'] = 'LHS Modulname';
$mod_strings['LBL_LHS_TABLE'] = 'LHS Tabellenname';
$mod_strings['LBL_RELATIONSHIP_NAME'] = 'Beziehungsname';
$mod_strings['LBL_RELATIONSHIP_ROLE_COLUMN'] = 'Beziehung Spalte Rollenname';
$mod_strings['LBL_RELATIONSHIP_ROLE_COLUMN_VALUE'] = 'Beziehung Rolle Spaltenwert';
$mod_strings['LBL_RELATIONSHIP_TYPE'] = 'Typ der Geschäftsbeziehung';
$mod_strings['LBL_REVERSE'] = 'Rückwärts';
$mod_strings['LBL_RHS_KEY'] = 'RHS Schlüsselname';
$mod_strings['LBL_RHS_MODULE'] = 'RHS Modulname';
$mod_strings['LBL_RHS_TABLE'] = 'RHS Tabellenname';
