<?php

require_once('modules/dtbc_Approvals/ApprovalsHelper.php');

class ApprovalsHandler {
	
	public function handleAfterSave($bean) {
		$module = $bean->object_name;
		$approvals = $this->getApprovalsForModule($module);
		
		foreach ($approvals as $approval) {
		
			if (!empty($this->getProcessedId($approval, $bean)))
				continue;
		
			if (!$this->checkForEntryCriteria($approval, $bean))
				continue;
			
			$this->setProcessedState($approval, $bean, ApprovalsStates::inProgress, null);
			
			$beanChanged = $this->doActions($approval, $bean, ApprovalsParts::initial, null);
			if ($beanChanged)
				$bean->save();
			
			$step = $this->getStep($approval, 0);
			if (empty($step)) {
				$this->approveRecord($approval, $bean, null);
				continue;
			}
			
			$this->handleStep($approval, $step, $bean);
		}
	}
	
	public function handleStep($approval, $step, $bean) {
		$db = DBManagerFactory::getInstance();
		
		$criteriasQuery = "SELECT * FROM dtbc_approvals_criterias WHERE approval_step_id = " . $db->quoted($step['id']) . " AND approval_id = " . $db->quoted($approval->id);
		$criteria = $db->fetchOne($criteriasQuery);	

		if (empty($criteria))
			return true;
		
		$criteriaMet = $this->criteriaMet($criteria, $bean);
		
		if ($criteriaMet) {
			$this->sendMailToApprovers($approval, $step, $bean);
			return;
		}
		
		if ($criteria['behaviour'] == 'approve') {
			$this->approveRecord($approval, $bean, $step);
		} else if ($criteria['behaviour'] == 'reject') {
			$this->rejectRecord($approval, $bean, $step);
		} else {
			$newStep = $this->getStep($approval, intval($step['order']) + 1);
			if (empty($newStep)) {
				$this->approveRecord($approval, $bean, $step);
				return;
			}
			
			$this->setProcessedState($approval, $bean, ApprovalsStates::inProgress, $newStep);
			$this->handleStep($approval, $newStep, $bean);
		}
	}
		
	public function getStep($approval, $order) {
		$db = DBManagerFactory::getInstance();
		
		$stepQuery = "SELECT * FROM dtbc_approvals_steps WHERE approval_id = " . $db->quoted($approval->id) . " AND `order` = $order";
		return $db->fetchOne($stepQuery);
	}
	
	public function approveRecord($approval, $bean, $step) {
		$beanChanged = $this->doActions($approval, $bean, ApprovalsParts::approval, null);
		$this->setProcessedState($approval, $bean, ApprovalsStates::approved, $step);
		
		if ($beanChanged)
			$bean->save();
	}
	
	public function rejectRecord($approval, $bean, $step) {
		$beanChanged = $this->doActions($approval, $bean, ApprovalsParts::rejection, null);
		$this->setProcessedState($approval, $bean, ApprovalsStates::rejected, $step);
		
		if ($beanChanged)
			$bean->save();
	}
	
	private function setProcessedState($approval, $bean, $state, $step) {
		$alreadyExistingProcessedId = $this->getProcessedId($approval, $bean);
		$stepNum = empty($step) ? 0 : $step['order'];
		
		$db = DBManagerFactory::getInstance();
		
		if (empty($alreadyExistingProcessedId)) {
			$fields = array(
				'id' => 'UUID()',
				'approval_id' => $db->quoted($approval->id),
				'bean_id' => $db->quoted($bean->id),
				'date_entered' => "NOW()",
				'date_modified' => "NOW()",
				'state' => $db->quoted($state),
				'step' => $stepNum
			);
			
			ApprovalsDbHelper::insertQuery("dtbc_approvals_processed", $fields);
		} else {
			$fields = array(
				'date_modified' => "NOW()",
				'state' => $db->quoted($state),
				'step' => $stepNum
			);
			
			ApprovalsDbHelper::updateQuery("dtbc_approvals_processed", $fields, $alreadyExistingProcessedId);
		}
	}
	
	private function getProcessedId($approval, $bean) {
		$db = DBManagerFactory::getInstance();
		
		$processedQuery = "SELECT id FROM dtbc_approvals_processed WHERE approval_id = " . $db->quoted($approval->id) . " AND bean_id = " . $db->quoted($bean->id);
		return $db->getOne($processedQuery);
	}
	
	private function doFieldUpdate($bean, $action) {
		$bean->{$action['field']} = $action['value'];
	}
	
	private function doEmailSend($bean, $action) {
		include_once('modules/EmailTemplates/EmailTemplate.php');
		$db = DBManagerFactory::getInstance();
		
		$recipientsQuery = "SELECT * FROM dtbc_approvals_recipients WHERE approval_action_id = " . $db->quoted($action['id']);
		$recipientsResponse = $db->query($recipientsQuery);
		
		while($row = $db->fetchByAssoc($recipientsResponse)) {
			// Sadly we need to get a new one every time because of the change machanism of the template
			$emailTemp = new EmailTemplate();
			$emailTemp->retrieve($action['template']);

			if($emailTemp->id == '')
				return false;
			
			$emailAddress = $this->getRecipientEmailAddress($bean, $row);
			
			$this->parse_template($bean, $emailTemp, array(), '', '');
			$this->sendEmail(array($emailAddress), $emailTemp->subject, $emailTemp->body_html, $emailTemp->body, $bean, array(), array(), array());
		}
	}
	
	private function getRecipientEmailAddress($bean, $recipient) {
		switch ($recipient['type']) {
			case 'email':
				return $recipient['value'];
			case 'user':
				return $this->loadUserAndGetEmailAddress($recipient['value']);
			case 'assigned':
				return $this->loadUserAndGetEmailAddress($bean->assigned_user_id);
			case 'creator':
				return $this->loadUserAndGetEmailAddress($bean->created_by);
		}
	}
	
	private function loadUserAndGetEmailAddress($userId) {
		$user = new User();
		$user->retrieve($userId);
		
		return $user->emailAddress->getPrimaryAddress($user);
	}
	
	private function doActions($approval, $bean, $partId, $stepId) {
		$db = DBManagerFactory::getInstance();
		
		$partOrStep = empty($partId) ? " approval_step_id = " . $db->quoted($stepId) : " approval_part_id = " . $db->quoted($partId);
		
		$actionsQuery = "SELECT * FROM dtbc_approvals_actions WHERE approval_id = " . $db->quoted($approval->id) . " AND $partOrStep ";
		$actionsResponse = $db->query($actionsQuery);
		
		$beanChanged = false;
		while($row = $db->fetchByAssoc($actionsResponse)) {
			if ($row['type'] == "fieldupdate") {
				$this->doFieldUpdate($bean, $row);
				$beanChanged = true;
			} else {
				$this->doEmailSend($bean, $row);
			}
		}
		
		return $beanChanged;
	}
	
	private function criteriaMet($criteria, $bean) {
		$criteriaLines = $this->getCriteriaLines($criteria);
		$formula = $this->sanitizeFormula($criteria['formula']);
		
		for ($i = 0; $i < count($criteriaLines); $i++) {
			$formula = str_replace("{C$i}", $this->compare($criteriaLines[$i], $bean) ? " true " : " false ", $formula);
		}

		$formula = str_replace("(", " ( ", $formula);
		$formula = str_replace(")", " ) ", $formula);
		$formula = str_replace("AND", " AND ", $formula);
		$formula = str_replace("OR", " OR ", $formula);
		
		return eval("return $formula;");
	}
	
	private function compare($criteriaLine, $bean) {
		$field = $criteriaLine['field'];
		$value = $criteriaLine['value'];
		
		$actualBean = $bean;
		$fieldValue = "";
		
		if (strtolower($criteriaLine['path']) != strtolower($bean->object_name)) {
			if (!$bean->load_relationship($criteriaLine['path']))
				return false;
			
			// Is it a problem if there is no connected bean?
			$actualBean = reset($bean->{$criteriaLine['path']}->getBeans());
			if (empty($actualBean))
				return false;
		}
		
		$data = $actualBean->field_defs[$field];
		$fieldValue = "";
		if($data['type'] == 'relate' && isset($data['id_name'])) {
			$field = $data['id_name'];
			$fieldValue = $actualBean->{$data['id_name']};
		}elseif($data['type'] == 'function' && !empty($data['function']['include']) && !empty($data['function']['name'])){
			$file = $data['function']['include'];
			if(file_exists($file)){
				require_once($file);
				$function = $data['function']['name'];
				if(function_exists($function)){
					require_once("custom/include/dtbc/helpers.php");
					$fieldValue = call_user_func($function, $actualBean, $data['name'], "", "ApprovalWorkFlow");

				}
			}
		} 
		else {
			$fieldValue = $actualBean->$field;
		}
		
		switch ($criteriaLine['operator']) {
			case "Not_Equal_To":
				return $fieldValue != $value;
            case "Greater_Than": 
				return $fieldValue >  $value;
            case "Less_Than": 
				return $fieldValue <  $value;
            case "Greater_Than_or_Equal_To":
				return $fieldValue >= $value;
            case "Less_Than_or_Equal_To":
				return $fieldValue <= $value;
            case "Contains" :
				return strpos($fieldValue,$value);
            case "Not_Contains" :
				return strpos($fieldValue,$value) === FALSE;
            case "Starts_With" :
				return strrpos($fieldValue,$value, -strlen($fieldValue));
            case "Ends_With" :
				return strpos($fieldValue,$value,strlen($fieldValue) - strlen($value));
            case "is_null":
				return $fieldValue == '';
            case "is_not_null":
				return $fieldValue != '' && $fieldValue != null;
            case "Equal_To":
            default:
				return $fieldValue == $value; 
		}
	}
	
	private function sanitizeFormula($formula) {
		$matches = array();
		preg_match_all('/(AND|OR|\(|\)|\{C[0-9+]\})/i', $formula, $matches);
		return implode('', $matches[0]);
	}
	
	private function getEmailsFromGroup($groupId) {
		$db = DBManagerFactory::getInstance();
	
		$emails = array();
		
		$groupsQuery = "SELECT user_id FROM securitygroups_users WHERE securitygroup_id = " . $db->quoted($groupId);
		$groupsResponse = $db->query($groupsQuery);
		
		while($row = $db->fetchByAssoc($groupsResponse)) {
			$emails[$row['user_id']] = $this->loadUserAndGetEmailAddress($row['user_id']);
		}

		return $emails;
	}
	
	private function getApproversEmailAdresses($step) {
		$db = DBManagerFactory::getInstance();
	
		$emails = array();
		
		$approversQuery = "SELECT * FROM dtbc_approvals_approvers WHERE approval_step_id = " . $db->quoted($step['id']);
		$approversResponse = $db->query($approversQuery);
		
		while($row = $db->fetchByAssoc($approversResponse)) {
			if ($row['type'] == 'user') {
				$emails[$row['value']] = $this->loadUserAndGetEmailAddress($row['value']);
			} else {
				$emails = array_merge($emails, $this->getEmailsFromGroup($row['value']));
			}
		}

		return array_unique($emails);
	}
	
	private function insertResponseRows($approval, $step, $bean, $emails) {
		$db = DBManagerFactory::getInstance();
		
		foreach ($emails as $user_id => $email) {
			$fields = array(
				'id' => 'UUID()',
				'approval_id' => $db->quoted($approval->id),
				'approval_step_id' => $db->quoted($step['id']),
				'bean_id' => $db->quoted($bean->id),
				'date_entered' => "NOW()",
				'date_modified' => "NOW()",
				'user_id' => $db->quoted($user_id),
				'response' => $db->quoted(ApprovalsResponses::none),
			);
		
			ApprovalsDbHelper::insertQuery("dtbc_approvals_responses", $fields);
		}
	}
	
	private function sendMailToApprovers($approval, $step, $bean) {
		$emails = $this->getApproversEmailAdresses($step);
		
		$this->insertResponseRows($approval, $step, $bean, $emails);
		
		$emailTemp = new EmailTemplate();
		$emailTemp->retrieve($approval->email_template);

		if($emailTemp->id == '')
			return false;
		
		$this->parse_template($bean, $emailTemp, array(), $approval, $step);
		$this->sendEmail(array_values($emails), $emailTemp->subject, $emailTemp->body_html, $emailTemp->body, $bean, array(), array(), array());
	}
	
	private function checkForEntryCriteria($approval, $bean) {
		$db = DBManagerFactory::getInstance();
		
		$criteriasQuery = "SELECT * FROM dtbc_approvals_criterias WHERE approval_part_id = " . $db->quoted(ApprovalsParts::entry) . " AND approval_id = " . $db->quoted($approval->id);
		$criteria = $db->fetchOne($criteriasQuery);	

		if (empty($criteria))
			return true;
		
		return $this->criteriaMet($criteria, $bean);
	}
	
	private function getCriteriaLines($criteria) {	
		$db = DBManagerFactory::getInstance();
	
		$criteriaLines = array();
		
		$criteriaLineQuery = "SELECT * FROM dtbc_approvals_criteria_lines WHERE approval_criteria_id = " . $db->quoted($criteria['id']) . " ORDER BY `order`";
		$criteriaLineResponse = $db->query($criteriaLineQuery);
		
		while($row = $db->fetchByAssoc($criteriaLineResponse)) {
			$criteriaLines []= $row;
		}

		return	$criteriaLines;
	}
	
	private function getApprovalsForModule($module) {
		$db = DBManagerFactory::getInstance();
		$approvals = BeanFactory::getBean('dtbc_Approvals')->get_full_list(''," active = 1 AND LOWER(target_module) = " . $db->quoted(strtolower($module)));
		
		return $approvals == null ? array() : $approvals;
	}
	
	private function parse_template(SugarBean $bean, &$template, $object_override = array(), $approval, $step){
        global $sugar_config;

        require_once('custom/modules/AOW_Actions/actions/templateParser.php');

        $object_arr[$bean->module_dir] = $bean->id;

        foreach($bean->field_defs as $bean_arr){
            if($bean_arr['type'] == 'relate'){
                if(isset($bean_arr['module']) &&  $bean_arr['module'] != '' && isset($bean_arr['id_name']) &&  $bean_arr['id_name'] != '' && $bean_arr['module'] != 'EmailAddress'){
                    $idName = $bean_arr['id_name'];
                    if(isset($bean->field_defs[$idName]) && $bean->field_defs[$idName]['source'] != 'non-db'){
                        if(!isset($object_arr[$bean_arr['module']])) $object_arr[$bean_arr['module']] = $bean->$idName;
                    }
                }
            }
            else if($bean_arr['type'] == 'link'){
                if(!isset($bean_arr['module']) || $bean_arr['module'] == '') $bean_arr['module'] = getRelatedModule($bean->module_dir,$bean_arr['name']);
                if(isset($bean_arr['module']) &&  $bean_arr['module'] != ''&& !isset($object_arr[$bean_arr['module']])&& $bean_arr['module'] != 'EmailAddress'){
                    $linkedBeans = $bean->get_linked_beans($bean_arr['name'],$bean_arr['module'], array(), 0, 1);
                    if($linkedBeans){
                        $linkedBean = $linkedBeans[0];
                        if(!isset($object_arr[$linkedBean->module_dir])) $object_arr[$linkedBean->module_dir] = $linkedBean->id;
                    }
                }
            }
        }

        $object_arr['Users'] = is_a($bean, 'User') ? $bean->id : $bean->assigned_user_id;

        $object_arr = array_merge($object_arr, $object_override);

        $parsedSiteUrl = parse_url($sugar_config['site_url']);
        $host = $parsedSiteUrl['host'];
        if(!isset($parsedSiteUrl['port'])) {
            $parsedSiteUrl['port'] = 80;
        }

        $port		= ($parsedSiteUrl['port'] != 80) ? ":".$parsedSiteUrl['port'] : '';
        $path		= !empty($parsedSiteUrl['path']) ? $parsedSiteUrl['path'] : "";
        $cleanUrl	= "{$parsedSiteUrl['scheme']}://{$host}{$port}{$path}";

        $url =  $cleanUrl."/index.php?module={$bean->module_dir}&action=DetailView&record={$bean->id}";

        $template->subject = str_replace("\$contact_user","\$user",$template->subject);
        $template->body_html = str_replace("\$contact_user","\$user",$template->body_html);
        $template->body = str_replace("\$contact_user","\$user",$template->body);
        $template->subject = CustomAowTemplateParser::parse_template($template->subject, $object_arr);
        $template->body_html = CustomAowTemplateParser::parse_template($template->body_html, $object_arr);
        $template->body_html = str_replace("\$url",$url,$template->body_html);
		
        $template->body = CustomAowTemplateParser::parse_template($template->body, $object_arr);
        $template->body = str_replace("\$url",$url,$template->body);
		
		if (!empty($approval) && !empty($step)) {
			$approveUrl = $cleanUrl."/index.php?module=dtbc_Approvals&action=approve&bean_id={$bean->id}&approval_id={$approval->id}&step_id=" . $step['id'];
			$declineUrl = $cleanUrl."/index.php?module=dtbc_Approvals&action=decline&bean_id={$bean->id}&approval_id={$approval->id}&step_id=" . $step['id'];
			
			$template->body_html = str_replace("@@APPROVE_URL@@", $approveUrl, $template->body_html);
			$template->body_html = str_replace("@@DECLINE_URL@@", $declineUrl, $template->body_html);
			
			$template->body = str_replace("@@APPROVE_URL@@", $approveUrl, $template->body);
			$template->body = str_replace("@@DECLINE_URL@@", $declineUrl, $template->body);
		}
    }
	
	private function sendEmail($emailTo, $emailSubject, $emailBody, $altemailBody, SugarBean $relatedBean = null, $emailCc = array(), $emailBcc = array(), $attachments = array())
    {
		require_once('custom/modules/Emails/customEmail.php');
        require_once('include/SugarPHPMailer.php');

        $emailObj = new CustomEmail();
        $defaults = $emailObj->getSystemDefaultEmail();
		
        $mail = new SugarPHPMailer();
        $mail->setMailerForSystem();
        $mail->From = $defaults['email'];
        $mail->FromName = $defaults['name'];
        $mail->ClearAllRecipients();
        $mail->ClearReplyTos();
        $mail->Subject=from_html($emailSubject);
        $mail->Body=$emailBody;
        $mail->AltBody = $altemailBody;
        $mail->handleAttachments($attachments);
        $mail->prepForOutbound();

        if(empty($emailTo)) return false;
        foreach($emailTo as $to){
            $mail->AddAddress($to);
        }
        if(!empty($emailCc)){
            foreach($emailCc as $email){
                $mail->AddCC($email);
            }
        }
        if(!empty($emailBcc)){
            foreach($emailBcc as $email){
                $mail->AddBCC($email);
            }
        }

        if (@$mail->Send()) {
            $emailObj->to_addrs= implode(',',$emailTo);
            $emailObj->cc_addrs= implode(',',$emailCc);
            $emailObj->bcc_addrs= implode(',',$emailBcc);
            $emailObj->type= 'out';
            $emailObj->deleted = '0';
            $emailObj->name = $mail->Subject;
            $emailObj->description = $mail->AltBody;
            $emailObj->description_html = $mail->Body;
            $emailObj->from_addr = $mail->From;
            if ( $relatedBean instanceOf SugarBean && !empty($relatedBean->id) ) {
                $emailObj->parent_type = $relatedBean->module_dir;
                $emailObj->parent_id = $relatedBean->id;
            }
            $emailObj->date_sent = TimeDate::getInstance()->nowDb();
            $emailObj->modified_user_id = '1';
            $emailObj->created_by = '1';
            $emailObj->status = 'sent';
            $emailObj->save();

            foreach($attachments as $attachment) {
                $note = new Note();
                $note->id = create_guid();
                $note->date_entered = $attachment->date_entered;
                $note->date_modified = $attachment->date_modified;
                $note->modified_user_id = $attachment->modified_user_id;
                $note->assigned_user_id = $attachment->assigned_user_id;
                $note->new_with_id = true;
                $note->parent_id = $emailObj->id;
                $note->parent_type = $attachment->parent_type;
                $note->name = $attachment->name;;
                $note->filename = $attachment->filename;
                $note->file_mime_type = $attachment->file_mime_type;
                $fileLocation = "upload://{$attachment->id}";
                $dest = "upload://{$note->id}";
                if(!copy($fileLocation, $dest)) {
                    $GLOBALS['log']->debug("EMAIL 2.0: could not copy attachment file to $fileLocation => $dest");
                }
                $note->save();
            }
			
            return true;
        }
		
        return false;
    }
}