<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('modules/dtbc_Approvals/ApprovalsHandler.php');

class dtbc_ApprovalsController extends SugarController
{
    public function action_new_field_update() {
		$this->view = "newfieldupdate";
	}
	
	public function action_new_email_alert() {
		$this->view = "newemailalert";
	}
	
	public function action_new_criteria() {
		$this->view = "newcriteria";
	}
	
	public function action_new_step() {
		$this->view = "newstep";
	}
	
	public function action_approve() {
		$result = $this->handleResponse(ApprovalsResponses::approved, $_REQUEST['bean_id'], $_REQUEST['approval_id'], $_REQUEST['step_id']);
		if ($result) {
			echo "SUCCESS: The record has been approved.";
		} else {
			echo "ERROR: Response already made or wrong record.";
		}
		
		die();
	}
	
	public function action_decline() {
		$result = $this->handleResponse(ApprovalsResponses::rejected, $_REQUEST['bean_id'], $_REQUEST['approval_id'], $_REQUEST['step_id']);
		if ($result) {
			echo "SUCCESS: The record has been rejected.";
		} else {
			echo "ERROR: Response already made or wrong record.";
		}
		
		die();
	}
	
	private function handleResponse($response, $beanId, $approvalId, $stepId) {
		global $current_user;
		
		$db = DBManagerFactory::getInstance();
		
		$responseQuery = "SELECT * FROM dtbc_approvals_responses
						  WHERE
							approval_id = " . $db->quoted($approvalId) . " AND 
							approval_step_id = " . $db->quoted($stepId) . " AND
							bean_id = " . $db->quoted($beanId) . " AND 
							user_id = " . $db->quoted($current_user->id);
							
		$responseRow = $db->fetchOne($responseQuery);	
		if (empty($responseRow) || $responseRow['response'] != ApprovalsResponses::none)
			return false;
		
		$stepQuery = "SELECT * FROM dtbc_approvals_steps WHERE id = " . $db->quoted($stepId);
		$step = $db->fetchOne($stepQuery);	
		if (empty($step))
			return false;
		
		$approval = BeanFactory::getBean('dtbc_Approvals')->retrieve($approvalId);
		if (empty($approval))
			return false;
		
		$bean = BeanFactory::getBean($approval->target_module)->retrieve($beanId);
		if (empty($bean))
			return false;
		
		$this->updateResponseRow($response, $responseRow['id']);
		
		$handler = new ApprovalsHandler();
		
		if ($response == ApprovalsResponses::rejected) {
			$handler->rejectRecord($approval, $bean, $step);
			return true;
		}
		
		if ($step['approval_method'] == ApprovalsMethods::single) {
			return $this->getAndHandleNextStep($handler, $approval, $step, $bean);
		}
		
		$existsNotApprovedLineQuery = "SELECT 1 FROM dtbc_approvals_responses
						  WHERE
							approval_id = " . $db->quoted($approvalId) . " AND 
							approval_step_id = " . $db->quoted($stepId) . " AND
							bean_id = " . $db->quoted($beanId) . " AND 
							response != " . $db->quoted(ApprovalsResponses::approved) . " LIMIT 1";
							
		$existsNotApproved = $db->getOne($existsNotApprovedLineQuery);
		if (!empty($existsNotApproved))
			return true;
		
		return $this->getAndHandleNextStep($handler, $approval, $step, $bean);
	}
	
	private function getAndHandleNextStep($handler, $approval, $step, $bean) {
		$nextStep = $handler->getStep($approval, intval($step['order']) + 1);
		if (empty($nextStep)) {
			$handler->approveRecord($approval, $bean, $step);
			return true;
		}
		
		$handler->handleStep($approval, $nextStep, $bean);
		
		return true;
	}
		
	private function updateResponseRow($response, $responseId) {
		$db = DBManagerFactory::getInstance();
		
		$responseUpdateFields = array(
			'date_modified' => "NOW()",
			'response_time' => "NOW()",
			'response' => $db->quoted($response),
		);
		
		ApprovalsDbHelper::updateQuery("dtbc_approvals_responses", $responseUpdateFields, $responseId);
	}
	
	public function action_save_field_update() {
		global $current_user;
		
		$db = DBManagerFactory::getInstance();
		
		if (empty($_REQUEST['action_id'])) {
			$fields = array(
				'id' => 'UUID()',
				'approval_id' => $db->quoted($_REQUEST['approval_id']),
				'approval_part_id' => $db->quoted($_REQUEST['approval_part_id']),
				'approval_step_id' => $db->quoted($_REQUEST['approval_step_id']),
				'name' => $db->quoted($_REQUEST['name']),
				'description' => $db->quoted($_REQUEST['description']),
				'date_entered' => "NOW()",
				'date_modified' => "NOW()",
				'created_by' => $db->quoted($current_user->id),
				'modified_user_id' => $db->quoted($current_user->id),
				'type' => $db->quoted('fieldupdate'),
				'field' => $db->quoted($_REQUEST['field']),
				'value' => $db->quoted($_REQUEST['value'])
			);
			
			ApprovalsDbHelper::insertQuery("dtbc_approvals_actions", $fields);
		} else {
			$fields = array(
				'name' => $db->quoted($_REQUEST['name']),
				'description' => $db->quoted($_REQUEST['description']),
				'date_modified' => "NOW()",
				'modified_user_id' => $db->quoted($current_user->id),
				'field' => $db->quoted($_REQUEST['field']),
				'value' => $db->quoted($_REQUEST['value'])
			);
			
			ApprovalsDbHelper::updateQuery("dtbc_approvals_actions", $fields, $_REQUEST['action_id']);
		}
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $_REQUEST['approval_id']));
	}
	
	public function action_save_email_alert() {
		global $current_user;
		
		$db = DBManagerFactory::getInstance();
		
		$actionId = $_REQUEST['action_id'];
		
		if (empty($_REQUEST['action_id'])) {
			$actionId = create_guid();
			
			$fields = array(
				'id' => $db->quoted($actionId),
				'approval_id' => $db->quoted($_REQUEST['approval_id']),
				'approval_part_id' => $db->quoted($_REQUEST['approval_part_id']),
				'approval_step_id' => $db->quoted($_REQUEST['approval_step_id']),
				'name' => $db->quoted($_REQUEST['name']),
				'description' => $db->quoted($_REQUEST['description']),
				'date_entered' => "NOW()",
				'date_modified' => "NOW()",
				'created_by' => $db->quoted($current_user->id),
				'modified_user_id' => $db->quoted($current_user->id),
				'type' => $db->quoted('emailalert'),
				'template' => $db->quoted($_REQUEST['template'])
			);
			
			ApprovalsDbHelper::insertQuery("dtbc_approvals_actions", $fields);
		} else {
			$fields = array(
				'name' => $db->quoted($_REQUEST['name']),
				'description' => $db->quoted($_REQUEST['description']),
				'date_modified' => "NOW()",
				'modified_user_id' => $db->quoted($current_user->id),
				'template' => $db->quoted($_REQUEST['template'])
			);
			
			ApprovalsDbHelper::updateQuery("dtbc_approvals_actions", $fields, $actionId);
			
			$db->query("DELETE FROM dtbc_approvals_recipients WHERE approval_action_id = " . $db->quoted($actionId));
		}
		
		$this->insertRecipients($actionId);
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $_REQUEST['approval_id']));
	}
	
	private function insertRecipients($actionId) {
		global $current_user;

		$count = count($_REQUEST['emailType']);
		if ($count == 0)
			return;
		
		$db = DBManagerFactory::getInstance();
		
		$query = "INSERT INTO dtbc_approvals_recipients (id, approval_action_id, date_entered, created_by, type, value) VALUES ";
		
		$valueSets = array();
		foreach ($_REQUEST['emailType'] as $key => $value) {
			$valueSets []= "(UUID(), " . $db->quoted($actionId) . ", NOW(), " . $db->quoted($current_user->id) . ", " . $db->quoted($_REQUEST['emailType'][$key]) . ", " . $db->quoted($_REQUEST['emailValue'][$key]) . ")";
		}
		
		$db->query($query . implode(", ", $valueSets));
	}
	
	public function action_save_criteria() {
		global $current_user;
		
		$db = DBManagerFactory::getInstance();
		
		$criteriaId = $_REQUEST['criteria_id'];
		
		if (empty($_REQUEST['criteria_id'])) {
			$criteriaId = create_guid();
			
			$fields = array(
				'id' => $db->quoted($criteriaId),
				'approval_id' => $db->quoted($_REQUEST['approval_id']),
				'approval_part_id' => $db->quoted($_REQUEST['approval_part_id']),
				'approval_step_id' => $db->quoted($_REQUEST['approval_step_id']),
				'date_entered' => "NOW()",
				'date_modified' => "NOW()",
				'created_by' => $db->quoted($current_user->id),
				'modified_user_id' => $db->quoted($current_user->id),
				'formula' => $db->quoted($_REQUEST['formula'])
			);
			
			if (!empty($_REQUEST['approval_step_id']))
				$fields['behaviour'] = $db->quoted($_REQUEST['behaviour']);
			
			ApprovalsDbHelper::insertQuery("dtbc_approvals_criterias", $fields);
		} else {
			$fields = array(
				'date_modified' => "NOW()",
				'modified_user_id' => $db->quoted($current_user->id),
				'formula' => $db->quoted($_REQUEST['formula'])
			);
			
			if (!empty($_REQUEST['approval_step_id']))
				$fields['behaviour'] = $db->quoted($_REQUEST['behaviour']);
			
			ApprovalsDbHelper::updateQuery("dtbc_approvals_criterias", $fields, $criteriaId);
			
			$db->query("DELETE FROM dtbc_approvals_criteria_lines WHERE approval_criteria_id = " . $db->quoted($criteriaId));
		}
		
		$this->insertCriteriaLines($criteriaId);
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $_REQUEST['approval_id']));
	}
	
	private function insertCriteriaLines($criteriaId) {
		global $current_user;

		$count = count($_REQUEST['field']);
		if ($count == 0)
			return;
		
		$db = DBManagerFactory::getInstance();
		
		$query = "INSERT INTO dtbc_approvals_criteria_lines (id, approval_criteria_id, date_entered, created_by, path, field, operator, value, `order`) VALUES ";
		
		$valueSets = array();
		$counter = 0;
		foreach ($_REQUEST['field'] as $key => $value) {
			$valueSets []= "(UUID(), " . $db->quoted($criteriaId) . ", NOW(), " . $db->quoted($current_user->id) . ", " . $db->quoted($_REQUEST['path'][$key]) . ", " . $db->quoted($_REQUEST['field'][$key]) . ", " . $db->quoted($_REQUEST['operator'][$key]) . ", " . $db->quoted($_REQUEST['value'][$key]) . ", $counter)";
			$counter++;
		}
		
		$db->query($query . implode(", ", $valueSets));
	}
		
	public function action_save_step() {
		global $current_user;
		
		if (empty($_REQUEST['approval_id']))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$db = DBManagerFactory::getInstance();
		
		$stepId = $_REQUEST['step_id'];
		
		if (empty($_REQUEST['step_id'])) {
			$orderQuery = "SELECT MAX(`order`) FROM dtbc_approvals_steps WHERE approval_id = " . $db->quoted($_REQUEST['approval_id']);
			$order = $db->getOne($orderQuery);
			$order = is_null($order) ? 0 : $order + 1;			
			
			$stepId = create_guid();
			
			$fields = array(
				'id' => $db->quoted($stepId),
				'approval_id' => $db->quoted($_REQUEST['approval_id']),
				'name' => $db->quoted($_REQUEST['name']),
				'description' => $db->quoted($_REQUEST['description']),
				'date_entered' => "NOW()",
				'date_modified' => "NOW()",
				'created_by' => $db->quoted($current_user->id),
				'modified_user_id' => $db->quoted($current_user->id),
				'`order`' => $order,
				'approval_method' => $db->quoted($_REQUEST['approval_method'])
			);
			
			ApprovalsDbHelper::insertQuery("dtbc_approvals_steps", $fields);
		} else {
			$fields = array(
				'name' => $db->quoted($_REQUEST['name']),
				'description' => $db->quoted($_REQUEST['description']),
				'date_modified' => "NOW()",
				'modified_user_id' => $db->quoted($current_user->id),
				'approval_method' => $db->quoted($_REQUEST['approval_method'])
			);
			
			ApprovalsDbHelper::updateQuery("dtbc_approvals_steps", $fields, $stepId);
			
			$db->query("DELETE FROM dtbc_approvals_approvers WHERE approval_step_id = " . $db->quoted($stepId));
		}
		
		$this->insertApprovers($stepId);
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $_REQUEST['approval_id']));
	}
	
	private function insertApprovers($stepId) {
		global $current_user;

		$count = count($_REQUEST['approverType']);
		if ($count == 0)
			return;
		
		$db = DBManagerFactory::getInstance();
		
		$query = "INSERT INTO dtbc_approvals_approvers (id, approval_step_id, date_entered, created_by, type, value) VALUES ";
		
		$valueSets = array();
		foreach ($_REQUEST['approverType'] as $key => $value) {
			$valueSets []= "(UUID(), " . $db->quoted($stepId) . ", NOW(), " . $db->quoted($current_user->id) . ", " . $db->quoted($_REQUEST['approverType'][$key]) . ", " . $db->quoted($_REQUEST['approverValue'][$key]) . ")";
		}
		
		$db->query($query . implode(", ", $valueSets));
	}
	
	public function action_remove_action() {
		$db = DBManagerFactory::getInstance();
		
		$actionQuery = "SELECT * FROM dtbc_approvals_actions WHERE id = " . $db->quoted($_REQUEST['action_id']);
		$action = $db->fetchOne($actionQuery);
				
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($action['approval_id']);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		ApprovalsDbHelper::deleteQuery('dtbc_approvals_actions', array('id' => $db->quoted($_REQUEST['action_id'])));
		
		if ($_REQUEST['type'] == 'emailalert') {
			ApprovalsDbHelper::deleteQuery('dtbc_approvals_recipients', array('approval_action_id' => $db->quoted($_REQUEST['action_id'])));
		}
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $approvalBean->id));
	}
	
	public function action_remove_criteria() {
		$db = DBManagerFactory::getInstance();
		
		$criteriaQuery = "SELECT * FROM dtbc_approvals_criterias WHERE id = " . $db->quoted($_REQUEST['criteria_id']);
		$criteria = $db->fetchOne($criteriaQuery);
				
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($criteria['approval_id']);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$this->removeCriteriaWithId($_REQUEST['criteria_id']);
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $approvalBean->id));
	}
	
	public function action_remove_step() {
		$db = DBManagerFactory::getInstance();
		
		$stepQuery = "SELECT * FROM dtbc_approvals_steps WHERE id = " . $db->quoted($_REQUEST['step_id']);
		$step = $db->fetchOne($stepQuery);
				
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($step['approval_id']);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$criteriaQuery = "SELECT id FROM dtbc_approvals_criterias WHERE approval_step_id = " . $db->quoted($_REQUEST['step_id']);
		$criteriaId = $db->getOne($criteriaQuery);
		
		ApprovalsDbHelper::deleteQuery('dtbc_approvals_steps', array('id' => $db->quoted($_REQUEST['step_id'])));
		ApprovalsDbHelper::deleteQuery('dtbc_approvals_approvers', array('approval_step_id' => $db->quoted($_REQUEST['step_id'])));
		
		$this->removeCriteriaWithId($criteriaId);
		
		$orderQuery = "UPDATE dtbc_approvals_steps SET `order` = `order` - 1 WHERE `order` > " . $step['order']; 
		$db->query($orderQuery);
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $approvalBean->id));
	}
	
	private function removeCriteriaWithId($criteriaId) {
		$db = DBManagerFactory::getInstance();
		
		ApprovalsDbHelper::deleteQuery('dtbc_approvals_criterias', array('id' => $db->quoted($criteriaId)));
		ApprovalsDbHelper::deleteQuery('dtbc_approvals_criteria_lines', array('approval_criteria_id' => $db->quoted($criteriaId)));
	}
	
	private function changeSteps($stepId, $change = 1) {
		$db = DBManagerFactory::getInstance();
		
		$stepQuery = "SELECT * FROM dtbc_approvals_steps WHERE id = " . $db->quoted($stepId);
		$step = $db->fetchOne($stepQuery);
				
		$approvalBean = BeanFactory::getBean('dtbc_Approvals')->retrieve($step['approval_id']);
		if (empty($approvalBean))
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'index');
		
		$otherStepQuery = "SELECT id FROM dtbc_approvals_steps WHERE approval_id = " . $db->quoted($approvalBean->id) . " AND `order` = " . (intval($step['order']) + $change);
		$otherStepId = $db->getOne($otherStepQuery);
		
		if (empty($otherStepId)) {
			ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $approvalBean->id));
			return;
		}
				
		$orderQuery = "UPDATE dtbc_approvals_steps SET `order` = `order` + " . $change . " WHERE id = " . $db->quoted($step['id']);
		$db->query($orderQuery);
		
		$orderQuery = "UPDATE dtbc_approvals_steps SET `order` = `order` + " . $change * -1 . " WHERE id = " . $db->quoted($otherStepId);
		$db->query($orderQuery);
		
		ApprovalsHelper::customRedirect('dtbc_Approvals', 'DetailView', array('record' => $approvalBean->id));
	}
	
	public function action_step_up() {
		$this->changeSteps($_REQUEST['step_id'], -1);
	}
	
	public function action_step_down() {
		$this->changeSteps($_REQUEST['step_id'], 1);
	}
}
