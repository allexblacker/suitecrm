{literal}
<style>
#approversTable td {
	padding: 0px 20px;
}

#approversTable th {
	padding: 6px 10px;
	text-align: center;
}
</style>
{/literal}

<div class="moduleTitle" style="margin-bottom: 30px;">
	<h2 class="module-title-text">{$mod.LBL_STEP}</h2>
	<div class="clear"></div>
</div>

<form action="index.php" method="POST" name="EditView" id="EditView">
	<input type="hidden" name="module" id="module" value="dtbc_Approvals" />
	<input type="hidden" name="action" id="action" value="save_step" />
	<input type="hidden" name="step_id" id="step_id" value="{$stepId}" />
	<input type="hidden" name="approval_id" id="approval_id" value="{$beanId}" />
	<input type="hidden" name="approval_part_id" id="approval_part_id" value="{$partId}" />
	
	<div id="EditView_tabs">
		<div class="panel-content">
			<div class="panel panel-default" style="margin-bottom: 20px;">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_BASIC}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_NAME}:<span class="required">*</span></div>
								<div class="col-xs-12 col-sm-8 edit-view-field" type="name">
									<input type="text" name="name" id="name" size="30" maxlength="255" value="{$name}" title="" />
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_DESCRIPTION}:</div>
								<div class="col-xs-12 col-sm-8 edit-view-field " type="text" colspan="3">
									<textarea id="description" name="description" rows="6" cols="80" title="" tabindex="">{$description}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading ">
					<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
						<div class="col-xs-10 col-sm-11 col-md-11">{$mod.LBL_APPROVERS}</div>
					</a>
				</div>
				<div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
					<div class="tab-content">
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_APPROVAL_METHOD}:<span class="required">*</span></div>
								<div class="col-xs-12 col-sm-8 edit-view-field" type="name">
									<select name="approval_method" id="approval_method">
										{html_options options=$approvalMethods selected=$approval_method}
									</select> 
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label">{$mod.LBL_RECIPIENTS}:</div>
								<div class="col-xs-12 col-sm-8">
									<select name="type" id="type" onchange="javascript:onTypeChange(this);" style="float: left;">
										<option value="user">{$mod.LBL_USER}</option>
										<option value="group">{$mod.LBL_SECURITY_GROUP}</option>
									</select> 
									<select id="users" style="float: left; margin-left: 10px;">
										{html_options options=$users}
									</select>
									<select id="groups" style="float: left; margin-left: 10px; display: none;">
										{html_options options=$groups}
									</select>
									<div id="buttonHolder" style="float: left; margin-left: 10px;">
										<input type='button' value='{$mod.LBL_ADD}' onclick='javascript:setupAndAddApproverLine()' id='addButton' />
									</div>
									<div style="clear: both;"></div>
								</div>
							</div>
						</div>
						<div class="row edit-view-row">
							<div class="col-xs-12 col-sm-12 edit-view-row-item">
								<div class="col-xs-12 col-sm-2 label"></div>
								<div class="col-xs-12 col-sm-8">
									<table id="approversTable" style="border-collapse: collapse;">
										<thead>
											<tr>
												<th>{$mod.LBL_TYPE}</th>
												<th>{$mod.LBL_VALUE}</th>
												<th>{$mod.LBL_REMOVE}</th>
											</tr>
										</thead>
										<tbody id="approversHolder">
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<input type="submit" value="{$mod.LBL_SAVE}" style="margin-top: 20px;" />
	<input type="button" value="{$mod.LBL_CANCEL}" style="margin-left: 10px;" onclick='window.location.href="index.php?module=dtbc_Approvals&action=DetailView&record={$beanId}"' />
</form>


{literal}
<script>

var currentMax = 0;

function onTypeChange(elem) {
	var value = $(elem).val();	

	if (value == "user") {
		$('#users').show();
		$('#groups').hide();
	} else {
		$('#users').hide();
		$('#groups').show();
	}
};

function removeRow(elem) {
	if (confirm("{/literal}{$mod.LBL_REALLY_DELETE}{literal}") == true) {
		$(elem).closest('tr').remove();
	}
}

function addApproverLine(type, value, displayValue) {
	var $table = $("#approversHolder");
	
	var $typeHiddenInput = $("<input type='hidden' id='approverType[" + currentMax + "]' name='approverType[" + currentMax + "]' value='" + type + "'>");
	var $valueHiddenInput = $("<input type='hidden' id='approverValue[" + currentMax + "]' name='approverValue[" + currentMax + "]' value='" + value + "'>");
	
	var $row = $("<tr>");
	var $typeTd = $("<td>" + $('#type > option[value=' + type + ']').text() + "</td>");
	var $valueTd = $("<td>" + displayValue + "</td>");
	var $removeTd = $("<td><input type='button' value='{/literal}{$mod.LBL_REMOVE}{literal}' onclick='javascript:removeRow(this);' /></td>");
	
	$row.append($typeTd);
	$row.append($valueTd);
	$row.append($removeTd);
	$row.append($typeHiddenInput);
	$row.append($valueHiddenInput);
	
	$table.append($row);
	
	currentMax++;
}

function setupAndAddApproverLine() {
	var type = $("#type").val();
	var value = type == "user" ? $("#users").val() : $("#groups").val();
	var displayValue = type == "user" ? $('#users > option[value=' + value + ']').text() : $('#groups > option[value=' + value + ']').text();

	addApproverLine(type, value, displayValue);
};

$(function(){
	{/literal}
	
	{foreach from=$approvers item=approver}
		addApproverLine("{$approver.type}", "{$approver.value}", "{$approver.displayValue}");
	{/foreach}
	
	{literal}
});

</script>
{/literal}

