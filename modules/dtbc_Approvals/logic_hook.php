<?php

require_once('modules/dtbc_Approvals/ApprovalsHandler.php');

class ApprovalsHook {
	
	public function after_save(&$bean, $event, $arguments) {
		$handler = new ApprovalsHandler();
		$handler->handleAfterSave($bean);
	}
}