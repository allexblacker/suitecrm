<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('modules/dtbc_Approvals/ApprovalsHelper.php');

class dtbc_ApprovalsViewEdit extends ViewEdit
{
 	public function display()
 	{
		ApprovalsHelper::recreateEmailTemplateList();
		
		parent::display();
 	}
}