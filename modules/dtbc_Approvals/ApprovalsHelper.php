<?php

class ApprovalsHelper {
	
	public static $templateListName = "dtbc_approval_email_template_list";
	
	public static function recreateEmailTemplateList() {
		global $db, $current_user, $app_list_strings;
		
		unset ($app_list_strings[self::$templateListName]);
		$app_list_strings[self::$templateListName] = array();   
		
		$sql = 'select id, name from email_templates where deleted = 0 order by name ';
		
		$dataset = $db->query($sql);
		while ($row = $db->fetchByAssoc($dataset)) {
			$app_list_strings[self::$templateListName][$row['id']] = $row['name'];
		}
	}
	
	public static function getEmailTemplateList() {
		global $app_list_strings;
		return $app_list_strings[self::$templateListName];
	}
	
	public static function customRedirect($module, $action, $additionalDataArray = array()) {     
        $queryParams = array(
            'module' => $module,
            'action' => $action,           
        );
		
		foreach($additionalDataArray as $key => $value) {
			$queryParams[$key] = $value;
		}	

        SugarApplication::redirect('index.php?' . http_build_query($queryParams));
    }
}

class ApprovalsDbHelper {
	public static function insertQuery($tableName, $fields) {
		$db = DBManagerFactory::getInstance();
		
		$columns = implode(", ", array_keys($fields));
		$values = implode(", ", array_values($fields));

		$query = "INSERT INTO $tableName ($columns) VALUES ($values)";
		$db->query($query);
	}
	
	public static function updateQuery($tableName, $fields, $id) {
		$db = DBManagerFactory::getInstance();
		
		// Some can say this is hacky, but I think it is quite elegant :)
		$keysAndValues = urldecode(http_build_query($fields, '', ', '));

		$query = "UPDATE $tableName SET $keysAndValues WHERE id = " . $db->quoted($id);
		$db->query($query);
	}
	
	public static function deleteQuery($tableName, $fields) {
		$db = DBManagerFactory::getInstance();
		
		$keysAndValues = urldecode(http_build_query($fields, '', ' AND '));

		$query = "DELETE FROM $tableName WHERE $keysAndValues";
		$db->query($query);
	}
}

class ApprovalsParts {
    const entry = "entry";
    const initial = "initial";
    const step = "step";
    const approval = "approval";
    const rejection = "rejection";
    const recall = "recall";
}

class ApprovalsStates {
    const inProgress = "in_progress";
    const approved = "approved";
    const rejected = "rejected";
}

class ApprovalsResponses {
    const none = "none";
    const approved = "approved";
    const rejected = "rejected";
}

class ApprovalsMethods {
    const all = "all";
    const single = "single";
}
