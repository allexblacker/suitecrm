<?php

$mod_strings['LBL_AOW_WORKFLOW_ID'] = 'WorkFlow ID';
$mod_strings['LBL_CONDITION_OPERATOR'] = 'Bedingungs Operator';
$mod_strings['LBL_CREATED'] = 'Erstellt von';
$mod_strings['LBL_CREATED_ID'] = 'Erstellt von Id';
$mod_strings['LBL_CREATED_USER'] = 'Erstellt von Benutzer';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_FIELD'] = 'Feld';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_MODIFIED'] = 'Geändert von';
$mod_strings['LBL_MODIFIED_ID'] = 'Geändert von Id';
$mod_strings['LBL_MODIFIED_NAME'] = 'Geändert von Name';
$mod_strings['LBL_MODIFIED_USER'] = 'Geändert von Benutzer';
$mod_strings['LBL_MODULE_NAME'] = 'Workflow Bedingungen';
$mod_strings['LBL_MODULE_PATH'] = 'Modul';
$mod_strings['LBL_MODULE_TITLE'] = 'Workflow Bedingungen';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_ORDER'] = 'Reihenfolge';
$mod_strings['LBL_VALUE'] = 'Wert';
$mod_strings['LBL_VALUE_TYPE'] = 'Typ';
