<?php

$mod_strings['LBL_ACCESS_DEFAULT'] = 'Standard';
$mod_strings['LBL_ACTION_ADMIN'] = 'Zugriffstyp';
$mod_strings['LBL_ALL'] = 'Alle';
$mod_strings['LBL_CREATE_ROLE'] = 'Neue Rolle';
$mod_strings['LBL_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_DUPLICATE_OF'] = 'Duplikat von';
$mod_strings['LBL_EDIT_VIEW_DIRECTIONS'] = 'Zum Ändern des Wertes die Zelle doppelklicken.';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Rollen';
$mod_strings['LBL_MODULE_NAME'] = 'Rollen';
$mod_strings['LBL_MODULE_TITLE'] = 'Rollen: Startseite';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_ROLE'] = 'Rolle';
$mod_strings['LBL_ROLES_SUBPANEL_TITLE'] = 'Benutzerrollen';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Suchen';
$mod_strings['LBL_SECURITYGROUPS'] = 'Berechtigungsgruppen';
$mod_strings['LBL_USERS_SUBPANEL_TITLE'] = 'Benutzer';
$mod_strings['LIST_ROLES'] = 'Liste Rollen';
$mod_strings['LIST_ROLES_BY_USER'] = 'Rollen nach Benutzern auflisten';
