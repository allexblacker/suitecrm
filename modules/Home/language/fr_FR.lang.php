<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Accueil',
    'LBL_MODULES_TO_SEARCH' => 'Modules utilisés pour la recherche',
    'LBL_NEW_FORM_TITLE' => 'Nouveau Contact',
    'LBL_FIRST_NAME' => 'Prénom :',
    'LBL_LAST_NAME' => 'Nom de famille :',
    'LBL_LIST_LAST_NAME' => 'Nom',
    'LBL_PHONE' => 'Téléphone :',
    'LBL_EMAIL_ADDRESS' => 'Adresse E-mail :',
    'LBL_MY_PIPELINE_FORM_TITLE' => 'Mon Portefeuille',
    'LBL_PIPELINE_FORM_TITLE' => 'Mon Portefeuille',
    'LBL_RGraph_PIPELINE_FORM_TITLE' => 'Portefeuille par phase de vente',
    'LBL_CAMPAIGN_ROI_FORM_TITLE' => 'ROI de Campagne',
    'LNK_NEW_CONTACT' => 'Créer Contact',
    'LNK_NEW_ACCOUNT' => 'Créer Compte',
    'LNK_NEW_OPPORTUNITY' => 'Créer Affaire',
    'LNK_NEW_LEAD' => 'Créer un prospect',
    'LNK_NEW_CASE' => 'Créer Ticket',
    'LNK_NEW_NOTE' => 'Créer Note',
    'LNK_NEW_CALL' => 'Planifier Appel',
    'LNK_NEW_EMAIL' => 'Archiver E-mail',
    'LNK_COMPOSE_EMAIL' => 'Envoyer E-mail',
    'LNK_NEW_MEETING' => 'Planifier Réunion',
    'LNK_NEW_TASK' => 'Créer Tâche',
    'LNK_NEW_BUG' => 'Signaler Bug',
    'LBL_ADD_BUSINESSCARD' => 'Créer Carte de Visite',
    'LBL_OPEN_TASKS' => 'Mes Tâches ouvertes',
    'LBL_SEARCH_RESULTS_IN' => 'dans',
    'LNK_NEW_SEND_EMAIL' => 'Envoyer E-mail',
    'LBL_NO_ACCESS' => 'Vous n&#39;avez pas accès à cet espace. Veuillez contacter votre administrateur pour en connaître la raison.',
    'LBL_NO_RESULTS_IN_MODULE' => '-- Pas de résultats --',
    'LBL_NO_RESULTS' => '<h2>Aucun résultat trouvé. Veuillez effectuer une nouvelle recherche.</h2><br>',
    'LBL_NO_RESULTS_TIPS' => '<h3>Aide à la recherche :</h3><ul><li>Assurez vous d&#39;avoir sélectionnées les catégories appropriées ci-dessus.</li><li>Élargissez vos critères de recherche.</li><li>Si vous ne trouvez aucun résultat, essayez la recherche avancée du module.</li></ul>',

    'LBL_ADD_DASHLETS' => 'Ajouter des Dashlets',
    'LBL_ADD_PAGE' => 'Ajouter une page',
    'LBL_DEL_PAGE' => 'Supprimer une Page',
    'LBL_WEBSITE_TITLE' => 'Site web',
    'LBL_RSS_TITLE' => 'News RSS',
    'LBL_DELETE_PAGE' => 'Supprimer une page',
    'LBL_CHANGE_LAYOUT' => 'Changer la disposition',
    'LBL_RENAME_PAGE' => 'Renommer une page',
    'LBL_CLOSE_DASHLETS' => 'Fermer',
    'LBL_OPTIONS' => 'Options',
    // dashlet search fields
    'LBL_TODAY' => 'Aujourd&#39;hui',
    'LBL_YESTERDAY' => 'Hier',
    'LBL_TOMORROW' => 'Demain',
    'LBL_LAST_WEEK' => 'La semaine dernière',
    'LBL_NEXT_WEEK' => 'La semaine prochaine',
    'LBL_LAST_7_DAYS' => 'Dans les 7 derniers jours',
    'LBL_NEXT_7_DAYS' => 'Dans les 7 prochains jours',
    'LBL_LAST_MONTH' => 'Le mois dernier',
    'LBL_NEXT_MONTH' => 'Le mois prochain',
    'LBL_LAST_QUARTER' => 'Le trimestre dernier',
    'LBL_THIS_QUARTER' => 'Ce trimestre',
    'LBL_LAST_YEAR' => 'L&#39;année dernière',
    'LBL_NEXT_YEAR' => 'L&#39;année prochaine',
    'LBL_LAST_30_DAYS' => 'Dans les 30 derniers jours',
    'LBL_NEXT_30_DAYS' => 'Dans les 30 prochains jours',
    'LBL_THIS_MONTH' => 'Ce Mois-ci',
    'LBL_THIS_YEAR' => 'Cette Année',

    'LBL_MODULES' => 'Modules',
    'LBL_CHARTS' => 'Graphiques',
    'LBL_TOOLS' => 'Outils',
    'LBL_WEB' => 'Web',
    'LBL_SEARCH_RESULTS' => 'Résultats de la Recherche',

    // Dashlet Categories
    'dashlet_categories_dom' => array(
        'Module Views' => 'Vue Modules',
        'Portal' => 'Portail',
        'Charts' => 'Graphiques',
        'Tools' => 'Outils',
        'Miscellaneous' => 'Divers'
    ),
    'LBL_ADDING_DASHLET' => 'Ajout de Dashlet ...',
    'LBL_ADDED_DASHLET' => 'Dashlet ajouté',
    'LBL_REMOVE_DASHLET_CONFIRM' => 'Etes-vous certain de vouloir supprimer le Dashlet ?',
    'LBL_REMOVING_DASHLET' => 'Suppression du Dashlet ...',
    'LBL_REMOVED_DASHLET' => 'Dashlet supprimé',
    'LBL_DASHLET_CONFIGURE_GENERAL' => 'Général',
    'LBL_DASHLET_CONFIGURE_FILTERS' => 'Filtres',
    'LBL_DASHLET_CONFIGURE_MY_ITEMS_ONLY' => 'Mes Eléments seulement',
    'LBL_DASHLET_CONFIGURE_TITLE' => 'Titre',
    'LBL_DASHLET_CONFIGURE_DISPLAY_ROWS' => 'Afficher les lignes',

    'LBL_DASHLET_DELETE' => 'Supprimer le Dashlet',
    'LBL_DASHLET_REFRESH' => 'Rafraîchir le Dashlet',
    'LBL_DASHLET_EDIT' => 'Editer le Dashlet',

    // Default out-of-box names for tabs
    'LBL_HOME_PAGE_1_NAME' => 'Mon CRM',
    'LBL_CLOSE_SITEMAP' => 'Fermer',

    'LBL_SEARCH' => 'Rechercher',
    'LBL_CLEAR' => 'Effacer',

    'LBL_BASIC_CHARTS' => 'Graphique basique',

    'LBL_DASHLET_SEARCH' => 'Recherche de Dashlets',

//ABOUT page
    'LBL_VERSION' => 'Version',
    'LBL_BUILD' => 'version de construction',

    'LBL_SOURCE_SUGAR' => 'SugarCRM Inc - fournisseurs de la plateforme CE',
    'LBL_SOURCE_XTEMPLATE' => 'XTemplate - Un moteur de mise en page pour PHP créé par Barnabás Debreceni',
    'LBL_SOURCE_NUSOAP' => 'NuSOAP - Un ensemble de classe PHP développées pour créer et utiliser des services web, créée par NuSphere Corporation and Dietrich Ayala',
    'LBL_SOURCE_JSCALENDAR' => 'JS Calendar - Un calendrier pour enregistrer des dates créé par Mihai Bazon',
    'LBL_SOURCE_PHPPDF' => 'PHP PDF - Une librairie pour créer des documents PDF créé par Wayne Munro',
    'LBL_SOURCE_HTTP_WEBDAV_SERVER' => 'HTTP_WebDAV_Server - Une implémentation de serveur WebDAV en PHP.',
    'LBL_SOURCE_PCLZIP' => 'PclZip - librairie qui permet la compression et l&#39;extraction de fichier Zip par Vincent Blavet',
    'LBL_SOURCE_SMARTY' => 'Smarty - Un moteur de template en PHP.',
    'LBL_SOURCE_YAHOO_UI_LIB' => 'Yahoo! User Interface Library - La librairie interface utilisateur pour faciliter l&#39;implentation de clients rîches.',
    'LBL_SOURCE_PHPMAILER' => 'PHPMailer - Une classe de transfert de mail en PHP',
    'LBL_SOURCE_JSHRINK' => 'JShrink - Un "minifier" Javascript écrit en PHP',
    'LBL_SOURCE_CRYPT_BLOWFISH' => 'Crypt_Blowfish - Permet le code en blowfish même si l&#39;extension MCrypt n&#39;est pas installée.',
    'LBL_SOURCE_XML_HTMLSAX3' => 'XML_HTMLSax3 - Un parseur SAX pour HTML et autres documents XML',
    'LBL_SOURCE_YAHOO_UI_LIB_EXT' => 'Yahoo! UI Extensions Library - Extensions pour Yahoo! User Interface Library de Jack Slocum',
    'LBL_SOURCE_SWFOBJECT' => 'SWFObject - Javascript Flash Player detection.',
    'LBL_SOURCE_TINYMCE' => 'TinyMCE - Un éditeur WYSIWYG pour éditer le contenu HTML',
    'LBL_SOURCE_EXT' => 'Ext - Un framework de client Javascript pour la création d&#39;application web.',
    'LBL_SOURCE_RECAPTCHA' => 'reCAPTCHA permet de prévenir des abus sur votre site (comme les commentaires de type SPAM ou enregistrements falacieux) en utilisant un CAPTCHA pour assurer que seuls des humains effectuent certaines actions.',
    'LBL_SOURCE_TCPDF' => 'TCPDF - Une classe PHP pour générer des documents PDF.',
    'LBL_SOURCE_CSSMIN' => 'CssMin - Un parser/analyseur et minifier/réducteur de CSS.',
    'LBL_SOURCE_PHPSAML' => 'PHP-SAML - Une boute à outils SAML pour PHP.',
    'LBL_SOURCE_ISCROLL' => 'iScroll - Gestion du paramètre overflow:scroll pour les mobiles. Permet de gérer le défilement natif pour les éléments à hauteur/largeur fixe.',
    'LBL_SOURCE_FLASHCANVAS' => 'FlashCanvas - FlashCanvas est une librairie Javascript qui ajoute le support de la balise HTML5 "canvas" à Internet Explorer. Elle permet le support des formes et des images au travers de l&#39;API flash. Elle supporte toutes les APIs Canvas et, dans certain cas, accélère le rendu par rapport aux autres librairies qui utilisent VML ou Silverlight.',
    'LBL_SOURCE_JIT' => 'JavaScript InfoVis Toolkit - L&#39;environnement javscript InfoVis Toolkit fournit des outils de création interactif pour la visualisation de données.',
    'LBL_SOURCE_ZEND' => 'Zend Framework - Un framework open source PHP5 orienté objet pour les applications web.',
    'LBL_SOURCE_PARSECSV' => 'parseCSV - Parseur de données CSV pour PHP',
    'LBL_SOURCE_PHPJS' => 'php.js - Fonctions PHP portées en JavaScript',
    'LBL_SOURCE_PHPSQL' => 'Parseur SQL en PHP',
    'LBL_SOURCE_HTMLPURIFIER' => 'HTML Purifier - Un librairie de filtrage respectant les standards HTML.',
    'LBL_SOURCE_XHPROF' => 'XHProf - Un profileur hiérarchique au niveau fonction pour PHP',
    'LBL_SOURCE_ELASTICA' => 'Elastica - Client PHP pour le moteur de recherche distribué elasticsearch',
    'LBL_SOURCE_FACEBOOKSDK' => 'Facebook PHP SDK',
    'LBL_SOURCE_JQUERY' => 'jQuery - jQuery est une bibliothèque JavaScript rapide et concise qui simplifie la manipulation des documents HTML, la gestion des évènements, l&#39;animation et les interactions Ajax pour le développement rapide d&#39;applications web.',
    'LBL_SOURCE_JQUERY_UI' => 'jQuery UI - jQuery UI est une librairie basée sur JQuery et fournissant des thèmes, widgets et effet pour les interfaces utilisateurs.',
    'LBL_SOURCE_OVERLIB' => 'OverlibMWS - La librairie overlibmws library utilise javascript pour les popups DHTML.',

  'LBL_DASHLET_TITLE' => 'Mes sites',
  'LBL_DASHLET_OPT_TITLE' => 'Titre',
  'LBL_DASHLET_INCORRECT_URL' => 'URL incorrecte',
  'LBL_DASHLET_OPT_URL' => 'URL',
  'LBL_DASHLET_OPT_HEIGHT' => 'Hauteur du Dashlet (en pixels)',
  'LBL_DASHLET_SUGAR_NEWS' => 'SuiteCRM Actualités',
  'LBL_DASHLET_DISCOVER_SUGAR_PRO' => 'Découvrir SuiteCRM',
	'LBL_POWERED_BY_SUGAR' => 'Propulsé par SugarCRM' /*for 508 compliance fix*/,
	'LBL_MORE_DETAIL' => 'Plus de détails' /*for 508 compliance fix*/,
	'LBL_BASIC_SEARCH' => 'Filtre rapide' /*for 508 compliance fix*/,
	'LBL_ADVANCED_SEARCH' => 'Filtre avancé' /*for 508 compliance fix*/,
    'LBL_TOUR_HOME' => 'Icône Accueil',
    'LBL_TOUR_HOME_DESCRIPTION' => 'Retour rapide à l\'accueil du tableau de bord en un seul clic.',
    'LBL_TOUR_MODULES' => 'Modules',
    'LBL_TOUR_MODULES_DESCRIPTION' => 'Tous vos modules importants sont ici.',
    'LBL_TOUR_MORE' => 'Plus de modules',
    'LBL_TOUR_MORE_DESCRIPTION' => 'Le reste des modules est ici.',
    'LBL_TOUR_SEARCH' => 'Recherche en plein texte (Full Text)',
    'LBL_TOUR_SEARCH_DESCRIPTION' => 'Les recherches précises fournissent de meilleurs résultats.',
    'LBL_TOUR_NOTIFICATIONS' => 'Notifications',
    'LBL_TOUR_NOTIFICATIONS_DESCRIPTION' => 'Notifications de SuiteCRM iraient ici.',
    'LBL_TOUR_PROFILE' => 'Profil',
    'LBL_TOUR_PROFILE_DESCRIPTION' => 'Profil, reglages et deconnexion.',
    'LBL_TOUR_QUICKCREATE' => 'Création rapide',
    'LBL_TOUR_QUICKCREATE_DESCRIPTION' => 'Créer rapidement des documents sans changer d\'ecran.',
    'LBL_TOUR_FOOTER' => 'Pied de page dépliable. ',
    'LBL_TOUR_FOOTER_DESCRIPTION' => 'Déplier ou replier le pied de page facilement.',
    'LBL_TOUR_CUSTOM' => 'Applications personnalisées',
    'LBL_TOUR_CUSTOM_DESCRIPTION' => 'Les intégrations personnalisées se trouveront ici.',
    'LBL_TOUR_BRAND' => 'Votre marque',
    'LBL_TOUR_BRAND_DESCRIPTION' => 'Votre logo se place ici.Survolez avec la souris pour plus d\'informations.',
    'LBL_TOUR_WELCOME' => 'Bienvenue dans SuiteCRM',
    'LBL_TOUR_WATCH' => 'Regardez les nouveautés de SuiteCRM',
    'LBL_TOUR_FEATURES' => '<ul style=""><li class="icon-ok">Nouvelle barre de navigation</li><li class="icon-ok">Nouveau pied de page dépliable</li><li class="icon-ok">Recherche améliorée</li><li class="icon-ok">Nouveau menu actions</li></ul><p>et beaucoup plus !</p>',
    'LBL_TOUR_VISIT' => 'Pour plus d\'informations, merci de visiter notre application.',
    'LBL_TOUR_DONE' => 'C\'est fait !',
    'LBL_TOUR_REFERENCE_1' => 'Vous pouvez toujours faire référence à notre',
    'LBL_TOUR_REFERENCE_2' => 'à l\'aide du "Lien Support" dans l\'onglet profil.',
    'LNK_TOUR_DOCUMENTATION' => 'documentation',
    'LBL_TOUR_CALENDAR_URL_1' => 'Voulez-vous partager votre calendrier SuiteCRM avec d\'autres applications, tels que Microsoft Outlook ou Exchange? Si oui, vous avez une nouvelle URL, plus sécurisé, qui comprend une clé personnelle pour éviter la publication non autorisée de votre calendrier.',
    'LBL_TOUR_CALENDAR_URL_2' => 'Récupérer votre nouvelle URL de calendrier partagé.',
    'LBL_ABOUT' => 'A propos',
    'LBL_CONTRIBUTORS' => 'Contributeurs',
    'LBL_ABOUT_SUITE' => 'A propos de SuiteCRM',
    'LBL_PARTNERS' => 'Partenaires',
    'LBL_FEATURING' => 'Modules AOS, AOW, AOR, AOP, AOE et replanification par SalesAgility.',

    'LBL_CONTRIBUTOR_SUITECRM' => 'SuiteCRM - CRM Open Source pour le monde',
    'LBL_CONTRIBUTOR_SECURITY_SUITE' => 'SecuritySuite par Jason Eggers',
    'LBL_CONTRIBUTOR_JJW_GMAPS' => 'JJWDesign Google Maps par Jeffrey J. Walters',
    'LBL_CONTRIBUTOR_CONSCIOUS' => 'Logo SuiteCRM fourni par Conscious Solutions',
    'LBL_CONTRIBUTOR_RESPONSETAP' => 'Contribution à la version 7.3 de SuiteCRM par ResponseTap',
    'LBL_CONTRIBUTOR_GMBH' => 'Workflow Calculated Fields contributed by diligent technology & business consulting GmbH',

    'LBL_LANGUAGE_ABOUT' => 'Au sujet des traductions SuiteCRM',
    'LBL_LANGUAGE_COMMUNITY_ABOUT' => 'Traduction collaborative par la communauté de SuiteCRM',
    'LBL_LANGUAGE_COMMUNITY_PACKS' => 'Traductions créées à l’aide de Crowdin',

    'LBL_ABOUT_SUITE_2' => 'SuiteCRM est publié sous une licence Open Source - GPL3',
    'LBL_ABOUT_SUITE_4' => 'Tout le code de SuiteCRM, géré et développé par ce projet, seras publié sous licence Open Source - GPL3',
    'LBL_ABOUT_SUITE_5' => 'L\'assistance sur SuiteCRM est disponible gratuitement ou par des options payantes',

    'LBL_SUITE_PARTNERS' => 'Nous avons des partenaires loyaux et passionnés d\'Open Source. Retrouvez la liste complète de ces partenaires sur notre site internet.',

);
