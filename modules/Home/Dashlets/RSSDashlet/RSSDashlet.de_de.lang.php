<?php

$dashletStrings['RSSDashlet']['ERR_LOADING_FEED'] = 'Laden des RSS Feed fehlgeschlagen';
$dashletStrings['RSSDashlet']['LBL_AUTO_SCROLL'] = 'Automatisches Scrollen';
$dashletStrings['RSSDashlet']['LBL_CONFIGURE_HEIGHT'] = 'Höhe (1 - 300)';
$dashletStrings['RSSDashlet']['LBL_CONFIGURE_RSSURL'] = 'RSS URL';
$dashletStrings['RSSDashlet']['LBL_CONFIGURE_TITLE'] = 'Titel';
$dashletStrings['RSSDashlet']['LBL_DBLCLICK_HELP'] = '';
$dashletStrings['RSSDashlet']['LBL_DESCRIPTION'] = 'News Feed';
$dashletStrings['RSSDashlet']['LBL_SAVED'] = 'Fertig';
$dashletStrings['RSSDashlet']['LBL_SAVING'] = 'Analysieren...';
$dashletStrings['RSSDashlet']['LBL_SCROLL_SPEED'] = 'Scrollgeschwindigkeit (%)';
$dashletStrings['RSSDashlet']['LBL_TITLE'] = 'News Feed';
