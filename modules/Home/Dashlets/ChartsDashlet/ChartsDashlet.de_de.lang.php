<?php

$dashletStrings['ChartsDashlet']['LBL_CONFIGURE_TITLE'] = 'Titel';
$dashletStrings['ChartsDashlet']['LBL_DESCRIPTION'] = 'Ein Dashlet, um Diagramme anzuzeigen';
$dashletStrings['ChartsDashlet']['LBL_TITLE'] = 'Diagramme';
