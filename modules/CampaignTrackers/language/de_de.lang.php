<?php

$mod_strings['LBL_CAMPAIGN'] = 'Kampagne';
$mod_strings['LBL_CAMPAIGN_ID'] = 'Kampagnen ID';
$mod_strings['LBL_CREATED_BY'] = 'Erstellt von';
$mod_strings['LBL_DATE_ENTERED'] = 'Erstellungsdatum';
$mod_strings['LBL_DATE_MODIFIED'] = 'Änderungsdatum';
$mod_strings['LBL_DELETED'] = 'Gelöscht';
$mod_strings['LBL_EDIT_CAMPAIGN_NAME'] = 'Kampagnenname:';
$mod_strings['LBL_EDIT_LAYOUT'] = 'Layout bearbeiten';
$mod_strings['LBL_EDIT_MESSAGE_URL'] = 'URL für Kampagnenmitteilung:';
$mod_strings['LBL_EDIT_OPT_OUT'] = 'Abmelde Link?';
$mod_strings['LBL_EDIT_TRACKER_KEY'] = 'Tracker-Schlüssel:';
$mod_strings['LBL_EDIT_TRACKER_NAME'] = 'Tracker-Name:';
$mod_strings['LBL_EDIT_TRACKER_URL'] = 'Tracker-URL:';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_MODIFIED_USER_ID'] = 'Geänderte Benutzer ID';
$mod_strings['LBL_MODULE_NAME'] = 'Kampagnen Tracker';
$mod_strings['LBL_OPTOUT'] = 'Abmelden';
$mod_strings['LBL_SUBPANEL_TRACKER_KEY'] = 'Schlüssel';
$mod_strings['LBL_SUBPANEL_TRACKER_NAME'] = 'Name';
$mod_strings['LBL_SUBPANEL_TRACKER_URL'] = 'URL';
$mod_strings['LBL_TRACKER_KEY'] = 'Tracker-Schlüssel';
$mod_strings['LBL_TRACKER_NAME'] = 'Tracker-Name';
$mod_strings['LBL_TRACKER_URL'] = 'Tracker-URL';
$mod_strings['LNK_CAMPAIGN_LIST'] = 'Kampagnen';
