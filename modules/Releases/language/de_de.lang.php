<?php

$mod_strings['ERR_DELETE_RECORD'] = 'Um diesen Datensatz zu löschen, muss eine Datensatznummer angegeben werden';
$mod_strings['LBL_EDITLAYOUT'] = 'Layout bearbeiten';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Freigabeliste';
$mod_strings['LBL_LIST_LIST_ORDER'] = 'Bestellung';
$mod_strings['LBL_LIST_NAME'] = 'Freigabename';
$mod_strings['LBL_LIST_ORDER'] = 'Bestellung:';
$mod_strings['LBL_LIST_STATUS'] = 'Status';
$mod_strings['LBL_MODULE_NAME'] = 'Freigaben ';
$mod_strings['LBL_MODULE_TITLE'] = 'Freigaben: Startseite';
$mod_strings['LBL_NAME'] = 'Freigabeversion';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Neue Freigabe';
$mod_strings['LBL_RELEASE'] = 'Freigabe:';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Freigabe suchen';
$mod_strings['LBL_STATUS'] = 'Status:';
$mod_strings['LNK_NEW_RELEASE'] = 'Freigabeliste';
$mod_strings['NTC_DELETE_CONFIRMATION'] = 'Sind Sie sicher, dass Sie diesen Datensatz löschen wollen?';
$mod_strings['NTC_LIST_ORDER'] = 'Legen Sie den Rang fest, auf dem diese Version in der Auswahlliste eingetragen werden soll';
$mod_strings['NTC_STATUS'] = 'Setzen Sie den Status auf inaktiv, um diese Freigabe aus der Auswahlliste zu löschen.';
$mod_strings['release_status_dom']['Active'] = 'Aktiv';
$mod_strings['release_status_dom']['Inactive'] = 'Inaktiv';
