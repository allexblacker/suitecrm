<?php

// Check newrelic extension
if (extension_loaded('newrelic'))
{
	//Add more specific name for a transaction
	if (hasRequestParameterNR('module') && hasRequestParameterNR('action'))
	{
		newrelic_name_transaction('/' . getRequestParameterNR('module') . '/' . getRequestParameterNR('action') . '/');
	}

	//Add record as a custom attribute
	if (hasRequestParameterNR('record'))
	{
		newrelic_add_custom_parameter('record', getRequestParameterNR('record'));
	}
}

function hasRequestParameterNR($keyName)
{
	return isset($_REQUEST[$keyName]) && !empty($_REQUEST[$keyName]);
}

function getRequestParameterNR($keyName)
{
	return $_REQUEST[$keyName];
}