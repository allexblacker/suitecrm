<?php
$module_name = 'emqu_dtbc_Email_Queue';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'status_c',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        1 => 
        array (
          0 => 'date_entered',
          1 => 'date_modified',
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'email_subject_c',
            'label' => 'LBL_EMAIL_SUBJECT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'from_name_c',
            'label' => 'LBL_FROM_NAME',
          ),
          1 => 
          array (
            'name' => 'from_address_c',
            'label' => 'LBL_FROM_ADDRESS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'to_address_c',
            'studio' => 'visible',
            'label' => 'LBL_TO_ADDRESS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'cc_address_c',
            'studio' => 'visible',
            'label' => 'LBL_CC_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'bcc_address_c',
            'studio' => 'visible',
            'label' => 'LBL_BCC_ADDRESS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'email_content_html_c',
            'studio' => 'visible',
            'label' => 'LBL_EMAIL_CONTENT_HTML',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'email_content_c',
            'studio' => 'visible',
            'label' => 'LBL_EMAIL_CONTENT',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'email_template_id_c',
            'label' => 'LBL_EMAIL_TEMPLATE_ID',
          ),
          1 => 
          array (
            'name' => 'aow_workflow_id_c',
            'label' => 'LBL_AOW_WORKFLOW_ID',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'related_bean_type_c',
            'label' => 'LBL_RELATED_BEAN_TYPE',
          ),
          1 => 
          array (
            'name' => 'related_bean_id_c',
            'label' => 'LBL_RELATED_BEAN_ID',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'smtp_config_key_c',
            'label' => 'LBL_SMTP_CONFIG_KEY',
          ),
          1 => 
          array (
            'name' => 'failer_counter_c',
            'label' => 'LBL_FAILER_COUNTER',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'next_delivery_c',
            'label' => 'LBL_NEXT_DELIVERY',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
