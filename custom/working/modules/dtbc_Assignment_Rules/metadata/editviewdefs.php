<?php
$module_name = 'dtbc_Assignment_Rules';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'rule_order_c',
            'label' => 'LBL_RULE_ORDER',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'criteria_c',
            'studio' => 'visible',
            'label' => 'LBL_CRITERIA',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'filter_logic_c',
            'label' => 'LBL_FILTER_LOGIC',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assign_to_user_c',
            'studio' => 'visible',
            'label' => 'LBL_ASSIGN_TO_USER',
          ),
          1 => 
          array (
            'name' => 'assign_to_group_c',
            'studio' => 'visible',
            'label' => 'LBL_ASSIGN_TO_GROUP',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'do_not_reassign_owner_c',
            'label' => 'LBL_DO_NOT_REASSIGN_OWNER',
          ),
        ),
      ),
    ),
  ),
);
?>
