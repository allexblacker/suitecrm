<?php
// created: 2017-11-01 02:12:01
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["os1_outside_sales_activity_tasks_1"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1',
  'type' => 'link',
  'relationship' => 'os1_outside_sales_activity_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_TASKS_TITLE',
);
