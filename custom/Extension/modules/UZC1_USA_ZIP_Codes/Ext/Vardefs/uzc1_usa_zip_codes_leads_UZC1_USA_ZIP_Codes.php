<?php
// created: 2017-08-31 12:53:12
$dictionary["UZC1_USA_ZIP_Codes"]["fields"]["uzc1_usa_zip_codes_leads"] = array (
  'name' => 'uzc1_usa_zip_codes_leads',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_leads',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_LEADS_FROM_LEADS_TITLE',
);
