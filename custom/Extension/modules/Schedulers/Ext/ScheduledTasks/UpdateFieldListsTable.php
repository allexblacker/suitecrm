<?php


$job_strings['UpdateFieldListsTable'] = 'custom_sol_cron_job';



function custom_sol_cron_job(){
    require_once 'custom/include/customScheduledTasks.php';
    require_once 'modules/ModuleBuilder/views/view.modulefields.php';

    global $db;
    global $dictionary;
    global $app_list_strings;
    global $moduleList; //the list of modules;

    foreach($moduleList as $module){ //iterating through the list of modules
       $bean = BeanFactory::newBean($module); //creating a bean
       if(!empty($bean)){
            $fieldDefinitons = $bean->getFieldDefinitions(); //getting field definitions of a bean
            //echo "<pre>";
            //print_r($fieldDefinitons);
            //echo "</pre>";
            foreach($fieldDefinitons as $key => $value){
                if(($value['type'] == 'enum' || $value['type'] == 'dynamicenum') && $value['source'] != 'non-db'){     //if the field is an enum (dropdown), add to the enums array, exlude non-db fields!
                    $fieldname = $key;
                    $listname = $value['options'];
                    $list = array();
                    if (isset($app_list_strings[$listname]))
                    {
                        $list = $app_list_strings[$listname];
                        foreach ($list as $key => $value)
                        {
                            //find if record exist
                            if($value != '')
                            {
                                $sql = "SELECT kob_value FROM kob_fieldlist WHERE kob_fieldname = '" . $fieldname . "' AND kob_key = '" . $key . "'";
                                //echo $sql . "<BR/>";
                                $result = $db->query($sql);
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) {
                                        foreach($row as $k => $v)
                                        {
                                           //echo $k . " - " . $v . "<BR/>";
                                           if($v != $value)
                                           {
                                               $sql = "UPDATE kob_fieldlist SET kob_value = '" . $value . "' WHERE kob_fieldname = '" . $fieldname . "' AND kob_key = '" . $key . "'";
                                           }
                                        }
                                    }
                                }
                                else {
                                    $sql = "INSERT INTO kob_fieldlist(idkob_fieldlist, kob_fieldname, kob_listname, kob_key, kob_value)
                                            VALUES (UUID(),'" . $fieldname . "','" . $listname . "','" . $key . "','" . $value . "')";
                                    $db->query($sql);
                                    //echo $sql . "<BR/>";
                                    //echo '$module' . $module . 'Field Name: ' . $fieldname . 'List Name: ' . $listname . 'Key: ' . $key .'Value: ' . $value . '<BR/>';
                                }
                            }
                        }
                    }
                }
            }
       }
    }
    return true; // Remember to return the result of successful execution whether this be from a function or not.
    ///return my_custom_function_from_my_included_file();
}
