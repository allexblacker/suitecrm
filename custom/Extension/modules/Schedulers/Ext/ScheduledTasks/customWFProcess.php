<?php

array_push($job_strings, 'custom_workflow_process');

function custom_workflow_process($schedulersJobObj) {
	require_once('custom/modules/AOW_WorkFlow/AOW_WorkFlow.php');
	$_REQUEST['module'] = 'Schedulers';
	$_REQUEST['record'] = $schedulersJobObj->scheduler_id;
    $workflow = new CustomAOW_WorkFlow();
    return $workflow->run_flows();
}

