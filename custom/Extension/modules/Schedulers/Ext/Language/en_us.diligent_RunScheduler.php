<?php

$mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_NOT_FOUND'] = 'Scheduler not found!';
$mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_IS_ALREADY_IN_QUEUE'] = 'Scheduler is already in the job queue! Status: ';
$mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_RUNNED_BY_AN_ANOTHER_PROCESS'] = 'Scheduler is runned by an another process!';
$mod_strings['LBL_RUN_SCHEDULER_JOB_DONE'] = 'Job done';
$mod_strings['LBL_RUN_SCHEDULER_MENU'] = 'Run Scheduler';
