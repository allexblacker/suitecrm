<?php
// created: 2017-10-04 15:26:52
$dictionary["dtbc_Contact_Roles"]["fields"]["contacts_dtbc_contact_roles_1"] = array (
  'name' => 'contacts_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_dtbc_contact_roles_1contacts_ida',
);
$dictionary["dtbc_Contact_Roles"]["fields"]["contacts_dtbc_contact_roles_1_name"] = array (
  'name' => 'contacts_dtbc_contact_roles_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_dtbc_contact_roles_1contacts_ida',
  'link' => 'contacts_dtbc_contact_roles_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["dtbc_Contact_Roles"]["fields"]["contacts_dtbc_contact_roles_1contacts_ida"] = array (
  'name' => 'contacts_dtbc_contact_roles_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_contact_roles_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);
