<?php

$hook_array['after_relationship_add'][] = array(
	10,
	'Automatically fill acase_id field for SolarEdge team',
	'custom/include/dtbc/hooks/caseRelationShips.php',
	'HandleRelationShips',
	'after_relationship_add'
);

$hook_array['after_relationship_delete'][] = array(
	10,
	'Automatically remove acase_id field\'s value for SolarEdge team',
	'custom/include/dtbc/hooks/caseRelationShips.php',
	'HandleRelationShips',
	'after_relationship_delete'
);

$hook_array['before_relationship_delete'][] = array(
    10,
    'Set AOP_Case_Updates deleted flag to 1',
    'custom/include/dtbc/hooks/caseRelationShips.php',
    'HandleRelationShips',
    'before_relationship_delete'
);