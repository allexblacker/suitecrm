<?php

$hook_array['after_save'][] = array(
	65,
	'Setting future variables to the bean on order to appear in email templates',
	'custom/include/dtbc/hooks/65_future_variables_for_email_templates.php',
	'FutureVariables',
	'after_save'
);