<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	10,
	'Remove unnecessary line breaks',
	'custom/include/dtbc/hooks/removeLineBreaksFromCaseDescription.php',
	'RemoveLineBreaks',
	'before_save'
);