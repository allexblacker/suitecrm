<?php

$hook_array['after_save'][] = array(
	30,
	'When "Notify Customer Date" is entered then the Contact will automatically receive an email with the Notify Email Template.',
	'custom/include/dtbc/notifycustomerdate.php',
	'NotifyCustomerDate',
	'after_save'
);