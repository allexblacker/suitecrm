<?php

$hook_array['after_save'][] = array(
	40,
	'Run only for compensation cases - set status and send email',
	'custom/include/dtbc/compensationcase.php',
	'CompensationCase',
	'after_save'
);

$hook_array['after_save'][] = array(
	50,
	'When changing an existing case "Case Record Type" field to "Compensation", the case must be auto-assigned to "Security Group: Compensation"',
	'custom/include/dtbc/compensationcase.php',
	'CompensationCase',
	'after_save_group'
);