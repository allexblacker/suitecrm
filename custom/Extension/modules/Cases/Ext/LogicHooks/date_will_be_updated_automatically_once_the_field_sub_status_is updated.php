<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	100,
	'DATE will be updated automatically once the field sub status is updated workflow',
	'custom/include/dtbc/hooks/workflow_date_will_be_updated_automatically_once_the_field_sub_status_is_updated.php',
	'SubStatudDateUpdater',
	'before_save'
);