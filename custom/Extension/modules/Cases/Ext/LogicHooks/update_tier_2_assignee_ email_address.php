<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	100,
	'Updating tier 2 assignees email address',
	'custom/include/dtbc/hooks/update_tier_2_assignee_email_address.php',
	'Tier2AssigneeEmailAddressUpdater',
	'before_save'
);