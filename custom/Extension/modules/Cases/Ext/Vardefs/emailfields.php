<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['email_serial_numbers'] = array(
	'name' => 'email_serial_numbers',
	'label' => 'LBL_POINTS_TOTAL',
	'vname' => 'LBL_POINTS_TOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_serial_numbers',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_serial_numbers.php',
	), 
);

$dictionary['Case']['fields']['doa_email_template_table_c'] = array(
	'name' => 'doa_email_template_table_c',
	'label' => 'LBL_DOA_TEMPLATE_TOTAL',
	'vname' => 'LBL_DOA_TEMPLATE_TOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_doa_table',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_doa_table_in_emails.php',
	), 
);
