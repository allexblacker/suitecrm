<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$dictionary["Case"]["fields"]["today_c"] = array(
	'name' => 'today_c',
	'label' => 'LBL_CASE_TODAY',
	'vname' => 'LBL_CASE_TODAY',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getToday',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

$dictionary["Case"]["fields"]["case_age_c"] = array(
	'name' => 'case_age_c',
	'label' => 'LBL_CASE_AGE',
	'vname' => 'LBL_CASE_AGE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getCaseAge',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

$dictionary["Case"]["fields"]["support_team_for_template_c"] = array(
	'name' => 'support_team_for_template_c',
	'label' => 'LBL_CASE_SUPPORT_TEAM',
	'vname' => 'LBL_CASE_SUPPORT_TEAM',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getSupportTeamForTemplate',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

$dictionary["Case"]["fields"]["last_modified_by_c"] = array(
	'name' => 'last_modified_by_c',
	'label' => 'LBL_CASE_LAST_MODIFIED_BY',
	'vname' => 'LBL_CASE_LAST_MODIFIED_BY',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getLastModifiedBy',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);