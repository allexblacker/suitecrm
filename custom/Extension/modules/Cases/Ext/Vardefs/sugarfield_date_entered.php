<?php
 // created: 2017-12-12 10:49:21
$dictionary['Case']['fields']['date_entered']['display_default']='now&12:00am';
$dictionary['Case']['fields']['date_entered']['comments']='Date record created';
$dictionary['Case']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Case']['fields']['date_entered']['audited']=true;

 ?>