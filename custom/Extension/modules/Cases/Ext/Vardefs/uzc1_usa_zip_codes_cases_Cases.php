<?php
// created: 2017-08-31 12:53:12
$dictionary["Case"]["fields"]["uzc1_usa_zip_codes_cases"] = array (
  'name' => 'uzc1_usa_zip_codes_cases',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_cases',
  'source' => 'non-db',
  'module' => 'UZC1_USA_ZIP_Codes',
  'bean_name' => 'UZC1_USA_ZIP_Codes',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
  'id_name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
);
$dictionary["Case"]["fields"]["uzc1_usa_zip_codes_cases_name"] = array (
  'name' => 'uzc1_usa_zip_codes_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
  'save' => true,
  'id_name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
  'link' => 'uzc1_usa_zip_codes_cases',
  'table' => 'uzc1_usa_zip_codes',
  'module' => 'UZC1_USA_ZIP_Codes',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida"] = array (
  'name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_cases',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_CASES_TITLE',
);
