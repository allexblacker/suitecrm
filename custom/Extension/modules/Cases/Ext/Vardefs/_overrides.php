<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['cases_cases_1']['rname'] = 'case_number';

$dictionary["Case"]["fields"]["cases_cases_1_name"]['rname'] = 'case_number';

$dictionary["Case"]["fields"]["case_attachments_display"]['function'] = array (
        'name' => 'display_case_attachments_fixed',
        'returns' => 'html',
        'include' => 'custom/modules/AOP_Case_Updates/Case_Updates.php',
      );


// Mass updates
$dictionary['Case']['fields']['do_not_send_survey_c']['massupdate'] = true;
$dictionary['Case']['fields']['alert_override_c']['massupdate'] = true;
$dictionary['Case']['fields']['replenishment_c']['massupdate'] = true;
