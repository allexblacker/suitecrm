<?php
// created: 2017-09-11 18:28:20
$dictionary["Case"]["fields"]["cases_sn1_serial_number_1"] = array (
  'name' => 'cases_sn1_serial_number_1',
  'type' => 'link',
  'relationship' => 'cases_sn1_serial_number_1',
  'source' => 'non-db',
  'module' => 'SN1_Serial_Number',
  'bean_name' => 'SN1_Serial_Number',
  'side' => 'right',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE',
);
