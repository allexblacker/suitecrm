<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['dtbc_notify_customer_date'] = array(
	'name' => 'dtbc_notify_customer_date',
	'vname' => 'LBL_NOTIFY_CUSTOMER_DATE',
	'type' => 'datetimecombo',
	'dbType' => 'datetime',
	'comment' => 'Notify Customer Date',
	'importable' => 'true',
	'required' => false,
);