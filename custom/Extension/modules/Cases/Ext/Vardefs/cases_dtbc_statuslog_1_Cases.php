<?php
// created: 2017-09-07 14:41:36
$dictionary["Case"]["fields"]["cases_dtbc_statuslog_1"] = array (
  'name' => 'cases_dtbc_statuslog_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_statuslog_1',
  'source' => 'non-db',
  'module' => 'dtbc_StatusLog',
  'bean_name' => 'dtbc_StatusLog',
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE',
);
