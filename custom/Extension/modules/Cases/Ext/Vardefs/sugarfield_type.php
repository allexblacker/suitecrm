<?php
 // created: 2017-11-26 14:58:22
$dictionary['Case']['fields']['type']['len']=100;
$dictionary['Case']['fields']['type']['audited']=true;
$dictionary['Case']['fields']['type']['inline_edit']='';
$dictionary['Case']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Case']['fields']['type']['merge_filter']='disabled';
$dictionary['Case']['fields']['type']['required']=true;

 ?>