<?php
// created: 2017-09-08 20:38:49
$dictionary["Case"]["fields"]["cases_dtbc_casessurvey_1"] = array (
  'name' => 'cases_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'dtbc_CasesSurvey',
  'bean_name' => 'dtbc_CasesSurvey',
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);
