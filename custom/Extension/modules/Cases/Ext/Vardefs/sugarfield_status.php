<?php
 // created: 2018-01-04 14:46:22
$dictionary['Case']['fields']['status']['required']=true;
$dictionary['Case']['fields']['status']['inline_edit']=true;
$dictionary['Case']['fields']['status']['comments']='The status of the case';
$dictionary['Case']['fields']['status']['merge_filter']='disabled';
$dictionary['Case']['fields']['status']['massupdate']=0;

 ?>