<?php
 // created: 2018-02-27 16:04:37
$dictionary['Case']['fields']['priority']['inline_edit']=true;
$dictionary['Case']['fields']['priority']['options']='case_priority_list';
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';
$dictionary['Case']['fields']['priority']['default']='4';

 ?>