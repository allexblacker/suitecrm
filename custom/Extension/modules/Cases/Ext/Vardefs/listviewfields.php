<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['listview_envelope'] = array(
	'name' => 'listview_envelope',
	'label' => 'LBL_LISTVIEW_ENVELOPE',
	'vname' => 'LBL_LISTVIEW_ENVELOPE',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

$dictionary['Case']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);