<?php

$mod_strings['LBL_ADD_CASE_FILE'] = 'Datei hinzufügen';
$mod_strings['LBL_AOP_CASE_EVENTS'] = 'Fälle?';
$mod_strings['LBL_CASE_ATTACHMENTS_DISPLAY'] = 'Fall-Anhänge';
$mod_strings['LBL_CASE_UPDATE_FORM'] = 'Anhang-Formular aktualisieren';
$mod_strings['LBL_CLEAR_CASE_DOCUMENT'] = 'Dokument leeren';
$mod_strings['LBL_CONTACT_CREATED_BY_NAME'] = 'Erstellt von Kontakt';
$mod_strings['LBL_REMOVE_CASE_FILE'] = 'Entferne Datei';
$mod_strings['LBL_SELECT_CASE_DOCUMENT'] = 'Dokument auswählen';
$mod_strings['LBL_SELECT_EXTERNAL_CASE_DOCUMENT'] = 'Externe Datei';
$mod_strings['LBL_SELECT_INTERNAL_CASE_DOCUMENT'] = 'Internes CRM-Dokument';
