<?php

if(!function_exists('dtbc_isLinkedToCompensation')){
	function dtbc_isLinkedToCompensation($rmaBeanId) {
		global $db;
		$sql = "SELECT *
				FROM cases_cases_1_c
				WHERE deleted = 0 AND
				cases_cases_1cases_ida = " . $db->quoted($rmaBeanId);
		$res = $db->fetchOne($sql);
		return !empty($res) && is_array($res) && count($res) > 0;
	}
}

if (!empty($_REQUEST['record']) && strtolower($_REQUEST['action']) == 'detailview' && strtolower($_REQUEST['module']) == 'cases') {
	global $current_user, $sugar_config, $db, $mod_strings;

	$module_menu[] = array(
		"index.php?module=Cases&action=changeTier2&module=" . $_REQUEST['module'] . "&record=" . $_REQUEST['record'],
		$mod_strings['LBL_BTN_TO_TIER2'],
		"",
		"Cases",
	);
	
	global $current_user;
	if ($current_user->tier3_c == 1)
		$module_menu[] = array(
			"index.php?module=Cases&action=changeTier3&module=" . $_REQUEST['module'] . "&record=" . $_REQUEST['record'],
			$mod_strings['LBL_BTN_TO_TIER3'],
			"",
			"Cases",
		);
	
	$bean = BeanFactory::getBean("Cases", $_REQUEST['record']);
	
	if ($bean != null && is_object($bean)) {
		if ($bean->case_record_type_c == 'rma' /*&& $bean->rma_yes_no_c == 'Yes'*/ && !dtbc_isLinkedToCompensation($_REQUEST['record'])) {
			$module_menu[] = array(
				"index.php?module=Cases&action=newCompensation&wrokingmodule=" . $_REQUEST['module'] . "&workingrecord=" . $_REQUEST['record'],
				$mod_strings['LBL_BTN_NEW_COMPENSATION'],
				"",
				"Cases",
			);
		}
		
		if ($bean->case_record_type_c != "rma" && $bean->case_record_type_c != "ask_sedg" && $bean->case_record_type_c != "compensation" && $bean->case_type_c == "2") {
			$module_menu[] = array(
				"index.php?module=Cases&action=newRma&wrokingmodule=" . $_REQUEST['module'] . "&workingrecord=" . $_REQUEST['record'] . "&newrma=1",
				$mod_strings['LBL_BTN_NEW_RMA'],
				"",
				"Cases",
			);
		}
		
		if ($bean->case_record_type_c == "compensation") {
			$module_menu[] = array(
				"index.php?module=Cases&action=declineCompensation&module=" . $_REQUEST['module'] . "&record=" . $_REQUEST['record'],
				$mod_strings['LBL_BTN_DECLINE_COMPENSATION'],
				"",
				"Cases",
			);
		}
	}
	
	$module_menu[] = array(
		"index.php?module=Cases&action=fieldEngineer&workingmodule=" . $_REQUEST['module'] . "&workingrecord=" . $_REQUEST['record'] . "&fieldengineer=1&return_module=Cases&return_action=DetailView&return_id=" . $_REQUEST['record'] . "&record=" . $_REQUEST['record'],
		$mod_strings['LBL_BTN_FIELD_ENGINEER'],
		"",
		"Cases",
	);
	
	if ($bean != null && is_object($bean)) {
		if ($bean->load_relationship('cases_cases_3')) {
			$spcases = $bean->cases_cases_3->getBeans();
			if (count($spcases) == 0) {
				$module_menu[] = array(
					"index.php?module=Cases&action=createspcase&return_module=Cases&return_action=DetailView&spcase=1&workingrecord=" . $_REQUEST['record'],
					$mod_strings['LBL_BTN_SPCASE'],
					"",
					"Cases",
				);
			}
		}
	}
	
	$module_menu []= array('index.php?module=Emails&action=index', $mod_strings['LBL_SEND_ARTICLE_BUTTON']);
	$module_menu []= array('index.php?module=Cases&action=check_alternative&record=' . $_REQUEST['record'], $mod_strings['LBL_CHECK_ALTERNATIVE_BUTTON']);
	$module_menu []= array('index.php?module=Cases&action=check_battery&record=' . $_REQUEST['record'], $mod_strings['LBL_CHECK_BATTERY_BUTTON']);
	$module_menu []= array('index.php?module=Cases&action=check_serial&record=' . $_REQUEST['record'], $mod_strings['LBL_CHECK_SERIAL_BUTTON']);
    $module_menu []= array('index.php?module=Cases&action=changeIndicator&record=' . $_REQUEST['record'], $mod_strings['LBL_CHANGE_INDICATOR_BUTTON']);
	
	$module_menu[] = array(
		"index.php?module=Cases&action=closeCase&return_module=Cases&return_action=DetailView&closecase=1&record=" . $_REQUEST['record'] . "&workingrecord=" . $_REQUEST['record'],
		$mod_strings['LBL_CLOSE_CASE_BUTTON'],
		"",
		"Cases",
	);
	
	if ($bean->case_record_type_c == "rma")
		$module_menu []= array('index.php?module=Cases&action=SendToERP&record=' . $_REQUEST['record'] . '&return_module=Cases', $mod_strings['LBL_SEND_TO_ERP_BUTTON']);
}

?>