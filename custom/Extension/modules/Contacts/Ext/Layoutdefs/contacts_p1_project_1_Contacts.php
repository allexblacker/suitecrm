<?php
 // created: 2017-09-25 18:21:08
$layout_defs["Contacts"]["subpanel_setup"]['contacts_p1_project_1'] = array (
  'order' => 100,
  'module' => 'P1_Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
  'get_subpanel_data' => 'contacts_p1_project_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
