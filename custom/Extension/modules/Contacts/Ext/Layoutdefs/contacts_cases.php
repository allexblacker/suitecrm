<?php

$layout_defs['Contacts']['subpanel_setup']['cases'] = array(
		'order' => 100,
		'module' => 'Cases',
		'get_subpanel_data'=>'function:getRelatedCases',
		'sort_order' => 'asc',
		'sort_by' => 'id',
		'generate_select' => false,
		'subpanel_name' => 'default',
		'title_key' => 'LBL_CASES_SUBPANEL_TITLE', 
		'function_parameters' => array(
			'import_function_file' => 'custom/modules/Contacts/dtbc/getRelatedCases.php',
		)
);