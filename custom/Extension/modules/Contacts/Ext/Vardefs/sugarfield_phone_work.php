<?php
 // created: 2017-08-24 10:08:50
$dictionary['Contact']['fields']['phone_work']['required']=true;
$dictionary['Contact']['fields']['phone_work']['inline_edit']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';

 ?>