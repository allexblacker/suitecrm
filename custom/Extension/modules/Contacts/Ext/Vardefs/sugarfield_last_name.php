<?php
 // created: 2017-10-24 16:08:02
$dictionary['Contact']['fields']['last_name']['required']=true;
$dictionary['Contact']['fields']['last_name']['inline_edit']=true;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']='1';

 ?>