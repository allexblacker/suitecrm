<?php
 // created: 2017-10-26 21:10:33
$dictionary['Contact']['fields']['primary_address_street']['len']='255';
$dictionary['Contact']['fields']['primary_address_street']['inline_edit']=true;
$dictionary['Contact']['fields']['primary_address_street']['comments']='Street address for primary address';
$dictionary['Contact']['fields']['primary_address_street']['merge_filter']='disabled';

 ?>