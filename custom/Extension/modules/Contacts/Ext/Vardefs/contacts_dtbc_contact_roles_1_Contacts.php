<?php
// created: 2017-10-04 15:26:52
$dictionary["Contact"]["fields"]["contacts_dtbc_contact_roles_1"] = array (
  'name' => 'contacts_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'dtbc_Contact_Roles',
  'bean_name' => 'dtbc_Contact_Roles',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);
