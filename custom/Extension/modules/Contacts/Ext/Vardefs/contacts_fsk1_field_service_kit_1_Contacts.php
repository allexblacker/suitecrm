<?php
// created: 2017-11-09 11:47:52
$dictionary["Contact"]["fields"]["contacts_fsk1_field_service_kit_1"] = array (
  'name' => 'contacts_fsk1_field_service_kit_1',
  'type' => 'link',
  'relationship' => 'contacts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'module' => 'FSK1_Field_Service_Kit',
  'bean_name' => 'FSK1_Field_Service_Kit',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
);
