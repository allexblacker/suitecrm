<?php
 // created: 2017-10-26 21:08:18
$dictionary['Contact']['fields']['alt_address_street']['len']='255';
$dictionary['Contact']['fields']['alt_address_street']['inline_edit']=true;
$dictionary['Contact']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Contact']['fields']['alt_address_street']['merge_filter']='disabled';

 ?>