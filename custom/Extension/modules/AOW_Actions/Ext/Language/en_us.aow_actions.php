<?php

$mod_strings['LBL_SENDEMAILWITHFROM'] = 'Send email with from';
$mod_strings['LBL_SEND_EMAIL_FROM_TEXT'] = 'From';
$mod_strings['LBL_CREATESCHEDULEDWORKFLOW'] = 'Create System Workflow Record';
$mod_strings['LBL_SENDEMAILFROMEMAILQUEUEWITHFROM'] = 'Send email from email queue with from';