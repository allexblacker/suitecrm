<?php

$mod_strings['LBL_RESCHEDULE'] = 'Wiedervorlage';
$mod_strings['LBL_RESCHEDULE_COUNT'] = 'Rufen Sie Versuche';
$mod_strings['LBL_RESCHEDULE_DATE'] = 'Datum';
$mod_strings['LBL_RESCHEDULE_ERROR1'] = 'Bitte ein gültiges Datum auswählen';
$mod_strings['LBL_RESCHEDULE_ERROR2'] = 'Bitte einen Grund auswählen';
$mod_strings['LBL_RESCHEDULE_HISTORY'] = 'Verlauf Anrufversuche';
$mod_strings['LBL_RESCHEDULE_PANEL'] = 'Wiedervorlage';
$mod_strings['LBL_RESCHEDULE_REASON'] = 'Grund';
