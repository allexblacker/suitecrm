<?php

$mod_strings['LBL_VALIDATION_ERR_FILTER_LOGIC'] = 'Filter logic must contain only the followings: curly brackets (), phrase and, phrase or, numbers and spaces. Please make sure that before and after the phrases has a space!';
$mod_strings['LBL_VALIDATION_ERR_FILTER_LOGIC_BRACKETS'] = 'Open and close brackets number not matched';
$mod_strings['LBL_VALIDATION_ERR_USERGROUP'] = 'You must set a user or a group (or check the "Do Not Reassign Owner" option)';
$mod_strings['LBL_SAVEBUTTON'] = 'Save';
