<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	1,
	'Autofill the name and module_c fields',
	'custom/include/dtbc/hooks/dtbc_assignment_rules_before_save.php',
	'DtbcAssignmentRulesHooks',
	'before_save_autofill'
);