<?php

// Mass updates
$dictionary['dtbc_Line_Item']['fields']['shipment_date_c']['massupdate'] = true;
$dictionary['dtbc_Line_Item']['fields']['discount_c']['massupdate'] = true;
$dictionary['dtbc_Line_Item']['fields']['quantity_c']['massupdate'] = true;
