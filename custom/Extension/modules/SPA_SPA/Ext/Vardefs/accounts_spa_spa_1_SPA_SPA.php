<?php
// created: 2018-02-26 16:32:15
$dictionary["SPA_SPA"]["fields"]["accounts_spa_spa_1"] = array (
  'name' => 'accounts_spa_spa_1',
  'type' => 'link',
  'relationship' => 'accounts_spa_spa_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_spa_spa_1accounts_ida',
);
$dictionary["SPA_SPA"]["fields"]["accounts_spa_spa_1_name"] = array (
  'name' => 'accounts_spa_spa_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_spa_spa_1accounts_ida',
  'link' => 'accounts_spa_spa_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["SPA_SPA"]["fields"]["accounts_spa_spa_1accounts_ida"] = array (
  'name' => 'accounts_spa_spa_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_spa_spa_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_SPA_SPA_TITLE',
);
