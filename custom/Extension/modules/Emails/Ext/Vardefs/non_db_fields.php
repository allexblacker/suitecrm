<?php

$dictionary['Email']['fields']['f_from_addr_c'] = array(
	'required' => false,
	'name' => 'f_from_addr_c',
	'vname' => 'LBL_AOP_CASE_UPDATES_THREADED',
	'type' => 'varchar',
	'source' => 'non-db',
	'massupdate' => 0,
	'studio' => 'visible',
	'importable' => 'false',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => 0,
	'audited' => false,
	'reportable' => false,
	'inline_edit' => 0,
);