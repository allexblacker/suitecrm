<?php

$hook_array['before_save'][] = Array(
    100,
    'Update USA Zip Code field with the value of Zip Postal Code in case of Account Country = United States AND USA Zip Code is blank AND Zip Postal Code is not blank',
    'custom/include/dtbc/hooks/accountsLogicHooks.php',
    'AccountsLogicHooks',
    'updateUSAZipCode'
);