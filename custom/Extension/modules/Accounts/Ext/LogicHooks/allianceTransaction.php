<?php

$order = 15;
$description = 'Setting last alliance transaction';
$filePath = 'custom/include/dtbc/hooks/accountsLogicHooks.php';
$className = "AccountsLogicHooks";
$functionName = "updateAllianceTransactionsFields";

$hookArray = array($order, $description, $filePath, $className, $functionName);

$hook_array['after_relationship_delete'][] = $hookArray;
$hook_array['after_relationship_add'][] = $hookArray;
