<?php
 // created: 2017-10-25 18:47:53
$layout_defs["Accounts"]["subpanel_setup"]['accounts_is1_inside_sales_1'] = array (
  'order' => 100,
  'module' => 'IS1_Inside_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE',
  'get_subpanel_data' => 'accounts_is1_inside_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
