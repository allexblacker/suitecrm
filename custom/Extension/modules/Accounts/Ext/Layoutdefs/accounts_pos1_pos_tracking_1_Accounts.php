<?php
 // created: 2017-10-18 10:13:09
$layout_defs["Accounts"]["subpanel_setup"]['accounts_pos1_pos_tracking_1'] = array (
  'order' => 100,
  'module' => 'POS1_POS_Tracking',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE',
  'get_subpanel_data' => 'accounts_pos1_pos_tracking_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
