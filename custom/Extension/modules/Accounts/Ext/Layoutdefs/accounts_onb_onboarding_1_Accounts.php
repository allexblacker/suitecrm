<?php
 // created: 2018-02-26 13:47:22
$layout_defs["Accounts"]["subpanel_setup"]['accounts_onb_onboarding_1'] = array (
  'order' => 100,
  'module' => 'ONB_Onboarding',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ONB_ONBOARDING_TITLE',
  'get_subpanel_data' => 'accounts_onb_onboarding_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
