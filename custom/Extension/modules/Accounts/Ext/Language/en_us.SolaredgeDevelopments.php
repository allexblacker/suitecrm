<?php





$app_list_strings['accountstates'][''] = '';
$app_list_strings['accountstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['accountstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['accountstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['accountstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['accountstates']['Unitedstates_California'] = 'California';
$app_list_strings['accountstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['accountstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['accountstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['accountstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['accountstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['accountstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['accountstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['accountstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['accountstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['accountstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['accountstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['accountstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['accountstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['accountstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['accountstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['accountstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['accountstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['accountstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['accountstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['accountstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['accountstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['accountstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['accountstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['accountstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['accountstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['accountstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['accountstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['accountstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['accountstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['accountstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['accountstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['accountstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['accountstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['accountstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['accountstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['accountstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['accountstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['accountstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['accountstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['accountstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['accountstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['accountstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['accountstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['accountstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['accountstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['accountstates']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['accountstates']['Canada_Alberta'] = 'Alberta';
$app_list_strings['accountstates']['Canada_British Columbia '] = 'British Columbia ';
$app_list_strings['accountstates']['Canada_Manitoba '] = 'Manitoba ';
$app_list_strings['accountstates']['Canada_NewBrunswick'] = 'New Brunswick';
$app_list_strings['accountstates']['Canada_NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['accountstates']['Canada_Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['accountstates']['Canada_Nunavut'] = 'Nunavut';
$app_list_strings['accountstates']['Canada_Ontario'] = 'Ontario';
$app_list_strings['accountstates']['Canada_PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['accountstates']['Canada_Quebec'] = 'Quebec';
$app_list_strings['accountstates']['Canada_Saskatchewan'] = 'Saskatchewan';
$app_list_strings['accountstates']['Canada_Yukon'] = 'Yukon';

