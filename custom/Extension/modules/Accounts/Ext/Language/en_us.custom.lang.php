<?php

$mod_strings['LBL_FUNCTION_LOYALTY_START'] = 'Alliance Plan start date';
$mod_strings['LBL_FUNCTION_BALANCE'] = 'Alliance Balance';
$mod_strings['LBL_FUNCTION_TOTAL_KW'] = 'Total KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2013'] = 'Year 2013 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2014'] = 'Year 2014 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2015'] = 'Year 2015 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2016'] = 'Year 2016 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2017'] = 'Year 2017 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2018'] = 'Year 2018 KW';
$mod_strings['LBL_FUNCTION_EXTRA'] = 'Extra';
$mod_strings['LBL_FUNCTION_UNIQUE_SITES'] = 'No of unique sites';
$mod_strings['LBL_FUNCTION_LAST_TRANSACTION'] = 'Last Alliance Transaction';
$mod_strings['LBL_FUNCTION_LAST_POS_TRANSACTION'] = 'Last POS transaction';
$mod_strings['LBL_FUNCTION_OF_OPPORTUNITIES'] = '# of Opportunities';
$mod_strings['LBL_FUNCTION_CHECKIFFIRSTOPP'] = 'CheckIfFirstOpp';
$mod_strings['LBL_FUNCTION_SERVICE_KIT'] = 'Service Kit';
$mod_strings['LBL_FUNCTION_NEW_ACCOUNT_BUTTON'] = 'New Account button';