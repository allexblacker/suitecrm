<?php
// created: 2017-10-25 18:47:53
$dictionary["Account"]["fields"]["accounts_is1_inside_sales_1"] = array (
  'name' => 'accounts_is1_inside_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_is1_inside_sales_1',
  'source' => 'non-db',
  'module' => 'IS1_Inside_Sales',
  'bean_name' => 'IS1_Inside_Sales',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE',
);
