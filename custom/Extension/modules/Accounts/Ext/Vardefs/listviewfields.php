<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Account']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

// Kobi
$dictionary["Account"]["fields"]["first_inside_conv_c"] = array(
	'name' => 'first_inside_conv_c',
	'label' => 'LBL_FIRST_INSIDE_CONV_DATE',
	'vname' => 'LBL_FIRST_INSIDE_CONV_DATE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_first_inside_conv_date',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

// Kobi
$dictionary["Account"]["fields"]["last_inside_conv_c"] = array(
	'name' => 'last_inside_conv_c',
	'label' => 'LBL_LAST_INSIDE_CONV_DATE',
	'vname' => 'LBL_LAST_INSIDE_CONV_DATE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_last_inside_conv_date',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["first_onboard_c"] = array(
	'name' => 'first_onboard_c',
	'label' => 'LBL_FIRST_ONBOARDING_DATE',
	'vname' => 'LBL_FIRST_ONBOARDING_DATE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_first_onboard_date',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);
