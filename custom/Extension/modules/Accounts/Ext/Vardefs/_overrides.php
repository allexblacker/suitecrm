<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

// Mass updates
$dictionary['Account']['fields']['state_c']['massupdate'] = true;
$dictionary['Account']['fields']['account_owner_c']['massupdate'] = true;
$dictionary['Account']['fields']['inactive_reason_c']['massupdate'] = true;