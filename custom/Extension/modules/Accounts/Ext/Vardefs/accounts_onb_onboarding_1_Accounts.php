<?php
// created: 2018-02-26 13:47:22
$dictionary["Account"]["fields"]["accounts_onb_onboarding_1"] = array (
  'name' => 'accounts_onb_onboarding_1',
  'type' => 'link',
  'relationship' => 'accounts_onb_onboarding_1',
  'source' => 'non-db',
  'module' => 'ONB_Onboarding',
  'bean_name' => 'ONB_Onboarding',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ONB_ONBOARDING_TITLE',
);
