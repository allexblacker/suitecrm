<?php 

$dictionary["Account"]["fields"]["Loyalty_Plan_start_date__c"] = array(
	'name' => 'Loyalty_Plan_start_date__c',
	'label' => 'LBL_FUNCTION_LOYALTY_START',
	'vname' => 'LBL_FUNCTION_LOYALTY_START',
	'type' => 'date',
	'studio' => 'visible',
);

$dictionary["Account"]["fields"]["Balance__c"] = array(
	'name' => 'Balance__c',
	'label' => 'LBL_FUNCTION_BALANCE',
	'vname' => 'LBL_FUNCTION_BALANCE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_balance',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["KW__c"] = array(
	'name' => 'KW__c',
	'label' => 'LBL_FUNCTION_TOTAL_KW',
	'vname' => 'LBL_FUNCTION_TOTAL_KW',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalKw',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Extra__c"] = array(
	'name' => 'Extra__c',
	'label' => 'LBL_FUNCTION_EXTRA',
	'vname' => 'LBL_FUNCTION_EXTRA',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_extra',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["No_of_unique_sites__c"] = array(
	'name' => 'No_of_unique_sites__c',
	'label' => 'LBL_FUNCTION_UNIQUE_SITES',
	'vname' => 'LBL_FUNCTION_UNIQUE_SITES',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_uniqueSites',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Last_Alliance_Transaction__c"] = array(
	'name' => 'Last_Alliance_Transaction__c',
	'label' => 'LBL_FUNCTION_LAST_TRANSACTION',
	'vname' => 'LBL_FUNCTION_LAST_TRANSACTION',
	'type' => 'date',
	'studio' => 'visible',
);

$dictionary["Account"]["fields"]["Year_2013_KW__c"] = array(
	'name' => 'Year_2013_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2013',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2013',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2013',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2014_KW__c"] = array(
	'name' => 'Year_2014_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2014',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2014',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2014',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2015_KW__c"] = array(
	'name' => 'Year_2015_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2015',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2015',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2015',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2016_KW__c"] = array(
	'name' => 'Year_2016_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2016',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2016',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2016',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2017_KW__c"] = array(
	'name' => 'Year_2017_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2017',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2017',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2017',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["year_2018_kw_c"] = array(
	'name' => 'year_2018_kw_c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2018',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2018',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2018',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);


$dictionary["Account"]["fields"]["last_pos_transaction_c"] = array(
	'name' => 'last_pos_transaction_c',
	'label' => 'LBL_FUNCTION_LAST_POS_TRANSACTION',
	'vname' => 'LBL_FUNCTION_LAST_POS_TRANSACTION',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lastPOSTransaction',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["of_opportunities_c"] = array(
	'name' => 'of_opportunities_c',
	'label' => 'LBL_FUNCTION_OF_OPPORTUNITIES',
	'vname' => 'LBL_FUNCTION_OF_OPPORTUNITIES',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_ofOpportunities',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["checkiffirstopp_c"] = array(
	'name' => 'checkiffirstopp_c',
	'label' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
	'vname' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_checkIfFirstOpp',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["service_kit_c"] = array(
	'name' => 'service_kit_c',
	'label' => 'LBL_FUNCTION_SERVICE_KIT',
	'vname' => 'LBL_FUNCTION_SERVICE_KIT',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_serviceKit',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["new_account_button_c"] = array(
	'name' => 'new_account_button_c',
	'label' => 'LBL_FUNCTION_NEW_ACCOUNT_BUTTON',
	'vname' => 'LBL_FUNCTION_NEW_ACCOUNT_BUTTON',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_newAccountButton',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);