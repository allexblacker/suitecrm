<?php
// created: 2017-09-20 16:42:55
$dictionary["Account"]["fields"]["accounts_al1_alliance_transaction_1"] = array (
  'name' => 'accounts_al1_alliance_transaction_1',
  'type' => 'link',
  'relationship' => 'accounts_al1_alliance_transaction_1',
  'source' => 'non-db',
  'module' => 'AL1_Alliance_Transaction',
  'bean_name' => 'AL1_Alliance_Transaction',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE',
);
