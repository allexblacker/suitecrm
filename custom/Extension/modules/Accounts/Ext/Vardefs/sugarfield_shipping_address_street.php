<?php
 // created: 2017-10-26 15:48:44
$dictionary['Account']['fields']['shipping_address_street']['len']='255';
$dictionary['Account']['fields']['shipping_address_street']['inline_edit']=true;
$dictionary['Account']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['Account']['fields']['shipping_address_street']['merge_filter']='disabled';

 ?>