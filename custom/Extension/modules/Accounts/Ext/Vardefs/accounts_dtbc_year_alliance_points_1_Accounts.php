<?php
// created: 2017-09-21 16:32:04
$dictionary["Account"]["fields"]["accounts_dtbc_year_alliance_points_1"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1',
  'type' => 'link',
  'relationship' => 'accounts_dtbc_year_alliance_points_1',
  'source' => 'non-db',
  'module' => 'dtbc_Year_Alliance_Points',
  'bean_name' => 'dtbc_Year_Alliance_Points',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE',
);
