<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Account']['fields']['email_points_total'] = array(
	'name' => 'email_points_total',
	'label' => 'LBL_POINTS_TOTAL',
	'vname' => 'LBL_POINTS_TOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_points_total',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	), 
);

$dictionary['Account']['fields']['email_points_table'] = array(
	'name' => 'email_points_table',
	'label' => 'LBL_POINTS_TABLE',
	'vname' => 'LBL_POINTS_TABLE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_points_table',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	), 
);