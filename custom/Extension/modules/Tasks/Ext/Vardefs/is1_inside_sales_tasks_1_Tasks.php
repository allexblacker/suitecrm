<?php
// created: 2017-11-01 02:15:14
$dictionary["Task"]["fields"]["is1_inside_sales_tasks_1"] = array (
  'name' => 'is1_inside_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'IS1_Inside_Sales',
  'bean_name' => 'IS1_Inside_Sales',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_IS1_INSIDE_SALES_TITLE',
  'id_name' => 'is1_inside_sales_tasks_1is1_inside_sales_ida',
);
$dictionary["Task"]["fields"]["is1_inside_sales_tasks_1_name"] = array (
  'name' => 'is1_inside_sales_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_IS1_INSIDE_SALES_TITLE',
  'save' => true,
  'id_name' => 'is1_inside_sales_tasks_1is1_inside_sales_ida',
  'link' => 'is1_inside_sales_tasks_1',
  'table' => 'is1_inside_sales',
  'module' => 'IS1_Inside_Sales',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["is1_inside_sales_tasks_1is1_inside_sales_ida"] = array (
  'name' => 'is1_inside_sales_tasks_1is1_inside_sales_ida',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_TASKS_TITLE',
);
