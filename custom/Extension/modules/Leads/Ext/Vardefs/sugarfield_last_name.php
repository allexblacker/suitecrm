<?php
 // created: 2017-10-24 16:13:07
$dictionary['Lead']['fields']['last_name']['required']=true;
$dictionary['Lead']['fields']['last_name']['inline_edit']=true;
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']='1';

 ?>