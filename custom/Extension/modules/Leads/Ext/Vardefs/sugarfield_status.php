<?php
 // created: 2017-10-31 13:43:29
$dictionary['Lead']['fields']['status']['required']=true;
$dictionary['Lead']['fields']['status']['inline_edit']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['options']='lead_status_list';

 ?>