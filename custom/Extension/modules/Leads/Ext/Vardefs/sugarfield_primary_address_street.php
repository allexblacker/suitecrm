<?php
 // created: 2017-11-08 14:59:43
$dictionary['Lead']['fields']['primary_address_street']['len']='255';
$dictionary['Lead']['fields']['primary_address_street']['inline_edit']=true;
$dictionary['Lead']['fields']['primary_address_street']['comments']='Street address for primary address';
$dictionary['Lead']['fields']['primary_address_street']['merge_filter']='disabled';

 ?>