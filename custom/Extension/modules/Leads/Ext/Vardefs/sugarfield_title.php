<?php
 // created: 2017-11-09 13:05:24
$dictionary['Lead']['fields']['title']['len']='255';
$dictionary['Lead']['fields']['title']['inline_edit']=true;
$dictionary['Lead']['fields']['title']['comments']='The title of the contact';
$dictionary['Lead']['fields']['title']['merge_filter']='disabled';

 ?>