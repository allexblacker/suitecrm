<?php
// created: 2017-09-26 12:28:23
$dictionary["dtbc_Project_Stakeholders"]["fields"]["p1_project_dtbc_project_stakeholders_1"] = array (
  'name' => 'p1_project_dtbc_project_stakeholders_1',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_stakeholders_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_STAKEHOLDERS_1_FROM_P1_PROJECT_TITLE',
  'id_name' => 'p1_project_dtbc_project_stakeholders_1p1_project_ida',
);
$dictionary["dtbc_Project_Stakeholders"]["fields"]["p1_project_dtbc_project_stakeholders_1_name"] = array (
  'name' => 'p1_project_dtbc_project_stakeholders_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_STAKEHOLDERS_1_FROM_P1_PROJECT_TITLE',
  'save' => true,
  'id_name' => 'p1_project_dtbc_project_stakeholders_1p1_project_ida',
  'link' => 'p1_project_dtbc_project_stakeholders_1',
  'table' => 'p1_project',
  'module' => 'P1_Project',
  'rname' => 'name',
);
$dictionary["dtbc_Project_Stakeholders"]["fields"]["p1_project_dtbc_project_stakeholders_1p1_project_ida"] = array (
  'name' => 'p1_project_dtbc_project_stakeholders_1p1_project_ida',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_stakeholders_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_STAKEHOLDERS_1_FROM_DTBC_PROJECT_STAKEHOLDERS_TITLE',
);
