<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['SN1_Serial_Number']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

$dictionary['SN1_Serial_Number']['fields']['subpanel_check_rma_c'] = array(
	'name' => 'subpanel_check_rma_c',
	'vname' => 'LBL_CHECK_RMA_CHECKBOX',
	'label' => 'LBL_CHECK_RMA_CHECKBOX',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);