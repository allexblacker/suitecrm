<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['SN1_Serial_Number']['fields']['rma_alert_c'] = array(
	'required' => false,
	'name' => 'rma_alert_c',
	'vname' => 'LBL_RMA_ALERT',
	'type' => 'function',
	'source' => 'non-db',
	'massupdate' => 0,
	'studio' => 'visible',
	'importable' => 'false',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => 0,
	'audited' => false,
	'reportable' => false,
	'inline_edit' => 0,
	'function' => array(
		'name' => 'dtbc_display_rma_alert_image',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/rma_alert_c.php',
	),
);