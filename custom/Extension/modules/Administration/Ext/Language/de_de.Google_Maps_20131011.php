<?php

$mod_strings['LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_DESC'] = 'Erlaubt Zugang zu Informationen über den Adressen Cache.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_TITLE'] = 'Adressen Cache';
$mod_strings['LBL_JJWG_MAPS_ADMIN_CONFIG_DESC'] = 'Konfigurationseinstellungen für Google Maps';
$mod_strings['LBL_JJWG_MAPS_ADMIN_CONFIG_TITLE'] = 'Google Maps-Einstellungen';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DESC'] = 'Verwalten Sie Ihre Geokodierungen, testen Sie Geokodierungen, überprüfen Sie die Resultate und ändern Sie die erweiterten Einstellungen.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DONATE_DESC'] = 'Bitte ziehen Sie eine Spende für dieses Projekt in Erwägung!';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DONATE_TITLE'] = 'Spenden Sie an dieses Projekt';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_DESC'] = 'Zeigt die Anzahl der geokodierten Modulobjekte gruppiert nach dem Ergebnis der Geokodierung an.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_TITLE'] = 'Anzahl Geokodierungen';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_DESC'] = 'Geokodieren Sie Ihre Objektadressen. Dieser Vorgang kann einige Minuten dauern!';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_TITLE'] = 'Adressen geokodieren';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_DESC'] = 'Führen Sie einen einzelnen Geokodierungstests mit detaillierten Resultaten durch.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_TITLE'] = 'Test Geokodierung';
$mod_strings['LBL_JJWG_MAPS_ADMIN_HEADER'] = 'Google Maps';
