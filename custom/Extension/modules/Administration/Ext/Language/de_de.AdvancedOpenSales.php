<?php

$mod_strings['LBL_AOS_ADMIN_CONTRACT_RENEWAL_REMINDER'] = 'Erinnerungsperiode Verlängerung';
$mod_strings['LBL_AOS_ADMIN_CONTRACT_SETTINGS'] = 'Vertragseinstellungen';
$mod_strings['LBL_AOS_ADMIN_ENABLE_LINE_ITEM_GROUPS'] = 'Zeilenelementgruppen erlauben';
$mod_strings['LBL_AOS_ADMIN_ENABLE_LINE_ITEM_TOTAL_TAX'] = 'Steuer zu Zeilensumme hinzufügen';
$mod_strings['LBL_AOS_ADMIN_INITIAL_INVOICE_NUMBER'] = 'Ursprüngliche Rechnungsnummer';
$mod_strings['LBL_AOS_ADMIN_INITIAL_QUOTE_NUMBER'] = 'Erste Angebotsnummer';
$mod_strings['LBL_AOS_ADMIN_INVOICE_SETTINGS'] = 'Einstellungen Rechnung';
$mod_strings['LBL_AOS_ADMIN_LINE_ITEM_SETTINGS'] = 'Zeilenelement Einstellungen';
$mod_strings['LBL_AOS_ADMIN_MANAGE_AOS'] = 'Advanced OpenSales Einstellungen';
$mod_strings['LBL_AOS_ADMIN_QUOTE_SETTINGS'] = 'Einstellungen Angebot';
$mod_strings['LBL_AOS_DAYS'] = 'Tage';
$mod_strings['LBL_AOS_EDIT'] = 'bearbeiten';
$mod_strings['LBL_AOS_PRODUCTS'] = 'AOS Produkte';
$mod_strings['LBL_AOS_SETTINGS'] = 'AOS Einstellungen';
$mod_strings['LBL_CHANGE_SETTINGS'] = 'Einstellungen für erweiterte OpenSale ändern';
$mod_strings['LBL_SALESAGILITY_ADMIN'] = 'Erweiterter OpenAdmin';
