<?php

$mod_strings['LBL_REPAIR_RESCHEDULE_DONE'] = 'Wiedervorlagen erfolgreich repariert';
$mod_strings['LBL_RESCHEDULE_ADMIN'] = 'Wiedervorlage Einstellungen';
$mod_strings['LBL_RESCHEDULE_ADMIN_DESC'] = 'Konfigurieren und verwalten von Wiedervorlagen';
$mod_strings['LBL_RESCHEDULE_REBUILD'] = 'Wiedervorlagen reparieren';
$mod_strings['LBL_RESCHEDULE_REBUILD_DESC'] = 'repariert das Wiedervorlagen Modul';
$mod_strings['LBL_SALESAGILITY_ADMIN'] = 'Advanced OpenAdmin';
