<?php
// created: 2018-03-12 10:45:56
$dictionary["Document"]["fields"]["spadi_spa_distributor_documents_1"] = array (
  'name' => 'spadi_spa_distributor_documents_1',
  'type' => 'link',
  'relationship' => 'spadi_spa_distributor_documents_1',
  'source' => 'non-db',
  'module' => 'SPADI_SPA_Distributor',
  'bean_name' => 'SPADI_SPA_Distributor',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
  'id_name' => 'spadi_spa_distributor_documents_1spadi_spa_distributor_ida',
);
$dictionary["Document"]["fields"]["spadi_spa_distributor_documents_1_name"] = array (
  'name' => 'spadi_spa_distributor_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
  'save' => true,
  'id_name' => 'spadi_spa_distributor_documents_1spadi_spa_distributor_ida',
  'link' => 'spadi_spa_distributor_documents_1',
  'table' => 'spadi_spa_distributor',
  'module' => 'SPADI_SPA_Distributor',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["spadi_spa_distributor_documents_1spadi_spa_distributor_ida"] = array (
  'name' => 'spadi_spa_distributor_documents_1spadi_spa_distributor_ida',
  'type' => 'link',
  'relationship' => 'spadi_spa_distributor_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
