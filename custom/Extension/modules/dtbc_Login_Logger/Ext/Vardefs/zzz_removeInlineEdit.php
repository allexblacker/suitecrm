<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$dictionary['dtbc_StatusLog']['fields']['ip_address_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['username_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['logger_user_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['login_date_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['userid_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_entered']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_modified']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['name']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['description']['inline_edit']='0';
