<?php
// created: 2017-09-07 14:41:36
$dictionary["dtbc_StatusLog"]["fields"]["cases_dtbc_statuslog_1"] = array (
  'name' => 'cases_dtbc_statuslog_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_statuslog_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_CASES_TITLE',
  'id_name' => 'cases_dtbc_statuslog_1cases_ida',
);
$dictionary["dtbc_StatusLog"]["fields"]["cases_dtbc_statuslog_1_name"] = array (
  'name' => 'cases_dtbc_statuslog_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_dtbc_statuslog_1cases_ida',
  'link' => 'cases_dtbc_statuslog_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["dtbc_StatusLog"]["fields"]["cases_dtbc_statuslog_1cases_ida"] = array (
  'name' => 'cases_dtbc_statuslog_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_dtbc_statuslog_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE',
);
