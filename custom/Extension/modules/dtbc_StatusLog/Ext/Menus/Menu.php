<?php


 if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $mod_strings, $app_strings, $sugar_config;

$module_menu = null;

if(ACLController::checkAccess('dtbc_StatusLog', 'list', true)){
    $module_menu[]=array('index.php?module=dtbc_StatusLog&action=index&return_module=dtbc_StatusLog&return_action=DetailView', $mod_strings['LNK_LIST'],'View', 'dtbc_StatusLog');
}
