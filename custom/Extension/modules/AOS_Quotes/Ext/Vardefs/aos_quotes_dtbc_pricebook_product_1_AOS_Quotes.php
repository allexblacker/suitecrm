<?php
// created: 2017-10-13 13:04:41
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook_Product',
  'bean_name' => 'dtbc_Pricebook_Product',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);
