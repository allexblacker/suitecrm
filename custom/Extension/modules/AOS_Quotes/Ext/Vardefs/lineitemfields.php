<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary["AOS_Quotes"]["fields"]["lineitem_subtotal"] = array(
	'name' => 'lineitem_subtotal',
	'label' => 'LBL_LINEITEM_SUBTOTAL',
	'vname' => 'LBL_LINEITEM_SUBTOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_subtotal',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

$dictionary["AOS_Quotes"]["fields"]["lineitem_discount"] = array(
	'name' => 'lineitem_discount',
	'label' => 'LBL_LINEITEM_DISCOUNT',
	'vname' => 'LBL_LINEITEM_DISCOUNT',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_discount',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

$dictionary["AOS_Quotes"]["fields"]["lineitem_totalprice"] = array(
	'name' => 'lineitem_totalprice',
	'label' => 'LBL_LINEITEM_TOTALPRICE',
	'vname' => 'LBL_LINEITEM_TOTALPRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_totalprice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

$dictionary["AOS_Quotes"]["fields"]["lineitem_grandtotal"] = array(
	'name' => 'lineitem_grandtotal',
	'label' => 'LBL_LINEITEM_GRANDTOTAL',
	'vname' => 'LBL_LINEITEM_GRANDTOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_grandtotal',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);