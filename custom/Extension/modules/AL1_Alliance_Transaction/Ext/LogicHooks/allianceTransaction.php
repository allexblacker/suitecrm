<?php

$hook_array['after_save'][] = array(
    10,
    'Update Last_Alliance_Transaction__c on Accounts module',
    'custom/include/dtbc/hooks/allianceLogicHooks.php',
    'AllianceLogicHooks',
    'setAllianceTransaction'
);