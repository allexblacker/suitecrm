<?php

$hook_array['before_save'][] = array(
	10,
	'After import calculations',
	'custom/include/dtbc/hooks/10_AfterImportCalculations.php',
	'CustomHooks',
	'before_save'
);

$hook_array['before_save'][] = array(
	20,
	'Set name field if necessary',
	'custom/include/dtbc/hooks/20_SetAllianceName.php',
	'CustomNameHook',
	'before_save'
);

