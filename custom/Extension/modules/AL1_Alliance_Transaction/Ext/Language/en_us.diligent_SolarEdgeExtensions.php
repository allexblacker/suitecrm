<?php 

$mod_strings["LBL_SAVEBUTTON"] = "Save";
$mod_strings["LBL_INVALID_POINTS"] = "Too much points";
$mod_strings["LBL_POINT_SMALLER_THAN_ZERO"] = "Error: Please enter a number greater then 0.";
$mod_strings["LBL_ACCOUNTS_MONITORING_ID"] = "Account Monitoring ID";

?>