<?php
 // created: 2017-09-25 21:15:11
$layout_defs["Users"]["subpanel_setup"]['users_p1_project_2'] = array (
  'order' => 100,
  'module' => 'P1_Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_USERS_P1_PROJECT_2_FROM_P1_PROJECT_TITLE',
  'get_subpanel_data' => 'users_p1_project_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
