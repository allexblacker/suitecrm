<?php
// created: 2017-09-25 21:15:11
$dictionary["User"]["fields"]["users_p1_project_2"] = array (
  'name' => 'users_p1_project_2',
  'type' => 'link',
  'relationship' => 'users_p1_project_2',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_P1_PROJECT_TITLE',
);
