<?php
// created: 2017-11-01 02:15:14
$dictionary["IS1_Inside_Sales"]["fields"]["is1_inside_sales_tasks_1"] = array (
  'name' => 'is1_inside_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_TASKS_TITLE',
);
