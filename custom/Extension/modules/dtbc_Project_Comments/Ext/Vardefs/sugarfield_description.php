<?php
 // created: 2017-09-26 13:48:53
$dictionary['dtbc_Project_Comments']['fields']['description']['inline_edit']=true;
$dictionary['dtbc_Project_Comments']['fields']['description']['comments']='Full text of the note';
$dictionary['dtbc_Project_Comments']['fields']['description']['merge_filter']='disabled';
$dictionary['dtbc_Project_Comments']['fields']['description']['rows']='12';

 ?>