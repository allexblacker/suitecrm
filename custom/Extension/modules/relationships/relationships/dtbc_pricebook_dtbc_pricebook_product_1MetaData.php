<?php
// created: 2017-10-04 15:50:43
$dictionary["dtbc_pricebook_dtbc_pricebook_product_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'dtbc_pricebook_dtbc_pricebook_product_1' => 
    array (
      'lhs_module' => 'dtbc_Pricebook',
      'lhs_table' => 'dtbc_pricebook',
      'lhs_key' => 'id',
      'rhs_module' => 'dtbc_Pricebook_Product',
      'rhs_table' => 'dtbc_pricebook_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'dtbc_pricebook_dtbc_pricebook_product_1_c',
      'join_key_lhs' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
      'join_key_rhs' => 'dtbc_priced32cproduct_idb',
    ),
  ),
  'table' => 'dtbc_pricebook_dtbc_pricebook_product_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'dtbc_priced32cproduct_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'dtbc_pricebook_dtbc_pricebook_product_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'dtbc_pricebook_dtbc_pricebook_product_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'dtbc_pricebook_dtbc_pricebook_product_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'dtbc_priced32cproduct_idb',
      ),
    ),
  ),
);