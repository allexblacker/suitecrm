<?php
// created: 2018-02-26 16:40:15
$dictionary["spa_spa_spadi_spa_distributor_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'spa_spa_spadi_spa_distributor_1' => 
    array (
      'lhs_module' => 'SPA_SPA',
      'lhs_table' => 'spa_spa',
      'lhs_key' => 'id',
      'rhs_module' => 'SPADI_SPA_Distributor',
      'rhs_table' => 'spadi_spa_distributor',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'spa_spa_spadi_spa_distributor_1_c',
      'join_key_lhs' => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
      'join_key_rhs' => 'spa_spa_spadi_spa_distributor_1spadi_spa_distributor_idb',
    ),
  ),
  'table' => 'spa_spa_spadi_spa_distributor_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'spa_spa_spadi_spa_distributor_1spadi_spa_distributor_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'spa_spa_spadi_spa_distributor_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'spa_spa_spadi_spa_distributor_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'spa_spa_spadi_spa_distributor_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'spa_spa_spadi_spa_distributor_1spadi_spa_distributor_idb',
      ),
    ),
  ),
);