<?php
// created: 2017-09-11 18:28:20
$dictionary["cases_sn1_serial_number_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cases_sn1_serial_number_1' => 
    array (
      'lhs_module' => 'Cases',
      'lhs_table' => 'cases',
      'lhs_key' => 'id',
      'rhs_module' => 'SN1_Serial_Number',
      'rhs_table' => 'sn1_serial_number',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_sn1_serial_number_1_c',
      'join_key_lhs' => 'cases_sn1_serial_number_1cases_ida',
      'join_key_rhs' => 'cases_sn1_serial_number_1sn1_serial_number_idb',
    ),
  ),
  'table' => 'cases_sn1_serial_number_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cases_sn1_serial_number_1cases_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cases_sn1_serial_number_1sn1_serial_number_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cases_sn1_serial_number_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cases_sn1_serial_number_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_sn1_serial_number_1cases_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cases_sn1_serial_number_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cases_sn1_serial_number_1sn1_serial_number_idb',
      ),
    ),
  ),
);