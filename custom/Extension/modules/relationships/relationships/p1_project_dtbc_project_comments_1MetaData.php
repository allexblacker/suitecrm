<?php
// created: 2017-09-26 12:27:38
$dictionary["p1_project_dtbc_project_comments_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'p1_project_dtbc_project_comments_1' => 
    array (
      'lhs_module' => 'P1_Project',
      'lhs_table' => 'p1_project',
      'lhs_key' => 'id',
      'rhs_module' => 'dtbc_Project_Comments',
      'rhs_table' => 'dtbc_project_comments',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'p1_project_dtbc_project_comments_1_c',
      'join_key_lhs' => 'p1_project_dtbc_project_comments_1p1_project_ida',
      'join_key_rhs' => 'p1_project_dtbc_project_comments_1dtbc_project_comments_idb',
    ),
  ),
  'table' => 'p1_project_dtbc_project_comments_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'p1_project_dtbc_project_comments_1p1_project_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'p1_project_dtbc_project_comments_1dtbc_project_comments_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'p1_project_dtbc_project_comments_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'p1_project_dtbc_project_comments_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'p1_project_dtbc_project_comments_1p1_project_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'p1_project_dtbc_project_comments_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'p1_project_dtbc_project_comments_1dtbc_project_comments_idb',
      ),
    ),
  ),
);