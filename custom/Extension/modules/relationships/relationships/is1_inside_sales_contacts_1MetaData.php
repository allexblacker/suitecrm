<?php
// created: 2017-11-03 23:37:26
$dictionary["is1_inside_sales_contacts_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'is1_inside_sales_contacts_1' => 
    array (
      'lhs_module' => 'IS1_Inside_Sales',
      'lhs_table' => 'is1_inside_sales',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'is1_inside_sales_contacts_1_c',
      'join_key_lhs' => 'is1_inside_sales_contacts_1is1_inside_sales_ida',
      'join_key_rhs' => 'is1_inside_sales_contacts_1contacts_idb',
    ),
  ),
  'table' => 'is1_inside_sales_contacts_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'is1_inside_sales_contacts_1is1_inside_sales_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'is1_inside_sales_contacts_1contacts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'is1_inside_sales_contacts_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'is1_inside_sales_contacts_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'is1_inside_sales_contacts_1is1_inside_sales_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'is1_inside_sales_contacts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'is1_inside_sales_contacts_1contacts_idb',
      ),
    ),
  ),
);