<?php
// created: 2017-09-25 20:31:20
$dictionary["s1_site_p1_project_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    's1_site_p1_project_1' => 
    array (
      'lhs_module' => 'S1_Site',
      'lhs_table' => 's1_site',
      'lhs_key' => 'id',
      'rhs_module' => 'P1_Project',
      'rhs_table' => 'p1_project',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 's1_site_p1_project_1_c',
      'join_key_lhs' => 's1_site_p1_project_1s1_site_ida',
      'join_key_rhs' => 's1_site_p1_project_1p1_project_idb',
    ),
  ),
  'table' => 's1_site_p1_project_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 's1_site_p1_project_1s1_site_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 's1_site_p1_project_1p1_project_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 's1_site_p1_project_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 's1_site_p1_project_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 's1_site_p1_project_1s1_site_ida',
      ),
    ),
    2 => 
    array (
      'name' => 's1_site_p1_project_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 's1_site_p1_project_1p1_project_idb',
      ),
    ),
  ),
);