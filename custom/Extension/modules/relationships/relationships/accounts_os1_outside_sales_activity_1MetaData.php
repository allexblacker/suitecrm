<?php
// created: 2017-10-25 18:47:11
$dictionary["accounts_os1_outside_sales_activity_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_os1_outside_sales_activity_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'OS1_Outside_Sales_Activity',
      'rhs_table' => 'os1_outside_sales_activity',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_os1_outside_sales_activity_1_c',
      'join_key_lhs' => 'accounts_os1_outside_sales_activity_1accounts_ida',
      'join_key_rhs' => 'accounts_o61f2ctivity_idb',
    ),
  ),
  'table' => 'accounts_os1_outside_sales_activity_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'accounts_o61f2ctivity_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'accounts_os1_outside_sales_activity_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'accounts_os1_outside_sales_activity_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_os1_outside_sales_activity_1accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'accounts_os1_outside_sales_activity_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_o61f2ctivity_idb',
      ),
    ),
  ),
);