<?php
 // created: 2018-02-26 16:32:15
$layout_defs["Accounts"]["subpanel_setup"]['accounts_spa_spa_1'] = array (
  'order' => 100,
  'module' => 'SPA_SPA',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_SPA_SPA_TITLE',
  'get_subpanel_data' => 'accounts_spa_spa_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
