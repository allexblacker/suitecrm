<?php
 // created: 2017-11-01 02:15:14
$layout_defs["IS1_Inside_Sales"]["subpanel_setup"]['is1_inside_sales_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'is1_inside_sales_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
