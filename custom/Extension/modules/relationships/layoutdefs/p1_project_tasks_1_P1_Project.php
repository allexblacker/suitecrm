<?php
 // created: 2017-10-18 12:46:47
$layout_defs["P1_Project"]["subpanel_setup"]['p1_project_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_P1_PROJECT_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'p1_project_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
