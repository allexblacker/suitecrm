<?php
 // created: 2017-10-04 15:48:02
$layout_defs["Opportunities"]["subpanel_setup"]['opportunities_dtbc_line_item_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Line_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
  'get_subpanel_data' => 'opportunities_dtbc_line_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
