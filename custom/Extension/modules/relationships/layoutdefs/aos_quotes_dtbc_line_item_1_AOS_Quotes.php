<?php
 // created: 2017-10-16 12:24:52
$layout_defs["AOS_Quotes"]["subpanel_setup"]['aos_quotes_dtbc_line_item_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Line_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
  'get_subpanel_data' => 'aos_quotes_dtbc_line_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
