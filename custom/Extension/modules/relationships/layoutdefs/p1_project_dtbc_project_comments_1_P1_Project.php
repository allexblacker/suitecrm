<?php
 // created: 2017-09-26 12:27:38
$layout_defs["P1_Project"]["subpanel_setup"]['p1_project_dtbc_project_comments_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Project_Comments',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_DTBC_PROJECT_COMMENTS_TITLE',
  'get_subpanel_data' => 'p1_project_dtbc_project_comments_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
