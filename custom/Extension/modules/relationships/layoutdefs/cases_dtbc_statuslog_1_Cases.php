<?php
 // created: 2017-09-07 14:41:36
$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1'] = array (
  'order' => 100,
  'module' => 'dtbc_StatusLog',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE',
  'get_subpanel_data' => 'cases_dtbc_statuslog_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
