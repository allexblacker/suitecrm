<?php
// created: 2018-03-12 10:44:31
$dictionary["SPA_SPA"]["fields"]["spa_spa_documents_1"] = array (
  'name' => 'spa_spa_documents_1',
  'type' => 'link',
  'relationship' => 'spa_spa_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
