<?php
// created: 2017-09-26 11:42:36
$dictionary["P1_Project"]["fields"]["p1_project_documents_1"] = array (
  'name' => 'p1_project_documents_1',
  'type' => 'link',
  'relationship' => 'p1_project_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
