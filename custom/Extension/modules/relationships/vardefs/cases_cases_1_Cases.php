<?php
// created: 2017-08-29 18:26:46
$dictionary["Case"]["fields"]["cases_cases_1"] = array (
  'name' => 'cases_cases_1',
  'type' => 'link',
  'relationship' => 'cases_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_1_FROM_CASES_L_TITLE',
  'id_name' => 'cases_cases_1cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_1_name"] = array (
  'name' => 'cases_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_1_FROM_CASES_L_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_1cases_ida',
  'link' => 'cases_cases_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_1cases_ida"] = array (
  'name' => 'cases_cases_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_CASES_1_FROM_CASES_R_TITLE',
);
