<?php
// created: 2017-10-04 15:48:02
$dictionary["Opportunity"]["fields"]["opportunities_dtbc_line_item_1"] = array (
  'name' => 'opportunities_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'dtbc_Line_Item',
  'bean_name' => 'dtbc_Line_Item',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);
