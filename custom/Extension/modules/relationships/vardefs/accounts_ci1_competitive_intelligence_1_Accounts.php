<?php
// created: 2017-10-24 19:32:31
$dictionary["Account"]["fields"]["accounts_ci1_competitive_intelligence_1"] = array (
  'name' => 'accounts_ci1_competitive_intelligence_1',
  'type' => 'link',
  'relationship' => 'accounts_ci1_competitive_intelligence_1',
  'source' => 'non-db',
  'module' => 'CI1_Competitive_Intelligence',
  'bean_name' => 'CI1_Competitive_Intelligence',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE',
);
