<?php
// created: 2017-09-25 20:31:20
$dictionary["P1_Project"]["fields"]["s1_site_p1_project_1"] = array (
  'name' => 's1_site_p1_project_1',
  'type' => 'link',
  'relationship' => 's1_site_p1_project_1',
  'source' => 'non-db',
  'module' => 'S1_Site',
  'bean_name' => 'S1_Site',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_S1_SITE_TITLE',
  'id_name' => 's1_site_p1_project_1s1_site_ida',
);
$dictionary["P1_Project"]["fields"]["s1_site_p1_project_1_name"] = array (
  'name' => 's1_site_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_S1_SITE_TITLE',
  'save' => true,
  'id_name' => 's1_site_p1_project_1s1_site_ida',
  'link' => 's1_site_p1_project_1',
  'table' => 's1_site',
  'module' => 'S1_Site',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["s1_site_p1_project_1s1_site_ida"] = array (
  'name' => 's1_site_p1_project_1s1_site_ida',
  'type' => 'link',
  'relationship' => 's1_site_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
