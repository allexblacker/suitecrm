<?php
// created: 2017-10-04 15:59:42
$dictionary["AOS_Products"]["fields"]["aos_products_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_products_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook_Product',
  'bean_name' => 'dtbc_Pricebook_Product',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);
