<?php
// created: 2018-03-12 10:44:31
$dictionary["Document"]["fields"]["spa_spa_documents_1"] = array (
  'name' => 'spa_spa_documents_1',
  'type' => 'link',
  'relationship' => 'spa_spa_documents_1',
  'source' => 'non-db',
  'module' => 'SPA_SPA',
  'bean_name' => 'SPA_SPA',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_SPA_SPA_TITLE',
  'id_name' => 'spa_spa_documents_1spa_spa_ida',
);
$dictionary["Document"]["fields"]["spa_spa_documents_1_name"] = array (
  'name' => 'spa_spa_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_SPA_SPA_TITLE',
  'save' => true,
  'id_name' => 'spa_spa_documents_1spa_spa_ida',
  'link' => 'spa_spa_documents_1',
  'table' => 'spa_spa',
  'module' => 'SPA_SPA',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["spa_spa_documents_1spa_spa_ida"] = array (
  'name' => 'spa_spa_documents_1spa_spa_ida',
  'type' => 'link',
  'relationship' => 'spa_spa_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
