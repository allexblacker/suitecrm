<?php
// created: 2018-03-12 10:45:56
$dictionary["SPADI_SPA_Distributor"]["fields"]["spadi_spa_distributor_documents_1"] = array (
  'name' => 'spadi_spa_distributor_documents_1',
  'type' => 'link',
  'relationship' => 'spadi_spa_distributor_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
