<?php
// created: 2017-09-20 16:42:55
$dictionary["AL1_Alliance_Transaction"]["fields"]["accounts_al1_alliance_transaction_1"] = array (
  'name' => 'accounts_al1_alliance_transaction_1',
  'type' => 'link',
  'relationship' => 'accounts_al1_alliance_transaction_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_al1_alliance_transaction_1accounts_ida',
);
$dictionary["AL1_Alliance_Transaction"]["fields"]["accounts_al1_alliance_transaction_1_name"] = array (
  'name' => 'accounts_al1_alliance_transaction_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_al1_alliance_transaction_1accounts_ida',
  'link' => 'accounts_al1_alliance_transaction_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AL1_Alliance_Transaction"]["fields"]["accounts_al1_alliance_transaction_1accounts_ida"] = array (
  'name' => 'accounts_al1_alliance_transaction_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_al1_alliance_transaction_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE',
);
