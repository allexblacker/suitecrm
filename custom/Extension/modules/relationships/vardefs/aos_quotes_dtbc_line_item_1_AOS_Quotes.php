<?php
// created: 2017-10-16 12:24:53
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_dtbc_line_item_1"] = array (
  'name' => 'aos_quotes_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'dtbc_Line_Item',
  'bean_name' => 'dtbc_Line_Item',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);
