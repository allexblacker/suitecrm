<?php
// created: 2018-02-26 16:40:15
$dictionary["SPA_SPA"]["fields"]["spa_spa_spadi_spa_distributor_1"] = array (
  'name' => 'spa_spa_spadi_spa_distributor_1',
  'type' => 'link',
  'relationship' => 'spa_spa_spadi_spa_distributor_1',
  'source' => 'non-db',
  'module' => 'SPADI_SPA_Distributor',
  'bean_name' => 'SPADI_SPA_Distributor',
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
);
