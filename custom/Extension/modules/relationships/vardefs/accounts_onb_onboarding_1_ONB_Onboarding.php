<?php
// created: 2018-02-26 13:47:22
$dictionary["ONB_Onboarding"]["fields"]["accounts_onb_onboarding_1"] = array (
  'name' => 'accounts_onb_onboarding_1',
  'type' => 'link',
  'relationship' => 'accounts_onb_onboarding_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_onb_onboarding_1accounts_ida',
);
$dictionary["ONB_Onboarding"]["fields"]["accounts_onb_onboarding_1_name"] = array (
  'name' => 'accounts_onb_onboarding_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_onb_onboarding_1accounts_ida',
  'link' => 'accounts_onb_onboarding_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["ONB_Onboarding"]["fields"]["accounts_onb_onboarding_1accounts_ida"] = array (
  'name' => 'accounts_onb_onboarding_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_onb_onboarding_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ONB_ONBOARDING_TITLE',
);
