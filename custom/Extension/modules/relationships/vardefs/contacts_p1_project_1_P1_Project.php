<?php
// created: 2017-09-25 18:21:08
$dictionary["P1_Project"]["fields"]["contacts_p1_project_1"] = array (
  'name' => 'contacts_p1_project_1',
  'type' => 'link',
  'relationship' => 'contacts_p1_project_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_p1_project_1contacts_ida',
);
$dictionary["P1_Project"]["fields"]["contacts_p1_project_1_name"] = array (
  'name' => 'contacts_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_p1_project_1contacts_ida',
  'link' => 'contacts_p1_project_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["P1_Project"]["fields"]["contacts_p1_project_1contacts_ida"] = array (
  'name' => 'contacts_p1_project_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
