<?php
// created: 2017-10-18 10:13:10
$dictionary["POS1_POS_Tracking"]["fields"]["accounts_pos1_pos_tracking_1"] = array (
  'name' => 'accounts_pos1_pos_tracking_1',
  'type' => 'link',
  'relationship' => 'accounts_pos1_pos_tracking_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_pos1_pos_tracking_1accounts_ida',
);
$dictionary["POS1_POS_Tracking"]["fields"]["accounts_pos1_pos_tracking_1_name"] = array (
  'name' => 'accounts_pos1_pos_tracking_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_pos1_pos_tracking_1accounts_ida',
  'link' => 'accounts_pos1_pos_tracking_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["POS1_POS_Tracking"]["fields"]["accounts_pos1_pos_tracking_1accounts_ida"] = array (
  'name' => 'accounts_pos1_pos_tracking_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_pos1_pos_tracking_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE',
);
