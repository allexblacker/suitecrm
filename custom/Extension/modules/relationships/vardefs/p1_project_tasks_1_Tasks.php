<?php
// created: 2017-10-18 12:46:47
$dictionary["Task"]["fields"]["p1_project_tasks_1"] = array (
  'name' => 'p1_project_tasks_1',
  'type' => 'link',
  'relationship' => 'p1_project_tasks_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_P1_PROJECT_TITLE',
  'id_name' => 'p1_project_tasks_1p1_project_ida',
);
$dictionary["Task"]["fields"]["p1_project_tasks_1_name"] = array (
  'name' => 'p1_project_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_P1_PROJECT_TITLE',
  'save' => true,
  'id_name' => 'p1_project_tasks_1p1_project_ida',
  'link' => 'p1_project_tasks_1',
  'table' => 'p1_project',
  'module' => 'P1_Project',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["p1_project_tasks_1p1_project_ida"] = array (
  'name' => 'p1_project_tasks_1p1_project_ida',
  'type' => 'link',
  'relationship' => 'p1_project_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_TASKS_TITLE',
);
