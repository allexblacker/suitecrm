<?php
// created: 2017-09-25 18:12:29
$dictionary["Opportunity"]["fields"]["opportunities_p1_project_1"] = array (
  'name' => 'opportunities_p1_project_1',
  'type' => 'link',
  'relationship' => 'opportunities_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
