<?php
// created: 2018-02-26 16:32:15
$dictionary["Account"]["fields"]["accounts_spa_spa_1"] = array (
  'name' => 'accounts_spa_spa_1',
  'type' => 'link',
  'relationship' => 'accounts_spa_spa_1',
  'source' => 'non-db',
  'module' => 'SPA_SPA',
  'bean_name' => 'SPA_SPA',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_SPA_SPA_TITLE',
);
