<?php
// created: 2017-10-25 18:47:12
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["accounts_os1_outside_sales_activity_1"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1',
  'type' => 'link',
  'relationship' => 'accounts_os1_outside_sales_activity_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
);
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["accounts_os1_outside_sales_activity_1_name"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
  'link' => 'accounts_os1_outside_sales_activity_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["accounts_os1_outside_sales_activity_1accounts_ida"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_os1_outside_sales_activity_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
);
