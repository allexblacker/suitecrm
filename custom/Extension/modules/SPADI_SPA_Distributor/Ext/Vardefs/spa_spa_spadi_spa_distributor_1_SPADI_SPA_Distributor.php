<?php
// created: 2018-02-26 16:40:15
$dictionary["SPADI_SPA_Distributor"]["fields"]["spa_spa_spadi_spa_distributor_1"] = array (
  'name' => 'spa_spa_spadi_spa_distributor_1',
  'type' => 'link',
  'relationship' => 'spa_spa_spadi_spa_distributor_1',
  'source' => 'non-db',
  'module' => 'SPA_SPA',
  'bean_name' => 'SPA_SPA',
  'vname' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPA_SPA_TITLE',
  'id_name' => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
);
$dictionary["SPADI_SPA_Distributor"]["fields"]["spa_spa_spadi_spa_distributor_1_name"] = array (
  'name' => 'spa_spa_spadi_spa_distributor_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPA_SPA_TITLE',
  'save' => true,
  'id_name' => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
  'link' => 'spa_spa_spadi_spa_distributor_1',
  'table' => 'spa_spa',
  'module' => 'SPA_SPA',
  'rname' => 'name',
);
$dictionary["SPADI_SPA_Distributor"]["fields"]["spa_spa_spadi_spa_distributor_1spa_spa_ida"] = array (
  'name' => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
  'type' => 'link',
  'relationship' => 'spa_spa_spadi_spa_distributor_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
);
