<?php 

$layout_defs['AOW_WorkFlow']['subpanel_setup']['related_emailqueue'] = array(
	'order' => 50,
	'module' => 'emqu_dtbc_Email_Queue',
	'subpanel_name' => 'Foremqu_dtbc_Email_Queue',
	'get_subpanel_data' => 'function:get_subpanel_info',
	'generate_select' => true,
	'title_key' => 'LBL_RELATEDEMAILQUEUE',
	'top_buttons' => array(),
	'function_parameters' => array(
		'import_function_file' => 'custom/include/dtbc/WorkflowCustomSubpanel.php',
		'id' => $this->_focus->id,
		'return_as_array' => 'true'
	),
);
