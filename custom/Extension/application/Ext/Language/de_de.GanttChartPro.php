<?php

$app_list_strings['duration_unit_dom']['Days'] = 'Tage';
$app_list_strings['duration_unit_dom']['Hours'] = 'Stunden';
$app_list_strings['holiday_resource_dom']['Contacts'] = 'Kontakte';
$app_list_strings['holiday_resource_dom']['Users'] = 'Benutzer';
$app_list_strings['moduleList']['AM_ProjectHolidays'] = 'Projektfeiertage';
$app_list_strings['moduleList']['AM_ProjectTemplates'] = 'Projektvorlagen';
$app_list_strings['moduleList']['AM_TaskTemplates'] = 'Vorlagen Projektaufgaben';
$app_list_strings['relationship_type_list']['FS'] = 'Ende bis Anfang';
$app_list_strings['relationship_type_list']['SS'] = 'Anfang bis Anfang';
$app_strings['LBL_CREATE_PROJECT'] = 'Projekt erstellen';
$app_strings['LBL_GANTT_BUTTON_LABEL'] = 'Gantt ansehen';
$app_strings['LBL_GANTT_BUTTON_TITLE'] = 'Gantt ansehen';
