<?php

$app_list_strings['fp_event_invite_status_dom']['Attended'] = 'Teilgenommen';
$app_list_strings['fp_event_invite_status_dom']['Invited'] = 'Eingeladen';
$app_list_strings['fp_event_invite_status_dom']['Not Attended'] = 'Nicht teilgenommen';
$app_list_strings['fp_event_invite_status_dom']['Not Invited'] = 'Nicht eingeladen';
$app_list_strings['fp_event_status_dom']['Accepted'] = 'Akzeptiert';
$app_list_strings['fp_event_status_dom']['Declined'] = 'Abgelehnt';
$app_list_strings['fp_event_status_dom']['No Response'] = 'Keine Antwort';
$app_list_strings['invite_template_list'][''] = '';
$app_list_strings['moduleList']['FP_Event_Locations'] = 'Orte';
$app_list_strings['moduleList']['FP_events'] = 'Ereignisse';
$app_strings['LBL_ACCEPT_STATUS'] = 'Status akzeptieren';
$app_strings['LBL_LISTVIEW_NONE'] = 'Alle abwählen';
$app_strings['LBL_LISTVIEW_OPTION_CURRENT'] = 'Diese Seite auswählen';
$app_strings['LBL_LISTVIEW_OPTION_ENTIRE'] = 'Alle auswählen';
$app_strings['LBL_STATUS_EVENT'] = 'Einladungsstatus';
