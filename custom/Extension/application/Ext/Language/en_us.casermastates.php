<?php





$app_list_strings['casermastates'][''] = '';
$app_list_strings['casermastates']['Alabama'] = 'Alabama';
$app_list_strings['casermastates']['Alaska'] = 'Alaska';
$app_list_strings['casermastates']['Arizona'] = 'Arizona';
$app_list_strings['casermastates']['Arkansas'] = 'Arkansas';
$app_list_strings['casermastates']['California'] = 'California';
$app_list_strings['casermastates']['Colorado'] = 'Colorado';
$app_list_strings['casermastates']['Connecticut'] = 'Connecticut';
$app_list_strings['casermastates']['Delaware'] = 'Delaware';
$app_list_strings['casermastates']['DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['casermastates']['Florida'] = 'Florida';
$app_list_strings['casermastates']['Georgia'] = 'Georgia';
$app_list_strings['casermastates']['Hawaii'] = 'Hawaii';
$app_list_strings['casermastates']['Idaho'] = 'Idaho';
$app_list_strings['casermastates']['Illinois'] = 'Illinois';
$app_list_strings['casermastates']['Indiana'] = 'Indiana';
$app_list_strings['casermastates']['Iowa'] = 'Iowa';
$app_list_strings['casermastates']['Kansas'] = 'Kansas';
$app_list_strings['casermastates']['Kentucky'] = 'Kentucky';
$app_list_strings['casermastates']['Louisiana'] = 'Louisiana';
$app_list_strings['casermastates']['Maine'] = 'Maine';
$app_list_strings['casermastates']['Maryland'] = 'Maryland';
$app_list_strings['casermastates']['Massachusetts'] = 'Massachusetts';
$app_list_strings['casermastates']['Michigan'] = 'Michigan';
$app_list_strings['casermastates']['Minnesota'] = 'Minnesota';
$app_list_strings['casermastates']['Mississippi'] = 'Mississippi';
$app_list_strings['casermastates']['Missouri'] = 'Missouri';
$app_list_strings['casermastates']['Montana'] = 'Montana';
$app_list_strings['casermastates']['Nebraska'] = 'Nebraska';
$app_list_strings['casermastates']['Nevada'] = 'Nevada';
$app_list_strings['casermastates']['NewHampshire'] = 'New Hampshire';
$app_list_strings['casermastates']['NewJersey'] = 'New Jersey';
$app_list_strings['casermastates']['NewMexico'] = 'New Mexico';
$app_list_strings['casermastates']['NewYork'] = 'New York';
$app_list_strings['casermastates']['NorthCarolina'] = 'North Carolina';
$app_list_strings['casermastates']['NorthDakota'] = 'North Dakota';
$app_list_strings['casermastates']['Ohio'] = 'Ohio';
$app_list_strings['casermastates']['Oklahoma'] = 'Oklahoma';
$app_list_strings['casermastates']['Oregon'] = 'Oregon';
$app_list_strings['casermastates']['Pennsylvania'] = 'Pennsylvania';
$app_list_strings['casermastates']['RhodeIsland'] = 'Rhode Island';
$app_list_strings['casermastates']['SouthCarolina'] = 'South Carolina';
$app_list_strings['casermastates']['SouthDakota'] = 'South Dakota';
$app_list_strings['casermastates']['Tennessee'] = 'Tennessee';
$app_list_strings['casermastates']['Texas'] = 'Texas';
$app_list_strings['casermastates']['Utah'] = 'Utah';
$app_list_strings['casermastates']['Vermont'] = 'Vermont';
$app_list_strings['casermastates']['Virginia'] = 'Virginia';
$app_list_strings['casermastates']['Washington'] = 'Washington';
$app_list_strings['casermastates']['WestVirginia'] = 'West Virginia';
$app_list_strings['casermastates']['Wisconsin'] = 'Wisconsin';
$app_list_strings['casermastates']['Wyoming'] = 'Wyoming';
$app_list_strings['casermastates']['Alberta'] = 'Alberta';
$app_list_strings['casermastates']['British Columbia '] = 'British Columbia ';
$app_list_strings['casermastates']['Manitoba '] = 'Manitoba ';
$app_list_strings['casermastates']['NewBrunswick'] = 'New Brunswick';
$app_list_strings['casermastates']['NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['casermastates']['Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['casermastates']['Nunavut'] = 'Nunavut';
$app_list_strings['casermastates']['Ontario'] = 'Ontario';
$app_list_strings['casermastates']['PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['casermastates']['Quebec'] = 'Quebec';
$app_list_strings['casermastates']['Saskatchewan'] = 'Saskatchewan';
$app_list_strings['casermastates']['Yukon'] = 'Yukon';

