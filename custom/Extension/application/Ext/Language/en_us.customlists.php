<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$app_list_strings['categorylist'][''] = '';
$app_list_strings['categorylist']["It's a monitoring issue"] = "It's a monitoring issue";
$app_list_strings['categorylist']["Something is physically Broken or Missing"] = 'Something is physically Broken or Missing';
$app_list_strings['categorylist']['Something is burnt / vented'] = 'Something is burnt / vented';
$app_list_strings['categorylist']["It's a shipment issue / shipment related question"] = "It's a shipment issue / shipment related question";
$app_list_strings['categorylist']["It's a communication issue"] = "It's a communication issue";
$app_list_strings['categorylist']["It's a configuration issue"] = "It's a configuration issue";
$app_list_strings['categorylist']["There is an issue with one of the installer's applications"] = "There is an issue with one of the installer's applications";
$app_list_strings['categorylist']['There is an Error code'] = 'There is an Error code';
$app_list_strings['categorylist']['There is no error code, but something is wrong with the inverter'] = 'There is no error code, but something is wrong with the inverter';
$app_list_strings['categorylist']["It's a system design or wrong installation"] = "It's a system design or wrong installation";
$app_list_strings['categorylist']['The optimizer is not functioning as should'] = "The optimizer is not functioning as should";
$app_list_strings['categorylist']['There is a problem with one of the accessories'] = 'There is a problem with one of the accessories';
$app_list_strings['categorylist']['None of the above'] = 'None of the above';
$app_list_strings['categorylist']['Duplication'] = 'Duplication';
