<?php





$app_list_strings['contactstates'][''] = '';
$app_list_strings['contactstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['contactstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['contactstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['contactstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['contactstates']['Unitedstates_California'] = 'California';
$app_list_strings['contactstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['contactstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['contactstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['contactstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['contactstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['contactstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['contactstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['contactstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['contactstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['contactstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['contactstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['contactstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['contactstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['contactstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['contactstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['contactstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['contactstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['contactstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['contactstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['contactstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['contactstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['contactstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['contactstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['contactstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['contactstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['contactstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['contactstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['contactstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['contactstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['contactstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['contactstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['contactstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['contactstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['contactstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['contactstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['contactstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['contactstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['contactstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['contactstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['contactstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['contactstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['contactstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['contactstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['contactstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['contactstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['contactstates']['Unitedstates_Wyoming'] = 'Wyoming';


