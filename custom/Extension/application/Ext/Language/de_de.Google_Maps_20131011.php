<?php

$app_list_strings['map_module_type_list']['Accounts'] = 'Firmen';
$app_list_strings['map_module_type_list']['Cases'] = 'Fälle';
$app_list_strings['map_module_type_list']['Contacts'] = 'Kontakte';
$app_list_strings['map_module_type_list']['Leads'] = 'Interessenten';
$app_list_strings['map_module_type_list']['Meetings'] = 'Meetings';
$app_list_strings['map_module_type_list']['Opportunities'] = 'Verkaufschancen';
$app_list_strings['map_module_type_list']['Project'] = 'Projekte';
$app_list_strings['map_module_type_list']['Prospects'] = 'Zielkontakte';
$app_list_strings['map_relate_type_list']['Accounts'] = 'Firma';
$app_list_strings['map_relate_type_list']['Cases'] = 'Anfrage';
$app_list_strings['map_relate_type_list']['Contacts'] = 'Kontakt';
$app_list_strings['map_relate_type_list']['Leads'] = 'Interessent';
$app_list_strings['map_relate_type_list']['Meetings'] = 'Meeting';
$app_list_strings['map_relate_type_list']['Opportunities'] = 'Verkaufschance';
$app_list_strings['map_relate_type_list']['Project'] = 'Projekt';
$app_list_strings['map_relate_type_list']['Prospects'] = 'Zielkontakt';
$app_list_strings['map_unit_type_list']['km'] = 'Kilometer';
$app_list_strings['map_unit_type_list']['mi'] = 'Meilen';
$app_list_strings['marker_image_list']['accident'] = 'Unfall';
$app_list_strings['marker_image_list']['administration'] = 'Verwaltung';
$app_list_strings['marker_image_list']['agriculture'] = 'Landwirtschaft';
$app_list_strings['marker_image_list']['aircraft_small'] = 'Kleinflugzeug';
$app_list_strings['marker_image_list']['airplane_tourism'] = 'Charterflugzeug';
$app_list_strings['marker_image_list']['airport'] = 'AirPort';
$app_list_strings['marker_image_list']['amphitheater'] = 'Amphitheater';
$app_list_strings['marker_image_list']['apartment'] = 'Wohnung';
$app_list_strings['marker_image_list']['aquarium'] = 'Aquarium';
$app_list_strings['marker_image_list']['arch'] = 'Bogen';
$app_list_strings['marker_image_list']['atm'] = 'Bankomat';
$app_list_strings['marker_image_list']['audio'] = 'Audio';
$app_list_strings['marker_image_list']['bank'] = 'Bank';
$app_list_strings['marker_image_list']['bank_euro'] = 'Bank Euro';
$app_list_strings['marker_image_list']['bank_pound'] = 'Bank Pfund';
$app_list_strings['marker_image_list']['bar'] = 'Leiste';
$app_list_strings['marker_image_list']['beach'] = 'Strand';
$app_list_strings['marker_image_list']['beautiful'] = 'Schön';
$app_list_strings['marker_image_list']['bicycle_parking'] = 'Fahrrad Parkplatz';
$app_list_strings['marker_image_list']['big_city'] = 'Großstadt';
$app_list_strings['marker_image_list']['bridge'] = 'Brücke';
$app_list_strings['marker_image_list']['bridge_modern'] = 'Bridge-Modern';
$app_list_strings['marker_image_list']['bus'] = 'Bus';
$app_list_strings['marker_image_list']['cable_car'] = 'Straßenbahn';
$app_list_strings['marker_image_list']['car'] = 'Auto';
$app_list_strings['marker_image_list']['car_rental'] = 'Autovermietung';
$app_list_strings['marker_image_list']['carrepair'] = 'Werkstatt';
$app_list_strings['marker_image_list']['castle'] = 'Schloss';
$app_list_strings['marker_image_list']['cathedral'] = 'DOM';
$app_list_strings['marker_image_list']['chapel'] = 'Kapelle';
$app_list_strings['marker_image_list']['church'] = 'Kirche';
$app_list_strings['marker_image_list']['city_square'] = 'Platz';
$app_list_strings['marker_image_list']['cluster'] = 'Anhäufung';
$app_list_strings['marker_image_list']['cluster_2'] = 'Anhäufung 2';
$app_list_strings['marker_image_list']['cluster_3'] = 'Anhäufung 3';
$app_list_strings['marker_image_list']['cluster_4'] = 'Anhäufung 4';
$app_list_strings['marker_image_list']['cluster_5'] = 'Anhäufung 5';
$app_list_strings['marker_image_list']['coffee'] = 'Kaffee';
$app_list_strings['marker_image_list']['community_centre'] = 'Bürgerzentrum';
$app_list_strings['marker_image_list']['company'] = 'Unternehmen';
$app_list_strings['marker_image_list']['conference'] = 'Konferenz';
$app_list_strings['marker_image_list']['construction'] = 'Baugewerbe';
$app_list_strings['marker_image_list']['convenience'] = 'Convenience';
$app_list_strings['marker_image_list']['court'] = 'Gericht';
$app_list_strings['marker_image_list']['cruise'] = 'Cruise';
$app_list_strings['marker_image_list']['currency_exchange'] = 'Wechselstube';
$app_list_strings['marker_image_list']['customs'] = 'Zoll';
$app_list_strings['marker_image_list']['cycling'] = 'Fahrradfahren';
$app_list_strings['marker_image_list']['dam'] = 'DAM';
$app_list_strings['marker_image_list']['days_dim'] = 'Days Dim';
$app_list_strings['marker_image_list']['days_dom'] = 'Days Dom';
$app_list_strings['marker_image_list']['days_jeu'] = 'Days Jeu';
$app_list_strings['marker_image_list']['days_jue'] = 'Days Jue';
$app_list_strings['marker_image_list']['days_lun'] = 'Days Lun';
$app_list_strings['marker_image_list']['days_mar'] = 'Days Mar';
$app_list_strings['marker_image_list']['days_mer'] = 'Days Mer';
$app_list_strings['marker_image_list']['days_mie'] = 'Days Mie';
$app_list_strings['marker_image_list']['days_qua'] = 'Days Qua';
$app_list_strings['marker_image_list']['days_qui'] = 'Days Qui';
$app_list_strings['marker_image_list']['days_sab'] = 'Days Sab';
$app_list_strings['marker_image_list']['days_sam'] = 'Days Sam';
$app_list_strings['marker_image_list']['days_seg'] = 'Days Seg';
$app_list_strings['marker_image_list']['days_sex'] = 'Days Sex';
$app_list_strings['marker_image_list']['days_ter'] = 'Days Ter';
$app_list_strings['marker_image_list']['days_ven'] = 'Days Ven';
$app_list_strings['marker_image_list']['days_vie'] = 'Days Vie';
$app_list_strings['marker_image_list']['dentist'] = 'Zahnarzt';
$app_list_strings['marker_image_list']['deptartment_store'] = 'Supermarkt';
$app_list_strings['marker_image_list']['disability'] = 'Personen mit Behinderungen';
$app_list_strings['marker_image_list']['disabled_parking'] = 'Behindertenparkplaz';
$app_list_strings['marker_image_list']['doctor'] = 'Arzt';
$app_list_strings['marker_image_list']['dog_leash'] = 'Leine';
$app_list_strings['marker_image_list']['down'] = 'Nach unten';
$app_list_strings['marker_image_list']['down_left'] = 'Nach unten links';
$app_list_strings['marker_image_list']['down_right'] = 'Nach unten rechts';
$app_list_strings['marker_image_list']['down_then_left'] = 'Nach unten dann links';
$app_list_strings['marker_image_list']['down_then_right'] = 'Nach unten dann rechts';
$app_list_strings['marker_image_list']['drugs'] = 'Arzneimittel';
$app_list_strings['marker_image_list']['elevator'] = 'Lift';
$app_list_strings['marker_image_list']['embassy'] = 'Embassy';
$app_list_strings['marker_image_list']['expert'] = 'Experte';
$app_list_strings['marker_image_list']['factory'] = 'Factory';
$app_list_strings['marker_image_list']['falling_rocks'] = 'Steinschlag';
$app_list_strings['marker_image_list']['fast_food'] = 'Fast food';
$app_list_strings['marker_image_list']['festival'] = 'Fest';
$app_list_strings['marker_image_list']['fjord'] = 'Förde';
$app_list_strings['marker_image_list']['forest'] = 'Gesamtstruktur';
$app_list_strings['marker_image_list']['fountain'] = 'Quelle';
$app_list_strings['marker_image_list']['friday'] = 'Freitag';
$app_list_strings['marker_image_list']['garden'] = 'Garten';
$app_list_strings['marker_image_list']['gas_station'] = 'Tankstelle';
$app_list_strings['marker_image_list']['geyser'] = 'Springbrunnen';
$app_list_strings['marker_image_list']['gifts'] = 'Geschenke';
$app_list_strings['marker_image_list']['gourmet'] = 'Genießer';
$app_list_strings['marker_image_list']['grocery'] = 'Lebensmittel';
$app_list_strings['marker_image_list']['hairsalon'] = 'Friseur';
$app_list_strings['marker_image_list']['helicopter'] = 'Helikopter';
$app_list_strings['marker_image_list']['highway'] = 'Kabelführung';
$app_list_strings['marker_image_list']['historical_quarter'] = 'Historisches Stadtviertel';
$app_list_strings['marker_image_list']['home'] = 'Startseite';
$app_list_strings['marker_image_list']['hospital'] = 'Krankenhaus';
$app_list_strings['marker_image_list']['hostel'] = 'Hostel';
$app_list_strings['marker_image_list']['hotel'] = 'Hotel';
$app_list_strings['marker_image_list']['hotel_1_star'] = 'Hotel 1 Stern';
$app_list_strings['marker_image_list']['hotel_2_stars'] = 'Hotel 2 Sterne';
$app_list_strings['marker_image_list']['hotel_3_stars'] = 'Hotel 3 Sterne';
$app_list_strings['marker_image_list']['hotel_4_stars'] = 'Hotel 4 Sterne';
$app_list_strings['marker_image_list']['hotel_5_stars'] = 'Hotel 5 Sterne';
$app_list_strings['marker_image_list']['info'] = 'Information';
$app_list_strings['marker_image_list']['justice'] = 'Justiz';
$app_list_strings['marker_image_list']['lake'] = 'See';
$app_list_strings['marker_image_list']['laundromat'] = 'Waschsalon';
$app_list_strings['marker_image_list']['left'] = 'Nach links';
$app_list_strings['marker_image_list']['left_then_down'] = 'Links dann nach unten';
$app_list_strings['marker_image_list']['left_then_up'] = 'Links dann nach oben';
$app_list_strings['marker_image_list']['library'] = 'Bibliothek';
$app_list_strings['marker_image_list']['lighthouse'] = 'Von einem Leuchtturm';
$app_list_strings['marker_image_list']['liquor'] = 'Alkohol';
$app_list_strings['marker_image_list']['lock'] = 'Schloss';
$app_list_strings['marker_image_list']['main_road'] = 'Hauptstraße';
$app_list_strings['marker_image_list']['massage'] = 'Massage';
$app_list_strings['marker_image_list']['mobile_phone_tower'] = 'Mobilfunkmast';
$app_list_strings['marker_image_list']['modern_tower'] = 'Moderner  Turm';
$app_list_strings['marker_image_list']['monastery'] = 'Kloster';
$app_list_strings['marker_image_list']['monday'] = 'Montag';
$app_list_strings['marker_image_list']['monument'] = 'Denkmal';
$app_list_strings['marker_image_list']['mosque'] = 'Moschee';
$app_list_strings['marker_image_list']['motorcycle'] = 'Motorrad:';
$app_list_strings['marker_image_list']['museum'] = 'Nordsee';
$app_list_strings['marker_image_list']['music_live'] = 'Live-Musik';
$app_list_strings['marker_image_list']['oil_pump_jack'] = 'Ölförderanlage';
$app_list_strings['marker_image_list']['pagoda'] = 'Pagode';
$app_list_strings['marker_image_list']['palace'] = 'Hamburg';
$app_list_strings['marker_image_list']['panoramic'] = 'Panorama';
$app_list_strings['marker_image_list']['park'] = 'Park';
$app_list_strings['marker_image_list']['park_and_ride'] = 'Park & Ride';
$app_list_strings['marker_image_list']['parking'] = 'Parken';
$app_list_strings['marker_image_list']['photo'] = 'Foto';
$app_list_strings['marker_image_list']['picnic'] = 'Picknick';
$app_list_strings['marker_image_list']['places_unvisited'] = 'Plätze nicht besucht';
$app_list_strings['marker_image_list']['places_visited'] = 'Plätze besucht';
$app_list_strings['marker_image_list']['playground'] = 'Effekt "Partikelsimulation"';
$app_list_strings['marker_image_list']['police'] = 'Polizei';
$app_list_strings['marker_image_list']['port'] = 'Anschluss';
$app_list_strings['marker_image_list']['postal'] = 'Post';
$app_list_strings['marker_image_list']['power_line_pole'] = 'Strommast';
$app_list_strings['marker_image_list']['power_plant'] = 'Kraftwerk';
$app_list_strings['marker_image_list']['power_substation'] = 'Umspannwerk';
$app_list_strings['marker_image_list']['public_art'] = 'Öffentliche Kunst';
$app_list_strings['marker_image_list']['rain'] = 'Regen';
$app_list_strings['marker_image_list']['real_estate'] = 'Immobilien';
$app_list_strings['marker_image_list']['regroup'] = 'Neu gruppieren';
$app_list_strings['marker_image_list']['resort'] = 'Urlaubsort';
$app_list_strings['marker_image_list']['restaurant'] = 'Restaurant';
$app_list_strings['marker_image_list']['restaurant_african'] = 'Afrikanische Restaurant';
$app_list_strings['marker_image_list']['restaurant_barbecue'] = 'Restaurant Grill';
$app_list_strings['marker_image_list']['restaurant_buffet'] = 'Restaurant Buffet';
$app_list_strings['marker_image_list']['restaurant_chinese'] = 'Restaurant Chinesisch';
$app_list_strings['marker_image_list']['restaurant_fish'] = 'Restaurant Fisch';
$app_list_strings['marker_image_list']['restaurant_fish_chips'] = 'Restaurant Fisch und Chips';
$app_list_strings['marker_image_list']['restaurant_gourmet'] = 'Restaurant Gourmet';
$app_list_strings['marker_image_list']['restaurant_greek'] = 'Restaurant-Griechisch';
$app_list_strings['marker_image_list']['restaurant_indian'] = 'Restaurant Indisch';
$app_list_strings['marker_image_list']['restaurant_italian'] = 'Restaurant-Italienisch';
$app_list_strings['marker_image_list']['restaurant_japanese'] = 'Restaurant-Japanisch';
$app_list_strings['marker_image_list']['restaurant_kebab'] = 'Restaurant-Kebab';
$app_list_strings['marker_image_list']['restaurant_korean'] = 'Restaurant Koreanisch';
$app_list_strings['marker_image_list']['restaurant_mediterranean'] = 'Restaurant Mediterran';
$app_list_strings['marker_image_list']['restaurant_mexican'] = 'Restaurant Mexikanisch';
$app_list_strings['marker_image_list']['restaurant_romantic'] = 'Restaurant romantische';
$app_list_strings['marker_image_list']['restaurant_thai'] = 'Restaurant Thailändisch';
$app_list_strings['marker_image_list']['restaurant_turkish'] = 'Restaurant Türkisch';
$app_list_strings['marker_image_list']['right'] = 'Rechts';
$app_list_strings['marker_image_list']['right_then_down'] = 'Rechts dann nach unten';
$app_list_strings['marker_image_list']['right_then_up'] = 'Rechts dann nach oben';
$app_list_strings['marker_image_list']['satursday'] = 'Samstag';
$app_list_strings['marker_image_list']['school'] = 'Schule';
$app_list_strings['marker_image_list']['shopping_mall'] = 'Einkaufszentrum';
$app_list_strings['marker_image_list']['shore'] = 'Ufer';
$app_list_strings['marker_image_list']['sight'] = 'Aussicht';
$app_list_strings['marker_image_list']['small_city'] = 'Kleine Stadt';
$app_list_strings['marker_image_list']['snow'] = 'Schnee';
$app_list_strings['marker_image_list']['spaceport'] = 'Raumfahrtzentrum';
$app_list_strings['marker_image_list']['speed_100'] = 'Geschwindigkeit 100';
$app_list_strings['marker_image_list']['speed_110'] = 'Geschwindigkeit 110';
$app_list_strings['marker_image_list']['speed_120'] = 'Geschwindigkeit von 120';
$app_list_strings['marker_image_list']['speed_130'] = 'Geschwindigkeit 130';
$app_list_strings['marker_image_list']['speed_20'] = 'Geschwindigkeit 20';
$app_list_strings['marker_image_list']['speed_30'] = 'Geschwindigkeit 30';
$app_list_strings['marker_image_list']['speed_40'] = 'Geschwindigkeit 40';
$app_list_strings['marker_image_list']['speed_50'] = 'Geschwindigkeit 50';
$app_list_strings['marker_image_list']['speed_60'] = 'Geschwindigkeit 60';
$app_list_strings['marker_image_list']['speed_70'] = 'Geschwindigkeit 70';
$app_list_strings['marker_image_list']['speed_80'] = 'Geschwindigkeit 80';
$app_list_strings['marker_image_list']['speed_90'] = 'Geschwindigkeit 90';
$app_list_strings['marker_image_list']['speed_hump'] = 'Rüttelschwelle';
$app_list_strings['marker_image_list']['stadium'] = 'Stadium';
$app_list_strings['marker_image_list']['statue'] = 'Statue';
$app_list_strings['marker_image_list']['steam_train'] = 'Dampfzug';
$app_list_strings['marker_image_list']['stop'] = 'Beenden';
$app_list_strings['marker_image_list']['stoplight'] = 'Ampel';
$app_list_strings['marker_image_list']['subway'] = 'U-Bahn';
$app_list_strings['marker_image_list']['sun'] = 'Sonne';
$app_list_strings['marker_image_list']['sunday'] = 'Sonntag';
$app_list_strings['marker_image_list']['supermarket'] = 'Supermarkt';
$app_list_strings['marker_image_list']['synagogue'] = 'Synagoge';
$app_list_strings['marker_image_list']['tapas'] = 'Tapas';
$app_list_strings['marker_image_list']['taxi'] = 'Taxi';
$app_list_strings['marker_image_list']['taxiway'] = 'Taxispur';
$app_list_strings['marker_image_list']['teahouse'] = 'Teehaus';
$app_list_strings['marker_image_list']['telephone'] = 'Telefon';
$app_list_strings['marker_image_list']['temple_hindu'] = 'Hindu Tempel';
$app_list_strings['marker_image_list']['terrace'] = 'Terrasse';
$app_list_strings['marker_image_list']['text'] = 'Text';
$app_list_strings['marker_image_list']['theater'] = 'Theater';
$app_list_strings['marker_image_list']['theme_park'] = 'Freizeitpark';
$app_list_strings['marker_image_list']['thursday'] = 'Donnerstag';
$app_list_strings['marker_image_list']['toilets'] = 'Toiletten';
$app_list_strings['marker_image_list']['toll_station'] = 'Mautstation';
$app_list_strings['marker_image_list']['tower'] = 'Tower';
$app_list_strings['marker_image_list']['traffic_enforcement_camera'] = 'Verkehrskamera';
$app_list_strings['marker_image_list']['train'] = 'Zug';
$app_list_strings['marker_image_list']['tram'] = 'Straßenbahn';
$app_list_strings['marker_image_list']['truck'] = 'Lastwagen';
$app_list_strings['marker_image_list']['tuesday'] = 'Dienstag';
$app_list_strings['marker_image_list']['tunnel'] = 'Tunnel';
$app_list_strings['marker_image_list']['turn_left'] = 'Links abbiegen';
$app_list_strings['marker_image_list']['turn_right'] = 'Rechts abbiegen';
$app_list_strings['marker_image_list']['university'] = 'Universität';
$app_list_strings['marker_image_list']['up'] = 'Nach oben';
$app_list_strings['marker_image_list']['up_left'] = 'Nach oben links';
$app_list_strings['marker_image_list']['up_right'] = 'Nach oben rechts';
$app_list_strings['marker_image_list']['up_then_left'] = 'Nach oben dann links';
$app_list_strings['marker_image_list']['up_then_right'] = 'Nach oben dann rechts';
$app_list_strings['marker_image_list']['vespa'] = 'Vespa';
$app_list_strings['marker_image_list']['video'] = 'Video';
$app_list_strings['marker_image_list']['villa'] = 'Die Villa';
$app_list_strings['marker_image_list']['water'] = 'Wasser';
$app_list_strings['marker_image_list']['waterfall'] = 'Wasserfall';
$app_list_strings['marker_image_list']['watermill'] = 'Mühle';
$app_list_strings['marker_image_list']['waterpark'] = 'Wasserpark';
$app_list_strings['marker_image_list']['watertower'] = 'Wasserturm';
$app_list_strings['marker_image_list']['wednesday'] = 'Mittwoch';
$app_list_strings['marker_image_list']['wifi'] = 'W-LAN';
$app_list_strings['marker_image_list']['wind_turbine'] = 'Windturbine';
$app_list_strings['marker_image_list']['windmill'] = 'Windrad';
$app_list_strings['marker_image_list']['winery'] = 'Winzer';
$app_list_strings['marker_image_list']['work_office'] = 'Büro';
$app_list_strings['marker_image_list']['world_heritage_site'] = 'Weltkulturerbe';
$app_list_strings['marker_image_list']['zoo'] = 'Zoo';
$app_list_strings['moduleList']['jjwg_Address_Cache'] = 'Kartenadressen Cache';
$app_list_strings['moduleList']['jjwg_Areas'] = 'Kartengebiete';
$app_list_strings['moduleList']['jjwg_Maps'] = 'Karten';
$app_list_strings['moduleList']['jjwg_Markers'] = 'Kartenmarker';
$app_strings['LBL_BUG_FIX'] = 'Fehlerbehebung';
$app_strings['LBL_JJWG_MAPS_ADDRESS'] = 'Adresse';
$app_strings['LBL_JJWG_MAPS_GEOCODE_STATUS'] = 'Geocode Status';
$app_strings['LBL_JJWG_MAPS_LAT'] = 'Latitude';
$app_strings['LBL_JJWG_MAPS_LNG'] = 'Länge';
$app_strings['LBL_MAP'] = 'Anzeigen';
$app_strings['LBL_MAPS'] = 'Karten';
