<?php
    $entry_point_registry['casesSurvey'] = array(
        'file' => 'custom/modules/dtbc_CasesSurvey/dtbc/EntryPoints/survey.php',
        'auth' => false
    );

    $entry_point_registry['saveSurvey'] = array(
        'file' => 'custom/modules/dtbc_CasesSurvey/dtbc/EntryPoints/surveySave.php',
        'auth' => false
    );
?>