<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(500, '', 'modules/dtbc_Approvals/logic_hook.php', 'ApprovalsHook', 'after_save'); 