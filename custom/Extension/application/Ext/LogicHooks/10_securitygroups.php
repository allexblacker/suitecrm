<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['process_record']) || !is_array($hook_array['process_record'])) {
    $hook_array['process_record'] = array();
}
$hook_array['process_record'][] = Array(90, '', 'custom/include/dtbc/hooks/securitygroups.php','GlobalProcessRecordHooks', 'displaySecurityGroupOnListView');