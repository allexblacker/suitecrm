<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'service/v4_1/registry.php';
class registry_v4_1_custom extends registry_v4_1
{
    protected function registerFunction()
    {
        parent::registerFunction();
        $this->serviceClass->registerFunction('getDepandencesTable',
        array(
        'session'=>'xsd:string',
        'message'=>'xsd:string'),
        array(
         'return'=>'xsd:boolean')
        );
        $this->serviceClass->registerFunction('insertNewCaseSerial',
        array(
        'session'=>'xsd:string',
        'caseNo' => 'xsd:string',
        'serialNo' => 'xsd:string',
        'message'=>'xsd:string'),
        array(
         'return'=>'xsd:boolean')
        );
    }
}