<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
//$date = new DateTime();
//echo $date->format('r');

require_once 'modules/ModuleBuilder/views/view.modulefields.php';

//do the function here.
global $db;
global $dictionary;
global $app_list_strings;
global $moduleList; //the list of modules;

foreach($moduleList as $module){ //iterating through the list of modules
   $bean = BeanFactory::newBean($module); //creating a bean
   if(!empty($bean)){
        $fieldDefinitons = $bean->getFieldDefinitions(); //getting field definitions of a bean
        //echo "<pre>";
        //print_r($fieldDefinitons);
        //echo "</pre>";
        foreach($fieldDefinitons as $key => $value){
            if(($value['type'] == 'enum' || $value['type'] == 'dynamicenum') && $value['source'] != 'non-db'){     //if the field is an enum (dropdown), add to the enums array, exlude non-db fields!
                $fieldname = $key;
                $listname = $value['options'];
                $list = array();
                if (isset($app_list_strings[$listname]))
                {
                    $list = $app_list_strings[$listname];
                    foreach ($list as $key => $value)
                    {
                        //find if record exist
                        if($value != '')
                        {
                            $sql = "SELECT kob_value FROM kob_fieldlist WHERE kob_fieldname = '" . $fieldname . "' AND kob_key = '" . $key . "'";
                            //echo $sql . "<BR/>";
                            $result = $db->query($sql);
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    foreach($row as $k => $v)
                                    {
                                       //echo $k . " - " . $v . "<BR/>";
                                       if($v != $value)
                                       {
                                           $sql = "UPDATE kob_fieldlist SET kob_value = '" . $value . "' WHERE kob_fieldname = '" . $fieldname . "' AND kob_key = '" . $key . "'";
                                       }
                                    }
                                }
                            }
                            else {
                                $sql = "INSERT INTO kob_fieldlist(idkob_fieldlist, kob_fieldname, kob_listname, kob_key, kob_value)
                                        VALUES (UUID(),'" . $fieldname . "','" . $listname . "','" . $key . "','" . $value . "')";
                                $db->query($sql);
                                //echo $sql . "<BR/>";
                                //echo '$module' . $module . 'Field Name: ' . $fieldname . 'List Name: ' . $listname . 'Key: ' . $key .'Value: ' . $value . '<BR/>';
                            }
                        }
                    }
                }
            }
        }
   }
//}



//$moduleList = array_intersect($GLOBALS['moduleList'],array_keys($GLOBALS['beanList']));
//foreach($moduleList as $module)
//{
    //echo $module . '<BR/>';
    //$bean = BeanFactory::getBean('Cases');
    //$field_defs[$module] = $bean->getFieldDefinitions();
    //echo "<pre>";
    //print_r($field_defs[$module]);
    //echo "</pre>";
   
    
    //$viewmodfields = new ViewModulefields();
    //$objectName = BeanFactory::getObjectName($module);
    //VardefManager::loadVardef($module, $objectName, true);

    //$fieldsData = array();
    //foreach($dictionary[$objectName]['fields'] as $def) {
        //echo "<pre>";
        //print_r($def);
        //echo "</pre>";
        //if($def['type'] == 'enum' || $def['type'] == 'dynamicenum')
        //{

            //$fieldname = $def['name'];
            //$listname = $def['options'];
            //print_r($mod_field);
            //echo "<pre>";
            //print_r($mod_field);
            //echo "</pre>";

//            $list = array();
//            if (isset($app_list_strings[$listname]))
//            {
//                $list = $app_list_strings[$listname];
//                foreach ($list as $key => $value)
//                {
//                    //$sql = "INSERT INTO kob_fieldlist(idkob_fieldlist, kob_fieldname, kob_listname, kob_key, kob_value)
//                    //	VALUES (UUID(),'" . $fieldname . "','" . $listname . "','" . $key . "','" . $value . "')";
//                    //echo $sql;
//                    //$db->query($sql);
//                    //echo '$module' . $module . 'Field Name: ' . $fieldname . 'List Name: ' . $listname . 'Key: ' . $key .'Value: ' . $value . '<BR/>';
//                }
//            }
            //echo "<pre>";
            //print_r($list);
            //echo "</pre>";
        //}
    //}
}
