<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer ID',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_SECURITYGROUPS' => 'Sicherheitsgruppen',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Sicherheitsgruppen',
  'LBL_CREATED' => 'Erstellt von',
  'LBL_CREATED_ID' => 'Erstellt von Id',
  'LBL_CREATED_USER' => 'Erstellt von Benutzer',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von Id',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_MODIFIED_USER' => 'Geändert von Benutzer',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_LIST_FORM_TITLE' => 'Onboarding Liste',
  'LBL_MODULE_NAME' => 'Onboarding',
  'LBL_MODULE_TITLE' => 'Onboarding',
  'LBL_HOMEPAGE_TITLE' => 'Mein Onboarding',
  'LNK_NEW_RECORD' => 'Erstellen Onboarding',
  'LNK_LIST' => 'Ansicht Onboarding',
  'LNK_IMPORT_ONB_ONBOARDING' => 'Import Onboarding',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Onboarding',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf anzeigen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten',
  'LBL_ONB_ONBOARDING_SUBPANEL_TITLE' => 'Onboarding',
  'LBL_NEW_FORM_TITLE' => 'Neue Onboarding',
  'LBL_CONTACT_CONTACT_ID' => 'Contact (related Contact ID)',
  'LBL_CONTACT' => 'Contact',
  'LBL_ONBOARDING_STATUS' => 'Onboarding Status',
  'LBL_ROLE' => 'Role',
  'LBL_ONBOARDING_CALL' => 'Onboarding Call',
  'LBL_SUMMARY_NEXT_STEPS' => 'Summary Next Steps',
);