<?php
 // created: 2017-08-31 12:51:23
$layout_defs["S1_Site"]["subpanel_setup"]['s1_site_cases'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_S1_SITE_CASES_FROM_CASES_TITLE',
  'get_subpanel_data' => 's1_site_cases',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
