<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => '担当ユーザID',
  'LBL_ASSIGNED_TO_NAME' => '担当ユーザ',
  'LBL_SECURITYGROUPS' => 'セキュリティグループ',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'セキュリティグループ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '生成ID',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '生成ユーザ',
  'LBL_MODIFIED_USER' => '更新ユーザ',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_LIST_FORM_TITLE' => 'SPA 一覧',
  'LBL_MODULE_NAME' => 'SPA',
  'LBL_MODULE_TITLE' => 'SPA',
  'LBL_HOMEPAGE_TITLE' => '私の SPA',
  'LNK_NEW_RECORD' => 'の作成 SPA',
  'LNK_LIST' => '画面 SPA',
  'LNK_IMPORT_SPA_SPA' => 'Import SPA',
  'LBL_SEARCH_FORM_TITLE' => 'フィルタ SPA',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動',
  'LBL_SPA_SPA_SUBPANEL_TITLE' => 'SPA',
  'LBL_NEW_FORM_TITLE' => '新規 SPA',
  'LBL_ACCOUNT' => 'Account',
  'LBL_SPA_REGION' => 'spa region',
  'LBL_DIRECTOR_USER_ID' => 'Director (related User ID)',
  'LBL_DIRECTOR' => 'Director',
  'LBL_RSM_USER_ID' => 'RSM (related User ID)',
  'LBL_RSM' => 'RSM',
  'LBL_SPA_STATUS' => 'spa status',
  'LBL_SPA_TYPE' => 'spa Type',
  'LBL_INSTALLER_SPA_TYPE' => 'Installer SPA Type',
  'LBL_INSTALLER_SPA_START_DATE' => 'Installer SPA Start Date',
  'LBL_INSTALLER_SPA_END_DATE' => 'Installer SPA End Date',
  'LBL_INSTALLER_SPA_LEVEL' => 'Installer SPA Level',
  'LBL_INSTALLER_SPA_WDC' => 'Installer SPA $/Wdc',
  'LBL_COMMUNICATION_TYPE' => 'Communication Type',
  'LBL_MARKETING_FUND' => 'Marketing Fund',
  'LBL_Q1_18_TARGET_KW' => 'Q1&#039;18 Target KW',
  'LBL_Q2_18_TARGET_KW' => 'Q2&#039;18 Target KW',
  'LBL_Q3_18_TARGET_KW' => 'Q3&#039;18 Target KW',
  'LBL_Q4_18_TARGET_KW' => 'q4 18 target kw',
  'LBL_Q1_18_MARKETING_FUND' => 'Q1&#039;18 Marketing Fund',
  'LBL_Q2_18_MARKETING_FUND' => 'Q2 &#039;18 Marketing Fund',
  'LBL_Q3_18_MARKETING_FUND' => 'Q3&#039;18 Marketing Fund',
  'LBL_Q4_18_MARKETING_FUND' => 'Q4&#039;18 Marketing Fund',
  'LBL_DISTRIBUTOR_1' => 'Distributor 1',
  'LBL_DISTRIBUTOER_1_PROGRAM_NUMBER' => 'Distributoer 1 Program Number',
  'LBL_DISTRIBUTOER_1_SPA_START_DATE' => 'Distributoer 1 SPA Start Date',
  'LBL_DISTRIBUTOR_1_SPA_END_DATE' => 'Distributor 1 SPA End Date',
  'LBL_DISTRIBUTOR_2' => 'Distributor 2',
  'LBL_DISTRIBUTOR_2_PROGRAM_NUMBER' => 'Distributor 2 Program Number',
  'LBL_DISTRIBUTOR_2_SPA_START_DATE' => 'Distributor 2 SPA Start Date',
  'LBL_DISTRIBUTOR_2_SPA_END_DATE' => 'distributor 2 spa end date',
  'LBL_DISTRIBUTOR_3' => 'Distributor 3',
  'LBL_DISTRIBUTOR_3_PROGRAM_NUMBER' => 'distributor 3 program Number',
  'LBL_DISTRIBUTOR_3_SPA_START_DATE' => 'distributor 3 spa start date',
  'LBL_DISTRIBUTOR_3_SPA_END_DATE' => 'distributor 3 spa end date',
  'LBL_NOTES_COMMENTS' => 'Notes/Comments',
);