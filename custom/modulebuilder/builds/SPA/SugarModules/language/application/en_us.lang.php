<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['SPA_SPA'] = 'SPA';
$app_list_strings['spa_region_list'][''] = '';
$app_list_strings['spa_region_list']['East'] = 'East';
$app_list_strings['spa_region_list']['West'] = 'West';
$app_list_strings['spa_region_list']['Central'] = 'Central';
$app_list_strings['spa_status_list'][''] = '';
$app_list_strings['spa_status_list']['Internal_review'] = 'Internal Review';
$app_list_strings['spa_status_list']['Installer_SPA_Signature'] = 'Installer SPA Signature';
$app_list_strings['spa_status_list']['Installer_SPA_Counter_Signature'] = 'Installer SPA Counter Signature';
$app_list_strings['spa_status_list']['Installer_SPA_Complete'] = 'Installer SPA Complete';
$app_list_strings['spa_status_list']['Ditribution_Notification'] = 'Distribution Notification';
$app_list_strings['spa_status_list']['Complete'] = 'Complete';
$app_list_strings['spa_status_list']['SPA_Termination'] = 'SPA Termination';
$app_list_strings['spa_status_list']['Not_renewing'] = 'Not Renewing';
$app_list_strings['spa_type_list'][''] = '';
$app_list_strings['spa_type_list']['New'] = 'New';
$app_list_strings['spa_type_list']['Renew'] = 'Renew';
$app_list_strings['spa_type_list']['Growth'] = 'Growth';
$app_list_strings['spa_type_list']['N_A'] = 'N-A';
$app_list_strings['installer_spa_type_list'][''] = '';
$app_list_strings['installer_spa_type_list']['Residential'] = 'Residential';
$app_list_strings['installer_spa_type_list']['Commercial'] = 'Commercial';
$app_list_strings['installer_spa_type_list']['Hybrid'] = 'Hybrid';
$app_list_strings['installer_spa_level_list'][''] = '';
$app_list_strings['installer_spa_level_list']['Level_A1'] = 'Level A %';
$app_list_strings['installer_spa_level_list']['Level_A2'] = 'Level A $';
$app_list_strings['installer_spa_level_list']['Level_B'] = 'Level B';
$app_list_strings['installer_spa_level_list']['Level_C'] = 'Level C';
$app_list_strings['installer_spa_level_list']['Level_D'] = 'Level D';
$app_list_strings['installer_spa_level_list']['Level_E'] = 'Level E';
$app_list_strings['installer_spa_level_list']['Comercial'] = 'Commercial';
$app_list_strings['communication_type_0'][''] = '';
$app_list_strings['communication_type_0']['Zigbee'] = 'Zigbee';
$app_list_strings['communication_type_0']['5Yr_GSM'] = '5yr. GSM';
$app_list_strings['communication_type_0']['12ys_GSM'] = '12yr. GSM';
$app_list_strings['communication_type_0']['None'] = 'None';
$app_list_strings['marketing_fund_list'][''] = '';
$app_list_strings['marketing_fund_list']['Yes'] = 'Yes';
$app_list_strings['marketing_fund_list']['No'] = 'No';
