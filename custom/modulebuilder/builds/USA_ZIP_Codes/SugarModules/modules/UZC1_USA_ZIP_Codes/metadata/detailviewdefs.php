<?php
$module_name = 'UZC1_USA_ZIP_Codes';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'zip',
            'label' => 'ZIP',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'state',
            'label' => 'State',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'county',
            'label' => 'County',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'city',
            'label' => 'City',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'canadian_province',
            'label' => 'Canadian_Province',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'place_name_canada_only',
            'label' => 'Place_Name_Canada_only',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
