<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/ListView/ListViewSubPanel.php');
require_once('include/SubPanel/SubPanel.php');
/**
 * Subpanel
 * @api
 */
class CustomSubPanel extends SubPanel {
	public function buildSearchQuery() {
		$thisPanel =& $this->subpanel_defs;
		$subpanel_defs = $thisPanel->_instance_properties;

		require_once('include/SubPanel/SubPanelSearchForm.php');

		if (isset($subpanel_defs['type']) && $subpanel_defs['type'] == 'collection') {
			$arrayValues = array_values($subpanel_defs['collection_list']);
			$collection = array_shift($arrayValues);
			$module = $collection['module'];
		} else {
			$module = $subpanel_defs['module'];
		}
		if($module) {
			$seed = BeanFactory::getBean($module);
		} else {
			$seed = new Meeting();
		}

		$_REQUEST['searchFormTab'] = 'basic_search';
		$searchForm = new SubPanelSearchForm($seed, $module, $this);

		$searchMetaData = $searchForm->retrieveSearchDefs($module);
		// Skip subpanel if defined in the config file's disabled array
		if ($this->subpanelEnabled($this->subpanel_id)) {
			$searchForm->setup($searchMetaData['searchdefs'], $searchMetaData['searchFields'], 'SubpanelSearchFormGeneric.tpl', 'basic_search');
		}

		$searchForm->populateFromRequest();

		$where_clauses = $searchForm->generateSearchWhere(true, $seed->module_dir);

		if (count($where_clauses) > 0 ) {
			$this->search_query = '('. implode(' ) AND ( ', $where_clauses) . ')';
		}
		$GLOBALS['log']->info("Subpanel Where Clause: $this->search_query");
	}
	
	private function subpanelEnabled($subpanelId) {
		global $sugar_config;
	
		if (isset($sugar_config['solaredge']) && isset($sugar_config['solaredge']['disabled_subpanel_filters']) && is_array($sugar_config['solaredge']['disabled_subpanel_filters']))
			return !in_array($subpanelId, $sugar_config['solaredge']['disabled_subpanel_filters']);
		
		return true;
	}
}
?>
