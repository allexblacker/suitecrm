<?php


require_once('custom/include/TemplateHandler/TemplateHandlerCustomCache.php');
require_once('include/EditView/EditView2.php');

/**
 * New EditView
 * @api
 */
class EditViewCustomCache extends EditView {
    protected function getTemplateHandler()
    {
        return new TemplateHandlerCustomCache();
    }
}

