<?php

if(!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');


class CustomMassUpdate{

    private $field;
    private $displayName;

    public function __construct($field, $displayName){

        $this->field = $field;
        $this->displayName = $displayName;
    }

    public function doAction(){

        switch($this->field['type']) {

            case 'decimal':
                return $this->addDecimalField();
            default:
                return '';
        }
    }

    private function addDecimalField(){

        $varName = $this->field['name'];
        $displayName = $this->displayName;

        $html = " <td scope='row' width='20%'>$displayName</td>
        <td class='dataField' width='30%'><input type='text' name='$varName' size='12' id='{$varName}' maxlength='10' value=''></td>
        <script> addToValidate('MassUpdate','$varName','decimal',false,'$displayName');</script>";
        return $html;
    }
}



