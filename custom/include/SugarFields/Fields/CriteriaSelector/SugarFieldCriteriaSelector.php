<?php

require_once('include/SugarFields/Fields/Base/SugarFieldBase.php');
require_once('custom/modules/AOW_Conditions/conditionLines.php');

class SugarFieldCriteriaSelector extends SugarFieldBase {
	
	private $focus = null;
	
	// https://www.upcurvecloud.com/blog/sugarcrm/sugarcrm-tutorial-how-to-create-a-re-usable-sugarfield
	
	public function getEditViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex, $searchView = false) {
		$this->setup($parentFieldArray, $vardef, $displayParams, $tabindex);
		
		global $moduleList, $app_list_strings, $mod_strings;
		$all_modules = array();
		
		foreach ($moduleList as $moduleName) {
			$all_modules[$moduleName] = $app_list_strings['moduleList'][$moduleName];
		}
		
		$this->ss->assign('all_modules', $this->getModuleList());
		$mod_strings['LBL_ADD_CONDITION'] = translate('LBL_ADD_CONDITION', 'AOW_Workflow');
		$fieldName = $this->getFieldName();
		$this->ss->assign('aow_conditions', display_custom_condition_lines($this->getFocus(), "", $fieldName, "EditView"));
		$this->ss->assign('flow_module_value', $this->getFlowModule($this->focus->{$fieldName}));
		
		return $this->fetch($this->findTemplate('EditView'));
	}
	
	function getDetailViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex, $searchView = false) {
		global $app_list_strings;
		
		$this->setup($parentFieldArray, $vardef, $displayParams, $tabindex);
		
		$fieldName = $this->getFieldName();
		$moduleId = $this->getFlowModule($this->focus->{$fieldName});
		$this->ss->assign('aow_conditions', display_custom_condition_lines($this->getFocus(), "", $fieldName, "DetailView"));
		$this->ss->assign('flow_module_value', $moduleId);
		$this->ss->assign('flow_module_name', $app_list_strings['moduleList'][$moduleId]);
		
		return $this->fetch($this->findTemplate('DetailView'));
	}
	
	public function getListViewSmarty($parentFieldArray, $vardef, $displayParams, $col) {
        $this->setup($parentFieldArray, $vardef, $displayParams, 1, false);

		$this->ss->assign('flow_module_value', $this->getFlowModule($parentFieldArray['CRITERIA_C']));

        return $this->fetch($this->findTemplate('ListView'));
    }
	
	public function save(&$bean, $params, $field, $properties, $prefix = '')
    {
		$resultArray = array();
		
		foreach ($_POST as $key => $value) {
			if (strpos($key, "aow_conditions_") !== false) {
				$resultArray = array_merge($resultArray, array($key => $value));
			}
		}
		
		$resultArray['flow_module'] = $_POST['flow_module'];
		
		$bean->{$field} = base64_encode(serialize($resultArray));
		
        parent::save($bean, $params, $field, $properties, $prefix);
    }
	
	private function getModuleList() {
		global $current_user, $app_list_strings;
		
		foreach (query_module_access_list($current_user) as $module) {
			if (isset($app_list_strings['moduleList'][$module])) {
				$fullModuleList[$module] = $app_list_strings['moduleList'][$module];
			}
		}
		
		asort($fullModuleList);
		
		return array_merge(array("" => ""), $fullModuleList);
	}
	
	private function getFocus() {
		if (!empty($this->focus))
			return $this->focus;
		
		$recordId = isset($_REQUEST['record']) ? $_REQUEST['record'] : "";
		$this->focus = BeanFactory::getBean($_REQUEST['module'], $recordId);
		
		return $this->focus;
	}
	
	private function getFieldName() {
		return $this->ss->_tpl_vars['vardef']['name'];
	}
	
	private function getFlowModule($savedValues) {
		$post_data = unserialize(base64_decode($savedValues));
		return $post_data['flow_module'];
	}
	
}
?>