{literal}
<style>
	.condition_holder {
		padding-top: 20px;
	}
</style>
{/literal}

<div>
	<label for="flow_module">{{sugar_translate module="Application" label="LBL_DTBC_CRITERIA_SELECTOR_MODULE"}}&nbsp;&nbsp;</label>
	{{html_options id='flow_module' name='flow_module' options=$all_modules selected=$flow_module_value class='dtbc_cs_moduleType'}}
</div>

<div class="condition_holder">
{literal}
	{{$aow_conditions}}
{/literal}
</div>

