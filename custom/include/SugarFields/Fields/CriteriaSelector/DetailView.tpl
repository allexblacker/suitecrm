{literal}
<style>
	.condition_holder {
		padding-top: 20px;
	}
</style>
{/literal}

<div>
	<label for="flow_module">{{sugar_translate module="Application" label="LBL_DTBC_CRITERIA_SELECTOR_MODULE"}}:&nbsp;{{$flow_module_name}}</label>
	<input type="hidden" id="flow_module" value="{{$flow_module_value}}"/>
</div>

<div class="condition_holder">
{literal}
	{{$aow_conditions}}
{/literal}
</div>
