<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');


class SugarWidgetSubPanelTopCreateCustom extends SugarWidgetSubPanelTopButton
{
	function display($defines, $additionalFormFields = NULL, $nonbutton = false)
	{
		global $app_strings;
		global $currentModule;

		$title = $app_strings['LBL_ADD_LINE_ITEM'];
		//$accesskey = $app_strings['LBL_NEW_BUTTON_KEY'];
		$value = $app_strings['LBL_ADD_LINE_ITEM'];
		$this->module = 'dtbc_Line_Item';
		
		$additionalFormFields = array();
		
		$button = $this->_get_form($defines, $additionalFormFields);
		$button .= "<input title='$title' class='button' type='submit' name='{$this->getWidgetId()}' id='{$this->getWidgetId()}' value='$value'/>\n";
		$button .= "</form>";
		return $button;
	}
}
?>