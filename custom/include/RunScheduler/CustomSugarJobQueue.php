<?php

require_once('include/SugarQueue/SugarJobQueue.php');
require_once('custom/include/RunScheduler/CustomSchedulersJob.php');

class CustomSugarJobQueue extends SugarJobQueue {
	public function __construct() {
		parent::__construct();
	}
	
	public function nextJob($clientID) {
		$now = $this->db->now();
		$queued = SchedulersJob::JOB_STATUS_QUEUED;
		$try = $this->jobTries;
		
		while ($try--) {
			$id = $this->db->getOne("
				SELECT
					id
				FROM
					{$this->job_queue_table}
				WHERE
					execute_time <= $now
					AND status = '$queued'
				ORDER BY
					date_entered ASC
			");
			
			if (empty($id)) {
				return null;
			}
			
			$job = new CustomSchedulersJob();
			$job->retrieve($id);
			
			if (empty($job->id)) {
				return null;
			}
			
			$job->status = SchedulersJob::JOB_STATUS_RUNNING;
			$job->client = $clientID;
			$client = $this->db->quote($clientID);
			
			$res = $this->db->query("
				UPDATE
					{$this->job_queue_table}
				SET
					status = '{$job->status}',
					date_modified = $now,
					client = '$client'
				WHERE
					id = '{$job->id}'
					AND status = '$queued'
			");
			
			if($this->db->getAffectedRowCount($res) == 0) {
				continue;
			} else {
				$job->save();
				
				break;
			}
		}
		
		return $job;
	}
}
