<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {config_load} function plugin
 *
 * Type:     function<br>
 * Name:     config_load<br>
 * Purpose:  load config file vars
 * @link http://smarty.php.net/manual/en/language.function.config.load.php {config_load}
 *       (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 * @author messju mohr <messju at lammfellpuschen dot de> (added use of resources)
 * @param array Format:
 * <pre>
 * array('file' => required config file name,
 *       'section' => optional config file section to load
 *       'scope' => local/parent/global
 *       'global' => overrides scope, setting to parent if true)
 * </pre>
 * @param Smarty
 */
function smarty_function_global_search_module_list($params, &$smarty)
{
	global $current_user, $beanList, $objectList;
	include('custom/modules/unified_search_modules_display.php');
	
	$users_modules = $current_user->getPreference('globalSearch', 'search');
	
	// preferences are empty, select all
	if(empty($users_modules)) {
		$users_modules = array();
		foreach ($unified_search_modules_display as $module => $data) {
			if (!empty($data['visible']) ) {
				$users_modules[$module] = $beanList[$module];
			}
		}
		$current_user->setPreference('globalSearch', $users_modules, 0, 'search');
	}	
	
	$users_modules = array_merge(array('All' => 'All'), $users_modules);
	$finalList = array();
	
	foreach ($users_modules as $key => $value) {
		if ($value == 'All') {
			$finalList[$key] = $value;
			continue;
		}
		$temp = new $value();
		$finalList[$key] = translate('LBL_MODULE_NAME', $temp->module_name);
		$temp = null;
	}
	
	$savedModules = $current_user->getPreference('globalSearch_modules', 'search');
	
    $smarty->assign('modules_list', $finalList);
	$smarty->assign('modules_selected', $savedModules);
}

/* vim: set expandtab: */

?>
