<?php

require_once('modules/SecurityGroups/SecurityGroup.php');
require_once("custom/include/dtbc/helpers.php");

class CompensationCase {
	
	public function after_save_group($bean, $event, $arguments) {
		if ($bean->fetched_row['case_record_type_c'] != $bean->case_record_type_c &&
			$bean->case_record_type_c == "compensation") {
				global $sugar_config;
				// If the case type has changed to Compensation, then assign the group
				$groupId = $sugar_config['solaredge']['compensation_group_id'];
				$secGroups = new SecurityGroup();
				$secGroups->addGroupToRecord("Cases", $bean->id, $groupId);
			}
	}
	
	public function after_save($bean, $event, $arguments) {
		if ($bean->case_record_type_c == "compensation") {
			// TODO: assign to Support Planner
			
			// Assign RMA case
			if (!empty($_REQUEST['workingrecord']))
				$this->linkRmaCase($bean->id, $_REQUEST['workingrecord']);
			/*
			// Send email to customer "Compensation Terms Check"
			require_once("custom/include/dtbc/EmailSender.php");
			global $sugar_config;
			$emailSender = new EmailSender();
			$emailAddress = $emailSender->getEmailAddresFromCase($bean);
			$emailId = $sugar_config['solaredge']['compensation_terms_check_email_id'];
			$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $bean->id, $emailId);
			*/
		}
	}
	
	private function linkRmaCase($compensationId, $rmaId) {
		global $db;
		$sql = "INSERT INTO cases_cases_1_c
				(id, date_modified, deleted, cases_cases_1cases_ida, cases_cases_1cases_idb)
				VALUES (
					UUID(),
					NOW(),
					0,
					" . $db->quoted($rmaId) . ",
					" . $db->quoted($compensationId) . "
				)";
		$db->query($sql);
	}

}
