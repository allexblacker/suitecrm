<?php

require_once("custom/include/dtbc/helpers.php");

class EmailDoaTableGenerator {
	
	private $fieldType;
	private $db;
	private $recordId;
	private $year;
	
	public function __construct() {
		global $db;
		$this->db = $db;
		if ($_REQUEST['module'] == 'Cases')
			$this->recordId = !empty($_REQUEST['record']) ? $_REQUEST['record'] : "";
		
		if (empty($this->recordId) && isset($_REQUEST['parent_type']) && $_REQUEST['parent_type'] == 'Cases')
			$this->recordId = !empty($_REQUEST['parent_id']) ? $_REQUEST['parent_id'] : "";
	}
	
	public function run() {
		$retval = "<table style='width:100%; border: 1px solid #000; border-collapse: collapse;'>";
		$retval .= "<tbody>";
		$retval .= $this->getHeader();
		$retval .= $this->getDataLines();
		$retval .= "</tbody>";
		$retval .= "</table>";
		
		return $retval;
	}
	
	private function getHeader() {
		return "
			<tr>
				<td style='border: 1px solid #000'>Description</td>
				<td style='border: 1px solid #000'>Issue</td>
				<td style='border: 1px solid #000'>Date</td>
				<td style='border: 1px solid #000'>Account Name</td>
				<td style='border: 1px solid #000'>Actions</td>
				<td style='border: 1px solid #000'>DOA/RMA?</td>
				<td style='border: 1px solid #000'>CASE #</td>
				<td style='border: 1px solid #000'>Operating period</td>
				<td style='border: 1px solid #000'>Serial #</td>
				<td style='border: 1px solid #000'>Part Number</td>
				<td style='border: 1px solid #000'>Tech</td>
			<tr>
		";
	}
	
	private function getDataLines() {
		$sql = "SELECT sn1_serial_number.fault_category,
					sn1_serial_number.fault_sub_category,
					sn1_serial_number.fault_description,
					sn1_serial_number.name AS serial_name,
					sn1_serial_number.product_new,
					cases.date_entered,
					cases.case_number,
					cases_cstm.operating_period_c,
					cases_cstm.doa_rma_c,
					accounts.name AS account_name,
					users.first_name,
					users.last_name,
					users.title
				FROM cases
				LEFT JOIN cases_cstm ON cases_cstm.id_c = cases.id
				LEFT JOIN cases_sn1_serial_number_1_c ON cases_sn1_serial_number_1cases_ida = cases.id AND cases_sn1_serial_number_1_c.deleted = 0
				LEFT JOIN sn1_serial_number ON cases_sn1_serial_number_1sn1_serial_number_idb = sn1_serial_number.id AND sn1_serial_number.deleted = 0
				LEFT JOIN accounts ON accounts.id = cases.account_id AND accounts.deleted = 0
				LEFT JOIN users ON users.id = cases.assigned_user_id
				WHERE cases.id = " . $this->db->quoted($this->recordId);
						
		$res = $this->db->query($sql);
		
		$retval = "";
		while ($row = $this->db->fetchByAssoc($res)) {
			$retval .= "<tr>";
			$retval .= "<td style='border: 1px solid #000'></td>";
			$retval .= "<td style='border: 1px solid #000'>Fault category: " . $row['fault_category'] . "<br/>
						Fault sub category: " . $row['fault_sub_category'] . "<br/>
						Fault description: " . $row['fault_description'] . "
						</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['date_entered'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['account_name'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'></td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['doa_rma_c'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['case_number'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['operating_period_c'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['serial_name'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $row['product_new'] . "</td>";
			$retval .= "<td style='border: 1px solid #000'>" . $this->getUserName($row['title'], $row['first_name'], $row['last_name']) . "</td>";
			$retval .= "</tr>";
		}
		
		return $retval;
	}
	
	private function getUserName($title, $first, $last) {
		$retval = "";
		
		if (!empty($title))
			$retval .= $title . " ";
		
		if (!empty($first))
			$retval .= $first . " ";
		
		if (!empty($last))
			$retval .= $last;
		
		return trim($retval);
	}
	
}

function dtbc_email_doa_table() {
	$obj = new EmailDoaTableGenerator();
	return $obj->run();
}
