<?php

if(!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class ProcessRecordsInsideSalesListView{

    private function isValidRequest(){

        return isset($_REQUEST['action']) && $_REQUEST['action'] = 'DetailView';
    }

    public function process_record($bean, $events, $arguments){

        if($this->isValidRequest())
            $bean->notes = html_entity_decode($bean->notes, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
    }
}