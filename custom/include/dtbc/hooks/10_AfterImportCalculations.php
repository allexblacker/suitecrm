<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class CustomHooks {
	public function before_save($bean, $event, $arguments) {
		if (!$this->isImportAction())
			return;
			
		$this->setRelatedAccount($bean->accounts_monitoring_id, $bean->id);
	}
	
	private function setRelatedAccount($monitoringId, $allianceId) {
		$accountId = $this->getRelatedAccount($monitoringId);
		
		if ($accountId == '' || $this->isRelationAdded($accountId, $allianceId))
			return;
		
		global $db;
		$sql = "INSERT INTO accounts_al1_alliance_transaction_1_c 
				(id,
				date_modified,
				deleted,
				accounts_al1_alliance_transaction_1accounts_ida,
				accounts_al1_alliance_transaction_1al1_alliance_transaction_idb) 
				VALUES 
				(UUID(),
				NOW(),
				0,
				" . $db->quoted($accountId) . ",
				" . $db->quoted($allianceId) . ")";
		$db->query($sql);
	}
	
	private function isRelationAdded($accountId, $allianceId) {
		global $db;
		$sql = "SELECT id 
				FROM accounts_al1_alliance_transaction_1_c 
				WHERE accounts_al1_alliance_transaction_1accounts_ida = " . $db->quoted($accountId) . " AND 
				accounts_al1_alliance_transaction_1al1_alliance_transaction_idb = " . $db->quoted($allianceId) . " AND
				deleted = 0";
		$res = $db->fetchOne($sql);
		return $res != null && !empty($res) && is_array($res) && isset($res['id']) && strlen($res['id']) > 0;
	}
	
	private function getRelatedAccount($monitoringId) {
		global $db;
		$sql = "SELECT id_c 
				FROM accounts_cstm 
				WHERE account_monitoring_id_c = " . $db->quoted($monitoringId);
		$res = $db->fetchOne($sql);
		
		if ($res != null && !empty($res) && is_array($res) && isset($res['id_c']) && strlen($res['id_c']) > 0)
			return $res['id_c'];
		
		return '';
	}
	
	private function isImportAction() {
		return $this->isEquals('module', 'Import') && $this->isEquals('import_module', 'AL1_Alliance_Transaction') && $this->isEquals('action', 'Step4');
	}
	
	private function isEquals($fieldName, $value) {
		return isset($_REQUEST[$fieldName]) && $_REQUEST[$fieldName] == $value;
	}
}