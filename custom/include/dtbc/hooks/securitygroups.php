<?php

require_once("custom/include/dtbc/helpers.php");

class GlobalProcessRecordHooks {
	
	public function displaySecurityGroupOnListView(&$bean, $event, $arguments) {
		if (property_exists($bean, "contact_created_by_id") && !empty($bean->contact_created_by_id) && empty($bean->contact_created_by_name)) {
			$cBean = BeanFactory::getBean("Contacts", $bean->contact_created_by_id);
			$bean->contact_created_by_name = $cBean->name;
		}
		
		if (property_exists($bean, "listview_security_group")) {
			$moduleName = ($_REQUEST['action'] == 'UnifiedSearch' || $_REQUEST['action'] == 'DetailView') ? $bean->module_dir : $_REQUEST['module'];
			$bean->listview_security_group = $this->getSecurityGroupName($bean->id, $moduleName);
		}
		
		if (property_exists($bean, "listview_envelope")) {
			$bean->listview_envelope = html_entity_decode($bean->change_indicator_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		}
		
	}
		
	private function getSecurityGroupName($recordId, $moduleName) {
		global $db;
		
		$sql = "SELECT securitygroups.name AS name
				FROM securitygroups_records 
				JOIN securitygroups ON securitygroups_records.securitygroup_id = securitygroups.id AND securitygroups.deleted = 0
				WHERE module = " . $db->quoted($moduleName) . " AND
				record_id = " . $db->quoted($recordId) . " AND
				securitygroups_records.deleted = 0";
		
		$secGroupArray = array();
		$result = $db->query($sql);
		while($res = $db->fetchByAssoc($result)){
			array_push($secGroupArray, $res['name']);
		}
		if (!empty($secGroupArray) && is_array($secGroupArray) && count($secGroupArray) > 0)
			return implode(';',$secGroupArray);
		
		return "";
	}
	
}
