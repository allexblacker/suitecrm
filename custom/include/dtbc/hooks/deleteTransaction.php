<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class TransactionRelationShips {
	public function before_relationship_delete_from_transact($bean, $event, $arguments) {
		$accountId = $this->getAccountId($bean->id);
		$year = $this->getYear($bean->id);
		$trs = $this->getNumberOfTransactions($accountId, $year);
		
		if (intval($trs) - 1 <= 0)
			$this->removeYearAlliance($accountId, $year);
	}
	
	private function removeYearAlliance($accountId, $year) {
		global $db;
		
		$sql = "SELECT dtbc_year_alliance_points_cstm.id_c AS pointId, accounts_dtbc_year_alliance_points_1_c.id AS yearId
				FROM dtbc_year_alliance_points_cstm
				JOIN accounts_dtbc_year_alliance_points_1_c ON accounts_dabb8_points_idb = dtbc_year_alliance_points_cstm.id_c AND accounts_dtbc_year_alliance_points_1_c.deleted = 0
				WHERE year_c = " . $db->quoted($year);
				
		$res = $db->fetchOne($sql);

		if ($res == null || empty($res)) 
			return;

		$sql = "UPDATE dtbc_year_alliance_points
				SET deleted = 1
				WHERE id = " . $db->quoted($res['pointId']);

		$db->query($sql);
				
		$sql = "UPDATE accounts_dtbc_year_alliance_points_1_c
				SET deleted = 1
				WHERE id = " . $db->quoted($res['yearId']);

		$db->query($sql);
	}
	
	private function getNumberOfTransactions($accountId, $year) {
		global $db;
		
		$sql = "SELECT count(al1_alliance_transaction.id) AS NumOfTrns
				FROM al1_alliance_transaction
				JOIN accounts_al1_alliance_transaction_1_c ON al1_alliance_transaction.id = accounts_al1_alliance_transaction_1al1_alliance_transaction_idb AND accounts_al1_alliance_transaction_1_c.deleted = 0
				WHERE YEAR(date1) = " . $db->quoted($year) . " AND
				al1_alliance_transaction.deleted = 0 AND
				accounts_al1_alliance_transaction_1accounts_ida = " . $db->quoted($accountId);
				
		$res = $db->fetchOne($sql);
		
		if ($res != null && !empty($res) && isset($res['NumOfTrns']))
			return $res['NumOfTrns'];
		
		return '';
	}
	
	private function getYear($transactionId) {
		global $db;
		
		$sql = "SELECT YEAR(date1) AS date1
				FROM al1_alliance_transaction
				WHERE id = " . $db->quoted($transactionId);
				
		$res = $db->fetchOne($sql);
		
		if ($res != null && !empty($res) && isset($res['date1']))
			return $res['date1'];
		
		return '';
	}
	
	private function getAccountId($transactionId) {
		global $db;
		
		$sql = "SELECT accounts_al1_alliance_transaction_1accounts_ida
				FROM accounts_al1_alliance_transaction_1_c
				WHERE accounts_al1_alliance_transaction_1al1_alliance_transaction_idb = " . $db->quoted($transactionId) . " AND
				accounts_al1_alliance_transaction_1_c.deleted = 0";
				
		$res = $db->fetchOne($sql);
		
		if ($res != null && !empty($res) && isset($res['accounts_al1_alliance_transaction_1accounts_ida']))
			return $res['accounts_al1_alliance_transaction_1accounts_ida'];
		
		return '';
	}
}