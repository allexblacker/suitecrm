<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("modules/ACLRoles/ACLRole.php");
require_once("modules/SecurityGroups/SecurityGroup.php");

class AfterUiHooks {
	public function removeButtonsFromBulkMenu($event, $arguments) {
		if (isset($GLOBALS['current_user']) && 
			isset($_REQUEST['module']) && 
			isset($_REQUEST['action']) && 
			$_REQUEST['action'] == 'index') {
			$this->checkButtons($GLOBALS['current_user']->id, $_REQUEST['module'], 'delete');
		}
	}
	
	private function checkButtons($userId, $module, $action) {
		if ($this->hasAccessToButton($userId, $module, $action))
			return;

		// Remove the button
		echo "
			<script type='text/javascript'>
				$(document).ready(function() {
					$('.subnav #delete_listview_top').remove();
				});
			</script>
		";
	}
	
	private function hasAccessToButton($userId, $module, $action) {
		global $db;
		
		$sql = "SELECT access_override 
				FROM acl_roles_users 
				JOIN acl_roles_actions ON acl_roles_actions.role_id = acl_roles_users.role_id
				JOIN acl_actions ON acl_actions.id = acl_roles_actions.action_id
				WHERE user_id = " . $db->quoted($userId) . " AND
				category = " . $db->quoted($module) . " AND
				acl_actions.name = " . $db->quoted($action);
				
		$res = $db->fetchOne($sql);
		
		$retval = true;
		
		if ($res != null && is_array($res) && count($res) > 0) {
			$retval = $res['access_override'] > 75 || strlen($res['access_override']) == 0;
		}
		
		return $retval;
	}
}