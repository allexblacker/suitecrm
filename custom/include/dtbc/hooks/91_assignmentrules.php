<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/modules/AOW_Conditions/conditionLines.php");

class AssignmentRules {

    private $workflowBean;

    public function run(&$bean, $event, $arguments) {
		$module = $bean->module_dir;
		$record = $bean->id;
		
		$rules = $this->getAssignmentRules($module);
		
		if (empty($rules))
			return;

		$this->workflowBean = new AOW_WorkFlow();
		
		foreach ($rules as $ruleNumber => $rule) {
			$criteria = dtbcGetConditionSettings($rule['criteria_c'], $record);
			$criteriaValues = $this->getCriteriaValues($bean, $criteria['values']);
            //DtbcHelpers::logger($criteriaValues);
		}

		// TODO: implementing the filter logic
    }
	
	private function getCriteriaValues($bean, $criteriaObjList) {
	    global $app_list_strings, $timedate, $db;

	    $retval = array();

		foreach ($criteriaObjList as $condition) {
            $path = unserialize(base64_decode($condition->module_path));

            $condition_bean = $bean;

            if(isset($path[0]) && $path[0] != $bean->module_dir){
                $query_array = $this->workflowBean->build_query_where($condition, $condition_bean, array());

                $sql = "SELECT " . $bean->table_name . ".id as result, " . $query_array['select'][0] . "
                        FROM " . $bean->table_name . " 
                         " . $query_array['join'][$path[0]] . " 
                        WHERE " . $query_array['where'][0];

                $dbRes = $db->fetchOne($sql);

                if (!empty($dbRes) && is_array($dbRes) && isset($dbRes['result'])) {
                    if (empty($dbRes['result']) || strlen($dbRes['result']) == '0')
                        $retval[] = array('order' => $condition->condition_order, 'value' => 'false');
                    else
                        $retval[] = array('order' => $condition->condition_order, 'value' => 'true');
                } else {
                    $retval[] = array('order' => $condition->condition_order, 'value' => 'false');
                }


                continue;
            }

            $field = $condition->field;
            $value = $condition->value;

            $dateFields = array('date','datetime', 'datetimecombo');
            if(isset($app_list_strings['aow_sql_operator_list'][$condition->operator])){

                $data = $condition_bean->field_defs[$field];

                if($data['type'] == 'relate' && isset($data['id_name'])) {
                    $field = $data['id_name'];
                    $condition->field = $data['id_name'];
                }
                $field = $condition_bean->$field;

                if(in_array($data['type'],$dateFields)) {
                    $field = strtotime($field);
                }

                switch($condition->value_type) {
                    case 'Field':
                        $data = $condition_bean->field_defs[$value];

                        if($data['type'] == 'relate' && isset($data['id_name'])) {
                            $value = $data['id_name'];
                        }
                        $value = $condition_bean->$value;

                        if(in_array($data['type'],$dateFields)) {
                            $value = strtotime($value);
                        }

                        break;

                    case 'Any_Change':
                        $value = $condition_bean->fetched_row[$condition->field];
                        if(in_array($data['type'],$dateFields)) {
                            $value = strtotime($value);
                        }
                        switch($condition->operator) {
                            case 'Not_Equal_To';
                                $condition->operator = 'Equal_To';
                                break;
                            case 'Equal_To';
                            default:
                                $condition->operator = 'Not_Equal_To';
                                break;
                        }
                        break;

                    case 'Date':
                        $params =  unserialize(base64_decode($value));
                        $dateType = 'datetime';
                        if($params[0] == 'now'){
                            $value = date('Y-m-d H:i:s');
                        } else if($params[0] == 'today'){
                            $dateType = 'date';
                            $value = date('Y-m-d');
                            $field = strtotime(date('Y-m-d', $field));
                        } else {
                            $fieldName = $params[0];
                            $value = $condition_bean->$fieldName;
                        }

                        if($params[1] != 'now'){
                            switch($params[3]) {
                                case 'business_hours';
                                    if(file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php')){
                                        require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

                                        $businessHours = new AOBH_BusinessHours();

                                        $amount = $params[2];
                                        if($params[1] != "plus"){
                                            $amount = 0-$amount;
                                        }

                                        $value = $businessHours->addBusinessHours($amount, $timedate->fromDb($value));
                                        $value = strtotime($timedate->asDbType( $value, $dateType ));
                                        break;
                                    }
                                    //No business hours module found - fall through.
                                    $params[3] = 'hours';
                                default:
                                    $value = strtotime($value.' '.$app_list_strings['aow_date_operator'][$params[1]]." $params[2] ".$params[3]);
                                    if($dateType == 'date') $value = strtotime(date('Y-m-d', $value));
                                    break;
                            }
                        } else {
                            $value = strtotime($value);
                        }
                        break;

                    case 'Multi':

                        $value = unencodeMultienum($value);
                        if($data['type'] == 'multienum') $field = unencodeMultienum($field);
                        switch($condition->operator) {
                            case 'Not_Equal_To';
                                $condition->operator = 'Not_One_of';
                                break;
                            case 'Equal_To';
                            default:
                                $condition->operator = 'One_of';
                                break;
                        }
                        break;
                    case 'SecurityGroup':
                        if(file_exists('modules/SecurityGroups/SecurityGroup.php')){
                            $sg_module = $condition_bean->module_dir;
                            if(isset($data['module']) && $data['module'] != ''){
                                $sg_module = $data['module'];
                            }
                            $value = $this->workflowBean->check_in_group($field, $sg_module, $value);
                            $field = true;
                        break;
                        }
                    case 'Value':
                    default:
                        if(in_array($data['type'],$dateFields) && trim($value) != '') {
                            $value = strtotime($value);
                        }
                        break;
                }

                if(!($this->workflowBean->compare_condition($field, $value, $condition->operator))){
                    $retval[] = array('order' => $condition->condition_order, 'value' => 'false');
                } else {
                    $retval[] = array('order' => $condition->condition_order, 'value' => 'true');
                }

            }
        }

        return $retval;
	}

	private function getAssignmentRules($module) {
		global $db;
		
		$retval = array();
		
		$sql = "SELECT *
				FROM dtbc_assignment_rules_cstm
				WHERE module_code_c = " . $db->quoted($module) . " 
				ORDER BY rule_order_c";
				
		$res = $db->query($sql);
		
		while ($row = $db->fetchByAssoc($res)) {
			$retval[] = $row;
		}
		
		return $retval;
	}
}