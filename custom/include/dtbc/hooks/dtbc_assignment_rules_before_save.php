<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class DtbcAssignmentRulesHooks {
	public function before_save_autofill(&$bean, $event, $arguments) {
		global $app_list_strings;
		$bean->module_code_c = $this->getModuleName($bean->criteria_c);
		$bean->module_c = $app_list_strings['moduleList'][$bean->module_code_c];
		$bean->name = $bean->module_c . " Rule #" . $bean->rule_order_c;
	}
	
	private function getModuleName($criteria) {
		$post_data = unserialize(base64_decode($criteria));
		return $post_data['flow_module'];
	}
}