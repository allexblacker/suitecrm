<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class LineItemCalculations {
	public function before_save($bean, $event, $arguments) {
		global $app_list_strings;
		
		$bean->unit_price_after_discount_c = $bean->list_price_c * ((100 - $bean->discount_c) / 100);
		$bean->total_price_c = $bean->unit_price_after_discount_c * $bean->quantity_c;

		$pbp = BeanFactory::getBean("dtbc_Pricebook_Product", $bean->dtbc_pricebook_product_id_c);
		if (empty($pbp))
			return;
		
		$aosp = BeanFactory::getBean("AOS_Products", $pbp->aos_products_dtbc_pricebook_product_1aos_products_ida);
		if (empty($aosp))
			return;
		
		if (empty($bean->total_kw_c) || $bean->quantity_c != $bean->fetched_row['quantity_c'])
			$bean->total_kw_c = $aosp->kw_c * $bean->quantity_c;
		$bean->kwforop_c = $bean->total_kw_c;
		$bean->product_family_from_product_c = $app_list_strings['family_list'][$aosp->family_c];
	}
}