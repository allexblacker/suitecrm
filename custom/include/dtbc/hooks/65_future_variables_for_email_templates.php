<?php

class FutureVariables
{
	public function after_save($bean, $event, $arguments){
		
		//case_number was not shown in email templates, it is an autoincrement variable
		if(!empty($bean->case_number))
			return;
		
		global $db;
		$sql = 'SELECT case_number FROM cases WHERE id = ' . $db->quoted($bean->id);
		$queryResult = $db->query($sql);
		$result = $db->fetchByAssoc($queryResult);
		$bean->case_number = $result['case_number'];
	}
}