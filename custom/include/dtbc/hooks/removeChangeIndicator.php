<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
require_once("custom/include/dtbc/classes/FieldUpdater.php");

class RemoveChangeIndicator {
	
	function after_save(&$bean, $event, $arguments){
		if ($bean->type != "out" || $bean->status != "out" || $bean->parent_type != "Cases" || empty($bean->parent_id))
			return;
		
		$caseBean = BeanFactory::getBean('Cases', $bean->parent_id);
		$caseBean->allow_override_c = '';
		$caseBean->save();
	}
}