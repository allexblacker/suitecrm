<?php

if(!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

class AllianceLogicHooks{

    public function setAllianceTransaction($bean, $events, $arguments)
    {

        if (!$this->isValidBeanFroAlliance($bean))
            return;

        global $db;

        $id = $bean->accounts_al1_alliance_transaction_1accounts_ida;
        if($_REQUEST['return_module'] == 'Accounts')
            $id = $_REQUEST['return_id'];

        $result = $db->fetchByAssoc($db->query(DtbcHelpers::getAllianceTransactionSql($id)));

        if ($result['max_date'] == $bean->date1)
            $this->updateFieldValue($db, 'last_alliance_transaction__c', $bean->date1, $id);

        if ($result['min_date'] == $bean->date1)
            $this->updateFieldValue($db, 'loyalty_plan_start_date__c', $bean->date1, $id);
    }

    private function updateFieldValue(&$db, $fieldName, $date, $id){

        $sql = "UPDATE accounts
                SET accounts.$fieldName = " . $db->quoted($date) . "
                WHERE accounts.id = " . $db->quoted($id);
        $db->query($sql);
    }

    private function isValidBeanFroAlliance(&$bean){

        return !empty($bean) && $bean->fetched_row['date1'] != $bean->date1 && !empty($bean->accounts_al1_alliance_transaction_1accounts_ida);
    }

}