<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
require_once("custom/include/dtbc/classes/FieldUpdater.php");

class CasesFieldUpdater{
	
	function after_save($bean, $event, $arguments){
		if (empty($bean->acase_id_c))
			return;
		
		$caseBean = BeanFactory::getBean('Cases', $bean->acase_id_c);
		if ($caseBean === false)
			return;
		$fieldUpdater = new FieldUpdater();
		$caseBean->countif_portia_rs485_c = $fieldUpdater->getCountIfPortia($caseBean->id);
		$caseBean->rma_lines_in_case_c = $fieldUpdater->getRMALinesInCase($caseBean->id);
		$caseBean->save();
	}
}