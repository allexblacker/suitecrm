<?php

class SystemWorkflows {
	
	public function before_save(&$bean, $event, $arguments) {
		if (!$this->isSystemWf($bean->name))
			return;
		
		global $mod_strings;
		
		$bean->description = $mod_strings['LBL_SYS_WF_DESCRIPTION'];
		$bean->multiple_runs = 1;
		$bean->status = "Inactive";
	}
	
	private function isSystemWf($beanName) {
		return strpos(str_replace(' ', '', strtolower($beanName)), "[systemworkflow]") !== false;
	}
}
