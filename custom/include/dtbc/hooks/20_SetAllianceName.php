<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class CustomNameHook {
	private $prefix = "AT - ";
	
	public function before_save(&$bean, $event, $arguments) {
		if ($this->isImportAction() || ($bean->name != null && strlen($bean->name) > 0))
			return;
			
		$bean->name = $this->prefix . DtbcHelpers::getNextNumberForAllianceName($this->prefix);
	}
	
	private function isImportAction() {
		return $this->isEquals('module', 'Import') && $this->isEquals('import_module', 'AL1_Alliance_Transaction') && $this->isEquals('action', 'Step4');
	}
	
	private function isEquals($fieldName, $value) {
		return isset($_REQUEST[$fieldName]) && $_REQUEST[$fieldName] == $value;
	}
}