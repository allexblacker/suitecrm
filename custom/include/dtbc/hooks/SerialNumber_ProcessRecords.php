<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/rma_alert_c.php");

class ProcessRecords {
	public function process_record_rma_alert($bean, $event, $arguments) {
		$bean->rma_alert_c = dtbc_display_rma_alert_image($bean);
	}
	
	public function process_record_check_rma($bean, $event, $arguments) {
		$bean->subpanel_check_rma_c = '<input type="checkbox" id="checkrma_' . $bean->id . '" name="checkrma[]" value="' . $bean->id . '">';
	}
}