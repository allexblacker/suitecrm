<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class RemoveLineBreaks {
	private function replaceInDescription($find, &$bean) {
		$bean->description = str_replace($find, '', $bean->description);
	}
	
	public function before_save(&$bean, $event, $argument) {
		$bean->description = DtbcHelpers::trimString($bean->description);
		$this->replaceInDescription("\xc2\xa0", $bean);
		
		$this->replaceInDescription("<p></p>", $bean);
		$this->replaceInDescription("<p>&nbsp;</p>", $bean);
		$this->replaceInDescription("<p class=\"MsoNormal\">&nbsp;</p>", $bean);
		
		$this->replaceInDescription("&lt;p&gt;&lt;/p&gt;", $bean);
		$this->replaceInDescription("&lt;p&gt;&nbsp;&lt;/p&gt;", $bean);
		$this->replaceInDescription("&lt;p class=&quot;MsoNormal&quot;&gt;&nbsp;&lt;/p&gt;", $bean);
	}
}