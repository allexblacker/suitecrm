<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

class CaseUpdatesThreadUpdater {
	
	private function replaceInDescription($find, &$bean){
		
		$bean->description = str_replace($find, '', $bean->description);
	}
	
	public function before_save($bean, $event, $argument) {
		$bean->description = DtbcHelpers::trimString($bean->description);
		
		$this->replaceInDescription("\xc2\xa0", $bean);
		$this->replaceInDescription("<p></p>", $bean);
	}
	
	public function after_save($bean, $event, $argument){
		if(!isset($bean->id) || empty($bean) || empty($bean->case_id))
			return;
		
		global $db;
		$caseBean = BeanFactory::getBean('Cases', $bean->case_id);
		
		if ($bean->internal == 1)
		{
			$img = $this->getImageTag('Blue_envelope.png');
			$caseBean->internal_comment_c = $img;
			$caseBean->new_internal_comment_c = 1;
		}
		else
		{
			if($bean->assigned_user_id != null || !empty($bean->assigned_user_id))
				return;

			// Change Indicator
			$img = $this->getImageTag('New_red_envelope.png');
			$sql = "UPDATE cases_cstm
				SET change_indicator_c = \"$img\"
				WHERE id_c = " . $db->quoted($bean->case_id);
			$db->query($sql);

			$sql = "UPDATE cases
				SET alert_override_c = 'red'
				WHERE id = " . $db->quoted($bean->case_id);
			$db->query($sql);

			// Set to In process if the customer answers
			$cBean = BeanFactory::getBean("Cases", $bean->case_id);
			if ($cBean->case_status_c == '3') 
				$this->updateCaseStatus(2, $bean->case_id);
			
			// Set to Reopen if the customer answers to a closed case
			if ((!isset($_REQUEST['emailToCaseNew']) || (isset($_REQUEST['emailToCaseNew']) && $_REQUEST['emailToCaseNew'] != '1')) && $cBean->case_status_c == '7') {
				$this->updateCaseStatus(8, $bean->case_id);
			}				
		}
	}
	
	private function updateCaseStatus($newStatus, $caseId) {
		global $db;
		$sql = "UPDATE cases
			SET case_status_c = " . $db->quoted($newStatus) . "
			WHERE id = " . $db->quoted($caseId);
		$db->query($sql);
	}
	
	private function getImageTag($imgName){
		return "<img src='custom/include/dtbc/images/$imgName' height='15' width='25' />";
	}
}