<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class groupnotifier_class {

	function groupnotifier_function(&$bean, $event, $arguments) {
		
		if ($bean->object_name == "SchedulersJob" || $bean->object_name == "AOD_IndexEvent")
			return;
		
		require_once("custom/include/dtbc/EmailSender.php");
		require_once('modules/SecurityGroups/SecurityGroup.php');
		global $db;
		
		$isUpdate = false;
		
		if (!empty($bean->record_id) && !empty($bean->record_module)) {
			// Update
			$recordId = $bean->record_id;
			$recordModule = $bean->record_module;
			$isUpdate = true;
		}
		
		if (!empty($bean->related_id) && !empty($bean->related_module)) {
			// Create
			$recordId = $bean->related_id;
			$recordModule = $bean->related_module;
			// After save hook fires earlier than group sets
			SecurityGroup::inherit($bean, $isUpdate);
		}
		
		if (!empty($recordId) && !empty($recordModule) && $this->isEmailSendingMandatory($recordModule, $recordId)) {
			/*
			$sql = "SELECT email_addresses.email_address FROM securitygroups_users
					JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = securitygroups_users.user_id AND email_addr_bean_rel.deleted = 0
					JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id AND email_addresses.deleted = 0
					JOIN securitygroups_records ON securitygroups_records.securitygroup_id = securitygroups_users.securitygroup_id AND securitygroups_records.deleted = 0
					WHERE securitygroups_records.module = " . $db->quoted($recordModule) . " AND
					securitygroups_records.record_id = " . $db->quoted($recordId) . " AND
					securitygroups_users.deleted = 0 AND
					email_addr_bean_rel.bean_module = 'Users'";
			
			$res = $db->query($sql);

			$emailSender = new EmailSender();
			
			while ($row = $db->fetchByAssoc($res)) {
				if (is_array($_REQUEST['dtbc_addresses'][$recordId]) && !in_array($row['email_address'], $_REQUEST['dtbc_addresses'][$recordId])) {
					$emailSender->sendNotificationOnNewRecordInGroup($row['email_address'], $recordModule, $recordId, $this->getEmailInfo($recordModule, $recordId));
				}
			}
			*/
		}
	}
	
	private function isEmailSendingMandatory($recordModule, $recordId) {
		$bean = BeanFactory::getBean($recordModule, $recordId);		
		return ($bean->assigned_user_id != $bean->fetched_row['assigned_user_id'] ||
				$bean->created_by != $bean->fetched_row['created_by']);
	}
	
	private function getEmailInfo($moduleName, $beanId) {
		global $sugar_config;
		return $sugar_config['site_url'] . "/index.php?module=" . $moduleName . "&action=DetailView&record=" . $beanId;
	}

}