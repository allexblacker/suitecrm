<?php

require_once("custom/include/dtbc/helpers.php");

class calculatepoints_class {
	
	public function calculatepoints_function(&$bean, $event, $arguments) {
		if ($this->isValidModuleRequest($bean)) {
						
			switch($bean->type){
				
				case "Site":
					$this->setPoints($bean->id, $bean->kw, false);
				break;
				case "Fault_Site":
					$this->setPoints($bean->id, $bean->kw, true);
				break;
				case "Gift":
					$this->setPoints($bean->id, $bean->kw, true);
				break;
			}
		}
	}
	
	private function isValidModuleRequest(&$bean) {
		return (!empty($_REQUEST['module']) && isset($_REQUEST['module']) && ($_REQUEST['module'] == "AL1_Alliance_Transaction" || $_REQUEST['module'] == "Import")) && (!empty($bean->kw) && empty($bean->points));
	}
	
	private function setPoints($id, $kw, $negative){
		global $db;
		$kwFactor = DtbcHelpers::getKwFactor($id);

		if($negative)
			$calcPoints = $kw * (-$kwFactor);
		else
			$calcPoints = $kw * $kwFactor;
		
		$id = $db->quoted($id);
		$sql = "UPDATE al1_alliance_transaction 
					SET points = $calcPoints 
					WHERE id = $id";
					
		$db->query($sql);
	}
	
}
