<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class FieldEngineerEmailUpdater {
	
	public function before_save(&$bean, $event, $arguments){
		if($bean->fetched_row['field_engineer_name_c'] != field_engineer_name_c){
			global $app_list_strings;
			$engineerName = $app_list_strings['field_engineer_name_list'][$bean->field_engineer_name_c];
			$email = "";
			
			switch($engineerName){
				case "Idan Maktubi":
					$email = "Idan.maktubi@solaredge.com";
				break;
				case "Jacques Gamrasni":
					$email = "Jacques.Gamrasni@solaredge.com";
				break;
				case "Ronald Riahi":
					$email = "Ronald.Riahi@solaredge.com";
				break;
				case "Giambattista  Zorzan":
					$email = "giambattista.z.cr@solaredge.com";
				break;
				case "Tokuo Ishizuki":
					$email = "Tokuo.ishizuki@solaredge.com";
				break;
				case "Nadav Ratzkowski":
					$email = "Nadav.Ratzkowski.CR@solaredge.com";
				break;
				case "Yael Support":
					$email = "yael.weiss@solaredge.com";
				break;
			}

			$bean->field_engineer_email_c = $email;
		}
	}
}