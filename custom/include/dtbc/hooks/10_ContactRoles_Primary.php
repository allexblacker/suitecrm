<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class HandlingPrimaryCheckbox {
	public function before_save($bean, $event, $arguments) {
		global $db;
		
		if ($bean->is_primary_c != 1)
			return;
		
		if (empty($bean->opportunities_dtbc_contact_roles_1opportunities_ida))
			return;
		
		$sql = "UPDATE dtbc_contact_roles_cstm SET is_primary_c = 0 WHERE id_c IN (SELECT opportunities_dtbc_contact_roles_1dtbc_contact_roles_idb
				FROM opportunities_dtbc_contact_roles_1_c
				WHERE opportunities_dtbc_contact_roles_1_c.deleted = 0 AND
				opportunities_dtbc_contact_roles_1opportunities_ida = " . $db->quoted($bean->opportunities_dtbc_contact_roles_1opportunities_ida) . ")";
		
		$db->query($sql);
	}
}