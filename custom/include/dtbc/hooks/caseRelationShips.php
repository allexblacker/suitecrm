<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/classes/FieldUpdater.php");

class HandleRelationShips {
	public function after_relationship_add($bean, $event, $arguments) {
		if ($arguments['related_module'] != "SN1_Serial_Number" || $arguments['relationship'] != "cases_sn1_serial_number_1")
			return;
		
		// Add relation to the serial number
		$arguments['related_bean']->acase_id_c = $arguments['id'];
		$arguments['related_bean']->save();
		
		$this->updateCaseFields($bean);
	}
	
	public function after_relationship_delete($bean, $event, $arguments) {
		if ($arguments['related_module'] != "SN1_Serial_Number" || $arguments['relationship'] != "cases_sn1_serial_number_1")
			return;
		
		// Remove relation to the serial number
		$arguments['related_bean']->acase_id_c = "";
		$arguments['related_bean']->save();
		
		$this->updateCaseFields($bean);
	}
	
	public function before_relationship_delete($bean, $event, $arguments) {
        if ($arguments['related_module'] != "AOP_Case_Updates" || $arguments['relationship'] != "cases_aop_case_updates")
            return;

        $arguments['related_bean']->deleted = 1;
        $arguments['related_bean']->save();
    }
	
	private function updateCaseFields($bean){
		$fieldUpdater = new FieldUpdater();
		$bean->countif_portia_rs485_c = $fieldUpdater->getCountIfPortia($bean->id);
		$bean->rma_lines_in_case_c = $fieldUpdater->getRMALinesInCase($bean->id);
		$bean->save();
	}
}