<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class FillWfFieldUpdatesModule {
	private $wfModule;
	private $wfBean;
	private $db;
	
	public function after_save(&$bean, $event, $arguments) {
		if (!$this->isRunnable())
			return;
		
		$this->initClass();
		
		// Need to remove everything and readding, because we can't figure out the deleted field and relationship removes
		$this->deleteRecord($bean->id);
		
		$fieldValues = $this->getMappedFields($bean);
		
		foreach ($fieldValues as $mappedDbValues)
			$this->createNewRecords($mappedDbValues);
	}
	
	private function createNewRecords($fieldValues) {
		// Prepare main table
		$newId = create_guid();
		$sql = "INSERT INTO dtbc_workflow_field_updates
				(id,
				name,
				date_entered,
				date_modified,
				modified_user_id,
				created_by,
				description,
				deleted,
				assigned_user_id)
				VALUES
				(" . $this->db->quoted($newId) . ",
				" . $this->db->quoted($this->getActionName($fieldValues['aow_actions_id_c'])) . ",
				UTC_TIMESTAMP,
				UTC_TIMESTAMP,
				1,
				1,
				'',
				0,
				1)";
				
		$this->db->query($sql);
		
		// Prepare cstm table insert
		$fields = "";
		$values = "";
		foreach ($fieldValues as $fieldName => $fieldValue) {
			if (strlen($fields) > 0) {
				$fields .= ", ";
				$values .= ", ";
			}

			$fields .= $fieldName;
			$values .= $this->db->quoted($fieldValue);
		}
		
		// Add guid
		if (strlen($fields) > 0) {
			$fields .= ", ";
			$values .= ", ";
		}
		$fields .= "id_c";
		$values .= $this->db->quoted($newId);
		
		$sql = "INSERT INTO dtbc_workflow_field_updates_cstm (" . $fields . ") VALUES (" . $values . ")";
		
		$this->db->query($sql);
	}
	
	private function getActionName($actionId) {
		foreach ($_REQUEST['aow_actions_id'] as $key => $value) {
			if ($value == $actionId) {
				return $_REQUEST['aow_actions_name'][$key];
			}
		}
				
		return "";
	}
	
	private function getMappedFields($aowActionsBean) {
		$retval = array();
		
		$data = unserialize(base64_decode($aowActionsBean->parameters));
		
		// Add fields
		foreach ($data['field'] as $key => $value) {
			if ($aowActionsBean->action != "CreateRecord" && $aowActionsBean->action != "ModifyRecord")
				continue;
			
			$retval[] = $this->getOneMappedField(
							$aowActionsBean->action, 
							$aowActionsBean->aow_workflow_id, 
							$aowActionsBean->id, 
							$data['record_type'],
							1,
							$value,
							$data['value_type'][$key],
							$data['value'][$key]);
		}
		
		// Add relationships
		foreach ($data['rel'] as $key => $value) {
			if ($aowActionsBean->action != "CreateRecord" && $aowActionsBean->action != "ModifyRecord")
				continue;
			
			$retval[] = $this->getOneMappedField(
							$aowActionsBean->action, 
							$aowActionsBean->aow_workflow_id, 
							$aowActionsBean->id, 
							$data['record_type'],
							2,
							$value,
							$data['rel_value_type'][$key],
							$data['rel_value'][$key]);
		}
		
		return $retval;
	}
	
	// Map one field from "AOW Actions" to "Workflow Field Updates"
	private function getOneMappedField($wf_action_c, $aow_workflow_id_c, $aow_actions_id_c, $wf_record_type_c, $wf_field_type_c, $wf_item_name_c, $wf_item_action_c, $value) {
		global $beanList;
		
		$bean = new $beanList[$wf_record_type_c]();
		
		$retval = array(
			"wf_action_c" => $wf_action_c,
			"wf_record_type_c" => $wf_record_type_c,
			"wf_field_type_c" => $wf_field_type_c,
			"wf_item_name_c" => $wf_item_name_c,
			"wf_item_name_label_c" => $bean->field_name_map[$wf_item_name_c]['vname'],
			"wf_item_action_c" => $wf_item_action_c,
			"wf_item_value_c" => "",
			"wf_item_value_label_c" => "",
			"aow_workflow_id_c" => $aow_workflow_id_c,
			"aow_workflow_module_c" => $this->wfModule,
			"aow_actions_id_c" => $aow_actions_id_c,
		);
				
		if (is_array($value)) {
			foreach ($value as $stringItem) {
				if (strlen($retval['wf_item_value_c']) > 0)
					$retval['wf_item_value_c'] .= " ";
				
				$retval['wf_item_value_c'] .= $stringItem;
			}
		} else {
			$retval['wf_item_value_c'] = $value;
		}
		
		if ($wf_item_action_c == "Field") {
			$retval['wf_item_value_label_c'] = $this->wfBean->field_name_map[$retval['wf_item_value_c']]['vname'];
		}
		
		return $retval;
	}
	
	private function deleteRecord($aow_actions_id_c) {
		$sql = "SELECT id_c 
				FROM dtbc_workflow_field_updates_cstm
				WHERE aow_actions_id_c = " . $this->db->quoted($aow_actions_id_c);
				
		$res = $this->db->query($sql);
		$idList = "";
		
		while ($row = $this->db->fetchByAssoc($res)) {
			if (strlen($idList) > 0)
				$idList .= ", ";
			$idList .= $this->db->quoted($row['id_c']);
		}
		
		if (empty($idList))
			return "";
		
		$sql = "DELETE
				FROM dtbc_workflow_field_updates
				WHERE id IN (" . $idList . ")";
		
		$this->db->query($sql);
		
		$sql = "DELETE
				FROM dtbc_workflow_field_updates_cstm
				WHERE id_c IN (" . $idList . ")";
		
		$this->db->query($sql);
	}
	
	private function initClass() {
		global $beanList;
		$this->wfModule = $_REQUEST['flow_module'];
		$this->wfBean = new $beanList[$this->wfModule]();
		$this->db = DBManagerFactory::getInstance();
	}
	
	private function isRunnable() {
		return isset($_REQUEST['aow_actions_id']) && isset($_REQUEST['aow_actions_deleted']);
	}
}