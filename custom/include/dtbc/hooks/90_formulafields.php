<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/formulafields/formulafields_config.php");

class formulafields_class {
	
	private $updateFields;

	public function formulafields_function(&$beanAction, $event, $arguments) {
		global $sugar_config;
		
		// Load custom config
		FormulaFieldsConfig::setConfig();
		
		$this->updateFields = array();

		// Make sure to run this hook only once
		if ($this->isValidField($_REQUEST, 'formulafields_function'))
			return true;
		
		// Skip on merging
		if (!empty($_REQUEST['module']) && isset($_REQUEST['module']) && $_REQUEST['module'] == 'MergeRecords')
			return true;
		
		$_REQUEST['formulafields_function'] = 1;

		if ((!empty($_REQUEST['module']) && isset($_REQUEST['module']) && strlen($_REQUEST['module']) > 0) &&
			(!empty($_REQUEST['record']) && isset($_REQUEST['record']) && strlen($_REQUEST['record']) > 0)) {
			$module = $_REQUEST['module'];
			$bean = BeanFactory::getBean($module, $_REQUEST['record']);

			if (!empty($sugar_config['solaredge']) && !empty($sugar_config['solaredge']['FormulaFields']) && !empty($sugar_config['solaredge']['FormulaFields'][$module])) {
				foreach($sugar_config['solaredge']['FormulaFields'][$module] as $settings) {
					if (!$this->isValidField($settings, 'FromField'))
						continue;

					$beanField = $settings['FromField'];
					
					if (!property_exists($bean, $beanField))
						continue;
					
					if ($bean->fetched_row[$beanField] == $bean->$beanField)
						continue;
					
					if (!$this->isValidField($settings, 'ToModule'))
						continue;
					
					if (!$this->isValidField($settings, 'ToField'))
						continue;
					
					if ($this->isValidField($settings, 'RelationName')) {
						
						// Handle relations
						if ($bean->load_relationship($settings['RelationName'])) {
							$relationName = $settings['RelationName'];
							$relBeans = $bean->$relationName->getBeans();
							if (!empty($relBeans) && is_array($relBeans) && count($relBeans) > 0) {
								
								// Refresh all related bean
								foreach ($relBeans as $relBean) {
									$this->handleFieldUpdates($bean, $beanField, $relBean, $settings);
								}
							}
						}
					} else if (!empty($settings['RelateFieldName'])) {
						
						// Handle relate field
						$idField = $bean->field_name_map[$beanField]['id_name'];
						$relBean = BeanFactory::getBean($settings['ToModule'], $bean->$idField);
						$this->handleFieldUpdates($bean, $beanField, $relBean, $settings);
					} else if (!empty($settings['RelatedModuleField'])) {
						
						// Handle core hack fields which available only in one side of a relation, eg. Case's contact_created_by_id field
						global $db;
						$sql = "SELECT id 
								FROM " . $settings['ToModule'] . " 
								LEFT JOIN " . $settings['ToModule'] . "_cstm ON id_c = id 
								WHERE " . $settings['RelatedModuleField'] . " = " . $db->quoted($bean->id);
								
						$res = $db->query($sql);

						while ($row = $db->fetchByAssoc($res)) {
							$relBean = BeanFactory::getBean($settings['ToModule'], $row['id']);
							$this->handleFieldUpdates($bean, $beanField, $relBean, $settings);
						}
					} else {
						if (file_exists($settings['Include'])) {
							require_once($settings['Include']);
							if (function_exists($settings['Function']))
								call_user_func($settings['Function'], $bean);
						}
					}
				}
			}
		}
		$this->runUpdateField();
		
		// Unload custom config
		FormulaFieldsConfig::destroyConfig();
	}
	
	// Calculate the field value which must be set in the related field
	private function handleFieldUpdates($bean, $beanField, $relBean, $settings) {
		global $app_list_strings;
		
		if (!property_exists($relBean, $settings['ToField']))
			return;
				
		// Identify database table name
		$tableName = $relBean->table_name;
		if ($this->isValidField($settings, 'IsLocatedInCstmTable') && $settings['IsLocatedInCstmTable'])
			$tableName .= "_cstm";
		
		// If the field is set then try assigning value to it
		if ($this->isValidField($settings, 'Include') && $this->isValidField($settings, 'Function')) {
			
			// Use external file and assign the external functions return value to the field
			if (file_exists($settings['Include'])) {
				require_once($settings['Include']);
				if (function_exists($settings['Function']))
					$this->saveUpdateField($relBean, $settings['ToField'], call_user_func($settings['Function'], $bean, $relBean), $tableName);
			}
		} else {
			
			// Simple value copy
			if ($this->isValidField($settings, 'DropDownListName') && !empty($app_list_strings[$settings['DropDownListName']]))
				$this->saveUpdateField($relBean, $settings['ToField'], $app_list_strings[$settings['DropDownListName']][$bean->$beanField], $tableName);
			else
				$this->saveUpdateField($relBean, $settings['ToField'], $bean->$beanField, $tableName);
		}
		
	}
	
	// Make and run update query
	private function runUpdateField() {
		global $db;
		
		foreach ($this->updateFields as $recordId => $tables) {
			foreach ($tables as $tableName => $values) {
				$idFieldName = substr($tableName, -5) == "_cstm" ? "id_c" : "id";
				
				$sql = "UPDATE " . $tableName . " 
						SET " . $this->concatenateFields($values) . " 
						WHERE " . $idFieldName . " = " . $db->quoted($recordId);
						
				$db->query($sql);
			}
		}
	}
	
	// Concatenate field values for update query
	private function concatenateFields($values) {
		$retval = "";
		foreach($values as $value) {
			if (strlen($retval) > 0)
				$retval .= ", ";
			$retval .= $value;
		}
		return $retval;
	}
	
	// Save modified fields in an internal field
	private function saveUpdateField($bean, $fieldName, $value, $tableName) {
		global $db;
		
		// Initialize entries
		if (!isset($this->updateFields[$bean->id]))
			$this->updateFields[$bean->id] = array(
				$tableName => array()
			);
		
		if (!isset($this->updateFields[$bean->id][$tableName]))
			$this->updateFields[$bean->id][$tableName] = array();
		
		// Add entry
		$this->updateFields[$bean->id][$tableName][] = $fieldName . " = " . $db->quoted($value);
	}
	
	// Check field existance
	private function isValidField($settings, $fieldName) {
		return !empty($settings[$fieldName]) && isset($settings[$fieldName]) && strlen($settings[$fieldName]) > 0;
	}
	
}