<?php

require_once("custom/include/dtbc/helpers.php");

class RemoveAssignedUser {
	public function after_save($bean, $event, $arguments) {
		
		if ($bean->is_email_to_case == 1 && isset($_REQUEST['emailToCaseNew']) && $_REQUEST['emailToCaseNew'] == 1) {
			$bean->assigned_user_id = '1';
			$this->sqlSetter($bean->id);
		}
	}
	
	private function sqlSetter($recId) {
		global $db;
		$sql = "UPDATE cases SET assigned_user_id = '1' WHERE id = " . $db->quoted($recId);
		$db->query($sql);
	}
}
