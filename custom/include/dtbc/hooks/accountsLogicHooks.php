<?php

if(!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class AccountsLogicHooks{

    private $db;

    public function __construct(){

        $this->db = DBManagerFactory::getInstance();
    }


    public function updateUSAZipCode($bean, $events, $arguments){

        if(!$this->isValidCriteriaForUpdate($bean))
            return;

        $sql = "SELECT 
                    id
                FROM
                    uzc1_usa_zip_codes
                WHERE
                    deleted = 0 AND name LIKE " . $this->db->quoted($bean->zip_postal_code_c);

        $queryResult = $this->db->query($sql);
        $result = $this->db->fetchByAssoc($queryResult);

        if(!empty($result))
            $bean->uzc1_usa_zip_codes_id_c = $result['id'];
        else
            $bean->uzc1_usa_zip_codes_id_c = '';
    }

    public function updateAllianceTransactionsFields($bean, $events, $arguments){

        if(!$this->isValidRelationForLastAlliance($bean, $arguments, $events) || ($_REQUEST['module'] != 'Accounts' && $events != "after_relationship_delete"))
            return;

        $sql = DtbcHelpers::getAllianceTransactionSql($bean->id);
        $queryResult = $this->db->query($sql);
        $result = $this->db->fetchByAssoc($queryResult);

        $lastAllianceTransaction = '';
        $alliancePlanStartDate = '';

        if (isset($result['max_date']))
            $lastAllianceTransaction = $result['max_date'];

        if (isset($result['min_date']))
            $alliancePlanStartDate = $result['min_date'];

        $bean->Last_Alliance_Transaction__c = $lastAllianceTransaction;
        $bean->Loyalty_Plan_start_date__c = $alliancePlanStartDate;

        if($this->checkFetchedRow($bean))
            $bean->save();
    }

    private function checkFetchedRow(&$bean){

        return $bean->fetched_row['Last_Alliance_Transaction__c'] != $bean->Last_Alliance_Transaction__c || $bean->fetched_row['Loyalty_Plan_start_date__c'] !=  $bean->Loyalty_Plan_start_date__c;
    }

    private function isValidRelationForLastAlliance(&$bean, &$arguments){

        return !empty($bean) && !empty($bean->id) && $arguments['relationship'] == 'accounts_al1_alliance_transaction_1' &&
            $arguments['module'] == 'Accounts' && $arguments['related_module'] == 'AL1_Alliance_Transaction';
    }

    private function isValidCriteriaForUpdate(&$bean){

        return !empty($bean) && $bean->country_c == 'Unitedstates' && empty($bean->usa_zip_code_c) &&!empty($bean->zip_postal_code_c);
    }
}
