<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

function get_subpanel_info($params) {
	global $db;

	$return_array['select'] = " SELECT emqu_dtbc_email_queue.*";
	$return_array['from'] = " FROM emqu_dtbc_email_queue emqu ";
	$return_array['where'] = " WHERE emqu_dtbc_email_queue.deleted = " . $db->quoted('0') . " AND emqucstm.aow_workflow_id_c = " . $db->quoted($params['id']);
	$return_array['join'] = " LEFT JOIN emqu_dtbc_email_queue_cstm emqucstm ON emqu_dtbc_email_queue.id = emqucstm.id_c ";
	$return_array['join_tables'] = "";
	
	return $return_array;
}
