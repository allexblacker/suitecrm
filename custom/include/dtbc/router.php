<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class Router {
	
	public static function getCreateProjectWithPrefillRoute($recordId){
		
		$opportunityBean = BeanFactory::getBean('Opportunities',$recordId);
		$account = BeanFactory::getBean('Accounts',$opportunityBean->account_id);
		return "index.php?module=P1_Project&action=EditView&return_module=P1_Project&return_action=DetailView&accounts_p1_project_1_name="
		. $account->name . "&opportunities_p1_project_1_name=" 
		. $opportunityBean->name . "&accounts_p1_project_1accounts_ida=" . $account->id . "&opportunities_p1_project_1opportunities_ida=" 
		. $opportunityBean->id;
	}
	
	public static function returnToDetailView($module, $record) {
		$queryParams = array(
			'module' => $module,
			'action' => 'DetailView',
			'record' => $record
		);
		
		SugarApplication::redirect('index.php?' . http_build_query($queryParams));
	}
}