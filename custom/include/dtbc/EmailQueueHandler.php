<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once('modules/Emails/Email.php');
require_once('include/SugarPHPMailer.php');

class EmailQueueHandler {
	
	private $enableLog = false;

	public function run() {
		global $db, $sugar_config;
		
		$emailsList = $this->getEmailsFromQueue();
		
		while ($row = $db->fetchByAssoc($emailsList)) {
			$emailTemplate = $this->getEmailTemplate($row);
			
			if ($emailTemplate === false)
				continue;
			
			// Handling special case of case update thread settings
			if ($row['smtp_config_key_c'] == 'AOP_Case_Updates')
				$SMTPConfig = $row['smtp_config_key_c'];
			else
				$SMTPConfig = $sugar_config['solaredge']['SMTPList'][$row['smtp_config_key_c']];
			
			$attachments = $this->getAttachments($emailTemplate);
			
			if ($this->sendEmailWithFrom($SMTPConfig, $attachments, $row)) {
				$this->createMailObject($attachments, $row);
				$this->setMailToDelivered($row);
			} else {
				$this->setMailToFailed($row);
			}
		}
		
		return true;
	}
	
	private function setMailToFailed($row) {
		global $db, $sugar_config;
		
		$this->jobLogger("Mail sent failed (queue id: " . $row['id'] . ")");
		
		$counter = $row['failer_counter_c'] + 1;
		
		if ($counter >= $sugar_config['solaredge']['email_queue']['max_retry']) {
			$status = 3;
			$next = 'UTC_TIMESTAMP';
		} else {
			$status = 4;
			$interval = $sugar_config['solaredge']['email_queue']['retry_time'][count($sugar_config['solaredge']['email_queue']['retry_time']) - 1];
			if (count($sugar_config['solaredge']['email_queue']['retry_time']) >= $row['failer_counter_c'])
				$interval = $sugar_config['solaredge']['email_queue']['retry_time'][$row['failer_counter_c']];
			$next = 'UTC_TIMESTAMP + INTERVAL ' . $interval . ' second';
		}
		
		$sql = "UPDATE emqu_dtbc_email_queue_cstm 
				SET status_c = " . $db->quoted($status) . ", 
				failer_counter_c = " . $db->quoted($counter) . ", 
				next_delivery_c = " . $next . " 
				WHERE id_c = " . $db->quoted($row['id']);
			
		$db->query($sql);
		
		$this->setDateModifiedToNow($row['id']);
	}
	
	private function setMailToDelivered($row) {
		global $db;
		
		$this->jobLogger("Mail sent (queue id: " . $row['id'] . ")");
		
		$sql = "UPDATE emqu_dtbc_email_queue_cstm 
				SET status_c = " . $db->quoted("2") . ", 
				failer_counter_c = 0, 
				next_delivery_c = NULL 
				WHERE id_c = " . $db->quoted($row['id']);
		
		$db->query($sql);
		
		$this->setDateModifiedToNow($row['id']);
	}
	
	private function setDateModifiedToNow($id) {
		global $db;
		
		$sql = "UPDATE emqu_dtbc_email_queue 
				SET date_modified = UTC_TIMESTAMP 
				WHERE id = " . $db->quoted($id);
		
		$db->query($sql);
	}
	
	private function createMailObject($attachments, $row) {
		$emailObj = new Email();

		$emailObj->to_addrs = $row['to_address_c'];
		$emailObj->cc_addrs = $row['cc_address_c'];
		$emailObj->bcc_addrs = $row['bcc_address_c'];
		$emailObj->type= 'out';
		$emailObj->deleted = '0';
		$emailObj->name = $row['email_subject_c'];
		$emailObj->description = $row['email_content_c'];
		$emailObj->description_html = $row['email_content_html_c'];
		$emailObj->from_addr = $row['from_address_c'];
		$emailObj->parent_type = $row['related_bean_type_c'];
		$emailObj->parent_id = $row['related_bean_id_c'];
		$emailObj->date_sent = TimeDate::getInstance()->nowDb();
		$emailObj->modified_user_id = '1';
		$emailObj->created_by = '1';
		$emailObj->status = 'sent';
		$emailObj->save();

		foreach($attachments as $attachment) {
			$note = new Note();
			$note->id = create_guid();
			$note->date_entered = $attachment->date_entered;
			$note->date_modified = $attachment->date_modified;
			$note->modified_user_id = $attachment->modified_user_id;
			$note->assigned_user_id = $attachment->assigned_user_id;
			$note->new_with_id = true;
			$note->parent_id = $emailObj->id;
			$note->parent_type = $attachment->parent_type;
			$note->name = $attachment->name;;
			$note->filename = $attachment->filename;
			$note->file_mime_type = $attachment->file_mime_type;
			
			$fileLocation = "upload://{$attachment->id}";
			$dest = "upload://{$note->id}";
			if(!copy($fileLocation, $dest)) {
				$this->jobLogger("EMAIL 2.0: could not copy attachment file to " . $fileLocation . " => " . $dest);
			}
			
			$note->save();
		}
	}
	
	private function sendEmailWithFrom($SMTPList, $attachments, $row) {
		$emailTo = explode(',', $row['to_address_c']);
        
		if (empty($emailTo)) {
			$this->jobLogger("Failed to send email bacause 'to' address is empty");
			return false;
		}
        
        $mail = new SugarPHPMailer();
        
        $mail->IsSMTP();
        $this->setSMTPSettings($mail, $SMTPList);
		
		$mail->From = $row['from_address_c'];
		$mail->FromName = $row['from_name_c'];
        
        $mail->Body = $row['email_content_html_c'];
        $mail->AltBody = $row['email_content_c'];
        $mail->handleAttachments($attachments);
        
		$mail->ClearAllRecipients();
		$mail->ClearReplyTos();
		$mail->Subject = $row['email_subject_c'];
		$mail->prepForOutbound();
		
		// Set Recipients
		$emailCc = explode(',', $row['cc_address_c']);
		$emailBcc = explode(',', $row['bcc_address_c']);
		
        foreach ($emailTo as $to) {
            $mail->AddAddress($to);
        }

        if (!empty($emailCc)) {
            foreach($emailCc as $email){
                $mail->AddCC($email);
            }
        }
		
        if (!empty($emailBcc)) {
            foreach($emailBcc as $email){
                $mail->AddBCC($email);
            }
        }

		return @$mail->Send();
    }
	
	private function setSMTPSettings(&$mail, $SMTPList) {
		if ($SMTPList == 'AOP_Case_Updates') {
			// Case update thread like emails
			$admin = new Administration();
			$admin->retrieveSettings();
			$mail->setMailerForSystem();
			return;
		}
		
        $mail->CharSet = $SMTPList['email_charset'];
        $mail->SMTPSecure = $SMTPList['email_smtp_secure'];
        $mail->Host = $SMTPList['email_host'];
        $mail->Port = $SMTPList['email_port'];
        $mail->Username = $SMTPList['email_username'];
        $mail->Password = $SMTPList['email_password'];
        $mail->SMTPAuth =  $SMTPList['email_smtp_auth'];
        $mail->IsHTML($SMTPList['email_is_html']);
    }
	
	private function getAttachments(EmailTemplate $template) {
        $attachments = array();
        if($template->id != ''){
            $note_bean = new Note();
            $notes = $note_bean->get_full_list('',"parent_type = 'Emails' AND parent_id = '" . $template->id . "'");

            if($notes != null){
                foreach ($notes as $note) {
                    $attachments[] = $note;
                }
            }
        }
        return $attachments;
    }
	
	private function getEmailTemplate($params) {
		$emailTemp = new EmailTemplate();
		$emailTemp->retrieve($params['email_template_id_c']);
		
		if ($emailTemp->id == '') {
			$this->jobLogger('Failed to get email template (Email queue id: ' . $params['id'] . ')');
            return false;
        }
		
		return $emailTemp;
	}

	private function getEmailsFromQueue() {
		global $db;
		$sql = 'SELECT *
				FROM emqu_dtbc_email_queue
				JOIN emqu_dtbc_email_queue_cstm ON id = id_c
				WHERE deleted = 0 AND
				(status_c = 1 OR
					(status_c = 4 AND
					next_delivery_c < NOW())
				)';
		return $db->query($sql);
	}
	
	private function jobLogger($value) {
		if ($this->enableLog)
			DtbcHelpers::logger($value, "email_queue.log");
	}
}