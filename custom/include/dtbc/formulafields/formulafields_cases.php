<?php

require_once("custom/include/dtbc/helpers.php");

class FormulaFields {
	private $fwdUrls_ups = 'http://wwwapps.ups.com/etracking/tracking.cgi?TypeOfInquiryNumber=T&tracknums_displayed=1&loc=US_US&InquiryNumber1=';
	private $fwdUrls_fedex = 'https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber=';
	
	private $contactBean;
	private $accountBean;
	private $siteBean;
	private $usaZipCodeBean;
	
	public function before_save($bean, $event, $arguments) {
		$this->setRelatedBeans($bean);

		$bean->account_contact_s_comments_c = $this->get_account_contact_s_comments_c();
		$bean->account_support_comments_c = $this->get_account_support_comments_c();
		$bean->action_log_c = $this->get_action_log_c($bean->inverter_serial_c);
		$bean->axiplus_indicator_c = $this->get_axiplus_indicator_c($bean->created_by_address_c);
		$bean->canadian_province_c = $this->get_canadian_province_c();
		$bean->certified_installer_tesla_c = $this->get_certified_installer_tesla_c();
		$bean->contact_email_c = $this->get_contact_email_c();
		$bean->contact_name_c = $this->get_contact_name_c();
		$bean->contact_support_comments_c = $this->get_contact_support_comments_c();
		$bean->county_c = $this->get_county_c();
		$bean->rma_pr_number_c = $this->get_rma_pr_number_c($bean, $bean->rma_pr_number_c);
		$bean->reason_for_not_eligible_c = $this->get_reason_for_not_eligible_c($bean->if_eligible_was_true_c, $bean->reason_for_not_eligible_cert_c, $bean->case_status_c, $bean->ibolt_status_c, $bean->monitored_c, $bean->installation_rules_c, $bean->rma_pr_number_c, $bean->id, $bean->date_entered, $bean->case_record_type_c == "rma");
		$bean->eligible_for_compensation_c = $this->get_eligible_for_compensation_c($bean->reason_for_not_eligible_c);
		$bean->estimated_pay_date_c = $this->get_estimated_pay_date_c($bean->transferred_to_finance_c);
		$bean->internal_notes_c = $this->get_internal_notes_c();
		$bean->inverter_id_c = $this->get_inverter_id_c($bean->inverter_serial_c);
		$bean->inverter_type_c = $this->get_inverter_type_c($bean->inverter_serial_c);
		$bean->valid_for_service_indication_c = $this->get_valid_for_service_indication_c($bean->valid_c);
		$bean->link_to_monitoring_c = $this->get_link_to_monitoring_c();
		$bean->link_to_tesla_rma_c = $this->get_link_to_tesla_rma_c($bean);
		$bean->link_to_ups_c = $this->get_link_to_ups_c($bean->shipping_tracking_number_c);
		$bean->link_to_ups_2_c = $this->get_link_to_ups_2_c($bean->shipping_tracking_number_2_c);
		$bean->monitoring_server_notes_c = $this->get_monitoring_server_notes_c();
		$bean->peak_power_kw_c = $this->get_peak_power_kw_c();
		$bean->place_name_canada_only_c = $this->get_place_name_canada_only_c();
		$bean->priority_indicator_c = $this->get_priority_indicator_c($bean->created_by_address_c);
		$bean->public_modules_c = $this->get_public_modules_c();
		$bean->receiving_forwarder_url_c = $this->get_receiving_forwarder_url_c($bean->receiving_forwarder_c);
		$bean->receiving_tracking_link_c = $this->get_receiving_tracking_link_c($bean->receiving_forwarder_url_c, $bean->return_label_awb_c);
		$bean->rma_yes_no_c = $this->get_rma_yes_no_c($bean->rma_in_erp_c);
		$bean->rma_contant_email_c = $this->get_contact_email_c();
		$bean->roof_type_c = $this->get_roof_type_c();
		$bean->service_instruction_flag_c = $this->get_service_instruction_flag_c();
		$bean->service_level_c = $this->get_service_level_c();
		$bean->site_address_c = $this->get_site_address_c();
		$bean->site_city_c = $this->get_site_city_c();
		$bean->site_country_c = $this->get_site_country_c();
		$bean->site_state_c = $this->get_site_state_c();
		$bean->site_zip_c = $this->get_site_zip_c();
		$bean->solaredge_service_kit_holder_c = $this->get_solaredge_service_kit_holder_c();
		$bean->special_service_instruction_c = $this->get_special_service_instruction_c();
		$bean->state_zip_c = $this->get_state_zip_c();
		$bean->whole_shipping_address_c = $this->get_whole_shipping_address_c($bean);
		$bean->alert_override_c = $this->get_alert_override_c($bean->alert_override_c, $bean->case_record_type_c);
		$bean->change_indicator_c = $this->get_change_indicator_c($bean->alert_override_c);
		$bean->field_service_kit_holder_c = $this->get_field_service_kit_holder_c();
		
		$bean->internal_comment_c = $this->get_internal_comment_c($bean->new_internal_comment_c);
		$bean->forwarder_url_c = $this->get_forwarder_url_c($bean->receiving_forwarder_c);
		$bean->service_level_flag_c = $this->get_service_level_flag_c();
		$bean->vendor_number_c = $this->get_vendor_number_c();
		$bean->city_c = $this->get_city_c();
		$bean->tracking_number_f_c = $bean->tracking_number_c;
		
		// New RMA Status change
		if (!empty($_REQUEST['newrma']) && isset($_REQUEST['newrma']) && $_REQUEST['newrma'] == '1' && $bean->get_address_confirmation_c == 0) {
			$bean->case_status_c = 4; // RMA approved
		}
		
		// Field Engineer Status change
		if (!empty($_REQUEST['fieldengineer']) && isset($_REQUEST['fieldengineer']) && $_REQUEST['fieldengineer'] == '1') {
			$bean->case_sub_status_c = 1; // Escalated to field engineer
		}
		
		// Log sub status change - log also when case created
		if ((!isset($bean->fetched_row['case_sub_status_c'])) || 
			(isset($bean->fetched_row['case_sub_status_c']) && $bean->case_sub_status_c != $bean->fetched_row['case_sub_status_c'])) {
			$bean->last_sub_status_change = date("Y-m-d H:i:s");
		}
		
		// New RMA
		if (!empty($_REQUEST['newrma']) && isset($_REQUEST['newrma'])) {
			$bean->case_record_type_c = "rma";
			$bean->case_status_c = $bean->get_address_confirmation_c ? "17" : "4";
			
		}
		
		//$bean->battery_valid_for_service_c = $this->get_battery_valid_for_service_c($bean->battery_is_monitored_c, $bean->any_battery_serial_number_c);
	}
	
	private function get_field_service_kit_holder_c() {
		if ($this->accountBean == null)
			return '';
		
		// Get Service kit holder's actual value
		$_REQUEST['accountrecordid'] = $this->accountBean->id;
		require_once($this->accountBean->field_name_map['service_kit_c']['function']['include']);
		$serviceKitValue = dtbc_serviceKit();

		return $serviceKitValue > 0 ? DtbcHelpers::getImageTagForFormulaFields('Toolbox.jpg','Service kit holder',true,false,36,36) : "";
	}
	
	private function get_alert_override_c($originalValue, $recordType) {
		return strtolower($recordType) == 'new' ? 'red' : $originalValue;
	}
	
	private function get_change_indicator_c($alert_override) {
		switch (strtolower($alert_override)) {
			case '':
				return '';
			case 'red':
				return DtbcHelpers::getImageTagForFormulaFields('New_red_envelope.png','Change has been made',true,false,15,25);
			case 'grey':
				return DtbcHelpers::getImageTagForFormulaFields('grey_envelope.png','Change has been made',true,false,15,25);
			default:
				return '';
		}
	}
	
	private function get_battery_valid_for_service_c($isMonitored, $anySerial) {
		/*
		battery_warranty_is_still_valid__c "Interface related - need to check if it is in the interface field mapping"
		if ( && $isMonitored == 1)
			return "Yes";
		else if (empty($anySerial) || strlen($anySerial) == 0)
			return "";
		else
			return "No";
		*/
	}
	
	private function get_city_c() {
		if ($this->usaZipCodeBean != null && !empty($this->usaZipCodeBean->city))
			return $this->usaZipCodeBean->city;

		return "";
	}
	
	private function get_vendor_number_c() {
		if ($this->accountBean != null && !empty($this->accountBean->vendor_number_c))
			return $this->accountBean->vendor_number_c;
		return "";
	}
	
	private function get_service_level_flag_c() {
		if ($this->accountBean != null && !empty($this->accountBean->service_level_c))
			return $this->accountBean->service_level_c;
		return "";
	}
	
	private function get_forwarder_url_c($receiveForwarder) {
		if ($receiveForwarder == "UPS")
			return "<a href='" . $this->fwdUrls_ups . "' target='_blank'>UPS</a>";
		
		if ($receiveForwarder == "FEDEX")
			return "<a href='" . $this->fwdUrls_fedex . "' target='_blank'>FEDEX</a>";
		
		return "";
	}
	
	private function get_internal_comment_c($newComment) {
		if (!empty($newComment) && $newComment == 1)
			return "<img src='custom/include/dtbc/images/Blue_envelope.png' alt='' height='15' width='25'/>";
		
		return "";
	}
	
	private function get_whole_shipping_address_c($caseBean) {
		return $this->getListValue('address_type_list', $caseBean->address_type_c) . " " . 
				$caseBean->rma_country_c . " " . 
				$this->getListValue('state_0', $caseBean->rma_state_c) . " " . 
				$caseBean->rma_city_c . " " . 
				$caseBean->rma_zip_postal_code_c . " " . 
				$caseBean->ship_to_name_c . " " . 
				$caseBean->rma_contant_email_c . " " . 
				$caseBean->site_contact_person_name_c . " " . 
				$caseBean->site_contact_person_phone_c;
	}
	
	private function get_state_zip_c() {
		if ($this->usaZipCodeBean != null && !empty($this->usaZipCodeBean->state))
			return $this->usaZipCodeBean->state;

		return "";
	}
	
	private function get_special_service_instruction_c() {
		if ($this->siteBean != null && !empty($this->siteBean->special_service_instruction))
			return $this->siteBean->special_service_instruction;
		
		return "";
	}
	
	private function get_solaredge_service_kit_holder_c() {
		global $sugar_config;
		
		if ($this->accountBean != null && !empty($this->accountBean->name) && in_array($this->accountBean->name, $sugar_config['solaredge']['service_kit_holder_companies']))
			return "<img src='custom/include/dtbc/images/Toolbox.jpg' alt='' />";

		return "";
	}
	
	private function get_site_zip_c() {
		if ($this->siteBean != null && !empty($this->siteBean->site_zip))
			return $this->siteBean->site_zip;
		
		return "";
	}
	
	private function get_site_state_c() {
		if ($this->siteBean != null && !empty($this->siteBean->site_state))
			return $this->getListValue('state_0', $this->siteBean->site_state);
		
		return "";
	}
	
	private function get_site_country_c() {
		global $app_list_strings;
		if ($this->siteBean != null && !empty($this->siteBean->site_country))
			return $app_list_strings['country_0'][$this->siteBean->site_country];
		
		return "";
	}
	
	private function get_site_city_c() {
		if ($this->siteBean != null && !empty($this->siteBean->site_city))
			return $this->siteBean->site_city;
		
		return "";
	}
	
	private function get_site_address_c() {
		if ($this->siteBean != null && !empty($this->siteBean->site_address))
			return $this->siteBean->site_address;
		
		return "";
	}
	
	private function get_service_level_c() {
		global $app_list_strings;
		if ($this->accountBean != null && !empty($this->accountBean->service_level_c))
			return $app_list_strings['service_level_list'][$this->accountBean->service_level_c];
		
		return "";
	}
	
	private function get_service_instruction_flag_c() {
		if ($this->siteBean != null && !empty($this->siteBean->service_instruction) && !empty($this->siteBean->site_name)) {
			if ($this->siteBean->service_instruction == 1 && strlen($this->siteBean->site_name) > 0)
				return "<img src='custom/include/dtbc/images/error_image.gif' alt='' />";
		}
		
		return "";
	}
	
	private function get_roof_type_c() {
		if ($this->siteBean != null && !empty($this->siteBean->roof_type))
			return $this->getListValue('roof_type_list', $this->siteBean->roof_type);
		
		return "";
	}
	
	private function get_rma_pr_number_c($caseBean, $originalValue) {
		if ($caseBean->load_relationship('cases_cases_1')) {
			$compensations = $caseBean->cases_cases_1->getBeans();
			if (!empty($compensations) && is_array($compensations) && count($compensations) > 0) {
				foreach ($compensations as $compensation) {
					return $compensation->rma_pr_number_c;
				}
			}
		}
		return $originalValue;
	}
	
	private function get_rma_yes_no_c($erpRma) {
		if (!empty($erpRma) && strlen($erpRma) > 0)
			return "Yes";
		
		return "No";
	}
	
	private function get_receiving_tracking_link_c($url, $label) {
		if (!empty($url) && strlen($url) > 0 && !empty($label) && strlen($label) > 0)
			return "<a href='" . $url . $label . "' target='_blank'>Tracking Link</a>";

		return "";
	}
	
	private function get_receiving_forwarder_url_c($forwarder) {
		if (!empty($forwarder)) {
			if ($forwarder == "UPS")
				return "<a href='http://wwwapps.ups.com/etracking/tracking.cgi?TypeOfInquiryNumber=T&tracknums_displayed=1&loc=US_US&InquiryNumber1=' target='_blank'>UPS</a>";
			
			if ($forwarder == "FEDEX")
				return "<a href='https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber=' target='_blank'>FEDEX</a>";
		}
	
		return "";
	}
	
	private function addToReasonForNotEligible(&$fieldValue, $value){
		 $fieldValue .= $value . " ";
	}
	
	private function get_reason_for_not_eligible_c($wasTrue, $reasonForNotEligibleCertification, $status, $iboltStatus, $monitored, $installation, $rmaNumber, $caseId, $dateEntered, $isRma) {
		$fieldValue = "";
		if ($wasTrue == 1 && $status != 10)
			return $fieldValue;
		
		if ($status != 15)
			$this->addToReasonForNotEligible($fieldValue, "RMA not completed.");
		
		if(!(empty($reasonForNotEligibleCertification)))
			$this->addToReasonForNotEligible($fieldValue, $reasonForNotEligibleCertification);
		
		if ($iboltStatus != "Success")
			$this->addToReasonForNotEligible($fieldValue, "Invalid serial.");
		
		if ($this->accountBean != null && !empty($this->accountBean->service_level_c)) {
			if ($this->accountBean->service_level_c == 4)
				$this->addToReasonForNotEligible($fieldValue, "Finance Hold.");
			if ($this->accountBean->service_level_c == 5)
				$this->addToReasonForNotEligible($fieldValue, "Service Hold.");
		}
		
		if ($monitored != 1)
			$this->addToReasonForNotEligible($fieldValue, "Not monitored.");
		
		if ($installation != 1)
			$this->addToReasonForNotEligible($fieldValue, "Installation rules.");
		
		if (strlen($rmaNumber) > 0)
			$this->addToReasonForNotEligible($fieldValue, "PR opened.");
		
		$changeDate = $isRma ? $this->getRecordTypeChangedToRMA($caseId, $dateEntered) : "";
		$isOldRma = $this->getIsOldRma($changeDate);
		
		if ($isOldRma)
			$this->addToReasonForNotEligible($fieldValue, "Old RMA.");
		
		return  $fieldValue;
	}
	
	private function getIsOldRma($changeDate) {
		if (strlen($changeDate) == 0)
			return false;
		
		try {
			$temp = explode(" ", $changeDate);
			$rmaDate = new DateTime($temp[0]);
			$today = new DateTime();
			$oneYearBefore = $today->modify('-1 year');
			
			return $rmaDate->getTimestamp() < $oneYearBefore->getTimestamp();
			
		} catch (Exception $e) {
			return false;
		}
		
	}
	
	private function getRecordTypeChangedToRMA($caseId, $dateEntered) {
		// Return the date when changed the case status to RMA
		global $db;
		$sql = "SELECT date_created 
				FROM cases_audit
				WHERE field_name = 'case_record_type_c' AND
				after_value_string = 'rma' AND
				parent_id = " . $db->quoted($caseId) . "
				ORDER BY date_created DESC";
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && !empty($res['date_created']))
			return $res['date_created'];
		
		return $dateEntered;
	}
	
	private function get_public_modules_c() {
		if ($this->siteBean != null && !empty($this->siteBean->public_modules))
			return $this->siteBean->public_modules;
		
		return "";
	}
	
	private function get_priority_indicator_c($address) {
		if (!empty($address) && strlen($address) > 0 && ($address == '1' || $address == '2'))
			return "<img src='custom/include/dtbc/images/Aligator.jpg' alt='' height='24' width='36'/>";
		
		return ""; // Account.SolarCity_Account_Flag__c
	}
	
	private function get_place_name_canada_only_c() {
		if ($this->usaZipCodeBean != null && !empty($this->usaZipCodeBean->place_name_canada_only))
			return $this->usaZipCodeBean->place_name_canada_only;

		return "";
	}
	
	private function get_peak_power_kw_c() {
		if ($this->siteBean != null && !empty($this->siteBean->peak_power_kw_h_peak))
			return $this->siteBean->peak_power_kw_h_peak;
		
		return "";
	}
	
	private function get_monitoring_server_notes_c() {
		if ($this->siteBean != null && !empty($this->siteBean->notes))
			return $this->siteBean->notes;

		return "";
	}
	
	private function get_link_to_ups_2_c($number) {
		if (!empty($number) && strlen($number) > 0)
			return "<a href='http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" . $number . "' target='_blank'>Link to UPS</a>";

		return "";
	}
	
	private function get_link_to_ups_c($number) {
		if (!empty($number) && strlen($number) > 0)
			return "<a href='http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" . $number . "' target='_blank'>Link to UPS</a>";
		
		return "";
	}
	
	private function get_link_to_tesla_rma_c() {
		return "<a href='https://teslafactory.wufoo.com/forms/qupqhie1354f9c/' target='_blank'>Tesla RMA</a>";
	}
	
	private function get_link_to_monitoring_c() {
		if ($this->siteBean != null && !empty($this->siteBean->solar_field_id))
			return "<a href='https://monitoring.solaredge.com/solaredge-web/p/site/" . $this->siteBean->solar_field_id . "/#/dashboard' target='_blank'>Link to Monitoring</a>";

		return "";
	}
	
	private function get_valid_for_service_indication_c($isValid) {
		if (!empty($isValid) && strlen($isValid) > 0 && strtolower($isValid) == 'yes')
			return "<img src='custom/include/dtbc/images/vsign.gif' alt=''/>";
		
		return "<img src='custom/include/dtbc/images/error32.png' alt=''/>";
	}
	
	private function get_inverter_type_c($serial) {
		if (!empty($serial) && strlen($serial) > 0) {
			$temp = strtolower($serial);
			if (strlen($temp) > 10) {
				$first2 = substr($temp, 0, 2);
				$first3 = substr($temp, 0, 3);
				$first11 = substr($temp, 0, 11);
				if ($first2 == "7e" ||
					$first2 == "ba" ||
					$first3 == "07e" ||
					$first3 == "0ba" ||
					(strpos(substr($serial, 0, 2), "S") !== false &&
					(strpos($first11, "7e") !== false ||
					 strpos($first11, "07e") !== false ||
					 strpos($first11, "0ba") !== false))) {
						return "JUPITER";
					 }
			}
			return "VENUS";
		}
		
		return "UNKNOWN";
	}
	
	private function get_inverter_id_c($serial) {
		if (!empty($serial) && strlen($serial) > 10)
			return substr(substr($serial, -11), 0, 8);
		
		return "";
	}

	
	private function get_internal_notes_c() {
		if ($this->siteBean != null && !empty($this->siteBean->internal_notes))
			return $this->siteBean->internal_notes;

		return "";
	}
	
	private function get_estimated_pay_date_c($trDate) {
		if (!empty($trDate) && strlen($trDate) > 0) {
			$dates = explode("-", $trDate);
			$dateInt = intval($dates[2]);
			
			if ($dateInt <= 5 || $dateInt > 20)
				return "around the 20 - by SLA";
			
			if ($dateInt > 5 && $dateInt < 20)
				return "around the 5 - by SLA";
		}
		
		return "";
	}
	
	private function get_eligible_for_compensation_c($reason) {
		if (empty($reason) || (!empty($reason) && (strlen($reason) == 0 || trim(strtolower($reason)) == "pr opened.")))
			return 1;
		
		return 0;
	}
	
	private function get_county_c() {
		if ($this->usaZipCodeBean != null && !empty($this->usaZipCodeBean->county))
			return $this->usaZipCodeBean->county;

		return "";
	}
	
	private function get_contact_support_comments_c() {
		if ($this->contactBean != null && !empty($this->contactBean->contact_support_comments_c) && strlen($this->contactBean->contact_support_comments_c) > 0)
			return $this->contactBean->contact_support_comments_c;
		
		return "";
	}
	
	private function get_contact_name_c() {
		if ($this->contactBean != null && !empty($this->contactBean))
			return $this->contactBean->first_name . " " . $this->contactBean->last_name;
		
		return "";
	}
	
	private function get_contact_email_c() {
		if ($this->contactBean != null && !empty($this->contactBean->email1))
			return $this->contactBean->email1;
		
		return "";
	}
	
	private function get_certified_installer_tesla_c() {
		global $app_list_strings;
		if ($this->accountBean != null && !empty($this->accountBean->certified_installer_tesla_c))
			return $app_list_strings['certified_installer_tesla_list'][$this->accountBean->certified_installer_tesla_c];
		
		return "";
	}
	
	private function get_canadian_province_c() {
		if ($this->usaZipCodeBean != null && !empty($this->usaZipCodeBean->canadian_province))
			return $this->usaZipCodeBean->canadian_province;
		
		return "";
	}
	
	private function get_axiplus_indicator_c($address) {
		if (!empty($address) && strlen($address) > 0 && ($address == '1' || $address == '2'))
			return "<img src='custom/include/dtbc/images/Aligator.jpg' alt='' height='24' width='36'/>";
		
		return "";
	}
	
	private function get_action_log_c($serial) {
		if (!empty($serial) && strlen($serial) > 0)
			return "<a href='http://passport/default.aspx?actions=" . $serial . "' target='_blank'>Action Log</a>";
		
		return "";
	}
	
	private function get_account_support_comments_c() {
		if ($this->accountBean != null && !empty($this->accountBean->support_comments_c))
			return $this->accountBean->support_comments_c;
		
		return "";
	}
	
	private function get_account_contact_s_comments_c() {
		if (($this->accountBean != null && !empty($this->accountBean->support_comments_c) && strlen($this->accountBean->support_comments_c) > 0) &&
			($this->contactBean != null && !empty($this->contactBean->contact_support_comments_c) && strlen($this->contactBean->contact_support_comments_c) > 0))
			return $this->accountBean->support_comments_c . " " . $this->contactBean->contact_support_comments_c;
		
		if (($this->accountBean != null && !empty($this->accountBean->support_comments_c) && strlen($this->accountBean->support_comments_c) > 0) &&
			($this->contactBean == null || ($this->contactBean != null && (empty($this->contactBean->contact_support_comments_c) || strlen($this->contactBean->contact_support_comments_c) == 0))))
			return $this->accountBean->support_comments_c;
			
		if (($this->accountBean == null || ($this->accountBean != null && (empty($this->accountBean->support_comments_c) || strlen($this->accountBean->support_comments_c) == 0))) &&
			($this->contactBean != null && !empty($this->contactBean->contact_support_comments_c) && strlen($this->contactBean->contact_support_comments_c) > 0))
			return $this->contactBean->contact_support_comments_c;
		
		return "";
	}
	
	private function setRelatedBeans($caseBean) {
		$this->contactBean = null;
		$this->accountBean = null;
		$this->siteBean = null;
		$this->usaZipCodeBean = null;
		
		if (!empty($caseBean->contact_created_by_id) && strlen($caseBean->contact_created_by_id) > 0) {
			$this->contactBean = BeanFactory::getBean("Contacts", $caseBean->contact_created_by_id);
		}
		
		if (!empty($caseBean->account_id) && strlen($caseBean->account_id) > 0) {
			$this->accountBean = BeanFactory::getBean("Accounts", $caseBean->account_id);
		}
		
		if ($caseBean->load_relationship('s1_site_cases')) {
			$sites = $caseBean->s1_site_cases->getBeans();
			if (!empty($sites) && is_array($sites) && count($sites) > 0) {
				foreach ($sites as $site) {
					$this->siteBean = BeanFactory::getBean("S1_Site", $site->id);
					break;
				}
			}
		}
		
		if ($caseBean->load_relationship('uzc1_usa_zip_codes_cases')) {
			$zipCodes = $caseBean->uzc1_usa_zip_codes_cases->getBeans();
			if (!empty($zipCodes) && is_array($zipCodes) && count($zipCodes) > 0) {
				foreach ($zipCodes as $zipCode) {
					$this->usaZipCodeBean = BeanFactory::getBean("UZC1_USA_ZIP_Codes", $zipCode->id);
					break;
				}
			}
		}
	}
	
	private function getListValue($listName, $listItemId) {
		global $app_list_strings;
		
		if (isset($app_list_strings[$listName]) && isset($app_list_strings[$listName][$listItemId]))
			return $app_list_strings[$listName][$listItemId];
		
		return '';
	}

}
