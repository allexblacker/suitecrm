<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class FormulaFields {
	
	private $opportunityBean;
	private $siteBean;
	
	public function before_save($bean, $event, $arguments) {
		$this->setRelatedBeans($bean);
		
		$bean->link_to_maps_c = '<a href="https://maps.google.com/maps?q=' . 
									$bean->site_address . 
									$bean->site_city . 
									$this->getListValue('state_0', $bean->site_state) . 
									$this->getListValue('country_0', $bean->site_country) . '/#/dashboard" target="_blank">Maps</a>';
		$bean->link_to_projects_c = $this->getSelfLink($bean->id, $bean->name);
		$bean->opportunity_owner_c = $bean->assigned_user_name;
		
		// Opportunity related field calculations
		if ($this->opportunityBean != null) {
			$bean->cae_c = $this->getListValue('cae_list', $this->opportunityBean->cae_c);
			$bean->country_c = $this->getListValue('country_0', $this->opportunityBean->country_c);
			$bean->cpm_c = $this->getOppRelatedFieldValue($bean->opportunities_p1_project_1opportunities_ida);
			$bean->estimated_kw_c = $this->opportunityBean->total_kw2_c > 0 ? $this->opportunityBean->total_kw2_c : $this->opportunityBean->estimated_kw_c;
			$bean->opportunity_stage_c = $this->getListValue('sales_stage_list', $this->opportunityBean->sales_stage);
			$bean->region_c = $this->opportunityBean->region_c;
			$bean->state_c = $this->getListValue('state_0', $this->opportunityBean->state_c);
			$bean->us_region_c = $this->getListValue('us_region_0', $this->opportunityBean->us_region_c);
		}
		
		// Site related field calculations
		if ($this->siteBean != null) {
			$bean->link_to_monitoring_c = '<a href="https://monitoring.solaredge.com/solaredge-web/p/site/' . $this->siteBean->solar_field_id . '/#/dashboard" target="_blank">Link to Monitoring</a>';
		}

	}
	
	private function getSelfLink($recordId, $text) {
		global $sugar_config;
		
		$url = $sugar_config['site_url'];
		
		if (substr($url, -1) != "/")
			$url .= "/";
		
		$url .= "index.php?module=P1_Project&action=DetailView&record=" . $recordId;
		
		return DtbcHelpers::getHyperLinkForFormulaFields($url, $text);
	}
	
	private function getOppRelatedFieldValue($opportunityId) {
		$oppBean = BeanFactory::getBean("Opportunities", $opportunityId);
		$userBean = BeanFactory::getBean("Users", $oppBean->user_id_c);
		return $userBean->name;
	}
		
	private function setRelatedBeans($bean) {
		$this->opportunityBean = null;
		$this->siteBean = null;
		
		if ($bean->load_relationship('opportunities_p1_project_1')) {
			$opps = $bean->opportunities_p1_project_1->getBeans();
			if (!empty($opps) && is_array($opps) && count($opps) > 0) {
				foreach ($opps as $opp) {
					$this->opportunityBean = BeanFactory::getBean("Opportunities", $opp->id);
					break;
				}
			}
		}
		
		if ($bean->load_relationship('s1_site_p1_project_1')) {
			$sites = $bean->s1_site_p1_project_1->getBeans();
			if (!empty($sites) && is_array($sites) && count($sites) > 0) {
				foreach ($sites as $site) {
					$this->siteBean = BeanFactory::getBean("S1_Site", $site->id);
					break;
				}
			}
		}
	}
	
	private function getListValue($listName, $listItemId) {
		global $app_list_strings;
		return $app_list_strings[$listName][$listItemId];
	}

}
