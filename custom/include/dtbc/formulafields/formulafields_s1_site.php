<?php

require_once("custom/include/dtbc/helpers.php");

class FormulaFieldsS1_site {
	
	public function before_save($bean, $event, $arguments) {
		$bean->link_to_monitoring_c = $this->get_link_to_monitoring_c($bean->solar_field_id);
	}
	
	private function get_link_to_monitoring_c($solarFieldId) {
		if (!empty($solarFieldId))
			return "<a href='https://monitoring.solaredge.com/solaredge-web/p/site/" . $solarFieldId . "/#/dashboard' target='_blank'>Link to Monitoring</a>";
		
		return "";
	}
}
