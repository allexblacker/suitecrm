<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class FormulaFieldsLeads
{
	public function before_save($bean, $event, $arguments){
		
		$bean->lead_age_c = $this->get_aging_c($bean->converted, $bean->fetched_row['date_entered']);
		$bean->google_search_c = $this->get_google_search_c($bean->first_name, $bean->last_name);
		$bean->total_lead_score_c = $this->get_total_lead_score_c($bean->campaign_score_c, $bean->lead_score_c);
		$bean->lead_priority_c = $this->get_lead_priority_c($bean->total_lead_score_c);
		$bean->xing_search_c = $this->get_xing_search_c($bean->first_name, $bean->last_name);
		$bean->county_c = $this->get_us_county_c($bean->uzc1_usa_zip_codes_id_c);
	}
	
	private function get_aging_c($converted, $creation_date){
		
		if($converted){
			if(!empty($creation_date)){
				global $timedate;
				$now = strtotime(date('Y-m-d H:i:s'));
				$creation = date_create_from_format($timedate->dbDayFormat . ' ' . $timedate->dbTimeFormat, $creation_date);
				$diff = strtotime($now) - strtotime($creation);
				return round($diff / 86400, 0);
			}
			return 0;
		}
		return '';
	}
	
	private function get_google_search_c($first_name, $last_name){
		$link = "http://www.google.com/search?q=$first_name+$last_name";
		$text = "Search Google for $first_name";
		return DtbcHelpers::getHyperLinkForFormulaFields($link,$text);
	}
	
	private function get_total_lead_score_c($campaing_score, $lead_score){
		return $campaing_score + $lead_score;
	}
	
	private function get_lead_priority_c($total_lead_score){
		if($total_lead_score < 30)
			return 'C';
		elseif($total_lead_score < 50)
			return 'B';
		else
			return 'A';
	}
	
	private function get_xing_search_c($first_name, $last_name){
		$link = "https://www.xing.com/profile/($first_name)_($last_name)";
		$text = "Search Xing for $first_name";
		return DtbcHelpers::getHyperLinkForFormulaFields($link,$text);
	}
	
	private function get_us_county_c($uzc1_usa_zip_codes_id_c){
		$zipBean = BeanFactory::getBean('UZC1_USA_ZIP_Codes',$uzc1_usa_zip_codes_id_c);
		return $zipBean->county;
	}
}