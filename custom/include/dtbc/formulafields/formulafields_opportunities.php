<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/dtbc/opportunitiesFunctionFields.php');

class FormulaFieldsOpportunities
{
	private $date_format;
	private $time_format;
	private $account_bean;
	
	public function before_save($bean, $event, $arguments) {
		$this->set_beans($bean);
		$this->set_format();
		
		$probability = $bean->probability / 100;
		$bean->aging_c = $this->get_aging_c($bean->fetched_row['date_entered']);
		$bean->closed_q_c = $this->get_closed_q_c($bean->date_closed);
		$bean->closed_year_c = $this->get_closed_year_c($bean->date_closed);
		$bean->inverters_qty_neto_c = $this->get_inverters_qty_neto_c($probability, dtbc_getTotalInverters());
		$bean->others_qty_neto_c = $this->get_others_qty_neto_c($probability, dtbc_getTotalOthers());
		$bean->open_closed_c = $this->get_open_closed_c($bean->sales_stage);
		$bean->opportunity_expected_revenue_c = $this->get_opportunity_expected_revenue_c($bean->shipped_q_c, $bean->expected_revenue_c, $bean->bklog_c);
		$bean->options_qty_neto_c = $this->get_options_qty_neto_c($probability, dtbc_getTotalOptions());
		
		$bean->q1_weighted_price_c = $this->get_q_weighted_price(dtbc_getQ1TotalPrice(), $probability);
		$bean->q2_weighted_price_c = $this->get_q_weighted_price(dtbc_getQ2TotalPrice(), $probability);
		$bean->q3_weighted_price_c = $this->get_q_weighted_price(dtbc_getQ3TotalPrice(), $probability);
		$bean->q4_weighted_price_c = $this->get_q_weighted_price(dtbc_getQ4TotalPrice(), $probability);
		
		$bean->q1_shipped_bl_c = $this->get_q_shipped_bl_c($bean->q1_shipped_c, $bean->q1_backlog_c);
		$bean->q2_shipped_bl_c = $this->get_q_shipped_bl_c($bean->q2_shipped_c, $bean->q2_backlog_c);
		$bean->q3_shipped_bl_c = $this->get_q_shipped_bl_c($bean->q3_shipped_c, $bean->q3_backlog_c);
		$bean->q4_shipped_bl_c = $this->get_q_shipped_bl_c($bean->q4_shipped_c, $bean->q4_backlog_c);
		
		$bean->q1_likely_c = $this->get_q_likely_c($bean->q1_weighted_price_c, $bean->q1_shipped_bl_c);
		$bean->q2_likely_c = $this->get_q_likely_c($bean->q2_weighted_price_c, $bean->q2_shipped_bl_c);
		$bean->q3_likely_c = $this->get_q_likely_c($bean->q3_weighted_price_c, $bean->q3_shipped_bl_c);
		$bean->q4_likely_c = $this->get_q_likely_c($bean->q4_weighted_price_c, $bean->q4_shipped_bl_c);
		
		$bean->region_c = $this->get_region_c();
		if ($this->account_bean !== false)
			$bean->sendingwhs_c = $this->get_sendingwhs_c($this->account_bean->region_c);
		$bean->shortcommitmentlevel_c = $this->get_shortcommitmentlevel_c($commitment_level);
		$bean->stageindicator_c = $this->get_stageindicator_c($bean->sales_stage);
		$bean->targername_c = $this->get_targername_c($bean->assigned_user_id, $bean->date_closed, $bean->closed_q_c);
		if ($this->account_bean !== false)
			$bean->owner_target_c = $this->account_bean->target_m_c;
	}
	
	private function set_format(){
		global $timedate;
		$this->date_format = $timedate->dbDayFormat;
		$this->time_format = $timedate->dbTimeFormat;
	}
	
	private function set_beans($bean){
		$this->account_bean = BeanFactory::getBean('Accounts', $bean->account_id);
	}
	
	private function get_aging_c($creation_date){
		
		if(!empty($creation_date)){
			global $sugar_config;
			$settingField = 'solaredge';
			$field = 'stage_name';
			$attribute = 'type';
			
			if (!isset($sugar_config[$settingField]) || !isset($sugar_config[$settingField][$field]) || !isset($sugar_config[$settingField][$field][$attribute]))
				return '';
			
			if ($sugar_config[$settingField][$field][$attribute] == 'Closed/Won' || $sugar_config[$settingField][$field][$attribute] == 'Closed/Lost')
				return '';

			$today = date_create(date($this->date_format));
			$creation = date_create_from_format($this->date_format . ' ' . $this->time_format, $creation_date);
			return date_diff($today, $creation)->format("%a");
		}
		return 0;
	}
	
	private function get_closed_q_c($date_closed){
		if(!empty($date_closed)){
			$month = DtbcHelpers::getDatePart($this->date_format,$date_closed,'month');
			return ceil($month / 3);
		}
		return '';
	}
	
	private function get_closed_year_c($date_closed){
		return DtbcHelpers::getDatePart($this->date_format,$date_closed,'year');
	}
	
	private function get_inverters_qty_neto_c($probability, $total_inverters){
		if(intval($probability) != 1)
			return $total_inverters * $probability;
		return 0;
	}
	
	private function get_others_qty_neto_c($probability, $total_others){
		if(intval($probability) != 1)
			return $total_others * $probability;
		return 0;
	}
	
	private function get_open_closed_c($stage_value){
		global $app_list_strings;
		$stage_name = $app_list_strings['sales_stage_list'][$stage_value];
		if($stage_name == 'Booked' || $stage_name == 'Completed')
			return 'Closed';
		return 'Open';
	}
	
	private function get_opportunity_expected_revenue_c($shipped_q, $expected_revenue, $bklog){
		if(intval($shipped_q) == 0 && intval($bklog) == 0)
			return $expected_revenue;
		return $shipped_q + $bklog;
	}
	
	private function get_options_qty_neto_c($probability, $total_options){
		if($probability != 1)
			return $total_options * $probability;
		return 0;
	}
	
	private function get_power_optimizer_qty_neto_c($probability, $total_power_optimizer){
		if($probability != 1)
			return $total_power_optimizer * $probability;
		return 0;
	}
	
	private function get_q_weighted_price($q_total_price, $probability){
		return $q_total_price * $probability;
	}
	
	private function get_q_shipped_bl_c($q_backlog, $q_shipped){
		return $q_backlog + $q_shipped;
	}
	
	private function get_q_likely_c($q_weighted_price, $q_shipped_bl){
		return $q_weighted_price + $q_shipped_bl;
	}
	
	private function get_region_c(){
		global $app_list_strings;
		
		if ($this->account_bean === false)
			return '';
		
		$regionValue = $this->account_bean->region_c;
		return $app_list_strings['region_list'][$regionValue];
	}
	
	private function get_sendingwhs_c($region_value){
		global $app_list_strings;
		$region = $app_list_strings['region_list'][$region_value];
		
		return $this->get_sendingwhs_value($region);
	}
	
	public function get_sendingwhs_value($region){
		switch($region){
			case 'APAC':
				return 'Jbl';
			case 'North America':
				return '';
			case 'China':
				return 'Jbl';
			case 'Australia':
				return 'Jbl';
			case 'Japan':
				return 'Jbl';
			default:
				return 'NTRL';
		}
	}
	
	private function get_shortcommitmentlevel_c($commitment_level_key){
		
		switch($commitment_level_key){
			case 'gone':
				return "0%";
			case 'low':
				return "<50%";
			case 'medium':
				return "50-75%";
			case 'high':
				return "75-90%";
			case 'commited':
				return "90-99%";
			case 'received':
				return "100%";
			default:
				return "";
		}
	}
	
	private function get_stageindicator_c($stage_value){
		
		if($stage_value == 'PO_received' || $stage_value == 'Booked')
			return DtbcHelpers::getImageTagForFormulaFields('vsign.gif', '', true, false, 25, 15);
		return '';
	}
	
	private function get_targername_c($user_id, $date_closed, $closed_q){
		$year = DtbcHelpers::getDatePart($this->date_format, $date_closed, 'year');
		return $user_id . ',' . $year . ',' . $closed_q;
	}
	
	private function get_weighted_for_current_q_c($stage_value,$total_price_c, $probability){
		global $sugar_config;
		if($sugar_config['solaredge']['stage_name'][$stage_value]['type'] == 'Closed/Won')
			return 0;
		return $total_price_c * $probability;
		
	}
	
}