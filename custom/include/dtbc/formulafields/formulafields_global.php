<?php

require_once("custom/include/dtbc/helpers.php");

function dtbc_site_InstructionFlag($bean, $relBean) {
	if (!empty($bean->service_instruction) && !empty($bean->site_name)) {
		if ($bean->service_instruction == 1 && strlen($bean->site_name) > 0)
			return "<img src='custom/include/dtbc/images/error_image.gif' alt='' />";
	}
	return "";
}

function dtbc_site_SolarFieldIdLink($bean, $relBean) {
	return '<a href="https://monitoring.solaredge.com/solaredge-web/p/site/' . $bean->solar_field_id . '/#/dashboard" target="_blank">Link to Monitoring</a>';
}

function dtbc_opp_EstimatedKw($bean, $relBean) {
	return $bean->total_kw2_c > 0 ? $bean->total_kw2_c : $bean->estimated_kw_c;
}

function dtbc_IsServiceKitHolder($bean, $relBean) {
	global $sugar_config;

	if (!empty($bean->name) && in_array($bean->name, $sugar_config['solaredge']['service_kit_holder_companies']))
		return "<img src='custom/include/dtbc/images/Toolbox.jpg' alt='' />";

	return "";
}

function addToReasonForNotEligible(&$fieldValue, $value){
		 $fieldValue .= $value . " ";
}

function dtbc_ReasonForEligible($bean, $relBean) {
	$wasTrue = $relBean->if_eligible_was_true_c;
	$status = $relBean->case_status_c;
	$reasonForNotEligibleCertification = $relBean->reason_for_not_eligible_cert_c;
	$iboltStatus = $relBean->ibolt_status_c;
	$monitored = $relBean->monitored_c;
	$installation = $relBean->installation_rules_c;
	$rmaNumber = $relBean->rma_pr_number_c;
	$caseId = $relBean->id;
	$dateEntered = $relBean->fetched_row['date_entered'];
	$isRma = $relBean->case_record_type_c == "rma";
	
	$fieldValue = "";
	if ($wasTrue == 1 && $status != 10)
		return $fieldValue;
	
	if ($status != 15)
		addToReasonForNotEligible($fieldValue, "RMA not completed.");
	
	if(!(empty($reasonForNotEligibleCertification)))
		addToReasonForNotEligible($fieldValue, $reasonForNotEligibleCertification);
	
	if ($iboltStatus != "Success")
		addToReasonForNotEligible($fieldValue, "Invalid serial.");
	
	if (!empty($bean->service_level_c)) {
		if ($bean->service_level_c == "Finance_Hold")
			addToReasonForNotEligible($fieldValue, "Finance Hold.");
		if ($bean->service_level_c == "Service_Hold")
			addToReasonForNotEligible($fieldValue, "Service Hold.");
	}
	
	if ($monitored != 1)
		addToReasonForNotEligible($fieldValue, "Not monitored.");
	
	if ($installation != 1)
		addToReasonForNotEligible($fieldValue, "Installation rules.");
	
	if (strlen($rmaNumber) > 0)
		addToReasonForNotEligible($fieldValue, "PR opened.");
	
	$changeDate = $isRma ? dtbc_getRecordTypeChangedToRMA($caseId, $dateEntered) : "";
	$isOldRma = dtbc_getIsOldRma($changeDate);
	
	if ($isOldRma)
		addToReasonForNotEligible($fieldValue, "Old RMA.");
	
	return $fieldValue;
}

function dtbc_getIsOldRma($changeDate) {
	if (strlen($changeDate) == 0)
		return false;
	
	try {
		$temp = explode(" ", $changeDate);
		$rmaDate = new DateTime($temp[0]);
		$today = new DateTime();
		$oneYearBefore = $today->modify('-1 year');
		
		return $rmaDate->getTimestamp() < $oneYearBefore->getTimestamp();
		
	} catch (Exception $e) {
		return false;
	}
	
}

function dtbc_getRecordTypeChangedToRMA($caseId, $dateEntered) {
	// Return the date when changed the case status to RMA
	global $db;
	$sql = "SELECT date_created 
			FROM cases_audit
			WHERE field_name = 'case_record_type_c' AND
			after_value_string = 'rma' AND
			parent_id = " . $db->quoted($caseId) . "
			ORDER BY date_created DESC";
	
	$res = $db->fetchOne($sql);
	
	if (!empty($res) && !empty($res['date_created']))
		return $res['date_created'];
	
	return $dateEntered;
}

function dtbc_AccountContactComment($bean, $relBean) {
	$contactBean = null;
	
	if (!empty($relBean->contact_created_by_id) && strlen($relBean->contact_created_by_id) > 0) {
		$contactBean = BeanFactory::getBean("Contacts", $relBean->contact_created_by_id);
	}
		
	if (($bean != null && !empty($bean->support_comments_c) && strlen($bean->support_comments_c) > 0) &&
		($contactBean != null && !empty($contactBean->contact_support_comments_c) && strlen($contactBean->contact_support_comments_c) > 0))
		return $bean->support_comments_c . " " . $contactBean->contact_support_comments_c;
	
	if (($bean != null && !empty($bean->support_comments_c) && strlen($bean->support_comments_c) > 0) &&
		($contactBean == null || ($contactBean != null && (empty($contactBean->contact_support_comments_c) || strlen($contactBean->contact_support_comments_c) == 0))))
		return $bean->support_comments_c;
		
	if (($bean == null || ($bean != null && (empty($bean->support_comments_c) || strlen($bean->support_comments_c) == 0))) &&
		($contactBean != null && !empty($contactBean->contact_support_comments_c) && strlen($contactBean->contact_support_comments_c) > 0))
		return $contactBean->contact_support_comments_c;
	
	return "";
}

function dtbc_AccountContactCommentFromContact($bean, $relBean) {
	$accountBean = null;
	
	if (!empty($relBean->account_id) && strlen($relBean->account_id) > 0) {
		$accountBean = BeanFactory::getBean("Accounts", $relBean->account_id);
	}
		
	if (($accountBean != null && !empty($accountBean->support_comments_c) && strlen($accountBean->support_comments_c) > 0) &&
		($bean != null && !empty($bean->contact_support_comments_c) && strlen($bean->contact_support_comments_c) > 0))
		return $accountBean->support_comments_c . " " . $bean->contact_support_comments_c;
	
	if (($accountBean != null && !empty($accountBean->support_comments_c) && strlen($accountBean->support_comments_c) > 0) &&
		($bean == null || ($bean != null && (empty($bean->contact_support_comments_c) || strlen($bean->contact_support_comments_c) == 0))))
		return $accountBean->support_comments_c;
		
	if (($accountBean == null || ($accountBean != null && (empty($accountBean->support_comments_c) || strlen($accountBean->support_comments_c) == 0))) &&
		($bean != null && !empty($bean->contact_support_comments_c) && strlen($bean->contact_support_comments_c) > 0))
		return $bean->contact_support_comments_c;
	
	return "";
}

function dtbc_getUSAZipCodeCounty($bean){
	global $db;
	$sql = "UPDATE accounts_cstm
			SET accounts_cstm.us_county_c = " . $db->quoted($bean->county) . 
			" WHERE accounts_cstm.uzc1_usa_zip_codes_id_c = " . $db->quoted($bean->id);
	$db->query($sql);
}

function dtbc_getContactDepartment($bean){
	global $db;
	
	if(empty($bean->department))
		$sql = "UPDATE contacts SET contacts.department = " .  $db->quoted($bean->division_c) . " WHERE contacts.created_by = " . $db->quoted($bean->id);
	else
		$sql = "UPDATE contacts SET contacts.department = " .  $db->quoted($bean->department) . " WHERE contacts.created_by = " . $db->quoted($bean->id);
		
	$db->query($sql);
}

function dtbc_setContactsAccountService($bean){
	
	global $db;
	$link = "";
	if(empty($bean->service_level_c))
		$link = DtbcHelpers::getImageTagForFormulaFields('Missing_information.jpg','Missing Contact Type/Account Service Level',true);
	
	$sql = "UPDATE contacts_cstm 
			INNER JOIN contacts
			ON contacts.id = contacts_cstm.id_c
			INNER JOIN accounts_contacts
			ON accounts_contacts.contact_id = contacts_cstm.id_c
			SET contacts_cstm.cont_type_acc_service_is_mi_c = " . $db->quoted($link) .
			" WHERE accounts_contacts.account_id = " . $db->quoted($bean->id);
	$db->query($sql);
}

function dtbc_setContactsLoyaltyPlanEmail($bean){
	
	global $db;
	$flag = "";
	if($bean->allow_loyalty_plan_email_c)
		$flag = "True";
	else
		$flag = "False";
	
	$sql = "UPDATE contacts_cstm
			INNER JOIN accounts_contacts
			ON accounts_contacts.contact_id = contacts_cstm.id_c
			SET contacts_cstm.allow_loyalty_plan_email_c = " . $db->quoted($flag) .
			" WHERE accounts_contacts.account_id = " . $db->quoted($bean->id);
	$db->query($sql);
}

function dtbc_setSendingWhs($bean){
	global $db;
	require_once('custom/include/dtbc/formulafields/formulafields_opportunities.php');
	$formula = new FormulaFieldsOpportunities();
	
	$sendingwhs = $formula->get_sendingwhs_value($bean->region_c);
	$sql = "UPDATE opportunities_cstm
			INNER JOIN accounts_opportunities
			ON opportunities_cstm.id_c = accounts_opportunities.opportunity_id
			SET opportunities_cstm.sendingwhs_c = " . $db->quoted($sendingwhs) . " 
			WHERE accounts_opportunities.account_id = " . $db->quoted($bean->id);
	$db->query($sql);
}

function dtbc_setRegion($bean){
	global $db, $app_list_strings;
	$regionValue = $bean->region_c;
	$region = $app_list_strings['region_list'][$regionValue];
	
	$sql = "UPDATE opportunities_cstm
			INNER JOIN accounts_opportunities
			ON opportunities_cstm.id_c = accounts_opportunities.opportunity_id
			SET opportunities_cstm.region_c = " . $db->quoted($region) . " 
			WHERE accounts_opportunities.account_id = " . $db->quoted($bean->id);
	
	$db->query($sql);
}

function dtbc_getUSAZipCodeCountyForLeads($bean){
	global $db;
	$sql = "UPDATE leads_cstm
			SET leads_cstm.county_c = " . $db->quoted($bean->county) . 
			" WHERE leads_cstm.uzc1_usa_zip_codes_id_c = " . $db->quoted($bean->id);
	$db->query($sql);
}