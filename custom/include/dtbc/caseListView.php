<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class ProcessRecordsCases {
	public function process_record($bean, $event, $arguments) {
		global $db;

		$sql = "SELECT change_indicator_c FROM cases_cstm WHERE id_c = " . $db->quoted($bean->id);

		$res = $db->fetchOne($sql);

		if (!empty($res) && is_array($res) && count($res) > 0) {
			$bean->listview_envelope = html_entity_decode($res['change_indicator_c'], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		}
		
		$bean->change_indicator_c = html_entity_decode($bean->change_indicator_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		$bean->listview_security_group = $this->getSecurityGroupName($bean->id);

	}
	
	private function getSecurityGroupName($recordId) {
		global $db;
		
		$sql = "SELECT securitygroups.name AS name
				FROM securitygroups_records 
				JOIN securitygroups ON securitygroups_records.securitygroup_id = securitygroups.id AND securitygroups.deleted = 0
				WHERE module = " . $db->quoted("Cases") . " AND
				record_id = " . $db->quoted($recordId) . " AND
				securitygroups_records.deleted = 0";
				
		$res = $db->fetchOne($sql);
		if (!empty($res) && is_array($res) && count($res) > 0)
			return $res['name'];
		
		return "";
	}
}
