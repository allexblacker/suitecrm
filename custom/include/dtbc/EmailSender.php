<?php

require_once("modules/EmailTemplates/EmailTemplate.php");
require_once('include/SugarPHPMailer.php');
require_once('custom/include/dtbc/helpers.php');

class EmailSender {
	
	function sendEmailWoCustomizedValues($to_email, $moduleName, $recordId, $emailTemplateId) {
		global $sugar_config;
		
		$bean = BeanFactory::getBean($moduleName, $recordId);

		$emailtemplate = new EmailTemplate();
		$emailtemplate = $emailtemplate->retrieve($emailTemplateId);

		$emailtemplate->parsed_entities = null;
		$temp = array();
		$template_data = $emailtemplate->parse_email_template(
			array(
				"subject" => $emailtemplate->subject,
				"body_html" => $emailtemplate->body_html,
				"body" => $emailtemplate->body
			), 
			$moduleName, 
			$bean, 
			$temp
		);
		$email_body = $template_data["body_html"];
		$email_subject = $template_data["subject"];
		
		$emailObj = new Email(); 
		$defaults = $emailObj->getSystemDefaultEmail(); 

		$mail = new SugarPHPMailer(); 
		$mail->setMailerForSystem(); 
		$mail->From = $defaults['email'];
		$mail->FromName = $defaults['name']; 
		$mail->Subject = $email_subject;
		$mail->ContentType = "text/html";
		$mail->Body = $email_body;
		$mail->prepForOutbound(); 
		$mail->AddAddress($to_email); 
		@$mail->Send();
	}
	
	function sendEmailWithCurrentUser($emailTo = array(), $emailCc = array(), $moduleName, $recordId, $emailTemplateId){
		
		if(empty($emailTo) || empty($moduleName) || empty($recordId) || empty($emailTemplateId))
			return;
		
		global $sugar_config;
		$settings = $sugar_config['solaredge']['SMTPList']['current_user'];
		
		if(empty($settings))
			return;
		
		$mail = new SugarPHPMailer(); 
		$mail->CharSet= $settings['email_charset'];
        $mail->SMTPSecure = $settings['email_smtp_secure'];
        $mail->Host = $settings['email_host'];
        $mail->Port = $settings['email_port'];
        $mail->Username = $settings['email_username'];
        $mail->Password = $settings['email_password'];
        $mail->SMTPAuth =  $settings['email_smtp_auth'];
        $mail->IsHTML($settings['email_is_html']);
        
		global $current_user;
        $mail->From = $current_user->email1;
        $mail->FromName = $current_user->name;
		
		$bean = BeanFactory::getBean($moduleName, $recordId);
		$emailtemplate = new EmailTemplate();
		$emailtemplate = $emailtemplate->retrieve($emailTemplateId);

		$emailtemplate->parsed_entities = null;
		$temp = array();
		$template_data = $emailtemplate->parse_email_template(
			array(
				"subject" => $emailtemplate->subject,
				"body_html" => $emailtemplate->body_html,
				"body" => $emailtemplate->body
			), 
			$moduleName, 
			$bean, 
			$temp
		);
		$email_body = $template_data["body_html"];
		$email_subject = $template_data["subject"];
		
		$emailObj = new Email(); 
		$defaults = $emailObj->getSystemDefaultEmail(); 

		$mail->Subject = $email_subject;
		$mail->ContentType = "text/html";
		$mail->Body = $email_body;
		$mail->prepForOutbound(); 
		
		foreach($emailTo as $to){
            $mail->AddAddress($to);
        }

        foreach($emailCc as $email){
            $mail->AddCC($email);
        }

		@$mail->Send();
	}
	
	function sendNotificationWhenTakeARecord($to_email, $moduleName, $recordId, $username) {
		global $sugar_config;
		
		$bean = BeanFactory::getBean($moduleName, $recordId);
		
		$emailtemplate = new EmailTemplate();
		$emailtemplate = $emailtemplate->retrieve($sugar_config['solaredge']['taken_record_notify_email_id']);

		$emailtemplate->parsed_entities = null;
		$temp = array();
		$template_data = $emailtemplate->parse_email_template(
			array(
				"subject" => $emailtemplate->subject,
				"body_html" => $this->insertCustomDataTakenRecord($emailtemplate->body_html, $moduleName, $recordId, $username),
				"body" => $emailtemplate->body
			), 
			$moduleName, 
			$bean, 
			$temp
		);
		$email_body = $template_data["body_html"];
		$email_subject = $template_data["subject"];
		
		$emailObj = new Email(); 
		$defaults = $emailObj->getSystemDefaultEmail(); 

		$mail = new SugarPHPMailer(); 
		$mail->setMailerForSystem(); 
		$mail->From = $defaults['email'];
		$mail->FromName = $defaults['name']; 
		$mail->Subject = $email_subject;
		$mail->ContentType = "text/html";
		$mail->Body = $email_body;
		$mail->prepForOutbound(); 
		$mail->AddAddress($to_email); 
		@$mail->Send();
	}
	
	private function insertCustomDataTakenRecord($template, $moduleName, $recordId, $username) {
		global $sugar_config;
		$vars = array(";;username;;", ";;recorddata;;");
		$vals = array($username, $sugar_config['site_url'] . "/index.php?module=" . $moduleName . "&action=DetailView&record=" . $recordId);
		return str_replace($vars, $vals, $template);
	}

	public function sendNotificationOnNewRecordInGroup($to_email, $moduleName, $recordId, $recordData) {
		global $sugar_config;
		
		$bean = BeanFactory::getBean($moduleName, $recordId);
		
		$emailtemplate = new EmailTemplate();
		$emailtemplate = $emailtemplate->retrieve($sugar_config['solaredge']['new_record_notify_email_id']);

		$emailtemplate->parsed_entities = null;
		$temp = array();
		$template_data = $emailtemplate->parse_email_template(
			array(
				"subject" => $emailtemplate->subject,
				"body_html" => $this->insertCustomDataNewRecord($emailtemplate->body_html, $moduleName, $recordData),
				"body" => $emailtemplate->body
			), 
			$moduleName, 
			$bean, 
			$temp
		);
		$email_body = $template_data["body_html"];
		$email_subject = $template_data["subject"];
		
		$emailObj = new Email(); 
		$defaults = $emailObj->getSystemDefaultEmail(); 

		$mail = new SugarPHPMailer(); 
		$mail->setMailerForSystem(); 
		$mail->From = $defaults['email'];
		$mail->FromName = $defaults['name']; 
		$mail->Subject = $email_subject; 
		$mail->ContentType = "text/html";
		$mail->Body = $email_body;
		$mail->prepForOutbound(); 
		$mail->AddAddress($to_email); 
		@$mail->Send();
		
	}
	
	public function getEmailAddresFromCase($caseBean) {
		if (!empty($caseBean->contact_created_by_id) && strlen($caseBean->contact_created_by_id) > 0) {
			global $db;
			$res = $db->fetchOne("SELECT email_addresses.email_address 
							FROM email_addresses 
							JOIN email_addr_bean_rel ON email_addresses.id = email_addr_bean_rel.email_address_id AND email_addr_bean_rel.deleted = 0
							WHERE bean_module = 'Contacts' AND
							bean_id = " . $db->quoted($caseBean->contact_created_by_id) . " AND
							email_addresses.deleted = 0");
							
			if (!empty($res) && isset($res['email_address']) && strlen($res['email_address']) > 0) {
				return $res['email_address'];
			}
		}
				
		return "";
	}
	
	private function insertCustomDataNewRecord($template, $moduleName, $recordData) {
		$vars = array(";;modulename;;", ";;recorddata;;");
		$vals = array($moduleName, $recordData);
		return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en_us"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>' . str_replace($vars, $vals, $template) . '</body></html>';
		
		return str_replace($vars, $vals, $template);
	}
}