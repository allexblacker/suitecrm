<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

function listSecurityGroups(){
	global $db;
	$sql = "SELECT DISTINCT `name` FROM securitygroups WHERE deleted = 0 ORDER BY `name`";
	$queryResult = $db->query($sql);

	$securityGroups = "<option value=''></option>";
	
	$defaultValue = isset($_REQUEST['searchform_security_group_advanced']) ? $_REQUEST['searchform_security_group_advanced'] : "";
	
	while ($record = $db->fetchByAssoc($queryResult)) {
		$secGroupName = $record['name'];
		$selected = $secGroupName == $defaultValue ? "selected" : "";
		$securityGroups .="<option value='$secGroupName' $selected>$secGroupName</option>";
	}

	return $securityGroups;
}
