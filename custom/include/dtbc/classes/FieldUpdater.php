<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');
require_once('custom/include/dtbc/helpers.php');
class FieldUpdater{
	
	public function getCountIfPortia($id){

		global $db;
		$sql = "SELECT 
					COUNT(*) AS total
				FROM
					cases_sn1_serial_number_1_c
						INNER JOIN
					sn1_serial_number ON sn1_serial_number.id = cases_sn1_serial_number_1_c.cases_sn1_serial_number_1sn1_serial_number_idb
						AND sn1_serial_number.deleted = 0
				WHERE
					cases_sn1_serial_number_1_c.deleted = 0
						AND sn1_serial_number.fault_description = 'RS485 port failure'
						AND (sn1_serial_number.family = 'Jupiter Plus'
						OR sn1_serial_number.family = 'Jupiter II')
						AND sn1_serial_number.rma_product = 'Portia'
						AND cases_sn1_serial_number_1cases_ida = " . $db->quoted($id);

		return $this->getResult($db, $sql, 'total');
	}
	
	public function getRMALinesInCase($id){
	
		global $db;
		$sql = "SELECT 
					COUNT(*) AS total
				FROM
					cases_sn1_serial_number_1_c
						INNER JOIN
					sn1_serial_number ON sn1_serial_number.id = cases_sn1_serial_number_1_c.cases_sn1_serial_number_1sn1_serial_number_idb
						AND sn1_serial_number.deleted = 0
				WHERE
					cases_sn1_serial_number_1_c.deleted = 0
						AND sn1_serial_number.rma = 1
						AND sn1_serial_number.product_old != 'MCI-CN-00120'
						AND sn1_serial_number.product_old != 'MCI-CN-00119'
						AND cases_sn1_serial_number_1cases_ida = " . $db->quoted($id);
		
		return $this->getResult($db, $sql, 'total');
	}
	
	private function getResult($db, $sql, $fieldName){
		$queryResult = $db->query($sql);
		$result = $db->fetchByAssoc($queryResult);
		if(!empty($result) && is_array($result) && count($result) > 0)
			return $result[$fieldName];
		else
			return 0;
	}
}

