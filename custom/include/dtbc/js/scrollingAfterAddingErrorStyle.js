var topPos = 0;
var leftPos = 0;

function add_error_style_custom(formName, fieldName, errorMessage){
	add_error_style(formName, fieldName, errorMessage);
	setPosition(fieldName);
}

function posInit(){
	topPos = 0;
	leftPos = 0;
}

function setPosition(elementName){
	var element = $('#' + elementName);
	var elementTop = element.offset().top;
	if(elementTop < topPos || topPos == 0){
		topPos = element.offset().top;
		leftPos = element.offset().left;
	}
}

function scrollToError(){
	var distance = 75;
	window.scrollTo(leftPos - distance, topPos - distance);
	posInit();
}