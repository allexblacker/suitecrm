$(function() {	
	YAHOO.util.Event.onContentReady('subpanel_list', function() {
		loadColoring();	
	});		
});

$( document ).ajaxStop(function() {
	loadColoring();
});

function loadColoring() {
	var file = document.createElement( 'link' );
	var path = "custom/include/dtbc/css/subpanelHeaderColoring.css";
		
	file.rel   = 'stylesheet';
	file.type   = 'text/css';
	file.href   = path;
	
	var head = document.getElementsByTagName( 'head' )[0];
	head.append( file );	
	colorSubpanelHeaders();	
}

function colorSubpanelHeaders() {		
	$('#subpanel_list .sub-panel').has('.oddListRowS1').find('a.collapsed').addClass('hascontent');
	$('#subpanel_list .sub-panel').not(':has(.footable-empty)').find('a[role="button"]').not('.collapsed').removeClass('hascontent');	
}
