/*globals $,YAHOO,open_popu,SUGAR,defaultPartnerRelateField,defaultAccountRelateField*/
$(function () {
    'use strict';

    var relateFields = {
        id: $('#qs_hidden_contact_id').val(),
        name: $('#qs_hidden_contact_name').val(),
        target: $('#qs_hidden_account_id').val(),
    };

    YAHOO.util.Event.onContentReady(relateFields.name, customContactSearch(relateFields));

    function getAccountId(target) {
        return $('#' + target).val();
    }

    function customContactSearch(relateField) {
        return function () {

            function updateSqs() {
                var viewName = "EditView_" + relateField.name;
                if (typeof(sqs_objects) != 'undefined' && typeof(sqs_objects[viewName]) != 'undefined') {
                    var params = getParams();
                    sqs_objects[viewName].metadata = params.metadata;
                    sqs_objects[viewName].query = params.query;
                }
            }

            var callback = function () {
                $('#' + relateField.name).val('');
                $('#' + relateField.id).val('');
                updateSqs();
            };
            YAHOO.util.Event.addListener(relateField.target, 'change', callback);

            var getParams = function () {
                var accountId = getAccountId(relateField.target);
                var queryParam = {};
                var ret = {
                    query: queryParam,
                    metadata: ''
                };
                if (accountId) {
                    queryParam = {
                        related: 'Accounts',
                        relatedId: accountId,
                    };
                    ret = {
                        query: queryParam,
                        metadata: 'relatedContact',
                    };
                }
                return ret;
            };

            var popupButton = $('#btn_' + relateField.name);
            popupButton.removeAttr('onclick');
            popupButton.on('click', function () {
                var params = getParams();
                var query = $.param(params.query);
                if (query) {
                    query = '&' + query;
                }

                var field_to_name_array = Object.assign({}, relateField);
                delete field_to_name_array.target;
                open_popup(
                    "Contacts",
                    600,
                    400,
                    query,
                    true,
                    false,
                    {
                        "call_back_function": "set_return",
                        "form_name": action_sugar_grp1,
                        "field_to_name_array": field_to_name_array
                    },
                    "single",
                    true,
                    params.metadata
                );
            });
            updateSqs();
        }
    }
});