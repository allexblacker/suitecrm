$(document).ready(function() {
	$('body').find('a').each(function() {
		var hrefValue = $(this).attr('href');
		
		if (hrefValue != undefined && hrefValue.toLowerCase().indexOf("mylinks.") >= 0) {
			$(this).attr('href', hrefValue.replace("mylinks.", "https://"));
		} else if (hrefValue != undefined && hrefValue.toLowerCase().indexOf("mylink.") >= 0) {
			$(this).attr('href', hrefValue.replace("mylink.", "http://"));
		}

	});
	
	
	$('div.detail-view').find('div').each(function() {
		var divValue = $(this).html();
		
		if (divValue != undefined && divValue.toLowerCase().indexOf("mylinks.") >= 0) {
			$(this).html(divValue.replace("mylinks.", "https://"));
		} else if (divValue != undefined && divValue.toLowerCase().indexOf("mylink.") >= 0) {
			$(this).html(divValue.replace("mylink.", "http://"));
		}
	});
	
});