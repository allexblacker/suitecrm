<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class ListViewFieldsForWfu {
	public function process_record(&$bean, $event, $arguments) {
		$tmpBean = BeanFactory::getBean("dtbc_Workflow_Field_Updates", $bean->id);

		$bean->wfinl_translated_c = translate($tmpBean->wf_item_name_label_c, $tmpBean->wf_record_type_c);
		$bean->wfivl_translated_c = $this->getItemValue($tmpBean);
		$bean->aow_workflow_name_link_c = $this->getWfLink($tmpBean->aow_workflow_id_c);
		$bean->flow_module_name_c = translate("LBL_MODULE_NAME", $tmpBean->aow_workflow_module_c);		
	}
	
	private function getItemValue(&$tmpBean) {
		if (!empty($tmpBean->wf_item_value_label_c))
			return translate($tmpBean->wf_item_value_label_c, $tmpBean->aow_workflow_module_c);
		
		global $db;
		$bean = BeanFactory::getBean($tmpBean->wf_record_type_c);
	
		$fieldData = $bean->field_name_map[$tmpBean->wf_item_name_c];

		if (isset($fieldData['module'])) {
			$dbField = "name";
			if ($fieldData['table'] == "users")
				$dbField = "CONCAT(first_name, ' ',last_name) AS name";
			$sql = "SELECT " . $dbField . " FROM " . $fieldData['table'] . " WHERE id = " . $db->quoted($tmpBean->wf_item_value_c);
			$res = $db->fetchOne($sql);
			if (!empty($res) && isset($res['name']))
				return $res['name'];
		}
		
		return $tmpBean->wf_item_value_c;
	}
	
	private function getWfLink($wfId) {
		global $db;
		
		$hrefTag = "?module=AOW_Workflow&action=DetailView&record=" . $wfId;
		
		$sql = "SELECT name FROM aow_workflow WHERE id = " . $db->quoted($wfId);
		$res = $db->fetchOne($sql);
		
		$wfName = !empty($res) && is_array($res) && isset($res['name']) ? $res['name'] : "";
		
		return '<a href="' . $hrefTag . '" target="_blank">' . $wfName . '</a>';
	}
}
