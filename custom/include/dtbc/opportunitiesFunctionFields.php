<?php
require_once("custom/include/dtbc/helpers.php");

function getSqlForLineItem($expressionAndFieldName,$criteria = ""){
	global $db;
	$record = !empty($_REQUEST['record']) ? $_REQUEST['record'] : "";
	$sql = "SELECT $expressionAndFieldName FROM dtbc_line_item_cstm
			INNER JOIN opportunities_dtbc_line_item_1_c
			ON opportunities_dtbc_line_item_1_c.opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item_cstm.id_c AND opportunities_dtbc_line_item_1_c.deleted = 0
			WHERE opportunities_dtbc_line_item_1_c.opportunities_dtbc_line_item_1opportunities_ida = " . $db->quoted($record) . " $criteria";
	return $sql;
}

function getStandardCriteriaForProductFamily(){
	return 'AND dtbc_line_item_cstm.product_family_from_product_c';
}


function runQueryAndReturnField($sql,$field){
	global $db;
	$result = $db->query($sql);
	$record = $db->fetchByAssoc($result);
	return $record[$field];
}

function dtbc_getTotalInverters(){
	$field = 'sum_of_quantity_c';
	$criteria = getStandardCriteriaForProductFamily() . " = 'Inverter'";
	$sql = getSqlForLineItem("SUM(quantity_c) AS $field", $criteria);
	return runQueryAndReturnField($sql,$field);
}

function dtbc_getTotalKw2(){
	$field = 'sum_of_kwforop_c';
	$sql = getSqlForLineItem("SUM(kwforop_c) AS $field");

	return runQueryAndReturnField($sql,$field);
}

function dtbc_getTotalOptions(){
	$field = 'sum_of_quantity_c';
	$criteria = getStandardCriteriaForProductFamily() . " = 'Options'";
	$sql = getSqlForLineItem("SUM(quantity_c) AS $field", $criteria);
	return runQueryAndReturnField($sql,$field);
}

function dtbc_getTotalOthers(){
	$field = 'sum_of_quantity_c';
	$criteria = getStandardCriteriaForProductFamily() . " = 'Others'";
	$sql = getSqlForLineItem("SUM(quantity_c) AS $field", $criteria);
	
	return runQueryAndReturnField($sql,$field);
}

function getTotalPriceSQL($quarter){
	$field = 'sum_of_total_price_c';
	$criteria = " AND dtbc_line_item_cstm.forecast_q_c = $quarter";
	$sql = getSqlForLineItem("SUM(total_price_c) AS $field", $criteria);
	return runQueryAndReturnField($sql,$field);
}

function dtbc_getQ1TotalPrice(){
	return getTotalPriceSQL(1);
}

function dtbc_getQ2TotalPrice(){
	return getTotalPriceSQL(2);
}

function dtbc_getQ3TotalPrice(){
	return getTotalPriceSQL(3);
}

function dtbc_getQ4TotalPrice(){
	return getTotalPriceSQL(4);
}


function dtbc_getTotalProducts(){
	global $db;
	$field = 'count_of_line_item';
	$sql = getSqlForLineItem("COUNT(*) AS $field");
	
	return runQueryAndReturnField($sql,$field);
}