<?php

require_once("custom/include/dtbc/helpers.php");

class EmailSerialNumberTableGenerator {
	
	private $fieldType;
	private $db;
	private $recordId;
	private $year;
	
	public function __construct() {
		global $db;
		$this->db = $db;
		$this->recordId = !empty($_REQUEST['record']) ? $_REQUEST['record'] : "";
	}
	
	public function run() {
		$retval = "<table border='1' style='width:450px;'>";
		$retval .= "<tbody>";
		// Header
		$retval .= "<tr>";
		$retval .= "<td colspan='2' align='center'>Serial Numbers</td>";
		$retval .= "</tr>";
		$retval .= "<tr>";
		$retval .= "<td>Serial Number</td>";
		$retval .= "<td>Product</td>";
		$retval .= "</tr>";
		// Products
		$retval .= $this->getProductLines();
		$retval .= "</tbody>";
		$retval .= "</table>";
		return $retval;
	}
	
	private function getProductLines() {
		global $app_list_strings;
		$sql = "SELECT serial_number_new, rma_product 
				FROM sn1_serial_number 
				JOIN cases_sn1_serial_number_1_c ON cases_sn1_serial_number_1sn1_serial_number_idb = sn1_serial_number.id AND cases_sn1_serial_number_1_c.deleted = 0 
				WHERE sn1_serial_number.deleted = 0 AND 
				(product_new IS NOT NULL && product_new <> '') AND 
				cases_sn1_serial_number_1cases_ida = " . $this->db->quoted($this->recordId);
		$res = $this->db->query($sql);
		$retval = "";
		while ($row = $this->db->fetchByAssoc($res)) {
			$retval .= "<tr>";
			$retval .= "<td>" . $row['serial_number_new'] . "</td>";
			$retval .= "<td>" . $app_list_strings['rma_product_list'][$row['rma_product']] . "</td>";
			$retval .= "</tr>";
		}
		return $retval;
	}
	
	
}

function dtbc_email_serial_numbers() {
	$obj = new EmailSerialNumberTableGenerator();
	return $obj->run();
}
