<?php

class YearlyPoints {
	private $data;
	
	public function __construct() {
		$this->data = array();
	}
	
	public function addData($data) {
		$this->data[] = $data;
	}
	
	public function getAvailablePoints() {
		$retval = array(
			"total" => 0,
			"gift" => 0,
		);
		foreach ($this->data as $data) {
			$retval['total'] += $data['sum_total'];
			$retval['gift'] += abs($data['sum_gift']);
		}
		return $retval['total'] - $retval['gift'];
	}
}
