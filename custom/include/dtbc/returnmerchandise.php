<?php

class ReturnMerchandise {
	public function before_save($bean, $event, $arguments) {
		if ($bean->case_record_type_c == "rma") {
			require_once("custom/include/dtbc/EmailSender.php"); 
			$emailSender = new EmailSender();
			// Only for RMA cases
			
			// AC 16.6.1, AC 16.6.2
			if (($bean->replacement_type_c != $bean->fetched_row['replacement_type_c'] && $bean->replacement_type_c == 2) || $this->isServiceKitPurchased($bean)) {
				// Replacement type changed to "From customer's stock"
				$bean->case_status_c = 10; // RMA Shipped - Waiting for return
				
				// Send email to customer "Compensation Terms Check"
				require_once("custom/include/dtbc/EmailSender.php");
				global $sugar_config;
				$emailAddress = $emailSender->getEmailAddresFromCase($bean);
				$emailId = $sugar_config['solaredge']['return_merchandise_return_part_email_id'];
				$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $bean->id, $emailId);
			}			
		}
	}
	
	private function isServiceKitPurchased($caseBean) {
		if (!empty($caseBean->account_id) && strlen($caseBean->account_id) > 0) {
			$accountBean = BeanFactory::getBean("Accounts", $caseBean->account_id);
			return $accountBean->solaredge_service_kit_holder_c == 1;
		}
		return false;
	}
	
}
