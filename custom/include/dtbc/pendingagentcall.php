<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class PendingAgentCall {
	
	public function after_save($bean, $event, $arguments) {
		// Sub status is "Pending Agent Call"
		if ($bean->case_sub_status_c == 22 && $bean->dtbc_agent_status_noti_sent_c == 0) {
			require_once("custom/include/dtbc/EmailSender.php");
			global $sugar_config;

			$userBean = BeanFactory::getBean("Users", $bean->assigned_user_id);
			$emailAddress = $userBean->email1;			
			$emailId = $sugar_config['solaredge']['pending_agent_call_email_id'];
			
			$emailSender = new EmailSender();
			$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $bean->id, $emailId);
		}
	}
}