<?php
// created: 2017-08-31 12:53:12
$dictionary["uzc1_usa_zip_codes_cases"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'uzc1_usa_zip_codes_cases' => 
    array (
      'lhs_module' => 'UZC1_USA_ZIP_Codes',
      'lhs_table' => 'uzc1_usa_zip_codes',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'uzc1_usa_zip_codes_cases_c',
      'join_key_lhs' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
      'join_key_rhs' => 'uzc1_usa_zip_codes_casescases_idb',
    ),
  ),
  'table' => 'uzc1_usa_zip_codes_cases_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'uzc1_usa_zip_codes_casescases_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'uzc1_usa_zip_codes_casesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'uzc1_usa_zip_codes_cases_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'uzc1_usa_zip_codes_cases_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'uzc1_usa_zip_codes_casescases_idb',
      ),
    ),
  ),
);