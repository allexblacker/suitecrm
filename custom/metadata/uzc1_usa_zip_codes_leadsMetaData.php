<?php
// created: 2017-08-31 12:53:12
$dictionary["uzc1_usa_zip_codes_leads"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'uzc1_usa_zip_codes_leads' => 
    array (
      'lhs_module' => 'UZC1_USA_ZIP_Codes',
      'lhs_table' => 'uzc1_usa_zip_codes',
      'lhs_key' => 'id',
      'rhs_module' => 'Leads',
      'rhs_table' => 'leads',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'uzc1_usa_zip_codes_leads_c',
      'join_key_lhs' => 'uzc1_usa_zip_codes_leadsuzc1_usa_zip_codes_ida',
      'join_key_rhs' => 'uzc1_usa_zip_codes_leadsleads_idb',
    ),
  ),
  'table' => 'uzc1_usa_zip_codes_leads_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'uzc1_usa_zip_codes_leadsuzc1_usa_zip_codes_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'uzc1_usa_zip_codes_leadsleads_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'uzc1_usa_zip_codes_leadsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'uzc1_usa_zip_codes_leads_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'uzc1_usa_zip_codes_leadsuzc1_usa_zip_codes_ida',
        1 => 'uzc1_usa_zip_codes_leadsleads_idb',
      ),
    ),
  ),
);