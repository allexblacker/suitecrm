<?php 
 //WARNING: The contents of this file are auto-generated



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$layout_defs['SN1_Serial_Number']['subpanel_setup']['alt_part'] = array(
		'order' => 100,
		'module' => 'AP1_Alternative_Part',
		'get_subpanel_data'=>'function:getAltParts',
		'sort_order' => 'asc',
		'sort_by' => 'name',
		'subpanel_name' => 'default',
		'title_key' => 'LBL_ALT_PART', 
		'generate_select' => false,
		'function_parameters' => array(
			'import_function_file' => 'custom/modules/SN1_Serial_Number/serialSubPanels.php',
			'serial_id' => $this->_focus->id,
		)
	);

//auto-generated file DO NOT EDIT
$layout_defs['SN1_Serial_Number']['subpanel_setup']['alt_part']['override_subpanel_name'] = 'SN1_Serial_Number_subpanel_alt_part';

?>