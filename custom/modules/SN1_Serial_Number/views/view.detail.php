<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/DetailView/DetailViewCustomCache.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class SN1_Serial_NumberViewDetail extends ViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailViewCustomCache();
 	    $this->dv->ss =&  $this->ss;
        
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
}
