<?php
$popupMeta = array (
    'moduleMain' => 'SN1_Serial_Number',
    'varName' => 'SN1_Serial_Number',
    'orderBy' => 'sn1_serial_number.name',
    'whereClauses' => array (
  'name' => 'sn1_serial_number.name',
  'case1' => 'sn1_serial_number.case1',
  'assigned_user_id' => 'sn1_serial_number.assigned_user_id',
  'searchform_security_group' => 'sn1_serial_number.searchform_security_group',
  'serial_number_new' => 'sn1_serial_number.serial_number_new',
  'cases_sn1_serial_number_1_name' => 'sn1_serial_number.cases_sn1_serial_number_1_name',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'case1',
  5 => 'assigned_user_id',
  6 => 'searchform_security_group',
  7 => 'serial_number_new',
  8 => 'cases_sn1_serial_number_1_name',
),
    'searchdefs' => array (
  'case1' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'Case1',
    'id' => 'ACASE_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'case1',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'searchform_security_group' => 
  array (
    'name' => 'searchform_security_group',
    'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
    'type' => 'enum',
    'displayParams' => 
    array (
      'size' => 1,
    ),
    'width' => '10%',
  ),
  'serial_number_new' => 
  array (
    'type' => 'varchar',
    'label' => 'Serial_Number_new',
    'width' => '10%',
    'name' => 'serial_number_new',
  ),
  'cases_sn1_serial_number_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
    'id' => 'CASES_SN1_SERIAL_NUMBER_1CASES_IDA',
    'width' => '10%',
    'name' => 'cases_sn1_serial_number_1_name',
  ),
),
);
