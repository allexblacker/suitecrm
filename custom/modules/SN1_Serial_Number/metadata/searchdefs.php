<?php
$module_name = 'SN1_Serial_Number';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'case1' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'Case1',
        'id' => 'ACASE_ID_C',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'case1',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'searchform_security_group' => 
      array (
        'name' => 'searchform_security_group',
        'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
        'default' => true,
        'type' => 'enum',
        'displayParams' => 
        array (
          'size' => 1,
        ),
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
