<?php
// created: 2018-03-07 08:18:33
$subpanel_layout['list_fields'] = array (
  'subpanel_check_rma_c' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_CHECK_RMA_CHECKBOX',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'product_old' => 
  array (
    'vname' => 'Product_old',
    'width' => '45%',
    'default' => true,
  ),
  'serial_number_new' => 
  array (
    'type' => 'varchar',
    'vname' => 'Serial_Number_new',
    'width' => '10%',
    'default' => true,
  ),
  'product_new' => 
  array (
    'vname' => 'Product_New',
    'width' => '45%',
    'default' => true,
  ),
  'fault_category' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'default' => true,
    'vname' => 'Fault_Category',
    'width' => '10%',
  ),
  'rma' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'RMA',
    'width' => '10%',
  ),
  'ibolt_status' => 
  array (
    'type' => 'varchar',
    'vname' => 'iBolt_Status',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'rma_alert_c' => 
  array (
    'type' => 'function',
    'studio' => 'visible',
    'vname' => 'LBL_RMA_ALERT',
    'width' => '10%',
    'default' => true,
  ),
);