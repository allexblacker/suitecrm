<?php
// created: 2018-01-23 12:08:40
$searchFields['SN1_Serial_Number'] = array (
  'searchform_security_group' => 
  array (
    'query_type' => 'default',
    'operator' => 'subquery',
    'subquery' => 'SELECT sn1_serial_number.id FROM securitygroups_records
						INNER JOIN sn1_serial_number
						ON sn1_serial_number.id = securitygroups_records.record_id
							AND sn1_serial_number.deleted = 0
						INNER JOIN securitygroups
						ON securitygroups.id = securitygroups_records.securitygroup_id
							AND securitygroups.deleted = 0
						WHERE securitygroups_records.deleted = 0
							AND securitygroups_records.module = \'SN1_Serial_Number\'
							AND securitygroups.name LIKE',
    'db_field' => 
    array (
      0 => 'id',
    ),
    'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
  ),
  'name' => 
  array (
    'query_type' => 'default',
  ),
  'current_user_only' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'assigned_user_id',
    ),
    'my_items' => true,
    'vname' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
  ),
  'assigned_user_id' => 
  array (
    'query_type' => 'default',
  ),
  'range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
);