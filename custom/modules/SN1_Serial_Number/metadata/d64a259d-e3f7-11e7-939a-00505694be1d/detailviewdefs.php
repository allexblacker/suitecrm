<?php
$module_name = 'SN1_Serial_Number';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'product_old',
            'label' => 'Product_old',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'serial_number_new',
            'label' => 'Serial_Number_new',
          ),
          1 => 
          array (
            'name' => 'product_new',
            'label' => 'Product_New',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'fault_category',
            'studio' => 'visible',
            'label' => 'Fault_Category',
          ),
          1 => 
          array (
            'name' => 'rma',
            'label' => 'RMA',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'fault_sub_category',
            'studio' => 'visible',
            'label' => 'Fault_sub_category',
          ),
          1 => 
          array (
            'name' => 'rma_product',
            'studio' => 'visible',
            'label' => 'RMA_Product',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'fault_description',
            'studio' => 'visible',
            'label' => 'Fault_Description',
          ),
          1 => 
          array (
            'name' => 'fa_by_air',
            'label' => 'FA_By_air',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'details_for_fault_ategory',
            'studio' => 'visible',
            'label' => 'details_for_fault_ategory_other',
          ),
        ),
      ),
      'lbl_detailview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'family',
            'label' => 'Family',
          ),
          1 => 
          array (
            'name' => 'gen',
            'label' => 'Gen',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'part_description',
            'label' => 'Part_Description',
          ),
          1 => 
          array (
            'name' => 'warrant_extension',
            'label' => 'Warrant_Extension',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case1',
            'studio' => 'visible',
            'label' => 'Case1',
          ),
          1 => 
          array (
            'name' => 'warranty_end_date',
            'label' => 'Warranty_End_Date',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'need_fa_immediate_return',
            'label' => 'Need_FA_immediate_return',
          ),
          1 => 
          array (
            'name' => 'doa',
            'studio' => 'visible',
            'label' => 'DOA',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'return_asked_by',
            'studio' => 'visible',
            'label' => 'Return_asked_by',
          ),
          1 => 
          array (
            'name' => 'rca',
            'label' => 'RCA',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'alternative_ibolt_status',
            'label' => 'Alternative_iBolt_Status',
          ),
          1 => 
          array (
            'name' => 'doa_type',
            'studio' => 'visible',
            'label' => 'DOA_Type',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'ibolt_status',
            'label' => 'iBolt_Status',
          ),
          1 => 
          array (
            'name' => 'model',
            'studio' => 'visible',
            'label' => 'Model',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'replaced_type',
            'studio' => 'visible',
            'label' => 'Replaced_Type',
          ),
          1 => 
          array (
            'name' => 'reason_for_not_in_warranty',
            'label' => 'reason_for_not_in_warranty',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'fa_is_done',
            'label' => 'FA_is_done',
          ),
          1 => 
          array (
            'name' => 'rma_in_erp',
            'label' => 'RMA_in_ERP',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'need_immediate_return_time',
            'label' => 'Need_Immediate_Return_Time_Stamp',
          ),
          1 => 
          array (
            'name' => 'manufacturing_related_fault',
            'studio' => 'visible',
            'label' => 'Manufacturing_Related_Fault',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'failure_type',
            'studio' => 'visible',
            'label' => 'Failure_Type',
          ),
          1 => 
          array (
            'name' => 'failed_board',
            'label' => 'Failed_Board',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'bsuf_file_is_attached',
            'label' => 'BSUF_file_is_attached',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'listview_security_group',
            'label' => 'LBL_LISTVIEW_GROUP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'fa_date',
            'label' => 'FA_Date',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'part_number',
            'label' => 'Part_Number',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'distributor_name',
            'label' => 'Distributor_Name',
          ),
          1 => 
          array (
            'name' => 'distributor_number',
            'label' => 'Distributor_Number',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'rml',
            'label' => 'RML',
          ),
          1 => 
          array (
            'name' => 'm40',
            'studio' => 'visible',
            'label' => 'M40',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'portia_memory',
            'studio' => 'visible',
            'label' => 'Portia_Memory',
          ),
          1 => 
          array (
            'name' => 'digital_pcb_version',
            'studio' => 'visible',
            'label' => 'Digital_PCB_Version',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'inv_assy_ac_dc_conduit',
            'studio' => 'visible',
            'label' => 'Inv_assy_AC_DC_conduit',
          ),
          1 => 
          array (
            'name' => 'inv_assy_heat_sink',
            'studio' => 'visible',
            'label' => 'Inv_assy_Heat_Sink',
          ),
        ),
      ),
    ),
  ),
);
?>
