<?php
$module_name = 'SN1_Serial_Number';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PRODUCT_OLD' => 
  array (
    'type' => 'varchar',
    'label' => 'Product_old',
    'width' => '10%',
    'default' => true,
  ),
  'PRODUCT_NEW' => 
  array (
    'type' => 'varchar',
    'label' => 'Product_New',
    'width' => '10%',
    'default' => true,
  ),
  'FAULT_CATEGORY' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Fault_Category',
    'width' => '10%',
    'default' => true,
  ),
  'FAULT_SUB_CATEGORY' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Fault_sub_category',
    'width' => '10%',
    'default' => true,
  ),
  'FAULT_DESCRIPTION' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Fault_Description',
    'width' => '10%',
    'default' => true,
  ),
  'RMA_PRODUCT' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'RMA_Product',
    'width' => '10%',
    'default' => true,
  ),
  'RMA' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'RMA',
    'width' => '10%',
  ),
  'IBOLT_STATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'iBolt_Status',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'RMA_ALERT_C' => 
  array (
    'type' => 'function',
    'studio' => 'visible',
    'label' => 'LBL_RMA_ALERT',
    'width' => '10%',
    'default' => true,
  ),
  'ALTERNATIVE_IBOLT_STATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'Alternative_iBolt_Status',
    'width' => '10%',
    'default' => false,
  ),
  'ATTACHING_BOARD_CONFIG_FILE' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Attaching_Board_config_file_is_mandatory',
    'width' => '10%',
  ),
  'BSUF_FILE_IS_ATTACHED' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'BSUF_file_is_attached',
    'width' => '10%',
  ),
  'CASE1' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'Case1',
    'id' => 'ACASE_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => false,
  ),
  'CE_SERVICEADMIN_FOR_WFS' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'CE_ServiceAdmin_for_WFs',
    'width' => '10%',
  ),
  'CRD' => 
  array (
    'type' => 'varchar',
    'label' => 'CRD',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'DETAILS_FOR_FAULT_ATEGORY' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'details_for_fault_ategory_other',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'DIGITAL_PCB_VERSION' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Digital_PCB_Version',
    'width' => '10%',
    'default' => false,
  ),
  'DISTRIBUTOR_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'Distributor_Name',
    'width' => '10%',
    'default' => false,
  ),
  'DISTRIBUTOR_NUMBER' => 
  array (
    'type' => 'varchar',
    'label' => 'Distributor_Number',
    'width' => '10%',
    'default' => false,
  ),
  'DOA' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'DOA',
    'width' => '10%',
    'default' => false,
  ),
  'DOA_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'DOA_Type',
    'width' => '10%',
    'default' => false,
  ),
  'END_OF_RMA' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'End_of_RMA',
    'width' => '10%',
  ),
  'EXCEPTION' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Exception',
    'width' => '10%',
  ),
  'FA_BY_AIR' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'FA_By_air',
    'width' => '10%',
  ),
  'FA_DATE' => 
  array (
    'type' => 'date',
    'label' => 'FA_Date',
    'width' => '10%',
    'default' => false,
  ),
  'FA_FAULT' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'FA_Fault',
    'width' => '10%',
    'default' => false,
  ),
  'FA_IS_DONE' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'FA_is_done',
    'width' => '10%',
  ),
  'FAILED_BOARD' => 
  array (
    'type' => 'varchar',
    'label' => 'Failed_Board',
    'width' => '10%',
    'default' => false,
  ),
  'FAILED_ITEM_SN' => 
  array (
    'type' => 'varchar',
    'label' => 'Failed_Item_SN',
    'width' => '10%',
    'default' => false,
  ),
  'FAILURE_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Failure_Type',
    'width' => '10%',
    'default' => false,
  ),
  'FAMILY' => 
  array (
    'type' => 'varchar',
    'label' => 'Family',
    'width' => '10%',
    'default' => false,
  ),
  'GEN' => 
  array (
    'type' => 'varchar',
    'label' => 'Gen',
    'width' => '10%',
    'default' => false,
  ),
  'INV_ASSY_AC_DC_CONDUIT' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Inv_assy_AC_DC_conduit',
    'width' => '10%',
    'default' => false,
  ),
  'INV_ASSY_HEAT_SINK' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Inv_assy_Heat_Sink',
    'width' => '10%',
    'default' => false,
  ),
  'MANUFACTURING_RELATED_FAULT' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Manufacturing_Related_Fault',
    'width' => '10%',
    'default' => false,
  ),
  'M40' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'M40',
    'width' => '10%',
    'default' => false,
  ),
  'MODEL' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Model',
    'width' => '10%',
    'default' => false,
  ),
  'NEED_FA_IMMEDIATE_RETURN' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Need_FA_immediate_return',
    'width' => '10%',
  ),
  'NEED_IMMEDIATE_RETURN_TIME' => 
  array (
    'type' => 'date',
    'label' => 'Need_Immediate_Return_Time_Stamp',
    'width' => '10%',
    'default' => false,
  ),
  'NOTE' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Note',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'PART_DESCRIPTION' => 
  array (
    'type' => 'varchar',
    'label' => 'Part_Description',
    'width' => '10%',
    'default' => false,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
  'PORTIA_MEMORY' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Portia_Memory',
    'width' => '10%',
    'default' => false,
  ),
  'QA_EMPLOYEE_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'QA_Employee_Name',
    'width' => '10%',
    'default' => false,
  ),
  'RCA' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'RCA',
    'width' => '10%',
  ),
  'REASON_FOR_NOT_IN_WARRANTY' => 
  array (
    'type' => 'varchar',
    'label' => 'reason_for_not_in_warranty',
    'width' => '10%',
    'default' => false,
  ),
  'REPLACED_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Replaced_Type',
    'width' => '10%',
    'default' => false,
  ),
  'RETURN_ASKED_BY' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Return_asked_by',
    'width' => '10%',
    'default' => false,
  ),
  'RMA_IN_ERP' => 
  array (
    'type' => 'varchar',
    'label' => 'RMA_in_ERP',
    'width' => '10%',
    'default' => false,
  ),
  'RML' => 
  array (
    'type' => 'varchar',
    'label' => 'RML',
    'width' => '10%',
    'default' => false,
  ),
  'SERIAL_NUMBER_NEW' => 
  array (
    'type' => 'varchar',
    'label' => 'Serial_Number_new',
    'width' => '10%',
    'default' => false,
  ),
  'WARRANTY_END_DATE' => 
  array (
    'type' => 'varchar',
    'label' => 'Warranty_End_Date',
    'width' => '10%',
    'default' => false,
  ),
  'WARRANT_EXTENSION' => 
  array (
    'type' => 'varchar',
    'label' => 'Warrant_Extension',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'WARRANTY_EXTENSION' => 
  array (
    'type' => 'varchar',
    'label' => 'Warranty_Extension',
    'width' => '10%',
    'default' => false,
  ),
);
?>
