<?php
$module_name = 'dtbc_Line_Item';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'discount_c',
            'label' => 'LBL_DISCOUNT',
          ),
          1 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'kwforop_c',
            'label' => 'LBL_KWFOROP',
          ),
          1 => 
          array (
            'name' => 'list_price_c',
            'label' => 'LBL_LIST_PRICE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'opportunities_dtbc_line_item_1_name',
            'label' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_OPPORTUNITIES_TITLE',
          ),
          1 => 
          array (
            'name' => 'product_c',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'product_family_from_product_c',
            'label' => 'LBL_PRODUCT_FAMILY_FROM_PRODUCT',
          ),
          1 => 
          array (
            'name' => 'quantity_c',
            'label' => 'LBL_QUANTITY',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'shipment_date_c',
            'label' => 'LBL_SHIPMENT_DATE',
          ),
          1 => 
          array (
            'name' => 'total_kw_c',
            'label' => 'LBL_TOTAL_KW',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'total_price_c',
            'label' => 'LBL_TOTAL_PRICE',
          ),
          1 => 
          array (
            'name' => 'unit_price_after_discount_c',
            'label' => 'LBL_UNIT_PRICE_AFTER_DISCOUNT',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'aos_quotes_dtbc_line_item_1_name',
            'label' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_AOS_QUOTES_TITLE',
          ),
        ),
      ),
    ),
  ),
);
?>
