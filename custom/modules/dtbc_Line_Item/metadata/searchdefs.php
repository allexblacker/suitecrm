<?php
$module_name = 'dtbc_Line_Item';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'quantity_c' => 
      array (
        'type' => 'decimal',
        'default' => true,
        'label' => 'LBL_QUANTITY',
        'width' => '10%',
        'name' => 'quantity_c',
      ),
      'shipment_date_c' => 
      array (
        'type' => 'date',
        'default' => true,
        'label' => 'LBL_SHIPMENT_DATE',
        'width' => '10%',
        'name' => 'shipment_date_c',
      ),
      'opportunities_dtbc_line_item_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_OPPORTUNITIES_TITLE',
        'id' => 'OPPORTUNITIES_DTBC_LINE_ITEM_1OPPORTUNITIES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'opportunities_dtbc_line_item_1_name',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
