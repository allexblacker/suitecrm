<?php
$module_name = 'dtbc_Line_Item';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'product_c',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT',
          ),
          1 => 
          array (
            'name' => 'list_price_c',
            'label' => 'LBL_LIST_PRICE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'eu_product_required_c',
            'studio' => 'visible',
            'label' => 'LBL_EU_PRODUCT_REQUIRED',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'quantity_c',
            'label' => 'LBL_QUANTITY',
          ),
          1 => 
          array (
            'name' => 'discount_c',
            'label' => 'LBL_DISCOUNT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'product_family_from_product_c',
            'label' => 'LBL_PRODUCT_FAMILY_FROM_PRODUCT',
          ),
          1 => 
          array (
            'name' => 'unit_price_after_discount_c',
            'label' => 'LBL_UNIT_PRICE_AFTER_DISCOUNT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'opportunities_dtbc_line_item_1_name',
          ),
          1 => 
          array (
            'name' => 'total_price_c',
            'label' => 'LBL_TOTAL_PRICE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'kwforop_c',
            'label' => 'LBL_KWFOROP',
          ),
          1 => 
          array (
            'name' => 'total_kw_c',
            'label' => 'LBL_TOTAL_KW',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'shipment_date_c',
            'label' => 'LBL_SHIPMENT_DATE',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 'description',
        ),
      ),
    ),
  ),
);
?>
