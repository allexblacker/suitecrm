function dtbc_set_return(popup_reply_data) {
    popup_reply_data.name_to_value_array.list_price_c = formatNumber(popup_reply_data.name_to_value_array.list_price_c, num_grp_sep, dec_sep);
    popup_reply_data.name_to_value_array.standard_price_c = formatNumber(popup_reply_data.name_to_value_array.standard_price_c, num_grp_sep, dec_sep);
    set_return(popup_reply_data);
	if (popup_reply_data.name_to_value_array.use_standard_price_c == "1" && popup_reply_data.name_to_value_array.standard_price_c != "")
		$("#list_price_c").val(popup_reply_data.name_to_value_array.standard_price_c);
}

function productPopup() {
	open_popup(
		"dtbc_Pricebook_Product", 
		600, 
		400, 
		"&opportunityId=" + $("#opportunities_dtbc_line_item_1opportunities_ida").val(), 
		true, 
		false, 
		{"call_back_function":"dtbc_set_return","form_name":"EditView","field_to_name_array":{"id":"dtbc_pricebook_product_id_c","name":"product_c", "list_price_c":"list_price_c", "standard_price_c":"standard_price_c", "use_standard_price_c":"use_standard_price_c"}}, 
		"single", 
		true
	);
}

$(function() {
	$("#btn_product_c").removeAttr("onclick");
    $("#btn_product_c").prop('onclick', null).off('click');
    $('#btn_product_c').on('click', function () {
        productPopup();
    });
});