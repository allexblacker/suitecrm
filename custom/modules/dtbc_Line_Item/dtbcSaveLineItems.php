<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

echo 'Get line item ids...<br>';

$db = DBManagerFactory::getInstance();

$sql  = "SELECT id_c 
		FROM dtbc_line_item_cstm 
		JOIN dtbc_line_item ON id = id_c
		WHERE deleted = 0";
$resp = $db->query($sql);

echo 'Get line item ids DONE.<br>';
echo '<br>';

echo 'Saving Line items...<br>';

$counter = 0;

while ($row = $db->fetchByAssoc($resp))
{
	$lineItem = BeanFactory::getBean('dtbc_Line_Item')->retrieve($row['id_c']);

	if ((empty($lineItem->list_price_c) || $lineItem->list_price_c == 0) && empty($lineItem->dtbc_pricebook_product_id_c))
	{
		continue;
	}

	if ((empty($lineItem->list_price_c) || $lineItem->list_price_c == 0) && !empty($lineItem->dtbc_pricebook_product_id_c))
	{
		$productBean = BeanFactory::getBean('dtbc_Pricebook_Product', $lineItem->dtbc_pricebook_product_id_c);

		if (empty($productBean->list_price_c) || $productBean->list_price_c == 0)
		{
			continue;
		}

		$lineItem->list_price_c = $productBean->list_price_c;
	}

	$lineItem->save();
	$lineItem = null;

	$counter++;
}

echo '<br>';
echo 'Saving Line items DONE.<br>';
echo '<br>';

var_dump('Saved items: ' . $counter);
