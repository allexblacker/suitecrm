<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/JSLoader.php');

class dtbc_Line_ItemViewEdit extends ViewEdit {

    function __construct(){
        parent::__construct();
    }

    function display() {
        parent::display();

		includeJavaScriptFile("custom/modules/dtbc_Line_Item/dtbc/editview.js");
    }	
}
