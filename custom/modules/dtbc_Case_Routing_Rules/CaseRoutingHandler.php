<?php

require_once('custom/include/dtbc/helpers.php');

class CaseRoutingHandler {
	public function setRouting(&$caseBean) {
		global $db;
		
		$sql = "SELECT case_priority_c, case_origin_c 
				FROM dtbc_case_routing_rules_cstm
				WHERE " . $db->quoted($caseBean->origin_email_address_c) . " LIKE CONCAT('%', email_address_c, '%')";
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res)) {
			$caseBean->priority = $res['case_priority_c'];
			$caseBean->case_origin_c = $res['case_origin_c'];
			return true;
		}
		
		return false;
	}
}

