<?php
$module_name = 'SPA_SPA';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'spa_status',
            'studio' => 'visible',
            'label' => 'LBL_SPA_STATUS',
          ),
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'spa_type',
            'studio' => 'visible',
            'label' => 'LBL_SPA_TYPE',
          ),
          1 => 
          array (
            'name' => 'installer_spa_type',
            'studio' => 'visible',
            'label' => 'LBL_INSTALLER_SPA_TYPE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'installer_spa_level',
            'studio' => 'visible',
            'label' => 'LBL_INSTALLER_SPA_LEVEL',
          ),
          1 => 
          array (
            'name' => 'spa_region',
            'studio' => 'visible',
            'label' => 'LBL_SPA_REGION',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'installer_spa_start_date',
            'label' => 'LBL_INSTALLER_SPA_START_DATE',
          ),
          1 => 
          array (
            'name' => 'installer_spa_end_date',
            'label' => 'LBL_INSTALLER_SPA_END_DATE',
          ),
        ),
      ),
    ),
  ),
);
?>
