<?php
$module_name = 'SPA_SPA';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'SPA_STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_SPA_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'SPA_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_SPA_TYPE',
    'width' => '10%',
    'default' => true,
  ),
  'INSTALLER_SPA_START_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INSTALLER_SPA_START_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'INSTALLER_SPA_END_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INSTALLER_SPA_END_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'INSTALLER_SPA_LEVEL' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_INSTALLER_SPA_LEVEL',
    'width' => '10%',
    'default' => true,
  ),
  'INSTALLER_SPA_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_INSTALLER_SPA_TYPE',
    'width' => '10%',
    'default' => true,
  ),
);
?>
