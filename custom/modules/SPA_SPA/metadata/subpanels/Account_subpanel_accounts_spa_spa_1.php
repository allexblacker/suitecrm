<?php
// created: 2018-02-26 16:57:30
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'spa_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_SPA_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'spa_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_SPA_TYPE',
    'width' => '10%',
    'default' => true,
  ),
  'installer_spa_start_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_INSTALLER_SPA_START_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'installer_spa_end_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_INSTALLER_SPA_END_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'installer_spa_level' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_INSTALLER_SPA_LEVEL',
    'width' => '10%',
    'default' => true,
  ),
  'installer_spa_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_INSTALLER_SPA_TYPE',
    'width' => '10%',
    'default' => true,
  ),
);