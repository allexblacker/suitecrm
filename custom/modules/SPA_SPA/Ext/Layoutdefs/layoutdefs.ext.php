<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-03-12 10:44:31
$layout_defs["SPA_SPA"]["subpanel_setup"]['spa_spa_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'spa_spa_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2018-02-26 16:40:15
$layout_defs["SPA_SPA"]["subpanel_setup"]['spa_spa_spadi_spa_distributor_1'] = array (
  'order' => 100,
  'module' => 'SPADI_SPA_Distributor',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
  'get_subpanel_data' => 'spa_spa_spadi_spa_distributor_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['SPA_SPA']['subpanel_setup']['spa_spa_spadi_spa_distributor_1']['override_subpanel_name'] = 'SPA_SPA_subpanel_spa_spa_spadi_spa_distributor_1';

?>