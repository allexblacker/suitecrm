<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-03-08 10:20:27

 

 // created: 2018-03-06 08:50:20

 

 // created: 2018-03-08 10:24:06
$dictionary['SPA_SPA']['fields']['q4_19_marketing_fund_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q4_19_marketing_fund_c']['labelValue']='Q4\'19 Marketing Fund';

 

// created: 2018-03-12 10:44:31
$dictionary["SPA_SPA"]["fields"]["spa_spa_documents_1"] = array (
  'name' => 'spa_spa_documents_1',
  'type' => 'link',
  'relationship' => 'spa_spa_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


 // created: 2018-03-06 08:50:43

 

 // created: 2018-03-08 10:23:42
$dictionary['SPA_SPA']['fields']['q3_19_marketing_fund_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q3_19_marketing_fund_c']['labelValue']='Q3\'19 Marketing Fund';

 

 // created: 2018-03-08 10:22:23
$dictionary['SPA_SPA']['fields']['q4_19_target_kw_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q4_19_target_kw_c']['labelValue']='Q4\'19 Target KW';

 

 // created: 2018-03-08 10:23:12
$dictionary['SPA_SPA']['fields']['q2_19_marketing_fund_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q2_19_marketing_fund_c']['labelValue']='Q2\'19 Marketing Fund';

 

 // created: 2018-03-08 10:21:52
$dictionary['SPA_SPA']['fields']['q3_19_target_kw_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q3_19_target_kw_c']['labelValue']='Q3\'19 Target KW';

 

 // created: 2018-03-08 10:21:07
$dictionary['SPA_SPA']['fields']['q1_19_target_kw_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q1_19_target_kw_c']['labelValue']='Q1\'19 Target KW';

 

 // created: 2018-03-08 10:22:49
$dictionary['SPA_SPA']['fields']['q1_19_marketing_fund_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q1_19_marketing_fund_c']['labelValue']='Q1\'19 Marketing Fund';

 

 // created: 2018-03-08 10:21:30
$dictionary['SPA_SPA']['fields']['q2_19_target_kw_c']['inline_edit']='1';
$dictionary['SPA_SPA']['fields']['q2_19_target_kw_c']['labelValue']='Q2\'19 Target KW';

 

// created: 2018-02-26 16:32:15
$dictionary["SPA_SPA"]["fields"]["accounts_spa_spa_1"] = array (
  'name' => 'accounts_spa_spa_1',
  'type' => 'link',
  'relationship' => 'accounts_spa_spa_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_spa_spa_1accounts_ida',
);
$dictionary["SPA_SPA"]["fields"]["accounts_spa_spa_1_name"] = array (
  'name' => 'accounts_spa_spa_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_spa_spa_1accounts_ida',
  'link' => 'accounts_spa_spa_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["SPA_SPA"]["fields"]["accounts_spa_spa_1accounts_ida"] = array (
  'name' => 'accounts_spa_spa_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_spa_spa_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SPA_SPA_1_FROM_SPA_SPA_TITLE',
);


// created: 2018-02-26 16:40:15
$dictionary["SPA_SPA"]["fields"]["spa_spa_spadi_spa_distributor_1"] = array (
  'name' => 'spa_spa_spadi_spa_distributor_1',
  'type' => 'link',
  'relationship' => 'spa_spa_spadi_spa_distributor_1',
  'source' => 'non-db',
  'module' => 'SPADI_SPA_Distributor',
  'bean_name' => 'SPADI_SPA_Distributor',
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
);


 // created: 2018-03-08 10:20:01

 

 // created: 2018-03-06 08:50:24

 
?>