<?php
$module_name = 'OS1_Outside_Sales_Activity';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'ATTENDING_AND_TITLE' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Attending_and_title',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'DISTRI_RIDE_ALONG' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Distri_Ride_Along',
    'width' => '10%',
    'default' => false,
  ),
  'F2F_MEETING_SET_BY_INSIDE_SALE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'F2F_Meeting_set_by_Inside_Sales	',
    'width' => '10%',
    'default' => false,
  ),
  'MEETING_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Meeting_Date',
    'width' => '10%',
    'default' => false,
  ),
  'NOTES' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Notes',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'MEETING_TYPE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Meeting_type',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'NEXT_STEPS_PLANNED_COMM' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Next_Steps_Planned_Comm',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'NEXT_STEPS_PLANNED_RESI' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Next_Steps_planned_resi',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'CONVERT_INCREASE_SEDG_SHARE' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'Convert_increase_SEDG_share_requirements',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
);
?>
