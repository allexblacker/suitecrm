<?php
// created: 2018-03-01 17:52:37
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'attending_and_title' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'Attending_and_title',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'meeting_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Meeting_type',
    'width' => '10%',
    'default' => true,
  ),
  'notes' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'Notes',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'next_steps_planned_resi' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'Next_Steps_planned_resi',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'next_steps_planned_comm' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'Next_Steps_Planned_Comm',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);