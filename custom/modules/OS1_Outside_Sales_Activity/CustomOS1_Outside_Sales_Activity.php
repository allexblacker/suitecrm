<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
require_once('modules/OS1_Outside_Sales_Activity/OS1_Outside_Sales_Activity.php');

class CustomOS1_Outside_Sales_Activity extends OS1_Outside_Sales_Activity {
    
	public function create_new_list_query($order_by, $where,$filter=array(),$params=array(), $show_deleted = 0,$join_type='', $return_array = false,$parentbean=null, $singleSelect = false, $ifListForExport = false) {
		$res = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);
		
		if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'index' && empty($_REQUEST['lvso']))
			$res['order_by'] = " ORDER BY os1_outside_sales_activity.date_entered DESC ";
		
		return $res;
	}

}