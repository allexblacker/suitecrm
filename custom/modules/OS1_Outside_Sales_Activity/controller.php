<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("include/MVC/Controller/SugarController.php");
require_once("custom/modules/OS1_Outside_Sales_Activity/CustomOS1_Outside_Sales_Activity.php");

class CustomOS1_Outside_Sales_ActivityController extends SugarController {
	
	public function loadBean()
	{
		if(!empty($GLOBALS['beanList'][$this->module])) {
			$this->bean = new CustomOS1_Outside_Sales_Activity();
			if(!empty($this->record)) {
				$this->bean->retrieve($this->record);
				if ($this->bean)
					$GLOBALS['FOCUS'] = $this->bean;
			}
		}
	}
	
}
