<?php
// created: 2017-11-10 13:30:38
$mod_strings = array (
  'LBL_DETAILVIEW_PANEL3' => 'Lead Detail',
  'LBL_DETAILVIEW_PANEL1' => 'Lead Detail',
  'LBL_DETAILVIEW_PANEL4' => 'Address Information',
  'LBL_DETAILVIEW_PANEL6' => 'Competitive Information',
  'LBL_DETAILVIEW_PANEL2' => 'New Panel 2',
  'LBL_STATE' => 'State',
  'COUNTY' => 'US County',
  'LBL_LEAD_AGE' => 'lead age',
  'LBL_CONVERTED' => 'Converted',
  'LBL_STATUS' => 'Status:',
  'LBL_COUNTRY' => 'Country',
  'LBL_LINKEDIN_SEARCH' => 'linkedin search',
  'LBL_DETAILVIEW_PANEL5' => 'Trade show Information',
  'LBL_EDITVIEW_PANEL2' => 'Lead Detail',
  'LBL_EDITVIEW_PANEL1' => 'Address Information',
  'LBL_EDITVIEW_PANEL3' => 'Additional Information',
  'LBL_EDITVIEW_PANEL4' => 'System Information',
  'LBL_LAST_NAME' => 'Last Name:',
  'LBL_ZIP_CODE' => 'zip code',
  'LBL_CANADIAN_PROVINCE' => 'Canadian Province',
  'LBL_PLACE_NAME_CANADA_ONLY' => 'place name canada only',
  'TYPE' => 'Type',
  'LBL_MOBILE_PHONE' => 'Mobile:',
  'LBL_GOOGLE_SEARCH' => 'google search',
  'LBL_EDITVIEW_PANEL6' => 'Trade show Information',
  'LBL_EDITVIEW_PANEL7' => 'Competitive Information',
  'LBL_BUSINESS_TYPE' => 'Business Type',
  'LBL_GOOGLE_SEARCH_COMPANY' => 'google search company',
  'LBL_CAMPAIGN_SCORE' => 'Campaign Score',
  'LBL_TOTAL_LEAD_SCORE' => 'Total Lead Score',
  'LBL_LEAD_PRIORITY' => 'Lead Priority',
  'LBL_XING_SEARCH' => 'Xing Search',
  'LBL_USA_ZIP_CODE_UZC1_USA_ZIP_CODES_ID' => 'USA Zip Code (related  ID)',
  'LBL_USA_ZIP_CODE' => 'USA Zip Code',
  'LBL_CUSTOMER_ACQUISITION_ACTIVIT' => 'customer Acquisition Activity',
  'LBL_WEBSITE' => 'Website',
  'LBL_EDITVIEW_PANEL8' => 'Additional Test Fields',
  'LBL_CAMPAIGN_TEXT' => 'Campaign (text)',
  'COMPANY' => 'Company',
  'REQUEST' => 'Request',
  'LBL_DISTRIBUTOR_NAME' => 'distributor name',
  'LBL_ACTIVITY_FOCUS' => 'activity focus',
  'LBL_LEAD_SCORE' => 'lead score',
  'LBL_CLASSIFICATION' => 'Classification',
  'LBL_NEWSLETTER' => 'Newsletter',
  'LBL_FIRST_NAME' => 'First Name:',
  'LBL_INDUSTRY' => 'Industry',
  'LBL_GEOGRAPHIC_AREA' => 'Geographic Area',
  'LBL_HOUSE_EXHIBITIONS' => 'House Exhibitions',
  'LBL_SE_TRAINING_ATTENDED' => 'SE training attended',
  'LBL_TYPE_OF_INSTALLATION' => 'Type Of Installation',
  'LBL_DISTRIBUTOR_STATUS' => 'Distributor Status',
  'LBL_BETS_SELLING_INVERTER' => 'Best Selling Inverter',
  'LBL_BEST_SELLING_INVERTER' => 'Best Selling Inverter',
  'LBL_AUSTRALIAN_STATE' => 'Australian State',
  'LBL_COMMIT_LEVEL' => 'Commit Level',
  'LBL_RATING' => 'Rating',
  'LBL_CONVERTED_DATE' => 'Converted Date',
  'LBL_MOBILE' => 'Mobile',
  'LBL_EMAIL_2' => 'Email 2',
  'LBL_TECHNICAL_BUYER' => 'Technical Buyer',
  'LBL_DECISION_MAKER' => 'Decision Maker',
  'LBL_STREET' => 'Street',
  'LBL_CITY' => 'City',
  'LBL_ZIP' => 'zip',
  'LBL_STATE_PROVINCE' => 'state province',
  'LBL_COMMENTS' => 'Comments',
  'LBL_ADDRESS' => 'Address',
  'LBL_LEAD_SOURCE' => 'Lead Source:',
  'LBL_PRIMARY_ADDRESS_STREET' => 'Primary Address Street',
  'LBL_PRIMARY_ADDRESS_STREET_BACK' => 'primary address street back',
  'LBL_TITLE' => 'Title:',
  'LBL_MAIN_PANEL_BRANDS_C' => 'Main Panel Brands',
);