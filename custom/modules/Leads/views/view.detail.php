<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/DetailView/DetailViewCustomCache.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class LeadsViewDetail extends ViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailViewCustomCache();
 	    $this->dv->ss =&  $this->ss;
        
		$fieldsToHTMLDecode = array('linkedin_search_c','google_search_c','xing_search_c');
		foreach($fieldsToHTMLDecode as $field){
			$this->bean->$field = html_entity_decode($this->bean->$field);
		}
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
}
