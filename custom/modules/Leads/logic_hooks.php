<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array(1, 'Leads push feed', 'modules/Leads/SugarFeeds/LeadFeed.php','LeadFeed', 'pushFeed'); 
$hook_array['before_save'][] = Array(77, 'updateGeocodeInfo', 'modules/Leads/LeadsJjwg_MapsLogicHook.php','LeadsJjwg_MapsLogicHook', 'updateGeocodeInfo'); 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(77, 'updateRelatedMeetingsGeocodeInfo', 'modules/Leads/LeadsJjwg_MapsLogicHook.php','LeadsJjwg_MapsLogicHook', 'updateRelatedMeetingsGeocodeInfo'); 
$hook_array['after_ui_frame'] = Array(); 
$hook_array['before_delete'] = Array(); 
$hook_array['before_delete'][] = Array(1, 'Notify Act-On when a lead is being deleted', 'custom/modules/Leads/acton_lead_hook.php','ActonLeadHook', 'delete'); 
$hook_array['before_restore'] = Array(); 
$hook_array['before_restore'][] = Array(1, 'Notify Act-On when a lead is being undeleted', 'custom/modules/Leads/acton_lead_hook.php','ActonLeadHook', 'undelete'); 



?>