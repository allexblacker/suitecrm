<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class leadCustomLogicHooks {
    function updateZipDetailsLogicHooks($bean, $event, $arguments){
        $leadId = $bean->id;
        // Get Lead Zip Code 
        $query =  "SELECT zip_code_c, country_c FROM leads_cstm "; 
        $query .= "WHERE id_c = '$leadId' LIMIT 1";
        $results = $bean->db->query($query, true);
        $row = $bean->db->fetchByAssoc($results);
        if($row != null) {
            $zipCode = $row['zip_code_c'];
            $country = $row['country_c'];
            // Get Zip Details
            $query =  "SELECT county, city, state, canadian_province, place_name_canada_only "; 
            $query .= "FROM uzc1_usa_zip_codes WHERE zip = '$zipCode' LIMIT 1";
            $results = $bean->db->query($query, true);
            $row1 = $bean->db->fetchByAssoc($results);
            if($row1 != null) {
                $state = $row1['state'];
                $county = $row1['county'];
                $city = $row1['city'];
                $canadian_province = $row1['canadian_province'];
                $place_name_canada_only = $row1['place_name_canada_only'];
                //Update Lead with Zip Details 
                //$query = "UPDATE leads_cstm SET county_c = '" . $county . "', canadian_province_c = '" . $canadian_province . "', place_name_canada_only_c = '" . $place_name_canada_only . "', state_c = '" . $state . "' ";
                //$query .= "WHERE id_c = '$leadId'";
                //$results = $bean->db->query($query, true, "Error updating");
                //$row = $bean->db->fetchByAssoc($results);
                $bean->county_c = $county;
                $bean->canadian_province_c = $canadian_province;
                $bean->place_name_canada_only_c = $place_name_canada_only;
                //$bean->description = $state;
                if($country = 'Unitedstates')
                   $bean->state_c = 'Unitedstates_' . $state;
                else
                   $bean->state_c = 'Australia_' . $state;
                $bean->primary_address_city = $city;
                //$bean->description = $query;
            }
        }
    }
}
