<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-10-04 15:59:42
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_products_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_products_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_dtbc_pricebook_product_1aos_products_ida',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_products_dtbc_pricebook_product_1_name"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_dtbc_pricebook_product_1aos_products_ida',
  'link' => 'aos_products_dtbc_pricebook_product_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_products_dtbc_pricebook_product_1aos_products_ida"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);


 // created: 2017-10-05 16:41:10
$dictionary['dtbc_Pricebook_Product']['fields']['standard_price_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook_Product']['fields']['standard_price_c']['labelValue']='Standard Price';

 

 // created: 2017-10-05 19:09:35
$dictionary['dtbc_Pricebook_Product']['fields']['list_price_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook_Product']['fields']['list_price_c']['labelValue']='List Price';

 

 // created: 2017-10-05 16:28:13
$dictionary['dtbc_Pricebook_Product']['fields']['active_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook_Product']['fields']['active_c']['labelValue']='Active';

 

 // created: 2017-10-05 16:31:40
$dictionary['dtbc_Pricebook_Product']['fields']['aos_products_id_c']['inline_edit']=1;

 

 // created: 2017-10-05 18:44:47
$dictionary['dtbc_Pricebook_Product']['fields']['currency_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook_Product']['fields']['currency_c']['labelValue']='Currency';

 

// created: 2017-10-04 15:50:43
$dictionary["dtbc_Pricebook_Product"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook',
  'bean_name' => 'dtbc_Pricebook',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_TITLE',
  'id_name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1_name"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_TITLE',
  'save' => true,
  'id_name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
  'link' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'table' => 'dtbc_pricebook',
  'module' => 'dtbc_Pricebook',
  'rname' => 'name',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
  'type' => 'link',
  'relationship' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);


 // created: 2017-10-05 16:31:40
$dictionary['dtbc_Pricebook_Product']['fields']['product_code_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook_Product']['fields']['product_code_c']['labelValue']='Product Code';

 

 // created: 2017-10-18 17:11:44
$dictionary['dtbc_Pricebook_Product']['fields']['use_standard_price_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook_Product']['fields']['use_standard_price_c']['labelValue']='Use Standard Price';

 

// created: 2017-10-13 13:04:41
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_quotes_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_QUOTES_TITLE',
  'id_name' => 'aos_quotes_dtbc_pricebook_product_1aos_quotes_ida',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_quotes_dtbc_pricebook_product_1_name"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_QUOTES_TITLE',
  'save' => true,
  'id_name' => 'aos_quotes_dtbc_pricebook_product_1aos_quotes_ida',
  'link' => 'aos_quotes_dtbc_pricebook_product_1',
  'table' => 'aos_quotes',
  'module' => 'AOS_Quotes',
  'rname' => 'name',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_quotes_dtbc_pricebook_product_1aos_quotes_ida"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1aos_quotes_ida',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);

?>