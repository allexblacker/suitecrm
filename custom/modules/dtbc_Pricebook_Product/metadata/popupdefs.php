<?php
$popupMeta = array (
    'moduleMain' => 'dtbc_Pricebook_Product',
    'varName' => 'dtbc_Pricebook_Product',
    'orderBy' => 'dtbc_pricebook_product.name',
    'whereClauses' => array (
  'name' => 'dtbc_pricebook_product.name',
),
    'searchInputs' => array (
  0 => 'dtbc_pricebook_product_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
    'name' => 'description',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
