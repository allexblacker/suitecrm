<?php
// created: 2017-10-17 15:22:50
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'product_code_c' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PRODUCT_CODE',
    'id' => 'AOS_PRODUCTS_ID_C',
    'link' => true,
    'width' => '10%',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'AOS_Products',
    'target_record_key' => 'aos_products_id_c',
  ),
  'currency_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_CURRENCY',
    'width' => '10%',
  ),
  'standard_price_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_STANDARD_PRICE',
    'width' => '10%',
  ),
  'list_price_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_LIST_PRICE',
    'width' => '10%',
  ),
  'active_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_ACTIVE',
    'width' => '10%',
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'dtbc_Pricebook_Product',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'dtbc_Pricebook_Product',
    'width' => '5%',
    'default' => true,
  ),
);