<?php
// created: 2017-10-18 16:16:31
$mod_strings = array (
  'LBL_SHIPMENT_DATE' => 'Shipment Date',
  'LBL_QUANTITY' => 'Quantity',
  'LBL_DISCOUNT' => 'Discount',
  'LBL_TOTAL_PRICE' => 'Total Price',
  'LBL_UNIT_PRICE_AFTER_DISCOUNT' => 'Unit Price After Discount',
  'LBL_SALE_TYPE' => 'Sale Type',
  'LBL_TOTAL_KW' => 'Total KW',
  'LBL_ACTIVE' => 'Active',
  'LBL_PRODUCT_CODE' => 'Product Code',
  'LBL_PRODUCT_CODE_AOS_PRODUCTS_ID' => 'Product Code (related  ID)',
  'LBL_LIST_PRICE' => 'List Price',
  'LBL_USE_STANDARD_PRICE' => 'Use Standard Price',
  'LBL_CURRENCY' => 'Currency',
  'LBL_STANDARD_PRICE' => 'Standard Price',
  'LBL_EDITVIEW_PANEL1' => 'Pricing',
);