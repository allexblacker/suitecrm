<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');

class dtbc_Pricebook_ProductViewEdit extends ViewEdit {
 	function display() {
        parent::display();
		echo getVersionedScript("custom/modules/dtbc_Pricebook_Product/dtbc/editview.js");
    }
}

