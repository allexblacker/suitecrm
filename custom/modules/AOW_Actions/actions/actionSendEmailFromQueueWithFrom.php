<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/modules/AOW_Actions/actions/actionSendEmail.php');
require_once("modules/emqu_dtbc_Email_Queue/emqu_dtbc_Email_Queue.php");
require_once("custom/include/dtbc/helpers.php");

class actionSendEmailFromQueueWithFrom extends actionSendEmail {
    
    function edit_display($line,SugarBean $bean = null, $params = array()){
        global $app_list_strings;
        $email_templates_arr = get_bean_select_array(true, 'EmailTemplate', 'name', '', 'name');

        if(!in_array($bean->module_dir,getEmailableModules())) unset($app_list_strings['aow_email_type_list']['Record Email']);
        $targetOptions = getRelatedEmailableFields($bean->module_dir);
        if(empty($targetOptions)) unset($app_list_strings['aow_email_type_list']['Related Field']);

        $html = '<input type="hidden" name="aow_email_type_list" id="aow_email_type_list" value="'.get_select_options_with_id($app_list_strings['aow_email_type_list'], '').'">
				<input type="hidden" name="aow_email_to_list" id="aow_email_to_list" value="'.get_select_options_with_id($app_list_strings['aow_email_to_list'], '').'">
				<input type="hidden" name="aow_actions_param['.$line.'][email_template]" value="'.$params['value'][1].'">';

        $html .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' data-workflow-action='send-email'>";
        $html .= "<tr>";

        if(!isset($params['email_template'])) $params['email_template'] = '';
        $hidden = "style='visibility: hidden;'";
        if($params['email_template'] != '') $hidden = "";

        $html .= '<td id="name_label" scope="row" valign="top"><label>' . translate("LBL_EMAIL_TEMPLATE",
                "AOW_Actions") . ':<span class="required">*</span></label></td>';
        $html .= "<td valign='top'>";
        $html .= "<select name='aow_actions_param[".$line."][email_template]' id='aow_actions_param_email_template".$line."' onchange='show_edit_template_link(this,".$line.");' >".get_select_options_with_id($email_templates_arr, $params['email_template'])."</select>";

        $html .= "&nbsp;<a href='javascript:open_email_template_form(".$line.")' >".translate('LBL_CREATE_EMAIL_TEMPLATE','AOW_Actions')."</a>";
        $html .= "&nbsp;<span name='edit_template' id='aow_actions_edit_template_link".$line."' $hidden><a href='javascript:edit_email_template_form(".$line.")' >".translate('LBL_EDIT_EMAIL_TEMPLATE','AOW_Actions')."</a></span>";
        $html .= "</td>";
        $html .= "</tr>";
        
		$html .= '
			<tr>
				<td><b>' . translate("LBL_WORKFLOW_ID", "AOW_Actions") . '</b></td>
				<td colspan="3">
					<script language="javascript">if(typeof sqs_objects == "undefined"){var sqs_objects = new Array;}sqs_objects["EditView_modified_by_name"]={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["modified_by_name","modified_user_id"],"required_list":["modified_user_id"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects["EditView_created_by_name"]={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["created_by_name","created_by"],"required_list":["created_by"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects["EditView_assigned_user_name"]={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["assigned_user_name","assigned_user_id"],"required_list":["assigned_user_id"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects["EditView_parent_name"]={"form":"EditView","method":"query","modules":["Accounts"],"group":"or","field_list":["name","id"],"populate_list":["parent_name","parent_id"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects["EditView_aow_actions_param[1][value][1]_display"]={"form":"EditView","method":"query","modules":["AOW_WorkFlow"],"group":"or","field_list":["name","id"],"populate_list":["aow_actions_param[1][value][1]_display","aow_aow_actions_param[1][value][1]_display"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};</script>
					<input type="text" name="aow_actions_param[' . $line . '][workflow_id][name]_display" class="sqsEnabled yui-ac-input" tabindex="1" id="aow_actions_param[' . $line . '][workflow_id][name]" size="" value="' . $params['workflow_id']['name'] . '" title="" autocomplete="off">
					<input type="hidden" name="aow_actions_param[' . $line . '][workflow_id][id]" id="aow_actions_param[' . $line . '][workflow_id][id]" value="' . $params['workflow_id']['id'] . '">
					<span class="id-ff multiple">
						<button type="button" title="Select" class="button firstChild" value="Select" onclick="open_popup(
							&quot;AOW_WorkFlow&quot;, 
							600, 
							400, 
							&quot;&quot;, 
							true, 
							false, 
							{&quot;call_back_function&quot;:&quot;set_return&quot;,&quot;form_name&quot;:&quot;EditView&quot;,&quot;field_to_name_array&quot;:{&quot;id&quot;:&quot;aow_actions_param[' . $line . '][workflow_id][id]&quot;,&quot;name&quot;:&quot;aow_actions_param[' . $line . '][workflow_id][name]&quot;}}, 
							&quot;single&quot;, 
							true
						);"><img src="themes/SolarEdgeP/images/id-ff-select.png?v=mou7_vwmhgFHEAHFov6GRQ"></button>
						<button type="button" tabindex="1" title="Clear Selection" class="button lastChild" onclick="SUGAR.clearRelateField(this.form, \"aow_actions_param[' . $line . '][workflow_id][name]\", \"aow_actions_param[' . $line . '][workflow_id][id]\");" value="Clear Selection"><img src="themes/SolarEdgeP/images/id-ff-clear.png?v=mou7_vwmhgFHEAHFov6GRQ"></button>
					</span>
					<script type="text/javascript">
						SUGAR.util.doWhen(
								"typeof(sqs_objects) != \"undefined\" && typeof(sqs_objects[\"EditView_aow_actions_param[1][value][1]_display\"]) != \"undefined\"",
								enableQS
						);
					</script>
				</td>
			</tr>';
		
        $html .= "<tr>";
        $html .= "<td>";
        $html .= "<b>" . translate('LBL_SEND_EMAIL_FROM_TEXT','AOW_Actions') . ":</b>";
        $html .= "</td>";
        $html .= "<td colspan='3'>";
        $html .= "<select name='aow_actions_param[" . $line . "][email_smtp]' id='aow_actions_param_email_smtp" .$line . "'>";
        
        global $sugar_config;
        $fromList = $sugar_config['solaredge']['SMTPList'];
        foreach($fromList as $key => $value){
            if (str_replace(" ", "_", $value['name']) == $params['email_smtp'])
				$html .= "<option value='$key' selected='selected'>" . $value['name'] . "</option>";
			else
				$html .= "<option value='$key'>" . $value['name'] . "</option>";
        }
            
        $html .= "</select>";
        $html .= "</td>";
        $html .= "</tr>";
        
        $html .= "<tr>";
        $html .= '<td id="name_label" scope="row" valign="top"><label>' . translate("LBL_EMAIL",
                "AOW_Actions") . ':<span class="required">*</span></label></td>';
        $html .= '<td valign="top" scope="row">';

        $html .='<button type="button" onclick="add_emailLine('.$line.')"><img src="'.SugarThemeRegistry::current()->getImageURL('id-ff-add.png').'"></button>';
        $html .= '<table id="emailLine'.$line.'_table" width="100%" class="email-line"></table>';
        $html .= '</td>';
        $html .= "</tr>";
        $html .= "</table>";

        $html .= "<script id ='aow_script".$line."'>";

        //backward compatible
        if(isset($params['email_target_type']) && !is_array($params['email_target_type'])){
            $email = '';
            switch($params['email_target_type']){
                case 'Email Address':
                    $email = $params['email'];
                    break;
                case 'Specify User':
                    $email = $params['email_user_id'];
                    break;
                case 'Related Field':
                    $email = $params['email_target'];
                    break;
            }
            $html .= "load_emailline('".$line."','to','".$params['email_target_type']."','".$email."');";
        }
        //end backward compatible

        if(isset($params['email_target_type'])){
            foreach($params['email_target_type'] as $key => $field){
                if(is_array($params['email'][$key]))$params['email'][$key] = json_encode($params['email'][$key]);
                $html .= "load_emailline('".$line."','".$params['email_to_type'][$key]."','".$params['email_target_type'][$key]."','".$params['email'][$key]."');";
            }
        }
        $html .= "</script>";

        return $html;

    }
    
    function run_action(SugarBean $bean, $params = array(), $in_save=false){
        
        include_once('modules/EmailTemplates/EmailTemplate.php');
        $emailTemp = new EmailTemplate();
        $emailTemp->retrieve($params['email_template']);

        if ($emailTemp->id == '') {
            return false;
        }

        $emails = $this->getEmailsFromParams($bean, $params);

        global $sugar_config;
        $SMTPConfigKey = $params['email_smtp'];

        if (!isset($emails['to']) || empty($emails['to']))
            return false;


        $this->parse_template($bean, $emailTemp);
		if($emailTemp->text_only=='1') {
			$email_body_html = $emailTemp->body;
		} else {
			$email_body_html = $emailTemp->body_html;
		}
		
		$SMTPList = $sugar_config['solaredge']['SMTPList'][$SMTPConfigKey];
		$from_email = $SMTPList['email_from'];
        $from_name = $SMTPList['email_from_name'];
			
		if ($SMTPList['email_from'] == "current_user") {
            global $current_user;
            $from_email = $current_user->email1;
            $from_name = $current_user->name;
        }
		
		return $this->setQueueRecord($params['email_template'], $SMTPConfigKey, $emails['to'], $emailTemp->subject, $email_body_html, $emailTemp->body, $bean, $emails['cc'], $emails['bcc'], $from_email, $from_name, $params['workflow_id']['id']);
			
    }

   
    
    private function getEmailsFromParams(SugarBean $bean, $params){
        $emails = array();
        //backward compatible
        if(isset($params['email_target_type']) && !is_array($params['email_target_type'])){
            $email = '';
            switch($params['email_target_type']){
                case 'Email Address':
                    $params['email'] = array($params['email']);
                    break;
                case 'Specify User':
                    $params['email'] = array($params['email_user_id']);
                    break;
                case 'Related Field':
                    $params['email'] = array($params['email_target']);
                    break;
				case 'Module_Field':
					$params['email'] = array($params['email']);
					break;
            }
            $params['email_target_type'] = array($params['email_target_type']);
            $params['email_to_type'] = array('to');
        }
        //end backward compatible
        if(isset($params['email_target_type'])){
            foreach($params['email_target_type'] as $key => $field){
                switch($field){
                    case 'Email Address':
                        if(trim($params['email'][$key]) != '')
                            $emails[$params['email_to_type'][$key]][] = $params['email'][$key];
                        break;
                    case 'Specify User':
                        $user = new User();
                        $user->retrieve($params['email'][$key]);
                        $user_email = $user->emailAddress->getPrimaryAddress($user);
                        if(trim($user_email) != '') {
                            $emails[$params['email_to_type'][$key]][] = $user_email;
                            $emails['template_override'][$user_email] = array('Users' => $user->id);
                        }

                        break;
                    case 'Users':
                        $users = array();
                        switch($params['email'][$key][0]) {
                            Case 'security_group':
                                if(file_exists('modules/SecurityGroups/SecurityGroup.php')){
                                    require_once('modules/SecurityGroups/SecurityGroup.php');
                                    $security_group = new SecurityGroup();
                                    $security_group->retrieve($params['email'][$key][1]);
                                    $users = $security_group->get_linked_beans( 'users','User');
                                    $r_users = array();
                                    if($params['email'][$key][2] != ''){
                                        require_once('modules/ACLRoles/ACLRole.php');
                                        $role = new ACLRole();
                                        $role->retrieve($params['email'][$key][2]);
                                        $role_users = $role->get_linked_beans( 'users','User');
                                        foreach($role_users as $role_user){
                                            $r_users[$role_user->id] = $role_user->name;
                                        }
                                    }
                                    foreach($users as $user_id => $user){
                                        if($params['email'][$key][2] != '' && !isset($r_users[$user->id])){
                                            unset($users[$user_id]);
                                        }
                                    }
                                    break;
                                }
                            //No Security Group module found - fall through.
                            Case 'role':
                                require_once('modules/ACLRoles/ACLRole.php');
                                $role = new ACLRole();
                                $role->retrieve($params['email'][$key][2]);
                                $users = $role->get_linked_beans( 'users','User');
                                break;
                            Case 'all':
                            default:
                                global $db;
                                $sql = "SELECT id from users WHERE status='Active' AND portal_only=0 ";
                                $result = $db->query($sql);
                                while ($row = $db->fetchByAssoc($result)) {
                                    $user = new User();
                                    $user->retrieve($row['id']);
                                    $users[$user->id] = $user;
                                }
                                break;
                        }
                        foreach($users as $user){
                            $user_email = $user->emailAddress->getPrimaryAddress($user);
                            if(trim($user_email) != '') {
                                $emails[$params['email_to_type'][$key]][] = $user_email;
                                $emails['template_override'][$user_email] = array('Users' => $user->id);
                            }
                        }
                        break;
                    case 'Related Field':
                        $emailTarget = $params['email'][$key];
                        $relatedFields = array_merge($bean->get_related_fields(), $bean->get_linked_fields());
                        $field = $relatedFields[$emailTarget];
                        if($field['type'] == 'relate') {
                            $linkedBeans = array();
                            $idName = $field['id_name'];
                            $id = $bean->$idName;
                            $linkedBeans[] = BeanFactory::getBean($field['module'], $id);
                        }
                        else if($field['type'] == 'link'){
                            $relField = $field['name'];
                            if(isset($field['module']) && $field['module'] != '') {
                                $rel_module = $field['module'];
                            } else if($bean->load_relationship($relField)){
                                $rel_module = $bean->$relField->getRelatedModuleName();
                            }
                            $linkedBeans = $bean->get_linked_beans($relField,$rel_module);
                        }else{
                            $linkedBeans = $bean->get_linked_beans($field['link'],$field['module']);
                        }
                        if($linkedBeans){
                            foreach($linkedBeans as $linkedBean) {
								if ($linkedBean->emailAddress == null)
									continue;
                                $rel_email = $linkedBean->emailAddress->getPrimaryAddress($linkedBean);
                                if (trim($rel_email) != '') {
                                    $emails[$params['email_to_type'][$key]][] = $rel_email;
                                    $emails['template_override'][$rel_email] = array($linkedBean->module_dir => $linkedBean->id);
                                }
                            }
                        }
                        break;
                    case 'Record Email':
                        $recordEmail = $bean->emailAddress->getPrimaryAddress($bean);
                        if($recordEmail == '' && isset($bean->email1)) $recordEmail = $bean->email1;
                        if(trim($recordEmail) != '')
                            $emails[$params['email_to_type'][$key]][] = $recordEmail;
                        break;
					case 'Module_Field':
						$fieldName = $params['email'][$key];
						$emails[$params['email_to_type'][$key]][] = $bean->$fieldName;
						break;
                }
            }
        }
        return $emails;
    }
        
    function setQueueRecord($emailTemplateId, $SMTPConfigKey, $emailToInput, $emailSubject, $emailBody, $altemailBody, SugarBean $relatedBean = null, $emailCc = array(), $emailBcc = array(), $from_email, $from_name, $wfId) {
		$emailTo = DtbcHelpers::validateEmailAddressArray($emailToInput);

        if (empty($emailTo)) {
			return false;
		}

        // now create a queue item
		$newQueue = new emqu_dtbc_Email_Queue();
		$newQueue->name = 'Email from ' . $from_email;
		$newQueue->email_subject_c = $emailSubject . " " . date("d-m-Y");
		$newQueue->email_content_c = $altemailBody;
		$newQueue->email_content_html_c = $emailBody;
		$newQueue->from_address_c = $from_email;
		$newQueue->from_name_c = $from_name;
		$newQueue->to_address_c = implode(',', $emailTo);
		$newQueue->cc_address_c = implode(',', $emailCc);
		$newQueue->bcc_address_c = implode(',', $emailBcc);
		if ($relatedBean instanceOf SugarBean && !empty($relatedBean->id)) {
			$newQueue->related_bean_type_c = $relatedBean->module_dir;
			$newQueue->related_bean_id_c = $relatedBean->id;
		}
		$newQueue->email_template_id_c = $emailTemplateId;
		$newQueue->smtp_config_key_c = $SMTPConfigKey;
		$newQueue->aow_workflow_id_c = $wfId;
		$newQueue->save();

        return true;
    }
}

