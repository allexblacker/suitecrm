<?php

require_once('modules/AOW_Actions/actions/actionCreateRecord.php');
require_once("custom/include/dtbc/helpers.php");

class actionCreateScheduledWorkflow extends actionCreateRecord {

    private $emailableModules = array();

    function __construct($id = ''){
        parent::__construct($id);
    }

    function edit_display($line, SugarBean $bean = null, $params = array()){
        global $app_list_strings;

        $modules = $app_list_strings['aow_moduleList'];

        $checked = 'CHECKED';
        if(isset($params['relate_to_workflow']) && !$params['relate_to_workflow']) $checked = '';

        $html = "<table border='0' cellpadding='0' cellspacing='0' width='100%' data-workflow-action='create-record'>";
        $html .= "<tr>";
        $html .= '<td id="name_label" class="name_label" scope="row" valign="top"><label>' .
                 translate("LBL_RECORD_TYPE", "AOW_Actions") .
                 '</label>:<span class="required">*</span>&nbsp;&nbsp;';
		$html .= $modules['dtbc_System_Workflow_Scheduling'];
		$html .= '<input type="hidden" name="aow_actions_param[' . $line . '][record_type]" id="aow_actions_param_record_type' . $line . '" value="dtbc_System_Workflow_Scheduling" /></td>';

        $html .= '<td id="relate_label" class="relate_label" scope="row" valign="top"><label>' .
                 translate("LBL_RELATE_WORKFLOW", "AOW_Actions") .
                 '</label>:';
        $html .= "<input type='hidden' name='aow_actions_param[" . $line . "][relate_to_workflow]' value='0' >";
        $html .= "<input type='checkbox' id='aow_actions_param[" . $line . "][relate_to_workflow]' name='aow_actions_param[" . $line . "][relate_to_workflow]' value='1' $checked></td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= '<td colspan="4" scope="row"><table id="crLine' . $line . '_table" width="100%" class="lines">';
		
		$html .= '
		<tbody id="crLine1_body0">
			<tr id="crLine1_line0" data-workflow-action-line="">
				<td></td>
				<td><input type="hidden" id="aow_actions_param1_field0" name="aow_actions_param[' . $line . '][field][0]" value="parent_type" />Parent Type</td>
				<td id="crLine1_fieldType0"><input type="hidden" id="aow_actions_param[' . $line . '][value_type][0]" name="aow_actions_param[' . $line . '][value_type][0]" value="Value" />Value</td>
				<td id="crLine1_field0"><input type="hidden" id="aow_actions_param[' . $line . '][value][0]" name="aow_actions_param[' . $line . '][value][0]" value="' . $bean->module_name . '" />' . $bean->module_name . '</td>
			</tr>
		</tbody>';
		
		$html .= '
		<tbody id="crLine1_body1">
			<tr id="crLine1_line1" data-workflow-action-line="">
				<td></td>
				<td><input type="hidden" id="aow_actions_param1_field1" name="aow_actions_param[' . $line . '][field][1]" value="aow_workflow_id_c" />' . translate("LBL_WORKFLOW_ID", "AOW_Actions") . '</td>
				<td id="crLine1_fieldType1"><input type="hidden" id="aow_actions_param[' . $line . '][value_type][1]" name="aow_actions_param[' . $line . '][value_type][1]" value="Value" />Value</td>
				<td id="crLine1_field1">
					<script language="javascript">if(typeof sqs_objects == "undefined"){var sqs_objects = new Array;}sqs_objects["EditView_modified_by_name"]={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["modified_by_name","modified_user_id"],"required_list":["modified_user_id"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects["EditView_created_by_name"]={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["created_by_name","created_by"],"required_list":["created_by"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects["EditView_assigned_user_name"]={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["assigned_user_name","assigned_user_id"],"required_list":["assigned_user_id"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects["EditView_parent_name"]={"form":"EditView","method":"query","modules":["Accounts"],"group":"or","field_list":["name","id"],"populate_list":["parent_name","parent_id"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects["EditView_aow_actions_param[1][value][1]_display"]={"form":"EditView","method":"query","modules":["AOW_WorkFlow"],"group":"or","field_list":["name","id"],"populate_list":["aow_actions_param[1][value][1]_display","aow_aow_actions_param[1][value][1]_display"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};</script>
					<input type="text" name="aow_actions_param[' . $line . '][value][1]_display" class="sqsEnabled yui-ac-input" tabindex="1" id="aow_actions_param[' . $line . '][value][1]_display" size="" value="' . $this->getParamValue($params, "value", 1, true) . '" title="" autocomplete="off"><div id="EditView_aow_actions_param[1][value][1]_display_results" class="yui-ac-container"><div class="yui-ac-content" style="display: none;"><div class="yui-ac-hd" style="display: none;"></div><div class="yui-ac-bd"><ul><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li></ul></div><div class="yui-ac-ft" style="display: none;"></div></div></div>
					<input type="hidden" name="aow_actions_param[' . $line . '][value][1]" id="aow_actions_param[' . $line . '][value][1]" value="' . $this->getParamValue($params, "value", 1) . '">
					<span class="id-ff multiple">
						<button type="button" name="btn_aow_actions_param[1][value][1]_display" id="btn_aow_actions_param[1][value][1]_display" tabindex="1" title="Select" class="button firstChild" value="Select" onclick="open_popup(
							&quot;AOW_WorkFlow&quot;, 
							600, 
							400, 
							&quot;&quot;, 
							true, 
							false, 
							{&quot;call_back_function&quot;:&quot;set_return&quot;,&quot;form_name&quot;:&quot;EditView&quot;,&quot;field_to_name_array&quot;:{&quot;id&quot;:&quot;aow_actions_param[' . $line . '][value][1]&quot;,&quot;name&quot;:&quot;aow_actions_param[' . $line . '][value][1]_display&quot;}}, 
							&quot;single&quot;, 
							true
						);"><img src="themes/SolarEdgeP/images/id-ff-select.png?v=mou7_vwmhgFHEAHFov6GRQ"></button><button type="button" name="btn_clr_aow_actions_param[1][value][1]_display" id="btn_clr_aow_actions_param[1][value][1]_display" tabindex="1" title="Clear Selection" class="button lastChild" onclick="SUGAR.clearRelateField(this.form, \"aow_actions_param[' . $line . '][value][1]_display\", \"aow_actions_param[' . $line . '][value][1]\");" value="Clear Selection"><img src="themes/SolarEdgeP/images/id-ff-clear.png?v=mou7_vwmhgFHEAHFov6GRQ"></button>
					</span>
					<script type="text/javascript">
						SUGAR.util.doWhen(
								"typeof(sqs_objects) != \"undefined\" && typeof(sqs_objects[\"EditView_aow_actions_param[1][value][1]_display\"]) != \"undefined\"",
								enableQS
						);
					</script>
				</td>
			</tr>
		</tbody>';
		
		$html .= '
		<tbody id="crLine1_body2">
			<tr id="crLine1_line2" data-workflow-action-line="">
				<td></td>
				<td><input type="hidden" id="aow_actions_param1_field2" name="aow_actions_param[' . $line . '][field][2]" value="trigger_time_c" />Trigger Time</td>
				<td id="crLine1_fieldType2"><input type="hidden" id="aow_actions_param[' . $line . '][value_type][2]" name="aow_actions_param[' . $line . '][value_type][2]" value="Date" />Date</td>
				<td id="crLine1_field2">
					<select type="text" name="aow_actions_param[' . $line . '][value][2][0]" id="aow_actions_param[' . $line . '][value][2][0]" title="" tabindex="116">' . 
						$this->getDateOptions($this->getParamValue($params, "value", 2, false, 0), $bean->module_name) . '
					</select>&nbsp;&nbsp;
					<select type="text" name="aow_actions_param[' . $line . '][value][2][1]" id="aow_actions_param[' . $line . '][value][2][1]" onchange="date_field_change(&quot;aow_actions_param[' . $line . '][value][2]&quot;)" title="" tabindex="116">' . 
						$this->getNormalOptions($this->getParamValue($params, "value", 2, false, 1), "aow_date_operator", "now") . '
					</select>&nbsp;
					<input type="text" style="" name="aow_actions_param[' . $line . '][value][2][2]" id="aow_actions_param[' . $line . '][value][2][2]" title="" value="' . $this->getParamValue($params, "value", 2, false, 2) . '" tabindex="116">&nbsp;
					<select type="text" style="" name="aow_actions_param[' . $line . '][value][2][3]" id="aow_actions_param[' . $line . '][value][2][3]" title="" tabindex="116">' . 
						$this->getNormalOptions($this->getParamValue($params, "value", 2, false, 3), "aow_date_type_list", "", "--None--") . '
					</select>
				</td>
			</tr>
		</tbody>';
		
		$html .= '</table></td>';
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= '<td colspan="4" scope="row"><input type="button" tabindex="116" style="display:none" class="button" value="'.translate("LBL_ADD_FIELD","AOW_Actions").'" id="addcrline'.$line.'" onclick="add_crLine('.$line.')" /></td>';
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= '<td colspan="4" scope="row"><table id="crRelLine' . $line . '_table" width="100%" class="relationship"></table></td>';
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= '<td colspan="4" scope="row"><input type="button" tabindex="116" style="display:none" class="button" value="'.translate("LBL_ADD_RELATIONSHIP","AOW_Actions").'" id="addcrrelline'.$line.'" onclick="add_crRelLine('.$line.')" /></td>';
        $html .= "</tr>";

        return $html;
    }
	
	private function getNormalOptions($default, $listName, $blankValue = "", $blankName = "") {
		global $app_list_strings;
		$retval = "";
		foreach ($app_list_strings[$listName] as $key => $value) {
			if (empty($key)) {
				if (!empty($blankValue))
					$key = $blankValue;
				if (!empty($blankName))
					$value = $blankName;
			}
				
			$defaultOption = $key == $default ? "selected" : "";
			$retval .= '<option value="' . $key . '" ' . $defaultOption . '>' . $value . '</option>';
		}
		return $retval;
	}
	
	private function getDateOptions($default, $module) {
		$retval = $this->getNormalOptions($default, 'aow_date_options');
		$focus = BeanFactory::getBean($module);
		foreach ($focus->field_name_map as $key => $value) {
			if ($value['type'] != "datetime" && $value['type'] != "date")
				continue;
			$defaultOption = $key == $default ? "selected" : "";
			$fieldValue = !empty($value['labelValue']) ? $value['labelValue'] : translate($value['vname']);
			$retval .= '<option value="' . $key . '" ' . $defaultOption . '>' . $fieldValue . '</option>';
		}
		return $retval;
	}
	
	private function getParamValue($params, $key, $index, $getRelatedName = false, $subIndex = -1) {
		if (empty($params[$key]) || !isset($params[$key]))
			return "";
		
		if (empty($params[$key][$index]) || !isset($params[$key][$index]))
			return "";
		
		if ($getRelatedName) {
			$focus = BeanFactory::getBean("AOW_WorkFlow", $params[$key][$index]);
			return $focus->name;
		}
		
		if ($subIndex > -1)
			return $params[$key][$index][$subIndex];

		return $params[$key][$index];
	}

    function run_action(SugarBean $bean, $params = array(), $in_save=false){
        global $beanList;
		if(isset($params['record_type']) && $params['record_type'] != ''){
			if($beanList[$params['record_type']]){
				$record = new $beanList[$params['record_type']]();
				$record->name = $bean->name . " - scheduled Workflow";
				$record->parent_type = $params['value'][0];
				$record->parent_id = $bean->id;
                $record->aow_workflow_id_c = $params['value'][1];
				$record->trigger_time_c = $this->getTriggerTime($params, $bean, $record);

				global $current_user;
				$record->current_user_id_c = $current_user->id;

				$record->save();
				
                $this->set_relationships($record, $bean, $params);

                if(isset($params['relate_to_workflow']) && $params['relate_to_workflow']){
                    require_once('modules/Relationships/Relationship.php');
                    $key = Relationship::retrieve_by_modules($bean->module_dir, $record->module_dir, $GLOBALS['db']);
                    if (!empty($key)) {
                        foreach($bean->field_defs as $field=>$def){
                            if($def['type'] == 'link' && !empty($def['relationship']) && $def['relationship'] == $key){
                                $bean->load_relationship($field);
                                $bean->$field->add($record->id);
                                break;
                            }
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
	
	private function getTriggerTime($params, $bean, $record) {
		global $app_list_strings, $timedate;
		$value = date("Y-m-d H:i:s");
        $record_vardefs = $record->getFieldDefinitions();
		$dformat = 'Y-m-d H:i:s';
		
		$key = 2;
		
		// From the core
		switch($params['value'][$key][3]) {
			case 'business_hours';
				if(file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php')){
					require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

					$businessHours = new AOBH_BusinessHours();

					$dateToUse = $params['value'][$key][0];
					$sign = $params['value'][$key][1];
					$amount = $params['value'][$key][2];

					if($sign != "plus"){
						$amount = 0 - $amount;
					}
					if($dateToUse == "now"){
						$value = $businessHours->addBusinessHours($amount);
					}else if($dateToUse == "field"){
						$dateToUse = $params['field'][$key];
						$value = $businessHours->addBusinessHours($amount, $timedate->fromDb($bean->$dateToUse));
					}else{
						$value = $businessHours->addBusinessHours($amount, $timedate->fromDb($bean->$dateToUse));
					}
					$value = $timedate->asDb( $value );
					break;
				}
				$params['value'][$key][3] = 'hours';
			//No business hours module found - fall through.
			default:
				if($params['value'][$key][0] == 'now'){
					$date = gmdate($dformat);
				} else if($params['value'][$key][0] == 'field'){
					$date = $record->fetched_row[$params['field'][$key]];
				} else if ($params['value'][$key][0] == 'today') {
					$date = $params['value'][$key][0];
				} else {
					$date = $bean->fetched_row[$params['value'][$key][0]];
				}

				if($params['value'][$key][1] != 'now'){
					$value = date($dformat, strtotime($date . ' '.$app_list_strings['aow_date_operator'][$params['value'][$key][1]].$params['value'][$key][2].' '.$params['value'][$key][3]));
				} else {
					$value = date($dformat, strtotime($date));
				}
				break;
		}
		
		return $value;
	}

}