<?php

require_once 'custom/modules/AOS_PDF_Templates/templateParser.php';


class CustomAowTemplateParser extends CustomTemplateParser {

	static function parse_template($string, $bean_arr) {
		global $beanList;

		$person = array();
		
		$calcAccountId = "";

		foreach($bean_arr as $bean_name => $bean_id) {
			$focus = BeanFactory::getBean($bean_name, $bean_id);

			if ($bean_name == 'Cases') {
				global $db;
				
				$sql = "SELECT account_id
						FROM accounts_contacts
						JOIN accounts ON accounts.id = account_id AND accounts.deleted = 0
						WHERE contact_id = " . $db->quoted($focus->contact_created_by_id) . " AND
						accounts_contacts.deleted = 0";
						
				$res = $db->fetchOne($sql);

				if (!empty($res) && isset($res['account_id']))
					$calcAccountId = $res['account_id'];
			}
			
			$string = CustomAowTemplateParser::parse_template_bean($string, strtolower($beanList[$bean_name]), $focus, $calcAccountId);

			if($focus instanceof Person){
				$person[] = $focus;
			}
		}

		if(!empty($person)){
			$focus = $person[0];
		} else {
			$focus = new Contact();
		}
		$string = CustomAowTemplateParser::parse_template_bean($string, 'contact', $focus);

		return $string;
	}
}
?>
