<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-11-01 02:15:14
$dictionary["Task"]["fields"]["is1_inside_sales_tasks_1"] = array (
  'name' => 'is1_inside_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'IS1_Inside_Sales',
  'bean_name' => 'IS1_Inside_Sales',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_IS1_INSIDE_SALES_TITLE',
  'id_name' => 'is1_inside_sales_tasks_1is1_inside_sales_ida',
);
$dictionary["Task"]["fields"]["is1_inside_sales_tasks_1_name"] = array (
  'name' => 'is1_inside_sales_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_IS1_INSIDE_SALES_TITLE',
  'save' => true,
  'id_name' => 'is1_inside_sales_tasks_1is1_inside_sales_ida',
  'link' => 'is1_inside_sales_tasks_1',
  'table' => 'is1_inside_sales',
  'module' => 'IS1_Inside_Sales',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["is1_inside_sales_tasks_1is1_inside_sales_ida"] = array (
  'name' => 'is1_inside_sales_tasks_1is1_inside_sales_ida',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_TASKS_TITLE',
);


 // created: 2017-11-16 19:01:07
$dictionary['Task']['fields']['status']['inline_edit']=true;
$dictionary['Task']['fields']['status']['merge_filter']='disabled';

 

// created: 2017-10-18 12:46:47
$dictionary["Task"]["fields"]["p1_project_tasks_1"] = array (
  'name' => 'p1_project_tasks_1',
  'type' => 'link',
  'relationship' => 'p1_project_tasks_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_P1_PROJECT_TITLE',
  'id_name' => 'p1_project_tasks_1p1_project_ida',
);
$dictionary["Task"]["fields"]["p1_project_tasks_1_name"] = array (
  'name' => 'p1_project_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_P1_PROJECT_TITLE',
  'save' => true,
  'id_name' => 'p1_project_tasks_1p1_project_ida',
  'link' => 'p1_project_tasks_1',
  'table' => 'p1_project',
  'module' => 'P1_Project',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["p1_project_tasks_1p1_project_ida"] = array (
  'name' => 'p1_project_tasks_1p1_project_ida',
  'type' => 'link',
  'relationship' => 'p1_project_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_TASKS_TITLE',
);


// created: 2017-11-01 02:12:01
$dictionary["Task"]["fields"]["os1_outside_sales_activity_tasks_1"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1',
  'type' => 'link',
  'relationship' => 'os1_outside_sales_activity_tasks_1',
  'source' => 'non-db',
  'module' => 'OS1_Outside_Sales_Activity',
  'bean_name' => 'OS1_Outside_Sales_Activity',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
  'id_name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
);
$dictionary["Task"]["fields"]["os1_outside_sales_activity_tasks_1_name"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
  'save' => true,
  'id_name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
  'link' => 'os1_outside_sales_activity_tasks_1',
  'table' => 'os1_outside_sales_activity',
  'module' => 'OS1_Outside_Sales_Activity',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
  'type' => 'link',
  'relationship' => 'os1_outside_sales_activity_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_TASKS_TITLE',
);


 // created: 2017-11-17 17:46:16
$dictionary['Task']['fields']['name']['len']='255';
$dictionary['Task']['fields']['name']['inline_edit']=true;
$dictionary['Task']['fields']['name']['merge_filter']='disabled';
$dictionary['Task']['fields']['name']['unified_search']=false;
$dictionary['Task']['fields']['name']['full_text_search']=array (
);

 
?>