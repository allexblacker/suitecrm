<?php
// created: 2018-02-26 14:05:59
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'accounts_onb_onboarding_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_ONB_ONBOARDING_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_onb_onboarding_1accounts_ida',
  ),
  'contact' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_CONTACT',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contact_id_c',
  ),
  'role' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_ROLE',
    'width' => '10%',
    'default' => true,
  ),
  'onboarding_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_ONBOARDING_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'onboarding_call' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_ONBOARDING_CALL',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'ONB_Onboarding',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'ONB_Onboarding',
    'width' => '5%',
    'default' => true,
  ),
);