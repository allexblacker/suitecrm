<?php
$module_name = 'ONB_Onboarding';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'contact',
            'studio' => 'visible',
            'label' => 'LBL_CONTACT',
          ),
          1 => 
          array (
            'name' => 'accounts_onb_onboarding_1_name',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'role',
            'studio' => 'visible',
            'label' => 'LBL_ROLE',
          ),
          1 => 
          array (
            'name' => 'summary_next_steps',
            'studio' => 'visible',
            'label' => 'LBL_SUMMARY_NEXT_STEPS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'onboarding_status',
            'studio' => 'visible',
            'label' => 'LBL_ONBOARDING_STATUS',
          ),
          1 => 
          array (
            'name' => 'onboarding_call',
            'studio' => 'visible',
            'label' => 'LBL_ONBOARDING_CALL',
          ),
        ),
      ),
    ),
  ),
);
?>
