<?php 
 //WARNING: The contents of this file are auto-generated


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';
$mod_strings['LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE'] = 'POS Tracking';
$mod_strings['LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE'] = 'Competitive Intelligence';
$mod_strings['LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ONB_ONBOARDING_TITLE'] = 'Onboarding';
$mod_strings['LBL_ACCOUNTS_SPA_SPA_1_FROM_SPA_SPA_TITLE'] = 'SPA';



$mod_strings['LBL_FUNCTION_LOYALTY_START'] = 'Alliance Plan start date';
$mod_strings['LBL_FUNCTION_BALANCE'] = 'Alliance Balance';
$mod_strings['LBL_FUNCTION_TOTAL_KW'] = 'Total KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2013'] = 'Year 2013 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2014'] = 'Year 2014 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2015'] = 'Year 2015 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2016'] = 'Year 2016 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2017'] = 'Year 2017 KW';
$mod_strings['LBL_FUNCTION_YEAR_KW_2018'] = 'Year 2018 KW';
$mod_strings['LBL_FUNCTION_EXTRA'] = 'Extra';
$mod_strings['LBL_FUNCTION_UNIQUE_SITES'] = 'No of unique sites';
$mod_strings['LBL_FUNCTION_LAST_TRANSACTION'] = 'Last Alliance Transaction';
$mod_strings['LBL_FUNCTION_LAST_POS_TRANSACTION'] = 'Last POS transaction';
$mod_strings['LBL_FUNCTION_OF_OPPORTUNITIES'] = '# of Opportunities';
$mod_strings['LBL_FUNCTION_CHECKIFFIRSTOPP'] = 'CheckIfFirstOpp';
$mod_strings['LBL_FUNCTION_SERVICE_KIT'] = 'Service Kit';
$mod_strings['LBL_FUNCTION_NEW_ACCOUNT_BUTTON'] = 'New Account button';

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';
$mod_strings['LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE'] = 'POS Tracking';
$mod_strings['LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE'] = 'Competitive Intelligence';







$app_list_strings['accountstates'][''] = '';
$app_list_strings['accountstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['accountstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['accountstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['accountstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['accountstates']['Unitedstates_California'] = 'California';
$app_list_strings['accountstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['accountstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['accountstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['accountstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['accountstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['accountstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['accountstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['accountstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['accountstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['accountstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['accountstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['accountstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['accountstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['accountstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['accountstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['accountstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['accountstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['accountstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['accountstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['accountstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['accountstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['accountstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['accountstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['accountstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['accountstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['accountstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['accountstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['accountstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['accountstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['accountstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['accountstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['accountstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['accountstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['accountstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['accountstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['accountstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['accountstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['accountstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['accountstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['accountstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['accountstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['accountstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['accountstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['accountstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['accountstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['accountstates']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['accountstates']['Canada_Alberta'] = 'Alberta';
$app_list_strings['accountstates']['Canada_British Columbia '] = 'British Columbia ';
$app_list_strings['accountstates']['Canada_Manitoba '] = 'Manitoba ';
$app_list_strings['accountstates']['Canada_NewBrunswick'] = 'New Brunswick';
$app_list_strings['accountstates']['Canada_NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['accountstates']['Canada_Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['accountstates']['Canada_Nunavut'] = 'Nunavut';
$app_list_strings['accountstates']['Canada_Ontario'] = 'Ontario';
$app_list_strings['accountstates']['Canada_PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['accountstates']['Canada_Quebec'] = 'Quebec';
$app_list_strings['accountstates']['Canada_Saskatchewan'] = 'Saskatchewan';
$app_list_strings['accountstates']['Canada_Yukon'] = 'Yukon';



//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';
$mod_strings['LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE'] = 'POS Tracking';
$mod_strings['LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE'] = 'Competitive Intelligence';
$mod_strings['LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';
$mod_strings['LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE'] = 'POS Tracking';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';

 
 //WARNING: The contents of this file are auto-generated


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_INS_SALES'] = 'Inside Sales';
$mod_strings['LBL_OUTS_SALES'] = 'Outside Sales Activity';
$mod_strings['LBL_ACCOUNT_SITE'] = 'Sites';
$mod_strings['LBL_FUNCTION_FIRST_INSIDE_CONV'] = 'First Inside Sales Conv.';
$mod_strings['LBL_FUNCTION_LAST_INSIDE_CONV'] = 'Last Inside Sales Conv.';
$mod_strings['LBL_FIRST_INSIDE_CONV_DATE'] = 'First Inside Sales Conv.';
$mod_strings['LBL_LAST_INSIDE_CONV_DATE'] = 'Last Inside Sales Conv.';
$mod_strings['LBL_FIRST_ONBOARDING_DATE'] = 'First Onboarding Intro Date';



 

$mod_strings["LBL_LISTVIEW_GROUP"] = "Security Group";
$mod_strings["LBL_LISTVIEW_ENVELOPE"] = " ";
$mod_strings['LBL_SEARCHFORM_SECURITY_GROUP'] = 'Security Group';

// Kobi
$mod_strings['LBL_FIRST_INSIDE_CONV_DATE'] = 'First Inside Sales Conv.';
$mod_strings['LBL_LAST_INSIDE_CONV_DATE'] = 'Last Inside Sales Conv.';
$mod_strings['LBL_FIRST_ONBOARDING_DATE'] = 'First Onboarding Intro Date';



//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';
$mod_strings['LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE'] = 'POS Tracking';
$mod_strings['LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE'] = 'Competitive Intelligence';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_S1_SITE_1_FROM_S1_SITE_TITLE'] = 'Site';
$mod_strings['LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE'] = 'Outside Sales Activity';
$mod_strings['LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE'] = 'Alliance Transaction';
$mod_strings['LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE'] = 'Year Alliance Points';
$mod_strings['LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE'] = 'Project';
$mod_strings['LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE'] = 'Field Service Kit';
$mod_strings['LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE'] = 'POS Tracking';
$mod_strings['LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE'] = 'Competitive Intelligence';
$mod_strings['LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE'] = 'Inside Sales';
$mod_strings['LBL_ACCOUNTS_ONB_ONBOARDING_1_FROM_ONB_ONBOARDING_TITLE'] = 'Onboarding';

?>