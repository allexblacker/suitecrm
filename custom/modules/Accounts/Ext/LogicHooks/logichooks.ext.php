<?php 
 //WARNING: The contents of this file are auto-generated



$hook_array['before_save'][] = Array(
    100,
    'Update USA Zip Code field with the value of Zip Postal Code in case of Account Country = United States AND USA Zip Code is blank AND Zip Postal Code is not blank',
    'custom/include/dtbc/hooks/accountsLogicHooks.php',
    'AccountsLogicHooks',
    'updateUSAZipCode'
);


$hook_array['before_save'][] = array(
	1,
	'Fill all the formula fields on Accounts',
	'custom/include/dtbc/formulafields/formulafields_accounts.php',
	'FormulaFieldsAccounts',
	'before_save'
);


$order = 15;
$description = 'Setting last alliance transaction';
$filePath = 'custom/include/dtbc/hooks/accountsLogicHooks.php';
$className = "AccountsLogicHooks";
$functionName = "updateAllianceTransactionsFields";

$hookArray = array($order, $description, $filePath, $className, $functionName);

$hook_array['after_relationship_delete'][] = $hookArray;
$hook_array['after_relationship_add'][] = $hookArray;



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$hook_array['after_save'][] = Array(89, 'UpdateZipDetails', 'custom/modules/Accounts/acctCustomLogicHooks.php','AccountsCustomLogicHooks', 'updateAcctZipDetailsLogicHooks');
?>