<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getInsideSales($params) {
    $db = DBManagerFactory::getInstance();

    $sql = "SELECT is1_inside_sales.*
            FROM is1_inside_sales
            JOIN is1_inside_sales_cstm ON is1_inside_sales.id = is1_inside_sales_cstm.id_c AND account_id_c = " . $db->quoted($params['account_id']) . 
           "JOIN accounts ON is1_inside_sales_cstm.account_id_c = accounts.id AND accounts.deleted = 0
            WHERE is1_inside_sales.deleted = 0";

    return $sql;
}

function getOutsideSales($params) {
    $db = DBManagerFactory::getInstance();

    $sql = "SELECT os1_outside_sales_activity.*
            FROM os1_outside_sales_activity
            JOIN os1_outside_sales_activity_cstm ON os1_outside_sales_activity.id = os1_outside_sales_activity_cstm.id_c AND account_id_c = " . $db->quoted($params['account_id']) .
           "JOIN accounts ON os1_outside_sales_activity_cstm.account_id_c = accounts.id AND accounts.deleted = 0
            WHERE os1_outside_sales_activity.deleted = 0";

    return $sql;
}

function getAccountSites($params) {
    $db = DBManagerFactory::getInstance();

    $sql = "SELECT s1_site.*
            FROM s1_site
            WHERE s1_site.deleted = 0 and s1_site.account_id_c = " . $db->quoted($_REQUEST['record']);
    return $sql;
}
