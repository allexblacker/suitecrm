<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/Accounts/views/view.edit.php');
require_once('custom/include/EditView/EditViewCustomCache.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");

class CustomAccountsViewEdit extends AccountsViewEdit {

    function __construct(){
        parent::__construct();
    }
	
	function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();
		
		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

        $this->ev = $this->getEditView();
        $this->ev->ss =& $this->ss;
        $this->ev->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/EditView/EditView.tpl'));
	}
	
	protected function getEditView() {
        return new EditViewCustomCache();
    }
	
}
