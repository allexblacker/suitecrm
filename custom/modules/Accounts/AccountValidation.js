/**
 * Created by sivan.h on 07/08/2017.
 */


$(document).ready(function(){
    function addAccountsCustomValidation(){
        addToValidate('EditView','installer_c','TextField',true,"Installer");
    }
    function removeAccountsCustomValidation(){
        removeFromValidate('EditView','installer_c');
    }
    function accountTypeChanged(){
        var val = $('#account_type').val();
        switch(val){
            case 'System_Owner':

                addAccountsCustomValidation();
                break;
            default:
                removeAccountsCustomValidation();
                break;
        }
    }
    $('#account_type').change(accountTypeChanged);
    accountTypeChanged();


/*
    function blankTypeChanged(){
        var valInstaller = $('#installer_c').val();
        var val = $('#account_type').val();

        if (valInstaller != '' && val != 'System_Owner') {

            alert("Installer only needed when the Account is from System Owner type.")

        }
    }
    $('#installer_c').change(blankTypeChanged);
    blankTypeChanged();


*/


});

