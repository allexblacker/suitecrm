<?php
$viewdefs ['Accounts'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          'AOS_GENLET' => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_GENERATE_LETTER}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'inside_sales_owner_c',
            'studio' => 'visible',
            'label' => 'INSIDE_SALES_OWNER',
          ),
          1 => 
          array (
            'name' => 'phone_c',
            'label' => 'PHONE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'comment' => 'Name of the Company',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'parent_account_c',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_ACCOUNT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'phone_alternate',
            'comment' => 'An alternate phone number',
            'label' => 'LBL_PHONE_ALT',
          ),
          1 => 
          array (
            'name' => 'phone_office',
            'comment' => 'The office phone number',
            'label' => 'LBL_PHONE_OFFICE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'checkiffirstopp_c',
            'label' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'rating',
            'comment' => 'An arbitrary rating for this company for use in comparisons with others',
            'label' => 'LBL_RATING',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'days_without_alliance_transa_c',
            'label' => 'LBL_DAYS_WITHOUT_ALLIANCE_TRANSA',
          ),
          1 => 
          array (
            'name' => 'pos_last_transaction_y_and_q_c',
            'label' => 'LBL_POS_LAST_TRANSACTION_Y_AND_Q',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
          1 => 
          array (
            'name' => 'distribution_partners_c',
            'label' => 'DISTRIBUTION_PARTNERS',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'vip_customer_c',
            'label' => 'LBL_VIP_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'financing_partners_c',
            'studio' => 'visible',
            'label' => 'FINANCING_PARTNERS',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'certified_installer_tesla_c',
            'studio' => 'visible',
            'label' => 'CERTIFIED_INSTALLER_TESLA',
          ),
          1 => 
          array (
            'name' => 'solaredge_optimized_partner_c',
            'label' => 'SOLAREDGE_OPTIMIZED_PARTNER',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'exclude_from_forecast_c',
            'label' => 'EXCLUDE_FROM_FORECAST',
          ),
          1 => 
          array (
            'name' => 'active_spa_c',
            'studio' => 'visible',
            'label' => 'ACTIVE_SPA',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'timestamp_recalculate_c',
            'label' => 'TIMESTAMP_RECALCULATE_ALLIANCE_POINTS',
          ),
          1 => 
          array (
            'name' => 'tier_c',
            'studio' => 'visible',
            'label' => 'TIER',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'us_county_c',
            'label' => 'LBL_US_COUNTY',
          ),
          1 => 
          array (
            'name' => 'tax_id_c',
            'label' => 'TAX_ID',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'vendor_name_c',
            'label' => 'VENDOR_NAME',
          ),
          1 => 
          array (
            'name' => 'vendor_number_c',
            'label' => 'VENDOR_NUMBER',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'account_type',
            'comment' => 'The Company is of this type',
            'label' => 'LBL_TYPE',
          ),
          1 => 
          array (
            'name' => 'segment_c',
            'studio' => 'visible',
            'label' => 'SEGMENT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'website',
            'type' => 'link',
            'label' => 'LBL_WEBSITE',
            'displayParams' => 
            array (
              'link_target' => '_blank',
            ),
          ),
          1 => 
          array (
            'name' => 'enphase_c',
            'label' => 'ENPHASE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'avg_proj_kw_c',
            'label' => 'AVG_SIZE_OF_PROJECT',
          ),
          1 => 
          array (
            'name' => 'sunpower_c',
            'label' => 'SUNPOWER',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'kw_per_month_resi_com_c',
            'label' => 'KW_PER_MONTH_RESI_COM',
          ),
          1 => 
          array (
            'name' => 'sunrun_c',
            'label' => 'SUNRUN',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'crews_c',
            'studio' => 'visible',
            'label' => 'CREWS',
          ),
          1 => 
          array (
            'name' => 'last_inside_conv_c',
            'label' => 'LBL_LAST_INSIDE_CONV_DATE',
            'studio' => 'visible',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'project_month_c',
            'label' => 'PROJECT_MONTH',
          ),
          1 => 
          array (
            'name' => 'first_inside_conv_c',
            'label' => 'LBL_FIRST_INSIDE_CONV_DATE',
            'studio' => 'visible',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'sedg_c',
            'label' => 'SEDG',
          ),
          1 => 
          array (
            'name' => 'first_onboard_c',
            'label' => 'LBL_FIRST_ONBOARDING_DATE',
            'studio' => 'visible',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'inverter_competitors_c',
            'studio' => 'visible',
            'label' => 'INVERTER_COMPETITORS',
          ),
          1 => 
          array (
            'name' => 'onboarding_category_c',
            'studio' => 'visible',
            'label' => 'LBL_ONBOARDING_CATEGORY_C',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'customer_funnel_c',
            'studio' => 'visible',
            'label' => 'CUSTOMER_FUNNEL',
          ),
          1 => 
          array (
            'name' => 'training_type_c',
            'studio' => 'visible',
            'label' => 'LBL_TRAINING_TYPE_C',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'inactive_reason_c',
            'studio' => 'visible',
            'label' => 'INACTIVE_REASON',
          ),
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'marketing_comments_c',
            'studio' => 'visible',
            'label' => 'MARKETING_COMMENTS',
          ),
          1 => '',
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'inside_sales_comments_c',
            'label' => 'INSIDE_SALES_COMMENTS',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'street_c',
            'label' => 'STREET',
          ),
          1 => 
          array (
            'name' => 'zip_postal_code_c',
            'label' => 'ZIP_POSTAL_CODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'city_c',
            'label' => 'CITY',
          ),
          1 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'COUNTRY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE',
          ),
          1 => 
          array (
            'name' => 'region_c',
            'studio' => 'visible',
            'label' => 'REGION',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'us_region_c',
            'studio' => 'visible',
            'label' => 'US_REGION',
          ),
          1 => 
          array (
            'name' => 'usa_zip_code_c',
            'studio' => 'visible',
            'label' => 'LBL_USA_ZIP_CODE',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_street',
            'label' => 'LBL_SHIPPING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'shipping',
            ),
          ),
          1 => 
          array (
            'name' => 'shipping_address_postalcode',
            'comment' => 'The zip code used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_city',
            'comment' => 'The city used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_CITY',
          ),
          1 => 
          array (
            'name' => 'shipping_address_state',
            'comment' => 'The state used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_STATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_country',
            'comment' => 'The country used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'Loyalty_Plan_start_date__c',
            'label' => 'LBL_FUNCTION_LOYALTY_START',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'No_of_unique_sites__c',
            'label' => 'LBL_FUNCTION_UNIQUE_SITES',
            'studio' => 'visible',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'KW__c',
            'label' => 'LBL_FUNCTION_TOTAL_KW',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'Last_Alliance_Transaction__c',
            'label' => 'LBL_FUNCTION_LAST_TRANSACTION',
            'studio' => 'visible',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'year_2018_kw_c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2018',
            'studio' => 'visible',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'Year_2017_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2017',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'recalculate_alliance_c',
            'label' => 'RECALCULATE_ALLIANCE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'Year_2016_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2016',
            'studio' => 'visible',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'Year_2015_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2015',
            'studio' => 'visible',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'Year_2014_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2014',
            'studio' => 'visible',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'Year_2013_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2013',
            'studio' => 'visible',
          ),
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'allow_alliance_plan_email_c',
            'label' => 'LBL_ALLOW_ALLIANCE_PLAN_EMAIL',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
