<?php
$listViewDefs ['Accounts'] = 
array (
  'NAME' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
  ),
  'COUNTRY_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'COUNTRY',
    'width' => '10%',
  ),
  'STATE_C' => 
  array (
    'type' => 'dynamicenum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATE',
    'width' => '10%',
  ),
  'CITY_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'CITY',
    'width' => '10%',
  ),
  'PHONE_OFFICE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_PHONE',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'width' => '5%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
  'ACCOUNT_TYPE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_TYPE',
    'default' => false,
  ),
  'INDUSTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_INDUSTRY',
    'default' => false,
  ),
  'ANNUAL_REVENUE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ANNUAL_REVENUE',
    'default' => false,
  ),
  'SERVICE_LEVEL_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'SERVICE_LEVEL',
    'width' => '10%',
  ),
  'SE_TRAINING_VISITED_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'SE_TRAINING_VISITED',
    'width' => '10%',
  ),
  'REGION_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'REGION',
    'width' => '10%',
  ),
  'SEGMENT_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'SEGMENT',
    'width' => '10%',
  ),
  'TRAINING_TIER_1_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'TRAINING_TIER_1_DATE',
    'width' => '10%',
  ),
  'SALES_MANAGER_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SALES_MANAGER',
    'width' => '10%',
  ),
  'RECALCULATE_ALLIANCE_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'RECALCULATE_ALLIANCE',
    'width' => '10%',
  ),
  'EXCLUDE_FROM_FORECAST_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'EXCLUDE_FROM_FORECAST',
    'width' => '10%',
  ),
  'INACTIVE_REASON_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'INACTIVE_REASON',
    'width' => '10%',
  ),
  'FINANCING_PARTNERS_C' => 
  array (
    'type' => 'multienum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'FINANCING_PARTNERS',
    'width' => '10%',
  ),
  'PHONE_FAX' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PHONE_FAX',
    'default' => false,
  ),
  'CUSTOMER_NUMBER_IN_PRIORITY_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'CUSTOMER_NUMBER_IN_PRIORITY',
    'width' => '10%',
  ),
  'AVG_PROJ_KW_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'AVG_SIZE_OF_PROJECT',
    'width' => '10%',
  ),
  'BILLING_ADDRESS_STREET' => 
  array (
    'width' => '15%',
    'label' => 'LBL_BILLING_ADDRESS_STREET',
    'default' => false,
  ),
  'BILLING_ADDRESS_STATE' => 
  array (
    'width' => '7%',
    'label' => 'LBL_BILLING_ADDRESS_STATE',
    'default' => false,
  ),
  'BILLING_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'CANADIAN_PROVINCE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'CANADIAN_PROVINCE',
    'width' => '10%',
  ),
  'BILLING_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_CITY',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STREET' => 
  array (
    'width' => '15%',
    'label' => 'LBL_SHIPPING_ADDRESS_STREET',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_CITY',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STATE' => 
  array (
    'width' => '7%',
    'label' => 'LBL_SHIPPING_ADDRESS_STATE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'RATING' => 
  array (
    'width' => '10%',
    'label' => 'LBL_RATING',
    'default' => false,
  ),
  'PHONE_ALTERNATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_OTHER_PHONE',
    'default' => false,
  ),
  'WEBSITE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_WEBSITE',
    'default' => false,
  ),
  'OWNERSHIP' => 
  array (
    'width' => '10%',
    'label' => 'LBL_OWNERSHIP',
    'default' => false,
  ),
  'EMPLOYEES' => 
  array (
    'width' => '10%',
    'label' => 'LBL_EMPLOYEES',
    'default' => false,
  ),
  'SIC_CODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SIC_CODE',
    'default' => false,
  ),
  'TICKER_SYMBOL' => 
  array (
    'width' => '10%',
    'label' => 'LBL_TICKER_SYMBOL',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'width' => '5%',
    'label' => 'LBL_DATE_MODIFIED',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_CREATED',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_MODIFIED',
    'default' => false,
  ),
  'OP_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'SURVEY_50_DATE',
    'width' => '10%',
  ),
  'PROJECT_MONTH_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'PROJECT_MONTH',
    'width' => '10%',
  ),
  'CREWS_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'CREWS',
    'width' => '10%',
  ),
  'TARGET_KW_C' => 
  array (
    'type' => 'decimal',
    'default' => false,
    'label' => 'TARGET_KW',
    'width' => '10%',
  ),
  'SEDG_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SEDG',
    'width' => '10%',
  ),
  'TAX_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'TAX_ID',
    'width' => '10%',
  ),
  'ACCOUNT_TARGET_C' => 
  array (
    'type' => 'decimal',
    'default' => false,
    'label' => 'ACCOUNT_TARGET',
    'width' => '10%',
  ),
  'ACCOUNT_MONITORING_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'ACCOUNT_MONITORING_ID',
    'width' => '10%',
  ),
  'ACTIVE_SPA_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'ACTIVE_SPA',
    'width' => '10%',
  ),
  'SURVEY_10_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SURVEY_10',
    'width' => '10%',
  ),
  'STREET_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'STREET',
    'width' => '10%',
  ),
  'BILLING_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'BEST_SELLING_INVERTER_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'BEST_SELLING_INVERTER',
    'width' => '10%',
  ),
  'KW_FACTOR__C' => 
  array (
    'type' => 'int',
    'default' => false,
    'label' => 'LBL_KW_FACTOR__C',
    'width' => '10%',
  ),
  'CERTIFIED_INSTALLER_TESLA_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'CERTIFIED_INSTALLER_TESLA',
    'width' => '10%',
  ),
  'INVERTER_COMPETITORS_C' => 
  array (
    'type' => 'multienum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'INVERTER_COMPETITORS',
    'width' => '10%',
  ),
  'INVERTER_BRANDS_C' => 
  array (
    'type' => 'multienum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'INVERTER_BRANDS',
    'width' => '10%',
  ),
  'INSTALLER_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'INSTALLER',
    'width' => '10%',
  ),
  'INSIDE_SALES_COMMENTS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'INSIDE_SALES_COMMENTS',
    'width' => '10%',
  ),
  'CSR_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'CSR',
    'width' => '10%',
  ),
  'EMAIL1' => 
  array (
    'width' => '15%',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'link' => true,
    'customCode' => '{$EMAIL1_LINK}{$EMAIL1}</a>',
    'default' => false,
  ),
  'DISTRIBUTION_PARTNERS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'DISTRIBUTION_PARTNERS',
    'width' => '10%',
  ),
  'BUSINESS_ENTITY_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'BUSINESS_ENTITY',
    'width' => '10%',
  ),
  'NUMBER_OF_SOP_EMPLOYEES_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'NUMBER_OF_SOP_EMPLOYEES',
    'width' => '10%',
  ),
  'AUSTRALIAN_STATE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'AUSTRALIAN_STATE',
    'width' => '10%',
  ),
  'FIRST_INSTALL_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'FIRST_INSTALL',
    'width' => '10%',
  ),
  'SOLAREDGE_OPTIMIZED_PARTNER_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'SOLAREDGE_OPTIMIZED_PARTNER',
    'width' => '10%',
  ),
  'TRAINING_TIER_2_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'TRAINING_TIER_2_DATE',
    'width' => '10%',
  ),
  'FIRST_INSTALL_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'FIRST_INSTALL_DATE',
    'width' => '10%',
  ),
  'MASS_UPDATE_ADMIN_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'MASS_UPDATE_ADMIN',
    'width' => '10%',
  ),
  'OTHER_MODULE_BRANDS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'OTHER_MODULE_BRANDS',
    'width' => '10%',
  ),
  'OUTSIDE_SALES_APPT_SET_BY_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'OUTSIDE_SALES_APPT_SET_BY_IST',
    'width' => '10%',
  ),
  'NOT_CURRENTLY_SATISFIED_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'NOT_CURRENTLY_SATISFIED',
    'width' => '10%',
  ),
  'OTHER_INVERTER_BRANDS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'OTHER_INVERTER_BRANDS',
    'width' => '10%',
  ),
  'ZIP_POSTAL_CODE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'ZIP_POSTAL_CODE',
    'width' => '10%',
  ),
  'WELCOME_KIT_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'WELCOME_KIT_DATE',
    'width' => '10%',
  ),
  'US_REGION_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'US_REGION',
    'width' => '10%',
  ),
  'NEXT_YEAR_TARGET_KWP_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'NEXT_YEAR_TARGET_KWP',
    'width' => '10%',
  ),
  'VENDOR_NUMBER_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'VENDOR_NUMBER',
    'width' => '10%',
  ),
  'WELCOME_KIT_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'WELCOME_KIT',
    'width' => '10%',
  ),
  'NEXT_YEAR_TOTAL_SYSTEMS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'NEXT_YEAR_TOTAL_SYSTEMS_TARGET',
    'width' => '10%',
  ),
  'MONITORING_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'MONITORING_ID',
    'width' => '10%',
  ),
  'MODULE_SHARE_IN_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'MODULE_SHARE_IN',
    'width' => '10%',
  ),
  'SURVEY_01_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'SURVEY_01_DATE',
    'width' => '10%',
  ),
  'SURVEY_01_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SURVEY_01',
    'width' => '10%',
  ),
  'SURVEY_100_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SURVEY_100',
    'width' => '10%',
  ),
  'SUPPORT_COMMENTS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SUPPORT_COMMENTS',
    'width' => '10%',
  ),
  'SUNPOWER_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'SUNPOWER',
    'width' => '10%',
  ),
  'SUNRUN_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'SUNRUN',
    'width' => '10%',
  ),
  'PROMOTION_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'PROMOTION_DATE',
    'width' => '10%',
  ),
  'PROMOTION_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'PROMOTION',
    'width' => '10%',
  ),
  'LAST_YEAR_INSTALLED_KWP_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LAST_YEAR_INSTALLED_KWP',
    'width' => '10%',
  ),
  'LAST_YEAR_TOT_INSTALLED_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LAST_YEAR_TOTAL_SYSTEMS_INSTALLED',
    'width' => '10%',
  ),
  'MAIN_PANEL_BRANDS_C' => 
  array (
    'type' => 'multienum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'MAIN_PANEL_BRANDS',
    'width' => '10%',
  ),
  'VENDOR_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'VENDOR_NAME',
    'width' => '10%',
  ),
  'KW_PER_MONTH_RESI_COM_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'KW_PER_MONTH_RESI_COM',
    'width' => '10%',
  ),
  'SURVEY_200_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'SURVEY_200_DATE',
    'width' => '10%',
  ),
  'SURVEY_200_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'SURVEY_200',
    'width' => '10%',
  ),
  'SURVEY_100_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'SURVEY_100_DATE',
    'width' => '10%',
  ),
);
?>
