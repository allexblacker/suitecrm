<?php
$viewdefs ['Accounts'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          'AOS_GENLET' => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_GENERATE_LETTER}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'account_owner_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_OWNER',
          ),
          1 => 
          array (
            'name' => 'exclude_from_forecast_c',
            'label' => 'EXCLUDE_FROM_FORECAST',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'csr_c',
            'studio' => 'visible',
            'label' => 'CSR',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'comment' => 'Name of the Company',
            'label' => 'LBL_NAME',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'account_currency_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'phone_c',
            'label' => 'PHONE',
          ),
        ),
        4 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'phone_fax',
            'comment' => 'The fax phone number of this company',
            'label' => 'LBL_FAX',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'sales_manager_c',
            'label' => 'SALES_MANAGER',
          ),
        ),
        6 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'account_monitoring_id_c',
            'label' => 'ACCOUNT_MONITORING_ID',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'service_level_c',
            'studio' => 'visible',
            'label' => 'SERVICE_LEVEL',
          ),
          1 => 
          array (
            'name' => 'target_kw_c',
            'label' => 'TARGET_KW',
          ),
        ),
        8 => 
        array (
          0 => '',
          1 => '',
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'marketing_comments_c',
            'studio' => 'visible',
            'label' => 'MARKETING_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'rating',
            'comment' => 'An arbitrary rating for this company for use in comparisons with others',
            'label' => 'LBL_RATING',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'support_comments_c',
            'label' => 'SUPPORT_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'vendor_number_c',
            'label' => 'VENDOR_NUMBER',
          ),
        ),
        11 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'vendor_name_c',
            'label' => 'VENDOR_NAME',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'account_target_c',
            'label' => 'ACCOUNT_TARGET',
          ),
          1 => '',
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'business_entity_c',
            'studio' => 'visible',
            'label' => 'BUSINESS_ENTITY',
          ),
          1 => '',
        ),
        14 => 
        array (
          0 => '',
          1 => '',
        ),
        15 => 
        array (
          0 => '',
          1 => '',
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'australian_state_c',
            'studio' => 'visible',
            'label' => 'AUSTRALIAN_STATE',
          ),
          1 => '',
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'canadian_province_c',
            'studio' => 'visible',
            'label' => 'CANADIAN_PROVINCE',
          ),
          1 => '',
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'not_currently_satisfied_c',
            'label' => 'NOT_CURRENTLY_SATISFIED',
          ),
          1 => '',
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
          1 => '',
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'next_year_target_kwp_c',
            'label' => 'NEXT_YEAR_TARGET_KWP',
          ),
          1 => '',
        ),
        21 => 
        array (
          0 => '',
          1 => '',
        ),
        22 => 
        array (
          0 => '',
          1 => '',
        ),
        23 => 
        array (
          0 => '',
          1 => '',
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'certified_installer_tesla_c',
            'studio' => 'visible',
            'label' => 'CERTIFIED_INSTALLER_TESLA',
          ),
          1 => '',
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'customer_number_in_priority_c',
            'label' => 'CUSTOMER_NUMBER_IN_PRIORITY',
          ),
          1 => '',
        ),
        26 => 
        array (
          0 => 
          array (
            'name' => 'customer_name_in_priority_c',
            'label' => 'CUSTOMER_NAME_IN_PRIORITY',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'distributor_status_c',
            'studio' => 'visible',
            'label' => 'DISTRIBUTOR_STATUS',
          ),
          1 => 
          array (
            'name' => 'other_inverter_brands_c',
            'label' => 'OTHER_INVERTER_BRANDS',
          ),
        ),
        2 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'module_share_in_c',
            'label' => 'MODULE_SHARE_IN',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'website',
            'type' => 'link',
            'label' => 'LBL_WEBSITE',
            'displayParams' => 
            array (
              'link_target' => '_blank',
            ),
          ),
          1 => 
          array (
            'name' => 'other_module_brands_c',
            'label' => 'OTHER_MODULE_BRANDS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'type_of_installation_c',
            'studio' => 'visible',
            'label' => 'TYPE_OF_INSTALLATION',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'activity_focus_c',
            'studio' => 'visible',
            'label' => 'ACTIVITY_FOCUS',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'employees',
            'comment' => 'Number of employees, varchar to accomodate for both number (100) or range (50-100)',
            'label' => 'LBL_EMPLOYEES',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => '',
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'decision_maker_c',
            'label' => 'DECISION_MAKER',
          ),
          1 => '',
        ),
        9 => 
        array (
          0 => '',
          1 => '',
        ),
        10 => 
        array (
          0 => '',
          1 => '',
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'inverter_brands_c',
            'studio' => 'visible',
            'label' => 'INVERTER_BRANDS',
          ),
          1 => 
          array (
            'name' => 'last_year_installed_kwp_c',
            'label' => 'LAST_YEAR_INSTALLED_KWP',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'best_selling_inverter_c',
            'studio' => 'visible',
            'label' => 'BEST_SELLING_INVERTER',
          ),
          1 => 
          array (
            'name' => 'last_year_tot_installed_c',
            'label' => 'LAST_YEAR_TOTAL_SYSTEMS_INSTALLED',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'installer_c',
            'label' => 'INSTALLER',
          ),
          1 => 
          array (
            'name' => 'next_year_total_systems_c',
            'label' => 'NEXT_YEAR_TOTAL_SYSTEMS_TARGET',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'main_panel_brands_c',
            'studio' => 'visible',
            'label' => 'MAIN_PANEL_BRANDS',
          ),
          1 => '',
        ),
        15 => 
        array (
          0 => '',
          1 => '',
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'account_type',
            'comment' => 'The Company is of this type',
            'label' => 'LBL_TYPE',
          ),
          1 => 
          array (
            'name' => 'se_training_visited_c',
            'studio' => 'visible',
            'label' => 'SE_TRAINING_VISITED',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'promotion_c',
            'label' => 'PROMOTION',
          ),
          1 => '',
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'annual_revenue',
            'comment' => 'Annual revenue for this company',
            'label' => 'LBL_ANNUAL_REVENUE',
          ),
          1 => '',
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'segment_c',
            'studio' => 'visible',
            'label' => 'SEGMENT',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'street_c',
            'label' => 'STREET',
          ),
          1 => 
          array (
            'name' => 'zip_postal_code_c',
            'label' => 'ZIP_POSTAL_CODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'city_c',
            'label' => 'CITY',
          ),
          1 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'COUNTRY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE',
          ),
          1 => 
          array (
            'name' => 'region_c',
            'studio' => 'visible',
            'label' => 'REGION',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_street',
            'label' => 'LBL_SHIPPING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'shipping',
            ),
          ),
          1 => 
          array (
            'name' => 'shipping_address_postalcode',
            'comment' => 'The zip code used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_city',
            'comment' => 'The city used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_CITY',
          ),
          1 => 
          array (
            'name' => 'shipping_address_state',
            'comment' => 'The state used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_STATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_country',
            'comment' => 'The country used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
        1 => 
        array (
          0 => '',
          1 => '',
        ),
        2 => 
        array (
          0 => '',
          1 => '',
        ),
        3 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'kw_factor__c',
            'label' => 'LBL_KW_FACTOR__C',
          ),
        ),
        4 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'recalculate_alliance_c',
            'label' => 'RECALCULATE_ALLIANCE',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'timestamp_recalculate_c',
            'label' => 'TIMESTAMP_RECALCULATE_ALLIANCE_POINTS',
          ),
        ),
        6 => 
        array (
          0 => '',
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'allow_alliance_plan_email_c',
            'label' => 'LBL_ALLOW_ALLIANCE_PLAN_EMAIL',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
      'lbl_editview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'crews_c',
            'studio' => 'visible',
            'label' => 'CREWS',
          ),
          1 => 
          array (
            'name' => 'of_opportunities_c',
            'label' => 'LBL_FUNCTION_OF_OPPORTUNITIES',
            'studio' => 'visible',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'project_month_c',
            'label' => 'PROJECT_MONTH',
          ),
          1 => 
          array (
            'name' => 'op_date_c',
            'label' => 'SURVEY_50_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'sedg_c',
            'label' => 'SEDG',
          ),
          1 => 
          array (
            'name' => 'service_lvl_is_missing_c',
            'label' => 'LBL_SERVICE_LVL_IS_MISSING',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'account_target_formula_c',
            'label' => 'LBL_ACCOUNT_TARGET_FORMULA',
          ),
          1 => 
          array (
            'name' => 'active_spa_c',
            'studio' => 'visible',
            'label' => 'ACTIVE_SPA',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_address_c',
            'label' => 'LBL_JJWG_MAPS_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'Balance__c',
            'label' => 'LBL_FUNCTION_BALANCE',
            'studio' => 'visible',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'Loyalty_Plan_start_date__c',
            'label' => 'LBL_FUNCTION_LOYALTY_START',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'allow_loyalty_plan_email_c',
            'label' => 'LBL_ALLOW_LOYALTY_PLAN_EMAIL',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'phone_alternate',
            'comment' => 'An alternate phone number',
            'label' => 'LBL_PHONE_ALT',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'avg_proj_kw_c',
            'label' => 'AVG_SIZE_OF_PROJECT',
          ),
          1 => 
          array (
            'name' => 'billing_address_city',
            'comment' => 'The city used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_CITY',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_country',
            'comment' => 'The country used for the billing address',
            'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'billing_address_postalcode',
            'comment' => 'The postal code used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_state',
            'comment' => 'The state used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_STATE',
          ),
          1 => 
          array (
            'name' => 'billing_address_street',
            'comment' => 'The street address used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_STREET',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'campaign_name',
            'comment' => 'The first campaign name for Account (Meta-data only)',
            'label' => 'LBL_CAMPAIGN',
          ),
          1 => 
          array (
            'name' => 'case_study__c',
            'label' => 'LBL_CASE_STUDY__C',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'case_study_date__c',
            'label' => 'LBL_CASE_STUDY_DATE__C',
          ),
          1 => 
          array (
            'name' => 'checkiffirstopp_c',
            'label' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
            'studio' => 'visible',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'county_c',
            'label' => 'COUNTY',
          ),
          1 => 
          array (
            'name' => 'customer_satisfaction_rating_c',
            'label' => 'LBL_CUSTOMER_SATISFACTION_RATING',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'days_without_alliance_transa_c',
            'label' => 'LBL_DAYS_WITHOUT_ALLIANCE_TRANSA',
          ),
          1 => 
          array (
            'name' => 'department_c',
            'label' => 'LBL_DEPARTMENT',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'distribution_partners_c',
            'label' => 'DISTRIBUTION_PARTNERS',
          ),
          1 => 
          array (
            'name' => 'enphase_c',
            'label' => 'ENPHASE',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'enphase__c',
            'label' => 'LBL_ENPHASE__C',
          ),
          1 => 
          array (
            'name' => 'Extra__c',
            'label' => 'LBL_FUNCTION_EXTRA',
            'studio' => 'visible',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'financing_partners_c',
            'studio' => 'visible',
            'label' => 'FINANCING_PARTNERS',
          ),
          1 => 
          array (
            'name' => 'first_install_date_c',
            'label' => 'FIRST_INSTALL_DATE',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_geocode_status_c',
            'label' => 'LBL_JJWG_MAPS_GEOCODE_STATUS',
          ),
          1 => 
          array (
            'name' => 'inactive_reason_c',
            'studio' => 'visible',
            'label' => 'INACTIVE_REASON',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'industry',
            'comment' => 'The company belongs in this industry',
            'label' => 'LBL_INDUSTRY',
          ),
          1 => 
          array (
            'name' => 'is1_inside_sales_accounts_1_name',
            'label' => 'LBL_IS1_INSIDE_SALES_ACCOUNTS_1_FROM_IS1_INSIDE_SALES_TITLE',
          ),
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'inside_sales_comments_c',
            'label' => 'INSIDE_SALES_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'inside_sales_owner_c',
            'studio' => 'visible',
            'label' => 'INSIDE_SALES_OWNER',
          ),
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'inverter_competitors_c',
            'studio' => 'visible',
            'label' => 'INVERTER_COMPETITORS',
          ),
          1 => 
          array (
            'name' => 'language_c',
            'label' => 'LBL_LANGUAGE',
          ),
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'Last_Alliance_Transaction__c',
            'label' => 'LBL_FUNCTION_LAST_TRANSACTION',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'last_pos_transaction_c',
            'label' => 'LBL_FUNCTION_LAST_POS_TRANSACTION',
            'studio' => 'visible',
          ),
        ),
        23 => 
        array (
          0 => 
          array (
            'name' => 'pos_last_transaction_y_and_q_c',
            'label' => 'LBL_POS_LAST_TRANSACTION_Y_AND_Q',
          ),
          1 => 
          array (
            'name' => 'jjwg_maps_lat_c',
            'label' => 'LBL_JJWG_MAPS_LAT',
          ),
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'location_c',
            'label' => 'LBL_LOCATION',
          ),
          1 => 
          array (
            'name' => 'jjwg_maps_lng_c',
            'label' => 'LBL_JJWG_MAPS_LNG',
          ),
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'marketing_comment_timestamp_c',
            'label' => 'MARKETING_COMMENT_TIMESTAMP',
          ),
          1 => 
          array (
            'name' => 'parent_name',
            'label' => 'LBL_MEMBER_OF',
          ),
        ),
        26 => 
        array (
          0 => 
          array (
            'name' => 'monitoring_id_c',
            'label' => 'MONITORING_ID',
          ),
          1 => 
          array (
            'name' => 'new_account_button_c',
            'label' => 'LBL_FUNCTION_NEW_ACCOUNT_BUTTON',
            'studio' => 'visible',
          ),
        ),
        27 => 
        array (
          0 => 
          array (
            'name' => 'No_of_unique_sites__c',
            'label' => 'LBL_FUNCTION_UNIQUE_SITES',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'number_of_sop_employees_c',
            'label' => 'NUMBER_OF_SOP_EMPLOYEES',
          ),
        ),
        28 => 
        array (
          0 => 
          array (
            'name' => 'phone_office',
            'comment' => 'The office phone number',
            'label' => 'LBL_PHONE_OFFICE',
          ),
          1 => 
          array (
            'name' => 'outside_sales_appt_set_by_c',
            'label' => 'OUTSIDE_SALES_APPT_SET_BY_IST',
          ),
        ),
        29 => 
        array (
          0 => 
          array (
            'name' => 'ownership',
            'comment' => '',
            'label' => 'LBL_OWNERSHIP',
          ),
          1 => 
          array (
            'name' => 'pos_transaction_quarter_c',
            'label' => 'LBL_POS_TRANSACTION_QUARTER',
          ),
        ),
        30 => 
        array (
          0 => 
          array (
            'name' => 'pos_transaction_year_c',
            'label' => 'LBL_POS_TRANSACTION_YEAR',
          ),
          1 => 
          array (
            'name' => 'place_name_c',
            'label' => 'PLACE_NAME',
          ),
        ),
        31 => 
        array (
          0 => 
          array (
            'name' => 'promotion_date_c',
            'label' => 'PROMOTION_DATE',
          ),
          1 => 
          array (
            'name' => 'customer_funnel_c',
            'studio' => 'visible',
            'label' => 'CUSTOMER_FUNNEL',
          ),
        ),
        32 => 
        array (
          0 => 
          array (
            'name' => 'training_tier_1_date_c',
            'label' => 'TRAINING_TIER_1_DATE',
          ),
          1 => 
          array (
            'name' => 'sic_code',
            'comment' => 'SIC code of the account',
            'label' => 'LBL_SIC_CODE',
          ),
        ),
        33 => 
        array (
          0 => 
          array (
            'name' => 'training_tier_2_date_c',
            'label' => 'TRAINING_TIER_2_DATE',
          ),
          1 => 
          array (
            'name' => 'survey_10_c',
            'label' => 'SURVEY_10',
          ),
        ),
        34 => 
        array (
          0 => 
          array (
            'name' => 'listview_security_group',
            'label' => 'LBL_LISTVIEW_GROUP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'service_kit_c',
            'label' => 'LBL_FUNCTION_SERVICE_KIT',
            'studio' => 'visible',
          ),
        ),
        35 => 
        array (
          0 => 
          array (
            'name' => 'service_level_flag_c',
            'label' => 'LBL_SERVICE_LEVEL_FLAG',
          ),
          1 => 
          array (
            'name' => 'first_install_c',
            'label' => 'FIRST_INSTALL',
          ),
        ),
        36 => 
        array (
          0 => 
          array (
            'name' => 'solaredge_optimized_partner_c',
            'label' => 'SOLAREDGE_OPTIMIZED_PARTNER',
          ),
          1 => 
          array (
            'name' => 'solarcity_account_flag_c',
            'label' => 'LBL_SOLARCITY_ACCOUNT_FLAG',
          ),
        ),
        37 => 
        array (
          0 => 
          array (
            'name' => 'sunpower_c',
            'label' => 'SUNPOWER',
          ),
          1 => 
          array (
            'name' => 'vip_customer_c',
            'label' => 'LBL_VIP_CUSTOMER',
          ),
        ),
        38 => 
        array (
          0 => 
          array (
            'name' => 'sunrun_c',
            'label' => 'SUNRUN',
          ),
          1 => 
          array (
            'name' => 'survey_01_c',
            'label' => 'SURVEY_01',
          ),
        ),
        39 => 
        array (
          0 => 
          array (
            'name' => 'survey_01_date_c',
            'label' => 'SURVEY_01_DATE',
          ),
          1 => 
          array (
            'name' => 'survey_100_c',
            'label' => 'SURVEY_100',
          ),
        ),
        40 => 
        array (
          0 => 
          array (
            'name' => 'survey_100_date_c',
            'label' => 'SURVEY_100_DATE',
          ),
          1 => 
          array (
            'name' => 'survey_200_c',
            'label' => 'SURVEY_200',
          ),
        ),
        41 => 
        array (
          0 => 
          array (
            'name' => 'survey_200_date_c',
            'label' => 'SURVEY_200_DATE',
          ),
          1 => 
          array (
            'name' => 'target_m_c',
            'label' => 'LBL_TARGET_M',
          ),
        ),
        42 => 
        array (
          0 => 
          array (
            'name' => 'ticker_symbol',
            'comment' => 'The stock trading (ticker) symbol for the company',
            'label' => 'LBL_TICKER_SYMBOL',
          ),
          1 => 
          array (
            'name' => 'tier_c',
            'studio' => 'visible',
            'label' => 'TIER',
          ),
        ),
        43 => 
        array (
          0 => 
          array (
            'name' => 'KW__c',
            'label' => 'LBL_FUNCTION_TOTAL_KW',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'us_county_c',
            'label' => 'LBL_US_COUNTY',
          ),
        ),
        44 => 
        array (
          0 => 
          array (
            'name' => 'us_region_c',
            'studio' => 'visible',
            'label' => 'US_REGION',
          ),
          1 => 
          array (
            'name' => 'usa_zip_code_c',
            'studio' => 'visible',
            'label' => 'LBL_USA_ZIP_CODE',
          ),
        ),
        45 => 
        array (
          0 => 
          array (
            'name' => 'welcome_kit_c',
            'label' => 'WELCOME_KIT',
          ),
          1 => 
          array (
            'name' => 'welcome_kit_date_c',
            'label' => 'WELCOME_KIT_DATE',
          ),
        ),
        46 => 
        array (
          0 => 
          array (
            'name' => 'Year_2013_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2013',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'Year_2014_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2014',
            'studio' => 'visible',
          ),
        ),
        47 => 
        array (
          0 => 
          array (
            'name' => 'Year_2015_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2015',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'Year_2016_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2016',
            'studio' => 'visible',
          ),
        ),
        48 => 
        array (
          0 => 
          array (
            'name' => 'Year_2017_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2017',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'canadian_province1_c',
            'label' => 'CANADIAN_PROVINCE1',
          ),
        ),
        49 => 
        array (
          0 => 
          array (
            'name' => 'kw_per_month_resi_com_c',
            'label' => 'KW_PER_MONTH_RESI_COM',
          ),
          1 => 
          array (
            'name' => 'solaredge_service_kit_holder_c',
            'label' => 'LBL_SOLAREDGE_SERVICE_KIT_HOLDER',
          ),
        ),
        50 => 
        array (
          0 => 
          array (
            'name' => 'tax_id_c',
            'label' => 'TAX_ID',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
