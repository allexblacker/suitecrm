<?php
$viewdefs ['Accounts'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          'AOS_GENLET' => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_GENERATE_LETTER}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'comment' => 'Name of the Company',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'phone_c',
            'label' => 'PHONE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'account_type',
            'comment' => 'The Company is of this type',
            'label' => 'LBL_TYPE',
          ),
          1 => 
          array (
            'name' => 'australian_state_c',
            'studio' => 'visible',
            'label' => 'AUSTRALIAN_STATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'account_owner_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_OWNER',
          ),
          1 => 
          array (
            'name' => 'canadian_province_c',
            'studio' => 'visible',
            'label' => 'CANADIAN_PROVINCE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'service_level_c',
            'studio' => 'visible',
            'label' => 'SERVICE_LEVEL',
          ),
          1 => 
          array (
            'name' => 'vendor_number_c',
            'label' => 'VENDOR_NUMBER',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'account_monitoring_id_c',
            'label' => 'ACCOUNT_MONITORING_ID',
          ),
          1 => 
          array (
            'name' => 'vendor_name_c',
            'label' => 'VENDOR_NAME',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'support_comments_c',
            'label' => 'SUPPORT_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'not_currently_satisfied_c',
            'label' => 'NOT_CURRENTLY_SATISFIED',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'marketing_comments_c',
            'studio' => 'visible',
            'label' => 'MARKETING_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'days_without_alliance_transa_c',
            'label' => 'LBL_DAYS_WITHOUT_ALLIANCE_TRANSA',
          ),
          1 => 
          array (
            'name' => 'vip_customer_c',
            'label' => 'LBL_VIP_CUSTOMER',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'segment_c',
            'studio' => 'visible',
            'label' => 'SEGMENT',
          ),
          1 => 
          array (
            'name' => 'service_lvl_is_missing_c',
            'label' => 'LBL_SERVICE_LVL_IS_MISSING',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'promotion_c',
            'label' => 'PROMOTION',
          ),
          1 => 
          array (
            'name' => 'certified_installer_tesla_c',
            'studio' => 'visible',
            'label' => 'CERTIFIED_INSTALLER_TESLA',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'csr_c',
            'studio' => 'visible',
            'label' => 'CSR',
          ),
          1 => 
          array (
            'name' => 'customer_number_in_priority_c',
            'label' => 'CUSTOMER_NUMBER_IN_PRIORITY',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'timestamp_recalculate_c',
            'label' => 'TIMESTAMP_RECALCULATE_ALLIANCE_POINTS',
          ),
          1 => 
          array (
            'name' => 'customer_name_in_priority_c',
            'label' => 'CUSTOMER_NAME_IN_PRIORITY',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'street_c',
            'label' => 'STREET',
          ),
          1 => 
          array (
            'name' => 'zip_postal_code_c',
            'label' => 'ZIP_POSTAL_CODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'city_c',
            'label' => 'CITY',
          ),
          1 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'COUNTRY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE',
          ),
          1 => 
          array (
            'name' => 'region_c',
            'studio' => 'visible',
            'label' => 'REGION',
          ),
        ),
        3 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_street',
            'label' => 'LBL_SHIPPING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'shipping',
            ),
          ),
          1 => 
          array (
            'name' => 'shipping_address_postalcode',
            'comment' => 'The zip code used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_city',
            'comment' => 'The city used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_CITY',
          ),
          1 => 
          array (
            'name' => 'shipping_address_state',
            'comment' => 'The state used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_STATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_country',
            'comment' => 'The country used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
