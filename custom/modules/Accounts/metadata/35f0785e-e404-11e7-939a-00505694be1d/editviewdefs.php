<?php
$viewdefs ['Accounts'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
        1 => 
        array (
          'file' => 'custom/modules/Accounts/AccountValidation.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
          1 => 
          array (
            'name' => 'phone_c',
            'label' => 'PHONE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'sales_manager_c',
            'label' => 'SALES_MANAGER',
          ),
          1 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'COUNTRY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'inside_sales_owner_c',
            'studio' => 'visible',
            'label' => 'INSIDE_SALES_OWNER',
          ),
          1 => 
          array (
            'name' => 'shipping_address_street',
            'hideLabel' => true,
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'shipping',
              'copy' => 'billing',
              'rows' => 2,
              'cols' => 30,
              'maxlength' => 150,
            ),
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'website',
            'type' => 'link',
            'label' => 'LBL_WEBSITE',
          ),
          1 => 
          array (
            'name' => 'city_c',
            'label' => 'CITY',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'account_monitoring_id_c',
            'label' => 'ACCOUNT_MONITORING_ID',
          ),
          1 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'certified_installer_tesla_c',
            'studio' => 'visible',
            'label' => 'CERTIFIED_INSTALLER_TESLA',
          ),
          1 => 
          array (
            'name' => 'zip_postal_code_c',
            'label' => 'ZIP_POSTAL_CODE',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'support_comments_c',
            'label' => 'SUPPORT_COMMENTS',
          ),
          1 => 'employees',
        ),
        2 => 
        array (
          0 => 'account_type',
          1 => 
          array (
            'name' => 'customer_name_in_priority_c',
            'label' => 'CUSTOMER_NAME_IN_PRIORITY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'enphase_c',
            'label' => 'ENPHASE',
          ),
          1 => 
          array (
            'name' => 'vendor_name_c',
            'label' => 'VENDOR_NAME',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'sunpower_c',
            'label' => 'SUNPOWER',
          ),
          1 => 
          array (
            'name' => 'vendor_number_c',
            'label' => 'VENDOR_NUMBER',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'sunrun_c',
            'label' => 'SUNRUN',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'street_c',
            'label' => 'STREET',
          ),
          1 => 
          array (
            'name' => 'parent_account_c',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_ACCOUNT',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_city',
            'comment' => 'The city used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_CITY',
          ),
          1 => 
          array (
            'name' => 'us_region_c',
            'studio' => 'visible',
            'label' => 'US_REGION',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'region_c',
            'studio' => 'visible',
            'label' => 'REGION',
          ),
          1 => 
          array (
            'name' => 'shipping_address_postalcode',
            'comment' => 'The zip code used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'account_owner_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_OWNER',
          ),
          1 => 
          array (
            'name' => 'shipping_address_state',
            'comment' => 'The state used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_STATE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'segment_c',
            'studio' => 'visible',
            'label' => 'SEGMENT',
          ),
          1 => 
          array (
            'name' => 'training_type_c',
            'studio' => 'visible',
            'label' => 'LBL_TRAINING_TYPE_C',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'shipping_address_country',
            'comment' => 'The country used for the shipping address',
            'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'onboarding_category_c',
            'studio' => 'visible',
            'label' => 'LBL_ONBOARDING_CATEGORY_C',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'department_c',
            'label' => 'LBL_DEPARTMENT',
          ),
          1 => 
          array (
            'name' => 'account_currency_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_CURRENCY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'checkiffirstopp_c',
            'label' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'service_level_c',
            'studio' => 'visible',
            'label' => 'SERVICE_LEVEL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'service_partner_c',
            'studio' => 'visible',
            'label' => 'LBL_SERVICE_PARTNER',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'marketing_comments_c',
            'studio' => 'visible',
            'label' => 'MARKETING_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'account_target_c',
            'label' => 'ACCOUNT_TARGET',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'business_entity_c',
            'studio' => 'visible',
            'label' => 'BUSINESS_ENTITY',
          ),
          1 => 
          array (
            'name' => 'exclude_from_forecast_c',
            'label' => 'EXCLUDE_FROM_FORECAST',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'pos_last_transaction_y_and_q_c',
            'label' => 'LBL_POS_LAST_TRANSACTION_Y_AND_Q',
          ),
          1 => 
          array (
            'name' => 'distribution_partners_c',
            'label' => 'DISTRIBUTION_PARTNERS',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'financing_partners_c',
            'studio' => 'visible',
            'label' => 'FINANCING_PARTNERS',
          ),
          1 => 
          array (
            'name' => 'solaredge_optimized_partner_c',
            'label' => 'SOLAREDGE_OPTIMIZED_PARTNER',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'active_spa_c',
            'studio' => 'visible',
            'label' => 'ACTIVE_SPA',
          ),
          1 => 
          array (
            'name' => 'not_currently_satisfied_c',
            'label' => 'NOT_CURRENTLY_SATISFIED',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'days_without_alliance_transa_c',
            'label' => 'LBL_DAYS_WITHOUT_ALLIANCE_TRANSA',
          ),
          1 => 
          array (
            'name' => 'canadian_province_c',
            'studio' => 'visible',
            'label' => 'CANADIAN_PROVINCE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'customer_number_in_priority_c',
            'label' => 'CUSTOMER_NUMBER_IN_PRIORITY',
          ),
          1 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'vip_customer_c',
            'label' => 'LBL_VIP_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'marketing_comment_timestamp_c',
            'label' => 'MARKETING_COMMENT_TIMESTAMP',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'timestamp_recalculate_c',
            'label' => 'TIMESTAMP_RECALCULATE_ALLIANCE_POINTS',
          ),
          1 => 
          array (
            'name' => 'No_of_unique_sites__c',
            'label' => 'LBL_FUNCTION_UNIQUE_SITES',
            'studio' => 'visible',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'Loyalty_Plan_start_date__c',
            'label' => 'LBL_FUNCTION_LOYALTY_START',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'KW__c',
            'label' => 'LBL_FUNCTION_TOTAL_KW',
            'studio' => 'visible',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'Last_Alliance_Transaction__c',
            'label' => 'LBL_FUNCTION_LAST_TRANSACTION',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'Year_2017_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2017',
            'studio' => 'visible',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'Year_2016_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2016',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'Year_2015_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2015',
            'studio' => 'visible',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'Year_2014_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2014',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'Year_2013_KW__c',
            'label' => 'LBL_FUNCTION_YEAR_KW_2013',
            'studio' => 'visible',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
