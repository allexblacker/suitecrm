<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-11-16 15:24:28
$dictionary['Campaign']['fields']['status']['inline_edit']=true;
$dictionary['Campaign']['fields']['status']['options']='status_0';
$dictionary['Campaign']['fields']['status']['comments']='Status of the campaign';
$dictionary['Campaign']['fields']['status']['merge_filter']='disabled';

 

 // created: 2017-11-16 15:26:42
$dictionary['Campaign']['fields']['campaign_type']['inline_edit']=true;
$dictionary['Campaign']['fields']['campaign_type']['options']='campaign_type_list';
$dictionary['Campaign']['fields']['campaign_type']['comments']='The type of campaign';
$dictionary['Campaign']['fields']['campaign_type']['merge_filter']='disabled';

 

 // created: 2017-11-16 14:57:43
$dictionary['Campaign']['fields']['name']['len']='255';
$dictionary['Campaign']['fields']['name']['inline_edit']=true;
$dictionary['Campaign']['fields']['name']['comments']='The name of the campaign';
$dictionary['Campaign']['fields']['name']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['name']['unified_search']=false;
$dictionary['Campaign']['fields']['name']['full_text_search']=array (
);

 
?>