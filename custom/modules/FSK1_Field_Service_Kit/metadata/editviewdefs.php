<?php
$module_name = 'FSK1_Field_Service_Kit';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'shipping_address',
            'studio' => 'visible',
            'label' => 'Shipping_Address',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accounts_fsk1_field_service_kit_1_name',
          ),
          1 => 
          array (
            'name' => 'shipping_date',
            'label' => 'Shipping_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'qty',
            'label' => 'QTY',
          ),
          1 => 
          array (
            'name' => 'tracking_number',
            'studio' => 'visible',
            'label' => 'Tracking_Number',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'contacts_fsk1_field_service_kit_1_name',
          ),
        ),
      ),
    ),
  ),
);
?>
