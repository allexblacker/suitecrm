<link rel="stylesheet" type="text/css" href="custom/modules/Administration/css/customLayoutSettings.css">

{literal}
<script>
	var roles = new Array();
	var dropdownIdMap = new Array();
	{/literal}
		{foreach item=row from=$roles}
			{literal}roles.push(['{/literal}{$row.id}{literal}', '{/literal}{$row.name}{literal}']);{/literal}
		{/foreach}
	{literal}
	
	function getDropdown(dropdownoptions, rowId, columnId, rowValue, columnValue) {
		var iId = "layouts_" + rowId + "_" + columnId;
		var retval = '<select name="' + iId + '" id="' + iId + '">';
		retval += dropdownoptions;
		dropdownIdMap.push([rowValue[0],  rowValue[1], columnValue.id, columnValue.name, iId]);
		return retval + '</retval>';
	}
	
	function generateTable(rows, columns, dropdownoptions) {
		var retval;
		for (var i = -1; i < rows.length; i++) {
			var newRow = "<tr>";
			for (var j = -1; j < columns.length; j++) {
				if (i == -1) {
					if (j == -1)
						newRow += "<td><input type='hidden' name='selected_module' value='" + $('#modules').val() + "'/></td>";
					else
						newRow += "<td><input type='hidden' name='columnNumber_" + j + "' value='" + columns[j].id + "'/>" + columns[j].name + "</td>";
				} else {
					if (j == -1)
						newRow += "<td class='first'><input type='hidden' name='rowNumber_" + i + "' value='" + rows[i][0] + "'/>" + rows[i][1] + "</td>";
					else
						newRow += "<td>" + getDropdown(dropdownoptions, i, j, rows[i], columns[j]) + "</td>";
				}
			}
			retval += newRow + "</tr>";
		}
		return retval;
	}
	
	function setSavedValues(saved) {
		var dropdowns = $("#settings_table select");
		for (var i = 0; i < saved.length; i++) {
			for (var j = 0; j < dropdownIdMap.length; j++) {
				if (saved[i].role_id == dropdownIdMap[j][0] && saved[i].dropdown_value == dropdownIdMap[j][2]) {
					$('#' + dropdownIdMap[j][4]).val(saved[i].layout_id);
					$('#' + dropdownIdMap[j][4] +' option[value="' + saved[i].layout_id + '"]').attr('selected', true);
				}
			}
		}
	}
	
	function changeModule() {
		$(".dtbc_role_layout_selector").hide();
		var moduleName = $('#modules').val();
		$.ajax({
			method: "POST",
			url: "index.php?module=Administration&action=getLayoutsForModule&to_pdf=1",
			data: {
				'module_name': moduleName,
			}
		})
		.done(function (datastring) {
			$("#selectedModuleName").val(moduleName);
			if (datastring) {
				dropdownIdMap = new Array();
				var data = JSON.parse(datastring);
				$("#settings_table tr").remove();
				$("#settings_table tbody").html(generateTable(roles, data.columns, data.options));
				setSavedValues(data.saved);
			}
			$(".dtbc_role_layout_selector").show();
		})
		.fail(function (e) {
			console.log(e);
			$(".dtbc_role_layout_selector").show();
		});
	}
</script>
{/literal}

<div class="dtbc_container">
	<div class="dtbc_module_selector">
		<select name="modules" id="modules" onChange="changeModule();">
			<option value="">{$selModuleLabel}</option>
			{foreach item=row from=$modules}<option value='{$row.id}'>{$row.name}</option>{/foreach}
		</select>
	</div>
	<div class="dtbc_role_layout_selector">
		<form method="post" action="index.php?module=Administration&action=saveLayoutSettings&to_pdf=1">
			<input type="hidden" value="" name="selectedModuleName" id="selectedModuleName"/>
			<table id="settings_table">
				<tbody>
				</tbody>
			</table>
			<input type=submit value="{$saveButtonLabel}" />
		</form>
	</div>
</div>
