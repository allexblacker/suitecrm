{literal}
<script>
    $(function(){
        var jsonConfigLayouts = '{/literal}{$configLayouts}{literal}';
        var jsonConfigLanguages = '{/literal}{$configLanguages}{literal}';
        
        var config = $.parseJSON(jsonConfigLayouts);
        if(config != null)
            $.each(config, function(key, value){
                if(value != "")
                    $("#layout_" + key).val(value);
            });
        
        config = $.parseJSON(jsonConfigLanguages);
        if(config != null)
            $.each(config, function(key, value){
                if(value != "")
                    $("#language_" + key).val(value);
            });
        
        $("#saveButton").click(function(){
            var dropDownListValues = [];
            $("#layoutSettings select").each(function(){
               var array = [];
                dropDownObject = {
                    id: $(this).attr('id'),
                    value: $(this).val()
                }
                dropDownListValues.push(dropDownObject); 
            });
            var layoutsJSON = JSON.stringify(dropDownListValues);
            
            $.ajax({
                url: "index.php?module=Administration&action=saveSurveyConfigSettings",
                data: {
                    layoutsAndLanguages: layoutsJSON,
                },
                type: "POST",
                success: function(result){
                    alert(result);
                },
                error: function(request, status, error){
                        alert(request.responseText);
                }
            });
        });
    });
    
</script>
{/literal}
<div id = "layoutSettings">
    <table>
        {foreach item=row key=indexer from=$languages}
        <tr>
            <td>{$row}</td>
            <td><select id="layout_{$indexer}">{$optionsLayouts}</select></td>
            <td><select id="language_{$indexer}">{$optionsLanguages}</select></td>
        </tr>
        {/foreach}
    </table>
    <input type="button" value ="{$saveText}" id="saveButton"/>
</div>