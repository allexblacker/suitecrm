<html>
<head>
{literal}
<link rel="stylesheet" type="text/css" href="custom/modules/Administration/css/dependency.css">
<script src='custom/include/dtbc/js/addToDropDown.js'></script>
<script src='custom/modules/Administration/js/dependency.js'></script>
<script src='custom/modules/Administration/js/tableHeadFixer.js'></script>
<style>

</style>
{/literal}
</head>
<body>
	<table>
	<thead>
		<tr>
		<td id="moduleLabel">{$module}</td>
		<td id="controllerLabel" class='hide'>{$controller}</td>
		<td id="controlledLabel" class='hide'>{$controlled}</td>
		<td></td>
		</tr>
	</thead>
	<tr>
		<td>
			<select id="modules" onChange='getDropDownValues()'>
			{foreach from=$moduleList key=moduleKey item=moduleValue}
				<option value="{$moduleKey}">{$moduleValue}</option>
			{/foreach}
			</select>
		</td>
		<td><select id="dropDownController" class="hide" onChange="getSettings(this.id, this.value)"></select></td>
		<td><select id="dropDownControlled" class="hide" onChange="getSettings(this.id, this.value)"></select></td>
		<td><input type='button' id='saveButton' value='{$saveButton}' onClick='setSettings()' class='hide' /></td>
	</tr>
	</table>
	<div id="container">
		<table id = "settings"></table>
	</div>
</body>
</html>