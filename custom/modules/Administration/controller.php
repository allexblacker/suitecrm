<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/SecurityGroups/SecurityGroup.php';
require_once 'custom/include/dtbc/EmailSender.php';
require_once("custom/include/dtbc/helpers.php");

class CustomAdministrationController extends SugarController {
	
	// Admin page: Layout settings
	public function action_saveLayoutSettings() {
		global $db;
		// columns = dropdown values
		// rows = roles
		$mustSave = $this->getSavedValues();
		
		//$originalSettings = $this->getValuesFromDb($_REQUEST['selectedModuleName']);
		
		// Set new values
		foreach ($mustSave as $data) {
			// Get id if exists
			$sql = "SELECT id 
					FROM dtbc_layouts_roles 
					WHERE deleted = 0 AND 
					role_id = " . $db->quoted($data['rowId']) . " AND 
					bean_variable_value = " . $db->quoted($data['columnId']);
			
			$res = $db->fetchOne($sql);

			// Insert or update values		
			if (!empty($res) && is_array($res) && count($res) > 0)
				$sql = "UPDATE dtbc_layouts_roles
						SET role_id = " . $db->quoted($data['rowId']) . ",
							bean_variable_value = " . $db->quoted($data['columnId']) . ",
							layout_id = " . $db->quoted($data['value']) . "
						WHERE id = " . $db->quoted($res['id']);
			else
				$sql = "INSERT INTO dtbc_layouts_roles
						(id, role_id, bean_variable_value, layout_id, deleted)
						VALUES (
							UUID(),
							" . $db->quoted($data['rowId']) . ",
							" . $db->quoted($data['columnId']) . ",
							" . $db->quoted($data['value']) . ",
							0
						)";
			$db->query($sql);
		}
		SugarApplication::redirect('index.php?' . http_build_query(array('module' => 'Administration', 'action' => 'dtbcCustomSettings', 'type' => 'layoutSettings')));
	}

	// Admin page: Layout settings
	private function getSavedValues() {
		$columnValues = array();
		$rowValues = array();
		$layoutValues = array();
		$module = "";
		// Collect selected values
		foreach ($_REQUEST as $key => $value) {
			if (strpos($key, "columnNumber") !== false) {
				$columnValues[] = array(
					"id" => explode("_", $key)[1],
					"value" => $value,
				);
			} else if (strpos($key, "rowNumber") !== false) {
				$rowValues[] = array(
					"id" => explode("_", $key)[1],
					"value" => $value,
				);
			} else if (strpos($key, "layouts") !== false && strlen($value) > 0) {
				$temp = explode("_", $key);
				$layoutValues[] = array(
					"rowId" => $temp[1],
					"columnId" => $temp[2],
					"value" => $value,
				);
			}
		}
		// Map selected ids to guids and values
		for ($i = 0; $i < count($layoutValues); $i++) {
			foreach ($columnValues as $colVal) {
				if ($layoutValues[$i]['columnId'] == $colVal['id']) {
					$layoutValues[$i]['columnId'] = $colVal['value'];
					break;
				}
			}
			foreach ($rowValues as $rowVal) {
				if ($layoutValues[$i]['rowId'] == $rowVal['id']) {
					$layoutValues[$i]['rowId'] = $rowVal['value'];
					break;
				}
			}
		}
		
		return $layoutValues;
	}
	
	// Admin page: Layout settings
	public function action_getLayoutsForModule() {
		$retval = $this->getValuesFromDb($_REQUEST['module_name']);

		echo json_encode($retval);
	}
	
	private function getValuesFromDb($moduleName) {
		global $db, $app_list_strings, $sugar_config, $mod_strings;
		$retval = array();
		// Get all layouts for the module
		$res = $db->query("SELECT * FROM dtbc_layouts
							WHERE module_name = " . $db->quoted($moduleName) . " AND
							deleted = 0
							ORDER BY name ASC");
		$options = '<option value="default">' . $mod_strings['LBL_ADMIN_DEFAULT_LAYOUT'] . '</option>';
		while ($role = $db->fetchByAssoc($res)) {
			$options .= '<option value="' . $role['id'] . '">' . $role['name'] . '</option>';
		}
		
		// Get all settings for the module
		$res = $db->query("SELECT * FROM dtbc_layouts_roles 
							WHERE EXISTS (SELECT * FROM dtbc_layouts WHERE module_name = " . $db->quoted($moduleName) . " AND 
							deleted = 0) AND deleted = 0");
		$retval["saved"] = array();
		while ($role = $db->fetchByAssoc($res)) {
			$retval["saved"][] = array(
				"role_id" => $role["role_id"],
				"dropdown_value" => $role["bean_variable_value"],
				"layout_id" => $role["layout_id"],
			);
		}
		
		$retval["options"] = $options;
		
		if (trim(strlen($sugar_config['solaredge']['layout_config'][$moduleName])) == 36)
			$retval["columns"] = array(array("id" => $sugar_config['solaredge']['layout_config'][$moduleName], "name" => "Layout"));
		else
			$retval["columns"] = $this->convertDropdownValues($app_list_strings[$sugar_config['solaredge']['layout_config'][$moduleName]]);
		
		return $retval;
	}
	
	// Common button in the left menu
	public function action_takeMe() {
		global $db, $current_user;
		$module = $_REQUEST['wrokingmodule'];
		$record = $_REQUEST['workingrecord'];
		
		// Add current user as an assigned user
		$this->modifyAssignedUser($module, $record, $current_user->id);
		
		// Special case processing
		if ($module == "Cases")
			$this->setCaseFields($record);
		
		SugarApplication::redirect('index.php?' . http_build_query(array('module' => $module, 'action' => 'DetailView', 'record' => $record)));
	}
	
	private function setCaseFields($recordId) {
		
		if(empty($recordId))
			return;
		
		$caseBean = BeanFactory::getBean('Cases', $recordId);
		
		if(empty($caseBean))
			return;
		
		$caseBean->case_status_c = 2;
		$caseBean->taken_c = 1;
		
		$caseBean->save(); 
	}
	
	// Admin page: Layout settings
	private function convertDropdownValues($list) {
		$retval = array();

		if (is_array($list)) {
			foreach ($list as $key => $value) {
				if (strlen($key) > 0)
					$retval[] = array(
						"id" => $key,
						"name" => $value,
					);
			}
		}
		
		return $retval;
	}
	
	private function modifyAssignedUser($module, $record, $newUserId) {
		global $db;
		// Must use SQL, because when using beans, some hook runs which set back the group (because the assigned user is a member of the group)
		$bean = BeanFactory::getBean($module, $record);
		
		$sql = "UPDATE " . $bean->table_name . " 
				SET assigned_user_id = " . $db->quoted($newUserId) . " 
				WHERE id = " . $db->quoted($record) . " AND 
				deleted = 0";
		
		$bean->assigned_user_id = $newUserId;
		$db->query($sql);
	}
	
	private function sendNotificationEmail($module, $record, $userName, $secGroupId, $userId) {
		global $db;
		$emailSender = new EmailSender();
		
		// Get group members
		$sql = "SELECT user_id, email_address FROM securitygroups_users 
				JOIN email_addr_bean_rel ON bean_id = user_id AND bean_module = 'Users' AND email_addr_bean_rel.deleted = 0
				JOIN email_addresses ON email_addresses.id = email_address_id AND email_addresses.deleted = 0
				WHERE securitygroup_id = " . $db->quoted($secGroupId) . " AND
				securitygroups_users.deleted = 0 AND 
				user_id <> " . $db->quoted($userId);
		
		$res = $db->query($sql);
		
		// Send emails to all the group members
		while ($row = $db->fetchByAssoc($res)) {
			$emailSender->sendNotificationWhenTakeARecord($row['email_address'], $module, $record, $userName);
		}		
	}
    
     public function action_saveSurveyConfigSettings(){
        
        $configurator = new Configurator();
        $configurator->loadConfig();

        if(!isset($_REQUEST['layoutsAndLanguages']))
            die('Invalid request!');

        $json = htmlspecialchars_decode($_REQUEST['layoutsAndLanguages']);
        $dropdownListValues = json_decode($json);
        foreach($dropdownListValues as $object){
            $strPosition = strpos($object->id,"_");
            $type = substr($object->id,0,$strPosition);
            $id = substr($object->id,$strPosition + 1,strlen($object->id) - $strPosition);
            $this->clearConfigParam($configurator, $type, $id);
            $configurator->config['solaredge']['survey'][$type][$id] = $object->value;
        }
        
        $configurator->saveConfig();
        
        die('The changes have been successfully saved!');
    }
    
    private function clearConfigParam($configurator, $type, $id) {
		global $sugar_config;
		foreach($sugar_config['solaredge']['survey'][$type][$id] as $oldKey => $oldVal) {					
			$configurator->clearCache();
			$configurator->readOverride();
			
			if(isset($configurator->config['solaredge']['survey'][$type][$id][$oldKey]))
				$configurator->config['solaredge']['survey'][$type][$id][$oldKey] = '';
			$configurator->handleOverride();	
			$configurator->saveConfig(); 
		}
	}
	
	public function action_getDependencySettings(){
		
		if(!isset($_REQUEST['controllerListName']) || !isset($_REQUEST['controlledListName'] ))
			die('REQUEST is not set!');

		$controllerListName = $_REQUEST['controllerListName'];
		$controlledListName = $_REQUEST['controlledListName'];
		
			
		global $app_list_strings;
		$result = array();
		$result[0] = $app_list_strings[$controllerListName];
		$result[1] = $app_list_strings[$controlledListName];
			
		$result[2] = $this->getExistingSettings();
		asort($result[0]);
		asort($result[1]);
		echo json_encode($result);
		die();
	}
	
	private function getExistingSettings(){
		
		global $db;
		$module = $_REQUEST['moduleName'];
		$fieldController = $_REQUEST['fieldController'];
		$fieldControlled = $_REQUEST['fieldControlled'];
		$sql = "SELECT controller_value, controlled_value FROM dtbc_dependencies WHERE module = " . $db->quoted($module) . 
				" AND controller = " . $db->quoted($fieldController);
		if(!empty($fieldControlled))
				$sql .= " AND controlled = " . $db->quoted($fieldControlled);
		
		$queryResult = $db->query($sql);
		$values = array();
		while($record = $db->fetchByAssoc($queryResult)){
			array_push($values,$record['controller_value'] . '|' . $record['controlled_value']);
		}
		return $values;
	}
	
	public function action_getExistingSettings(){
		
		global $db;
		$module = $_REQUEST['moduleName'];
		$fieldController = $_REQUEST['fieldController'];
		$controllerValue = $_REQUEST['controllerValue'];
		
		$connections = array();
		$sql = "SELECT DISTINCT controlled FROM dtbc_dependencies WHERE module = " . $db->quoted($module) . 
		" AND controller = " . $db->quoted($fieldController);
		$queryResult = $db->query($sql);
		
		$values = array();
		while($record = $db->fetchByAssoc($queryResult)){
			$controlled = $record['controlled'];
			$values[$controlled] = array();
			array_push($connections, $record['controlled']);
		}
		
		if(!empty($connections)){
			$sql = "SELECT controlled,controlled_value,controlled_list_name FROM dtbc_dependencies WHERE module = " . $db->quoted($module) . 
					" AND controller = " . $db->quoted($fieldController) . " AND controller_value = " . $db->quoted($controllerValue) .
					" ORDER BY controlled, controlled_list_name, controlled_value";
			$queryResult = $db->query($sql);
			while($record = $db->fetchByAssoc($queryResult)){
				$key = array_search($record['controlled'], $connections);
				if($key !== false)
					unset($connections[$key]);	
				
				$controlled = $record['controlled'];
				array_push($values[$controlled], array('value' => $record['controlled_value'], 'list' => $record['controlled_list_name']));
			}

			$elementNumber = count($connections);
			for($i = 0; $i < $elementNumber; $i++){
				$controlled = $connections[$i];
				array_push($values[$controlled], array('value' => '', 'list' => ''));
			}
			echo json_encode($values);
		}
		die();
	}
	
	
	
	public function action_getModuleDropDowns(){
		
		if(!isset($_REQUEST['moduleName']))
			die('REQUEST is not set!');
		
		$module = $_REQUEST['moduleName'];
		$bean = BeanFactory::newBean($module);
		
		if($bean != false){
			$fieldDefinitons = $bean->getFieldDefinitions();
			$enums = array();
			foreach($fieldDefinitons as $key => $value){
				if($value['type'] == 'enum'){
					$enums[$key] = $value['options'];
				}
			}
			ksort($enums);
			echo json_encode($enums);
		}
		die();
	}
	
	public function action_setDependencySettings(){
		
		global $db, $current_user, $mod_strings;
		$values = $_REQUEST['selectedValues'];
		$controller = $db->quoted($_REQUEST['controller']);
		$controlled = $db->quoted($_REQUEST['controlled']);
		$controllerListName = $db->quoted($_REQUEST['controllerListName']);
		$controlledListName = $db->quoted($_REQUEST['controlledListName']);
		$module = $db->quoted($_REQUEST['moduleName']);
		
		$valuesLength = count($values);
		$sql = "DELETE FROM dtbc_dependencies WHERE module LIKE $module AND controller LIKE $controller AND controlled LIKE $controlled";
		$db->query($sql);
		for($i = 0; $i < $valuesLength; $i++){
			$explodeValues = explode('|', $values[$i]);
			$controllerValue = $db->quoted($explodeValues[0]);
			$controlledValue = $db->quoted($explodeValues[1]);
			$user = $db->quoted($current_user->id);
			
			$sql = "INSERT INTO dtbc_dependencies(id,module,controller,controller_value,controller_list_name,controlled,controlled_value,controlled_list_name,modified_by) VALUES(UUID(),$module,$controller,$controllerValue,$controllerListName,$controlled,$controlledValue,$controlledListName,$user)";

			try{
				$db->query($sql);
			}
			catch(Exception $e){
				die('Error: ' . $e->getMessage());
			}
			
		}
		echo $mod_strings['LBL_SAVE_MESSAGE'];
		die();
	}
	
	public function action_getDependenciesOfModule(){
		
		if(!isset($_REQUEST['moduleName']))
			die();

		global $db;
		$sql = "SELECT controller,
						controller_value,
						controller_list_name,
						controlled,
						controlled_value,
						controlled_list_name
				FROM dtbc_dependencies WHERE module LIKE " . $db->quoted($_REQUEST['moduleName']) .
				" order by controlled_value";

		$result = $db->query($sql);
		$dependencies = array();
		while($record = $db->fetchByAssoc($result)){
			array_push($dependencies,$record);
		}
		echo json_encode($dependencies);
		die();
	}
}
