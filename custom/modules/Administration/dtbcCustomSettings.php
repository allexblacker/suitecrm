<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/modules/Administration/CustomLayoutSettings.php');

if (isset($_REQUEST['type'])) {
	switch ($_REQUEST['type']) {
		case 'layoutSettings':
			$layouts = new CustomLayoutSettings();
			$groups = $layouts->generate();
			break;
	}
}

