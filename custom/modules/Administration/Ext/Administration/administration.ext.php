<?php 
 //WARNING: The contents of this file are auto-generated



global $app_list_strings;

$admin_option_defs=array();
$admin_option_defs['Administration']['DHA_PlantillasDocumentos_validations']= array(
   'DHA_PlantillasDocumentos',
   translate('LBL_MODULE_CONFIG_DESC', 'DHA_PlantillasDocumentos'),
   translate('LBL_MODULE_CONFIG_DESC', 'DHA_PlantillasDocumentos'),
   'index.php?module=DHA_PlantillasDocumentos&action=Configuration'
);
   
$admin_group_header[]= array(
   $app_list_strings['moduleList']['DHA_PlantillasDocumentos'], //translate('LBL_MODULE_TITLE', 'DHA_PlantillasDocumentos'),
   '',
   false,
   $admin_option_defs, 
   translate('LBL_MODULE_CONFIG_SECTION_DESC', 'DHA_PlantillasDocumentos')
);




if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$admin_option_defs = array();

$admin_option_defs['Administration']['dtbc_layout_settings']= array('Administration', 'LBL_ADMIN_LAYOUT_SETTINGS', 'LBL_ADMIN_LAYOUT_SETTINGS_DESC', './index.php?module=Administration&action=dtbcCustomSettings&type=layoutSettings');
$admin_option_defs['Administration']['solaredge_survey_settings']= array('Administration','LBL_SOLAREDGE_SURVEY_SETTINGS','LBL_SOLAREDGE_SURVEY_SETTINGS_DESC','./index.php?module=Administration&action=SolaredgeSurveyConfigs');

$admin_option_defs['Administration']['dependency_settings']= array('Administration','LBL_DEPENDECY_SETTINGS','LBL_DEPENDENCY_SETTINGS_DESC','./index.php?module=Administration&action=DependencySettings');

$admin_group_header[]= array('LBL_CUSTOM_ADMIN_SETTINGS_TITLE', '', false, $admin_option_defs, '');


?>