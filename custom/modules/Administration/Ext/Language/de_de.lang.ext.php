<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_AOD_ADMIN_MANAGE_AOD'] = 'Erweiterter OpenAdmin Einstellungen';
$mod_strings['LBL_AOD_ENABLE'] = 'AOD aktivieren';
$mod_strings['LBL_AOD_SETTINGS'] = 'AOD Einstellungen';
$mod_strings['LBL_SALESAGILITY_ADMIN'] = 'Erweiterter OpenAdmin';



$mod_strings['LBL_CONFIG_QUICKCRM'] = 'Definition von sichtbaren Modulen und Feldern';
$mod_strings['LBL_CONFIG_QUICKCRM_TITLE'] = 'Konfiguration von QuickCRM Mobile';
$mod_strings['LBL_ERR_DIR_MSG'] = 'Manche Dateien konnten nicht erstellt werden. Bitte überprüfen Sie die Schreibberechtigung für:';
$mod_strings['LBL_QUICKCRM'] = 'QuickCRM Mobile';
$mod_strings['LBL_UPDATE_MSG'] = '<strong>Die Applikation wurde für QuickCRM auf mobilen Endgeräten aktualisiert</strong><br/>Sie erreichen die mobile Version unter:';
$mod_strings['LBL_UPDATE_QUICKCRM'] = 'Erstellt Felddefinitionen neu';
$mod_strings['LBL_UPDATE_QUICKCRM_TITLE'] = 'QuickCRM Aktualisierung';



$mod_strings['LBL_ACTION_MENU_BACKGROUND'] = 'Aktionsmenü Hintergrund-Farbe';
$mod_strings['LBL_ACTION_MENU_BACKGROUND_HOVER'] = 'Aktionsmenü Hover-Farbe';
$mod_strings['LBL_COLOUR_ADMIN_BASE'] = 'Farbe Basismenü:';
$mod_strings['LBL_COLOUR_ADMIN_BTMGR'] = 'Farbe Unterer Verlauf:';
$mod_strings['LBL_COLOUR_ADMIN_BTNBTM'] = 'Farbe Schaltfläche unten:';
$mod_strings['LBL_COLOUR_ADMIN_BTNHOVER'] = 'Farbe Schaltfläche Mouseover:';
$mod_strings['LBL_COLOUR_ADMIN_BTNLNK'] = 'Schaltfläche Linkfarbe';
$mod_strings['LBL_COLOUR_ADMIN_BTNLNKHOVER'] = 'Schaltfläche Hover-Linkfarbe';
$mod_strings['LBL_COLOUR_ADMIN_BTNMID1'] = 'Farbe Schaltfläche oberer Bereich:';
$mod_strings['LBL_COLOUR_ADMIN_BTNMID2'] = 'Farbe Schaltfläche unterer Bereich:';
$mod_strings['LBL_COLOUR_ADMIN_BTNTOP'] = 'Farbe Schaltfläche oben:';
$mod_strings['LBL_COLOUR_ADMIN_BUTTON'] = 'Schaltflächen Farbeinstellungen';
$mod_strings['LBL_COLOUR_ADMIN_CUSTOM'] = 'Einstellungen benutzerdefinierter Code';
$mod_strings['LBL_COLOUR_ADMIN_DASHHEAD'] = 'Farbe Dashlet Kopfzeile:';
$mod_strings['LBL_COLOUR_ADMIN_DASHLET'] = 'Übersicht Farbeinstellungen';
$mod_strings['LBL_COLOUR_ADMIN_DDLINK'] = 'Auswahlmenü Linkfarbe:';
$mod_strings['LBL_COLOUR_ADMIN_DDLINK_HOVER'] = 'Auswahllisten-Menü-Link-Farbe: ';
$mod_strings['LBL_COLOUR_ADMIN_DDMENU'] = 'Auswahlmenü Farbe:';
$mod_strings['LBL_COLOUR_ADMIN_ICON'] = 'Iconfarbe:';
$mod_strings['LBL_COLOUR_ADMIN_INTRO'] = 'Ändern Sie diese Einstellungen um ihr SuiteCRM Design anzupassen. <strong>Hinweis:</strong> Bitte vergessen Sie nicht Ihren Browsercache zu leeren (STRG + F5) nachdem Sie Ihre Einstellungen gespeichert haben.';
$mod_strings['LBL_COLOUR_ADMIN_MENU'] = 'Menü Farbeneinstellung';
$mod_strings['LBL_COLOUR_ADMIN_MENUBOTTOM'] = 'Gradient des unteren Menüs';
$mod_strings['LBL_COLOUR_ADMIN_MENUBRD'] = 'Farbe Menü Einfassung:';
$mod_strings['LBL_COLOUR_ADMIN_MENUFONT'] = 'Menü Linkfarbe';
$mod_strings['LBL_COLOUR_ADMIN_MENUHOVER'] = 'Menüfarbe Mouseover:';
$mod_strings['LBL_COLOUR_ADMIN_MENULNKHVR'] = 'Menü Linkfarbe Mouseover:';
$mod_strings['LBL_COLOUR_ADMIN_MENUTOP'] = 'Gradient des oberen Menüs';
$mod_strings['LBL_COLOUR_ADMIN_PAGE'] = 'Seitenfarbe Einstellungen';
$mod_strings['LBL_COLOUR_ADMIN_PAGEHEADER'] = 'Seitenkopf Farben';
$mod_strings['LBL_COLOUR_ADMIN_PAGELINK'] = 'Seitenlink Farben';
$mod_strings['LBL_COLOUR_ADMIN_TABS'] = 'Menü Inhaltseinstellungen';
$mod_strings['LBL_COLOUR_ADMIN_TABSNUM'] = 'Anzahl an Menüs/Reiter:';
$mod_strings['LBL_COLOUR_ADMIN_TOPGR'] = 'Farbe oberer Verlauf:';
$mod_strings['LBL_COLOUR_ADMIN_VISITED'] = 'Farbe besuchte Links:';
$mod_strings['LBL_COLOUR_DESC'] = 'Passen Sie Ihr SuiteCRM Design an';
$mod_strings['LBL_COLOUR_SETTINGS'] = 'Design Einstellungen';
$mod_strings['LBL_DASHLET_COLUMNS'] = 'Anzahl Dashlet Spalten:';
$mod_strings['LBL_DEFAULT_ADMIN_DASHLET'] = 'Dashlet Spalteneinstellungen';
$mod_strings['LBL_DISPLAY_SIDEBAR'] = 'Sidebar anzeigen';



$mod_strings['LBL_REPAIR_RESCHEDULE_DONE'] = 'Wiedervorlagen erfolgreich repariert';
$mod_strings['LBL_RESCHEDULE_ADMIN'] = 'Wiedervorlage Einstellungen';
$mod_strings['LBL_RESCHEDULE_ADMIN_DESC'] = 'Konfigurieren und verwalten von Wiedervorlagen';
$mod_strings['LBL_RESCHEDULE_REBUILD'] = 'Wiedervorlagen reparieren';
$mod_strings['LBL_RESCHEDULE_REBUILD_DESC'] = 'repariert das Wiedervorlagen Modul';
$mod_strings['LBL_SALESAGILITY_ADMIN'] = 'Advanced OpenAdmin';



$mod_strings['LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_DESC'] = 'Erlaubt Zugang zu Informationen über den Adressen Cache.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_TITLE'] = 'Adressen Cache';
$mod_strings['LBL_JJWG_MAPS_ADMIN_CONFIG_DESC'] = 'Konfigurationseinstellungen für Google Maps';
$mod_strings['LBL_JJWG_MAPS_ADMIN_CONFIG_TITLE'] = 'Google Maps-Einstellungen';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DESC'] = 'Verwalten Sie Ihre Geokodierungen, testen Sie Geokodierungen, überprüfen Sie die Resultate und ändern Sie die erweiterten Einstellungen.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DONATE_DESC'] = 'Bitte ziehen Sie in Erwägung an dieses Projekt zu spenden!';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DONATE_TITLE'] = 'Spenden an dieses Projekt';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_DESC'] = 'Zeigt die Anzahl der geokodierten Modulobjekte gruppiert nach dem Ergebnis der Geokodierung an.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_TITLE'] = 'Anzahl Geokodierungen';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_DESC'] = 'Geokodieren Sie Ihre Objektadressen. Dieser Vorgang kann einige Minuten dauern!';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_TITLE'] = 'Adressen geokodieren';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_DESC'] = 'Führen Sie einen einzelnen Geokodierungstests mit detaillierten Resultaten durch.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_TITLE'] = 'Test Geokodierung';
$mod_strings['LBL_JJWG_MAPS_ADMIN_HEADER'] = 'Google Maps';



$mod_strings['LBL_AOS_ADMIN_CONTRACT_RENEWAL_REMINDER'] = 'Erinnerungsperiode Verlängerung';
$mod_strings['LBL_AOS_ADMIN_CONTRACT_SETTINGS'] = 'Vertragseinstellungen';
$mod_strings['LBL_AOS_ADMIN_ENABLE_LINE_ITEM_GROUPS'] = 'Zeilenelementgruppen erlauben';
$mod_strings['LBL_AOS_ADMIN_ENABLE_LINE_ITEM_TOTAL_TAX'] = 'Steuer zu Zeilensumme hinzufügen';
$mod_strings['LBL_AOS_ADMIN_INITIAL_INVOICE_NUMBER'] = 'Ursprüngliche Rechnungsnummer';
$mod_strings['LBL_AOS_ADMIN_INITIAL_QUOTE_NUMBER'] = 'Erste Angebotsnummer';
$mod_strings['LBL_AOS_ADMIN_INVOICE_SETTINGS'] = 'Einstellungen Rechnung';
$mod_strings['LBL_AOS_ADMIN_LINE_ITEM_SETTINGS'] = 'Zeilenelement Einstellungen';
$mod_strings['LBL_AOS_ADMIN_MANAGE_AOS'] = 'Advanced OpenSales Einstellungen';
$mod_strings['LBL_AOS_ADMIN_QUOTE_SETTINGS'] = 'Einstellungen Angebot';
$mod_strings['LBL_AOS_DAYS'] = 'Tage';
$mod_strings['LBL_AOS_EDIT'] = 'bearbeiten';
$mod_strings['LBL_AOS_PRODUCTS'] = 'AOS Produkte';
$mod_strings['LBL_AOS_SETTINGS'] = 'AOS Einstellungen';
$mod_strings['LBL_CHANGE_SETTINGS'] = 'Einstellungen für erweiterte OpenSale ändern';
$mod_strings['LBL_SALESAGILITY_ADMIN'] = 'Erweiterter OpenAdmin';



$mod_strings['LBL_CONFIG_SECURITYGROUPS'] = 'Die Einstellungen für Berechtigungsgruppen anpassen - Vererbungsregelen, additive Rechte usw.';
$mod_strings['LBL_CONFIG_SECURITYGROUPS_TITLE'] = 'Berechtigungsgruppen Einstellungen';
$mod_strings['LBL_MANAGE_SECURITYGROUPS'] = 'Berechtigungsgruppen bearbeiten';
$mod_strings['LBL_MANAGE_SECURITYGROUPS_TITLE'] = 'Berechtigungsgruppen Verwaltung';
$mod_strings['LBL_SECURITYGROUPS'] = 'Berechtigungsgruppen';
$mod_strings['LBL_SECURITYGROUPS_DASHLETPUSH'] = 'Machen Sie das Nachrichten Dashlet auf der Startseite aller Benutzer sichtbar. Dieser Vorgang kann, abhängig von der Anzahl der Benutzer, ein bisschen dauern.';
$mod_strings['LBL_SECURITYGROUPS_DASHLETPUSH_TITLE'] = 'Nachrichten Dashlet verteilen';
$mod_strings['LBL_SECURITYGROUPS_HOOKUP'] = 'Selbst erstellte Module mit Berechtigungsgruppen verbinden';
$mod_strings['LBL_SECURITYGROUPS_HOOKUP_TITLE'] = 'Modul verbinden';
$mod_strings['LBL_SECURITYGROUPS_INFO'] = 'Generelle Informationen';
$mod_strings['LBL_SECURITYGROUPS_INFO_TITLE'] = 'Berechtigungsgruppen Info';
$mod_strings['LBL_SECURITYGROUPS_SUGAROUTFITTERS'] = 'Holen Sie sich die neueste Version der Berechtigungsgruppen bzw. andere SugarCRM Module, Designs und Integrationen zusammen mit Bewertungen, Dokumenten, Unterstützung und von der Community bestätigten Versionen.';
$mod_strings['LBL_SECURITYGROUPS_SUGAROUTFITTERS_TITLE'] = 'SugarOutfitters';
$mod_strings['LBL_SECURITYGROUPS_UPGRADE_INFO_TITLE'] = 'Aktualisierung und generelle Infos';



$mod_strings['LBL_AOP_ADD_STATUS'] = 'Hinzufügen';
$mod_strings['LBL_AOP_ADMIN_MANAGE_AOP'] = 'Erweiterte OpenPortal Einstellungen';
$mod_strings['LBL_AOP_ASSIGNMENT_OPTIONS'] = 'Verteilungs-Ziel';
$mod_strings['LBL_AOP_ASSIGNMENT_USER'] = 'Verteilung Benutzer';
$mod_strings['LBL_AOP_BUSINESS_HOURS_SETTINGS'] = 'Öffnungszeiten';
$mod_strings['LBL_AOP_CASE_ASSIGNMENT'] = 'Fall Verteilung';
$mod_strings['LBL_AOP_CASE_CLOSURE_EMAIL_TEMPLATE'] = 'E-Mail Vorlage für geschlossenen Fall';
$mod_strings['LBL_AOP_CASE_CREATION_EMAIL_TEMPLATE'] = 'E-Mail Vorlage für erstellten Fall';
$mod_strings['LBL_AOP_CASE_REMINDER_EMAIL_TEMPLATE'] = 'E-Mail Vorlage für Fall Erinnerung';
$mod_strings['LBL_AOP_CASE_STATUS_SETTINGS'] = 'Fall Statusänderung';
$mod_strings['LBL_AOP_CLOSING_HOURS'] = 'Geöffnet bis';
$mod_strings['LBL_AOP_CONTACT_EMAIL_TEMPLATE'] = 'E-Mail Vorlage Kontakt';
$mod_strings['LBL_AOP_DISTRIBUTION_METHOD'] = 'Verteilungsmethode';
$mod_strings['LBL_AOP_EMAIL_SETTINGS'] = 'E-Mail Einstellungen';
$mod_strings['LBL_AOP_ENABLE_AOP'] = 'AOP aktivieren';
$mod_strings['LBL_AOP_ENABLE_PORTAL'] = 'Portal aktivieren';
$mod_strings['LBL_AOP_IF_STATUS'] = 'Wenn Status gleich';
$mod_strings['LBL_AOP_JOOMLA_ACCESS_KEY'] = 'Joomla Zugangsschlüssel';
$mod_strings['LBL_AOP_JOOMLA_ACCOUNT_CREATION_EMAIL_TEMPLATE'] = 'Kontoerstellungsvorlage Joomla Support Portal';
$mod_strings['LBL_AOP_JOOMLA_SETTINGS'] = 'Portal-Einstellungen';
$mod_strings['LBL_AOP_JOOMLA_URL'] = 'Joomla-URL';
$mod_strings['LBL_AOP_OPENING_DAYS'] = 'Geöffnete Tage';
$mod_strings['LBL_AOP_OPENING_HOURS'] = 'Geöffnet von';
$mod_strings['LBL_AOP_REMOVE_STATUS'] = 'Entfernen';
$mod_strings['LBL_AOP_SETTINGS'] = 'AOP Einstellungen';
$mod_strings['LBL_AOP_THEN_STATUS'] = 'Status ändern in';
$mod_strings['LBL_AOP_USER_EMAIL_TEMPLATE'] = 'E-Mail Vorlage Benutzer';
$mod_strings['LBL_CREATE_EMAIL_TEMPLATE'] = 'Erstellen';
$mod_strings['LBL_EDIT_EMAIL_TEMPLATE'] = 'Bearbeiten';
$mod_strings['LBL_SALESAGILITY_ADMIN'] = 'Erweiterter OpenAdmin';
$mod_strings['LBL_SINGLE_USER'] = 'Einzelbenutzer';
$mod_strings['LBL_SUPPORT_FROM_ADDRESS'] = 'Support E-Mail von Adresse';
$mod_strings['LBL_SUPPORT_FROM_NAME'] = 'Support E-Mail von Name';



$mod_strings['LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_DESC'] = 'Erlaubt Zugang zu Informationen über den Adressen Cache.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_TITLE'] = 'Adressen Cache';
$mod_strings['LBL_JJWG_MAPS_ADMIN_CONFIG_DESC'] = 'Konfigurationseinstellungen für Google Maps';
$mod_strings['LBL_JJWG_MAPS_ADMIN_CONFIG_TITLE'] = 'Google Maps-Einstellungen';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DESC'] = 'Verwalten Sie Ihre Geokodierungen, testen Sie Geokodierungen, überprüfen Sie die Resultate und ändern Sie die erweiterten Einstellungen.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DONATE_DESC'] = 'Bitte ziehen Sie eine Spende für dieses Projekt in Erwägung!';
$mod_strings['LBL_JJWG_MAPS_ADMIN_DONATE_TITLE'] = 'Spenden Sie an dieses Projekt';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_DESC'] = 'Zeigt die Anzahl der geokodierten Modulobjekte gruppiert nach dem Ergebnis der Geokodierung an.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_TITLE'] = 'Anzahl Geokodierungen';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_DESC'] = 'Geokodieren Sie Ihre Objektadressen. Dieser Vorgang kann einige Minuten dauern!';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_TITLE'] = 'Adressen geokodieren';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_DESC'] = 'Führen Sie einen einzelnen Geokodierungstests mit detaillierten Resultaten durch.';
$mod_strings['LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_TITLE'] = 'Test Geokodierung';
$mod_strings['LBL_JJWG_MAPS_ADMIN_HEADER'] = 'Google Maps';

?>