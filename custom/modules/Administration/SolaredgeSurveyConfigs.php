<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

global $app_list_strings, $db, $sugar_config;

$languages = $app_list_strings['country_0'];

$optionsLayouts = createDropDownListOptionsForLayouts($db);
$optionsLanguages = createDropDownListOptionsForLanguages($db,$sugar_config);
$configLayouts = $sugar_config['solaredge']['survey']['layout'];
$configLanguages = $sugar_config['solaredge']['survey']['language'];

$smarty = new Sugar_Smarty();		
$smarty->assign('languages', $languages);		
$smarty->assign('optionsLayouts', $optionsLayouts);		
$smarty->assign('optionsLanguages', $optionsLanguages);		
$smarty->assign('configLayouts', json_encode($configLayouts));		
$smarty->assign('configLanguages', json_encode($configLanguages));
$smarty->assign('saveText',translate('LBL_SAVE','dtbc_CasesSurvey'));
$smarty->display('custom/modules/Administration/tpls/SurveyConfigSettings.tpl');

function createDropDownListOptionsForLayouts($db){
    
    $layoutsSql = "SELECT * FROM dtbc_layouts WHERE module_name LIKE 'dtbc_CasesSurvey' AND deleted = 0;"; 
    $result = $db->query($layoutsSql);
    
    $dropdownListOptions = "<option value='default'>editviewdefs(default)</option>";
    while($record = $db->fetchByAssoc($result)){
        
        $id = $record["id"];
        $name = $record["name"];
        $dropdownListOptions .= "<option value='$id'>$name</option>";
    }
    return $dropdownListOptions;
}

function createDropDownListOptionsForLanguages($db,$sugar_config){
    
    $languages = $sugar_config['languages'];
    
    $dropdownListOptions = "";
    foreach($languages as $key => $value){
        
        $dropdownListOptions .= "<option value='$key'>$value</option>";
    }
     return $dropdownListOptions;
}