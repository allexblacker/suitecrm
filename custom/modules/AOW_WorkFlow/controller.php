<?php

require_once("modules/AOW_WorkFlow/aow_utils.php");

class AOW_WorkFlowController extends SugarController {

    protected function action_getModuleFields()
    {
        if (!empty($_REQUEST['aow_module']) && $_REQUEST['aow_module'] != '') {
            if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
                $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
            } else {
                $module = $_REQUEST['aow_module'];
            }
            echo getModuleFields($module,$_REQUEST['view'],$_REQUEST['aow_value']);
        }
        die;

    }

    protected function action_getRelatedModule()
    {
        if (!empty($_REQUEST['aow_module']) && $_REQUEST['aow_module'] != '') {
            if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
                $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
            } else {
                $module = $_REQUEST['aow_module'];
            }
            echo htmlspecialchars($module);
        }
        die;

    }

    protected function action_getModuleRelationships()
    {
        if (!empty($_REQUEST['aow_module']) && $_REQUEST['aow_module'] != '') {
            if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
                $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
            } else {
                $module = $_REQUEST['aow_module'];
            }
            echo getModuleRelationships($module);
        }
        die;

    }


    protected function action_getModuleOperatorField(){

        global $app_list_strings, $beanFiles, $beanList;

        if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
            $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        } else {
            $module = $_REQUEST['aow_module'];
        }
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';


        require_once($beanFiles[$beanList[$module]]);
        $focus = new $beanList[$module];
        $vardef = $focus->getFieldDefinition($fieldname);

        if($vardef){

            switch($vardef['type']) {
                case 'double':
                case 'decimal':
                case 'float':
                case 'currency':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null');
                    break;
                case 'uint':
                case 'ulong':
                case 'long':
                case 'short':
                case 'tinyint':
                case 'int':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null');
                    break;
                case 'date':
                case 'datetime':
                case 'datetimecombo':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null');
                    break;
                case 'enum':
                case 'multienum':
                $valid_opp = array('Equal_To','Not_Equal_To','is_null');
                    break;
                default:
                $valid_opp = array('Equal_To','Not_Equal_To','Contains', 'Starts_With', 'Ends_With','is_null');
                    break;
            }

            foreach($app_list_strings['aow_operator_list'] as $key => $keyValue){
                if(!in_array($key, $valid_opp)){
                    unset($app_list_strings['aow_operator_list'][$key]);
                }
            }



            $app_list_strings['aow_operator_list'];
            if($view == 'EditView'){
                echo "<select type='text' name='$aow_field' id='$aow_field ' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_operator_list'], $value) ."</select>";
            }else{
                echo $app_list_strings['aow_operator_list'][$value];
            }
        }
        die;

    }

    protected function action_getFieldTypeOptions(){

        global $app_list_strings, $beanFiles, $beanList;

        if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
            $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        } else {
            $module = $_REQUEST['aow_module'];
        }
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';


        require_once($beanFiles[$beanList[$module]]);
        $focus = new $beanList[$module];
        $vardef = $focus->getFieldDefinition($fieldname);

        switch($vardef['type']) {
            case 'double':
            case 'decimal':
            case 'float':
            case 'currency':
                $valid_opp = array('Value','Field','Any_Change');
                break;
            case 'uint':
            case 'ulong':
            case 'long':
            case 'short':
            case 'tinyint':
            case 'int':
                $valid_opp = array('Value','Field','Any_Change');
                break;
            case 'date':
            case 'datetime':
            case 'datetimecombo':
                $valid_opp = array('Value','Field','Any_Change','Date');
                break;
            case 'enum':
            case 'dynamicenum':    
            case 'multienum':
                $valid_opp = array('Value','Field','Any_Change', 'Multi');
                break;
            case 'relate':
            case 'id':
                $valid_opp = array('Value','Field','Any_Change', 'SecurityGroup');
                break;
            default:
                $valid_opp = array('Value','Field','Any_Change');
                break;
        }

        if(!file_exists('modules/SecurityGroups/SecurityGroup.php')){
            unset($app_list_strings['aow_condition_type_list']['SecurityGroup']);
        }
        foreach($app_list_strings['aow_condition_type_list'] as $key => $keyValue){
            if(!in_array($key, $valid_opp)){
                unset($app_list_strings['aow_condition_type_list'][$key]);
            }
        }

        if($view == 'EditView'){
            echo "<select type='text'  name='$aow_field' id='$aow_field' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_condition_type_list'], $value) ."</select>";
        }else{
            echo $app_list_strings['aow_condition_type_list'][$value];
        }
        die;

    }

    protected function action_getActionFieldTypeOptions(){

        global $app_list_strings, $beanFiles, $beanList;

        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';


        require_once($beanFiles[$beanList[$module]]);
        $focus = new $beanList[$module];
        $vardef = $focus->getFieldDefinition($fieldname);

        switch($vardef['type']) {
            case 'double':
            case 'decimal':
            case 'float':
            case 'currency':
                $valid_opp = array('Value','Field');
                break;
            case 'uint':
            case 'ulong':
            case 'long':
            case 'short':
            case 'tinyint':
            case 'int':
                $valid_opp = array('Value','Field');
                break;
            case 'date':
            case 'datetime':
            case 'datetimecombo':
                $valid_opp = array('Value','Field', 'Date');
                break;
            case 'enum':
            case 'multienum':
                $valid_opp = array('Value','Field');
                break;
            case 'relate':
                $valid_opp = array('Value','Field');
                if($vardef['module'] == 'Users') $valid_opp = array('Value','Field','Round_Robin','Least_Busy','Random');
                break;
            default:
                $valid_opp = array('Value','Field');
                break;
        }

        foreach($app_list_strings['aow_action_type_list'] as $key => $keyValue){
            if(!in_array($key, $valid_opp)){
                unset($app_list_strings['aow_action_type_list'][$key]);
            }
        }

        if($view == 'EditView'){
            echo "<select type='text'  name='$aow_field' id='$aow_field' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_action_type_list'], $value) ."</select>";
        }else{
            echo $app_list_strings['aow_action_type_list'][$value];
        }
        die;

    }

    protected function action_getModuleFieldType()
    {
        if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
            $rel_module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        } else {
            $rel_module = $_REQUEST['aow_module'];
        }
        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        switch($_REQUEST['aow_type']) {
            case 'Field':
                if(isset($_REQUEST['alt_module']) && $_REQUEST['alt_module'] != '') $module = $_REQUEST['alt_module'];
                if($view == 'EditView'){
                    echo "<select type='text'  name='$aow_field' id='$aow_field ' title='' tabindex='116'>". getModuleFields($module, $view, $value, getValidFieldsTypes($module, $fieldname)) ."</select>";
                }else{
                    echo getModuleFields($module, $view, $value);
                }
                break;
            case 'Any_Change';
                echo '';
                break;
            case 'Date':
                echo getDateField($module, $aow_field, $view, $value, false);
                break;
            case 'Multi':
                echo $this->getCustomModuleField($rel_module,$fieldname, $aow_field, $view, $value,'multienum');
                break;
            case 'SecurityGroup':
                $module = 'Accounts';
                $fieldname = 'SecurityGroups';
            case 'Value':
            default:
                echo $this->getCustomModuleField($rel_module, $fieldname, $aow_field, $view, $value );
                break;
        }
        die;

    }
	
	private function getCustomModuleField($module, $fieldname, $aow_field, $view='EditView',$value = '', $alt_type = '', $currency_id = '', $params= array()) {
		global $current_language, $app_strings, $app_list_strings, $current_user, $beanFiles, $beanList;

		// use the mod_strings for this module
		$mod_strings = return_module_language($current_language,$module);

		// set the filename for this control
		$file = create_cache_directory('modules/AOW_WorkFlow/') . $module . $view . $alt_type . $fieldname . '.tpl';

		$displayParams = array();

		if ( !is_file($file)
			|| inDeveloperMode()
			|| !empty($_SESSION['developerMode']) ) {

			if ( !isset($vardef) ) {
				require_once($beanFiles[$beanList[$module]]);
				$focus = new $beanList[$module];
				$vardef = $focus->getFieldDefinition($fieldname);
			}

			// Bug: check for AOR value SecurityGroups value missing
			if(stristr($fieldname, 'securitygroups') != false && empty($vardef)) {
				require_once($beanFiles[$beanList['SecurityGroups']]);
				$module = 'SecurityGroups';
				$focus = new $beanList[$module];
				$vardef = $focus->getFieldDefinition($fieldname);
			}


			//$displayParams['formName'] = 'EditView';

			// if this is the id relation field, then don't have a pop-up selector.
			if( $vardef['type'] == 'relate' && $vardef['id_name'] == $vardef['name']) {
				$vardef['type'] = 'varchar';
			}

			if(isset($vardef['precision'])) unset($vardef['precision']);

			//$vardef['precision'] = $locale->getPrecedentPreference('default_currency_significant_digits', $current_user);

			//TODO Fix datetimecomebo
			//temp work around
			if( $vardef['type'] == 'datetimecombo') {
				$vardef['type'] = 'datetime';
			}

			// trim down textbox display
			if( $vardef['type'] == 'text' ) {
				$vardef['rows'] = 2;
				$vardef['cols'] = 32;
			}

			// create the dropdowns for the parent type fields
			if ( $vardef['type'] == 'parent_type' ) {
				$vardef['type'] = 'enum';
			}

			if($vardef['type'] == 'link'){
				$vardef['type'] = 'relate';
				$vardef['rname'] = 'name';
				$vardef['id_name'] = $vardef['name'].'_id';
				if((!isset($vardef['module']) || $vardef['module'] == '') && $focus->load_relationship($vardef['name'])) {
					$relName = $vardef['name'];
					$vardef['module'] = $focus->$relName->getRelatedModuleName();
				}

			}

			//check for $alt_type
			if ( $alt_type != '' ) {
				$vardef['type'] = $alt_type;
			}

			// remove the special text entry field function 'getEmailAddressWidget'
			if ( isset($vardef['function'])
				&& ( $vardef['function'] == 'getEmailAddressWidget'
					|| $vardef['function']['name'] == 'getEmailAddressWidget' ) )
				unset($vardef['function']);

			if(isset($vardef['name']) && ($vardef['name'] == 'date_entered' || $vardef['name'] == 'date_modified')){
				$vardef['name'] = 'aow_temp_date';
			}

			// load SugarFieldHandler to render the field tpl file
			static $sfh;

			if(!isset($sfh)) {
				require_once('include/SugarFields/SugarFieldHandler.php');
				$sfh = new SugarFieldHandler();
			}

			$contents = $sfh->displaySmarty('fields', $vardef, $view, $displayParams);

			// Remove all the copyright comments
			$contents = preg_replace('/\{\*[^\}]*?\*\}/', '', $contents);

			if( $view == 'EditView' &&  ($vardef['type'] == 'relate' || $vardef['type'] == 'parent')){
				$contents = str_replace('"'.$vardef['id_name'].'"','{/literal}"{$fields.'.$vardef['name'].'.id_name}"{literal}', $contents);
				$contents = str_replace('"'.$vardef['name'].'"','{/literal}"{$fields.'.$vardef['name'].'.name}"{literal}', $contents);
			}

			// hack to disable one of the js calls in this control
			if ( isset($vardef['function']) && ( $vardef['function'] == 'getCurrencyDropDown' || $vardef['function']['name'] == 'getCurrencyDropDown' ) )
				$contents .= "{literal}<script>function CurrencyConvertAll() { return; }</script>{/literal}";

			// Save it to the cache file
			if($fh = @sugar_fopen($file, 'w')) {
				fputs($fh, $contents);
				fclose($fh);
			}
		}

		// Now render the template we received
		$ss = new Sugar_Smarty();

		// Create Smarty variables for the Calendar picker widget
		global $timedate;
		$time_format = $timedate->get_user_time_format();
		$date_format = $timedate->get_cal_date_format();
		$ss->assign('USER_DATEFORMAT', $timedate->get_user_date_format());
		$ss->assign('TIME_FORMAT', $time_format);
		$time_separator = ":";
		$match = array();
		if(preg_match('/\d+([^\d])\d+([^\d]*)/s', $time_format, $match)) {
			$time_separator = $match[1];
		}
		$t23 = strpos($time_format, '23') !== false ? '%H' : '%I';
		if(!isset($match[2]) || $match[2] == '') {
			$ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M");
		}
		else {
			$pm = $match[2] == "pm" ? "%P" : "%p";
			$ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M" . $pm);
		}

		$ss->assign('CALENDAR_FDOW', $current_user->get_first_day_of_week());

		// populate the fieldlist from the vardefs
		$fieldlist = array();
		if ( !isset($focus) || !($focus instanceof SugarBean) )
			require_once($beanFiles[$beanList[$module]]);
		$focus = new $beanList[$module];
		// create the dropdowns for the parent type fields
		$vardefFields = $focus->getFieldDefinitions();
		if (isset($vardefFields[$fieldname]['type']) && $vardefFields[$fieldname]['type'] == 'parent_type' ) {
			$focus->field_defs[$fieldname]['options'] = $focus->field_defs[$vardefFields[$fieldname]['group']]['options'];
		}
		foreach ( $vardefFields as $name => $properties ) {
			$fieldlist[$name] = $properties;
			// fill in enums
			if(isset($fieldlist[$name]['options']) && is_string($fieldlist[$name]['options']) && isset($app_list_strings[$fieldlist[$name]['options']]))
				$fieldlist[$name]['options'] = $app_list_strings[$fieldlist[$name]['options']];
			// Bug 32626: fall back on checking the mod_strings if not in the app_list_strings
			elseif(isset($fieldlist[$name]['options']) && is_string($fieldlist[$name]['options']) && isset($mod_strings[$fieldlist[$name]['options']]))
				$fieldlist[$name]['options'] = $mod_strings[$fieldlist[$name]['options']];
			// Bug 22730: make sure all enums have the ability to select blank as the default value.
			if(!isset($fieldlist[$name]['options']['']))
				$fieldlist[$name]['options'][''] = '';
		}

		// fill in function return values
		if ( !in_array($fieldname,array('email1','email2')) )
		{
			if (!empty($fieldlist[$fieldname]['function']['returns']) && $fieldlist[$fieldname]['function']['returns'] == 'html')
			{
				$function = $fieldlist[$fieldname]['function']['name'];
				// include various functions required in the various vardefs
				if ( isset($fieldlist[$fieldname]['function']['include']) && is_file($fieldlist[$fieldname]['function']['include']))
					require_once($fieldlist[$fieldname]['function']['include']);
				$_REQUEST[$fieldname] = $value;
				$value = $function($focus, $fieldname, $value, $view);

				$value = str_ireplace($fieldname, $aow_field, $value);
			}
		}

		if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'link'){
			$fieldlist[$fieldname]['id_name'] = $fieldlist[$fieldname]['name'].'_id';

			if((!isset($fieldlist[$fieldname]['module']) || $fieldlist[$fieldname]['module'] == '') && $focus->load_relationship($fieldlist[$fieldname]['name'])) {
				$relName = $fieldlist[$fieldname]['name'];
				$fieldlist[$fieldname]['module'] = $focus->$relName->getRelatedModuleName();
			}
		}

		if(isset($fieldlist[$fieldname]['name']) && ($fieldlist[$fieldname]['name'] == 'date_entered' || $fieldlist[$fieldname]['name'] == 'date_modified')){
			$fieldlist[$fieldname]['name'] = 'aow_temp_date';
			$fieldlist['aow_temp_date'] = $fieldlist[$fieldname];
			$fieldname = 'aow_temp_date';
		}

		$quicksearch_js = '';
		if(isset( $fieldlist[$fieldname]['id_name'] ) && $fieldlist[$fieldname]['id_name'] != '' && $fieldlist[$fieldname]['id_name'] != $fieldlist[$fieldname]['name']){
			$rel_value = $value;

			require_once("include/TemplateHandler/TemplateHandler.php");
			$template_handler = new TemplateHandler();
			$quicksearch_js = $template_handler->createQuickSearchCode($fieldlist,$fieldlist,$view);
			$quicksearch_js = str_replace($fieldname, $aow_field.'_display', $quicksearch_js);
			$quicksearch_js = str_replace($fieldlist[$fieldname]['id_name'], $aow_field, $quicksearch_js);

			echo $quicksearch_js;

			if(isset($fieldlist[$fieldname]['module']) && $fieldlist[$fieldname]['module'] == 'Users'){
				$rel_value = get_assigned_user_name($value);
			} else if(isset($fieldlist[$fieldname]['module'])){
				require_once($beanFiles[$beanList[$fieldlist[$fieldname]['module']]]);
				$rel_focus = new $beanList[$fieldlist[$fieldname]['module']];
				$rel_focus->retrieve($value);
				if(isset($fieldlist[$fieldname]['rname']) && $fieldlist[$fieldname]['rname'] != ''){
					$relDisplayField = $fieldlist[$fieldname]['rname'];
				} else {
					$relDisplayField = 'name';
				}
				$rel_value = $rel_focus->$relDisplayField;
			}

			$fieldlist[$fieldlist[$fieldname]['id_name']]['value'] = $value;
			$fieldlist[$fieldname]['value'] = $rel_value;
			$fieldlist[$fieldname]['id_name'] = $aow_field;
			$fieldlist[$fieldlist[$fieldname]['id_name']]['name'] = $aow_field;
			$fieldlist[$fieldname]['name'] = $aow_field.'_display';
		} else if(isset( $fieldlist[$fieldname]['type'] ) && $view == 'DetailView' && ($fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' || $fieldlist[$fieldname]['type'] == 'date')){
			$value = $focus->convertField($value, $fieldlist[$fieldname]);
			if(!empty($params['date_format']) && isset($params['date_format'])){
				$convert_format = "Y-m-d H:i:s";
				if($fieldlist[$fieldname]['type'] == 'date') $convert_format = "Y-m-d";
				$fieldlist[$fieldname]['value'] = $timedate->to_display($value, $convert_format, $params['date_format']);
			}else{
				$fieldlist[$fieldname]['value'] = $timedate->to_display_date_time($value, true, true);
			}
			$fieldlist[$fieldname]['name'] = $aow_field;
		} else if(isset( $fieldlist[$fieldname]['type'] ) && ($fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' || $fieldlist[$fieldname]['type'] == 'date')){
			$value = $focus->convertField($value, $fieldlist[$fieldname]);
			$fieldlist[$fieldname]['value'] = $timedate->to_display_date($value);
			$fieldlist[$fieldname]['name'] = $aow_field;
		} else {
			$fieldlist[$fieldname]['value'] = $value;
			$fieldlist[$fieldname]['name'] = $aow_field;
		}
		
		// Set proper value for boolean field types
		if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'bool'){
			$boolean_false_values = array('off', 'false', '0', 'no');
			$fieldlist[$fieldname]['value'] = in_array(strval($value), $boolean_false_values) ? '0' : '1';
		}

		if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'currency' && $view != 'EditView'){
			static $sfh;

			if(!isset($sfh)) {
				require_once('include/SugarFields/SugarFieldHandler.php');
				$sfh = new SugarFieldHandler();
			}

			if($currency_id != '' && !stripos($fieldname, '_USD')){
				$userCurrencyId = $current_user->getPreference('currency');
				if($currency_id != $userCurrencyId){
					$currency = new Currency();
					$currency->retrieve($currency_id);
					$value = $currency->convertToDollar($value);
					$currency->retrieve($userCurrencyId);
					$value = $currency->convertFromDollar($value);
				}
			}

			$parentfieldlist[strtoupper($fieldname)] = $value;

			return ($sfh->displaySmarty($parentfieldlist, $fieldlist[$fieldname], 'ListView', $displayParams));
		}

		$ss->assign("QS_JS", $quicksearch_js);
		$ss->assign("fields",$fieldlist);
		$ss->assign("form_name",$view);
		$ss->assign("bean",$focus);

		// add in any additional strings
		$ss->assign("MOD", $mod_strings);
		$ss->assign("APP", $app_strings);

		return $ss->fetch($file);
	}

    protected function action_getModuleFieldTypeSet()
    {
        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        switch($_REQUEST['aow_type']) {
            case 'Field':
                $valid_fields = getValidFieldsTypes($module, $fieldname);
                if(isset($_REQUEST['alt_module']) && $_REQUEST['alt_module'] != '') $module = $_REQUEST['alt_module'];
                if($view == 'EditView'){
                    echo "<select type='text'  name='$aow_field' id='$aow_field ' title='' tabindex='116'>". getModuleFields($module, $view, $value, $valid_fields) ."</select>";
                }else{
                    echo getModuleFields($module, $view, $value);
                }
                break;
            case 'Date':
                if(isset($_REQUEST['alt_module']) && $_REQUEST['alt_module'] != '') $module = $_REQUEST['alt_module'];
                echo getDateField($module, $aow_field, $view, $value);
                break;
            Case 'Round_Robin';
            Case 'Least_Busy';
            Case 'Random';
                echo getAssignField($aow_field, $view, $value);
                break;
            case 'Value':
            default:
                echo $this->getCustomModuleField($module,$fieldname, $aow_field, $view, $value );
                break;
        }
        die;

    }

    protected function action_getModuleField()
    {
        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        echo $this->getCustomModuleField($_REQUEST['aow_module'],$_REQUEST['aow_fieldname'], $_REQUEST['aow_newfieldname'], $view, $value );
        die;
    }

    protected function action_getRelFieldTypeSet()
    {
        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        switch($_REQUEST['aow_type']) {
            case 'Field':
                if(isset($_REQUEST['alt_module']) && $_REQUEST['alt_module'] != '') $module = $_REQUEST['alt_module'];
                if($view == 'EditView'){
                    echo "<select type='text'  name='$aow_field' id='$aow_field ' title='' tabindex='116'>". getModuleFields($module, $view, $value) ."</select>";
                }else{
                    echo getModuleFields($module, $view, $value);
                }
                break;
            case 'Value':
            default:
                echo $this->getCustomModuleField($module,$fieldname, $aow_field, $view, $value );
                break;
        }
        die;

    }

    protected function action_getRelActionFieldTypeOptions(){

        global $app_list_strings, $beanFiles, $beanList;

        $module = $_REQUEST['aow_module'];
        $alt_module = $_REQUEST['alt_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';


        require_once($beanFiles[$beanList[$module]]);
        $focus = new $beanList[$module];
        $vardef = $focus->getFieldDefinition($fieldname);
        $valid_opp = array('Value','Field');

        foreach($app_list_strings['aow_rel_action_type_list'] as $key => $keyValue){
            if(!in_array($key, $valid_opp)){
                unset($app_list_strings['aow_rel_action_type_list'][$key]);
            }
        }

        if($view == 'EditView'){
            echo "<select type='text'  name='$aow_field' id='$aow_field' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_rel_action_type_list'], $value) ."</select>";
        }else{
            echo $app_list_strings['aow_rel_action_type_list'][$value];
        }
        die;

    }

    protected function action_getAction(){
        global $beanList, $beanFiles;

        $action_name = 'action'.$_REQUEST['aow_action'];
        $line = $_REQUEST['line'];

        if($_REQUEST['aow_module'] == '' || !isset($beanList[$_REQUEST['aow_module']])){
            echo '';
            die;
        }

        if(file_exists('custom/modules/AOW_Actions/actions/'.$action_name.'.php')){

            require_once('custom/modules/AOW_Actions/actions/'.$action_name.'.php');

        } else if(file_exists('modules/AOW_Actions/actions/'.$action_name.'.php')){

            require_once('modules/AOW_Actions/actions/'.$action_name.'.php');

        } else {
            echo '';
            die;
        }

        $custom_action_name = "custom" . $action_name;
        if(class_exists($custom_action_name)){
            $action_name = $custom_action_name;
        }

        $id = '';
        $params = array();
        if(isset($_REQUEST['id'])){
            require_once('modules/AOW_Actions/AOW_Action.php');
            $aow_action = new AOW_Action();
            $aow_action->retrieve($_REQUEST['id']);
            $id = $aow_action->id;
            $params = unserialize(base64_decode($aow_action->parameters));
        }

        $action = new $action_name($id);

        require_once($beanFiles[$beanList[$_REQUEST['aow_module']]]);
        $bean = new $beanList[$_REQUEST['aow_module']];
        echo $action->edit_display($line,$bean,$params);
        die;
    }

    protected function action_getEmailField()
    {
        $module = $_REQUEST['aow_module'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        switch($_REQUEST['aow_type']) {
            case 'Record Email';
                echo '';
                break;
            case 'Related Field':
                $rel_field_list = getRelatedEmailableFields($module);
                if($view == 'EditView'){
                    echo "<select type='text'  name='$aow_field' id='$aow_field' title='' tabindex='116'>". get_select_options_with_id($rel_field_list, $value) ."</select>";
                }else{
                    echo $rel_field_list[$value];
                }
                break;
            case 'Specify User':
                echo $this->getCustomModuleField('Accounts','assigned_user_name', $aow_field, $view, $value);
                break;
            case 'Users':
                echo getAssignField($aow_field, $view, $value);
                break;
            case 'Email Address':
            default:
                if($view == 'EditView'){
                    echo "<input type='text' name='$aow_field' id='$aow_field' size='25' title='' tabindex='116' value='$value'>";
                }else{
                    echo $value;
                }
                break;
        }
        die;

    }


    protected function action_testFlow(){

        echo 'Started<br />';
        require_once('modules/AOW_WorkFlow/AOW_WorkFlow.php');
        $workflow = new AOW_WorkFlow();

        if($workflow->run_flows())echo 'PASSED';

    }

}
