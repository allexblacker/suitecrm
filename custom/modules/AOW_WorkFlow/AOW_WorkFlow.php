<?php

require_once("custom/include/dtbc/helpers.php");

class CustomAOW_WorkFlow extends AOW_WorkFlow {
		
	public function __construct(){
		parent::__construct();
	}
	
	function run_flows() {
		
		if (!$this->checkAndSetRunning())
			return true;
		
		global $sugar_config;
		
		// Run all the workflows from the scheduler
		$query = "SELECT id FROM aow_workflow WHERE aow_workflow.status = 'Active'  AND (aow_workflow.run_when = 'Always' OR aow_workflow.run_when = 'In_Scheduler' OR aow_workflow.run_when = 'Create') AND deleted = 0 ORDER BY custom_order_c ASC ";
        
		$result = $this->db->query($query, false);
		$flow = new CustomAOW_WorkFlow();
		while (($row = $this->db->fetchByAssoc($result)) != null) {
			$flow->retrieve($row['id']);
			$flow->run_flow();
		}
		
		// Check case's groups and set to "Tier 1 IL" if don't have one
		$sql = "SELECT *
				FROM cases
				WHERE date_entered >= NOW() - INTERVAL 24 HOUR AND
				NOT EXISTS (SELECT 1 FROM securitygroups_records WHERE record_id = cases.id) AND
				cases.deleted = 0";

		$result = $this->db->query($sql);

		while ($row = $this->db->fetchByAssoc($result)) {
			// Insert default group
			$sql = "INSERT INTO securitygroups_records
					(id,
					securitygroup_id,
					record_id,
					module,
					date_modified,
					modified_user_id,
					created_by,
					deleted)
					VALUES
					(UUID(),
					" . $this->db->quoted($sugar_config['solaredge']['default_group_id']) . ",
					" . $this->db->quoted($row['id']) . ",
					" . $this->db->quoted("Cases") . ",
					NOW(),
					1,
					NOW(),
					0);
					";
			$this->db->query($sql);
		}
		
		$this->setRunningOver();
		
        return true;
	}
	
	private function setRunningOver() {
		if (isset($_REQUEST['module']) && $_REQUEST['module'] == 'Schedulers' && isset($_REQUEST['record'])) {
			// Remove the sign which says "I'm running" - so 'stop' the scheduler
			$customId = str_replace("-", "", $_REQUEST['record']);
			$sql = "DELETE FROM job_queue WHERE scheduler_id = " . $this->db->quoted($customId);
			$this->db->query($sql);
		}
	}
	
	private function checkAndSetRunning() {
		if (isset($_REQUEST['module']) && $_REQUEST['module'] == 'Schedulers' && isset($_REQUEST['record'])) {
			$customId = str_replace("-", "", $_REQUEST['record']);
			$sql = "SELECT scheduler_id, date_entered, NOW() AS currentdate FROM job_queue WHERE deleted = 0 AND scheduler_id = " . $this->db->quoted($customId);
			$res = $this->db->fetchOne($sql);
			
			if ($res != null && is_array($res) && count($res) > 0 && $res['scheduler_id'] == $customId) {
				// If the running cycle is less than 24 hours return false and don't allow to run twice the scheduler, 
				// else reset it an start.
				$current = new DateTime($res['currentdate']);
				$start = new DateTime($res['date_entered']);
				$diff = $current->diff($start);
				if ($diff->format("%d") > 0)
					$this->setRunningOver();
				else
					return false;
			}
			
			// If the shceduler doesn't run, make a record in the database which means it's running
			$sql = "INSERT INTO job_queue
					(assigned_user_id,
					id,
					name,
					deleted,
					date_entered,
					date_modified,
					scheduler_id,
					execute_time,
					status,
					resolution,
					message,
					target,
					data,
					requeue,
					retry_count,
					failure_count,
					job_delay,
					client,
					percent_complete)
					VALUES
					(1,
					UUID(),
					'CustomAOW_WorkFlow is running',
					0,
					NOW(),
					NOW(),
					" . $this->db->quoted($customId) . ",
					NOW(),
					'done',
					'success',
					'While you see this, the scheduler runs in the background',
					'CustomAOW_WorkFlow',
					" . $this->db->quoted('Real scheduler id is: ' . $_REQUEST['record']) . ",
					0,
					NULL,
					NULL,
					0,
					'temp',
					NULL);
					";
			$this->db->query($sql);
		}
		return true;
	}
	
	// Override from the core
	function run_flow() {
		global $beanList;

        $beans = $this->get_flow_beans(true);
        if (!empty($beans)) {
			$bean = new $beanList[$this->flow_module]();
            foreach ($beans as $beanId) {
                $bean->retrieve($beanId);
                $this->run_actions($bean, false, true);
            }
        }
    }
	
	function run_bean_flows(SugarBean &$bean){
		if ($bean->module_dir == 'AOW_Processed' || $bean->module_dir == 'AOW_Conditions' || $bean->module_dir == 'AOW_Actions' || $bean->module_dir == 'AOW_WorkFlow')
			return true;
		
        if(!isset($_REQUEST['module']) || $_REQUEST['module'] != 'Import'){

			$query = "SELECT id FROM aow_workflow WHERE aow_workflow.flow_module = '".$bean->module_dir."' AND aow_workflow.status = 'Active' AND (aow_workflow.run_when = 'Always' OR aow_workflow.run_when = 'On_Save' OR aow_workflow.run_when = 'Create') AND aow_workflow.deleted = 0 ORDER BY custom_order_c ASC ";
            $result = $this->db->query($query, false);
            $flow = new CustomAOW_WorkFlow();
            
			while (($row = $this->db->fetchByAssoc($result)) != null){
                $flow->retrieve($row['id']);
                if ($flow->check_valid_bean($bean))
                    $flow->run_actions($bean, true);
            }
        }
        return true;
    }

    function build_query_where(AOW_Condition $condition, $module, $query = array()){
        global $beanList, $app_list_strings, $sugar_config, $timedate;
        $path = unserialize(base64_decode($condition->module_path));
		
        $condition_module = $module;
        $table_alias = $condition_module->table_name;
        if(isset($path[0]) && $path[0] != $module->module_dir){
            foreach($path as $rel){
                $query = $this->build_flow_query_join($rel, $condition_module, 'relationship', $query);
                $condition_module = new $beanList[getRelatedModule($condition_module->module_dir,$rel)];
                $table_alias = $rel;
            }
        }

        if(isset($app_list_strings['aow_sql_operator_list'][$condition->operator])){
            $where_set = false;

            $data = $condition_module->field_defs[$condition->field];

            if($data['type'] == 'relate' && isset($data['id_name'])) {
                $condition->field = $data['id_name'];
            }
            if(  (isset($data['source']) && $data['source'] == 'custom_fields')) {
                $field = $table_alias.'_cstm.'.$condition->field;
                $query = $this->build_flow_query_join($table_alias.'_cstm', $condition_module, 'custom', $query);
				// Set the proper alias - Fixed by B
				// In some cases the main table alias is the name of the relation, but in the cstm table the query uses the original name, not the alias
				// eg: 
				// LEFT JOIN contacts contact_created_by ON cases.contact_created_by_id=contact_created_by.id AND contact_created_by.deleted=0
				// LEFT JOIN contacts_cstm contact_created_by_cstm ON contacts.id = contact_created_by_cstm.id_c
				// The query will failed because of the alias. The following few lines will figure out the real alias and replace it in the join.
				if (isset($query['join'][$table_alias])) {
					$subQuery1 = substr($query['join'][$table_alias], strlen("LEFT JOIN " . $condition_module->table_name));
					if (substr($subQuery1, 0, 1) == " ") {
						$tableAlias = trim(substr($subQuery1, 1, strpos($subQuery1, " ON")));
						if (strlen($tableAlias) > 0) {
							$query['join'][$table_alias . '_cstm'] = str_replace($condition_module->table_name . ".", $tableAlias . ".", $query['join'][$table_alias . '_cstm']);
						}
					}					
				}			
            } else {
                $field = $table_alias.'.'.$condition->field;
            }

            if($condition->operator == 'is_null'){
                $query['where'][] = '('.$field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator].' OR '.$field.' '.$app_list_strings['aow_sql_operator_list']['Equal_To']." '')";
                return $query;
            }
			
			if($condition->operator == 'is_not_null'){
                $query['where'][] = '('.$field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator].' AND '.$field.' '.$app_list_strings['aow_sql_operator_list']['Not_Equal_To']." '')";
                return $query;
            }

            switch($condition->value_type) {
                case 'Field':
                    $data = $module->field_defs[$condition->value];

                    if($data['type'] == 'relate' && isset($data['id_name'])) {
                        $condition->value = $data['id_name'];
                    }
                    if(  (isset($data['source']) && $data['source'] == 'custom_fields')) {
                        $value = $module->table_name.'_cstm.'.$condition->value;
                        $query = $this->build_flow_query_join($module->table_name.'_cstm', $module, 'custom', $query);
                    } else {
                        $value = $module->table_name.'.'.$condition->value;
                    }
                    break;
                case 'Any_Change':
                    //can't detect in scheduler so return
                    return array();
                case 'Date':
                    $params =  unserialize(base64_decode($condition->value));
                    if($params[0] == 'now'){
                        if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                            $value  = 'GetUTCDate()';
                        } else {
                            $value = 'UTC_TIMESTAMP()';
                        }
                    } else if($params[0] == 'today'){
                        if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                            //$field =
                            $value  = 'CAST(GETDATE() AS DATE)';
                        } else {
                            $field = 'DATE('.$field.')';
                            $value = 'Curdate()';
                        }
                    } else {
                        $data = $module->field_defs[$params[0]];
                        if(  (isset($data['source']) && $data['source'] == 'custom_fields')) {
                            $value = $module->table_name.'_cstm.'.$params[0];
                            $query = $this->build_flow_query_join($module->table_name.'_cstm', $module, 'custom', $query);
                        } else {
                            $value = $module->table_name.'.'.$params[0];
                        }
                    }

                    if($params[1] != 'now'){
                        switch($params[3]) {
                            case 'business_hours';
                                if(file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php') && $params[0] == 'now'){
                                    require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

                                    $businessHours = new AOBH_BusinessHours();

                                    $amount = $params[2];

                                    if($params[1] != "plus"){
                                        $amount = 0-$amount;
                                    }
                                    $value = $businessHours->addBusinessHours($amount);
                                    $value = "'".$timedate->asDb( $value )."'";
                                    break;
                                }
                                //No business hours module found - fall through.
                                $params[3] = 'hour';
                            default:
                                if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                                    $value = "DATEADD(".$params[3].",  ".$app_list_strings['aow_date_operator'][$params[1]]." $params[2], $value)";
                                } else {
                                    $value = "DATE_ADD($value, INTERVAL ".$app_list_strings['aow_date_operator'][$params[1]]." $params[2] ".$params[3].")";
                                }
                                break;
                        }
                    }
                    break;

                case 'Multi':
                    $sep = ' AND ';
                    if($condition->operator == 'Equal_To') $sep = ' OR ';
                    $multi_values = unencodeMultienum($condition->value);
                    if(!empty($multi_values)){
                        $value = '(';
                        if($data['type'] == 'multienum'){
                            $multi_operator =  $condition->operator == 'Equal_To' ? 'LIKE' : 'NOT LIKE';
                            foreach($multi_values as $multi_value){
                                if($value != '(') $value .= $sep;
                                $value .= $field." $multi_operator '%^".$multi_value."^%'";
                            }
                        }
                        else {
                            foreach($multi_values as $multi_value){
                                if($value != '(') $value .= $sep;
                                $value .= $field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator]." '".$multi_value."'";
                            }
                        }
                        $value .= ')';
                        $query['where'][] = $value;
                    }
                    $where_set = true;
                    break;
                case 'SecurityGroup':
                    $sgModule = $condition_module->module_dir;
                    if (isset($data['module']) && $data['module'] !== '') {
                        $sgModule = $data['module'];
                    }
                    $sql = 'EXISTS (SELECT 1 FROM securitygroups_records WHERE record_id = ' . $field . " AND module = '" . $sgModule . "' AND securitygroup_id = '" . $condition->value . "' AND deleted=0)";
                    if ($sgModule === 'Users') {
                        $sql = 'EXISTS (SELECT 1 FROM securitygroups_users WHERE user_id = ' . $field . " AND securitygroup_id = '" . $condition->value . "' AND deleted=0)";
                    }
                    $query['where'][] = $sql;
                    $where_set = true;
                    break;
                case 'Value':
                default:
                    $value = "'".$condition->value."'";
                    break;
            }

            //handle like conditions
            Switch($condition->operator) {
                case 'Contains':
                    $value = "CONCAT('%', ".$value." ,'%')";
                    break;
				case 'Not_Contains':
                    $value = "CONCAT('%', ".$value." ,'%')";
                    break;
                case 'Starts_With':
                    $value = "CONCAT(".$value." ,'%')";
                    break;
                case 'Ends_With':
                    $value = "CONCAT('%', ".$value.")";
                    break;
            }


            if(!$where_set) $query['where'][] = $field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator].' '.$value;
        }

        return $query;

    }

    function compare_condition($var1, $var2, $operator = 'Equal_To'){
        switch ($operator) {
            case "Not_Equal_To": return $var1 != $var2;
            case "Greater_Than":  return floatval($var1) >  floatval($var2);
            case "Less_Than":  return floatval($var1) <  floatval($var2);
            case "Greater_Than_or_Equal_To": return floatval($var1) >= floatval($var2);
            case "Less_Than_or_Equal_To": return floatval($var1) <= floatval($var2);
            case "Contains" : return strpos(strtolower($var1), strtolower($var2)) !== FALSE;
            case "Not_Contains" : return strpos(strtolower($var1), strtolower($var2)) === FALSE;
            case "Starts_With" : return $this->startsWith(strtolower($var1), strtolower($var2));
            case "Ends_With" : return $this->endsWith(strtolower($var1), strtolower($var2));
            case "is_null": return $var1 == null || $var1 == '';
            case "is_not_null": return $var1 != null && $var1 != '';
            case "One_of":
                if(is_array($var1)){
                    foreach($var1 as $var){
                        if(in_array($var,$var2)) return true;
                    }
                    return false;
                }
                else return in_array($var1,$var2);
            case "Not_One_of":
                if(is_array($var1)){
                    foreach($var1 as $var){
                        if(in_array($var,$var2)) return false;
                    }
                    return true;
                }
                else return !in_array($var1,$var2);
            case "Equal_To":
            default: return $var1 == $var2;
        }
    }
	
	private function startsWith($searchFrom, $searchText) {
		$length = strlen($searchText);
		return (substr($searchFrom, 0, $length) === $searchText);
	}

	private function endsWith($searchFrom, $searchText) {
		$length = strlen($searchText);
		return $length === 0 || (substr($searchFrom, -$length) === $searchText);
	}
	
	// Override the core and use aowProcessChecked variable to be able to skip processed table check
	function run_actions(SugarBean &$bean, $in_save = false, $aowProcessChecked = false){

        require_once('modules/AOW_Processed/AOW_Processed.php');
        $processed = new AOW_Processed();
        if (!$this->multiple_runs && !$aowProcessChecked) {
            $processed->retrieve_by_string_fields(array('aow_workflow_id' => $this->id,'parent_id' => $bean->id));

            if($processed->status == 'Complete'){
                //should not have gotten this far, so return
                return true;
            }
        }
        $processed->aow_workflow_id = $this->id;
        $processed->parent_id = $bean->id;
        $processed->parent_type = $bean->module_dir;
        $processed->status = 'Running';
        $processed->save(false);
        $processed->load_relationship('aow_actions');

        $pass = true;

        $sql = "SELECT id FROM aow_actions WHERE aow_workflow_id = '".$this->id."' AND deleted = 0 ORDER BY action_order ASC";
        $result = $this->db->query($sql);

        while ($row = $this->db->fetchByAssoc($result)) {
            $action = new AOW_Action();
            $action->retrieve($row['id']);

            if($this->multiple_runs || !$processed->db->getOne("select id from aow_processed_aow_actions where aow_processed_id = '".$processed->id."' AND aow_action_id = '".$action->id."' AND status = 'Complete'")){
                $action_name = 'action'.$action->action;

                if(file_exists('custom/modules/AOW_Actions/actions/'.$action_name.'.php')){
                    require_once('custom/modules/AOW_Actions/actions/'.$action_name.'.php');
                } else if(file_exists('modules/AOW_Actions/actions/'.$action_name.'.php')){
                    require_once('modules/AOW_Actions/actions/'.$action_name.'.php');
                } else {
                    return false;
                }

                $custom_action_name = "custom" . $action_name;
                if(class_exists($custom_action_name)){
                    $action_name = $custom_action_name;
                }


                $flow_action = new $action_name($action->id);
                if(!$flow_action->run_action($bean, unserialize(base64_decode($action->parameters)), $in_save)){
                    $pass = false;
                    $processed->aow_actions->add($action->id, array('status' => 'Failed'));
                } else {
                    $processed->aow_actions->add($action->id, array('status' => 'Complete'));
                }
            }

        }

        if($pass) $processed->status = 'Complete';
        else $processed->status = 'Failed';
        $processed->save(false);

        return $pass;
    }
	
	// Overriding core, because the core give back beans which can kick the memory limit. This will give back only bean ids
	function get_flow_beans($customReturnValue = false){
        global $beanList;

        if($beanList[$this->flow_module]){
            $module = new $beanList[$this->flow_module]();

            $query = '';
            $query_array = array();

            $query_array['select'][] = $module->table_name.".id AS id";
            $query_array = $this->build_flow_query_where($query_array);

            if(!empty($query_array)){
                foreach ($query_array['select'] as $select){
                    $query .=  ($query == '' ? 'SELECT ' : ', ').$select;
                }

                $query .= ' FROM '.$module->table_name.' ';

                if(isset($query_array['join'])){
                    foreach ($query_array['join'] as $join){
                        $query .= $join;
                    }
                }
                if(isset($query_array['where'])){
                    $query_where = '';
                    foreach ($query_array['where'] as $where){
                        $query_where .=  ($query_where == '' ? 'WHERE ' : ' AND ').$where;
                    }
                    $query .= ' '.$query_where;
                }
				
				if ($customReturnValue) {
					// Get search result and return only the ids
					$module = null;
					$result = $this->db->query($query);
					$retval = array();
					while (($row = $this->db->fetchByAssoc($result)) != null) {
						$retval[] = $row['id'];
					}
					return $retval;
				}

				return $module->process_full_list_query($query);
            }

        }
		if ($customReturnValue)
			return array();
		
		return null;
    }

}
?>
