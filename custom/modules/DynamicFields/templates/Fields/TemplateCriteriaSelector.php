<?php
if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('modules/DynamicFields/templates/Fields/TemplateText.php');

class TemplateCriteriaSelector extends TemplateText {
	var $type = 'text';
	var $len = '';

	function __construct()
	{
		$this->vardef_map['is_typed'] = 'ext1';
		$this->vardef_map['options'] = 'ext2';
		$this->vardef_map['rule'] = 'ext3';
	}

	function set($values){
	   parent::set($values);
	   
	   if(!empty($this->ext1)){
	       $this->is_typed = $this->ext1;
	   }
	   
	   if(!empty($this->ext2)){
	       $this->options = $this->ext2;
	   }
	   
	   if(!empty($this->ext3)){
	       $this->rule = $this->ext3;
	   }
	}

	function get_field_def()
	{
		$def = parent::get_field_def();
		$def['studio'] = 'visible';
		$def['dbType'] = 'text';
		
		$def['is_typed'] = !empty($this->is_typed) ? $this->is_typed : $this->ext1;
		$def['options'] = !empty($this->options) ? $this->options : $this->ext2;
		$def['rule'] = !empty($this->rule) ? $this->rule : $this->ext3;

		return $def;
	}

	function get_db_default($modify = false)
	{
	    // TEXT columns in MySQL cannot have a DEFAULT value - let the Bean handle it on save
        return null; // Bug 16612 - null so that the get_db_default() routine in TemplateField doesn't try to set DEFAULT
    }

}
