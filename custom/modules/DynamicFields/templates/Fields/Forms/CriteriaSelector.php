<?php

 function get_body(&$ss, $vardef){	
	$package_strings = array();
	if(!empty($_REQUEST['view_package'])){
		$view_package = $_REQUEST['view_package'];
		if($view_package != 'studio') {
			require_once('modules/ModuleBuilder/MB/ModuleBuilder.php');
			$mb = new ModuleBuilder();
			$module =& $mb->getPackageModule($view_package, $_REQUEST['view_module']);
			$lang = $GLOBALS['current_language'];
			$module->mblanguage->generateAppStrings(false);
			$package_strings = $module->mblanguage->appListStrings[$lang.'.lang.php'];
		}
	}
	
	global $app_list_strings;
	$my_list_strings = $app_list_strings;
	$my_list_strings = array_merge($my_list_strings, $package_strings);
	foreach($my_list_strings as $key=>$value){
		if(!is_array($value)){
			unset($my_list_strings[$key]);
		}
	}
	$dropdowns = array_keys($my_list_strings);
	sort($dropdowns);
    $default_dropdowns = array();
    if(!empty($vardef['options']) && !empty($my_list_strings[$vardef['options']])){
    		$default_dropdowns = $my_list_strings[$vardef['options']];
    }else{
    	$key = $dropdowns[0];
    	$default_dropdowns = $my_list_strings[$key];
    }
    
    $selected_dropdown = '';
    if(!empty($vardef['options'])){
    	$selected_dropdown = $vardef['options'];

    }

	$ss->assign('dropdowns', $dropdowns);
	$ss->assign('default_dropdowns', $default_dropdowns);
	$ss->assign('selected_dropdown', $selected_dropdown);
	
	return $ss->fetch('custom/modules/DynamicFields/templates/Fields/Forms/CriteriaSelector.tpl');
 }
?>
