<?php
// created: 2017-10-30 15:51:24
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'date_of_completion' => 
  array (
    'type' => 'date',
    'vname' => 'Date_Of_Completion',
    'width' => '10%',
    'default' => true,
  ),
  'expiration_date' => 
  array (
    'type' => 'date',
    'vname' => 'Expiration_Date',
    'width' => '10%',
    'default' => true,
  ),
);