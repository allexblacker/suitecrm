<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
require_once('modules/AOS_Quotes/controller.php');
require_once('custom/include/dtbc/helpers.php');
require_once('custom/include/dtbc/DocumentGenerator.php');
require_once('custom/include/dtbc/router.php');

class CustomAOS_QuotesController extends AOS_QuotesController {

	public function action_generateDocument() {
		$docGen = new DocumentGenerator($_REQUEST['templateId'], $_REQUEST['record'], $_REQUEST['module']);
		$docGen->setGlobalVariables();
		$docGen->generateDocument();
		Router::returnToDetailView("AOS_Quotes", $_REQUEST['record']);
	}

	public function action_sync() {
		global $db;
		
		// Set all the sync flags to zero
		$sql = "UPDATE aos_quotes_cstm
				SET is_synced_c = 0
				WHERE id_c IN (SELECT id FROM aos_quotes WHERE opportunity_id = " . $db->quoted($this->bean->opportunity_id) . ")";
		$db->query($sql);
		
		// Set flag's value to the opposite
		$this->bean->is_synced_c = $this->bean->is_synced_c == 1 ? 0 : 1;
		$this->bean->save();
		
		if ($this->bean->is_synced_c == 1 && strlen($this->bean->opportunity_id) > 0)
			$this->syncAllLineItems($_REQUEST['record'], $this->bean->opportunity_id);
		
		Router::returnToDetailView("AOS_Quotes", $_REQUEST['record']);
	}
	
	private function syncAllLineItems($qId, $oppId) {
		global $db;
		$this->deleteAllLineItemsFromOpportunity($oppId);
		
		$lineItemIds = $this->getAllLineItemsFromQuote($qId);

		foreach ($lineItemIds as $liId) {
			// Duplicate line item
			$newId = $this->duplicateBean($liId);
			
			// Add relatio to the opportunity
			$sql = "INSERT INTO opportunities_dtbc_line_item_1_c
					(id, date_modified, deleted, opportunities_dtbc_line_item_1opportunities_ida, opportunities_dtbc_line_item_1dtbc_line_item_idb)
					VALUES (
						UUID(),
						NOW(),
						0,
						" . $db->quoted($oppId) . ",
						" . $db->quoted($newId) . "
					)";
			$db->query($sql);
			
			// Write back the new bean's id
			$sql = "UPDATE dtbc_line_item_cstm
					SET dtbc_line_item_id_c = " . $db->quoted($newId) . "
					WHERE id_c = " . $db->quoted($liId);
			$db->query($sql);
		}
	}
	
	private function duplicateBean($oldId) {
		global $db;
		$newId = create_guid();
		
		$sql = "SELECT * 
				FROM dtbc_line_item 
				JOIN dtbc_line_item_cstm ON id_c = id
				WHERE id = " . $db->quoted($oldId);
		
		$oldBean = $db->fetchOne($sql);
		
		if (empty($oldBean) || (!empty($oldBean) && count($oldBean) == 0))
			return;

		$sql = "INSERT INTO dtbc_line_item
				(id,
				name,
				date_entered,
				date_modified,
				modified_user_id,
				created_by,
				description,
				deleted,
				assigned_user_id)
				VALUES (
					" . $db->quoted($newId) . ",
					" . $db->quoted($oldBean['name']) . ",
					NOW(),
					NOW(),
					" . $db->quoted($oldBean['modified_user_id']) . ",
					" . $db->quoted($oldBean['created_by']) . ",
					" . $db->quoted($oldBean['description']) . ",
					0,
					" . $db->quoted($oldBean['assigned_user_id']) . "
				)";
		$db->query($sql);

		$sql = "INSERT INTO dtbc_line_item_cstm
				(id_c,
				dtbc_pricebook_product_id_c,
				discount_c,
				quantity_c,
				shipment_date_c,
				total_price_c,
				total_kw_c,
				unit_price_after_discount_c,
				sale_type_c,
				list_price_c,
				kwforop_c,
				product_family_from_product_c,
				dtbc_line_item_id_c)
				VALUES (
					" . $db->quoted($newId) . ",
					" . $db->quoted($oldBean['dtbc_pricebook_product_id_c']) . ",
					" . $this->getNumberFieldForSql($oldBean['discount_c']) . ",
					" . $this->getNumberFieldForSql($oldBean['quantity_c']) . ",
					" . $this->getDateFieldForSql($oldBean['shipment_date_c']) . ",
					" . $this->getNumberFieldForSql($oldBean['total_price_c']) . ",
					" . $db->quoted($oldBean['total_kw_c']) . ",
					" . $db->quoted($oldBean['unit_price_after_discount_c']) . ",
					" . $db->quoted($oldBean['sale_type_c']) . ",
					" . $this->getNumberFieldForSql($oldBean['list_price_c']) . ",
					" . $this->getNumberFieldForSql($oldBean['kwforop_c']) . ",
					" . $db->quoted($oldBean['product_family_from_product_c']) . ",
					NULL
				)";
		$db->query($sql);

		return $newId;
	}
	
	private function getDateFieldForSql($value) {
		global $db;
		return empty($value) ? "NULL" : $db->quoted($value);
	}
	
	private function getNumberFieldForSql($value) {
		global $db;
		return empty($value) ? 0 : $db->quoted($value);
	}
	
	private function getAllLineItemsFromQuote($qId) {
		global $db;
		$retval = array();
		
		$sql = "SELECT aos_quotes_dtbc_line_item_1dtbc_line_item_idb AS id
				FROM aos_quotes_dtbc_line_item_1_c
				WHERE aos_quotes_dtbc_line_item_1aos_quotes_ida = " . $db->quoted($qId) . " AND
				aos_quotes_dtbc_line_item_1_c.deleted = 0";
		
		$res = $db->query($sql);
		
		while ($row = $db->fetchByAssoc($res)) {
			$retval[] = $row['id'];
		}
		
		return $retval;
	}

	private function deleteAllLineItemsFromOpportunity($oppId) {
		// Get all opportunity related line item
		global $db;
		$sql = "SELECT id, opportunities_dtbc_line_item_1dtbc_line_item_idb
				FROM opportunities_dtbc_line_item_1_c
				WHERE opportunities_dtbc_line_item_1opportunities_ida = " . $db->quoted($oppId);
		
		$res = $db->query($sql);
		
		$lineItemIds = "";
		$relationIds = "";
		
		// Set all the ids for deletion query
		while ($row = $db->fetchByAssoc($res)) {
			if (strlen($lineItemIds) > 0)
				$lineItemIds .= ",";
			$lineItemIds .= $db->quoted($row['opportunities_dtbc_line_item_1dtbc_line_item_idb']);
			
			if (strlen($relationIds) > 0)
				$relationIds .= ",";
			$relationIds .= $db->quoted($row['id']);
		}
		
		if (strlen($relationIds) == 0)
			return;

		// Delete line items from opportunities
		$sql = "UPDATE opportunities_dtbc_line_item_1_c
				SET deleted = 1
				WHERE id IN (" . $relationIds . ")";
		$db->query($sql);
		
		$sql = "UPDATE dtbc_line_item
				SET deleted = 1
				WHERE id IN (" . $lineItemIds . ")";
		$db->query($sql);
	}
}

?> 
