<?php 
 //WARNING: The contents of this file are auto-generated


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE'] = 'Pricebook Product';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE'] = 'Pricebook Product';
$mod_strings['LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE'] = 'Line Item';
$mod_strings['LBL_AOS_QUOTES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documents';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE'] = 'Pricebook Product';
$mod_strings['LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE'] = 'Line Item';

 

$mod_strings["LBL_LISTVIEW_GROUP"] = "Security Group";
$mod_strings["LBL_LISTVIEW_ENVELOPE"] = " ";

$mod_strings["LBL_LINEITEM_SUBTOTAL"] = "Subtotal";
$mod_strings["LBL_LINEITEM_DISCOUNT"] = "Discount";
$mod_strings["LBL_LINEITEM_TOTALPRICE"] = "Total Price";
$mod_strings["LBL_LINEITEM_GRANDTOTAL"] = "Grand Total";

$mod_strings["LBL_SYNC_START"] = "Start Sync";
$mod_strings["LBL_SYNC_STOP"] = "Stop Sync";
$mod_strings["LBL_SYNC_WARNING"] = "Quote Line Items will replace opportunity products.<br/><br/>After initial sync, all future updates to quote line items and opportunity products will automatically sync both ways.";
$mod_strings["LBL_SYNC_TITLE"] = "Synchronize quote and opportunity";
$mod_strings["LBL_SYNC_OK"] = "Sync";
$mod_strings['LBL_SEARCHFORM_SECURITY_GROUP'] = 'Security Group';

$mod_strings['LBL_BTN_GENDOC'] = 'Create PDF';

$mod_strings['LBL_PU_TITLE'] = 'Create PDF';
$mod_strings['LBL_PU_DOCTYPE'] = 'Select document template';
$mod_strings['LBL_PU_CREATE'] = 'Create';


?>