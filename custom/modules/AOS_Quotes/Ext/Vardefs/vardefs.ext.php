<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-16 14:03:03
$dictionary['AOS_Quotes']['fields']['is_synced_c']['inline_edit']='';
$dictionary['AOS_Quotes']['fields']['is_synced_c']['labelValue']='Is Synced';

 

 // created: 2017-11-02 14:40:19
$dictionary['AOS_Quotes']['fields']['ship_to_street_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ship_to_street_c']['labelValue']='Ship To Street';

 

 // created: 2017-10-26 16:18:56
$dictionary['AOS_Quotes']['fields']['shipment_terms_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['shipment_terms_c']['labelValue']='Shipment Terms';

 

 // created: 2017-10-27 14:21:55
$dictionary['AOS_Quotes']['fields']['site_description_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['site_description_c']['labelValue']='Site Description';

 

 // created: 2017-10-27 14:28:23
$dictionary['AOS_Quotes']['fields']['phone_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['phone_c']['labelValue']='Phone';

 

 // created: 2017-10-26 15:50:52
$dictionary['AOS_Quotes']['fields']['approved_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['approved_c']['labelValue']='Approved';

 

 // created: 2017-10-26 16:23:14
$dictionary['AOS_Quotes']['fields']['terms_and_conditions_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['terms_and_conditions_c']['labelValue']='terms and conditions';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary["AOS_Quotes"]["fields"]["lineitem_subtotal"] = array(
	'name' => 'lineitem_subtotal',
	'label' => 'LBL_LINEITEM_SUBTOTAL',
	'vname' => 'LBL_LINEITEM_SUBTOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_subtotal',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

$dictionary["AOS_Quotes"]["fields"]["lineitem_discount"] = array(
	'name' => 'lineitem_discount',
	'label' => 'LBL_LINEITEM_DISCOUNT',
	'vname' => 'LBL_LINEITEM_DISCOUNT',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_discount',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

$dictionary["AOS_Quotes"]["fields"]["lineitem_totalprice"] = array(
	'name' => 'lineitem_totalprice',
	'label' => 'LBL_LINEITEM_TOTALPRICE',
	'vname' => 'LBL_LINEITEM_TOTALPRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_totalprice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

$dictionary["AOS_Quotes"]["fields"]["lineitem_grandtotal"] = array(
	'name' => 'lineitem_grandtotal',
	'label' => 'LBL_LINEITEM_GRANDTOTAL',
	'vname' => 'LBL_LINEITEM_GRANDTOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lineitem_grandtotal',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/aos_quotesFunctionFields.php',
	),
);

 // created: 2017-10-26 15:59:02
$dictionary['AOS_Quotes']['fields']['payment_terms_text_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['payment_terms_text_c']['labelValue']='Payment Terms (Other)';

 

 // created: 2017-11-16 18:29:54
$dictionary['AOS_Quotes']['fields']['currency_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['currency_c']['labelValue']='currency';

 

 // created: 2017-10-27 14:26:45
$dictionary['AOS_Quotes']['fields']['shipping_name_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['shipping_name_c']['labelValue']='Ship To Name';

 

 // created: 2017-10-26 15:49:51
$dictionary['AOS_Quotes']['fields']['billing_account']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['billing_account']['merge_filter']='disabled';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['AOS_Quote']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-11-10 15:29:25
$dictionary['AOS_Quotes']['fields']['status_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['status_c']['labelValue']='status';

 

 // created: 2017-10-26 16:19:48
$dictionary['AOS_Quotes']['fields']['shipment_terms_text_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['shipment_terms_text_c']['labelValue']='Shipment Terms (Other)';

 

 // created: 2017-10-26 15:53:09
$dictionary['AOS_Quotes']['fields']['business_entity_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['business_entity_c']['labelValue']='Business Entity';

 


$dictionary["AOS_Quotes"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'enum',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-10-27 14:25:58
$dictionary['AOS_Quotes']['fields']['billing_name_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['billing_name_c']['labelValue']='Bill To Name';

 

 // created: 2017-10-26 16:09:57
$dictionary['AOS_Quotes']['fields']['project_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['project_c']['labelValue']='Project';

 

 // created: 2017-11-06 15:31:50
$dictionary['AOS_Quotes']['fields']['payment_terms_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['payment_terms_c']['labelValue']='Payment Terms';

 

 // created: 2017-10-26 16:26:54
$dictionary['AOS_Quotes']['fields']['quotation_is_valid_until_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['quotation_is_valid_until_c']['labelValue']='Valid Until';

 

 // created: 2017-11-09 17:42:18
$dictionary['AOS_Quotes']['fields']['quote_to_name_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['quote_to_name_c']['labelValue']='quote to name';

 

// created: 2017-10-27 12:36:39
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_documents_1"] = array (
  'name' => 'aos_quotes_documents_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2017-10-13 13:04:41
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook_Product',
  'bean_name' => 'dtbc_Pricebook_Product',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);


 // created: 2017-11-02 14:39:49
$dictionary['AOS_Quotes']['fields']['bill_to_street_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['bill_to_street_c']['labelValue']='Bill To Street';

 

// created: 2017-10-16 12:24:53
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_dtbc_line_item_1"] = array (
  'name' => 'aos_quotes_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'dtbc_Line_Item',
  'bean_name' => 'dtbc_Line_Item',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);

?>