<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-27 12:36:39
$layout_defs["AOS_Quotes"]["subpanel_setup"]['aos_quotes_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_QUOTES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'aos_quotes_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-13 13:04:38
$layout_defs["AOS_Quotes"]["subpanel_setup"]['aos_quotes_dtbc_pricebook_product_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Pricebook_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
  'get_subpanel_data' => 'aos_quotes_dtbc_pricebook_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-16 12:24:52
$layout_defs["AOS_Quotes"]["subpanel_setup"]['aos_quotes_dtbc_line_item_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Line_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
  'get_subpanel_data' => 'aos_quotes_dtbc_line_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



$topButtons = array(
    0 => array('widget_class' => 'SubPanelTopSyncLineItemsButton'),
);

$layout_defs['AOS_Quotes']['subpanel_setup']['aos_quotes_dtbc_line_item_1']['top_buttons'] = $topButtons;

//auto-generated file DO NOT EDIT
$layout_defs['AOS_Quotes']['subpanel_setup']['aos_quotes_dtbc_line_item_1']['override_subpanel_name'] = 'AOS_Quotes_subpanel_aos_quotes_dtbc_line_item_1';

?>