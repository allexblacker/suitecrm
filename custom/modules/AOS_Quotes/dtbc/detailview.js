function displayingPopup(titleVal, htmltext, myButtons) {						
	dialog = new YAHOO.widget.Dialog('details_popup_div', {
		width: '600px',
		fixedcenter: true,
		visible: true,
		draggable: true,
		close: false,
		effect: [
		  {effect: YAHOO.widget.ContainerEffect.FADE, duration: 0.1}],
		modal: true
	});     
    
	dialog.setHeader(titleVal);
	dialog.setBody(htmltext);
	dialog.setFooter('');
	dialog.cfg.queueProperty("buttons", myButtons);
	dialog.render(document.body);
	dialog.show();
}

function docGenPopup(recordId) {
	var titleVal = '<strong>' + SUGAR.language.get('AOS_Quotes','LBL_PU_TITLE') + '</strong>';	

	var htmltext = '<table style="width: 100%;text-align:left;">'
					+ '<tr><td style="padding: 2px;text-align:left;">'
								+ '<div class="panel-heading">' + SUGAR.language.get('AOS_Quotes','LBL_PU_DOCTYPE') + '</div>'
							+'</td>'
							+'<td>' + getDropDown() + '</td>'
					+'</tr>';

	var handleCancel = function () {
		this.cancel();
	};

	var handleSubmit = function() {
		var templateId = $("#docTypeSelector").find(":selected").val();
		window.location.replace("index.php?module=AOS_Quotes&action=generateDocument&templateId=" + templateId + "&record=" + recordId);
		this.cancel();
	};
	
	var myButtons = [
		{text: SUGAR.language.get('AOS_Quotes','LBL_PU_CREATE'), handler: handleSubmit, isDefault: true},
		{text: SUGAR.language.languages.app_strings.LBL_CANCEL_BUTTON_LABEL, handler: handleCancel, isDefault: true}
	];

	displayingPopup(titleVal, htmltext, myButtons);

	$('#details_popup_div button').addClass(' button');
	$('#details_popup_div_c').css('z-index', '11');
}

function getDropDown() {
	var docGenDocTypes = $("#dtbc_document_templates").val();
	var myArray = JSON.parse(docGenDocTypes);
	var result = '<select name="docTypeSelector" id="docTypeSelector">';
	for (var i = 0; i < myArray.length; i++) {
		result += '<option value="' + myArray[i].id + '">' + myArray[i].name + '</option>'
	}
	return result + '</select>';
}

function syncPopup(isSynced, recordId) {
	if (isSynced == 0) {
		var titleVal = '<strong>' + SUGAR.language.get('AOS_Quotes','LBL_SYNC_TITLE') + '</strong>';
	
		var htmltext = SUGAR.language.get('AOS_Quotes','LBL_SYNC_WARNING');
		
		var handleCancel = function () {
			this.cancel();
		};

		var handleSubmit = function() {
			window.location.replace('index.php?module=AOS_Quotes&action=sync&return_action=DetailView&record=' + recordId);
			this.cancel();
		};

		var myButtons = [
			{text: SUGAR.language.get('AOS_Quotes','LBL_SYNC_OK'), handler: handleSubmit, isDefault: false},
			{text: SUGAR.language.languages.app_strings.LBL_CANCEL_BUTTON_LABEL, handler: handleCancel, isDefault: true}
		];

		displayingPopup(titleVal, htmltext, myButtons);

		$('#details_popup_div_c').css('z-index', '11');
	} else {
		window.location.replace('index.php?module=AOS_Quotes&action=sync&return_action=DetailView&record=' + recordId);
	}
}


$(document).ready(function() {
	var pbName = $('#dtbc_selected_pricebook_name').val();
	if (pbName.length > 0)
		$('#whole_subpanel_aos_quotes_dtbc_line_item_1 #subpanel_title_aos_quotes_dtbc_line_item_1 div div').append("(" + pbName + ")");
});
