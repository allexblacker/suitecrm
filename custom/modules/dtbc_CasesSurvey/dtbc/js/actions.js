$(function(){

$("a").remove();
$(".button").remove();
$("#detailpanel_1").find("br").remove();    
    
$("#saveButton").click(function(){
    
    if(confirm('Are you sure you want to save the survey?')){
        
        var radioButtons = [];
        $("#detailpanel_1 input:checked").each(function() {
            var array = [];
            radioObject = {
                field: $(this).attr('name'),
                value: $(this).val()
            }
            radioButtons.push(radioObject);
        });

        var json = JSON.stringify(radioButtons);

        $.ajax({
            url: "index.php?entryPoint=saveSurvey",
            type: "POST",
            data:{
                  questions: json,
                  description: $("#description").val(),
                  contactId: $('#contactId').val(),
                  caseId: $('#caseId').val(),
                },
            success: function(result){
                $("body").html("<div style='text-align: center;'><h1>" + result + "</h1></div>");
            },
            error: function(request, status, error){
                alert(request.responseText);
            }
        });
    }
});    
    
});
