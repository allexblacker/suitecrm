<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class CasesSurveyNameGenerator{
    
    public function before_save($bean){
		if (!isset($_REQUEST['caseId']) || !isset($_REQUEST['contactId']))
			return;
		
        $caseBean = BeanFactory::getBean('Cases',$_REQUEST['caseId']);
        $contactBean = BeanFactory::getBean('Contacts',$_REQUEST['contactId']);
        $bean->name = $caseBean->name . ' - ' . $contactBean->name;
    }
}
