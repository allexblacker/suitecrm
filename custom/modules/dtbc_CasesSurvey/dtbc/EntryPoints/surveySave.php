<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$module = 'dtbc_CasesSurvey';
if(isset($_REQUEST['questions']) && isset($_REQUEST['caseId']) && isset($_REQUEST['contactId']) && isset($_REQUEST['description'])){
 
    $contactId = $_REQUEST['contactId'];
    $caseId = $_REQUEST['caseId'];
    $caseBean = BeanFactory::getBean('Cases',$caseId);
    
    require_once('custom/modules/dtbc_CasesSurvey/dtbc/classes/LanguageChanger.php');
    global $current_language;
    $current_language = LanguageChanger::changeLanguage($caseBean);
    
    require_once('custom/modules/dtbc_CasesSurvey/dtbc/classes/RelationshipChecker.php');
    RelationshipChecker::CheckRelationBetweenCaseAndContact($caseBean,$contactId);
    
    $contactBean = BeanFactory::getBean('Contacts',$contactId);
    $questions = json_decode(htmlspecialchars_decode($_REQUEST['questions']));
    $description = $_REQUEST['description'];
}
else
    die('Invalid parameters');

$elementNumber = count($questions);
$surveyBean = BeanFactory::newBean($module);

for($i=0;$i<$elementNumber;$i++){

    $field = $questions[$i]->field;
    $value = $questions[$i]->value;
    $surveyBean->$field= $value;
}

$surveyBean->description= $description;
$surveyBean->save();

if(!$caseBean->load_relationship('cases_dtbc_casessurvey_1'))
   die(translate('LBL_NOT_EXISTING_RELATIONSHIP_CASES_SURVEY',$module));

$caseBean->cases_dtbc_casessurvey_1->add($surveyBean->id);

if(!$contactBean->load_relationship('contacts_dtbc_casessurvey_1'))
    die(translate('LBL_NOT_EXISTING_RELATIONSHIP_CASES_CONTACTS',$module));

$contactBean->contacts_dtbc_casessurvey_1->add($surveyBean->id);

echo translate('LBL_RESPONSE',$module);