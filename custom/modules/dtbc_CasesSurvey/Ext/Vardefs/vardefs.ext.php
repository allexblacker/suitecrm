<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-11 11:12:10
$dictionary['dtbc_CasesSurvey']['fields']['initial_response_time_c']['inline_edit']='1';
$dictionary['dtbc_CasesSurvey']['fields']['initial_response_time_c']['labelValue']='Initial response time and overall availability';

 

 // created: 2017-09-11 11:15:18
$dictionary['dtbc_CasesSurvey']['fields']['professionalism_and_service_c']['inline_edit']='1';
$dictionary['dtbc_CasesSurvey']['fields']['professionalism_and_service_c']['labelValue']='Professionalism and service-oriented conduct of the staff';

 

 // created: 2017-09-11 11:16:16
$dictionary['dtbc_CasesSurvey']['fields']['time_until_issue_resolved_c']['inline_edit']='1';
$dictionary['dtbc_CasesSurvey']['fields']['time_until_issue_resolved_c']['labelValue']='Time until issue was resolved';

 

// created: 2017-09-08 20:40:19
$dictionary["dtbc_CasesSurvey"]["fields"]["contacts_dtbc_casessurvey_1"] = array (
  'name' => 'contacts_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_dtbc_casessurvey_1contacts_ida',
);
$dictionary["dtbc_CasesSurvey"]["fields"]["contacts_dtbc_casessurvey_1_name"] = array (
  'name' => 'contacts_dtbc_casessurvey_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_dtbc_casessurvey_1contacts_ida',
  'link' => 'contacts_dtbc_casessurvey_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["dtbc_CasesSurvey"]["fields"]["contacts_dtbc_casessurvey_1contacts_ida"] = array (
  'name' => 'contacts_dtbc_casessurvey_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_casessurvey_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);


 // created: 2017-09-11 11:15:47
$dictionary['dtbc_CasesSurvey']['fields']['technical_knowledge_c']['inline_edit']='1';
$dictionary['dtbc_CasesSurvey']['fields']['technical_knowledge_c']['labelValue']='Technical knowledge of the staff';

 

// created: 2017-09-08 20:38:49
$dictionary["dtbc_CasesSurvey"]["fields"]["cases_dtbc_casessurvey_1"] = array (
  'name' => 'cases_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_CASES_TITLE',
  'id_name' => 'cases_dtbc_casessurvey_1cases_ida',
);
$dictionary["dtbc_CasesSurvey"]["fields"]["cases_dtbc_casessurvey_1_name"] = array (
  'name' => 'cases_dtbc_casessurvey_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_dtbc_casessurvey_1cases_ida',
  'link' => 'cases_dtbc_casessurvey_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["dtbc_CasesSurvey"]["fields"]["cases_dtbc_casessurvey_1cases_ida"] = array (
  'name' => 'cases_dtbc_casessurvey_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_dtbc_casessurvey_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);

?>