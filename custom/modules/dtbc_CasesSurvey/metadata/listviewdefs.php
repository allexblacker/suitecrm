<?php
$module_name = 'dtbc_CasesSurvey';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'INITIAL_RESPONSE_TIME_C' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_INITIAL_RESPONSE_TIME',
    'width' => '10%',
  ),
  'PROFESSIONALISM_AND_SERVICE_C' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_PROFESSIONALISM_AND_SERVICE',
    'width' => '10%',
  ),
  'TIME_UNTIL_ISSUE_RESOLVED_C' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TIME_UNTIL_ISSUE_RESOLVED',
    'width' => '10%',
  ),
  'TECHNICAL_KNOWLEDGE_C' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TECHNICAL_KNOWLEDGE',
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
