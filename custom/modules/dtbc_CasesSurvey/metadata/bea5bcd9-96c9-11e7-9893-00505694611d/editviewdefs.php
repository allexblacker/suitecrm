<?php
$module_name = 'dtbc_CasesSurvey';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'initial_response_time_c',
            'studio' => 'visible',
            'label' => 'LBL_INITIAL_RESPONSE_TIME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'professionalism_and_service_c',
            'studio' => 'visible',
            'label' => 'LBL_PROFESSIONALISM_AND_SERVICE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'technical_knowledge_c',
            'studio' => 'visible',
            'label' => 'LBL_TECHNICAL_KNOWLEDGE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'time_until_issue_resolved_c',
            'studio' => 'visible',
            'label' => 'LBL_TIME_UNTIL_ISSUE_RESOLVED',
          ),
        ),
        4 => 
        array (
          0 => 'description',
        ),
      ),
    ),
  ),
);
?>
