<?php
// created: 2017-11-10 19:57:38
$mod_strings = array (
  'LBL_INITIAL_RESPONSE_TIME' => 'Reaktionszeit und generelle Verfügbarkeit',
  'LBL_PROFESSIONALISM_AND_SERVICE' => 'Professionalität und service-orientiertes Verhalten der Mitarbeiter',
  'LBL_TECHNICAL_KNOWLEDGE' => 'Technisches Fachwissen der Mitarbeiter',
  'LBL_TIME_UNTIL_ISSUE_RESOLVED' => 'Dauer bis zur Lösung des Problems',
  'LBL_CASE_NUMBER' => 'Case Nummer',
  'LBL_CASE_SUBJECT' => 'Thema',
  'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_CONTACTS_TITLE' => 'Kontakte',
  'LBL_FILLED_SURVEY' => 'Diese Befragung ist schon befüllt!',
  'LBL_NOT_EXISTING_CONTACT' => 'Kontakt existiert nicht!',
  'LBL_NOT_EXISTING_RELATIONSHIP_CASES_ACCOUNTS' => 'Keine Verknüpfung zwischen Firma und Kontakt!',
  'LBL_NOT_EXISTING_RELATIONSHIP_CASES_CONTACTS' => 'Keine Verknüpfung zwischen Case und Kontakt!',
  'LBL_NOT_EXISTING_RELATIONSHIP_CASES_SURVEY' => 'Keine Verknüpfung zwischen Case und Befragung!',
  'LBL_NOT_EXISTING_RELATIONSHIP_THIS_CASE_CONTACT' => 'Keine Verknüpfung zwischen Case und Kontakt!',
  'LBL_RESPONSE' => 'Danke für Ihre Rückmeldung!',
  'LBL_SAVE' => 'Speichern',
  'LBL_SAVE_MESSAGE' => 'Sind Sie sicher, dass Sie die Befragung speichern möchten?',
);