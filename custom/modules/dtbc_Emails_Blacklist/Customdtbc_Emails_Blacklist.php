<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/dtbc_Emails_Blacklist/dtbc_Emails_Blacklist.php');
require_once("custom/modules/dtbc_Emails_Blacklist/TempTableHandler.php");

class Customdtbc_Emails_Blacklist extends dtbc_Emails_Blacklist
{
	public function cleanBlacklistTables() {
		TempTableHandler::cleanTable();
		$this->removeExpiredRecords();
	}
	
	public function runBlacklistLogic($subject, $fromAddrObj) {
		$from_addr = $this->getEmailFromObj($fromAddrObj);
		$tTable = new TempTableHandler();
		
		$tTable->insertNewItem($from_addr, $subject);
				
		// Already in the blacklist table
		if ($this->isEmailBlacklisted($from_addr))
			return true;
		
		if ($tTable->isBlacklistEmail($from_addr, $subject)) {
			// New blacklist email
			$this->name = $from_addr;
			$this->email_address_c = $from_addr;
			$this->expiration_c = $this->getExpirationDate();
			$this->save();
			return true;
		}
		
		// Not a blacklist item
		return false;
	}
	
	public function removeExpiredRecords() {
		global $db;
		
		$sql = "UPDATE dtbc_emails_blacklist
				SET deleted = 1
				WHERE id IN (SELECT id_c FROM dtbc_emails_blacklist_cstm WHERE expiration_c <= utc_timestamp())";
		
		$db->query($sql);
	}
	
	public function getExpirationDate() {
		global $sugar_config, $db;
		
		$factor = explode(":", trim($sugar_config['solaredge']['emails_blacklist']['expiration_time']));
		$now = gmdate('Y-m-d H:i:s');
		
		if (empty($factor) || !is_array($factor))
			return $now;
		
		$sql = "SELECT DATE_ADD(DATE_ADD(utc_timestamp(), INTERVAL " . $factor[0] . " HOUR), INTERVAL " . $factor[1] . " MINUTE) AS expiration";
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && !empty($res['expiration']))
			return $res['expiration'];
		
		return $now;
	}
	
	public function isEmailBlacklisted($emailAddress) {
		global $db;
		
		$sql = "SELECT id_c 
				FROM dtbc_emails_blacklist_cstm 
				JOIN dtbc_emails_blacklist ON id = id_c 
				WHERE deleted = 0 AND 
				email_address_c = " . $db->quoted($emailAddress);	
				
		$res = $db->fetchOne($sql);
		
		return !empty($res) && is_array($res) && !empty($res['id_c']) && strlen($res['id_c']) > 0;
	}
	
	private function getEmailFromObj($obj) {
		if (!is_array($obj) || count($obj) < 1)
			return '';
		
		return $obj[0]->mailbox . "@" . $obj[0]->host;
	}
}