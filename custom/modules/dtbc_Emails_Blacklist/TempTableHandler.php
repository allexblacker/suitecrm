<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

class TempTableHandler
{
	private $tableName = 'dtbc_emails_blacklist_helper';
	
	public function insertNewItem($from_addr, $subject) {
		global $db;
		
		if (!$this->isValidInputs($from_addr))
			return false;
		
		$sql = "INSERT INTO dtbc_emails_blacklist_helper
				(from_addr, subject, date_entered)
				VALUES (
					" . $db->quoted($from_addr) . ",
					" . $db->quoted($subject) . ",
					utc_timestamp()
				);
				";
				
		$db->query($sql);
		
		return true;
	}
	
	public static function cleanTable() {
		global $sugar_config, $db;
		
		$sql = "DELETE
				FROM dtbc_emails_blacklist_helper
				WHERE date_entered < DATE_SUB(utc_timestamp(), INTERVAL " . $sugar_config['solaredge']['emails_blacklist']['timeframe'] . " MINUTE)";
				
		$db->query($sql);
	}
	
	public function isBlacklistEmail($from_addr, $subject) {
		global $sugar_config, $db;
		
		$sql = "SELECT COUNT(id) AS num 
				FROM dtbc_emails_blacklist_helper
				WHERE from_addr = " . $db->quoted($from_addr) . " AND
				subject = " . $db->quoted($subject) . " AND
				date_entered >= DATE_SUB(utc_timestamp(), INTERVAL " . $sugar_config['solaredge']['emails_blacklist']['timeframe'] . " MINUTE)";
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && !empty($res['num']))
			return $res['num'] >= $sugar_config['solaredge']['emails_blacklist']['number_of_emails'];
		
		return false;
	}
	
	private function isValidInputs($from_addr) {
		return !empty($from_addr) && filter_var($from_addr, FILTER_VALIDATE_EMAIL);
	}

}