<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

interface MetaDataImplementationInterface2
{
    public function getViewdefs () ;
    public function getFielddefs () ;
    public function getLanguage () ;
    public function deploy ($defs) ;
    public function getHistory () ;
}