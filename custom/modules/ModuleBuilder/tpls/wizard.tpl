{literal}
<script>
function checkNameField() {
	return ($('#newLayoutName').val().length > 0);
}
</script>
{/literal}

<div class='wizard' width='100%' >
	<div align='left' id='export'>{$actions}</div>
	
	<div id="Buttons">

		<table align="center" cellspacing="7" width="90%">
			<tr>
				<td>
					<form name='custom_form' id='custom_form_id' onsubmit='return false;'>
						<input type='hidden' name='module' value='ModuleBuilder'>
						<input type='hidden' name='action' value='saveCustomLayout'>
						<input type='hidden' name='new_dropdown' value=''>
						<input type='hidden' name='to_pdf' value='true'>
						<input type='hidden' name='view_module' value='{$view_module}' />
						<input type='hidden' name='refreshTree' value='1'>
						{$field_label}:&nbsp;
						<input type="text" id="newLayoutName" name="newLayoutName">&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='button' class='button' name='saverelbtn' value='{$button_label}' onclick='if(checkNameField() && check_form("custom_form")) ModuleBuilder.submitForm("custom_form");'>
					</form>
				</td>
			</tr>
		</table>
	
<!-- Hidden div for hidden content so IE doesn't ignore it -->
<div style="float:left; left:-100px; display: hidden;">&nbsp;
	{literal}
	<style type='text/css'>
		.wizard { padding: 5px; text-align:center; font-weight:bold}
		.title{ color:#990033; font-weight:bold; padding: 0px 5px 0px 0px; font-size: 20pt}
		.backButton {position:absolute; left:10px; top:35px}
	</style>
    {/literal}

	<script>
		addForm('custom_form');
		ModuleBuilder.helpSetup('studioWizard','{$defaultHelp}');
	</script>
</div>
{include file='modules/ModuleBuilder/tpls/assistantJavascript.tpl'}
