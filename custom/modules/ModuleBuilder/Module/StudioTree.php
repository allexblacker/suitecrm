<?php

require_once('modules/ModuleBuilder/MB/MBPackageTree.php');
require_once('custom/modules/ModuleBuilder/Module/StudioBrowser.php');
class StudioTree2 extends MBPackageTree{
	function __construct(){
		$this->tree = new Tree('package_tree');
		$this->tree->id = 'package_tree';
		$this->mb = new StudioBrowser2();
		$this->populateTree($this->mb->getNodes(), $this->tree);
	}

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    function StudioTree(){
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if(isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        }
        else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }


	function getName(){
		return translate('LBL_SECTION_MODULES');
	}

}
?>