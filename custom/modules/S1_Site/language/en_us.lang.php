<?php
// created: 2018-02-20 12:58:19
$mod_strings = array (
  'LBL_EDITVIEW_PANEL5' => 'New Panel 5',
  'SITE_STATE' => 'Site State',
  'LBL_NAME' => 'Name',
  'MONITORING_SITE_ID' => 'Monitoring Site ID',
  'ROOF_TYPE' => 'Roof Type',
  'PANEL_TYPE' => 'Panel Type',
  'ACCOUNT_TIME' => 'Account Time',
  'LBL_LINK_TO_MONITORING' => 'link to monitoring',
  'STATUS' => 'Status',
  'FIELD_TYPE' => 'field type',
);