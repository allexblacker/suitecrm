function stateMandatory(formName) {
	if ($('#site_state').length) {
		if ($('#site_country').val() == 'Unitedstates') {
			addRequired('site_state', formName);
		} else {
			removeRequired('site_state', formName);
		}
	}

	return true;
}

function addRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex == -1) {
			$().html(current.substring(0, myIndex));
			$('[dtbc-data=' + labelName + ']').append('<span class="required">*</span>');
			addToValidate(formName, fieldName, fieldName, true);
		}
	}	
}

function removeRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex != -1) {
			$('[dtbc-data=' + labelName + ']').html(current.substring(0, myIndex));
			removeFromValidate(formName, fieldName);
		}
	}
}

function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation() {
	clear_all_errors();
	var formName = 'EditView';
	var retval = check_form(formName);
	retval = calcRetval(retval, stateMandatory(formName));

	// Original validations
	if (retval) {
		var _form = document.getElementById(formName);
	    _form.action.value = 'Save';
	    SUGAR.ajaxUI.submitForm(_form);

	    return true;
	}
	scrollToError();
	
	return false;
}

function fieldChanged(fieldName) {
	if (dtbc_changedFields.indexOf(fieldName) == -1)
		dtbc_changedFields.push(fieldName);
}

$(document).ready(function() {
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	$("<input title='" + SUGAR.language.get('S1_Site','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation();' type='button' name='button' value='" + SUGAR.language.get('S1_Site','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
	
	var editFormName = 'EditView';
		
	if ($("#site_country").length) {
		$("#site_country").change(function() {
			stateMandatory(editFormName);
		});
		stateMandatory(editFormName);
	}
	
});