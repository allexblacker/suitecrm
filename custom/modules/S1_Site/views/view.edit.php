<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once("custom/include/dtbc/helpers.php");

class S1_SiteViewEdit extends ViewEdit {
	
    function display() {
        parent::display();
		echo getVersionedScript("custom/modules/S1_Site/dtbc/validations.js");
    }
		
}
