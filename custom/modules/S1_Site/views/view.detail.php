<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class S1_SiteViewDetail extends ViewDetail {
    
	public function preDisplay() {
        $this->bean->link_to_monitoring_c = html_entity_decode($this->bean->link_to_monitoring_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		
 	    parent::preDisplay();
    }
		
}
