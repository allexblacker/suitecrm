<?php
// created: 2017-10-30 15:23:32
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'module_type' => 
  array (
    'type' => 'varchar',
    'vname' => 'Module_type',
    'width' => '10%',
    'default' => true,
  ),
  'peak_power_kw_h_peak' => 
  array (
    'type' => 'decimal',
    'vname' => 'Peak_power_kw_h_peak',
    'width' => '10%',
    'default' => true,
  ),
  'site_address' => 
  array (
    'type' => 'varchar',
    'vname' => 'Site_Address',
    'width' => '10%',
    'default' => true,
  ),
  'site_city' => 
  array (
    'type' => 'varchar',
    'vname' => 'Site_City',
    'width' => '10%',
    'default' => true,
  ),
  'site_country' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Site_Country',
    'width' => '10%',
    'default' => true,
  ),
  'site_state' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Site_State',
    'width' => '10%',
    'default' => true,
  ),
  'site_zip' => 
  array (
    'type' => 'varchar',
    'vname' => 'Site_zip',
    'width' => '10%',
    'default' => true,
  ),
);