<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/Cases/Case.php');

class CustomCaseForViews extends aCase {

	private $permittedRelations = array('accounts');

    public function load_relationship($rel_name){

		if(!in_array($rel_name, $this->permittedRelations))			
			return;
	
		return parent::load_relationship($rel_name);
    }
}