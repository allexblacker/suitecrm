<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/Cases/controller.php');
require_once('custom/modules/Cases/CustomCase.php');
require_once('modules/SecurityGroups/SecurityGroup.php');
require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/router.php");


if (!isset($_REQUEST['record']) || empty($_REQUEST['record'])) {
	echo "Please set the 'record' value in the url";
	die();
}

global $db, $app_list_strings;

$sql = "SELECT case_status_c, contact_email_c, case_origin_c, do_not_send_survey_c, case_type_c, case_record_type_c, contacts_cstm.country_c, accounts.name
		FROM cases
		LEFT JOIN cases_cstm ON cases.id = cases_cstm.id_c
		LEFT JOIN contacts_cstm ON contacts_cstm.id_c = cases.contact_created_by_id
		LEFT JOIN accounts ON accounts.id = cases.account_id
		WHERE cases.id = " . $db->quoted($_REQUEST['record']);

$res = $db->fetchOne($sql);

if ($res != null && is_array($res) && !empty($res)) {
	echo '<b>Cases.Status (case_status_c):</b> ' . $app_list_strings['case_status_list'][$res['case_status_c']] . '<br/>';
	echo '<b>Cases.Contact Email (contact_email_c):</b> ' . $res['contact_email_c'] . '<br/>';
	echo '<b>Cases.Case Origin (case_origin_c):</b> ' . $app_list_strings['case_origin_list'][$res['case_origin_c']] . '<br/>';
	$doNotSendSurvey = $res['do_not_send_survey_c'] == '1' ? "checked" : "NOT checked";
	echo '<b>Cases.Do Not Send Survey (do_not_send_survey_c):</b> ' . $doNotSendSurvey . '<br/>';
	echo '<b>Cases.Type (case_type_c):</b> ' . $app_list_strings['case_type_list'][$res['case_type_c']] . '<br/>';
	echo '<b>Cases.Case Record Type (case_record_type_c):</b> ' . $app_list_strings['case_record_type_list'][$res['case_record_type_c']] . '<br/>';
	echo '<b>Contacts.Country (country_c):</b> ' . $app_list_strings['country_0'][$res['country_c']] . '<br/>';
	echo '<b>Accounts.Name (name):</b> ' . $res['name'] . '<br/>';
	echo '<br/><br/>Debug:';
	echo '<pre>';
	print_r($res);
	echo '</pre>';
}

die();
