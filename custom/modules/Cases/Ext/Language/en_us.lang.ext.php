<?php 
 //WARNING: The contents of this file are auto-generated


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE'] = 'Status Log';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE'] = 'Site';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_CASES_1_FROM_CASES_L_TITLE'] = 'Compensation Case';
$mod_strings['LBL_CASES_CASES_1_FROM_CASES_R_TITLE'] = 'RMA Cases';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE'] = 'USA ZIP Codes';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE'] = 'Serial Number';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE'] = 'Cases survey';

 

$mod_strings['LBL_INTERNAL_CUSTOM'] = 'Internal Note';

$mod_strings['LBL_SAVEBUTTON'] = 'Save';
  
$mod_strings['LBL_BTN_TO_TIER2'] = 'Escalate to Tier 2';
$mod_strings['LBL_BTN_TO_TIER3'] = 'Escalate to Tier 3';
$mod_strings['LBL_BTN_NEW_COMPENSATION'] = 'New Compensation Case';
$mod_strings['LBL_BTN_APPROVE_COMPENSATION'] = 'Approve Compensation';
$mod_strings['LBL_BTN_DECLINE_COMPENSATION'] = 'Decline Compensation';
$mod_strings['LBL_BTN_NEW_RMA'] = 'New RMA';
$mod_strings['LBL_BTN_FIELD_ENGINEER'] = 'Field Engineer';
$mod_strings['LBL_BTN_SPCASE'] = 'Create Service Partner Case';

$mod_strings['LBL_SEND_NOTIFICATION'] = 'Send Notification';
$mod_strings['LBL_CHANGE_CASE_STATUS'] = 'Change Status to Awaiting Customer Feedback';

$mod_strings['LBL_HIDDEN_2DAYS'] = 'Hidden - 2 days notification sent';
$mod_strings['LBL_HIDDEN_3DAYS'] = 'Hidden - 3 days notification sent';
$mod_strings['LBL_HIDDEN_5DAYS'] = 'Hidden - 5 days notification sent';
$mod_strings['LBL_HIDDEN_AFTERSAVE'] = 'Hidden - agent status notification sent';

$mod_strings['LBL_NOTIFY_CUSTOMER_DATE'] = 'Notify Customer Date';

$mod_strings['LBL_CHECK_SERIAL_BUTTON'] = "Check Serial";
$mod_strings['LBL_CHECK_BATTERY_BUTTON'] = "Check Battery";
$mod_strings['LBL_CHECK_ALTERNATIVE_BUTTON'] = "Check Alternative";
$mod_strings['LBL_SEND_ARTICLE_BUTTON'] = "Send Article to Customer";
$mod_strings['LBL_CLOSE_CASE_BUTTON'] = "Close Case";
$mod_strings['LBL_CHANGE_INDICATOR_BUTTON'] = "Change Indicator";
$mod_strings['LBL_SEND_TO_ERP_BUTTON'] = "Send To ERP";

$mod_strings['LBL_VALIDATION_ERR_SITENAME'] = "Please Insert the Site Name or Monitoring Site Name before closing the case.";
$mod_strings['LBL_VALIDATION_ERR_BATTERY'] = "You can't open an RMA because the Battery is not valid for service";
$mod_strings['LBL_VALIDATION_ERR_INVERTER'] = "Inverter Validation failed, can't open RMA";
$mod_strings['LBL_VALIDATION_ERR_RMACOUNTRY'] = "RMA county can't be Deutschland";
$mod_strings['LBL_VALIDATION_ERR_FAULTSUB'] = "Please insert a value in Details for Fault sub-category – Other";
$mod_strings['LBL_VALIDATION_ERR_ERRCODE'] = "Please Insert a value in the field 'Error Code Number'";
$mod_strings['LBL_VALIDATION_ERR_ERRCODEVALUE'] = "Please enter a value between 1 and 9999.";
$mod_strings['LBL_VALIDATION_ERR_ERREVENT'] = "Please choose Error / Event Code";
$mod_strings['LBL_VALIDATION_ERR_FAULTVALUES'] = "Please fill the following fields before closing the case: Fault Category, Fault Sub Category, Fault Description";
$mod_strings['LBL_VALIDATION_ERR_INVERTERSERIAL'] = "Inverter Serial is mandatory For Technical Issue Case";
$mod_strings['LBL_VALIDATION_ERR_INVERTERSERIALFORMAT'] = "Inverter Serial Number must be in the format of: XXXXXXXXX-XX";
$mod_strings['LBL_VALIDATION_ERR_CASEADDRESS'] = "Please fill out all address fields.";
$mod_strings['LBL_VALIDATION_ERR_RMASTREET'] = "Please do not enter a PO box information in the field 'RMA street'";
$mod_strings['LBL_VALIDATION_ERR_FAULTSUBTYPE'] = "Please elaborate on fault sub-type";
$mod_strings['LBL_VALIDATION_ERR_FAULTSUBTYPE2'] = "Please elaborate on Power board failure fault sub-type";
$mod_strings['LBL_VALIDATION_ERR_RESOLUTION'] = "Please select a value from the 'Resolution' picklist before closing the case";
$mod_strings['LBL_VALIDATION_ERR_FAULTSUBTYPE3'] = "Fault Type and Fault Sub Type are mandatory for RMAs";
$mod_strings['LBL_VALIDATION_ERR_SERVICEINSTR'] = "This site has special service instructions, please act according to the instructions and check this ACK in order to proceed";
$mod_strings['LBL_VALIDATION_ERR_SITENAME2'] = "Please enter a Site name";
$mod_strings['LBL_VALIDATION_ERR_CUSTOMERCASE'] = "Please insert customer case #";
$mod_strings['LBL_VALIDATION_ERR_SPAM'] = "Spam message";
$mod_strings['LBL_VALIDATION_ERR_TEMPLATECASE'] = "You can't update a TEMPLATE CASE, please clone this case in order to create a new Case";


$mod_strings["TITLE_NEWRMA_TITLE"] = "Create New RMA - Contact Address Confirmation";

$mod_strings["TITLE_NEWRMA_CDETAILS"] = "Contact Details are:";
		
$mod_strings["LABEL_NEWRMA_CNAME"] = "Contact Name";
$mod_strings['LABEL_NEWRMA_SCOUNTRY'] = "Contact Country";
$mod_strings['LABEL_NEWRMA_SCITY'] = "Contact City";
$mod_strings['LABEL_NEWRMA_SADDRESS'] = "Contact Address";
$mod_strings['LABEL_NEWRMA_SZIP'] = "Contact Zip";
		
$mod_strings["TITLE_NEWRMA_CONFIRMED"] = "Have you confirmed shipment address?";
		
$mod_strings["LABEL_NEWRMA_YES"] = "Yes";
$mod_strings["LABEL_NEWRMA_NO"] = "No";
		
$mod_strings["BTN_CONTINUE"] = "Continue";
$mod_strings["BTN_CANCEL"] = "Cancel";

$mod_strings["MSG_ERRORALERT"] = "Please confirm that the address is valid or not";


$mod_strings["LBL_LAST_MODIFIED_BY"] = "Last Modified By";
$mod_strings["LBL_WEB_NAME"] = "Web Name";
$mod_strings["LBL_WEB_EMAIL"] = "Web Email";
$mod_strings["LBL_FIELD_ENGINEER_EMAIL"] = "Field Engineer Email";
$mod_strings["LBL_FIELD_ORIGIN_EMAIL"] = "Email to case email address";
$mod_strings["LBL_FIELD_IS_EMAIL_TO_CASE"] = "Email to case";
$mod_strings["LBL_FIELD_LAST_SUBSTATUSCHANGE"] = "Sub status last changed";

$mod_strings["LBL_LISTVIEW_GROUP"] = "Security Group";
$mod_strings["LBL_LISTVIEW_ENVELOPE"] = " ";
$mod_strings['LBL_SEARCHFORM_SECURITY_GROUP'] = 'Security Group';

$mod_strings['LBL_VALIDATION_ERR_MODEL'] = 'Please fill in the field Model';
$mod_strings['LBL_VALIDATION_ERR_SITE_MANDATORY'] = 'Please enter a Site name';
$mod_strings['LBL_VALIDATION_ERR_SUB_SYMPTOM'] = 'Please choose a sub symptom';
$mod_strings['LBL_VALIDATION_ERR_FAULTSUBEMPTY'] = 'Please insert a value in "Details For Fault Sub Type-Other"';

$mod_strings['LBL_CASE_TODAY'] = 'Today';
$mod_strings['LBL_CASE_AGE'] = 'Case Age';
$mod_strings['LBL_CASE_SUPPORT_TEAM'] = 'Support Team for template';
$mod_strings['LBL_CASE_LAST_MODIFIED_BY'] = 'Last Modified By';
$mod_strings['LBL_CASE_COUNTIF_PORTIA'] = 'CountIF Portia RS485';

$mod_strings['LBL_POINTS_TOTAL'] = 'Email template field for serial numbers';
$mod_strings['LBL_DOA_TEMPLATE_TOTAL'] = 'Email template field for DOA table';



//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_CASES_3_FROM_CASES_L_TITLE'] = 'SP Case';
$mod_strings['LBL_CASES_CASES_3_FROM_CASES_R_TITLE'] = 'SP Case';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_CASES_2_FROM_CASES_L_TITLE'] = 'Cases';
$mod_strings['LBL_CASES_CASES_2_FROM_CASES_R_TITLE'] = 'Cases';

?>