<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-07 14:41:36
$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1'] = array (
  'order' => 100,
  'module' => 'dtbc_StatusLog',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE',
  'get_subpanel_data' => 'cases_dtbc_statuslog_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



unset($layout_defs['Cases']['subpanel_setup']['bugs']);
unset($layout_defs['Cases']['subpanel_setup']['project']);








 // created: 2017-08-29 11:10:07
$layout_defs["Cases"]["subpanel_setup"]['cases_sn1_serial_number_1'] = array (
  'order' => 100,
  'module' => 'SN1_Serial_Number',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE',
  'get_subpanel_data' => 'cases_sn1_serial_number_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-08-29 20:14:53
$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_CASES_2_FROM_CASES_R_TITLE',
  'get_subpanel_data' => 'cases_cases_2cases_ida',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$bean = BeanFactory::getBean("Cases", $_REQUEST['record']);

$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = null;

if($bean->case_record_type_c != "compensation"){
	unset($layout_defs['Cases']['subpanel_setup']['cases_cases_1cases_ida']);
}

$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1']['top_buttons'] = array();



 // created: 2017-09-08 20:38:48
$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_casessurvey_1'] = array (
  'order' => 100,
  'module' => 'dtbc_CasesSurvey',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
  'get_subpanel_data' => 'cases_dtbc_casessurvey_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-08-29 18:26:46
$layout_defs["Cases"]["subpanel_setup"]['cases_cases_1cases_ida'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_CASES_1_FROM_CASES_R_TITLE',
  'get_subpanel_data' => 'cases_cases_1cases_ida',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Cases']['subpanel_setup']['documents']['override_subpanel_name'] = 'Case_subpanel_documents';


//auto-generated file DO NOT EDIT
$layout_defs['Cases']['subpanel_setup']['cases_sn1_serial_number_1']['override_subpanel_name'] = 'Case_subpanel_cases_sn1_serial_number_1';



if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$bean = BeanFactory::getBean("Cases", $_REQUEST['record']);

$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = null;

if($bean->case_record_type_c != "compensation"){
	unset($layout_defs['Cases']['subpanel_setup']['cases_cases_1cases_ida']);
}

$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1']['top_buttons'] = array();


if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$bean = BeanFactory::getBean("Cases", $_REQUEST['record']);

$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = null;

if($bean->case_record_type_c != "compensation"){
	unset($layout_defs['Cases']['subpanel_setup']['cases_cases_1cases_ida']);
}

$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1']['top_buttons'] = array();
 
$layout_defs["Cases"]["subpanel_setup"]['cases_sn1_serial_number_1']['top_buttons'] = array (
	array('widget_class' => 'SubPanelTopCreateButton'),
	array('widget_class' => 'SubPanelTopCheckRma'),
);


//auto-generated file DO NOT EDIT
$layout_defs['Cases']['subpanel_setup']['securitygroups']['override_subpanel_name'] = 'Case_subpanel_securitygroups';


//auto-generated file DO NOT EDIT
$layout_defs['Cases']['subpanel_setup']['cases_dtbc_casessurvey_1']['override_subpanel_name'] = 'Case_subpanel_cases_dtbc_casessurvey_1';



if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

foreach ($layout_defs['Cases']['subpanel_setup']['activities']['top_buttons'] as $key => $value) {
	if ($value['widget_class'] == "SubPanelTopComposeEmailButton") {
		$layout_defs['Cases']['subpanel_setup']['activities']['top_buttons'][$key]['widget_class'] = 'SubPanelTopCustomComposeEmailButton';
		break;
	}
}
?>