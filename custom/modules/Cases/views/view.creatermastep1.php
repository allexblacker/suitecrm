<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/helpers.php');

class CasesViewCreatermastep1 extends SugarView {

	public function __construct() {
		parent::__construct();
	}

	function display() {
		global $mod_strings, $app_list_strings;
		
		$this->bean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
		
		$smarty = new Sugar_Smarty();
        
		parent::display();
		
		// Labels
		$smarty->assign("title", $mod_strings["TITLE_NEWRMA_TITLE"]); // Create New RMA - Contact Address Confirmation
		$smarty->assign("label_cdetails_title", $mod_strings["TITLE_NEWRMA_CDETAILS"]); // Contact Details are:
		
		$smarty->assign("label_cname", $mod_strings["LABEL_NEWRMA_CNAME"]); // Contact Name
        $smarty->assign("label_scountry", $mod_strings['LABEL_NEWRMA_SCOUNTRY']); // Site Country
		$smarty->assign("label_scity", $mod_strings['LABEL_NEWRMA_SCITY']); // Site City
		$smarty->assign("label_saddress", $mod_strings['LABEL_NEWRMA_SADDRESS']); // Site Address
		$smarty->assign("label_szip", $mod_strings['LABEL_NEWRMA_SZIP']); // Site Zip
		
		$smarty->assign("label_confirmed_title", $mod_strings["TITLE_NEWRMA_CONFIRMED"]); // Have you confirmed shipment address?
		
		$smarty->assign("label_yes", $mod_strings["LABEL_NEWRMA_YES"]); // Yes
		$smarty->assign("label_no", $mod_strings["LABEL_NEWRMA_NO"]); // No
		
		$smarty->assign("btnContinue", $mod_strings["BTN_CONTINUE"]);
		$smarty->assign("btnCancel", $mod_strings["BTN_CANCEL"]);
		
		$smarty->assign("alertmessage", $mod_strings["MSG_ERRORALERT"]);
		
		// Values
		$cData = $this->getContactAddressInfo($this->bean->contact_created_by_id);
		$smarty->assign("v_cname", $this->bean->contact_created_by_name);
		$smarty->assign("v_scountry", $app_list_strings['country_0'][$cData['country_c']]);
		$smarty->assign("v_scity", $cData['city_c']);
		$smarty->assign("v_saddress", $cData['street_c']);
		$smarty->assign("v_szip", $cData['zip_postal_code_c']);
		
		// Other
		$smarty->assign("recordid", $this->bean->id);
		
		$smarty->display('custom/modules/Cases/tpl/creatermastep1.tpl');
	}
	
	private function getContactAddressInfo($contactId) {
		global $db;
		$sql = "SELECT country_c, city_c, street_c,	zip_postal_code_c
				FROM contacts_cstm
				WHERE id_c = " . $db->quoted($contactId);
				
		$res = $db->fetchOne($sql);

		return $res;
	}
	
	/*
	country_c
	city_c
	street_c
	zip_postal_code_c
	*/
	
}
