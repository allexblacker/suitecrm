<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/helpers.php');

class CasesViewCreatestep1 extends SugarView {

	public function __construct() {
		parent::__construct();
	}

	function display() {
		global $app_list_strings;
		
		$smarty = new Sugar_Smarty();
        
		parent::display();
		
		$smarty->assign("title", "Select Case Record Type");
		$smarty->assign("label", "Record Type of new record");
        $smarty->assign("options", $app_list_strings['case_record_type_list']);
		
		$smarty->assign("btnContinue", "Continue");
		$smarty->assign("btnCancel", "Cancel");
		
		$smarty->assign("requests", $_REQUEST);
		
		$smarty->display('custom/modules/Cases/tpl/createstep1.tpl');

	}
	
}
