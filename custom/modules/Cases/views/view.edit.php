<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/EditView/EditViewCustomCache.php');
require_once('include/SugarTinyMCE.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");

class CasesViewEdit extends ViewEdit {
	
	var $useForSubpanel = true;

    function __construct(){
        parent::__construct();
    }
	
	function preDisplay() {
		// Set return back action
		if ($_REQUEST['return_module'] == "Contacts") {
			$_REQUEST['return_module'] = "Cases";
			$_REQUEST['return_action'] = "DetailView";
			unset($_REQUEST['return_id']);
		}		
		
		// Skip in quick create view
		if (!empty($_REQUEST['target_action']) && $_REQUEST['target_action'] == "QuickCreate")
			return;
				
		// Get record type from url or from bean
		$recordType = "";
		if (strlen($this->bean->id) > 0) {
			if (!empty($_REQUEST['fieldengineer']) && isset($_REQUEST['fieldengineer']) && $_REQUEST['fieldengineer'] == 1) {
				$this->prefillSiteAddress();
				$recordType = 'fieldengineer';
			} else if (!empty($_REQUEST['newrma']) && isset($_REQUEST['newrma']) && $_REQUEST['newrma'] == 2) {
				$this->bean->confirm_contact_address_c = $_REQUEST['confValue'] == 0 ? "1" : "0";
				if ($this->bean->confirm_contact_address_c == 0)
					$this->prefillRmaAddress();
				$recordType = 'newrmacreation';
			} else if (!empty($_REQUEST['closecase']) && isset($_REQUEST['closecase']) && $_REQUEST['closecase'] == 1) {
				$this->bean->case_status_c = 7; // Closed
				$recordType = $this->bean->case_record_type_c == "rma" ? "rmaclosecase" : "closecase";
			} else {
				$recordType = TplCalculator::getRecordType($this->bean->case_record_type_c, isset($_REQUEST['caseType']) ? $_REQUEST['caseType'] : "");
			}
		} else if (!empty($_REQUEST['caseType']) && isset($_REQUEST['caseType'])) {
			$recordType = $_REQUEST['caseType'];
			$parentBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
			$this->bean->parent_case_c = $parentBean->case_number;
			$this->bean->acase_id_c = $_REQUEST['workingrecord'];
			$this->bean->case_record_type_c = $_REQUEST['caseType'];
		} else if (!empty($_REQUEST['spcase']) && isset($_REQUEST['spcase']) && $_REQUEST['spcase'] == 1) {
			$parentBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
			$this->bean->parent_case_c = $parentBean->case_number;
			$this->bean->acase_id_c = $_REQUEST['workingrecord'];
			$this->prefillSpCaseFields();
			$recordType = 'spcase';
		}
		
		// Set fields from parent
		if (!empty($_REQUEST['parent_type']) && isset($_REQUEST['parent_type']) &&
			!empty($_REQUEST['parent_id']) && isset($_REQUEST['parent_id']) &&
			$_REQUEST['parent_type'] == "S1_Site") {
				$this->setSitePredefinedValues($_REQUEST['parent_type'], $_REQUEST['parent_id']);
		}

		// Get Custom TPL based on the custom layout logic
        $metadataFile = TplCalculator::getMetadata($_REQUEST['module'], $recordType, "editviewdefs.php");

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}
		
		// Set account and contact if request has the values and the request come from the Contacts subpanel
		if (isset($_REQUEST['contact_id']) && isset($_REQUEST['account_id']) && isset($_REQUEST['return_relationship']) && $_REQUEST['return_relationship'] == 'getRelatedCases')
				$this->setAccountContact($_REQUEST['account_id'], $_REQUEST['contact_id']);

        $this->ev = $this->getEditView();
        $this->ev->ss =& $this->ss;
        $this->ev->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/EditView/EditView.tpl'));
	}
	
	private function setAccountContact($accId, $conId) {
		// Set Account
		$accBean = BeanFactory::getBean("Accounts", $accId);
		if ($accBean !== false) {
			$this->bean->account_name = $accBean->name;
			$this->bean->account_id = $accId;
		}
		
		// Set Contact
		$conBean = BeanFactory::getBean("Contacts", $conId);
		if ($conBean !== false) {
			$this->bean->contact_created_by_name = $conBean->name;
			$this->bean->contact_created_by_id = $conId;
		}
	}

    function display() {
		$this->prefillFromCase();
		
        parent::display();
		
		// Quick create view "mode"
		if (!empty($_REQUEST['target_action']) && $_REQUEST['target_action'] == "QuickCreate") {
			if (!empty($_REQUEST['parent_type']) && $_REQUEST['parent_type'] == "S1_Site")
				$this->setSitePredefinedValues($_REQUEST['parent_type'], $_REQUEST['parent_id']);
			return;
		}

        global $sugar_config;
        $new = empty($this->bean->id);
        if ($new) {
            ?>
            <script>
                $(document).ready(function(){
                    $('#update_text').closest('.edit-view-row-item').html('');
                    $('#update_text_label').closest('.edit-view-row-item').html('');
                    $('#internal').closest('.edit-view-row-item').html('');
                    $('#internal_label').closest('.edit-view-row-item').html('');
                    $('#addFileButton').closest('.edit-view-row-item').html('');
                    $('#case_update_form_label').closest('.edit-view-row-item').html('');
                });
            </script>
			<?php
        }
		if (!empty($_REQUEST['spcase']) && isset($_REQUEST['spcase']) && $_REQUEST['spcase'] == 1) {
			echo getVersionedScript("custom/modules/Cases/dtbc/spcase_settings.js");
		}
		if (!empty($_REQUEST['fieldengineer']) && isset($_REQUEST['fieldengineer']) && $_REQUEST['fieldengineer'] == 1) {
			echo getVersionedScript("custom/modules/Cases/dtbc/fieldeng_validations.js");
		}
		if (!empty($_REQUEST['newrma']) && isset($_REQUEST['newrma']) && $_REQUEST['newrma'] == 2) {
			echo getVersionedScript("custom/modules/Cases/dtbc/rma_validations.js");
			echo '<input type="hidden" id="validation_newrma" value="1" />';			
		}
		echo '<input type="hidden" id="validation_workingrecord" value="' . $_REQUEST['workingrecord'] . '" />';
		echo '<input type="hidden" id="prefill_old_case_id" value="' . $_REQUEST['workingrecord'] . '" />';
		echo getVersionedScript("custom/modules/Cases/dtbc/validations.js");
		echo getVersionedScript("custom/include/dtbc/js/commonvalidations.js");
		if (strlen($this->bean->id) > 0) {
			$createdDate = new DateTime($this->bean->date_entered);			
			$oldDate = new DateTime("2012-08-09");
			$value = $oldDate < $createdDate ? 1 : 0;
			echo '<input type="hidden" id="validation_dateCreatedBigger" value="' . $value . '" />';
			echo '<input type="hidden" id="validation_origSerial" value="' . $this->bean->inverter_serial_c . '" />';
			
			$oldDate = new DateTime("2012-09-12");
			$value = $oldDate < $createdDate ? 1 : 0;
			echo '<input type="hidden" id="validation_dateCreatedBigger2" value="' . $value . '" />';
		}
		echo $this->getDefaultValuesForQsFields();
		echo getVersionedScript('custom/include/dtbc/js/qsContacts.js');
    }
	
	private function setSitePredefinedValues($moduleType, $recordId) {
		$site = BeanFactory::getBean($moduleType, $recordId);
		$this->bean->account_name = $site->account_name;
		$this->bean->account_id = $site->account_id_c;
		$this->bean->case_site_c = $site->site_name;
		echo '<input type="hidden" id="dtbc_account_name" value="' . $site->account_name . '" />';
		echo '<input type="hidden" id="dtbc_account_id" value="' . $site->account_id_c . '" />';
		echo '<input type="hidden" id="dtbc_site_name" value="' . $site->site_name . '" />';
		echo getVersionedScript("custom/modules/Cases/dtbc/quickcreate.js");
	}
	
	private function prefillSpCaseFields() {
		global $sugar_config;
		// Get original case
		$origBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
		
		// Save values
		$temp = array(
			"case_type_c" => $origBean->case_type_c,
			"symptom_c" => $origBean->symptom_c,
			"sub_symptom_c" => $origBean->sub_symptom_c,
			"error_code_number_c" => $origBean->error_code_number_c,
			"inverter_serial_c" => $origBean->inverter_serial_c,
			"case_site_c" => $origBean->case_site_c,
			"fault_category_c" => $origBean->fault_category_c,
			"fault_sub_type_c" => $origBean->fault_sub_type_c,
			"inverter_model_c" => $origBean->inverter_model_c,
			"panel_c" => $origBean->panel_c,
			"site_contact_name_c" => $origBean->site_contact_name_c,
			"site_contact_phone_c" => $origBean->site_contact_phone_c,
			"error_event_code_c" => $origBean->error_event_code_c,
			"case_record_type_c" => "spcase",
			"name" => "SP case " . $origBean->case_number,
			"contact_created_by" => $sugar_config['solaredge']['spcase']['ContactId'],
			"contact_created_by_id" => $sugar_config['solaredge']['spcase']['ContactId'],
			"contact_created_by_name" => $sugar_config['solaredge']['spcase']['ContactName'],
		);
		
		// Prefill values
		foreach ($temp as $key => $value)
			$this->bean->$key = $value;
	}
	
	private function prefillFromCase() {
		if (isset($_REQUEST['caseType']) && $_REQUEST['caseType'] == 'compensation' && !empty($_REQUEST['workingrecord'])) {
			$fromBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
			
			if ($fromBean != null) {
				global $current_user;
				
				$this->bean->account_id = $fromBean->account_id;
				$this->bean->account_name = $fromBean->account_name;
				$this->bean->assigned_user_id = $current_user->id;
				$this->bean->assigned_user_name = $current_user->name;
				$this->bean->case_type_c = 5;
				$this->bean->priority = 3;
				$this->bean->name = "Compensation For " . $fromBean->rma_in_erp_c;
				$this->bean->description = "Compensation RMU";
				$this->bean->case_record_type_c = "compensation";
				$this->bean->contact_created_by_id = $fromBean->contact_created_by_id;
				$this->bean->contact_created_by_name = $fromBean->contact_created_by_name;
				
			}
		}
	}
	
	private function prefillRmaAddress() {
		if (!empty($this->bean->contact_created_by_id) && strlen($this->bean->contact_created_by_id) > 0) {
			$selContact = BeanFactory::getBean("Contacts", $this->bean->contact_created_by_id);
			$this->bean->rma_street_c = $selContact->street_c;
			$this->bean->rma_city_c = $selContact->city_c;
			$this->bean->rma_zip_postal_code_c = $selContact->zip_postal_code_c;
			$this->bean->rma_state_c = $selContact->state_c;
			$this->bean->rma_country_c = $this->getCountryByValue($selContact->country_c, 'country_0');
			
			$this->bean->site_contact_person_name_c = $selContact->name;
			$this->bean->site_contact_person_phone_c = $selContact->mobile_c;
		}
	}
	
	private function getCountryByValue($countryKey, $listNameFrom, $listNameTo = 'country_list') {
		global $app_list_strings;
		
		$countryValue = $app_list_strings[$listNameFrom][$countryKey];
		
		if (!isset($app_list_strings[$listNameTo]))
			return '';
		
		foreach ($app_list_strings[$listNameTo] as $key => $value) {
			if (trim(strtolower($value)) == trim(strtolower($countryValue)))
				return $key;
		}
		
		return '';
	}
	
	private function prefillSiteAddress() {
		if (!empty($this->bean->s1_site_casess1_site_ida) && strlen($this->bean->s1_site_casess1_site_ida) > 0) {
			$selSite = BeanFactory::getBean("S1_Site", $this->bean->s1_site_casess1_site_ida);
			$this->bean->case_address_c = $selSite->site_address;
			$this->bean->case_city_c = $selSite->site_city;
			$this->bean->case_zip_c = $selSite->site_zip;
			$this->bean->case_state_c = $selSite->site_state;
			$this->bean->case_country_c = $selSite->site_country;
		}
	}
	
	protected function getEditView() {
        return new EditViewCustomCache();
    }
	
	private function getDefaultValuesForQsFields() {
		return "
			<input type='hidden' value='account_id' id='qs_hidden_account_id'>
			<input type='hidden' value='contact_created_by_id' id='qs_hidden_contact_id'>
			<input type='hidden' value='contact_created_by_name' id='qs_hidden_contact_name'>
		";
	}
}
