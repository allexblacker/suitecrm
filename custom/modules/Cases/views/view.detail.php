<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once('custom/include/DetailView/DetailViewCustomCache.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class CasesViewDetail extends ViewDetail {
    
	public function preDisplay() {
		// Get record type from url or from bean
		$recordType = TplCalculator::getRecordType(!empty($this->bean->case_record_type_c) ? $this->bean->case_record_type_c : "", isset($_REQUEST['caseType']) ? $_REQUEST['caseType'] : "");
		
		// Get Custom TPL based on the custom layout logic
        $metadataFile = TplCalculator::getMetadata($_REQUEST['module'], $recordType, "detailviewdefs.php");
		
		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailViewCustomCache();
 	    $this->dv->ss =&  $this->ss;
        
        $this->bean->change_indicator_c = html_entity_decode($this->bean->change_indicator_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->receiving_forwarder_url_c = html_entity_decode($this->bean->receiving_forwarder_url_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		$this->bean->forwarder_url_c = html_entity_decode($this->bean->forwarder_url_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->link_to_ups_2_c = html_entity_decode($this->bean->link_to_ups_2_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->link_to_ups_c = html_entity_decode($this->bean->link_to_ups_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->link_to_tesla_rma_c = html_entity_decode($this->bean->link_to_tesla_rma_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->link_to_monitoring_c = DtbcHelpers::getCustomHtmlForDisplay($this->bean->link_to_monitoring_c);
        $this->bean->action_log_c = html_entity_decode($this->bean->action_log_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->internal_comment_c = html_entity_decode($this->bean->internal_comment_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->solaredge_service_kit_holder_c = html_entity_decode($this->bean->solaredge_service_kit_holder_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->service_instruction_flag_c = html_entity_decode($this->bean->service_instruction_flag_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->receiving_tracking_link_c = html_entity_decode($this->bean->receiving_tracking_link_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->priority_indicator_c = html_entity_decode($this->bean->priority_indicator_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->valid_for_service_indication_c = html_entity_decode($this->bean->valid_for_service_indication_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->change_indicator_c = html_entity_decode($this->bean->change_indicator_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $this->bean->axiplus_indicator_c = html_entity_decode($this->bean->axiplus_indicator_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
	function display() {
		parent::display();
		echo getVersionedScript("custom/modules/Cases/dtbc/detailview.js");
		echo getVersionedScript("custom/include/dtbc/js/fixHtmlLinks.js");
	}
	
}
