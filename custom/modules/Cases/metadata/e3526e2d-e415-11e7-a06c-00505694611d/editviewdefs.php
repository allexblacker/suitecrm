<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/bindWithDelay.js',
        ),
        1 => 
        array (
          'file' => 'modules/AOK_KnowledgeBase/AOK_KnowledgeBase_SuggestionBox.js',
        ),
        2 => 
        array (
          'file' => 'include/javascript/qtip/jquery.qtip.min.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 'account_name',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
          1 => 
          array (
            'name' => 'contact_created_by_name',
            'label' => 'LBL_CONTACT_CREATED_BY_NAME',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
          1 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'inverter_serial_c',
            'label' => 'LBL_INVERTER_SERIAL',
          ),
          1 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'alternative_ibolt_status_c',
            'studio' => 'visible',
            'label' => 'ALTERNATIVE_IBOLT_STATUS',
          ),
          1 => 
          array (
            'name' => 'site_type_c',
            'studio' => 'visible',
            'label' => 'SITE_TYPE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'symptom_c',
            'studio' => 'visible',
            'label' => 'SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'doa_c',
            'studio' => 'visible',
            'label' => 'DOA',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'sub_symptom_c',
            'studio' => 'visible',
            'label' => 'SUB_SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'doa_type_c',
            'studio' => 'visible',
            'label' => 'DOA_TYPE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'error_code_number_c',
            'label' => 'ERROR_CODE_NUMBER',
          ),
          1 => 
          array (
            'name' => 'operating_period_c',
            'studio' => 'visible',
            'label' => 'LBL_OPERATING_PERIOD',
          ),
        ),
        10 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'tier_2_escalation_completed_c',
            'label' => 'TIER_2_ESCALATION_COMPLETED_TIMESTAMP',
          ),
          1 => 
          array (
            'name' => 'tier_2_assignee_c',
            'studio' => 'visible',
            'label' => 'TIER_2_ASSIGNEE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'tier_3_flag_c',
            'label' => 'TIER_3_FLAG',
          ),
          1 => 
          array (
            'name' => 'tier3_escalation_owner_c',
            'studio' => 'visible',
            'label' => 'TIER3_ESCALATION_OWNER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'jira_fa_c',
            'label' => 'JIRA_FA',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'customer_complaint_c',
            'label' => 'CUSTOMER_COMPLAINT',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'inverter_id_c',
            'label' => 'LBL_INVERTER_ID',
          ),
          1 => 
          array (
            'name' => 'distributor_c',
            'label' => 'DISTRIBUTOR',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'part_number_c',
            'label' => 'PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'distributer_number_c',
            'label' => 'DISTRIBUTER_NUMBER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'part_description_c',
            'label' => 'PART_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'cpu_firmware_c',
            'label' => 'CPU_FIRMWARE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'family_c',
            'label' => 'FAMILY',
          ),
          1 => 
          array (
            'name' => 'warranty_extention_c',
            'label' => 'WARRANTY_EXTENTION',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'valid_c',
            'studio' => 'visible',
            'label' => 'VALID',
          ),
          1 => 
          array (
            'name' => 'dsp1_c',
            'label' => 'DSP1',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'warranty_end_date_c',
            'label' => 'WARRANTY_END_DATE',
          ),
          1 => 
          array (
            'name' => 'dsp2_c',
            'label' => 'DSP2',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'warrant_extension_c',
            'label' => 'LBL_WARRANT_EXTENSION',
          ),
          1 => 
          array (
            'name' => 'activation_code_c',
            'label' => 'ACTIVATION_CODE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'reason_for_not_in_warranty_c',
            'label' => 'REASON_FOR_NOT_IN_WARRANTY',
          ),
          1 => 
          array (
            'name' => 'first_telemetry_date_c',
            'label' => 'FIRST_TELEMETRY_DATE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'ibolt_status_c',
            'label' => 'LBL_IBOLT_STATUS',
          ),
          1 => 
          array (
            'name' => 'last_telemetry_date_c',
            'label' => 'LAST_TELEMETRY_DATE',
          ),
        ),
      ),
      'lbl_editview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'site_address_c',
            'label' => 'LBL_SITE_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'peak_power_kw_c',
            'label' => 'LBL_PEAK_POWER_KW',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_city_c',
            'label' => 'LBL_SITE_CITY',
          ),
          1 => 
          array (
            'name' => 'site_zip_c',
            'label' => 'LBL_SITE_ZIP',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'site_state_c',
            'label' => 'LBL_SITE_STATE',
          ),
          1 => 
          array (
            'name' => 'site_country_c',
            'label' => 'LBL_SITE_COUNTRY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'panel_c',
            'studio' => 'visible',
            'label' => 'PANEL',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'action_log_c',
            'label' => 'LBL_ACTION_LOG',
          ),
          1 => 
          array (
            'name' => 'refurbish_c',
            'label' => 'REFURBISH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'product_group_c',
            'label' => 'PRODUCT_GROUP',
          ),
          1 => 
          array (
            'name' => 'power_c',
            'label' => 'POWER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'communication_board_was_c',
            'label' => 'COMMUNICATION_BOARD_WAS_REPLACED',
          ),
          1 => 
          array (
            'name' => 'date_rma_unit_shipped_c',
            'label' => 'LBL_DATE_RMA_UNIT_SHIPPED',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'new_internal_comment_c',
            'label' => 'LBL_NEW_INTERNAL_COMMENT',
          ),
          1 => 
          array (
            'name' => 'date_rma_approval_c',
            'label' => 'DATE_RMA_APPROVAL',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'customer_case_c',
            'label' => 'CUSTOMER_CASE',
          ),
          1 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'customer_should_go_to_the_c',
            'label' => 'CUSTOMER_SHOULD_GO_TO_THE_SITE',
          ),
          1 => 
          array (
            'name' => 'comments_for_sunpower_c',
            'label' => 'COMMENTS_FOR_SUNPOWER',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'do_not_send_survey_c',
            'label' => 'DO_NOT_SEND_SURVEY',
          ),
          1 => 
          array (
            'name' => 'digital_board_was_replaced_c',
            'label' => 'DIGITAL_BOARD_WAS_REPLACED',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'parent_case_c',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_CASE',
          ),
          1 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'priority',
            'comment' => 'The priority of the case',
            'label' => 'LBL_PRIORITY',
          ),
          1 => 
          array (
            'name' => 'service_level_c',
            'label' => 'LBL_SERVICE_LEVEL',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'details_for_fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER',
          ),
          1 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'battery_part_description_c',
            'label' => 'BATTERY_PART_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'battery_is_monitored_c',
            'label' => 'BATTERY_IS_MONITORED',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'case_record_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_RECORD_TYPE',
          ),
          1 => 
          array (
            'name' => 'case_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_REASON',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'battery_part_number_c',
            'label' => 'BATTERY_PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'se_battery_support_c',
            'label' => 'SE_BATTERY_SUPPORT',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'any_battery_serial_number_c',
            'label' => 'ANY_BATTERY_SERIAL_NUMBER',
          ),
          1 => 
          array (
            'name' => 'battery_end_warranty_date_c',
            'label' => 'BATTERY_END_WARRANTY_DATE',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'battery_buyer_id_c',
            'label' => 'BATTERY_BUYER_ID',
          ),
          1 => 
          array (
            'name' => 'battery_ibolt_status_c',
            'studio' => 'visible',
            'label' => 'BATTERY_IBOLT_STATUS',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'battery_distributor_c',
            'label' => 'BATTERY_DISTRIBUTOR',
          ),
          1 => 
          array (
            'name' => 'battery_installation_date_c',
            'label' => 'BATTERY_INSTALLATION_DATE',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'battery_distributor_number_c',
            'label' => 'BATTERY_DISTRIBUTOR_NUMBER',
          ),
          1 => 
          array (
            'name' => 'tesla_case_number_c',
            'label' => 'TESLA_CASE_NUMBER',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'battery_installer_id_c',
            'label' => 'BATTERY_INSTALLER_ID',
          ),
          1 => 
          array (
            'name' => 'escalation_to_tesla_c',
            'studio' => 'visible',
            'label' => 'ESCALATION_TO_TESLA',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'battery_tesla_authorized_c',
            'label' => 'BATTERY_TESLA_AUTHORIZED_RESELLER_ID',
          ),
          1 => 
          array (
            'name' => 'battery_fault_description_c',
            'studio' => 'visible',
            'label' => 'BATTERY_FAULT_DESCRIPTION',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'service_instruction_flag_c',
            'label' => 'LBL_SERVICE_INSTRUCTION_FLAG',
          ),
          1 => 
          array (
            'name' => 'ack_service_instruction_c',
            'label' => 'ACK_SERVICE_INSTRUCTION',
          ),
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'solarcity_flag_c',
            'label' => 'LBL_SOLARCITY_FLAG',
          ),
          1 => '',
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'rca_c',
            'label' => 'LBL_RCA',
          ),
          1 => 
          array (
            'name' => 'internal_comment_c',
            'label' => 'LBL_INTERNAL_COMMENT',
          ),
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'forwarder_url_c',
            'label' => 'LBL_FORWARDER_URL',
          ),
          1 => 
          array (
            'name' => 'receiving_forwarder_url_c',
            'label' => 'LBL_RECEIVING_FORWARDER_URL_C',
          ),
        ),
        23 => 
        array (
          0 => 
          array (
            'name' => 'whole_shipping_address_c',
            'studio' => 'visible',
            'label' => 'LBL_WHOLE_SHIPPING_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'axiplus_indicator_c',
            'label' => 'LBL_AXIPLUS_INDICATOR',
          ),
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'internal_comments_c',
            'studio' => 'visible',
            'label' => 'LBL_INTERNAL_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'video_chat_c',
            'label' => 'VIDEO_CHAT',
          ),
          1 => 
          array (
            'name' => 'date_sunpower_partner_c',
            'label' => 'DATE_SUNPOWER_PARTNER_CONTACTED_BY_SEDG',
          ),
        ),
        26 => 
        array (
          0 => 
          array (
            'name' => 'sunpower_partner_c',
            'label' => 'LBL_SUNPOWER_PARTNER',
          ),
          1 => 
          array (
            'name' => 'solution_statement__c',
            'label' => 'LBL_SOLUTION_STATEMENT_',
          ),
        ),
        27 => 
        array (
          0 => 
          array (
            'name' => 'vip_flag_c',
            'label' => 'LBL_VIP_FLAG',
          ),
          1 => 
          array (
            'name' => 'immediate_escalation_to_batt_c',
            'studio' => 'visible',
            'label' => 'LBL_IMMEDIATE_ESCALATION_TO_BATT',
          ),
        ),
        28 => 
        array (
          0 => 
          array (
            'name' => 'as_part_number_c',
            'label' => 'AS_PART_NUMBER',
          ),
          1 => '',
        ),
        29 => 
        array (
          0 => 
          array (
            'name' => 'traceability_c',
            'studio' => 'visible',
            'label' => 'TRACEABILITY',
          ),
          1 => 
          array (
            'name' => 'thread_id_c',
            'label' => 'LBL_THREAD_ID',
          ),
        ),
        30 => 
        array (
          0 => 
          array (
            'name' => 'current_software_version_c',
            'label' => 'CURRENT_SOFTWARE_VERSION',
          ),
          1 => 
          array (
            'name' => 'public_modules_c',
            'label' => 'LBL_PUBLIC_MODULES',
          ),
        ),
        31 => 
        array (
          0 => 
          array (
            'name' => 'special_service_instruction_c',
            'studio' => 'visible',
            'label' => 'LBL_SPECIAL_SERVICE_INSTRUCTION',
          ),
          1 => 
          array (
            'name' => 'roof_type_c',
            'label' => 'LBL_ROOF_TYPE',
          ),
        ),
        32 => 
        array (
          0 => 
          array (
            'name' => 'internal_notes_c',
            'studio' => 'visible',
            'label' => 'LBL_INTERNAL_NOTES',
          ),
          1 => 
          array (
            'name' => 'uzc1_usa_zip_codes_cases_name',
            'label' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
          ),
        ),
        33 => 
        array (
          0 => 
          array (
            'name' => 'replenishment_c',
            'label' => 'REPLENISHMENT',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
