<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/bindWithDelay.js',
        ),
        1 => 
        array (
          'file' => 'modules/AOK_KnowledgeBase/AOK_KnowledgeBase_SuggestionBox.js',
        ),
        2 => 
        array (
          'file' => 'include/javascript/qtip/jquery.qtip.min.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_WEB_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_SUNPOWER_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 'account_name',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
          1 => 
          array (
            'name' => 'contact_created_by_name',
            'label' => 'LBL_CONTACT_CREATED_BY_NAME',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
          1 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'inverter_serial_c',
            'label' => 'LBL_INVERTER_SERIAL',
          ),
          1 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'account_support_comments_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT_SUPPORT_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'site_type_c',
            'studio' => 'visible',
            'label' => 'SITE_TYPE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'symptom_c',
            'studio' => 'visible',
            'label' => 'SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'doa_c',
            'studio' => 'visible',
            'label' => 'DOA',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'sub_symptom_c',
            'studio' => 'visible',
            'label' => 'SUB_SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'doa_type_c',
            'studio' => 'visible',
            'label' => 'DOA_TYPE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'error_code_number_c',
            'label' => 'ERROR_CODE_NUMBER',
          ),
          1 => 
          array (
            'name' => 'operating_period_c',
            'studio' => 'visible',
            'label' => 'LBL_OPERATING_PERIOD',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'case_record_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_RECORD_TYPE',
          ),
          1 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'jira_fa_c',
            'label' => 'JIRA_FA',
          ),
          1 => 
          array (
            'name' => 'case_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_REASON',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
          1 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'parent_case_c',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_CASE',
          ),
          1 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'details_for_fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER',
          ),
          1 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'do_not_send_survey_c',
            'label' => 'DO_NOT_SEND_SURVEY',
          ),
          1 => 
          array (
            'name' => 'support_comments_cc_c',
            'label' => 'SUPPORT_COMMENTS_CC',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'severity_c',
            'studio' => 'visible',
            'label' => 'SEVERITY',
          ),
          1 => 
          array (
            'name' => 'new_internal_comment_c',
            'label' => 'LBL_NEW_INTERNAL_COMMENT',
          ),
        ),
      ),
      'LBL_PANEL_WEB_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'web_email_c',
            'label' => 'LBL_WEB_EMAIL',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'priority',
            'comment' => 'The priority of the case',
            'label' => 'LBL_PRIORITY',
          ),
          1 => 
          array (
            'name' => 'service_level_c',
            'label' => 'LBL_SERVICE_LEVEL',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'service_instruction_flag_c',
            'label' => 'LBL_SERVICE_INSTRUCTION_FLAG',
          ),
          1 => 
          array (
            'name' => 'ack_service_instruction_c',
            'label' => 'ACK_SERVICE_INSTRUCTION',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'solarcity_flag_c',
            'label' => 'LBL_SOLARCITY_FLAG',
          ),
          1 => 
          array (
            'name' => 'panel_c',
            'studio' => 'visible',
            'label' => 'PANEL',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'rca_c',
            'label' => 'LBL_RCA',
          ),
          1 => 
          array (
            'name' => 'internal_comment_c',
            'label' => 'LBL_INTERNAL_COMMENT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'forwarder_url_c',
            'label' => 'LBL_FORWARDER_URL',
          ),
          1 => 
          array (
            'name' => 'receiving_forwarder_url_c',
            'label' => 'LBL_RECEIVING_FORWARDER_URL_C',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'whole_shipping_address_c',
            'studio' => 'visible',
            'label' => 'LBL_WHOLE_SHIPPING_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'axiplus_indicator_c',
            'label' => 'LBL_AXIPLUS_INDICATOR',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'internal_comments_c',
            'studio' => 'visible',
            'label' => 'LBL_INTERNAL_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'video_chat_c',
            'label' => 'VIDEO_CHAT',
          ),
          1 => 
          array (
            'name' => 'date_sunpower_partner_c',
            'label' => 'DATE_SUNPOWER_PARTNER_CONTACTED_BY_SEDG',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'sunpower_partner_c',
            'label' => 'LBL_SUNPOWER_PARTNER',
          ),
          1 => 
          array (
            'name' => 'vip_flag_c',
            'label' => 'LBL_VIP_FLAG',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'solution_statement__c',
            'label' => 'LBL_SOLUTION_STATEMENT_',
          ),
          1 => 
          array (
            'name' => 'immediate_escalation_to_batt_c',
            'studio' => 'visible',
            'label' => 'LBL_IMMEDIATE_ESCALATION_TO_BATT',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'customer_complaint_c',
            'label' => 'CUSTOMER_COMPLAINT',
          ),
          1 => 
          array (
            'name' => 'distributer_number_c',
            'label' => 'DISTRIBUTER_NUMBER',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'as_part_number_c',
            'label' => 'AS_PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'traceability_c',
            'studio' => 'visible',
            'label' => 'TRACEABILITY',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'reason_for_not_in_warranty_c',
            'label' => 'REASON_FOR_NOT_IN_WARRANTY',
          ),
          1 => 
          array (
            'name' => 'current_software_version_c',
            'label' => 'CURRENT_SOFTWARE_VERSION',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'special_service_instruction_c',
            'studio' => 'visible',
            'label' => 'LBL_SPECIAL_SERVICE_INSTRUCTION',
          ),
          1 => 
          array (
            'name' => 'public_modules_c',
            'label' => 'LBL_PUBLIC_MODULES',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'internal_notes_c',
            'studio' => 'visible',
            'label' => 'LBL_INTERNAL_NOTES',
          ),
          1 => 
          array (
            'name' => 'roof_type_c',
            'label' => 'LBL_ROOF_TYPE',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'thread_id_c',
            'label' => 'LBL_THREAD_ID',
          ),
          1 => 
          array (
            'name' => 'uzc1_usa_zip_codes_cases_name',
            'label' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
          ),
        ),
      ),
      'LBL_PANEL_SUNPOWER_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'date_rma_approval_c',
            'label' => 'DATE_RMA_APPROVAL',
          ),
          1 => 
          array (
            'name' => 'date_rma_unit_shipped_c',
            'label' => 'LBL_DATE_RMA_UNIT_SHIPPED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'comments_for_sunpower_c',
            'label' => 'COMMENTS_FOR_SUNPOWER',
          ),
        ),
      ),
    ),
  ),
);
?>
