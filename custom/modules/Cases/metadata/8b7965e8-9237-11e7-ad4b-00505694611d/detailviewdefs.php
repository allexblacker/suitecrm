<?php
$viewdefs ['Cases'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_DESCRIPTION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_SUNPOWER_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'case_number',
            'comment' => 'Visual unique identifier',
            'studio' => 
            array (
              'quickcreate' => false,
            ),
            'label' => 'LBL_NUMBER',
          ),
          1 => 
          array (
            'name' => 'account_support_comments_c',
            'label' => 'LBL_ACCOUNT_SUPPORT_COMMENTS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
          1 => 
          array (
            'name' => 'contact_support_comments_c',
            'label' => 'LBL_CONTACT_SUPPORT_COMMENTS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
          1 => 
          array (
            'name' => 'support_comments_cc_c',
            'label' => 'SUPPORT_COMMENTS_CC',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
          1 => 
          array (
            'name' => 'internal_comment_c',
            'label' => 'LBL_INTERNAL_COMMENT',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'solaredge_service_kit_holder_c',
            'label' => 'LBL_SOLAREDGE_SERVICE_KIT_HOLDER',
          ),
          1 => 
          array (
            'name' => 'new_internal_comment_c',
            'label' => 'LBL_NEW_INTERNAL_COMMENT',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'whole_shipping_address_c',
            'label' => 'LBL_WHOLE_SHIPPING_ADDRESS',
          ),
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
          1 => 
          array (
            'name' => 'change_indicator_c',
            'label' => 'LBL_CHANGE_INDICATOR',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'alert_override_c',
            'label' => 'LBL_ALERT_OVERRIDE',
          ),
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'vip_flag_c',
            'label' => 'LBL_VIP_FLAG',
          ),
          1 => 
          array (
            'name' => 'web_email_c',
            'label' => 'LBL_WEB_EMAIL',
          ),
        ),
        11 => 
        array (
          0 => 'priority',
          1 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'confirm_contact_address_c',
            'label' => 'CONFIRM_CONTACT_ADDRESS',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_DESCRIPTION' => 
      array (
        0 => 
        array (
          0 => 'description',
          1 => '',
        ),
      ),
      'LBL_PANEL_SUNPOWER_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'date_sunpower_partner_c',
            'label' => 'DATE_SUNPOWER_PARTNER_CONTACTED_BY_SEDG',
          ),
          1 => 
          array (
            'name' => 'comments_for_sunpower_c',
            'label' => 'COMMENTS_FOR_SUNPOWER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'sunpower_partner_c',
            'label' => 'LBL_SUNPOWER_PARTNER',
          ),
          1 => 
          array (
            'name' => 'date_rma_approval_c',
            'label' => 'DATE_RMA_APPROVAL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'rma_yes_no_c',
            'label' => 'LBL_RMA_YES_NO',
          ),
          1 => 
          array (
            'name' => 'date_rma_unit_shipped_c',
            'label' => 'LBL_DATE_RMA_UNIT_SHIPPED',
          ),
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aop_case_updates_threaded_custom',
            'studio' => 'visible',
            'label' => 'LBL_AOP_CASE_UPDATES_THREADED',
          ),
        ),
      ),
    ),
  ),
);
?>
