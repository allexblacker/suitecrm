<?php
$viewdefs ['Cases'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_DESCRIPTION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ADDITIONAL_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_INVERTER_VALIDATION_DATA' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_BATTERY_VALIDATION_DATA' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_RMA_LOGISTICS' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_SHIPPING_ADDRESS_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_USA_ZIP_VERIFICATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_SITE_DETAILS' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_SYSTEM_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
          array (
            'name' => 'account_name',
            'comment' => 'The name of the account represented by the account_id field',
            'label' => 'LBL_ACCOUNT_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'case_number',
            'comment' => 'Visual unique identifier',
            'studio' => 
            array (
              'quickcreate' => false,
            ),
            'label' => 'LBL_NUMBER',
          ),
          1 => 
          array (
            'name' => 'contact_created_by_name',
            'label' => 'LBL_CONTACT_CREATED_BY_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'account_contact_s_comments_c',
            'label' => 'LBL_ACCOUNT_CONTACT_S_COMMENTS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
          1 => 
          array (
            'name' => 'contact_phone_c',
            'label' => 'LBL_CONTACT_PHONE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
          1 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'symptom_c',
            'studio' => 'visible',
            'label' => 'SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'field_service_kit_holder_c',
            'label' => 'LBL_FIELD_SERVICE_KIT_HOLDER',
          ),
          1 => 
          array (
            'name' => 'link_to_monitoring_c',
            'label' => 'LBL_LINK_TO_MONITORING',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'sub_symptom_c',
            'studio' => 'visible',
            'label' => 'SUB_SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'service_instruction_flag_c',
            'label' => 'LBL_SERVICE_INSTRUCTION_FLAG',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'error_code_number_c',
            'label' => 'ERROR_CODE_NUMBER',
          ),
          1 => 
          array (
            'name' => 'special_service_instruction_c',
            'label' => 'LBL_SPECIAL_SERVICE_INSTRUCTION',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'case_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_REASON',
          ),
          1 => 
          array (
            'name' => 'ack_service_instruction_c',
            'label' => 'ACK_SERVICE_INSTRUCTION',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
          1 => 
          array (
            'name' => 'internal_comment_c',
            'label' => 'LBL_INTERNAL_COMMENT',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
          1 => 
          array (
            'name' => 'new_internal_comment_c',
            'label' => 'LBL_NEW_INTERNAL_COMMENT',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
          1 => 
          array (
            'name' => 'change_indicator_c',
            'label' => 'LBL_CHANGE_INDICATOR',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'panel_c',
            'studio' => 'visible',
            'label' => 'PANEL',
          ),
          1 => '',
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'details_for_fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER',
          ),
          1 => 
          array (
            'name' => 'service_level_c',
            'label' => 'LBL_SERVICE_LEVEL',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
          1 => 
          array (
            'name' => 'cases_cases_3_name',
            'label' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'inverter_serial_c',
            'label' => 'LBL_INVERTER_SERIAL',
          ),
          1 => 
          array (
            'name' => 'jira_fa_c',
            'label' => 'JIRA_FA',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'service_level_flag_c',
            'label' => 'LBL_SERVICE_LEVEL_FLAG',
          ),
          1 => 
          array (
            'name' => 'resolution',
            'comment' => 'The resolution of the case',
            'label' => 'LBL_RESOLUTION',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'whole_shipping_address_c',
            'label' => 'LBL_WHOLE_SHIPPING_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'video_chat_c',
            'label' => 'VIDEO_CHAT',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'vip_flag_c',
            'label' => 'LBL_VIP_FLAG',
          ),
          1 => '',
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
          1 => '',
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'confirm_contact_address_c',
            'label' => 'CONFIRM_CONTACT_ADDRESS',
          ),
          1 => '',
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'case_record_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_RECORD_TYPE',
          ),
          1 => '',
        ),
        23 => 
        array (
          0 => 
          array (
            'name' => 'tier_2_assignee_c',
            'studio' => 'visible',
            'label' => 'TIER_2_ASSIGNEE',
          ),
          1 => '',
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'tier_2_assignee_email_c',
            'studio' => 'visible',
            'label' => 'LBL_TIER_2_ASSIGNEE_EMAIL',
          ),
          1 => '',
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'tier_2_region_c',
            'studio' => 'visible',
            'label' => 'TIER_2_REGION',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_DESCRIPTION' => 
      array (
        0 => 
        array (
          0 => 'description',
          1 => '',
        ),
      ),
      'LBL_PANEL_ADDITIONAL_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'immediate_escalation_to_c',
            'studio' => 'visible',
            'label' => 'LBL_IMMEDIATE_ESCALATION_TO',
          ),
          1 => 
          array (
            'name' => 'customer_complaint_c',
            'label' => 'CUSTOMER_COMPLAINT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'tier3_escalation_owner_c',
            'studio' => 'visible',
            'label' => 'TIER3_ESCALATION_OWNER',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_INVERTER_VALIDATION_DATA' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'distributer_number_c',
            'label' => 'DISTRIBUTER_NUMBER',
          ),
          1 => 
          array (
            'name' => 'refurbish_c',
            'label' => 'REFURBISH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'distributor_c',
            'label' => 'DISTRIBUTOR',
          ),
          1 => 
          array (
            'name' => 'as_part_number_c',
            'label' => 'AS_PART_NUMBER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'inverter_id_c',
            'label' => 'LBL_INVERTER_ID',
          ),
          1 => 
          array (
            'name' => 'cpu_firmware_c',
            'label' => 'CPU_FIRMWARE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'ibolt_status_c',
            'studio' => 'visible',
            'label' => 'IBOLT_STATUS',
          ),
          1 => 
          array (
            'name' => 'dsp1_c',
            'label' => 'DSP1',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'valid_c',
            'studio' => 'visible',
            'label' => 'VALID',
          ),
          1 => 
          array (
            'name' => 'dsp2_c',
            'label' => 'DSP2',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'warranty_end_date_c',
            'label' => 'WARRANTY_END_DATE',
          ),
          1 => 
          array (
            'name' => 'activation_code_c',
            'label' => 'ACTIVATION_CODE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'warrant_extension_c',
            'label' => 'LBL_WARRANT_EXTENSION',
          ),
          1 => 
          array (
            'name' => 'traceability_c',
            'studio' => 'visible',
            'label' => 'TRACEABILITY',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'part_number_c',
            'label' => 'PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'reason_for_not_in_warranty_c',
            'label' => 'REASON_FOR_NOT_IN_WARRANTY',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'part_description_c',
            'label' => 'PART_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'current_software_version_c',
            'label' => 'CURRENT_SOFTWARE_VERSION',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'family_c',
            'label' => 'FAMILY',
          ),
          1 => 
          array (
            'name' => 'action_log_c',
            'label' => 'LBL_ACTION_LOG',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'product_group_c',
            'label' => 'PRODUCT_GROUP',
          ),
          1 => 
          array (
            'name' => 'first_telemetry_date_c',
            'label' => 'FIRST_TELEMETRY_DATE',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'power_c',
            'label' => 'POWER',
          ),
          1 => 
          array (
            'name' => 'last_telemetry_date_c',
            'label' => 'LAST_TELEMETRY_DATE',
          ),
        ),
      ),
      'LBL_PANEL_BATTERY_VALIDATION_DATA' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'any_battery_serial_number_c',
            'label' => 'ANY_BATTERY_SERIAL_NUMBER',
          ),
          1 => 
          array (
            'name' => 'battery_valid_for_service_c',
            'label' => 'LBL_BATTERY_VALID_FOR_SERVICE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'battery_part_number_c',
            'label' => 'BATTERY_PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'battery_end_warranty_date_c',
            'label' => 'BATTERY_END_WARRANTY_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'battery_part_description_c',
            'label' => 'BATTERY_PART_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'se_battery_support_c',
            'label' => 'SE_BATTERY_SUPPORT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'battery_distributor_c',
            'label' => 'BATTERY_DISTRIBUTOR',
          ),
          1 => 
          array (
            'name' => 'battery_is_monitored_c',
            'label' => 'BATTERY_IS_MONITORED',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'battery_distributor_number_c',
            'label' => 'BATTERY_DISTRIBUTOR_NUMBER',
          ),
          1 => 
          array (
            'name' => 'battery_installation_date_c',
            'label' => 'BATTERY_INSTALLATION_DATE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'battery_buyer_id_c',
            'label' => 'BATTERY_BUYER_ID',
          ),
          1 => 
          array (
            'name' => 'link_to_tesla_rma_c',
            'label' => 'LBL_LINK_TO_TESLA_RMA',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'battery_installer_id_c',
            'label' => 'BATTERY_INSTALLER_ID',
          ),
          1 => 
          array (
            'name' => 'tesla_case_number_c',
            'label' => 'TESLA_CASE_NUMBER',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'battery_tesla_authorized_c',
            'label' => 'BATTERY_TESLA_AUTHORIZED_RESELLER_ID',
          ),
          1 => 
          array (
            'name' => 'battery_ibolt_status_c',
            'studio' => 'visible',
            'label' => 'BATTERY_IBOLT_STATUS',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'certified_installer_tesla_c',
            'label' => 'LBL_CERTIFIED_INSTALLER_TESLA',
          ),
          1 => 
          array (
            'name' => 'escalation_to_tesla_c',
            'studio' => 'visible',
            'label' => 'ESCALATION_TO_TESLA',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'battery_fault_description_c',
            'studio' => 'visible',
            'label' => 'BATTERY_FAULT_DESCRIPTION',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_RMA_LOGISTICS' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'send_to_priority_c',
            'label' => 'SEND_TO_PRIORITY',
          ),
          1 => 
          array (
            'name' => 'shipping_tracking_number_c',
            'label' => 'SHIPPING_TRACKING_NUMBER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'rma_in_erp_c',
            'label' => 'RMA_IN_ERP',
          ),
          1 => 
          array (
            'name' => 'link_to_ups_c',
            'label' => 'LBL_LINK_TO_UPS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'replacement_type_c',
            'studio' => 'visible',
            'label' => 'REPLACEMENT_TYPE',
          ),
          1 => 
          array (
            'name' => 'link_to_ups_2_c',
            'label' => 'LBL_LINK_TO_UPS_2',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'reason_for_additional_rma_c',
            'studio' => 'visible',
            'label' => 'REASON_FOR_ADDITIONAL_RMA',
          ),
          1 => 
          array (
            'name' => 'shipping_tracking_number_2_c',
            'label' => 'SHIPPING_TRACKING_NUMBER_2',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'shipping_service_type_c',
            'studio' => 'visible',
            'label' => 'SHIPPING_SERVICE_TYPE',
          ),
          1 => 
          array (
            'name' => 'receiving_forwarder_c',
            'studio' => 'visible',
            'label' => 'LBL_RECEIVING_FORWARDER',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'receiving_tracking_number_c',
            'label' => 'LBL_RECEIVING_TRACKING_NUMBER',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'need_pre_configuration_c',
            'label' => 'NEED_PRE_CONFIGURATION',
          ),
          1 => 
          array (
            'name' => 'receiving_tracking_link_c',
            'studio' => 'visible',
            'label' => 'LBL_RECEIVING_TRACKING_LINK',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'rca_location_c',
            'studio' => 'visible',
            'label' => 'LBL_RCA_LOCATION',
          ),
          1 => 
          array (
            'name' => 'rma_comments_c',
            'studio' => 'visible',
            'label' => 'RMA_COMMENTS',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'communication_board_was_c',
            'label' => 'COMMUNICATION_BOARD_WAS_REPLACED',
          ),
          1 => 
          array (
            'name' => 'urgency_c',
            'studio' => 'visible',
            'label' => 'LBL_URGENCY',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'digital_board_was_replaced_c',
            'label' => 'DIGITAL_BOARD_WAS_REPLACED',
          ),
          1 => 
          array (
            'name' => 'faulty_inverter_c',
            'label' => 'FAULTY_INVERTER',
          ),
        ),
        10 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'return_asked_by_c',
            'studio' => 'visible',
            'label' => 'LBL_RETURN_ASKED_BY',
          ),
        ),
        11 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'tracking_number_fa_c',
            'label' => 'LBL_TRACKING_NUMBER_FA',
          ),
        ),
      ),
      'LBL_PANEL_SHIPPING_ADDRESS_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'rma_country_c',
            'studio' => 'visible',
            'label' => 'RMA_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'rma_street_c',
            'label' => 'RMA_STREET',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'rma_state_c',
            'studio' => 'visible',
            'label' => 'RMA_STATE',
          ),
          1 => 
          array (
            'name' => 'rma_city_c',
            'label' => 'RMA_CITY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'site_contact_person_name_c',
            'label' => 'SITE_CONTACT_PERSON_NAME',
          ),
          1 => 
          array (
            'name' => 'rma_zip_postal_code_c',
            'label' => 'RMA_ZIP_POSTAL_CODE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'site_contact_person_phone_c',
            'label' => 'SITE_CONTACT_PERSON_PHONE',
          ),
          1 => 
          array (
            'name' => 'rma_contant_email_c',
            'label' => 'LBL_RMA_CONTANT_EMAIL',
          ),
        ),
      ),
      'LBL_PANEL_USA_ZIP_VERIFICATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'uzc1_usa_zip_codes_cases_name',
            'label' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
          ),
          1 => 
          array (
            'name' => 'state_zip_c',
            'label' => 'LBL_STATE_ZIP',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'canadian_province_c',
            'label' => 'LBL_CANADIAN_PROVINCE',
          ),
          1 => 
          array (
            'name' => 'case_city_c',
            'label' => 'CASE_CITY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'place_name_canada_only_c',
            'label' => 'LBL_PLACE_NAME_CANADA_ONLY',
          ),
          1 => 
          array (
            'name' => 'county_c',
            'label' => 'LBL_COUNTY',
          ),
        ),
      ),
      'LBL_PANEL_SITE_DETAILS' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'site_country_c',
            'label' => 'LBL_SITE_COUNTRY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_state_c',
            'label' => 'LBL_SITE_STATE',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'site_city_c',
            'label' => 'LBL_SITE_CITY',
          ),
          1 => 
          array (
            'name' => 'peak_power_kw_c',
            'label' => 'LBL_PEAK_POWER_KW',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'site_address_c',
            'label' => 'LBL_SITE_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'internal_notes_c',
            'label' => 'LBL_INTERNAL_NOTES',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'site_zip_c',
            'label' => 'LBL_SITE_ZIP',
          ),
          1 => 
          array (
            'name' => 'roof_type_c',
            'label' => 'LBL_ROOF_TYPE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'public_modules_c',
            'label' => 'LBL_PUBLIC_MODULES',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_SYSTEM_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'last_modified_by_c',
            'label' => 'LBL_LAST_MODIFIED_BY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'thread_id_c',
            'label' => 'LBL_THREAD_ID',
          ),
          1 => '',
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aop_case_updates_threaded_custom',
            'studio' => 'visible',
            'label' => 'LBL_AOP_CASE_UPDATES_THREADED',
          ),
        ),
      ),
    ),
  ),
);
?>
