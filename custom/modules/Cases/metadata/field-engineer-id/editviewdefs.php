<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/bindWithDelay.js',
        ),
        1 => 
        array (
          'file' => 'modules/AOK_KnowledgeBase/AOK_KnowledgeBase_SuggestionBox.js',
        ),
        2 => 
        array (
          'file' => 'include/javascript/qtip/jquery.qtip.min.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_FIELD_ENGINEER_CASE' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'LBL_FIELD_ENGINEER_CASE' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'field_engineer_c',
            'studio' => 'visible',
            'label' => 'FIELD_ENGINEER',
          ),
          1 => 
          array (
            'name' => 'case_city_c',
            'label' => 'CASE_CITY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'field_engineer_name_c',
            'studio' => 'visible',
            'label' => 'FIELD_ENGINEER_NAME',
          ),
          1 => 
          array (
            'name' => 'case_zip_c',
            'label' => 'CASE_ZIP',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
          1 => 
          array (
            'name' => 'tile_roof_c',
            'studio' => 'visible',
            'label' => 'LBL_TILE_ROOF',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
          1 => 
          array (
            'name' => 'workplan_remarks_c',
            'studio' => 'visible',
            'label' => 'WORKPLAN_REMARKS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'time_of_visit_c',
            'label' => 'TIME_OF_VISIT',
          ),
          1 => 
          array (
            'name' => 'site_contact_name_c',
            'label' => 'SITE_CONTACT_NAME',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'case_country_c',
            'studio' => 'visible',
            'label' => 'CASE_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'site_contact_email_c',
            'label' => 'SITE_CONTACT_EMAIL',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'case_state_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATE',
          ),
          1 => 
          array (
            'name' => 'site_contact_phone_c',
            'label' => 'SITE_CONTACT_PHONE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'case_address_c',
            'label' => 'CASE_ADDRESS',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
