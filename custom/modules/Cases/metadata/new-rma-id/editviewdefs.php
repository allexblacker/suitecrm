<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/bindWithDelay.js',
        ),
        1 => 
        array (
          'file' => 'modules/AOK_KnowledgeBase/AOK_KnowledgeBase_SuggestionBox.js',
        ),
        2 => 
        array (
          'file' => 'include/javascript/qtip/jquery.qtip.min.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_RMA_CASE' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'LBL_PANEL_RMA_CASE' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'doa_c',
            'studio' => 'visible',
            'label' => 'DOA',
          ),
          1 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'operating_period_c',
            'studio' => 'visible',
            'label' => 'LBL_OPERATING_PERIOD',
          ),
          1 => 
          array (
            'name' => 'digital_board_was_replaced_c',
            'label' => 'DIGITAL_BOARD_WAS_REPLACED',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'doa_type_new_c',
            'studio' => 'visible',
            'label' => 'LBL_DOA_TYPE_NEW',
          ),
          1 => 
          array (
            'name' => 'communication_board_was_c',
            'label' => 'COMMUNICATION_BOARD_WAS_REPLACED',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'rca_c',
            'label' => 'LBL_RCA',
          ),
          1 => 
          array (
            'name' => 'faulty_inverter_c',
            'label' => 'FAULTY_INVERTER',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'rca_location_c',
            'studio' => 'visible',
            'label' => 'LBL_RCA_LOCATION',
          ),
          1 => 
          array (
            'name' => 'details_for_fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'error_code_number_c',
            'label' => 'ERROR_CODE_NUMBER',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'confirm_contact_address_c',
            'label' => 'CONFIRM_CONTACT_ADDRESS',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'replacement_type_c',
            'studio' => 'visible',
            'label' => 'REPLACEMENT_TYPE',
          ),
          1 => 
          array (
            'name' => 'inverter_s_size_s_c',
            'label' => 'INVERTER_S_SIZE_S',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
          1 => 
          array (
            'name' => 'customer_case_c',
            'label' => 'CUSTOMER_CASE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'ship_to_name_c',
            'label' => 'SHIP_TO_NAME',
          ),
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'rma_street_c',
            'label' => 'RMA_STREET',
          ),
          1 => '',
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'rma_city_c',
            'label' => 'RMA_CITY',
          ),
          1 => 
          array (
            'name' => 'site_contact_person_name_c',
            'label' => 'SITE_CONTACT_PERSON_NAME',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'rma_country_c',
            'studio' => 'visible',
            'label' => 'RMA_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'site_contact_person_email_c',
            'label' => 'SITE_CONTACT_PERSON_EMAIL',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'rma_state_c',
            'studio' => 'visible',
            'label' => 'RMA_STATE',
          ),
          1 => 
          array (
            'name' => 'site_contact_person_phone_c',
            'label' => 'SITE_CONTACT_PERSON_PHONE',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'rma_zip_postal_code_c',
            'label' => 'RMA_ZIP_POSTAL_CODE',
          ),
          1 => 
          array (
            'name' => 'installation_rules_c',
            'studio' => 'visible',
            'label' => 'INSTALLATION_RULES',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'job_number_us_only_c',
            'label' => 'JOB_NUMBER_US_ONLY',
          ),
          1 => 
          array (
            'name' => 'guidelines_deviation_reason_c',
            'label' => 'GUIDELINES_DEVIATION_REASON',
          ),
        ),
        16 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'monitored_c',
            'studio' => 'visible',
            'label' => 'MONITORED',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'case_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_REASON',
          ),
          1 => 
          array (
            'name' => 'rma_type_c',
            'studio' => 'visible',
            'label' => 'RMA_TYPE',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
          1 => 
          array (
            'name' => 'rma_contant_email_c',
            'label' => 'LBL_RMA_CONTANT_EMAIL',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
