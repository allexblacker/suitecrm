<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/Cases/Case.php');
require_once('custom/include/dtbc/helpers.php');

class CustomCase extends aCase {
    public function create_new_list_query($order_by, $where,$filter=array(),$params=array(), $show_deleted = 0,$join_type='', $return_array = false,$parentbean=null, $singleSelect = false, $ifListForExport = false) {
		$res = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);
		
		if ((isset($_REQUEST['request_data']) && strpos($_REQUEST['request_data'], "cases_cases_1cases_ida") !== false) || 
			(isset($_REQUEST['selectingRma']) && $_REQUEST['selectingRma'] == 1))
			return $this->filterOutRmaWithCompensation($res);
		
		if ((isset($_REQUEST['module']) && $_REQUEST['module'] == "Home") && 
			(isset($_REQUEST['action']) && $_REQUEST['action'] == "UnifiedSearch"))
			return $this->customQueryForCaseSearch($res);
		
		return $res;
	}

	private function filterOutRmaWithCompensation($query) {
		$retval = $query;
		$retval['where'] .= " AND cases.case_record_type_c LIKE 'rma' AND NOT EXISTS (SELECT id FROM cases_cases_1_c WHERE deleted = 0 AND cases_cases_1cases_ida = cases.id) ";
		return $retval;
	}
	
	private function customQueryForCaseSearch($query) {
		if (strpos($query['where'], "SearchBySecGroup") === false)
			return $query;
		
		global $db;
		$retval = $query;
		
		$retval['from'] = " FROM securitygroups_records
							JOIN securitygroups ON securitygroups.id = securitygroups_records.securitygroup_id AND securitygroups.deleted = 0
							JOIN cases ON securitygroups_records.record_id = cases.id
							JOIN cases_cstm ON cases_cstm.id_c = cases.id
							LEFT JOIN  accounts accounts ON cases.account_id=accounts.id AND accounts.deleted=0 AND accounts.deleted=0  
							LEFT JOIN  users jt1 ON cases.assigned_user_id=jt1.id AND jt1.deleted=0 AND jt1.deleted=0 ";
							
		$retval['where'] = " WHERE securitygroups_records.deleted = 0 AND
							securitygroups_records.module = 'Cases' AND
							securitygroups.name LIKE  " . $db->quoted('%' . $_REQUEST['query_string'] . '%') . " ";

		return $retval;
	}

}