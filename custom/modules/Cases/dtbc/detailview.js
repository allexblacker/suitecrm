function dtbc_getSelectedIds() {
	var chkBoxes = $('[id^=checkrma_]');
	var retval = [];
	for (var i = 0; i < chkBoxes.length; i++) {
		if (chkBoxes[i].checked == true)
			retval.push(chkBoxes[i].id.substring(9));
	}
	return retval;
}

function checkRma() {
	$.ajax({
		method: "POST",
		url: "index.php?module=Cases&action=checkRma&to_pdf=1",
		data: { 
			selectedRecords: dtbc_getSelectedIds(),
			record: $('[name=acase_id]').val(),
		},
	})
	.done(function (response) {
		showSubPanel('cases_sn1_serial_number_1', null, true);
	})
	.fail(function (e) {
		console.log(e);
	}); 
}

$(document).ready(function() {
	$("#cases_cases_1_select_button").removeAttr("onclick");
    $("#cases_cases_1_select_button").prop('onclick', null).off('click');
    $('#cases_cases_1_select_button').on('click', function () {
        open_popup("Cases",
			600,
			400,
			"&selectingRma=1",
			true,
			true,
			{"call_back_function":"set_return_and_save_background",
			"form_name":"DetailView",
			"field_to_name_array":{"id":"subpanel_id"},
			"passthru_data":{"child_field":"cases_cases_1cases_ida",
							"return_url":"index.php%3Fmodule%3DCases%26action%3DSubPanelViewer%26subpanel%3Dcases_cases_1cases_ida%26record%3Dea1a69e5-c4de-8606-6240-5a27c526935e%26sugar_body_only%3D1",
							"link_field_name":"cases_cases_1cases_ida","module_name":"cases_cases_1cases_ida","refresh_page":0}
			},
			"MultiSelect",
			true);
    });
});