{literal}
<style>
	#container {
		padding-left: 20px;
	}
	
	#buttons {
		padding-top: 10px;
		text-align: center;
		width: 50%;
	}
</style>

<script>
	function btnRedirect(btnType) {
		// Default link is cancel
		var link = "index.php?module=Cases&action=index";
		
		// If additional things has set
		if ($('#return_module').length && $('#return_action').length && $('#return_id').length)
			link = "index.php?module=" + $('#return_module').val() + "&action=" + $('#return_action').val() + "&record=" + $('#return_id').val();
		
		if (btnType == 1) {
			// Continue
			var parentType = "";
			if ($('#parent_type').length)
				parentType = $('#parent_type').val();
			
			var parentId = "";
			if ($('#parent_id').length)
				parentId = $('#parent_id').val();
				
			link = "index.php?module=Cases&action=EditView&return_module=Cases&return_action=DetailView&caseType=" + $('#caseType').val() + "&parent_type=" + parentType + "&parent_id=" + parentId;
			
			if ($('#return_module').length && $('#return_action').length && $('#return_id').length) {
				link = "index.php?module=Cases&action=EditView&return_module=" + $('#return_module').val() + "&return_action=" + $('#return_action').val() + "&return_id=" + $('#return_id').val() + "&caseType=" + $('#caseType').val() + "&parent_type=" + $('#parent_type').val() + "&parent_id=" + $('#parent_id').val();
				if ($('#return_relationship').length)
					link += "&return_relationship=" + $('#return_relationship').val();
				if ($('#return_name').length)
					link += "&return_name=" + $('#return_name').val();
				if ($('#contact_id').length)
					link += "&contact_id=" + $('#contact_id').val();
				if ($('#account_id').length)
					link += "&account_id=" + $('#account_id').val();
			}
				
		}
		
		// Create link
		var tIndex = document.location.href.indexOf("index.php");
		var url = document.location.href.substr(0, tIndex);
		window.location = url + link;
		return false;
	}
</script>
{/literal}

<h2>{$title}</h2>
<div id="container">
	{foreach from=$requests key=k item=v}
	   <input type="hidden" id="{$k}" value="{$v}"/>
	{/foreach}
	<div id="fields">
		<label>{$label}</label>
		<select id="caseType">
			{foreach from=$options item=label key=key}
				<option value='{$key}'>{$label}</option>
			{/foreach}		
		</select>
	</div>
	<div id="buttons">
		<input type="submit" onclick="return btnRedirect(1);" value="{$btnContinue}" />
		<input type="submit"  onclick="return btnRedirect(2);" value="{$btnCancel}" />
	</div>
</div>