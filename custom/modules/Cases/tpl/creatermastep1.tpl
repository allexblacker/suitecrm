{literal}
<style>
	#container {
		padding-left: 20px;
	}
	
	#buttons {
		padding-top: 10px;
		text-align: center;
		width: 50%;
	}
	
	.valuelabel {
		font-weight: bold;
	}
	
	h3 {
		margin-top: 10px;
	}
</style>

<script>
	function btnRedirect(btnType) {
		// Default link is cancel
		var link = "index.php?module=Cases&action=DetailView&record={/literal}{$recordid}{literal}";
		var confValue = $("input:radio[name ='confirmation']:checked").val();
		if (btnType == 1) {
			if (confValue == undefined) {
				// Confirmation answer is mandatory
				alert('{/literal}{$alertmessage}{literal}');
				return false;
			}
			// Continue
			link = "index.php?module=Cases&action=EditView&return_module=Cases&return_action=DetailView&record={/literal}{$recordid}{literal}&newrma=2&confValue=" + confValue;
		}
		// Create link
		var tIndex = document.location.href.indexOf("index.php");
		var url = document.location.href.substr(0, tIndex);
		window.location = url + link;
		return false;
	}
</script>
{/literal}

<h2>{$title}</h2>
<div id="container">
	<h3>{$label_cdetails_title}</h3>
	<div id="fields1">
		<table>
			<tbody>
				<tr>
					<td><span class="valuelabel">{$label_cname}</span></td>
					<td width="10"></td>
					<td><span class="value">{$v_cname}</span></td>
				</tr>
				
				<tr>
					<td><span class="valuelabel">{$label_scountry}</span></td>
					<td width="10"></td>
					<td><span class="value">{$v_scountry}</span></td>
				</tr>
				<tr>
					<td><span class="valuelabel">{$label_scity}</span></td>
					<td width="10"></td>
					<td><span class="value">{$v_scity}</span></td>
				</tr>
				<tr>
					<td><span class="valuelabel">{$label_saddress}</span></td>
					<td width="10"></td>
					<td><span class="value">{$v_saddress}</span></td>
				</tr>
				<tr>
					<td><span class="valuelabel">{$label_szip}</span></td>
					<td width="10"></td>
					<td><span class="value">{$v_szip}</span></td>
				</tr>
			</tbody>
		</table>
	</div>
	<h3>{$label_confirmed_title}</h3>
	<div id="fields2">
		<input type="radio" name="confirmation" value="1">&nbsp;{$label_yes}<br/>
		<input type="radio" name="confirmation" value="0">&nbsp;{$label_no}
	</div>
	<div id="buttons">
		<input type="submit" onclick="return btnRedirect(1);" value="{$btnContinue}" />
		<input type="submit"  onclick="return btnRedirect(2);" value="{$btnCancel}" />
	</div>
</div>