<?php

require_once("custom/include/dtbc/helpers.php");

class CustomTemplateParser {
    static function parse_template($string, $bean_arr)
    {

        foreach ($bean_arr as $bean_name => $bean_id) {

            $focus = BeanFactory::getBean($bean_name, $bean_id);
            $string = CustomTemplateParser::parse_template_bean($string, $focus->table_name, $focus);

            foreach ($focus->field_defs as $focus_name => $focus_arr) {
                if ($focus_arr['type'] == 'relate') {
                    if (isset($focus_arr['module']) && $focus_arr['module'] != '' && $focus_arr['module'] != 'EmailAddress') {

                        $idName = $focus_arr['id_name'];
                        $relate_focus = BeanFactory::getBean($focus_arr['module'], $focus->$idName);

                        $string = CustomTemplateParser::parse_template_bean($string, $focus_arr['name'], $relate_focus);
                    }
                }
            }

        }
        return $string;
    }

    function parse_template_bean($string, $key, &$focus, $calcAccountId = "")
    {
        global $app_strings, $sugar_config;
        $repl_arr = array();

        foreach ($focus->field_defs as $field_def) {

            if (isset($field_def['name']) && $field_def['name'] != '') {
                $fieldName = $field_def['name'];
                if ($field_def['type'] == 'currency') {
                    $repl_arr[$key . "_" . $fieldName] = currency_format_number($focus->$fieldName, $params = array('currency_symbol' => false));
                } else if (($field_def['type'] == 'radioenum' || $field_def['type'] == 'enum' || $field_def['type'] == 'dynamicenum') && isset($field_def['options'])) {
                    $repl_arr[$key . "_" . $fieldName] = translate($field_def['options'], $focus->module_dir, $focus->$fieldName);
                } else if ($field_def['type'] == 'multienum' && isset($field_def['options'])) {
                    $repl_arr[$key . "_" . $fieldName] = implode(', ', unencodeMultienum($focus->$fieldName));
                } //Fix for Windows Server as it needed to be converted to a string.
                else if ($field_def['type'] == 'int') {
                    $repl_arr[$key . "_" . $fieldName] = strval($focus->$fieldName);
                } else if ($field_def['type'] == 'image') {
                    $secureLink = $sugar_config['site_url'] . '/' . "public/". $focus->id .  '_' . $fieldName;
                    $file_location = $sugar_config['upload_dir'] . '/'  . $focus->id .  '_' . $fieldName;
                    // create a copy with correct extension by mime type
                    if(!file_exists('public')) {
                        sugar_mkdir('public', 0777);
                    }
                    if(!copy($file_location, "public/{$focus->id}".  '_' . "$fieldName")) {
                        $secureLink = $sugar_config['site_url'] . '/'. $file_location;
                    }
                    $link = $secureLink;
                    $repl_arr[$key . "_" . $fieldName] = '<img src="' . $link . '" width="'.$field_def['width'].'" height="'.$field_def['height'].'"/>';
                } else if ($field_def['type'] == 'function') {
					// Ad function field values
					$funcResult = "";
					if (!empty($field_def['function']) && isset($field_def['function']) && is_array($field_def['function']) && count($field_def['function']) > 0) {					
						if (!empty($field_def['function']['include']) && isset($field_def['function']['include']) && strlen($field_def['function']['include']) > 0 && file_exists($field_def['function']['include'])) {
							// Skip core function fields and run only customizations
							if (strpos($field_def['function']['include'], "custom/") === false ||
								$fieldName == 'aop_case_updates_threaded_custom')
								continue;
							require_once($field_def['function']['include']);
						}
						if (!empty($field_def['function']['name']) && isset($field_def['function']['name']) && strlen($field_def['function']['name']) > 0)
							$funcResult = call_user_func($field_def['function']['name'], $focus, $fieldName, $calcAccountId, "WorkFlow");
					}

					$repl_arr[$key . "_" . $fieldName] = $funcResult;
				} else {
                    $repl_arr[$key . "_" . $fieldName] = $focus->$fieldName;
                }
				
            }
        } // end foreach()

        krsort($repl_arr);
        reset($repl_arr);

        foreach ($repl_arr as $name => $value) {
            if (strpos($name, 'product_discount') > 0) {
                if ($value != '' && $value != '0.00') {
                    if ($repl_arr['aos_products_quotes_discount'] == 'Percentage') {
                        $sep = get_number_seperators();
                        $value = rtrim(rtrim(format_number($value), '0'), $sep[1]);//.$app_strings['LBL_PERCENTAGE_SYMBOL'];
                    } else {
                        $value = currency_format_number($value, $params = array('currency_symbol' => false));
                    }
                } else {
                    $value = '';
                }
            }
            if ($name === 'aos_products_product_image' && !empty($value)) {
                $value = '<img src="' . $value . '"width="50" height="50"/>';
            }
            if ($name === 'aos_products_quotes_product_qty') {
                $sep = get_number_seperators();
                $value = rtrim(rtrim(format_number($value), '0'), $sep[1]);
            }
            if ($name === 'aos_products_quotes_vat' || strpos($name, 'pct') > 0 || strpos($name, 'percent') > 0 || strpos($name, 'percentage') > 0) {
                $sep = get_number_seperators();
                $value = rtrim(rtrim(format_number($value), '0'), $sep[1]) . $app_strings['LBL_PERCENTAGE_SYMBOL'];
            }
            if (strpos($name, 'date') > 0 || strpos($name, 'expiration') > 0) {
                if ($value != '') {
                    $dt = explode(' ', $value);
                    $value = $dt[0];
                }
            }
            if ($value != '' && is_string($value)) {
                $string = str_replace("\$$name", $value, $string);
            } else if (strpos($name, 'address') > 0) {
                $string = str_replace("\$$name<br />", '', $string);
                $string = str_replace("\$$name <br />", '', $string);
                $string = str_replace("\$$name", '', $string);
            } else {
                $string = str_replace("\$$name", '&nbsp;', $string);
            }

        }

        return $string;
    }
}

?>
