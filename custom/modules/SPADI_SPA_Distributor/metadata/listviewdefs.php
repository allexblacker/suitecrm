<?php
$module_name = 'SPADI_SPA_Distributor';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'DISTRIBUTOR_NAME' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_DISTRIBUTOR_NAME',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPA_SPA_TITLE',
    'id' => 'SPA_SPA_SPADI_SPA_DISTRIBUTOR_1SPA_SPA_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'DISTRIBUTOR_SPA_START_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DISTRIBUTOR_SPA_START_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'DISTRIBUTOR_SPA_END_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DISTRIBUTOR_SPA_END_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'DISTRIBUTOR_PROGRAM_NUMBER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DISTRIBUTOR_PROGRAM_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
