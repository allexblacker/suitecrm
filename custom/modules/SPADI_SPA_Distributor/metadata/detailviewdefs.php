<?php
$module_name = 'SPADI_SPA_Distributor';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'distributor_name',
            'studio' => 'visible',
            'label' => 'LBL_DISTRIBUTOR_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'distributor_spa_start_date',
            'label' => 'LBL_DISTRIBUTOR_SPA_START_DATE',
          ),
          1 => 
          array (
            'name' => 'distributor_spa_end_date',
            'label' => 'LBL_DISTRIBUTOR_SPA_END_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'distributor_program_number',
            'label' => 'LBL_DISTRIBUTOR_PROGRAM_NUMBER',
          ),
          1 => 'description',
        ),
        4 => 
        array (
          0 => 'date_entered',
          1 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
        ),
        5 => 
        array (
          0 => 'date_modified',
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
