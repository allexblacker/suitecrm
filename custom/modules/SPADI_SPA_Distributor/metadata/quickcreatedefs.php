<?php
$module_name = 'SPADI_SPA_Distributor';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'distributor_name',
            'studio' => 'visible',
            'label' => 'LBL_DISTRIBUTOR_NAME',
          ),
          1 => 
          array (
            'name' => 'distributor_program_number',
            'label' => 'LBL_DISTRIBUTOR_PROGRAM_NUMBER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'distributor_spa_start_date',
            'label' => 'LBL_DISTRIBUTOR_SPA_START_DATE',
          ),
          1 => 
          array (
            'name' => 'distributor_spa_end_date',
            'label' => 'LBL_DISTRIBUTOR_SPA_END_DATE',
          ),
        ),
      ),
    ),
  ),
);
?>
