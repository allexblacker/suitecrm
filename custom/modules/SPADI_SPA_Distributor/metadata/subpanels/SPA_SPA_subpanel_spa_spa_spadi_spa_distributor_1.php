<?php
// created: 2018-03-08 09:52:24
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'distributor_name' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_DISTRIBUTOR_NAME',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'account_id_c',
  ),
  'spa_spa_spadi_spa_distributor_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPA_SPA_TITLE',
    'id' => 'SPA_SPA_SPADI_SPA_DISTRIBUTOR_1SPA_SPA_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'SPA_SPA',
    'target_record_key' => 'spa_spa_spadi_spa_distributor_1spa_spa_ida',
  ),
  'distributor_spa_start_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DISTRIBUTOR_SPA_START_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'distributor_spa_end_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DISTRIBUTOR_SPA_END_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
);