<?php
$module_name = 'AOS_Products';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
        'headerTpl' => 'modules/AOS_Products/tpls/EditViewHeader.tpl',
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/AOS_Products/js/products.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'is_active_c',
            'label' => 'LBL_IS_ACTIVE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'kw_c',
            'label' => 'LBL_KW',
          ),
          1 => 
          array (
            'name' => 'product_code_c',
            'label' => 'LBL_PRODUCT_CODE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'product_numbering_c',
            'label' => 'LBL_PRODUCT_NUMBERING',
          ),
          1 => 
          array (
            'name' => 'family_c',
            'studio' => 'visible',
            'label' => 'LBL_FAMILY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'type',
            'label' => 'LBL_TYPE',
          ),
          1 => 
          array (
            'name' => 'product_group_2_c',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_GROUP_2',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'part_type_c',
            'studio' => 'visible',
            'label' => 'LBL_PART_TYPE',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'project_products_c',
            'studio' => 'visible',
            'label' => 'LBL_PROJECT_PRODUCTS',
          ),
          1 => 
          array (
            'name' => 'standard_price_c',
            'label' => 'LBL_STANDARD_PRICE',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      ),
    ),
  ),
);
?>
