<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-04 15:59:42
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_dtbc_pricebook_product_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Pricebook_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
  'get_subpanel_data' => 'aos_products_dtbc_pricebook_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>