<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-05 16:43:58
$dictionary['AOS_Products']['fields']['kw_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['kw_c']['labelValue']='kw';

 

 // created: 2017-10-18 17:03:14
$dictionary['AOS_Products']['fields']['standard_price_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['standard_price_c']['labelValue']='Standard Price';

 

 // created: 2017-10-13 14:47:30
$dictionary['AOS_Products']['fields']['external_id_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['external_id_c']['labelValue']='External ID';

 

 // created: 2017-10-13 15:22:07
$dictionary['AOS_Products']['fields']['is_active_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['is_active_c']['labelValue']='Active';

 

 // created: 2017-11-13 17:50:37
$dictionary['AOS_Products']['fields']['currency_iso_code_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['currency_iso_code_c']['labelValue']='Product Currency';

 

 // created: 2017-10-05 18:24:24
$dictionary['AOS_Products']['fields']['description']['audited']=true;
$dictionary['AOS_Products']['fields']['description']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['description']['comments']='Full text of the note';
$dictionary['AOS_Products']['fields']['description']['merge_filter']='disabled';

 

 // created: 2017-10-13 14:00:20
$dictionary['AOS_Products']['fields']['product_numbering_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['product_numbering_c']['labelValue']='Product Numbering';

 

 // created: 2017-10-13 14:02:58
$dictionary['AOS_Products']['fields']['pssd_product_cost_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['pssd_product_cost_c']['labelValue']='Product Cost';

 

 // created: 2017-10-13 14:44:21
$dictionary['AOS_Products']['fields']['external_data_source_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['external_data_source_c']['labelValue']='External Data Source';

 

 // created: 2017-10-05 18:10:27
$dictionary['AOS_Products']['fields']['product_group_2_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['product_group_2_c']['labelValue']='Product Group 2';

 

// created: 2017-10-04 15:59:42
$dictionary["AOS_Products"]["fields"]["aos_products_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_products_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook_Product',
  'bean_name' => 'dtbc_Pricebook_Product',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);


 // created: 2017-10-13 15:20:15
$dictionary['AOS_Products']['fields']['type']['default']='';
$dictionary['AOS_Products']['fields']['type']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['type']['options']='product_type_list';
$dictionary['AOS_Products']['fields']['type']['merge_filter']='disabled';

 

 // created: 2017-11-13 18:38:16
$dictionary['AOS_Products']['fields']['quantity_unit_of_measures_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['quantity_unit_of_measures_c']['labelValue']='Quantity Unit Of Measures';

 

 // created: 2017-10-05 17:34:38
$dictionary['AOS_Products']['fields']['product_group_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['product_group_c']['labelValue']='Product Type';

 

 // created: 2017-10-13 14:26:41
$dictionary['AOS_Products']['fields']['p1_project_id_c']['inline_edit']=1;

 

 // created: 2017-10-13 14:56:27
$dictionary['AOS_Products']['fields']['maincode']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['maincode']['merge_filter']='disabled';

 

 // created: 2017-10-13 15:23:27
$dictionary['AOS_Products']['fields']['product_id_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['product_id_c']['labelValue']='ID';

 

 // created: 2017-10-05 16:31:00
$dictionary['AOS_Products']['fields']['product_code_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['product_code_c']['labelValue']='Product Code';

 

 // created: 2017-10-05 18:19:52
$dictionary['AOS_Products']['fields']['part_type_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['part_type_c']['labelValue']='Part Type';

 

 // created: 2017-10-13 14:26:41
$dictionary['AOS_Products']['fields']['project_products_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['project_products_c']['labelValue']='Project';

 

 // created: 2017-10-13 15:05:09
$dictionary['AOS_Products']['fields']['display_url_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['display_url_c']['labelValue']='Display URL';

 

 // created: 2017-11-13 18:21:35
$dictionary['AOS_Products']['fields']['family_c']['inline_edit']='1';
$dictionary['AOS_Products']['fields']['family_c']['labelValue']='Product Family';

 
?>