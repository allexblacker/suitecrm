<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

global $current_user;

if ($current_user->isAdmin() != 1)
	return;

class UserPreferencesHandler {
	public function getUserPreference($userId) {
		$retval = "";
		
		if (empty($userId))
			return $retval;
		
		global $db;
		
		$sql = "SELECT *
				FROM user_preferences
				WHERE deleted = 0 AND
				assigned_user_id = " . $db->quoted($userId);

		$res = $db->query($sql);
		
		while ($row = $db->fetchByAssoc($res)) {
			$retval .= "<div style='padding-bottom: 13px'>";
			$retval .= "<div style='font-weight: bold; font-size: 14px'>" . $row['category'] . "</div>";
			$retval .= "<div style='font-style: italic;'>";
			$retval .= "Id: " . $row['id'] . "<br/>";
			$retval .= "Date entered (db value): " . $row['date_entered'] . "<br/>";
			$retval .= "Date modified (db value): " . $row['date_modified'] . "<br/>";
			$retval .= "</div>";
			$retval .= "<pre>";
			$retval .= print_r(unserialize(base64_decode($row['contents'])), true);
			$retval .= "</pre>";
			$retval .= "</div>";
		}
		
		return $retval;
	}
	
	public function getUsersDropdown($default) {
		global $db;
		$retval = "<select name='user_id'>";
		
		$sql = "SELECT id, user_name, first_name, last_name
				FROM users
				WHERE users.deleted = 0 AND
				status = 'Active'
				ORDER BY user_name";
				
		$res = $db->query($sql);
		
		while ($row = $db->fetchByAssoc($res)) {
			$selected = "";
			if ($row['id'] == $default)
				$selected = " selected='selected' ";
			
			$retval .= "<option value='" . $row['id'] . "'" . $selected . ">" . $row['user_name'] . " (" . $row['first_name'] . " " . $row['last_name'] . ")</option>";
		}
		
		$retval .= "</select>";
		
		return $retval;
	}
}

$obj = new UserPreferencesHandler();
$selectedUser = "";

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'UserPreferences' &&
	isset($_REQUEST['user_id']) && strlen($_REQUEST['user_id'])) {
	$selectedUser = $_REQUEST['user_id'];
}

?>

<form method="POST" action="index.php">
	<input type="hidden" name="module" value="Users"/>
	<input type="hidden" name="action" value="UserPreferences"/>
	<label>Selected user:</label>
	<?php echo $obj->getUsersDropdown($selectedUser); ?>
	<br/>
	<input type="submit" value="Get preferences"/>
</form>

<?php echo $obj->getUserPreference($selectedUser); ?>