<?php
$module_name = 'dtbc_System_Workflow_Scheduling';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'WORKFLOW_ID_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_WORKFLOW_ID',
    'id' => 'AOW_WORKFLOW_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'CASE_ID_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CASE_ID',
    'id' => 'ACASE_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'TRIGGERED_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_TRIGGERED',
    'width' => '10%',
  ),
  'TRIGGER_TIME_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_TRIGGER_TIME',
    'width' => '10%',
  ),
);
?>
