<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$db = DBManagerFactory::getInstance();

// Get unnecessary scheduled wfs
$sql  = "SELECT dtbc_system_workflow_scheduling.id
				FROM dtbc_system_workflow_scheduling
				JOIN dtbc_system_workflow_scheduling_cstm ON dtbc_system_workflow_scheduling_cstm.id_c = dtbc_system_workflow_scheduling.id
				JOIN aow_workflow ON aow_workflow.id = dtbc_system_workflow_scheduling_cstm.aow_workflow_id_c
				WHERE dtbc_system_workflow_scheduling.deleted = 0 AND
				dtbc_system_workflow_scheduling_cstm.triggered_c = 0 AND
                dtbc_system_workflow_scheduling_cstm.trigger_time_c < '2018-01-12'";
$resp = $db->query($sql);

while ($row = $db->fetchByAssoc($resp)) {
	echo $row['id'] . "<br>";
	$query = "UPDATE dtbc_system_workflow_scheduling
				SET description = " . $db->quoted("Manually set to triggered on 2018. jan. 12. - Not necessary to run this on a closed case. It happened because of the wf issues at Christmas") . "
				WHERE id = " . $db->quoted($row['id']);
	$db->query($query);
	
	$query = "UPDATE dtbc_system_workflow_scheduling_cstm
				SET triggered_c = 1
				WHERE id_c = " . $db->quoted($row['id']);
	$db->query($query);
}

echo "Finish";