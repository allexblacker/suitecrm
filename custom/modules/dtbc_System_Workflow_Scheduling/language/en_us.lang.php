<?php
// created: 2018-01-18 17:15:53
$mod_strings = array (
  'LBL_CASE_ID' => 'Case',
  'LBL_TRIGGER_TIME' => 'Trigger time',
  'LBL_TRIGGERED' => 'Triggered',
  'LBL_FLEX_RELATE' => 'Related bean',
  'LBL_CURRENT_USER_ID' => 'Current User Id',
);