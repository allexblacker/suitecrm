<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/schedulers.php");

class CustomAL1_Alliance_TransactionController extends SugarController {
	
	public function action_checkPoints() {

		if (empty($_REQUEST['points']) || empty($_REQUEST['record']) || !$this->isAccountValid($_REQUEST['record']))
			$this->returnToUi(0);
		global $db;

		$schedulers = new DtbcSchedulers();
		
		$customWhere = " AND accounts_al1_alliance_transaction_1accounts_ida = " . $db->quoted($_REQUEST['record']);
		if(!empty($_REQUEST['alliance']))
			$customWhere .= " AND accounts_al1_alliance_transaction_1_c.accounts_al1_alliance_transaction_1al1_alliance_transaction_idb != " . $db->quoted($_REQUEST['alliance']) . " ";
		// Get points data for only one account, without yearly point refresh - so this is only a check
		$data = $schedulers->calculateYearlyPoints($customWhere, true);

		$this->returnToUi($this->isBalancePositiveWithNewValue($data, $_REQUEST['points']));
	}
	
	private function isBalancePositiveWithNewValue($data, $points) {
		$balance = 0;
		foreach ($data as $a_id => $years) {
			$balance += $years->getAvailablePoints();
		}

		return $balance - abs($points) > 0 ? "1" : "2";
	}
	
	private function isAccountValid($recordId) {
		global $db;
		
		$sql = "SELECT *
				FROM accounts
				WHERE id = " . $db->quoted($recordId);
				
		$res = $db->fetchOne($sql);
		
		return !empty($res) && is_array($res) && count($res) > 0 && strlen($res['id']) > 0;
	}
	
	private function returnToUi($value) {
		echo $value;
		die();
	}
	
	public function action_getKwFactor(){
		
		$accountBean = BeanFactory::getBean('Accounts', $_REQUEST['account']);
		if(empty($accountBean->kw_factor__c)){
			global $sugar_config;
			echo $sugar_config['solaredge']['default_kw_factor'];
		}
		else
			echo $accountBean->kw_factor__c;
			
		die();
	}
    
}
