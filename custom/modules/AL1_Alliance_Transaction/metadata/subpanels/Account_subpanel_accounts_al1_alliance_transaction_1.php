<?php
// created: 2017-10-30 15:26:45
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'monitoring_site_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'Monitoring_Site_id',
    'width' => '10%',
    'default' => true,
  ),
  'site_name' => 
  array (
    'type' => 'varchar',
    'vname' => 'Site_Name',
    'width' => '10%',
    'default' => true,
  ),
  'type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Type',
    'width' => '10%',
    'default' => true,
  ),
  'points' => 
  array (
    'type' => 'decimal',
    'vname' => 'Points',
    'width' => '10%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'kw' => 
  array (
    'type' => 'decimal',
    'vname' => 'KW',
    'width' => '10%',
    'default' => true,
  ),
  'date1' => 
  array (
    'type' => 'date',
    'vname' => 'Date',
    'width' => '10%',
    'default' => true,
  ),
  'collect_redeem' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Collect_redeem',
    'width' => '10%',
    'default' => true,
  ),
);