var redeemArray = ["Fault_Site", "Gift"];

function isBalancePositive(formName, type) {
	
	if ($("#" + formName + " #collect_redeem").val() == 1 && redeemArray.indexOf(type) == -1)
		return true;
	
	var accountId = getAccountId();
	var	points = Math.abs($("#kw").val()) * getKwFactor(accountId);
	if ($("#kw").val() == '' || $("#kw").val() == 0)
		points = $('#points').val();
	
	if (points == 0)
		return true;
	
	var bool = false;
	$.ajax({
		method: "POST",
		url: "index.php?module=AL1_Alliance_Transaction&action=checkPoints&to_pdf=1",
		async: false,
		data: { 
			points: points,
			record: accountId,
			alliance: getAlliance()
		},
	})
	.done(function (response) {
		// response == 0 -> no points sent
		// response == 1 -> ok, you can add these points
		// response == 2 -> oohh no, to much points!
		if (response == 2) {
			$('[dtbc-data=points_label]').append(getErrorMessage(SUGAR.language.get('AL1_Alliance_Transaction','LBL_INVALID_POINTS')));
			return;
		}
		
		bool = true;
	})
	.fail(function (e) {
		console.log(e);
	});
	
	return bool;
}

function getErrorMessage(message){
	if ($('#InvalidPoints').length)
			$('#InvalidPoints').remove();
	return "<div class='required validation-message' id='InvalidPoints'>" + message + "</div>";
}

function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation(formName) {
	var type = $("#type").val();
	if($("#points").val() < 0 && redeemArray.indexOf(type) == -1){
		$('[dtbc-data=points_label]').append(getErrorMessage(SUGAR.language.get('AL1_Alliance_Transaction','LBL_POINT_SMALLER_THAN_ZERO')));
		return false;
	}
	var retval = isBalancePositive(formName, type);
	var checkForm = check_form(formName);
	// Original validations
	var _form = document.getElementById(formName);
	_form.action.value='Save';
	if (checkForm && retval)
		SUGAR.ajaxUI.submitForm(_form);

	return false;
}

function getKwFactor(accountId){
	var kwFactor;
	$.ajax({
		method: "POST",
		url: "index.php?module=AL1_Alliance_Transaction&action=getKwFactor&to_pdf=1",
		async: false,
		data: {
			account: accountId,
		},
	})
	.done(function (response) {
		kwFactor = response;
	})
	.fail(function (e) {
		console.log(e);
	});
	return kwFactor;
}

function getAlliance(){
	return $("input[name='record']").val();
}

function getAccountId(){
	var accountId;
	if ($('#form_SubpanelQuickCreate_AL1_Alliance_Transaction div.buttons').length)
		accountId = $("#recordid").val();
	else
		accountId = $("#accounts_al1_alliance_transaction_1accounts_ida").val();
	
	return accountId;
}

$(document).ready(function() {
	$("#save_and_continue").remove();
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	if ($('#form_SubpanelQuickCreate_AL1_Alliance_Transaction div.buttons').length)
		$("<input title='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation(\"form_SubpanelQuickCreate_AL1_Alliance_Transaction\");' type='button' name='button' value='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("#form_SubpanelQuickCreate_AL1_Alliance_Transaction div.buttons");
	else
		$("<input title='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation(\"EditView\");' type='button' name='button' value='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
});