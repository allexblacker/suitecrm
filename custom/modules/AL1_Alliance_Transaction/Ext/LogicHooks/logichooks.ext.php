<?php 
 //WARNING: The contents of this file are auto-generated



$hook_array['before_save'][] = array(
	10,
	'After import calculations',
	'custom/include/dtbc/hooks/10_AfterImportCalculations.php',
	'CustomHooks',
	'before_save'
);

$hook_array['before_save'][] = array(
	20,
	'Set name field if necessary',
	'custom/include/dtbc/hooks/20_SetAllianceName.php',
	'CustomNameHook',
	'before_save'
);




$hook_array['after_save'][] = array(
    10,
    'Update Last_Alliance_Transaction__c on Accounts module',
    'custom/include/dtbc/hooks/allianceLogicHooks.php',
    'AllianceLogicHooks',
    'setAllianceTransaction'
);


$hook_array['before_relationship_delete'][] = array(
	10,
	'Sync line items',
	'custom/include/dtbc/hooks/deleteTransaction.php',
	'TransactionRelationShips',
	'before_relationship_delete_from_transact'
); 
?>