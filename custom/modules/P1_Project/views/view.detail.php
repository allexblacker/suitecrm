<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/DetailView/DetailViewCustomCache.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class P1_ProjectViewDetail extends ViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailViewCustomCache();
 	    $this->dv->ss =&  $this->ss;
        
        $this->bean->link_to_maps_c = html_entity_decode($this->bean->link_to_maps_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		$this->bean->link_to_monitoring_c = html_entity_decode($this->bean->link_to_monitoring_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
		$this->bean->link_to_projects_c = html_entity_decode($this->bean->link_to_projects_c, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
}
