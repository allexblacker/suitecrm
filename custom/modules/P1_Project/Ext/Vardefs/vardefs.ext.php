<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-25 18:19:49
$dictionary['P1_Project']['fields']['pre_commissioning_checklist_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['pre_commissioning_checklist_c']['labelValue']='Pre Commissioning Checklist';

 

 // created: 2017-09-25 17:06:23
$dictionary['P1_Project']['fields']['cae_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['cae_c']['labelValue']='CAE';

 

 // created: 2017-09-25 17:37:36
$dictionary['P1_Project']['fields']['inspection_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['inspection_c']['labelValue']='Inspection';

 

 // created: 2017-09-25 20:41:07
$dictionary['P1_Project']['fields']['training_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['training_c']['labelValue']='Training';

 

 // created: 2017-09-25 20:33:47
$dictionary['P1_Project']['fields']['punch_list_complete_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['punch_list_complete_c']['labelValue']='Punch List Complete?';

 

 // created: 2017-12-19 14:46:50

 

 // created: 2017-09-25 17:49:50
$dictionary['P1_Project']['fields']['link_to_monitoring_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['link_to_monitoring_c']['labelValue']='Link To Monitoring';

 

 // created: 2017-09-25 17:10:32
$dictionary['P1_Project']['fields']['communication_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['communication_c']['labelValue']='Communication';

 

 // created: 2017-09-25 17:17:39
$dictionary['P1_Project']['fields']['dc_to_ac_ratio_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['dc_to_ac_ratio_c']['labelValue']='DC to AC ratio';

 

// created: 2017-09-26 12:28:23
$dictionary["P1_Project"]["fields"]["p1_project_dtbc_project_stakeholders_1"] = array (
  'name' => 'p1_project_dtbc_project_stakeholders_1',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_stakeholders_1',
  'source' => 'non-db',
  'module' => 'dtbc_Project_Stakeholders',
  'bean_name' => 'dtbc_Project_Stakeholders',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_STAKEHOLDERS_1_FROM_DTBC_PROJECT_STAKEHOLDERS_TITLE',
);


 // created: 2017-09-25 20:36:31
$dictionary['P1_Project']['fields']['state_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['state_c']['labelValue']='State';

 

 // created: 2017-09-25 18:10:02
$dictionary['P1_Project']['fields']['module_watt_power_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['module_watt_power_c']['labelValue']='Module Watt Power';

 

 // created: 2017-09-25 17:24:46
$dictionary['P1_Project']['fields']['system_handover_to_supp_done_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['system_handover_to_supp_done_c']['labelValue']='System Handover To Support Done';

 

 // created: 2017-09-25 20:40:12
$dictionary['P1_Project']['fields']['system_installation_starts_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['system_installation_starts_c']['labelValue']='Ssystem Installation Starts';

 

// created: 2017-09-25 21:15:11
$dictionary["P1_Project"]["fields"]["users_p1_project_2"] = array (
  'name' => 'users_p1_project_2',
  'type' => 'link',
  'relationship' => 'users_p1_project_2',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_USERS_TITLE',
  'id_name' => 'users_p1_project_2users_ida',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_2_name"] = array (
  'name' => 'users_p1_project_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_p1_project_2users_ida',
  'link' => 'users_p1_project_2',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_2users_ida"] = array (
  'name' => 'users_p1_project_2users_ida',
  'type' => 'link',
  'relationship' => 'users_p1_project_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-09-25 18:15:40
$dictionary['P1_Project']['fields']['optimizer_configuration_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['optimizer_configuration_c']['labelValue']='Optimizer Configuration';

 

 // created: 2017-09-25 18:17:24
$dictionary['P1_Project']['fields']['physical_layout_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['physical_layout_c']['labelValue']='Physical layout';

 

 // created: 2017-09-25 18:09:20
$dictionary['P1_Project']['fields']['monitoring_account_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['monitoring_account_c']['labelValue']='Monitoring Account';

 

 // created: 2017-09-25 17:35:22
$dictionary['P1_Project']['fields']['final_inspection_complete_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['final_inspection_complete_c']['labelValue']='Final Inspection Complete';

 

 // created: 2017-09-25 18:00:17
$dictionary['P1_Project']['fields']['mapping_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['mapping_c']['labelValue']='Mapping';

 

 // created: 2017-09-26 15:26:36

 

 // created: 2017-09-25 17:16:22
$dictionary['P1_Project']['fields']['cpm_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['cpm_c']['labelValue']='CPM';

 

 // created: 2017-09-25 20:25:57
$dictionary['P1_Project']['fields']['project_notice_to_proceed_po_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['project_notice_to_proceed_po_c']['labelValue']='Project Notice To Proceed (PO)';

 

 // created: 2017-09-25 17:23:44
$dictionary['P1_Project']['fields']['system_installation_done_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['system_installation_done_c']['labelValue']='System Installation Starts Done';

 

 // created: 2017-09-25 17:24:09
$dictionary['P1_Project']['fields']['first_system_energize_done_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['first_system_energize_done_c']['labelValue']='First System Energize Done';

 

 // created: 2017-09-25 20:38:20
$dictionary['P1_Project']['fields']['system_additional_desc_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['system_additional_desc_c']['labelValue']='System Additional Description';

 

// created: 2017-09-25 18:12:29
$dictionary["P1_Project"]["fields"]["opportunities_p1_project_1"] = array (
  'name' => 'opportunities_p1_project_1',
  'type' => 'link',
  'relationship' => 'opportunities_p1_project_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_p1_project_1opportunities_ida',
);
$dictionary["P1_Project"]["fields"]["opportunities_p1_project_1_name"] = array (
  'name' => 'opportunities_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_p1_project_1opportunities_ida',
  'link' => 'opportunities_p1_project_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["opportunities_p1_project_1opportunities_ida"] = array (
  'name' => 'opportunities_p1_project_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-09-25 17:21:26
$dictionary['P1_Project']['fields']['project_notice_to_proceed_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['project_notice_to_proceed_c']['labelValue']='Project Notice to Proceed Done';

 

 // created: 2017-09-25 20:30:37
$dictionary['P1_Project']['fields']['project_phase_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['project_phase_c']['labelValue']='Project Phase';

 

// created: 2017-09-25 21:15:05
$dictionary["P1_Project"]["fields"]["users_p1_project_1"] = array (
  'name' => 'users_p1_project_1',
  'type' => 'link',
  'relationship' => 'users_p1_project_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_USERS_TITLE',
  'id_name' => 'users_p1_project_1users_ida',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_1_name"] = array (
  'name' => 'users_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_p1_project_1users_ida',
  'link' => 'users_p1_project_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_1users_ida"] = array (
  'name' => 'users_p1_project_1users_ida',
  'type' => 'link',
  'relationship' => 'users_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-09-25 20:38:43
$dictionary['P1_Project']['fields']['system_energized_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['system_energized_c']['labelValue']='System Energized';

 

 // created: 2017-09-25 17:26:04
$dictionary['P1_Project']['fields']['est_kw_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['est_kw_c']['labelValue']='Est. kw';

 

 // created: 2017-09-25 17:13:16
$dictionary['P1_Project']['fields']['communication_type_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['communication_type_c']['labelValue']='Communication Type';

 

 // created: 2017-12-24 14:59:32
$dictionary['P1_Project']['fields']['test_name_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['test_name_c']['labelValue']='test name';

 

 // created: 2017-09-25 17:35:56
$dictionary['P1_Project']['fields']['first_system_energize_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['first_system_energize_c']['labelValue']='First System Energize';

 

 // created: 2017-09-25 18:10:55
$dictionary['P1_Project']['fields']['number_of_modules_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['number_of_modules_c']['labelValue']='Number Of Modules';

 

// created: 2017-09-25 16:58:45
$dictionary["P1_Project"]["fields"]["accounts_p1_project_1"] = array (
  'name' => 'accounts_p1_project_1',
  'type' => 'link',
  'relationship' => 'accounts_p1_project_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_p1_project_1accounts_ida',
);
$dictionary["P1_Project"]["fields"]["accounts_p1_project_1_name"] = array (
  'name' => 'accounts_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_p1_project_1accounts_ida',
  'link' => 'accounts_p1_project_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["accounts_p1_project_1accounts_ida"] = array (
  'name' => 'accounts_p1_project_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-09-25 20:41:29
$dictionary['P1_Project']['fields']['us_region_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['us_region_c']['labelValue']='US Region';

 

// created: 2017-09-25 18:21:08
$dictionary["P1_Project"]["fields"]["contacts_p1_project_1"] = array (
  'name' => 'contacts_p1_project_1',
  'type' => 'link',
  'relationship' => 'contacts_p1_project_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_p1_project_1contacts_ida',
);
$dictionary["P1_Project"]["fields"]["contacts_p1_project_1_name"] = array (
  'name' => 'contacts_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_p1_project_1contacts_ida',
  'link' => 'contacts_p1_project_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["P1_Project"]["fields"]["contacts_p1_project_1contacts_ida"] = array (
  'name' => 'contacts_p1_project_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-09-25 17:31:07
$dictionary['P1_Project']['fields']['estimated_kw_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['estimated_kw_c']['labelValue']='Estimated KW';

 

 // created: 2017-09-25 16:52:19
$dictionary['P1_Project']['fields']['currencyisocode_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['currencyisocode_c']['labelValue']='Currency';

 

 // created: 2017-09-25 20:34:33
$dictionary['P1_Project']['fields']['region_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['region_c']['labelValue']='Region';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['P1_Project']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-12-19 14:48:54
$dictionary['P1_Project']['fields']['fse_new_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['fse_new_c']['labelValue']='fse new';

 

 // created: 2017-09-25 20:34:11
$dictionary['P1_Project']['fields']['rec_updatec_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['rec_updatec_c']['labelValue']='Rec / Updatec';

 

 // created: 2017-09-25 17:48:43
$dictionary['P1_Project']['fields']['link_to_maps_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['link_to_maps_c']['labelValue']='Link To Maps';

 

// created: 2017-10-18 12:46:47
$dictionary["P1_Project"]["fields"]["p1_project_tasks_1"] = array (
  'name' => 'p1_project_tasks_1',
  'type' => 'link',
  'relationship' => 'p1_project_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_TASKS_TITLE',
);


 // created: 2017-09-25 20:39:40
$dictionary['P1_Project']['fields']['system_handover_to_support_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['system_handover_to_support_c']['labelValue']='System Handover To Support';

 


$dictionary["P1_Project"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'enum',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


// created: 2017-09-26 11:42:36
$dictionary["P1_Project"]["fields"]["p1_project_documents_1"] = array (
  'name' => 'p1_project_documents_1',
  'type' => 'link',
  'relationship' => 'p1_project_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


 // created: 2017-10-16 15:03:48

 

// created: 2017-09-25 20:31:20
$dictionary["P1_Project"]["fields"]["s1_site_p1_project_1"] = array (
  'name' => 's1_site_p1_project_1',
  'type' => 'link',
  'relationship' => 's1_site_p1_project_1',
  'source' => 'non-db',
  'module' => 'S1_Site',
  'bean_name' => 'S1_Site',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_S1_SITE_TITLE',
  'id_name' => 's1_site_p1_project_1s1_site_ida',
);
$dictionary["P1_Project"]["fields"]["s1_site_p1_project_1_name"] = array (
  'name' => 's1_site_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_S1_SITE_TITLE',
  'save' => true,
  'id_name' => 's1_site_p1_project_1s1_site_ida',
  'link' => 's1_site_p1_project_1',
  'table' => 's1_site',
  'module' => 'S1_Site',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["s1_site_p1_project_1s1_site_ida"] = array (
  'name' => 's1_site_p1_project_1s1_site_ida',
  'type' => 'link',
  'relationship' => 's1_site_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-09-25 17:24:21
$dictionary['P1_Project']['fields']['done_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['done_c']['labelValue']='Done';

 

 // created: 2017-09-25 17:50:46
$dictionary['P1_Project']['fields']['link_to_projects_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['link_to_projects_c']['labelValue']='Link To Projects';

 

 // created: 2017-10-24 16:31:22
$dictionary['P1_Project']['fields']['opportunity_stage_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['opportunity_stage_c']['labelValue']='Opportunity Stage';

 

 // created: 2017-09-25 18:10:30
$dictionary['P1_Project']['fields']['monitoring_site_open_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['monitoring_site_open_c']['labelValue']='Monitoring Site Open';

 

 // created: 2017-09-25 18:19:01
$dictionary['P1_Project']['fields']['plant_type_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['plant_type_c']['labelValue']='Plant Type';

 

 // created: 2017-09-25 20:23:25
$dictionary['P1_Project']['fields']['project_evaulation_needed_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['project_evaulation_needed_c']['labelValue']='Project Evaulation Nneeded';

 

 // created: 2017-09-25 18:13:28
$dictionary['P1_Project']['fields']['opportunity_owner_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['opportunity_owner_c']['labelValue']='Opportunity Owner';

 

 // created: 2017-09-25 17:47:45
$dictionary['P1_Project']['fields']['lesson_learnt_issued_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['lesson_learnt_issued_c']['labelValue']='Lesson Learnt Issued';

 

 // created: 2017-09-25 18:16:34
$dictionary['P1_Project']['fields']['optimizers_per_string_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['optimizers_per_string_c']['labelValue']='Optimizers Per String';

 

 // created: 2017-09-25 17:25:07
$dictionary['P1_Project']['fields']['epc_kit_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['epc_kit_c']['labelValue']='EPC Kit';

 

 // created: 2017-09-25 17:33:44
$dictionary['P1_Project']['fields']['evaulation_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['evaulation_c']['labelValue']='Evaulation';

 

// created: 2017-09-26 12:27:38
$dictionary["P1_Project"]["fields"]["p1_project_dtbc_project_comments_1"] = array (
  'name' => 'p1_project_dtbc_project_comments_1',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_comments_1',
  'source' => 'non-db',
  'module' => 'dtbc_Project_Comments',
  'bean_name' => 'dtbc_Project_Comments',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_DTBC_PROJECT_COMMENTS_TITLE',
);


 // created: 2017-09-25 17:04:36
$dictionary['P1_Project']['fields']['all_equipment_arrived_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['all_equipment_arrived_c']['labelValue']='All Equipment Arrived';

 

 // created: 2017-09-25 17:48:04
$dictionary['P1_Project']['fields']['lesson_learned_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['lesson_learned_c']['labelValue']='Lesson Learned';

 

 // created: 2017-09-25 17:15:30
$dictionary['P1_Project']['fields']['country_c']['inline_edit']='1';
$dictionary['P1_Project']['fields']['country_c']['labelValue']='Country';

 

 // created: 2017-12-24 14:59:08

 

 // created: 2017-12-19 14:48:54
$dictionary['P1_Project']['fields']['user_id_c']['inline_edit']=1;

 
?>