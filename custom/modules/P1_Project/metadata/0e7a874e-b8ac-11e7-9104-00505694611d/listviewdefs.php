<?php
$module_name = 'P1_Project';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'COMMISSIONING' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Commissioning',
    'width' => '10%',
  ),
  'COMMISSIONING_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Commissioning_Date',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'FINAL_REPORT_EXTERNAL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Final_Report_External',
    'width' => '10%',
  ),
  'FINAL_REPORT_EXTERNAL_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Final_Report_External_Date',
    'width' => '10%',
    'default' => false,
  ),
  'FSE_PICKLIST' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'FSE_picklist',
    'width' => '10%',
    'default' => false,
  ),
  'FSE_EMAIL' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'FSE_Email',
    'width' => '10%',
    'default' => false,
  ),
  'FSE_EMAIL_ADDRESS_FOR_WF' => 
  array (
    'type' => 'varchar',
    'label' => 'FSE_Email_address_for_WF',
    'width' => '10%',
    'default' => false,
  ),
  'INITIAL_SITE_VISIT' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Initial_Site_Visit',
    'width' => '10%',
  ),
  'KICK_OFF_MEETING_EXTERNAL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Kick_off_meeting_External',
    'width' => '10%',
  ),
  'KICK_OFF_MEETING_EXTERNAL_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Kick_off_meeting_External_Date',
    'width' => '10%',
    'default' => false,
  ),
  'KICK_OFF_MEETING_INTERNAL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Kick_off_meeting_Internal',
    'width' => '10%',
  ),
  'KICK_OFF_MEETING_INTERNAL_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Kick_off_meeting_Internal_date',
    'width' => '10%',
    'default' => false,
  ),
  'MATERIAL_DELIVERY' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Material_Delivery',
    'width' => '10%',
  ),
  'MATERIAL_DELIVERY_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Material_Delivery_Date',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'OFFSITE_TRAINING_SCHEDULED_AS' => 
  array (
    'type' => 'date',
    'label' => 'Offsite_Training_scheduled_as_necessar_D',
    'width' => '10%',
    'default' => false,
  ),
  'OFFSITE_TRAINING_SCHEDULED' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Offsite_Training_scheduled_as_necessar',
    'width' => '10%',
  ),
  'PRE_COMMISSIONING_SITE_VISIT' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Pre_Commissioning_Site_Visit',
    'width' => '10%',
  ),
  'PRE_COMMISSIONING_SITE_VISIT_D' => 
  array (
    'type' => 'date',
    'label' => 'Pre_Commissioning_Site_Visit_Date',
    'width' => '10%',
    'default' => false,
  ),
  'PROJECT_START_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Project_Start_Date',
    'width' => '10%',
    'default' => false,
  ),
  'PROJECT_START_DATE2' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Project_Start_Date2',
    'width' => '10%',
  ),
  'PROJECT_STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Project_Status',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_ADDRESS' => 
  array (
    'type' => 'varchar',
    'label' => 'Site_Address',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_CITY' => 
  array (
    'type' => 'varchar',
    'label' => 'Site_City',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_COUNTRY' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Site_Country',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_EVALUATION' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Site_Evaluation',
    'width' => '10%',
  ),
  'SITE_EVALUATION_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Site_Evaluation_Date',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_STATE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'Site_State',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_VISIT_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Site_Visit_Date',
    'width' => '10%',
    'default' => false,
  ),
  'SITE_ZIP' => 
  array (
    'type' => 'varchar',
    'label' => 'Site_ZIP',
    'width' => '10%',
    'default' => false,
  ),
  'WELCOME_TO_SEDG_EMAIL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Welcome_to_SEDG_Email',
    'width' => '10%',
  ),
  'WELCOME_TO_SUPPORT_EMAIL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'Welcome_to_Support_Email',
    'width' => '10%',
  ),
  'WELCOME_TO_SEDG_EMAIL_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Welcome_to_SEDG_Email_Date',
    'width' => '10%',
    'default' => false,
  ),
  'WELCOME_TO_SUPPORT_EMAIL_DATE' => 
  array (
    'type' => 'date',
    'label' => 'Welcome_to_Support_Email_Date',
    'width' => '10%',
    'default' => false,
  ),
);
?>
