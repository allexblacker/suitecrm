<?php
$module_name = 'P1_Project';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'opportunity_owner_c',
            'label' => 'LBL_OPPORTUNITY_OWNER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accounts_p1_project_1_name',
            'label' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_ACCOUNTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'opportunities_p1_project_1_name',
            'label' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_OPPORTUNITIES_TITLE',
          ),
          1 => 
          array (
            'name' => 'currencyisocode_c',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCYISOCODE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'country_c',
            'label' => 'LBL_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'cpm_c',
            'label' => 'LBL_CPM',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'label' => 'LBL_STATE',
          ),
          1 => 
          array (
            'name' => 'cae_c',
            'label' => 'LBL_CAE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'us_region_c',
            'label' => 'LBL_US_REGION',
          ),
          1 => 
          array (
            'name' => 'fse_picklist',
            'studio' => 'visible',
            'label' => 'FSE_picklist',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'region_c',
            'label' => 'LBL_REGION',
          ),
          1 => 
          array (
            'name' => 'fse_email',
            'studio' => 'visible',
            'label' => 'FSE_Email',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'contacts_p1_project_1_name',
            'label' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_CONTACTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'estimated_kw_c',
            'label' => 'LBL_ESTIMATED_KW',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 's1_site_p1_project_1_name',
            'label' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_S1_SITE_TITLE',
          ),
          1 => 
          array (
            'name' => 'project_status',
            'studio' => 'visible',
            'label' => 'Project_Status',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'link_to_monitoring_c',
            'label' => 'LBL_LINK_TO_MONITORING',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'site_address',
            'label' => 'Site_Address',
          ),
          1 => 
          array (
            'name' => 'site_country',
            'studio' => 'visible',
            'label' => 'Site_Country',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_city',
            'label' => 'Site_City',
          ),
          1 => 
          array (
            'name' => 'site_state',
            'studio' => 'visible',
            'label' => 'Site_State',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'site_zip',
            'label' => 'Site_ZIP',
          ),
          1 => 
          array (
            'name' => 'link_to_maps_c',
            'label' => 'LBL_LINK_TO_MAPS',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'kick_off_meeting_internal',
            'label' => 'Kick_off_meeting_Internal',
          ),
          1 => 
          array (
            'name' => 'kick_off_meeting_external',
            'label' => 'Kick_off_meeting_External',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'kick_off_meeting_internal_date',
            'label' => 'Kick_off_meeting_Internal_date',
          ),
          1 => 
          array (
            'name' => 'kick_off_meeting_external_date',
            'label' => 'Kick_off_meeting_External_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'welcome_to_sedg_email',
            'label' => 'Welcome_to_SEDG_Email',
          ),
          1 => 
          array (
            'name' => 'material_delivery',
            'label' => 'Material_Delivery',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'welcome_to_sedg_email_date',
            'label' => 'Welcome_to_SEDG_Email_Date',
          ),
          1 => 
          array (
            'name' => 'material_delivery_date',
            'label' => 'Material_Delivery_Date',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'offsite_training_scheduled',
            'label' => 'Offsite_Training_scheduled_as_necessar',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'offsite_training_scheduled_as',
            'label' => 'Offsite_Training_scheduled_as_necessar_D',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'project_start_date',
            'label' => 'Project_Start_Date',
          ),
          1 => 
          array (
            'name' => 'pre_commissioning_site_visit',
            'label' => 'Pre_Commissioning_Site_Visit',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'project_start_date2',
            'label' => 'Project_Start_Date2',
          ),
          1 => 
          array (
            'name' => 'pre_commissioning_site_visit_d',
            'label' => 'Pre_Commissioning_Site_Visit_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'initial_site_visit',
            'label' => 'Initial_Site_Visit',
          ),
          1 => 
          array (
            'name' => 'commissioning',
            'label' => 'Commissioning',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'site_visit_date',
            'label' => 'Site_Visit_Date',
          ),
          1 => 
          array (
            'name' => 'commissioning_date',
            'label' => 'Commissioning_Date',
          ),
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'site_evaluation',
            'label' => 'Site_Evaluation',
          ),
          1 => 
          array (
            'name' => 'welcome_to_support_email',
            'label' => 'Welcome_to_Support_Email',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_evaluation_date',
            'label' => 'Site_Evaluation_Date',
          ),
          1 => 
          array (
            'name' => 'welcome_to_support_email_date',
            'label' => 'Welcome_to_Support_Email_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'final_report_external',
            'label' => 'Final_Report_External',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'final_report_external_date',
            'label' => 'Final_Report_External_Date',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
