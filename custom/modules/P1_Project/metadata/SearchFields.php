<?php
// created: 2017-09-26 15:26:36
$searchFields['P1_Project'] = array (
 'searchform_security_group' => 
  array (
    'query_type' => 'default',
    'operator' => 'subquery',
    'subquery' => 'SELECT p1_project.id FROM securitygroups_records
					INNER JOIN p1_project
					ON p1_project.id = securitygroups_records.record_id
						AND p1_project.deleted = 0
					INNER JOIN securitygroups
					ON securitygroups.id = securitygroups_records.securitygroup_id
						AND securitygroups.deleted = 0
					WHERE securitygroups_records.deleted = 0
						AND securitygroups_records.module = \'P1_Project\'
						AND securitygroups.name LIKE',
    'db_field' => 
    array (
      0 => 'id',
  ),
    'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
  ),
  'name' => 
  array (
    'query_type' => 'default',
  ),
  'current_user_only' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'assigned_user_id',
    ),
    'my_items' => true,
    'vname' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
  ),
  'assigned_user_id' => 
  array (
    'query_type' => 'default',
  ),
  'range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
);