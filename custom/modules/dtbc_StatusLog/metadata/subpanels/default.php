<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$module_name = 'dtbc_StatusLog';
$subpanel_layout = array(
    'top_buttons' => '',

    'where' => '',

    'list_fields' => array(
		'name' => array(
            'vname' => 'LBL_NAME',
            'width' => '15%',
        ),
        'cases_dtbc_statuslog_1_name' => array(
            'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_CASES_TITLE',
            'width' => '15%',
        ),
		 'case_status_c' => array(
            'vname' => 'LBL_CASE_STATUS',
            'width' => '15%',
        ),
		 'case_sub_status_c' => array(
            'vname' => 'LBL_CASE_SUB_STATUS',
            'width' => '15%',
        ),
		 'date_start_c' => array(
            'vname' => 'LBL_DATE_START',
            'width' => '15%',
        ),
		'date_end_c' => array(
            'vname' => 'LBL_DATE_END',
            'width' => '15%',
        ),
		'time_spent_c' => array(
            'vname' => 'LBL_TIME_SPENT',
            'width' => '15%',
        ),
		'userid_c' => array(
            'vname' => 'LBL_USERID',
            'width' => '15%',
        ),
    ),
);
