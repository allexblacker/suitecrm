function pricebookPopup(recordId, titleLabel, buttonLabel, dropdownLabel, pricebookId, pricebookWarning) {
	if (pricebookId.length > 0 && !confirm(pricebookWarning))
		return;
	
	SUGAR.ajaxUI.showLoadingPanel();
	var dropdown = getPBDropdown(pricebookId);
	SUGAR.ajaxUI.hideLoadingPanel();
	
	var titleVal = '<strong>' + titleLabel + '</strong>';
	
	var htmltext = '<table style="width: 100%;text-align:left;">'
					+ '<tr><td style="padding: 2px;text-align:left;">'
								+ '<div class="panel-heading">' + dropdownLabel + '</div>'
							+'</td>'
							+'<td>' + dropdown + '</td>'
					+'</tr>';
					
	dialog = new YAHOO.widget.Dialog('details_popup_div', {
		width: '600px',
		fixedcenter: true,
		visible: true,
		draggable: true,
		close: false,
		effect: [
		  {effect: YAHOO.widget.ContainerEffect.FADE, duration: 0.1}],
		modal: true
	});     
    
	dialog.setHeader(titleVal);
	dialog.setBody(htmltext);
	dialog.setFooter('');
	
	var handleCancel = function () {
		this.cancel();
	};

	var handleSubmit = function() {
		window.location.replace("index.php?module=Opportunities&action=selectPricebook&record=" + recordId + "&pricebook=" + $("#pricebook_selector_dd").val());
		this.cancel();
	};
	
	var myButtons = [
		{text: buttonLabel, handler: handleSubmit, isDefault: true},
		{text: SUGAR.language.languages.app_strings.LBL_CANCEL_BUTTON_LABEL, handler: handleCancel, isDefault: true}
	];
	dialog.cfg.queueProperty("buttons", myButtons);
	dialog.render(document.body);
	dialog.show();
	$('#details_popup_div button').addClass(' button');
}

function getPBDropdown(pricebookId) {
	var retval = "<select id='pricebook_selector_dd'/>";
	$.ajax({
		method: "POST",
		url: "index.php?module=Opportunities&action=getPricebooks&to_pdf=1",
		async: false,
	})
	.done(function (response) {
		if (response.length > 0) {
			var ddValues = JSON.parse(response);
			console.log(ddValues);
			var selectedValue = "";
			for (var i = 0; i < ddValues.length; i++) {
				if (pricebookId == ddValues[i].id)
					selectedValue = "selected='selected'";
				retval += "<option value='" + ddValues[i].id + "' " + selectedValue + ">" + ddValues[i].name + "</option>";
				selectedValue = "";
			}
		}
		retval += "</select>";
		return retval;
	})
	.fail(function (e) {
		console.log(e);
	}); 

	retval += "</select>";
	return retval;
}

$(document).ready(function() {
	var pbName = $('#dtbc_selected_pricebook_name').val();
	if (pbName.length > 0)
		$('#whole_subpanel_opportunities_dtbc_line_item_1 #subpanel_title_opportunities_dtbc_line_item_1 div div').append("(" + pbName + ")");
});
