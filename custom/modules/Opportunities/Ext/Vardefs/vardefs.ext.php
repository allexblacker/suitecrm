<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-11 14:02:31
$dictionary['Opportunity']['fields']['expected_revenue_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['expected_revenue_c']['labelValue']='Expected Revenue';

 

 // created: 2017-08-29 12:09:40
$dictionary['Opportunity']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2017-10-25 10:37:25
$dictionary['Opportunity']['fields']['cae_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['cae_c']['labelValue']='CAE';

 

 // created: 2017-10-20 16:30:17
$dictionary['Opportunity']['fields']['requested_delivery_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['requested_delivery_c']['labelValue']='Requested Delivery';

 

 // created: 2017-11-08 19:51:37
$dictionary['Opportunity']['fields']['powerbox_type2_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_type2_c']['labelValue']='PowerBox Type 2';

 

 // created: 2017-10-20 15:51:52
$dictionary['Opportunity']['fields']['comments_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['comments_c']['labelValue']='Comments';

 

 // created: 2017-10-20 15:51:29
$dictionary['Opportunity']['fields']['inactive_items_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inactive_items_c']['labelValue']='Inactive Items (cloned with products)';

 

 // created: 2017-10-20 16:23:55
$dictionary['Opportunity']['fields']['q1_shipped_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q1_shipped_c']['labelValue']='Q1 Shipped';

 

 // created: 2017-11-08 14:02:32
$dictionary['Opportunity']['fields']['total_opportunity_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['total_opportunity_quantity_c']['labelValue']='Total Opportunity Quantity';

 

 // created: 2017-10-23 15:35:10
$dictionary['Opportunity']['fields']['shipped_q_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['shipped_q_c']['labelValue']='Shipped at Q';

 

 // created: 2017-10-11 14:00:23
$dictionary['Opportunity']['fields']['aging_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['aging_c']['labelValue']='aging';

 

 // created: 2017-10-25 11:36:56
$dictionary['Opportunity']['fields']['state_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['state_c']['labelValue']='State';

 

 // created: 2017-11-08 15:46:38
$dictionary['Opportunity']['fields']['tfi_350_mc4_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['tfi_350_mc4_c']['labelValue']='TFI-350-MC4';

 

 // created: 2017-10-06 15:50:05
$dictionary['Opportunity']['fields']['pricebook_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['pricebook_c']['labelValue']='Pricebook';

 

 // created: 2018-02-27 14:08:04
$dictionary['Opportunity']['fields']['sales_stage']['len']=100;
$dictionary['Opportunity']['fields']['sales_stage']['inline_edit']=true;
$dictionary['Opportunity']['fields']['sales_stage']['options']='sales_stage_list';
$dictionary['Opportunity']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['Opportunity']['fields']['sales_stage']['merge_filter']='disabled';

 

 // created: 2017-10-16 15:02:30
$dictionary['Opportunity']['fields']['nam_ship_from_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['nam_ship_from_c']['labelValue']='NAM Ship From';

 

 // created: 2017-08-20 14:14:08
$dictionary['Opportunity']['fields']['erp_customer_number_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['erp_customer_number_c']['labelValue']='ERP Customer Number';

 

 // created: 2017-08-14 15:51:59
$dictionary['Opportunity']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-11-08 15:50:39
$dictionary['Opportunity']['fields']['slot_date_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['slot_date_c']['labelValue']='Slot Date';

 

 // created: 2017-10-11 14:05:17
$dictionary['Opportunity']['fields']['opportunity_expected_revenue_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['opportunity_expected_revenue_c']['labelValue']='Opportunity Expected Revenue';

 

 // created: 2017-08-14 15:51:59
$dictionary['Opportunity']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-10-11 14:16:55
$dictionary['Opportunity']['fields']['owner_target_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['owner_target_c']['labelValue']='Target (M)';

 

 // created: 2017-10-11 14:05:38
$dictionary['Opportunity']['fields']['options_qty_neto_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['options_qty_neto_c']['labelValue']='Options Qty. Neto';

 

 // created: 2017-10-11 14:08:48
$dictionary['Opportunity']['fields']['q3_weighted_price_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q3_weighted_price_c']['labelValue']='Q3 Weighted Price';

 

 // created: 2017-10-20 15:50:05
$dictionary['Opportunity']['fields']['customer_contact_info_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['customer_contact_info_c']['labelValue']='Customer contact info';

 

 // created: 2017-10-20 16:20:29
$dictionary['Opportunity']['fields']['q2_shipped_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q2_shipped_c']['labelValue']='Q2 Shipped';

 

 // created: 2017-10-20 16:31:11
$dictionary['Opportunity']['fields']['booking_date_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['booking_date_c']['labelValue']='Booking date';

 

 // created: 2017-11-08 18:44:45
$dictionary['Opportunity']['fields']['monitoring_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['monitoring_c']['labelValue']='Monitoring License';

 

 // created: 2017-11-09 12:46:47
$dictionary['Opportunity']['fields']['delivery_installation_status_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['delivery_installation_status_c']['labelValue']='Delivery/Installation Status';

 

 // created: 2017-10-06 15:50:05
$dictionary['Opportunity']['fields']['dtbc_pricebook_id_c']['inline_edit']=1;

 

 // created: 2017-11-08 15:44:35
$dictionary['Opportunity']['fields']['csi_250_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['csi_250_c']['labelValue']='CSI-250';

 

 // created: 2017-10-16 15:00:34
$dictionary['Opportunity']['fields']['opportunity_type1_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['opportunity_type1_c']['labelValue']='Opportunity Type';

 

 // created: 2017-10-20 17:30:56
$dictionary['Opportunity']['fields']['cpm_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['cpm_c']['labelValue']='CPM';

 

 // created: 2017-10-20 16:15:04
$dictionary['Opportunity']['fields']['q3_backlog_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q3_backlog_c']['labelValue']='Q3 Backlog';

 

 // created: 2017-10-20 16:27:23
$dictionary['Opportunity']['fields']['commit_level_last_modified_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['commit_level_last_modified_c']['labelValue']='Commit level last modified';

 

 // created: 2017-11-09 13:14:10
$dictionary['Opportunity']['fields']['tracking_number_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['tracking_number_c']['labelValue']='Tracking Number';

 

 // created: 2017-11-08 18:23:41
$dictionary['Opportunity']['fields']['inverter_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverter_type_c']['labelValue']='Inverter Type';

 

 // created: 2017-10-20 16:14:45
$dictionary['Opportunity']['fields']['q2_backlog_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q2_backlog_c']['labelValue']='Q2 Backlog';

 

 // created: 2017-11-08 19:43:46
$dictionary['Opportunity']['fields']['inverter_type3_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverter_type3_c']['labelValue']='Inverter Type 3';

 

 // created: 2017-11-08 16:12:28
$dictionary['Opportunity']['fields']['next_step_help_needed_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['next_step_help_needed_c']['labelValue']='Next step/help needed';

 

 // created: 2017-10-23 15:35:32
$dictionary['Opportunity']['fields']['shipped_todate_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['shipped_todate_c']['labelValue']='Shipped ToDate $';

 

 // created: 2017-08-14 15:51:59
$dictionary['Opportunity']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-10-20 16:00:33
$dictionary['Opportunity']['fields']['geographic_area_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['geographic_area_c']['labelValue']='Geographic area';

 

 // created: 2017-10-11 14:17:14
$dictionary['Opportunity']['fields']['weighted_for_current_q_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['weighted_for_current_q_c']['labelValue']='Weighted for Current Q';

 

 // created: 2017-10-20 15:48:12
$dictionary['Opportunity']['fields']['so_number_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['so_number_c']['labelValue']='SO Number';

 

 // created: 2017-10-24 16:16:41
$dictionary['Opportunity']['fields']['installation_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['installation_type_c']['labelValue']='Project Management Type';

 

 // created: 2017-11-08 14:57:48
$dictionary['Opportunity']['fields']['powerbox_quantity3_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_quantity3_c']['labelValue']='Powerbox Quantity 3';

 

 // created: 2017-09-25 21:17:18
$dictionary['Opportunity']['fields']['total_kw2_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['total_kw2_c']['labelValue']='Total KW';

 

 // created: 2017-10-11 14:00:40
$dictionary['Opportunity']['fields']['closed_q_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['closed_q_c']['labelValue']='ClosedQ';

 

 // created: 2017-11-09 12:54:43
$dictionary['Opportunity']['fields']['order_number_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['order_number_c']['labelValue']='Order Number';

 

 // created: 2017-11-08 15:48:01
$dictionary['Opportunity']['fields']['comm_box_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['comm_box_c']['labelValue']='Comm. Box';

 

 // created: 2017-10-11 14:11:31
$dictionary['Opportunity']['fields']['q2_likely_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q2_likely_c']['labelValue']='Q2 Likely';

 

 // created: 2017-10-20 16:13:44
$dictionary['Opportunity']['fields']['q4_backlog_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q4_backlog_c']['labelValue']='Q4 Backlog';

 

 // created: 2017-11-08 14:45:44
$dictionary['Opportunity']['fields']['probe_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['probe_quantity_c']['labelValue']='Probe Quantity';

 

 // created: 2017-10-11 14:12:49
$dictionary['Opportunity']['fields']['q4_likely_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q4_likely_c']['labelValue']='Q4 Likely';

 

 // created: 2017-10-11 14:16:37
$dictionary['Opportunity']['fields']['targername_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['targername_c']['labelValue']='TargerName';

 

 // created: 2017-10-09 13:40:39
$dictionary['Opportunity']['fields']['date_closed']['inline_edit']=true;
$dictionary['Opportunity']['fields']['date_closed']['comments']='Expected or actual date the oppportunity will close';
$dictionary['Opportunity']['fields']['date_closed']['merge_filter']='disabled';

 

 // created: 2017-10-11 14:09:47
$dictionary['Opportunity']['fields']['q1_shipped_bl_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q1_shipped_bl_c']['labelValue']='Q1 Shipped BL';

 

 // created: 2017-11-09 16:07:58
$dictionary['Opportunity']['fields']['inverter_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverter_quantity_c']['labelValue']='Inverter Quantity';

 

 // created: 2017-11-08 20:03:12
$dictionary['Opportunity']['fields']['csr_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['csr_c']['labelValue']='CSR';

 

 // created: 2017-11-08 14:47:09
$dictionary['Opportunity']['fields']['bridge_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['bridge_quantity_c']['labelValue']='Bridge Quantity';

 

 // created: 2017-10-20 16:30:50
$dictionary['Opportunity']['fields']['monthly_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['monthly_c']['labelValue']='Monthly';

 

 // created: 2017-10-20 16:12:46
$dictionary['Opportunity']['fields']['q1_backlog_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q1_backlog_c']['labelValue']='Q1 Backlog';

 

 // created: 2017-11-08 17:04:31
$dictionary['Opportunity']['fields']['forecast_category_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['forecast_category_c']['labelValue']='Forecast Category';

 

 // created: 2017-11-08 15:31:42
$dictionary['Opportunity']['fields']['se6000_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['se6000_c']['labelValue']='SE6000';

 

 // created: 2017-10-11 14:13:10
$dictionary['Opportunity']['fields']['shortcommitmentlevel_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['shortcommitmentlevel_c']['labelValue']='ShortCommitmentLevel';

 

 // created: 2017-10-20 15:47:53
$dictionary['Opportunity']['fields']['why_lost_at_distributor_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['why_lost_at_distributor_c']['labelValue']='Why lost at distributor';

 

 // created: 2017-10-16 15:00:00
$dictionary['Opportunity']['fields']['us_region_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['us_region_c']['labelValue']='US Region';

 

 // created: 2017-10-11 14:11:11
$dictionary['Opportunity']['fields']['q2_shipped_bl_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q2_shipped_bl_c']['labelValue']='Q2 Shipped BL';

 

 // created: 2017-10-11 14:07:27
$dictionary['Opportunity']['fields']['others_qty_neto_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['others_qty_neto_c']['labelValue']='Others Qty. Neto';

 

 // created: 2017-10-25 17:08:41
$dictionary['Opportunity']['fields']['currency_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['currency_c']['labelValue']='currency';

 

 // created: 2017-10-11 14:08:29
$dictionary['Opportunity']['fields']['q2_weighted_price_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q2_weighted_price_c']['labelValue']='Q2 Weighted Price';

 

 // created: 2017-10-20 15:47:32
$dictionary['Opportunity']['fields']['erp_cust_name_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['erp_cust_name_c']['labelValue']='ERP Customer Name';

 

 // created: 2017-10-20 16:08:12
$dictionary['Opportunity']['fields']['estimated_kw_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['estimated_kw_c']['labelValue']='Estimated KW';

 

 // created: 2017-10-16 15:01:56
$dictionary['Opportunity']['fields']['open_booked_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['open_booked_c']['labelValue']='Open Booked';

 

// created: 2017-10-04 15:48:02
$dictionary["Opportunity"]["fields"]["opportunities_dtbc_line_item_1"] = array (
  'name' => 'opportunities_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'dtbc_Line_Item',
  'bean_name' => 'dtbc_Line_Item',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);


 // created: 2017-11-08 16:24:47
$dictionary['Opportunity']['fields']['help_field_for_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['help_field_for_type_c']['labelValue']='Help field for';

 

 // created: 2017-11-08 16:16:37
$dictionary['Opportunity']['fields']['winning_factor_distributor_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['winning_factor_distributor_c']['labelValue']='Winning factor at distributor';

 

 // created: 2017-11-08 15:29:31
$dictionary['Opportunity']['fields']['weights_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['weights_c']['labelValue']='Weights';

 

// created: 2017-09-25 18:12:29
$dictionary["Opportunity"]["fields"]["opportunities_p1_project_1"] = array (
  'name' => 'opportunities_p1_project_1',
  'type' => 'link',
  'relationship' => 'opportunities_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-08-29 12:21:54
$dictionary['Opportunity']['fields']['region_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['region_c']['labelValue']='Region';

 

 // created: 2017-10-26 20:44:33
$dictionary['Opportunity']['fields']['amount']['audited']=true;
$dictionary['Opportunity']['fields']['amount']['inline_edit']=true;
$dictionary['Opportunity']['fields']['amount']['comments']='Unconverted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['amount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount']['required']=false;

 

// created: 2017-10-04 15:28:36
$dictionary["Opportunity"]["fields"]["opportunities_dtbc_contact_roles_1"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'dtbc_Contact_Roles',
  'bean_name' => 'dtbc_Contact_Roles',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);



if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Opportunity']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-10-20 16:25:32
$dictionary['Opportunity']['fields']['status_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['status_c']['labelValue']='Status';

 

 // created: 2017-10-24 16:15:05
$dictionary['Opportunity']['fields']['account_name_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['account_name_c']['labelValue']='Account Name';

 

 // created: 2017-11-03 18:18:56
$dictionary['Opportunity']['fields']['connector_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['connector_type_c']['labelValue']='Connector Type';

 

 // created: 2017-11-08 18:38:13
$dictionary['Opportunity']['fields']['probe_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['probe_type_c']['labelValue']='Probe';

 

 // created: 2017-11-08 14:43:30
$dictionary['Opportunity']['fields']['polestar_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['polestar_c']['labelValue']='Polestar';

 

 // created: 2017-10-11 14:10:35
$dictionary['Opportunity']['fields']['q1_likely_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q1_likely_c']['labelValue']='Q1 Likely';

 

 // created: 2017-10-11 14:09:09
$dictionary['Opportunity']['fields']['q4_weighted_price_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q4_weighted_price_c']['labelValue']='Q4 Weighted Price';

 

 // created: 2017-11-03 18:18:36
$dictionary['Opportunity']['fields']['application_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['application_c']['labelValue']='Application';

 

 // created: 2017-11-08 18:53:50
$dictionary['Opportunity']['fields']['monitoring_service_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['monitoring_service_c']['labelValue']='Monitoring Service';

 
 

$dictionary["Opportunity"]["fields"]["total_inverters_c"] = array(
	'name' => 'total_inverters_c',
	'label' => 'LBL_FUNCTION_TOTAL_INVERTERS',
	'vname' => 'LBL_FUNCTION_TOTAL_INVERTERS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalInverters',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_kw2_c"] = array(
	'name' => 'total_kw2_c',
	'label' => 'LBL_FUNCTION_TOTAL_KW2',
	'vname' => 'LBL_FUNCTION_TOTAL_KW2',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalKw2',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_options_c"] = array(
	'name' => 'total_options_c',
	'label' => 'LBL_FUNCTION_TOTAL_OPTIONS',
	'vname' => 'LBL_FUNCTION_TOTAL_OPTIONS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalOptions',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_products_c"] = array(
	'name' => 'total_products_c',
	'label' => 'LBL_FUNCTION_TOTAL_PRODUCTS',
	'vname' => 'LBL_FUNCTION_TOTAL_PRODUCTS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalProducts',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_others_c"] = array(
	'name' => 'total_others_c',
	'label' => 'LBL_FUNCTION_TOTAL_OTHERS',
	'vname' => 'LBL_FUNCTION_TOTAL_OTHERS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalOthers',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q1_total_price_c"] = array(
	'name' => 'q1_total_price_c',
	'label' => 'LBL_FUNCTION_Q1_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q1_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ1TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q2_total_price_c"] = array(
	'name' => 'q2_total_price_c',
	'label' => 'LBL_FUNCTION_Q2_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q2_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ2TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q3_total_price_c"] = array(
	'name' => 'q3_total_price_c',
	'label' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ3TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q3_total_price_c"] = array(
	'name' => 'q4_total_price_c',
	'label' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ4TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

 // created: 2017-10-11 14:13:27
$dictionary['Opportunity']['fields']['stageindicator_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['stageindicator_c']['labelValue']='Stage Indicator';

 

 // created: 2017-11-09 12:52:55
$dictionary['Opportunity']['fields']['main_competitors_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['main_competitors_c']['labelValue']='Main Competitors';

 

 // created: 2017-10-05 14:13:45
$dictionary['Opportunity']['fields']['total_split_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['total_split_c']['labelValue']='Total Split';

 


$dictionary["Opportunity"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'enum',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-11-08 15:40:24
$dictionary['Opportunity']['fields']['yes_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['yes_c']['labelValue']='OPP Open?';

 

 // created: 2017-11-08 15:32:07
$dictionary['Opportunity']['fields']['se8000_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['se8000_c']['labelValue']='SE8000';

 

 // created: 2017-11-06 14:25:29
$dictionary['Opportunity']['fields']['bridge_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['bridge_type_c']['labelValue']='Bridge Type';

 

 // created: 2017-10-24 11:46:28
$dictionary['Opportunity']['fields']['opportunity_type']['len']=100;
$dictionary['Opportunity']['fields']['opportunity_type']['inline_edit']=true;
$dictionary['Opportunity']['fields']['opportunity_type']['comments']='Type of opportunity (ex: Existing, New)';
$dictionary['Opportunity']['fields']['opportunity_type']['merge_filter']='disabled';

 

 // created: 2017-11-09 16:33:35
$dictionary['Opportunity']['fields']['inverters_qty_neto_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverters_qty_neto_c']['labelValue']='Inverters Qty. Neto';

 

 // created: 2017-10-11 14:02:04
$dictionary['Opportunity']['fields']['open_closed_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['open_closed_c']['labelValue']='Open/Closed';

 

 // created: 2017-11-08 19:58:09
$dictionary['Opportunity']['fields']['powerbox_type3_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_type3_c']['labelValue']='PowerBox Type 3';

 

 // created: 2017-10-11 14:08:08
$dictionary['Opportunity']['fields']['q1_weighted_price_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q1_weighted_price_c']['labelValue']='Q1 Weighted Price';

 

 // created: 2017-10-12 16:27:10
$dictionary['Opportunity']['fields']['closed_year_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['closed_year_c']['labelValue']='Closed Year';

 

 // created: 2017-10-20 16:05:45
$dictionary['Opportunity']['fields']['q4_shipped_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q4_shipped_c']['labelValue']='Q4 Shipped';

 

 // created: 2017-11-08 15:55:13
$dictionary['Opportunity']['fields']['send_help_daniel_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['send_help_daniel_c']['labelValue']='Send help needed to Daniel';

 

 // created: 2017-11-08 18:27:11
$dictionary['Opportunity']['fields']['powerbox_type_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_type_c']['labelValue']='PowerBox Type';

 

 // created: 2017-11-08 14:48:13
$dictionary['Opportunity']['fields']['supporter_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['supporter_quantity_c']['labelValue']='Supporter Quantity';

 

 // created: 2017-10-20 15:56:16
$dictionary['Opportunity']['fields']['duration_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['duration_c']['labelValue']='Duration';

 

 // created: 2017-11-08 14:55:31
$dictionary['Opportunity']['fields']['inverter_quantity3_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverter_quantity3_c']['labelValue']='Inverter Quantity 3';

 

 // created: 2017-11-08 19:27:50
$dictionary['Opportunity']['fields']['inverter_type2_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverter_type2_c']['labelValue']='Inverter Type 2';

 

 // created: 2017-11-08 15:00:26
$dictionary['Opportunity']['fields']['slotted_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['slotted_c']['labelValue']='Slotted';

 

 // created: 2017-11-08 13:43:14
$dictionary['Opportunity']['fields']['isprivate_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['isprivate_c']['labelValue']='isprivate';

 

 // created: 2017-10-11 14:12:33
$dictionary['Opportunity']['fields']['q4_shipped_bl_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q4_shipped_bl_c']['labelValue']='Q4 Shipped BL';

 

 // created: 2017-10-20 15:46:43
$dictionary['Opportunity']['fields']['customer_poc_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['customer_poc_c']['labelValue']='Customer POC';

 

 // created: 2017-10-25 11:42:36
$dictionary['Opportunity']['fields']['bklog_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['bklog_c']['labelValue']='Backlog for Qtr';

 

 // created: 2017-11-08 18:14:10
$dictionary['Opportunity']['fields']['powerbox_type4_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_type4_c']['labelValue']='Power Box Type 4';

 

 // created: 2017-10-11 14:12:18
$dictionary['Opportunity']['fields']['q3_likely_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q3_likely_c']['labelValue']='Q3 Likely';

 

 // created: 2017-11-09 17:03:08
$dictionary['Opportunity']['fields']['name']['len']='255';
$dictionary['Opportunity']['fields']['name']['inline_edit']=true;
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
);

 

 // created: 2017-10-20 16:26:30
$dictionary['Opportunity']['fields']['confirmed_ship_date_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['confirmed_ship_date_c']['labelValue']='confirmed ship date';

 

 // created: 2017-11-09 13:03:49
$dictionary['Opportunity']['fields']['supporter_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['supporter_c']['labelValue']='Supporter';

 

 // created: 2017-08-29 12:13:05
$dictionary['Opportunity']['fields']['sendingwhs_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['sendingwhs_c']['labelValue']='SendingWhs';

 

 // created: 2017-11-08 14:33:50
$dictionary['Opportunity']['fields']['powerbox_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_quantity_c']['labelValue']='PowerBox Quantity';

 

 // created: 2017-10-11 14:11:58
$dictionary['Opportunity']['fields']['q3_shipped_bl_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q3_shipped_bl_c']['labelValue']='Q3 Shipped BL';

 

 // created: 2017-10-23 15:33:47
$dictionary['Opportunity']['fields']['po_sum_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['po_sum_c']['labelValue']='Booking $ (ERP)';

 

 // created: 2017-11-08 14:56:43
$dictionary['Opportunity']['fields']['powerbox_quantity2_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['powerbox_quantity2_c']['labelValue']='Powerbox Quantity 2';

 

 // created: 2017-11-08 14:54:23
$dictionary['Opportunity']['fields']['inverter_quantity2_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['inverter_quantity2_c']['labelValue']='Inverter Quantity 2';

 

 // created: 2017-10-23 15:32:40
$dictionary['Opportunity']['fields']['backlog_for_next_q_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['backlog_for_next_q_c']['labelValue']='Backlog for Next Q';

 

 // created: 2017-08-14 15:51:59
$dictionary['Opportunity']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-10-11 14:07:52
$dictionary['Opportunity']['fields']['power_optimizer_qty_neto_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['power_optimizer_qty_neto_c']['labelValue']='Power Optimizer Qty. Neto';

 

 // created: 2017-10-23 15:32:17
$dictionary['Opportunity']['fields']['backlog_to_date_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['backlog_to_date_c']['labelValue']='Backlog To Date $';

 

 // created: 2017-11-08 15:49:28
$dictionary['Opportunity']['fields']['empty_inverter_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['empty_inverter_c']['labelValue']='Empty Inverter';

 

 // created: 2017-08-17 15:34:52
$dictionary['Opportunity']['fields']['oppnum_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['oppnum_c']['labelValue']='Opp#';

 

 // created: 2017-10-25 10:29:26
$dictionary['Opportunity']['fields']['commit_level_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['commit_level_c']['labelValue']='Commit Level';

 

 // created: 2017-10-20 15:51:06
$dictionary['Opportunity']['fields']['why_lost_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['why_lost_c']['labelValue']='Why lost';

 

 // created: 2017-11-08 16:08:24
$dictionary['Opportunity']['fields']['winning_factor_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['winning_factor_c']['labelValue']='Winning Factor';

 

 // created: 2017-11-08 15:52:26
$dictionary['Opportunity']['fields']['empty_powerbox_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['empty_powerbox_c']['labelValue']='Empty Powerbox';

 

 // created: 2017-10-20 15:57:13
$dictionary['Opportunity']['fields']['q3_shipped_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['q3_shipped_c']['labelValue']='Q3 Shipped';

 

 // created: 2017-11-03 16:35:29
$dictionary['Opportunity']['fields']['lead_source']['len']=100;
$dictionary['Opportunity']['fields']['lead_source']['inline_edit']=true;
$dictionary['Opportunity']['fields']['lead_source']['options']='leadsource_list';
$dictionary['Opportunity']['fields']['lead_source']['comments']='Source of the opportunity';
$dictionary['Opportunity']['fields']['lead_source']['merge_filter']='disabled';

 

 // created: 2017-10-24 16:15:31
$dictionary['Opportunity']['fields']['country_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['country_c']['labelValue']='Country';

 

 // created: 2017-10-20 17:30:56
$dictionary['Opportunity']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2017-10-20 15:52:12
$dictionary['Opportunity']['fields']['distributor_status_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['distributor_status_c']['labelValue']='Distributor Status';

 
?>