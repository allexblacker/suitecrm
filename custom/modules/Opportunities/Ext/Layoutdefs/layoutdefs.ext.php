<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-04 15:48:02
$layout_defs["Opportunities"]["subpanel_setup"]['opportunities_dtbc_line_item_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Line_Item',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
  'get_subpanel_data' => 'opportunities_dtbc_line_item_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-09-25 18:12:29
$layout_defs["Opportunities"]["subpanel_setup"]['opportunities_p1_project_1'] = array (
  'order' => 100,
  'module' => 'P1_Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
  'get_subpanel_data' => 'opportunities_p1_project_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);




unset($layout_defs['Opportunities']['subpanel_setup']['project']);




 // created: 2017-10-04 15:28:36
$layout_defs["Opportunities"]["subpanel_setup"]['opportunities_dtbc_contact_roles_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Contact_Roles',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
  'get_subpanel_data' => 'opportunities_dtbc_contact_roles_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



$topButtons = array(
    0 => array('widget_class' => 'SubPanelTopSelectPriceBookButton'),
    1 => array('widget_class' => 'SubPanelTopCreateCustom'),
);

$layout_defs['Opportunities']['subpanel_setup']['opportunities_dtbc_line_item_1']['top_buttons'] = $topButtons;

//auto-generated file DO NOT EDIT
$layout_defs['Opportunities']['subpanel_setup']['opportunities_dtbc_contact_roles_1']['override_subpanel_name'] = 'Opportunity_subpanel_opportunities_dtbc_contact_roles_1';


//auto-generated file DO NOT EDIT
$layout_defs['Opportunities']['subpanel_setup']['opportunity_aos_quotes']['override_subpanel_name'] = 'Opportunity_subpanel_opportunity_aos_quotes';



$topButtons = array(
    array('widget_class' => 'SubPanelTopCreateButton'),
);

$layout_defs['Opportunities']['subpanel_setup']['opportunity_aos_quotes']['top_buttons'] = $topButtons;

//auto-generated file DO NOT EDIT
$layout_defs['Opportunities']['subpanel_setup']['opportunities_dtbc_line_item_1']['override_subpanel_name'] = 'Opportunity_subpanel_opportunities_dtbc_line_item_1';



$topButtons = array(
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateOpportunityButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
);

$layout_defs['Opportunities']['subpanel_setup']['opportunities_p1_project_1']['top_buttons'] = $topButtons;
?>