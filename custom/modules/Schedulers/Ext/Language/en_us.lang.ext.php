<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_NOT_FOUND'] = 'Scheduler not found!';
$mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_IS_ALREADY_IN_QUEUE'] = 'Scheduler is already in the job queue! Status: ';
$mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_RUNNED_BY_AN_ANOTHER_PROCESS'] = 'Scheduler is runned by an another process!';
$mod_strings['LBL_RUN_SCHEDULER_JOB_DONE'] = 'Job done';
$mod_strings['LBL_RUN_SCHEDULER_MENU'] = 'Run Scheduler';


$mod_strings['LBL_CUSTOM_CRON_JOB']='Calculate Lead Age Job';


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$mod_strings['LBL_CUSTOM_SOL_CRON_JOB']= 'Update Filed Lists Table';
 

$mod_strings['LBL_CASE_JOB_2DAYS'] = 'Case - Two days after contacted with customer';
$mod_strings['LBL_CASE_JOB_3DAYS'] = 'Case - Three days after Waiting for customer';
$mod_strings['LBL_CASE_JOB_5DAYS'] = 'Case - Five days after Waiting for customer';
$mod_strings['LBL_EMAIL_TO_CASE'] = 'Create cases from emails';
$mod_strings['LBL_CALCULATE_YEARLY_POINTS'] = 'Calculate yearly point values';
$mod_strings['LBL_SYSTEMWORKFLOWS'] = 'Runs System Workflows - aka timed Workflows';
$mod_strings['LBL_CUSTOM_INBOUND_EMAILS'] = 'Check Inbound Mailboxes - And replace variables in the email template';
$mod_strings['LBL_CUSTOM_INBOUND_EMAILS_RESET'] = 'Reset Inbound email job, after the time out period';
$mod_strings['LBL_CUSTOM_WORKFLOW_PROCESS'] = 'Process Workflow Tasks - And use custom order';
$mod_strings['LBL_RESET_JOB'] = 'Reset Job';
$mod_strings['LBL_EMAIL_QUEUE'] = 'Sending Emails from Queue';

?>