<?php 
 //WARNING: The contents of this file are auto-generated




$job_strings['CalcLeadAgeJob'] = 'custom_cron_job';



function custom_cron_job(){
//    require_once 'custom/include/customScheduledTasks.php';
    //do the function here.
    $bean = BeanFactory::getBean('Leads');
    $query = "leads.status <> 'Converted' and leads.status <> 'Dead'";
    $actLeads = $bean->get_full_list('',$query);
    foreach($actLeads as $actLead){
       
       //Mark each meeting as Not Held
       $tmp = new DateTime();
       $sToday = $tmp->format("Y-m-d");
       $Today = new DateTime($sToday);
       $sEnterDate = $actLead->date_entered; 
       $tmpEnterDate = new DateTime($sEnterDate) ;
       $sEnterDate = $tmpEnterDate->format("Y-m-d");
       $EnterDate = new DateTime($sEnterDate);
       $dateDifference = $Today->diff($EnterDate);   
       $actLead->lead_age_c = $dateDifference->days;
       $actLead->save();
        
    }

    return true; // Remember to return the result of successful execution whether this be from a function or not.
    ///return my_custom_function_from_my_included_file();
}



array_push($job_strings, 'custom_workflow_process');

function custom_workflow_process($schedulersJobObj) {
	require_once('custom/modules/AOW_WorkFlow/AOW_WorkFlow.php');
	$_REQUEST['module'] = 'Schedulers';
	$_REQUEST['record'] = $schedulersJobObj->scheduler_id;
    $workflow = new CustomAOW_WorkFlow();
    return $workflow->run_flows();
}




array_push($job_strings, 'calculate_yearly_points');

function calculate_yearly_points() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->calculateYearlyPoints();
}




array_push($job_strings, 'case_job_5days');

function case_job_5days() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->caseStatuses("case_job_5days");
}




array_push($job_strings, 'email_to_case');

function email_to_case() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->emailToCase();
}




array_push($job_strings, 'systemWorkflows');

function systemWorkflows() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->systemWorkflows();
}




array_push($job_strings, 'case_job_3days');

function case_job_3days() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->caseStatuses("case_job_3days");
}




array_push($job_strings, 'custom_inbound_emails_reset');

function custom_inbound_emails_reset() {
	require_once("custom/include/dtbc/InboundEmailScheduler.php");
	
	$temp = new InboundEmailScheduler();
	
	return $temp->resetScheduler(true);
}




array_push($job_strings, 'custom_inbound_emails');

function custom_inbound_emails() {
	require_once("custom/include/dtbc/InboundEmailScheduler.php");
	
	$temp = new InboundEmailScheduler();
	
	return $temp->runScheduler();
}





$job_strings['UpdateFieldListsTable'] = 'custom_sol_cron_job';



function custom_sol_cron_job(){
    require_once 'custom/include/customScheduledTasks.php';
    require_once 'modules/ModuleBuilder/views/view.modulefields.php';

    global $db;
    global $dictionary;
    global $app_list_strings;
    global $moduleList; //the list of modules;

    foreach($moduleList as $module){ //iterating through the list of modules
       $bean = BeanFactory::newBean($module); //creating a bean
       if(!empty($bean)){
            $fieldDefinitons = $bean->getFieldDefinitions(); //getting field definitions of a bean
            //echo "<pre>";
            //print_r($fieldDefinitons);
            //echo "</pre>";
            foreach($fieldDefinitons as $key => $value){
                if(($value['type'] == 'enum' || $value['type'] == 'dynamicenum') && $value['source'] != 'non-db'){     //if the field is an enum (dropdown), add to the enums array, exlude non-db fields!
                    $fieldname = $key;
                    $listname = $value['options'];
                    $list = array();
                    if (isset($app_list_strings[$listname]))
                    {
                        $list = $app_list_strings[$listname];
                        foreach ($list as $key => $value)
                        {
                            //find if record exist
                            if($value != '')
                            {
                                $sql = "SELECT kob_value FROM kob_fieldlist WHERE kob_fieldname = '" . $fieldname . "' AND kob_key = '" . $key . "'";
                                //echo $sql . "<BR/>";
                                $result = $db->query($sql);
                                if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) {
                                        foreach($row as $k => $v)
                                        {
                                           //echo $k . " - " . $v . "<BR/>";
                                           if($v != $value)
                                           {
                                               $sql = "UPDATE kob_fieldlist SET kob_value = '" . $value . "' WHERE kob_fieldname = '" . $fieldname . "' AND kob_key = '" . $key . "'";
                                           }
                                        }
                                    }
                                }
                                else {
                                    $sql = "INSERT INTO kob_fieldlist(idkob_fieldlist, kob_fieldname, kob_listname, kob_key, kob_value)
                                            VALUES (UUID(),'" . $fieldname . "','" . $listname . "','" . $key . "','" . $value . "')";
                                    $db->query($sql);
                                    //echo $sql . "<BR/>";
                                    //echo '$module' . $module . 'Field Name: ' . $fieldname . 'List Name: ' . $listname . 'Key: ' . $key .'Value: ' . $value . '<BR/>';
                                }
                            }
                        }
                    }
                }
            }
       }
    }
    return true; // Remember to return the result of successful execution whether this be from a function or not.
    ///return my_custom_function_from_my_included_file();
}



array_push($job_strings, 'email_queue');

function email_queue($schedulersJobObj) {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->emailQueue();
}




array_push($job_strings, 'case_job_2days');

function case_job_2days() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->customerNotifier2days();
}


?>