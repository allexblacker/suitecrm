<?php

require_once('custom/include/RunScheduler/CustomSugarCronJobs.php');

$db = DBManagerFactory::getInstance();
$job_handler = new CustomSugarCronJobs();

$job_handler->verbose = true;
register_shutdown_function(array($job_handler, 'unexpectedExit'));

$scheduler = BeanFactory::getBean('Schedulers')->retrieve($_REQUEST['record']);

if (!$scheduler) {
	echo $mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_NOT_FOUND'];
	
	return;
}

$job_status = $db->getOne("
	SELECT
		status
	FROM
		job_queue
	WHERE
		deleted = 0
		AND scheduler_id = " . $db->quoted($scheduler->id) . "
		AND status != " . $db->quoted(SchedulersJob::JOB_STATUS_DONE)
);
if ($job_status) {
	echo $mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_IS_ALREADY_IN_QUEUE'] . $job_status;
	
	return;
}

$job = $scheduler->createJob();
$job_handler->queue->submitJob($job);

$job_handler->job = $job_handler->queue->nextJob($job_handler->getMyId());
if (empty($job_handler->job)) {
	echo $mod_strings['LBL_RUN_SCHEDULER_SCHEDULER_RUNNED_BY_AN_ANOTHER_PROCESS'];
	
	return;
}

if (!$job_handler->job->runJob()) {
	$job_handler->jobFailing($job_handler->job);
}
$job_handler->job = null;

if ($job_handler->runOk()) {
	echo $mod_strings['LBL_RUN_SCHEDULER_JOB_DONE'];
}
