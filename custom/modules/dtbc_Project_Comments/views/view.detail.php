<?php

require_once('include/MVC/View/views/view.detail.php');

class dtbc_Project_CommentsViewDetail extends ViewDetail{
	
	public function preDisplay(){
		
		parent::preDisplay();
		$this->bean->description = html_entity_decode($this->bean->description);
	}
}