<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-26 13:48:53
$dictionary['dtbc_Project_Comments']['fields']['description']['inline_edit']=true;
$dictionary['dtbc_Project_Comments']['fields']['description']['comments']='Full text of the note';
$dictionary['dtbc_Project_Comments']['fields']['description']['merge_filter']='disabled';
$dictionary['dtbc_Project_Comments']['fields']['description']['rows']='12';

 

// created: 2017-09-26 12:27:38
$dictionary["dtbc_Project_Comments"]["fields"]["p1_project_dtbc_project_comments_1"] = array (
  'name' => 'p1_project_dtbc_project_comments_1',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_comments_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_P1_PROJECT_TITLE',
  'id_name' => 'p1_project_dtbc_project_comments_1p1_project_ida',
);
$dictionary["dtbc_Project_Comments"]["fields"]["p1_project_dtbc_project_comments_1_name"] = array (
  'name' => 'p1_project_dtbc_project_comments_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_P1_PROJECT_TITLE',
  'save' => true,
  'id_name' => 'p1_project_dtbc_project_comments_1p1_project_ida',
  'link' => 'p1_project_dtbc_project_comments_1',
  'table' => 'p1_project',
  'module' => 'P1_Project',
  'rname' => 'name',
);
$dictionary["dtbc_Project_Comments"]["fields"]["p1_project_dtbc_project_comments_1p1_project_ida"] = array (
  'name' => 'p1_project_dtbc_project_comments_1p1_project_ida',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_comments_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_DTBC_PROJECT_COMMENTS_TITLE',
);

?>