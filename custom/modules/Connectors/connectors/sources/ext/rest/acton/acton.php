<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/connectors/sources/ext/rest/rest.php');
class ext_rest_acton extends ext_rest
{
	protected $_enable_in_wizard = false;
	protected $_enable_in_hover = false;
    protected $_enable_in_admin_properties = true;
    protected $_enable_in_admin_mapping = false;
    protected $_enable_in_admin_search = false;
	protected $_has_testing_enabled = true;

    public static $allowedModuleList;

	public function __construct()
	{
        global $app_list_strings;
        $this->allowedModuleList = array('Contacts' => $app_list_strings['moduleList']['Contacts'],
                                         'Leads' => $app_list_strings['moduleList']['Leads'],
										 'Targets' => $app_list_strings['moduleList']['Prospects']);
		parent::__construct();
	}

    public function test()
	{
		$actonConfig = parent::getConfig();
		$actonConfig['sessionToken'] = "";
		$actonConfig['sessionTimeout'] = "";
		$sessionToken = $this->getSessionToken($actonConfig);
		if($sessionToken != null)
		{
			return true;
		}
		else
		{
			throw new Exception($actonConfig['lastErrorMessage']);
			return false;
		}
    }

    public function filterAllowedModules( $moduleList )
	{
		try
		{
			$outModuleList = array();
			foreach ( $moduleList as $module )
			{
				if (!in_array($module,$this->allowedModuleList))
				{
					continue;
				}
				else
				{
					$outModuleList[$module] = $module;
				}
			}
		}
		catch (Exception $e)
		{
			$GLOBALS['log']->error("Failure in filterAllowedModules: $e->getMessage()");
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
        return $outModuleList;
    }

    public function saveMappingHook($mapping)
	{
		try
		{
			$removeList = array();
			foreach ($this->allowedModuleList as $module_name => $display_name)
			{
				$GLOBALS['log']->debug("Adding $module_name to the removeList.");
				$removeList[$module_name] = $module_name;
			}
		}
		catch (Exception $e)
		{
			$GLOBALS['log']->error("Failure in saveMappingHook building removeList: $e->getMessage()");
		}
		try
		{
			if(is_array($mapping['beans']))
			{
				foreach($mapping['beans'] as $module => $ignore)
				{
					$GLOBALS['log']->debug("Removing $module from the removeList.");
					unset($removeList[$module]);
					$GLOBALS['log']->debug("Adding $module logic hook.");
					check_logic_hook_file($module, 'after_ui_frame', array(1, $module. ' Act-On Activity History', 'custom/modules/Connectors/connectors/sources/ext/rest/acton/ActOnActivityHistoryLogicHook.php', 'ActOnActivityHistoryLogicHook', 'showFrame') );
				}
			}
		}
		catch (Exception $e)
		{
			$GLOBALS['log']->error("Failure in saveMappingHook searching module list: $e->getMessage()");
		}
		try
		{
			foreach($removeList as $module)
			{
				remove_logic_hook($module, 'after_ui_frame', array(1, $module. ' Act-On Activity History', 'custom/modules/Connectors/connectors/sources/ext/rest/acton/ActOnActivityHistoryLogicHook.php', 'ActOnActivityHistoryLogicHook', 'showFrame') );
			}
		}
		catch (Exception $e)
		{
			$GLOBALS['log']->error("Failure in saveMappingHook cleanup: $e->getMessage()");
		}

        return parent::saveMappingHook($mapping);
    }
	/*
	 * getItem
	 *
	 * As the connector does not have a true API call, we simply
	 * override this abstract method
	 */
	public function getItem($args=array(), $module=null){}

    /*
	 * getList
	 *
	 * As the connector does not have a true API call, we simply
	 * override this abstract method

	 */
    public function getList($args = array(), $module = null) {}

	public function getSessionToken($actonConfig)
	{
		$sessionToken = $actonConfig['sessionToken'];
		$sessionTimeout = $actonConfig['sessionTimeout'];
		$currentTime = intval($this->millitime());

		if($sessionToken == "" || intval($sessionTimeout) < $currentTime)
		{

			$url = $actonConfig['properties']['url'] . '/acton/api/auth.jsp';
			$authArgs = array('authorizationToken' => urlencode($actonConfig['properties']['key']), 'cmd' => 'renew');
			$authArgsQuery = http_build_query($authArgs);
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $authArgsQuery);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			$GLOBALS['log']->debug("HTTP client call: $url -> $authArgs");
			$authResponse = curl_exec($curl);
			if($authResponse === false)
			{
				$curl_errno = curl_errno($curl);
				$curl_error = curl_error($curl);
				$GLOBALS['log']->error("HTTP client: cURL call failed: error $curl_errno: $curl_error");
				die("Request failed.\n");
			}
			$GLOBALS['log']->debug("HTTP client response: $authResponse");
			curl_close($curl);
			$authResult = json_decode($authResponse, true);
			if($authResult != NULL)
			{
				$status = $authResult['status'];
				if($status == "success")
				{
					$sessionToken = $authResult['sessionKey'];
					$timeLeft = $authResult['millisRemaining'];
					//Remove 5 seconds, just because we don't want to cut it too close
					$calcTimeLeft = intval($timeLeft) - 5000;
					$actonConfig['sessionToken'] = $sessionToken;
					$actonConfig['sessionTimeout'] = $currentTime + $calcTimeLeft;
					$actonConfig['lastErrorCode'] = "";
					$actonConfig['lastErrorMessage'] = "";
				}
				else if($status == "error")
				{
					$code =  $authResult['code'];
					if($code == "BAD_ADDRESS")
					{
						$actonConfig['lastErrorMessage'] = $authResult['message'] . ": " . $authResult['endpoint'];
					}
					else
					{
						$actonConfig['lastErrorMessage'] = $authResult['message'];
					}
					$actonConfig['lastErrorCode'] = $code;
					$actonConfig['sessionToken'] = "";
					$actonConfig['sessionTimeout'] = "";
				}
				$this->setConfig($actonConfig);
				$this->saveConfig();
			}
		}
		return $sessionToken;
	}

	function millitime()
	{
		$microtime = microtime();
		$comps = explode(' ', $microtime);

		// Note: Using a string here to prevent loss of precision
		// in case of "overflow" (PHP converts it to a double)
		return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
	}

    public function ext_actonSessionToken( $request )
	{
		$actonConfig = parent::getConfig();
        return $this->getSessionToken($actonConfig);
    }
}
?>