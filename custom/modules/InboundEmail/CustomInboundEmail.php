<?php

require_once("modules/InboundEmail/InboundEmail.php");
require_once("custom/include/dtbc/helpers.php");

class CustomInboundEmail extends InboundEmail {

    public function retrieveAllByGroupIdWithGroupAccounts($id, $includePersonal = true, $useCoreCode = false) {

		if ($useCoreCode)
			return parent::retrieveAllByGroupIdWithGroupAccounts($id, $includePersonal);

		global $current_user;
		
		$beans = ($includePersonal) ? $this->retrieveByGroupId($id) : array();

		$q = "SELECT inbound_email.id 
				FROM folders_subscriptions 
				JOIN folders ON folders.id = folders_subscriptions.folder_id AND folders.deleted = 0 
				JOIN inbound_email ON inbound_email.name = folders.name AND inbound_email.deleted = 0 
				WHERE folders_subscriptions.assigned_user_id = " . $this->db->quoted($current_user->id) . " AND 
				folders.parent_folder = '' AND 
				folders.folder_type = ''";

		$r = $this->db->query($q, true);

		while($a = $this->db->fetchByAssoc($r)) {
			$found = false;
			foreach($beans as $bean) {
				if($bean->id == $a['id']) {
					$found = true;
				}
			}

			if(!$found) {
				$ie = new InboundEmail();
				$ie->retrieve($a['id']);
				$beans[$a['id']] = $ie;
			}
		}

		return $beans;
        
    } // fn
	
	
}