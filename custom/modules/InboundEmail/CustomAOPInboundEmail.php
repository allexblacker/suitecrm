<?php

require_once("modules/InboundEmail/AOPInboundEmail.php");
require_once("custom/include/dtbc/helpers.php");
require_once("modules/AOW_WorkFlow/aow_utils.php");
require_once("custom/modules/AOW_Actions/actions/templateParser.php");
require_once('custom/modules/dtbc_Emails_Blacklist/Customdtbc_Emails_Blacklist.php');
require_once('custom/modules/dtbc_Case_Routing_Rules/CaseRoutingHandler.php');

class CustomAOPInboundEmail extends AOPInboundEmail {

    private $skipArray = array();

	// From the core:
    function handleCreateCase($email, $userId) {
        global $current_user, $mod_strings, $current_language;
        $mod_strings = return_module_language($current_language, "Emails");
        $GLOBALS['log']->debug('In handleCreateCase in CustomAOPInboundEmail');
        $c = new aCase();
        $this->getCaseIdFromCaseNumber($email->name, $c);

        if (!$this->handleCaseAssignment($email) && $this->isMailBoxTypeCreateCase()) {
            // create a case
            $GLOBALS['log']->debug('retrieveing email');
            $email->retrieve($email->id);
            $c = new aCase();

            $notes = $email->get_linked_beans('notes','Notes');
            $noteIds = array();
            foreach($notes as $note){
                $noteIds[] = $note->id;
            }
            if($email->description_html) {
                $c->description = $this->processImageLinks(SugarCleaner::cleanHtml($email->description_html),$noteIds);
            }else{
                $c->description = $email->description;
            }
			
			$c->description = DtbcHelpers::trimString($c->description);

            $c->assigned_user_id = $userId;
            $c->name = $email->name;
            $c->status = 'New';
            $c->priority = 'P1';

			if ($this->isChatEmail($email->name)) {
				$contactAddr = $this->getContactEmailFromBody($email->description);
				$c->chat_email_c = $contactAddr;
			} else if(!empty($email->reply_to_email)) {
                $contactAddr = $email->reply_to_email;
            } else {
                $contactAddr = $email->from_addr;
            }

            $GLOBALS['log']->debug('finding related accounts with address ' . $contactAddr);
            if($accountIds = $this->getRelatedId($contactAddr, 'accounts')) {
                if (sizeof($accountIds) == 1) {
                    $c->account_id = $accountIds[0];

                    $acct = new Account();
                    $acct->retrieve($c->account_id);
                    $c->account_name = $acct->name;
                } // if
            } // if
            $contactIds = $this->getRelatedId($contactAddr, 'contacts');
            if(!empty($contactIds)) {
                $c->contact_created_by_id = $contactIds[0];
            }

			// Handle custom fields
			$c->is_email_to_case = 1;
			$c->origin_email_address_c = $email->to_addrs;
			$_REQUEST['emailToCaseNew'] = 1;
			$caseRoutingHandler = new CaseRoutingHandler();
			$caseRoutingHandler->setRouting($c);
            $c->save(true);

            $caseId = $c->id;
            $c = new aCase();
            $c->retrieve($caseId);
            if($c->load_relationship('emails')) {
                $c->emails->add($email->id);
            } // if
                if(!empty($contactIds) && $c->load_relationship('contacts')) {
                    if (!$accountIds && count($contactIds) == 1) {
                        $contact = BeanFactory::getBean('Contacts', $contactIds[0]);
                        if ($contact->load_relationship('accounts')) {
                            $acct = $contact->accounts->get();
                            if ($c->load_relationship('accounts') && !empty($acct[0])) {
                                $c->accounts->add($acct[0]);
                            }
                        }
                    }
                    $c->contacts->add($contactIds);
                } // if

            foreach($notes as $note){
                //Link notes to case also
                $newNote = BeanFactory::newBean('Notes');
                $newNote->name = $note->name;
                $newNote->file_mime_type = $note->file_mime_type;
                $newNote->filename = $note->filename;
                $newNote->parent_type = 'Cases';
                $newNote->parent_id = $c->id;
                $newNote->save();
                $srcFile = "upload://{$note->id}";
                $destFile = "upload://{$newNote->id}";
                copy($srcFile,$destFile);

            }

            $c->email_id = $email->id;
            $email->parent_type = "Cases";
            $email->parent_id = $caseId;
            // assign the email to the case owner
            $email->assigned_user_id = $c->assigned_user_id;
            $email->name = str_replace('%1', $c->case_number, $c->getEmailSubjectMacro()) . " ". $email->name;
            
			$email->save();
            $GLOBALS['log']->debug('InboundEmail created one case with number: '.$c->case_number);
            $createCaseTemplateId = $this->get_stored_options('create_case_email_template', "");
            if(!empty($this->stored_options)) {
                $storedOptions = unserialize(base64_decode($this->stored_options));
            }
            if(!empty($createCaseTemplateId)) {
                $fromName = "";
                $fromAddress = "";
                if (!empty($this->stored_options)) {
                    $fromAddress = $storedOptions['from_addr'];
                    $fromName = from_html($storedOptions['from_name']);
                    $replyToName = (!empty($storedOptions['reply_to_name']))? from_html($storedOptions['reply_to_name']) :$fromName ;
                    $replyToAddr = (!empty($storedOptions['reply_to_addr'])) ? $storedOptions['reply_to_addr'] : $fromAddress;
                } // if
                $defaults = $current_user->getPreferredEmail();
                $fromAddress = (!empty($fromAddress)) ? $fromAddress : $defaults['email'];
                $fromName = (!empty($fromName)) ? $fromName : $defaults['name'];
                $to[0]['email'] = $contactAddr;

                // handle to name: address, prefer reply-to
                if(!empty($email->reply_to_name)) {
                    $to[0]['display'] = $email->reply_to_name;
                } elseif(!empty($email->from_name)) {
                    $to[0]['display'] = $email->from_name;
                }

                $et = new EmailTemplate();
                $et->retrieve($createCaseTemplateId);
                if(empty($et->subject))		{ $et->subject = ''; }
                if(empty($et->body))		{ $et->body = ''; }
                if(empty($et->body_html))	{ $et->body_html = ''; }

                $et->subject = "Re:" . " " . str_replace('%1', $c->case_number, $c->getEmailSubjectMacro() . " ". $c->name);

                $email->email2init();
                $email->from_addr = $email->from_addr_name;
                $email->to_addrs = $email->to_addrs_names;
                $email->cc_addrs = $email->cc_addrs_names;
                $email->bcc_addrs = $email->bcc_addrs_names;
                $email->from_name = $email->from_addr;

                $email = $email->et->handleReplyType($email, "reply");
                $ret = $email->et->displayComposeEmail($email);

                $ret['description'] = empty($email->description_html) ?  str_replace("\n", "\n<BR/>", DtbcHelpers::trimString($email->description)) : DtbcHelpers::trimString($email->description_html);

                $reply = new Email();
                $reply->type				= 'out';
                $reply->to_addrs			= $to[0]['email'];
                $reply->to_addrs_arr		= $to;
                $reply->cc_addrs_arr		= array();
                $reply->bcc_addrs_arr		= array();
                $reply->from_name			= $fromName;
                $reply->from_addr			= $fromAddress;
                $reply->reply_to_name		= $replyToName;
                $reply->reply_to_addr		= $replyToAddr;
                $reply->name				= $et->subject;
                $reply->description			= $et->body . "<div><hr /></div>" .  DtbcHelpers::trimString($email->description, 65434);
                if (!$et->text_only) {
                    $reply->description_html = $et->body_html .  "<div><hr /></div>" . DtbcHelpers::trimString($email->description, 65434);
                }
				// These few lines are my customizations in the code:
				$object_arr = DtbcHelpers::getRelatedBeansForEmails($email->parent_type, $email->parent_id);
				$reply->name = CustomAowTemplateParser::parse_template($reply->name, $object_arr);
				$reply->description = CustomAowTemplateParser::parse_template($reply->description, $object_arr);
				$reply->description_html = CustomAowTemplateParser::parse_template($reply->description_html, $object_arr);

                $GLOBALS['log']->debug('saving and sending auto-reply email');
                //$reply->save(); // don't save the actual email.
                $reply->send();
            } // if

        } else {
            echo "First if not matching\n";
            if(!empty($email->reply_to_email)) {
                $contactAddr = $email->reply_to_email;
            } else {
                $contactAddr = $email->from_addr;
            }
            $this->handleAutoresponse($email, $contactAddr);
        }
        echo "End of handle create case (custom)\n";

    } // fn
	
	// From core
	public function saveAttachments($msgNo, $parts, $emailId, $breadcrumb='0', $forDisplay) {
		global $sugar_config;
		/*
			Primary body types for a part of a mail structure (imap_fetchstructure returned object)
			0 => text
			1 => multipart
			2 => message
			3 => application
			4 => audio
			5 => image
			6 => video
			7 => other
		*/

        $imsgNo = $msgNo;
        $iparts = $parts;
        $iemailId = $emailId;
        $ibreadcrumb = $breadcrumb;
        $iforDisplay = $forDisplay;

		foreach($parts as $k => $part) {
			$thisBc = $k+1;
			if($breadcrumb != '0') {
				$thisBc = $breadcrumb.'.'.$thisBc;
			}
			$attach = null;
			// check if we need to recurse into the object
			//if($part->type == 1 && !empty($part->parts)) {
			if(isset($part->parts) && !empty($part->parts) && !( isset($part->subtype) && strtolower($part->subtype) == 'rfc822')  ) {
				$this->saveAttachments($msgNo, $part->parts, $emailId, $thisBc, $forDisplay);
                continue;
			} elseif($part->ifdisposition) {
				// we will take either 'attachments' or 'inline'
				if(strtolower($part->disposition) == 'attachment' || ((strtolower($part->disposition) == 'inline') && $part->type != 0)) {
					$attach = $this->getNoteBeanForAttachment($emailId);
					$fname = $this->handleEncodedFilename($this->retrieveAttachmentNameFromStructure($part));

					if(!empty($fname)) {//assign name to attachment
						$attach->name = $fname;
					} else {//if name is empty, default to filename
						$attach->name = urlencode($this->retrieveAttachmentNameFromStructure($part));
					}
					$attach->filename = $attach->name;
					if (empty($attach->filename)) {
						continue;
					}

					// deal with the MIME types email has
					$attach->file_mime_type = $this->getMimeType($part->type, $part->subtype);
					$attach->safeAttachmentName();
					if($forDisplay) {
						$attach->id = $this->getTempFilename();
					} else {
						// only save if doing a full import, else we want only the binaries
						$attach->name = $this->checkFieldLength($attach->name, 255);
						$attach->save();
					}
                    $this->skipArray[] = $thisBc;
				} // end if disposition type 'attachment'
			}// end ifdisposition
			//Retrieve contents of subtype rfc8822
			elseif ($part->type == 2 && isset($part->subtype) && strtolower($part->subtype) == 'rfc822' )
			{
			    $tmp_eml =  imap_fetchbody($this->conn, $msgNo, $thisBc);
			    $attach = $this->getNoteBeanForAttachment($emailId);
			    $attach->file_mime_type = 'messsage/rfc822';
			    $attach->description = $tmp_eml;
			    $attach->filename = 'bounce.eml';
			    $attach->safeAttachmentName();
			    if($forDisplay) {
			        $attach->id = $this->getTempFilename();
			    } else {
			        // only save if doing a full import, else we want only the binaries
					$attach->name = $this->checkFieldLength($attach->name, 255);
			        $attach->save();
			    }
                $this->skipArray[] = $thisBc;
			} elseif(!$part->ifdisposition && $part->type != 1 && $part->type != 2 && $thisBc != '1') {
        		// No disposition here, but some IMAP servers lie about disposition headers, try to find the truth
				// Also Outlook puts inline attachments as type 5 (image) without a disposition
				if($part->ifparameters) {

                    // Check normal file name related content
                    foreach($part->parameters as $param) {
                        if(strtolower($param->attribute) == "name" || strtolower($param->attribute) == "filename") {
                            $fname = $this->handleEncodedFilename($param->value);
                            break;
                        }
                    }

                    if(empty($fname)) continue;

					// we assume that named parts are attachments too
                    $attach = $this->getNoteBeanForAttachment($emailId);

					$attach->filename = $attach->name = $fname;
					$attach->file_mime_type = $this->getMimeType($part->type, $part->subtype);

					$attach->safeAttachmentName();
					if($forDisplay) {
						$attach->id = $this->getTempFilename();
					} else {
						// only save if doing a full import, else we want only the binaries
						$attach->name = $this->checkFieldLength($attach->name, 255);
						$attach->save();
					}
                    $this->skipArray[] = $thisBc;
				}
			}
			$this->saveAttachmentBinaries($attach, $msgNo, $thisBc, $part, $forDisplay);
		} // end foreach

	}
	
	// From the core!
	function importOneEmail($msgNo, $uid, $forDisplay=false, $clean_email=true) {
		$GLOBALS['log']->debug("InboundEmail processing 1 email {$msgNo}-----------------------------------------------------------------------------------------");
		global $timedate;
		global $app_strings;
		global $app_list_strings;
		global $sugar_config;
		global $current_user;

        // Bug # 45477
        // So, on older versions of PHP (PHP VERSION < 5.3),
        // calling imap_headerinfo and imap_fetchheader can cause a buffer overflow for exteremly large headers,
        // This leads to the remaining messages not being read because Sugar crashes everytime it tries to read the headers.
        // The workaround is to mark a message as read before making trying to read the header of the msg in question
        // This forces this message not be read again, and we can continue processing remaining msgs.

        // UNCOMMENT THIS IF YOU HAVE THIS PROBLEM!  See notes on Bug # 45477
        // $this->markEmails($uid, "read");

		$header = imap_headerinfo($this->conn, $msgNo);
		
		// Custom code begin
		$blObj = new Customdtbc_Emails_Blacklist();
		$blRes = $blObj->runBlacklistLogic($header->subject, $header->from);
		if ($blRes) {
			// Mark read to skip and return without any action
			$this->markEmails($uid, "read");
			return;
		}
		// Custom code end
		
		$fullHeader = imap_fetchheader($this->conn, $msgNo); // raw headers

		// reset inline images cache
		$this->inlineImages = array();

		// handle messages deleted on server
		if(empty($header)) {
			if(!isset($this->email) || empty($this->email)) {
				$this->email = new Email();
			}

			$q = "";
            $queryUID = $this->db->quote($uid);
			if ($this->isPop3Protocol()) {
				$this->email->name = $app_strings['LBL_EMAIL_ERROR_MESSAGE_DELETED'];
				$q = "DELETE FROM email_cache WHERE message_id = '{$queryUID}' AND ie_id = '{$this->id}' AND mbox = '{$this->mailbox}'";
			} else {
				$this->email->name = $app_strings['LBL_EMAIL_ERROR_IMAP_MESSAGE_DELETED'];
				$q = "DELETE FROM email_cache WHERE imap_uid = '{$queryUID}' AND ie_id = '{$this->id}' AND mbox = '{$this->mailbox}'";
			} // else
			// delete local cache
			$r = $this->db->query($q);

			$this->email->date_sent = $timedate->nowDb();
			return false;
			//return "Message deleted from server.";
		}

		///////////////////////////////////////////////////////////////////////
		////	DUPLICATE CHECK
		$dupeCheckResult = $this->importDupeCheck($header->message_id, $header, $fullHeader);
		if($forDisplay || $dupeCheckResult) {
			$GLOBALS['log']->debug('*********** NO duplicate found, continuing with processing.');

			$structure = imap_fetchstructure($this->conn, $msgNo); // map of email

			///////////////////////////////////////////////////////////////////
			////	CREATE SEED EMAIL OBJECT
			$email = new Email();
			$email->isDuplicate = ($dupeCheckResult) ? false : true;
			$email->mailbox_id = $this->id;
			$message = array();
			$email->id = create_guid();
			$email->new_with_id = true; //forcing a GUID here to prevent double saves.
			////	END CREATE SEED EMAIL
			///////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////
			////	PREP SYSTEM USER
			if(empty($current_user)) {
				// I-E runs as admin, get admin prefs

				$current_user = new User();
				$current_user->getSystemUser();
			}
			$tPref = $current_user->getUserDateTimePreferences();
			////	END USER PREP
			///////////////////////////////////////////////////////////////////
            if(!empty($header->date)) {
			    $unixHeaderDate = $timedate->fromString($header->date);
            }
			///////////////////////////////////////////////////////////////////
			////	HANDLE EMAIL ATTACHEMENTS OR HTML TEXT
			////	Inline images require that I-E handle attachments before body text
			// parts defines attachments - be mindful of .html being interpreted as an attachment
			if($structure->type == 1 && !empty($structure->parts)) {
				$GLOBALS['log']->debug('InboundEmail found multipart email - saving attachments if found.');
                $this->skipArray = array();
				$this->saveAttachments($msgNo, $structure->parts, $email->id, 0, $forDisplay);
                $this->handleMsgFiles($msgNo, $structure->parts, $email->id, 0, $forDisplay);
			} elseif($structure->type == 0) {
				$uuemail = ($this->isUuencode($email->description)) ? true : false;
				/*
				 * UUEncoded attachments - legacy, but still have to deal with it
				 * format:
				 * begin 777 filename.txt
				 * UUENCODE
				 *
				 * end
				 */
				// set body to the filtered one
				if($uuemail) {
					$email->description = $this->handleUUEncodedEmailBody($email->description, $email->id);
					$email->retrieve($email->id);
			   		$email->save();
		   		}
			} else {
				if($this->port != 110) {
					$GLOBALS['log']->debug('InboundEmail found a multi-part email (id:'.$msgNo.') with no child parts to parse.');
				}
			}
			////	END HANDLE EMAIL ATTACHEMENTS OR HTML TEXT
			///////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////
			////	ASSIGN APPROPRIATE ATTRIBUTES TO NEW EMAIL OBJECT
			// handle UTF-8/charset encoding in the ***headers***
			global $db;
			$email->name			= $this->handleMimeHeaderDecode($header->subject);
			$email->type = 'inbound';
			if(!empty($unixHeaderDate)) {
			    $email->date_sent = $timedate->asUser($unixHeaderDate);
			    list($email->date_start, $email->time_start) = $timedate->split_date_time($email->date_sent);
			} else {
			    $email->date_start = $email->time_start = $email->date_sent = "";
			}
			$email->status = 'unread'; // this is used in Contacts' Emails SubPanel
			if(!empty($header->toaddress)) {
				$email->to_name	 = $this->handleMimeHeaderDecode($header->toaddress);
				$email->to_addrs_names = $email->to_name;
			}
			if(!empty($header->to)) {
				$email->to_addrs	= $this->convertImapToSugarEmailAddress($header->to);
			}
			$email->from_name		= $this->handleMimeHeaderDecode($header->fromaddress);
			$email->from_addr_name = $email->from_name;
			$email->from_addr		= $this->convertImapToSugarEmailAddress($header->from);
			if(!empty($header->cc)) {
				$email->cc_addrs	= $this->convertImapToSugarEmailAddress($header->cc);
			}
			if(!empty($header->ccaddress)) {
				$email->cc_addrs_names	 = $this->handleMimeHeaderDecode($header->ccaddress);
			} // if
			$email->reply_to_name   = $this->handleMimeHeaderDecode($header->reply_toaddress);
			$email->reply_to_email  = $this->convertImapToSugarEmailAddress($header->reply_to);
			if (!empty($email->reply_to_email)) {
				$email->reply_to_addr   = $email->reply_to_name;
			}
			$email->intent			= $this->mailbox_type;

			$email->message_id		= $this->compoundMessageId; // filled by importDupeCheck();

			$oldPrefix = $this->imagePrefix;
			if(!$forDisplay) {
				// Store CIDs in imported messages, convert on display
				$this->imagePrefix = "cid:";
			}
			// handle multi-part email bodies
			$email->description_html= $this->getMessageText($msgNo, 'HTML', $structure, $fullHeader,$clean_email); // runs through handleTranserEncoding() already
			$email->description	= $this->getMessageText($msgNo, 'PLAIN', $structure, $fullHeader,$clean_email); // runs through handleTranserEncoding() already
			$this->imagePrefix = $oldPrefix;

			// empty() check for body content
			if(empty($email->description)) {
				$GLOBALS['log']->debug('InboundEmail Message (id:'.$email->message_id.') has no body');
			}

			// assign_to group
			if (!empty($_REQUEST['user_id'])) {
				$email->assigned_user_id = $_REQUEST['user_id'];
			} else {
				// Samir Gandhi : Commented out this code as its not needed
				//$email->assigned_user_id = $this->group_id;
			}

	        //Assign Parent Values if set
	        if (!empty($_REQUEST['parent_id']) && !empty($_REQUEST['parent_type'])) {
                $email->parent_id = $_REQUEST['parent_id'];
                $email->parent_type = $_REQUEST['parent_type'];

                $mod = strtolower($email->parent_type);
                $rel = array_key_exists($mod, $email->field_defs) ? $mod : $mod . "_activities_emails"; //Custom modules rel name

                if(! $email->load_relationship($rel) )
                    return FALSE;
                $email->$rel->add($email->parent_id);
	        }

			// override $forDisplay w/user pref
			if($forDisplay) {
				if($this->isAutoImport()) {
					$forDisplay = false; // triggers save of imported email
				}
			}

			if(!$forDisplay) {
				$email->save();

				$email->new_with_id = false; // to allow future saves by UPDATE, instead of INSERT
				////	ASSIGN APPROPRIATE ATTRIBUTES TO NEW EMAIL OBJECT
				///////////////////////////////////////////////////////////////////

				///////////////////////////////////////////////////////////////////
				////	LINK APPROPRIATE BEANS TO NEWLY SAVED EMAIL
				//$contactAddr = $this->handleLinking($email);
				////	END LINK APPROPRIATE BEANS TO NEWLY SAVED EMAIL
				///////////////////////////////////////////////////////////////////

				///////////////////////////////////////////////////////////////////
				////	MAILBOX TYPE HANDLING
				$this->handleMailboxType($email, $header);
				////	END MAILBOX TYPE HANDLING
				///////////////////////////////////////////////////////////////////

				///////////////////////////////////////////////////////////////////
				////	SEND AUTORESPONSE
				if(!empty($email->reply_to_email)) {
					$contactAddr = $email->reply_to_email;
				} else {
					$contactAddr = $email->from_addr;
				}
				if (!$this->isMailBoxTypeCreateCase()) {
					$this->handleAutoresponse($email, $contactAddr);
				}
				////	END SEND AUTORESPONSE
				///////////////////////////////////////////////////////////////////
				////	END IMPORT ONE EMAIL
				///////////////////////////////////////////////////////////////////
			}
		} else {
			// only log if not POP3; pop3 iterates through ALL mail
			if($this->protocol != 'pop3') {
				$GLOBALS['log']->info("InboundEmail found a duplicate email: ".$header->message_id);
				//echo "This email has already been imported";
			}
			return false;
		}
		////	END DUPLICATE CHECK
		///////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////
		////	DEAL WITH THE MAILBOX
		if(!$forDisplay) {
			$r = imap_setflag_full($this->conn, $msgNo, '\\SEEN');

			// if delete_seen, mark msg as deleted
			if($this->delete_seen == 1  && !$forDisplay) {
				$GLOBALS['log']->info("INBOUNDEMAIL: delete_seen == 1 - deleting email");
				imap_setflag_full($this->conn, $msgNo, '\\DELETED');
			}
		} else {
			// for display - don't touch server files?
			//imap_setflag_full($this->conn, $msgNo, '\\UNSEEN');
		}

		$GLOBALS['log']->debug('********************************* InboundEmail finished import of 1 email: '.$email->name);
		////	END DEAL WITH THE MAILBOX
		///////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////
		////	TO SUPPORT EMAIL 2.0
		$this->email = $email;

		if(empty($this->email->et)) {
			$this->email->email2init();
		}

		return true;
	}
	
	private function checkFieldLength($value, $maxLength) {
		if (strlen($value) > $maxLength)
			return substr($value, 0, 250) . "...";
		
		return $value;
	}
	
	private function isChatEmail($subjectInput) {
		$subject = strtolower(trim($subjectInput));
		return strpos($subject, "transcript") !== false || strpos($subject, "offline message") !== false;
	}
	
    private function getContactEmailFromBody($emailBody) {
        $body = strtolower(trim($emailBody));
        $searchText = "visitor details";
		$posVisitorDetails = strpos($body, $searchText);
        $contactEmail = $this->getStringBetween($emailBody, 'mailto:', '&gt;', $posVisitorDetails);

        return filter_var($contactEmail, FILTER_VALIDATE_EMAIL) ? $contactEmail : "";
    }

    private function getStringBetween($string, $start, $end, $offset){
        $string = ' ' . $string;
        $ini = strpos($string, $start, $offset);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    private function handleMsgFiles($msgNo, $parts, $emailId, $breadcrumb = '0', $forDisplay)
    {
        foreach($parts as $k => $part) {
            $thisBc = $k+1;
            if($breadcrumb != '0') {
                $thisBc = $breadcrumb.'.'.$thisBc;
            }
            $attach = null;

            if (!empty($this->skipArray) && in_array($thisBc, $this->skipArray))
                continue;

            if ($part->lines > 0 && $part->bytes > 0 && $part->type != "PLAIN") {
                $fname = "";
                foreach($part->parameters as $param) {
                    if(strtolower($param->attribute) == "name" || strtolower($param->attribute) == "filename") {
                        $fname = $this->handleEncodedFilename($param->value);
                        break;
                    }
                }

                // MSG object details:
                // ifsubtype = 1
                // subtype = RFC822
                // id = <516D83C6D41FFC44BB664FA6142FB745@eurprd05.prod.outlook.com>

                if (empty($fname))
                    $fname = "file" . $thisBc . ".mht";

                $data = imap_fetchbody($this->conn, $msgNo, $thisBc);
                $message = $this->handleTranserEncoding($data, $part->encoding);

                // Save file
                $attach = $this->getNoteBeanForAttachment($emailId);

                $attach->description = "";

                $attach->filename = $attach->name = $fname;
                $attach->file_mime_type = "text/html";

                $attach->safeAttachmentName();

                $attach->save();

                file_put_contents("upload/" . $attach->id, $message,LOCK_EX);
            } else if (isset($part->parts)) {
                $this->handleMsgFiles($msgNo, $part->parts, $emailId, $thisBc, $forDisplay);
            }
        } // end foreach
    }
}