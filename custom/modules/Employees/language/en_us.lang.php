<?php
// created: 2017-12-03 14:20:08
$mod_strings = array (
  'LBL_TIER3' => 'Tier 3',
  'LBL_DIVISION' => 'Division',
  'LBL_SALESFORCE_ID' => 'SalesForce ID',
  'LBL_REGION' => 'User Region',
  'LBL_ROLE' => 'Role',
  'LBL_ADDRESS_COUNTRY' => 'Country',
  'LBL_SUITE_ID' => 'suite id',
);