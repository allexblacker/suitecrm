<?php 
 //WARNING: The contents of this file are auto-generated

 
 // created: 2018-01-17 22:24:21
$mod_strings['LBL_AOW_WORKFLOW_ID'] = 'Workflow id';
$mod_strings['LBL_BCC_ADDRESS'] = 'BCC Address';
$mod_strings['LBL_CC_ADDRESS'] = 'CC Address';
$mod_strings['LBL_EMAIL_CONTENT'] = 'Email&#039;s Content';
$mod_strings['LBL_EMAIL_CONTENT_HTML'] = 'Email&#039;s HTML Content';
$mod_strings['LBL_EMAIL_SUBJECT'] = 'Email&#039;s Subject';
$mod_strings['LBL_EMAIL_TEMPLATE_ID'] = 'Email Template ID';
$mod_strings['LBL_FAILER_COUNTER'] = 'Failer Counter';
$mod_strings['LBL_FROM_ADDRESS'] = 'From Address';
$mod_strings['LBL_FROM_NAME'] = 'From Name';
$mod_strings['LBL_NEXT_DELIVERY'] = 'Next Delivery';
$mod_strings['LBL_RELATED_BEAN_ID'] = 'Related Bean ID';
$mod_strings['LBL_RELATED_BEAN_TYPE'] = 'Related Bean Type';
$mod_strings['LBL_SMTP_CONFIG_KEY'] = 'SMTP Config Key';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_TO_ADDRESS'] = 'To Address';
$mod_strings['LBL_EDITVIEW_PANEL1'] = 'Record Info';
$mod_strings['LBL_EDITVIEW_PANEL2'] = 'Email Info';
$mod_strings['LBL_EDITVIEW_PANEL3'] = 'Technical Info';



?>