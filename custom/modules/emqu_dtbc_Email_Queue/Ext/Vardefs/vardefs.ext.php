<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['next_delivery_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['next_delivery_c']['labelValue']='Next Delivery';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['from_name_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['from_name_c']['labelValue']='From Name';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_template_id_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_template_id_c']['labelValue']='Email Template ID';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['from_address_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['from_address_c']['labelValue']='From Address';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_content_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_content_c']['labelValue']='Email\'s Content';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['smtp_config_key_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['smtp_config_key_c']['labelValue']='SMTP Config Key';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['bcc_address_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['bcc_address_c']['labelValue']='BCC Address';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_content_html_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_content_html_c']['labelValue']='Email\'s HTML Content';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['failer_counter_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['failer_counter_c']['labelValue']='Failer Counter';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['aow_workflow_id_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['aow_workflow_id_c']['labelValue']='Workflow id';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['status_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['status_c']['labelValue']='Status';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['cc_address_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['cc_address_c']['labelValue']='CC Address';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_subject_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['email_subject_c']['labelValue']='Email\'s Subject';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['to_address_c']['inline_edit']='1';
$dictionary['emqu_dtbc_Email_Queue']['fields']['to_address_c']['labelValue']='To Address';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['related_bean_type_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['related_bean_type_c']['labelValue']='Related Bean Type';

 

 // created: 2018-01-17 22:27:25
$dictionary['emqu_dtbc_Email_Queue']['fields']['related_bean_id_c']['inline_edit']='';
$dictionary['emqu_dtbc_Email_Queue']['fields']['related_bean_id_c']['labelValue']='Related Bean ID';

 
?>