<?php

require_once('include/MVC/View/views/view.detail.php');

class emqu_dtbc_Email_QueueViewDetail extends ViewDetail {
    
	public function preDisplay() {
		parent::preDisplay();
		$this->bean->email_content_html_c = DtbcHelpers::getCustomHtmlForDisplay($this->bean->email_content_html_c);
    }
	
	function display() {
		parent::display();
		echo getVersionedScript("custom/include/dtbc/js/fixHtmlLinks.js");
	}
}
