<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$module_name = 'emqu_dtbc_Email_Queue';
$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
    ),

    'where' => '',

    'list_fields' => array(
        'name' => array(
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '45%',
        ),
		'to_address_c' => array(
            'vname' => 'LBL_TO_ADDRESS',
            'width' => '45%',
        ),
		'email_subject_c' => array(
            'vname' => 'LBL_EMAIL_SUBJECT',
            'width' => '25%',
        ),
		'date_modified' => array(
            'vname' => 'LBL_DATE_MODIFIED',
            'width' => '45%',
        ),
    ),
);
