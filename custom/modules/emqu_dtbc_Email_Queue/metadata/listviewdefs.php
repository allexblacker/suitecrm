<?php
$module_name = 'emqu_dtbc_Email_Queue';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'FROM_ADDRESS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_FROM_ADDRESS',
    'width' => '10%',
  ),
  'TO_ADDRESS_C' => 
  array (
    'type' => 'text',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TO_ADDRESS',
    'sortable' => false,
    'width' => '10%',
  ),
  'EMAIL_SUBJECT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_EMAIL_SUBJECT',
    'width' => '10%',
  ),
  'RELATED_BEAN_TYPE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_RELATED_BEAN_TYPE',
    'width' => '10%',
  ),
  'STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'AOW_WORKFLOW_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_AOW_WORKFLOW_ID',
    'width' => '10%',
  ),
  'BCC_ADDRESS_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_BCC_ADDRESS',
    'sortable' => false,
    'width' => '10%',
  ),
  'CC_ADDRESS_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CC_ADDRESS',
    'sortable' => false,
    'width' => '10%',
  ),
  'FROM_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_FROM_NAME',
    'width' => '10%',
  ),
  'RELATED_BEAN_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_RELATED_BEAN_ID',
    'width' => '10%',
  ),
  'SMTP_CONFIG_KEY_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_SMTP_CONFIG_KEY',
    'width' => '10%',
  ),
  'EMAIL_TEMPLATE_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_EMAIL_TEMPLATE_ID',
    'width' => '10%',
  ),
  'NEXT_DELIVERY_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_NEXT_DELIVERY',
    'width' => '10%',
  ),
  'FAILER_COUNTER_C' => 
  array (
    'type' => 'int',
    'default' => false,
    'label' => 'LBL_FAILER_COUNTER',
    'width' => '10%',
  ),
);
?>
