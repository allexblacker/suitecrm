<?php
$module_name = 'CI1_Competitive_Intelligence';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'subject',
            'label' => 'Name',
          ),
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'notes',
            'label' => 'Notes',
          ),
          1 => 
          array (
            'name' => 'article',
            'studio' => 'visible',
            'label' => 'Article',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'source',
            'label' => 'Source',
          ),
          1 => 
          array (
            'name' => 'topic',
            'studio' => 'visible',
            'label' => 'Topic',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'accounts_ci1_competitive_intelligence_1_name',
            'label' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_ACCOUNTS_TITLE',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
