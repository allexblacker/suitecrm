<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-09-26 11:42:36
$dictionary["Document"]["fields"]["p1_project_documents_1"] = array (
  'name' => 'p1_project_documents_1',
  'type' => 'link',
  'relationship' => 'p1_project_documents_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'vname' => 'LBL_P1_PROJECT_DOCUMENTS_1_FROM_P1_PROJECT_TITLE',
  'id_name' => 'p1_project_documents_1p1_project_ida',
);
$dictionary["Document"]["fields"]["p1_project_documents_1_name"] = array (
  'name' => 'p1_project_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_P1_PROJECT_DOCUMENTS_1_FROM_P1_PROJECT_TITLE',
  'save' => true,
  'id_name' => 'p1_project_documents_1p1_project_ida',
  'link' => 'p1_project_documents_1',
  'table' => 'p1_project',
  'module' => 'P1_Project',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["p1_project_documents_1p1_project_ida"] = array (
  'name' => 'p1_project_documents_1p1_project_ida',
  'type' => 'link',
  'relationship' => 'p1_project_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2017-11-15 18:23:49
$dictionary["Document"]["fields"]["leads_documents_1"] = array (
  'name' => 'leads_documents_1',
  'type' => 'link',
  'relationship' => 'leads_documents_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_documents_1leads_ida',
);
$dictionary["Document"]["fields"]["leads_documents_1_name"] = array (
  'name' => 'leads_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_documents_1leads_ida',
  'link' => 'leads_documents_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["Document"]["fields"]["leads_documents_1leads_ida"] = array (
  'name' => 'leads_documents_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2018-03-12 10:44:31
$dictionary["Document"]["fields"]["spa_spa_documents_1"] = array (
  'name' => 'spa_spa_documents_1',
  'type' => 'link',
  'relationship' => 'spa_spa_documents_1',
  'source' => 'non-db',
  'module' => 'SPA_SPA',
  'bean_name' => 'SPA_SPA',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_SPA_SPA_TITLE',
  'id_name' => 'spa_spa_documents_1spa_spa_ida',
);
$dictionary["Document"]["fields"]["spa_spa_documents_1_name"] = array (
  'name' => 'spa_spa_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_SPA_SPA_TITLE',
  'save' => true,
  'id_name' => 'spa_spa_documents_1spa_spa_ida',
  'link' => 'spa_spa_documents_1',
  'table' => 'spa_spa',
  'module' => 'SPA_SPA',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["spa_spa_documents_1spa_spa_ida"] = array (
  'name' => 'spa_spa_documents_1spa_spa_ida',
  'type' => 'link',
  'relationship' => 'spa_spa_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPA_SPA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2017-10-27 12:36:39
$dictionary["Document"]["fields"]["aos_quotes_documents_1"] = array (
  'name' => 'aos_quotes_documents_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_documents_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_AOS_QUOTES_DOCUMENTS_1_FROM_AOS_QUOTES_TITLE',
  'id_name' => 'aos_quotes_documents_1aos_quotes_ida',
);
$dictionary["Document"]["fields"]["aos_quotes_documents_1_name"] = array (
  'name' => 'aos_quotes_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_QUOTES_DOCUMENTS_1_FROM_AOS_QUOTES_TITLE',
  'save' => true,
  'id_name' => 'aos_quotes_documents_1aos_quotes_ida',
  'link' => 'aos_quotes_documents_1',
  'table' => 'aos_quotes',
  'module' => 'AOS_Quotes',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["aos_quotes_documents_1aos_quotes_ida"] = array (
  'name' => 'aos_quotes_documents_1aos_quotes_ida',
  'type' => 'link',
  'relationship' => 'aos_quotes_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2018-03-12 10:45:56
$dictionary["Document"]["fields"]["spadi_spa_distributor_documents_1"] = array (
  'name' => 'spadi_spa_distributor_documents_1',
  'type' => 'link',
  'relationship' => 'spadi_spa_distributor_documents_1',
  'source' => 'non-db',
  'module' => 'SPADI_SPA_Distributor',
  'bean_name' => 'SPADI_SPA_Distributor',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
  'id_name' => 'spadi_spa_distributor_documents_1spadi_spa_distributor_ida',
);
$dictionary["Document"]["fields"]["spadi_spa_distributor_documents_1_name"] = array (
  'name' => 'spadi_spa_distributor_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE',
  'save' => true,
  'id_name' => 'spadi_spa_distributor_documents_1spadi_spa_distributor_ida',
  'link' => 'spadi_spa_distributor_documents_1',
  'table' => 'spadi_spa_distributor',
  'module' => 'SPADI_SPA_Distributor',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["spadi_spa_distributor_documents_1spadi_spa_distributor_ida"] = array (
  'name' => 'spadi_spa_distributor_documents_1spadi_spa_distributor_ida',
  'type' => 'link',
  'relationship' => 'spadi_spa_distributor_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPADI_SPA_DISTRIBUTOR_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


 // created: 2017-11-01 16:27:36
$dictionary['Document']['fields']['document_status_c']['inline_edit']='1';
$dictionary['Document']['fields']['document_status_c']['labelValue']='Status';

 
?>