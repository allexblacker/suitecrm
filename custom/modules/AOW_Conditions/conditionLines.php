<?php

// $value contains the actual field name
function display_custom_condition_lines($focus, $field, $value, $view) {

    global $locale, $app_list_strings, $mod_strings;

    $html = '';

    if (!is_file('cache/jsLanguage/AOW_Conditions/' . $GLOBALS['current_language'] . '.js')) {
        require_once ('include/language/jsLanguage.php');
        jsLanguage::createModuleStringsCache('AOW_Conditions', $GLOBALS['current_language']);
    }
    $html .= '<script src="cache/jsLanguage/AOW_Conditions/'. $GLOBALS['current_language'] . '.js"></script>';
	
	$settings = dtbcGetConditionSettings($focus->{$value}, $focus->id);

    if ($view == 'EditView') {

        $html .= '<script src="custom/modules/AOW_Conditions/conditionLines.js"></script>';
        $html .= "<table border='0' cellspacing='4' width='100%' id='conditionLines'></table>";

        $html .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
        $html .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"".$mod_strings['LBL_ADD_CONDITION']."\" id=\"btn_ConditionLine\" onclick=\"insertConditionLine()\" disabled/>";
        $html .= "</div>";

        if (isset($settings['flow_module']) && $settings['flow_module'] != '') {
            require_once("modules/AOW_WorkFlow/aow_utils.php");
            $html .= "<script>";
            $html .= "flow_rel_modules = \"".trim(preg_replace('/\s+/', ' ', getModuleRelationships($settings['flow_module'])))."\";";
            $html .= "flow_module = \"" . $settings['flow_module'] . "\";";
            $html .= "document.getElementById('btn_ConditionLine').disabled = '';";
            $html .= dtbcGetConditionLinesHtml($settings);
            $html .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ', getModuleFields($settings['flow_module'])))."\";";
            $html .= "</script>";
        }

    } else if ($view == 'DetailView') {
        $html .= '<script src="custom/modules/AOW_Conditions/conditionLines.js"></script>';
        $html .= "<table border='0' cellspacing='0' width='100%' id='conditionLines'></table>";

        if (isset($settings['flow_module']) && $settings['flow_module'] != '') {
            require_once("modules/AOW_WorkFlow/aow_utils.php");
            $html .= "<script>";
            $html .= "flow_rel_modules = \"".trim(preg_replace('/\s+/', ' ', getModuleRelationships($settings['flow_module'])))."\";";
            $html .= "flow_module = \"" . $settings['flow_module'] . "\";";
			$html .= dtbcGetConditionLinesHtml($settings);
			$html .= "</script>";
        }
    }
	
	$_REQUEST['back_flow_module'] = isset($settings['flow_module']) ? $settings['flow_module'] : "";
	$_REQUEST['back_line_count'] = $settings['values'] ? count($settings['values']) : 0;

    return $html;
}

function dtbcGetConditionLinesHtml($settings) {
	$html = "";
	if (isset($settings['values']) && !empty($settings['values']) && is_array($settings['values'])) {
		foreach ($settings['values'] as $condition_name) {
			$condition_name->module_path = unserialize(base64_decode($condition_name->module_path));
			if ($condition_name->module_path == '')
				$condition_name->module_path = $settings['flow_module'];
			$html .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ', getModuleFields(getRelatedModule($settings['flow_module'], $condition_name->module_path[0]))))."\";";
			if ($condition_name->value_type == 'Date') {
				$condition_name->value = unserialize(base64_decode($condition_name->value));
			}
			$condition_item = json_encode($condition_name->toArray());
			$html .= "loadConditionLine(".$condition_item.");";
		}
	}
	return $html;
}

function dtbcGetConditionSettings($savedValues, $parentId) {
	$retval = array();
	
	if (empty($savedValues))
		return $retval;
	
	$post_data = unserialize(base64_decode($savedValues));
	
	require_once('modules/AOW_WorkFlow/aow_utils.php');
	
	$retval['flow_module'] = $post_data['flow_module'];
	$retval['values'] = array();
	$key = "aow_conditions_";

	$line_count = count($post_data[$key . 'field']);
	$j = 0;
	for ($i = 0; $i < $line_count; ++$i) {

		if ($post_data[$key . 'deleted'][$i] == 1) {
			//$this->mark_deleted($post_data[$key . 'id'][$i]);
		} else {
			$condition = new AOW_Condition();
			foreach ($condition->field_defs as $field_def) {
				$field_name = $field_def['name'];
				if (isset($post_data[$key . $field_name][$i])) {
					if (is_array($post_data[$key . $field_name][$i])) {
						if ($field_name == 'module_path') {
							$post_data[$key . $field_name][$i] = base64_encode(serialize($post_data[$key . $field_name][$i]));
						} else {
							switch ($condition->value_type) {
								case 'Date':
									$post_data[$key . $field_name][$i] = base64_encode(serialize($post_data[$key . $field_name][$i]));
									break;
								default:
									$post_data[$key . $field_name][$i] = encodeMultienumValue($post_data[$key . $field_name][$i]);
							}
						}
					} else if ($field_name === 'value' && $post_data[$key . 'value_type'][$i] === 'Value') {
						$post_data[$key . $field_name][$i] = fixUpFormatting($post_data['flow_module'], $condition->field, $post_data[$key . $field_name][$i]);
					}
					$condition->$field_name = $post_data[$key . $field_name][$i];
				}

			}
			if (trim($condition->field) != '') {
				$condition->condition_order = trim(++$j . " ");
				$condition->aow_workflow_id = $parentId;
				$retval['values'][] = $condition;
			}
		}
	}
	
	return $retval;
}

?>
