function validateFilterLogic(formName) {
	if ($('#filter_logic_c').length) {
		var logic = $('#filter_logic_c').val().toLowerCase().trim();
		var beginBracket = logic.split("(").length - 1;
		var endBracket = logic.split(")").length - 1;
		
		$.each(logic.split(" "), function (i, val) {
			if (val.trim() == '' ||
				val.trim() == 'and' ||
				val.trim() == 'or' ||
				/^[)]*$/.test(val.trim()) ||
				/^[(]*$/.test(val.trim()) ||
				/^\d+$/.test(val.trim()) ||
				/^[0-9]*[)]*$/.test(val.trim()) ||
				/^[(]*[0-9]*$/.test(val.trim())
				) {
					// valid
				} else {
					// invalid
					add_error_style_custom(formName, 'filter_logic_c', SUGAR.language.get('dtbc_Assignment_Rules','LBL_VALIDATION_ERR_FILTER_LOGIC'));
					return false;
				}
		});
		
		if (beginBracket != endBracket) {
			add_error_style_custom(formName, 'filter_logic_c', SUGAR.language.get('dtbc_Assignment_Rules','LBL_VALIDATION_ERR_FILTER_LOGIC_BRACKETS'));
			return false;
		}
	}
	return true;
}

function validateUserGroup(formName) {
	if ($('#user_id_c').length && $('#securitygroup_id_c').length && 
		($('#do_not_reassign_owner_c').length && $("#do_not_reassign_owner_c:checked").val() != 1) || !$('#do_not_reassign_owner_c').length) {
			if ($('#user_id_c').val() != '' || $('#securitygroup_id_c').val() != '') {
				// valid
			} else {
				add_error_style_custom(formName, 'user_id_c', SUGAR.language.get('dtbc_Assignment_Rules','LBL_VALIDATION_ERR_USERGROUP'));
				add_error_style_custom(formName, 'securitygroup_id_c', SUGAR.language.get('dtbc_Assignment_Rules','LBL_VALIDATION_ERR_USERGROUP'));
				return false;
			}
	}
	return true;
}

function calculateRequiredFields(formName) {
	if ($('#user_id_c').length && !$('#securitygroup_id_c').length) {
		addRequired('user_id_c', formName);
	}
	
	if (!$('#user_id_c').length && $('#securitygroup_id_c').length) {
		addRequired('securitygroup_id_c', formName);
	}
}

function addRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex == -1) {
			$().html(current.substring(0, myIndex));
			$('[dtbc-data=' + labelName + ']').append('<span class="required">*</span>');
			addToValidate(formName, fieldName, fieldName, true);
		}
	}	
}

function removeRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex != -1) {
			$('[dtbc-data=' + labelName + ']').html(current.substring(0, myIndex));
			removeFromValidate(formName, fieldName);
		}
	}
}

function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation() {
	clear_all_errors();
	var formName = 'EditView';
	var retval = check_form(formName);
	retval = calcRetval(retval, validateUserGroup(formName));
	retval = calcRetval(retval, validateFilterLogic(formName));

	// Original validations
	if (retval) {
		var _form = document.getElementById(formName);
	    _form.action.value = 'Save';
	    SUGAR.ajaxUI.submitForm(_form);

	    return true;
	}
	scrollToError();
	
	return false;
}

$(document).ready(function() {
	jQuery.getScript('custom/include/dtbc/js/scrollingAfterAddingErrorStyle.js');
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	$("<input title='" + SUGAR.language.get('dtbc_Assignment_Rules','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation();' type='button' name='button' value='" + SUGAR.language.get('dtbc_Assignment_Rules','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
	
	var editFormName = 'EditView';
	
	calculateRequiredFields(editFormName);
});