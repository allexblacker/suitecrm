<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_VALIDATION_ERR_FILTER_LOGIC'] = 'Filter logic must contain only the followings: curly brackets (), phrase and, phrase or, numbers and spaces. Please make sure that before and after the phrases has a space!';
$mod_strings['LBL_VALIDATION_ERR_FILTER_LOGIC_BRACKETS'] = 'Open and close brackets number not matched';
$mod_strings['LBL_VALIDATION_ERR_USERGROUP'] = 'You must set a user or a group (or check the "Do Not Reassign Owner" option)';
$mod_strings['LBL_SAVEBUTTON'] = 'Save';

 
 // created: 2018-03-10 09:04:39
$mod_strings['LBL_ASSIGN_TO_GROUP_SECURITYGROUP_ID'] = 'Group (related  ID)';
$mod_strings['LBL_ASSIGN_TO_GROUP'] = 'Group';
$mod_strings['LBL_ASSIGN_TO_USER_USER_ID'] = 'User (related User ID)';
$mod_strings['LBL_ASSIGN_TO_USER'] = 'User';
$mod_strings['LBL_CRITERIA'] = 'Criteria';
$mod_strings['LBL_DO_NOT_REASSIGN_OWNER'] = 'Do Not Reassign Owner';
$mod_strings['LBL_FILTER_LOGIC'] = 'Filter Logic';
$mod_strings['LBL_MODULE'] = 'Module';
$mod_strings['LBL_RULE_ORDER'] = 'Order';
$mod_strings['LBL_EDITVIEW_PANEL1'] = 'Assignment';
$mod_strings['LBL_EDITVIEW_PANEL2'] = 'New Panel 2';
$mod_strings['LBL_EDITVIEW_PANEL3'] = 'Criteria';
$mod_strings['LBL_EDITVIEW_PANEL4'] = 'Order';
$mod_strings['LBL_MODULE_CODE'] = 'Module Code';



?>