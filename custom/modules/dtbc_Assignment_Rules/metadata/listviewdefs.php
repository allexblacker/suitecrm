<?php
$module_name = 'dtbc_Assignment_Rules';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'RULE_ORDER_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_RULE_ORDER',
    'width' => '10%',
  ),
  'MODULE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_MODULE',
    'width' => '10%',
  ),
  'ASSIGN_TO_GROUP_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_ASSIGN_TO_GROUP',
    'id' => 'SECURITYGROUP_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'ASSIGN_TO_USER_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_ASSIGN_TO_USER',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'FILTER_LOGIC_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_FILTER_LOGIC',
    'width' => '10%',
  ),
  'DO_NOT_REASSIGN_OWNER_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_DO_NOT_REASSIGN_OWNER',
    'width' => '10%',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
?>
