<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once("custom/include/dtbc/helpers.php");

class dtbc_Assignment_RulesViewEdit extends ViewEdit {

    function display() {		
        parent::display();
		
		echo getVersionedScript("custom/modules/dtbc_Assignment_Rules/dtbc/editview.js");
    }
	
}
