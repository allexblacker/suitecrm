<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/SugarView.php');
require_once('custom/include/dtbc/helpers.php');

class ImportCommonStep4 extends SugarView
{
    private $currentStep;

    public function __construct($bean = null, $view_object_map = array())
    {
        parent::__construct($bean, $view_object_map);
        $this->currentStep = isset($_REQUEST['current_step']) ? ($_REQUEST['current_step'] + 1) : 1;
    }

    /**
     * @see SugarView::display()
     */
 	public function display($importerClassName)
    {
        global $mod_strings, $sugar_config;

        // Check to be sure we are getting an import file that is in the right place
        $uploadFile = "upload://".basename($_REQUEST['tmp_file']);
        if(!file_exists($uploadFile)) {
			DtbcHelpers::logger("File not exists: " . $uploadFile, "import.log");
            trigger_error($mod_strings['LBL_CANNOT_OPEN'],E_USER_WARNING);
        }

        // Open the import file
        $importSource = new ImportFile($uploadFile, $_REQUEST['custom_delimiter'],html_entity_decode($_REQUEST['custom_enclosure'],ENT_QUOTES));

        //Ensure we have a valid file.
        if ( !$importSource->fileExists() ) {
			DtbcHelpers::logger("Invalid file: " . $uploadFile, "import.log");
			trigger_error($mod_strings['LBL_CANNOT_OPEN'],E_USER_WARNING);
		}
            

        if (!ImportCacheFiles::ensureWritable()) {
			DtbcHelpers::logger("Cannot write cache file. (" . $uploadFile . ")", "import.log");
            trigger_error($mod_strings['LBL_ERROR_IMPORT_CACHE_NOT_WRITABLE'], E_USER_WARNING);
        }

        //$importer = new ImporterAlliance($importSource, $this->bean);
		$importer = new $importerClassName($importSource, $this->bean);
        $importer->import();
    }
}
