<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/modules/Import/views/view.common_step4.php');
require_once('custom/modules/Import/Importer.php');
require_once('custom/include/dtbc/helpers.php');

class ImportViewstep4_alliance extends ImportCommonStep4
{
	public function display()
    {
		parent::display('ImporterAlliance');
	}
}
