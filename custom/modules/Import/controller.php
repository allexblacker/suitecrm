<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
require_once('modules/Import/controller.php');

class CustomImportController extends ImportController {

	public function action_Step4() {
		if (isset($_REQUEST['import_module']) && $_REQUEST['import_module'] == 'AL1_Alliance_Transaction')
			$this->view = 'step4_alliance';
		else if (isset($_REQUEST['import_module']) && $_REQUEST['import_module'] == 'SN1_Serial_Number')
			$this->view = 'step4_serialnumber';
		else
			$this->view = 'step4';
    }
	
	public function action_index() {
        $this->action_Step1();
    }
	
	public function action_Step1() {
		if (isset($_REQUEST['import_module']) && $_REQUEST['import_module'] == 'AL1_Alliance_Transaction') {
			$this->view = 'step2_alliance';
		} else if (isset($_REQUEST['import_module']) && $_REQUEST['import_module'] == 'SN1_Serial_Number') {
			$this->view = 'step2_serialnumber';
		} else {
			parent::action_Step1();
		}
    }
}