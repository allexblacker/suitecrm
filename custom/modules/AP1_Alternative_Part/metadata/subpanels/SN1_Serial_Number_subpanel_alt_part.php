<?php
// created: 2017-10-30 16:00:56
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'priority' => 
  array (
    'type' => 'decimal',
    'vname' => 'Priority',
    'width' => '10%',
    'default' => true,
  ),
  'warehouse' => 
  array (
    'type' => 'varchar',
    'vname' => 'Warehouse',
    'width' => '10%',
    'default' => true,
  ),
  'bin' => 
  array (
    'type' => 'varchar',
    'vname' => 'Bin',
    'width' => '10%',
    'default' => true,
  ),
  'stock_level' => 
  array (
    'type' => 'decimal',
    'vname' => 'Stock_Level',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);