<?php
$viewdefs ['Contacts'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'hidden' => 
        array (
          0 => '<input type="hidden" name="opportunity_id" value="{$smarty.request.opportunity_id}">',
          1 => '<input type="hidden" name="case_id" value="{$smarty.request.case_id}">',
          2 => '<input type="hidden" name="bug_id" value="{$smarty.request.bug_id}">',
          3 => '<input type="hidden" name="email_id" value="{$smarty.request.email_id}">',
          4 => '<input type="hidden" name="inbound_email_id" value="{$smarty.request.inbound_email_id}">',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'first_name',
            'customCode' => '{html_options name="salutation" id="salutation" options=$fields.salutation.options selected=$fields.salutation.value}&nbsp;<input name="first_name"  id="first_name" size="25" maxlength="25" type="text" value="{$fields.first_name.value}">',
          ),
          1 => 
          array (
            'name' => 'phone_work',
            'comment' => 'Work phone number of the contact',
            'label' => 'LBL_OFFICE_PHONE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'last_name',
          ),
          1 => 
          array (
            'name' => 'mobile_c',
            'label' => 'MOBILE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'email1',
            'studio' => 'false',
            'label' => 'LBL_EMAIL_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'w_9_c',
            'label' => 'W_9',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'account_name',
            'displayParams' => 
            array (
              'key' => 'billing',
              'copy' => 'primary',
              'billingKey' => 'primary',
              'additionalFields' => 
              array (
                'phone_office' => 'phone_work',
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'master_contact_c',
            'label' => 'MASTER_CONTACT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'contact_support_comments_c',
            'label' => 'CONTACT_SUPPORT_COMMENTS',
          ),
          1 => 
          array (
            'name' => 'contact_type_c',
            'studio' => 'visible',
            'label' => 'CONTACT_TYPE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'linkedin_search_c',
            'label' => 'LBL_LINKEDIN_SEARCH',
          ),
          1 => 
          array (
            'name' => 'cont_type_acc_service_is_mi_c',
            'label' => 'LBL_CONT_TYPE_ACC_SERVICE_IS_MI',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'google_search_c',
            'label' => 'LBL_GOOGLE_SEARCH',
          ),
          1 => 
          array (
            'name' => 'xing_search_c',
            'label' => 'LBL_XING_SEARCH_C',
          ),
        ),
      ),
    ),
  ),
);
?>
