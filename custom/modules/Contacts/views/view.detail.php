<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/DetailView/DetailViewCustomCache.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class ContactsViewDetail extends ViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailViewCustomCache();
 	    $this->dv->ss =&  $this->ss;
        
		$fieldsToHTMLDecode = array("cont_type_acc_service_is_mi_c","certified_contact_c","google_search_c","linkedin_search_c","location_c","xing_search_c");
		foreach($fieldsToHTMLDecode as $field){
			$this->bean->$field = html_entity_decode($this->bean->$field);
		}
		
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
	protected function _displaySubPanels() {
        if (isset($this->bean) &&
            !empty($this->bean->id) &&
            (file_exists('modules/' . $this->module . '/metadata/subpaneldefs.php') ||
             file_exists('custom/modules/' . $this->module . '/metadata/subpaneldefs.php') ||
             file_exists('custom/modules/' . $this->module . '/Ext/Layoutdefs/layoutdefs.ext.php'))
        ) {
            $GLOBALS['focus'] = $this->bean;
            require_once('custom/include/SubPanel/CustomSubPanelTiles.php');
            $subpanel = new CustomSubPanelTiles($this->bean, $this->module);
            echo $subpanel->display();
        }
    }
}
