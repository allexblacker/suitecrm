<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/MVC/View/views/view.list.php');

class ContactsViewList extends CustomViewList{

    function __construct(){
        parent::__construct();
    }
}
