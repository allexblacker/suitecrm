<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getTrainingLMS($params) {
    $db = DBManagerFactory::getInstance();

    $sql = "SELECT tla1_training_lms_absorb.*
            FROM tla1_training_lms_absorb
            JOIN tla1_training_lms_absorb_cstm ON tla1_training_lms_absorb.id = tla1_training_lms_absorb_cstm.id_c AND contact_id_c = " . $db->quoted($params['contact_id']) .
           "JOIN contacts ON tla1_training_lms_absorb_cstm.contact_id_c = contacts.id AND contacts.deleted = 0
            WHERE tla1_training_lms_absorb.deleted = 0";

    return $sql;
}
