<?php

require_once('modules/Contacts/Contact.php');
require_once('custom/include/dtbc/helpers.php');

class CustomContact extends Contact {
	
	public function create_new_list_query($order_by, $where,$filter=array(),$params=array(), $show_deleted = 0,$join_type='', $return_array = false,$parentbean=null, $singleSelect = false, $ifListForExport = false) {	
		$retval = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);

		$retval = $this->handleQs($retval);

		if (empty($_REQUEST['accountId']))
			return $retval;

		global $db;
		// There is a selected Account, we need to display only the contacts related to the account

		if (strlen($retval['where']) > 0)
			$retval['where'] .= " AND";
		$retval['where'] .= " accounts.id = " . $db->quoted($_REQUEST['accountId']);

		return $retval;
	}
	
	
	private function handleQs($query) {
		if (!isset($_REQUEST['action']) || $_REQUEST['action'] != 'quicksearchQuery')
			return $query;
		
		global $db;
		$data = json_decode(html_entity_decode_utf8($_REQUEST['data']));
		
		if (empty($data->query->relatedId))
			return $query;
			
		$retval = str_replace("ORDER BY", " AND accounts_contacts.account_id = " . $db->quoted($data->query->relatedId) . " ORDER BY", $query);
		$retval = str_replace("where", " join accounts_contacts on accounts_contacts.contact_id = contacts.id and accounts_contacts.deleted = 0 where", $retval);
		
		return $retval;

	}
}

?>