<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

function getRelatedCases($params){
	$db = DBManagerFactory::getInstance();
    $contactId = $db->quoted($_REQUEST['record']);
	return "SELECT * FROM cases WHERE contact_created_by_id = $contactId AND deleted = 0";
}