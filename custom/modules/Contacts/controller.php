<?php

require_once('modules/Contacts/controller.php');
require_once('custom/modules/Contacts/customContact.php');

class CustomContactsController extends ContactsController {
	
	public function action_Popup() {
		$this->bean = new CustomContact();
		parent::action_Popup();
	}

}
?>