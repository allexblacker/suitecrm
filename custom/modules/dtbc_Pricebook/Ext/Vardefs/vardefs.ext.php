<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-05 15:40:36
$dictionary['dtbc_Pricebook']['fields']['description']['audited']=true;
$dictionary['dtbc_Pricebook']['fields']['description']['inline_edit']=true;
$dictionary['dtbc_Pricebook']['fields']['description']['comments']='Full text of the note';
$dictionary['dtbc_Pricebook']['fields']['description']['merge_filter']='disabled';

 

 // created: 2017-10-05 15:34:41
$dictionary['dtbc_Pricebook']['fields']['active_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook']['fields']['active_c']['labelValue']='Active';

 

 // created: 2017-10-05 15:43:20
$dictionary['dtbc_Pricebook']['fields']['is_standard_price_book_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook']['fields']['is_standard_price_book_c']['labelValue']='Is Standard Price Book';

 

 // created: 2017-10-05 16:19:36
$dictionary['dtbc_Pricebook']['fields']['currency_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook']['fields']['currency_c']['labelValue']='Currency';

 

 // created: 2017-10-05 15:32:50
$dictionary['dtbc_Pricebook']['fields']['name']['audited']=true;
$dictionary['dtbc_Pricebook']['fields']['name']['inline_edit']=true;
$dictionary['dtbc_Pricebook']['fields']['name']['duplicate_merge']='disabled';
$dictionary['dtbc_Pricebook']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['dtbc_Pricebook']['fields']['name']['merge_filter']='disabled';
$dictionary['dtbc_Pricebook']['fields']['name']['unified_search']=false;

 

 // created: 2017-10-05 15:46:47
$dictionary['dtbc_Pricebook']['fields']['price_book_id_c']['inline_edit']='1';
$dictionary['dtbc_Pricebook']['fields']['price_book_id_c']['labelValue']='Price Book ID';

 

// created: 2017-10-04 15:50:43
$dictionary["dtbc_Pricebook"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook_Product',
  'bean_name' => 'dtbc_Pricebook_Product',
  'side' => 'right',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);

?>