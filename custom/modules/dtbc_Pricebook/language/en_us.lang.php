<?php
// created: 2017-10-17 15:22:50
$mod_strings = array (
  'LBL_NAME' => 'Price Book Name',
  'LBL_ACTIVE' => 'Active',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_IS_STANDARD_PRICE_BOOK' => 'Is Standard Price Book',
  'LBL_PRICE_BOOK_ID' => 'Price Book ID',
  'LBL_CURRENCY' => 'Currency',
  'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE' => 'Pricebook Product',
);