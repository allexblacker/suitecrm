<?php
// created: 2017-10-10 13:27:34
$mod_strings = array (
  'LBL_LOGIN_DATE' => 'Login Date',
  'LBL_IP_ADDRESS' => 'IP Address',
  'LBL_USERID' => 'User ID',
  'LBL_USERNAME' => 'Username',
  'LBL_LOGGED_USER_USER_ID' => 'User (related User ID)',
  'LBL_LOGGED_USER' => 'User',
  'LBL_EDITVIEW_PANEL1' => 'Login Log',
  'LBL_EDITVIEW_PANEL2' => 'New Panel 2',
);