<?php

require_once 'modules/Home/quickSearch.php';

class quicksearchQueryCustom extends quicksearchQuery
{
    protected function getRawResults($args, $singleSelect = false) {
        $orderBy = !empty($args['order']) ? $args['order'] : '';
        $limit = !empty($args['limit']) ? intval($args['limit']) : '';
        $data = array();

        foreach ($args['modules'] as $module) {
            $focus = $this->_getBean($module);

            $orderBy = $focus->db->getValidDBName(($args['order_by_name'] && $focus instanceof Person && $args['order'] == 'name') ? 'last_name' : $orderBy);

            if ($focus->ACLAccess('ListView', true)) {
                $where = $this->constructWhere($focus, $args);
                $data = $this->updateData($data, $focus, $orderBy, $where, $limit, $singleSelect);
            }
        }

        return $data;
    }

    private function _getBean($module) {
        global $beanList;
        if ($beanList[$module]) {
            $customModuleName = 'custom' . $beanList[$module];
            $customBeanFile = 'custom/modules/' . $module . '/' . $customModuleName . '.php';
            if (file_exists($customBeanFile)) {
                require_once $customBeanFile;
                return new $customModuleName();
            }
        }

        return SugarModule::get($module)->loadBean();
    }

}