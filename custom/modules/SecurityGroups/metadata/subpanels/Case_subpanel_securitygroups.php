<?php
// created: 2018-02-12 18:48:56
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'description' => 
  array (
    'vname' => 'LBL_DESCRIPTION',
    'width' => '10%',
    'sortable' => false,
    'default' => true,
  ),
  'remove_button' => 
  array (
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'SecurityGroups',
    'width' => '5%',
    'default' => true,
  ),
);