<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');

class IS1_Inside_SalesViewEdit extends ViewEdit{

	public function display() {	
		parent::display();
		echo $this->getDefaultValuesForQsFields();
		echo getVersionedScript('custom/modules/IS1_Inside_Sales/dtbc/validations.js');
		echo getVersionedScript('custom/include/dtbc/js/qsContacts.js');
	}
	
	private function getDefaultValuesForQsFields() {
		return "
			<input type='hidden' value='accounts_is1_inside_sales_1accounts_ida' id='qs_hidden_account_id'>
			<input type='hidden' value='contact_id_c' id='qs_hidden_contact_id'>
			<input type='hidden' value='contact_c' id='qs_hidden_contact_name'>
		";
	}
}