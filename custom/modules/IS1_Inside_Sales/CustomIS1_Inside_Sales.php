<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
require_once('modules/IS1_Inside_Sales/IS1_Inside_Sales.php');

class CustomIS1_Inside_Sales extends IS1_Inside_Sales {
    
	public function create_new_list_query($order_by, $where,$filter=array(),$params=array(), $show_deleted = 0,$join_type='', $return_array = false,$parentbean=null, $singleSelect = false, $ifListForExport = false) {
		$res = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);
		
		if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'index' && empty($_REQUEST['lvso']))
			$res['order_by'] = " ORDER BY is1_inside_sales.date_entered DESC ";
		
		return $res;
	}

}