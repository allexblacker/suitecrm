<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("include/MVC/Controller/SugarController.php");
require_once("custom/modules/IS1_Inside_Sales/CustomIS1_Inside_Sales.php");

class CustomIS1_Inside_SalesController extends SugarController {
	
	public function loadBean()
	{
		if(!empty($GLOBALS['beanList'][$this->module])) {
			$this->bean = new CustomIS1_Inside_Sales();
			if(!empty($this->record)) {
				$this->bean->retrieve($this->record);
				if ($this->bean)
					$GLOBALS['FOCUS'] = $this->bean;
			}
		}
	}
	
}
