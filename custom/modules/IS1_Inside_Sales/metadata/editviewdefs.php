<?php
$module_name = 'IS1_Inside_Sales';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'role',
            'studio' => 'visible',
            'label' => 'Role',
          ),
          1 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accounts_is1_inside_sales_1_name',
          ),
          1 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'Type',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'call_c',
            'studio' => 'visible',
            'label' => 'LBL_CALL_C',
          ),
          1 => 
          array (
            'name' => 'notes',
            'studio' => 'visible',
            'label' => 'Notes',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'call_to_action',
            'studio' => 'visible',
            'label' => 'Call_to_Action',
          ),
          1 => 
          array (
            'name' => 'ist_appt_set',
            'label' => 'IST_APPT_set',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'decision_maker',
            'studio' => 'visible',
            'label' => 'Decision_Maker',
          ),
          1 => 
          array (
            'name' => 'contact_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'highlights_c',
            'label' => 'LBL_HIGHLIGHTS',
          ),
          1 => 
          array (
            'name' => 'lowlights_c',
            'label' => 'LBL_LOWLIGHTS',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'chips_c',
            'studio' => 'visible',
            'label' => 'LBL_CHIPS',
          ),
        ),
      ),
    ),
  ),
);
?>
