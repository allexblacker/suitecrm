<?php
// created: 2018-02-27 12:54:23
$mod_strings = array (
  'LBL_ACCOUNT' => 'Account',
  'ROLE' => 'Role',
  'LBL_CALL_C' => 'Call',
  'CALL_TO_ACTION' => 'Call to Action',
  'DECISION_MAKER' => 'Decision Maker',
  'TYPE' => 'Type',
  'LBL_CONTACT_CONTACT_ID' => 'Contact (related Contact ID)',
  'LBL_CONTACT' => 'Contact',
  'LBL_HIGHLIGHTS' => 'Highlights',
  'LBL_LOWLIGHTS' => 'Lowlights',
  'LBL_CHIPS' => 'Chips',
);