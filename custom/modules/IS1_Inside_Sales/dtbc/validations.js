function customValidation() {
	clear_all_errors();
	var formName = 'EditView';
	var checkForm = check_form(formName);
	var retval = ist_appt_set_is_mandatory_when_call_to(formName);

	if (retval && checkForm) {
		var _form = document.getElementById(formName);
	    _form.action.value = 'Save';
	    SUGAR.ajaxUI.submitForm(_form);

	    return true;
	}
	
	scrollToError();
	return false;
}

function ist_appt_set_is_mandatory_when_call_to(formName){
	var fieldName = 'ist_appt_set';
	var field = $("#" + fieldName);
	if(field.length == 0)
		return true;
	
	if($('#call_to_action').val() == 3 && field.val() == ''){
		add_error_style_custom(formName, fieldName, SUGAR.language.get('IS1_Inside_Sales','LBL_VALIDATION_ERR_IST_APPT_SET'));
		return false;
	}
	return true;
}

$(document).ready(function() {
	jQuery.getScript('custom/include/dtbc/js/scrollingAfterAddingErrorStyle.js');
	$("input[type=submit]").hide();
	$("<input title='" + SUGAR.language.get('IS1_Inside_Sales','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation();' type='button' name='button' value='" + SUGAR.language.get('IS1_Inside_Sales','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
	
	$("#btn_contact_c").removeAttr("onclick");
    $("#btn_contact_c").prop('onclick', null).off('click');
    $('#btn_contact_c').on('click', function () {
        open_popup(
			"Contacts", 
			600, 
			400, 
			"&accountId=" + $("#accounts_is1_inside_sales_1accounts_ida").val(), 
			true, 
			false, 
			{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"contact_id_c","name":"contact_c"}}, 
			"single", 
			true
		);
    });

});