<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2018-02-27 12:45:45
$dictionary['IS1_Inside_Sales']['fields']['highlights_c']['inline_edit']='1';
$dictionary['IS1_Inside_Sales']['fields']['highlights_c']['labelValue']='Highlights';

 

 // created: 2017-10-24 19:24:19

 

 // created: 2017-11-03 23:39:32
$dictionary['IS1_Inside_Sales']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2017-10-24 19:24:33

 

// created: 2017-11-03 23:37:27
$dictionary["IS1_Inside_Sales"]["fields"]["is1_inside_sales_contacts_1"] = array (
  'name' => 'is1_inside_sales_contacts_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_CONTACTS_1_FROM_CONTACTS_TITLE',
);


 // created: 2017-11-03 23:39:32
$dictionary['IS1_Inside_Sales']['fields']['contact_c']['inline_edit']='1';
$dictionary['IS1_Inside_Sales']['fields']['contact_c']['labelValue']='Contact';

 

 // created: 2018-02-27 12:45:57
$dictionary['IS1_Inside_Sales']['fields']['lowlights_c']['inline_edit']='1';
$dictionary['IS1_Inside_Sales']['fields']['lowlights_c']['labelValue']='Lowlights';

 

 // created: 2018-02-15 12:22:07

 

// created: 2017-10-25 18:47:53
$dictionary["IS1_Inside_Sales"]["fields"]["accounts_is1_inside_sales_1"] = array (
  'name' => 'accounts_is1_inside_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_is1_inside_sales_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_is1_inside_sales_1accounts_ida',
);
$dictionary["IS1_Inside_Sales"]["fields"]["accounts_is1_inside_sales_1_name"] = array (
  'name' => 'accounts_is1_inside_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_is1_inside_sales_1accounts_ida',
  'link' => 'accounts_is1_inside_sales_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["IS1_Inside_Sales"]["fields"]["accounts_is1_inside_sales_1accounts_ida"] = array (
  'name' => 'accounts_is1_inside_sales_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_is1_inside_sales_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE',
);


 // created: 2017-10-24 19:25:01

 

// created: 2017-11-01 02:15:14
$dictionary["IS1_Inside_Sales"]["fields"]["is1_inside_sales_tasks_1"] = array (
  'name' => 'is1_inside_sales_tasks_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_TASKS_1_FROM_TASKS_TITLE',
);


 // created: 2017-11-02 12:50:11

 

 // created: 2018-02-27 12:54:23
$dictionary['IS1_Inside_Sales']['fields']['chips_c']['inline_edit']='1';
$dictionary['IS1_Inside_Sales']['fields']['chips_c']['labelValue']='Chips';

 
?>