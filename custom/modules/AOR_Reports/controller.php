<?php

if(!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/AOR_Reports/controller.php');
require_once ('custom/modules/AOR_Reports/CustomAOR_Report.php');

class CustomAOR_ReportsController extends AOR_ReportsController{

    public function action_export(){

        $customBean = new CustomAOR_Report();
        $this->bean = $customBean->retrieve($_REQUEST['record']);
        parent::action_export();
    }

}