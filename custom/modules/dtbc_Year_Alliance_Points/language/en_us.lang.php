<?php
// created: 2017-10-12 11:02:10
$mod_strings = array (
  'LBL_KW_INSTALLED' => 'KW Installed',
  'LBL_TOTAL_POINTS' => 'Total Points',
  'LBL_GIFT_VALUE' => 'Gift Value',
  'LBL_AVAILABLE_POINTS' => 'Available Points',
  'LBL_EXPIRED_POINTS' => 'Expired Points',
  'LBL_DATE_EXPIRATION' => 'Date Expiration',
  'LBL_NAME' => 'Year',
  'LBL_YEAR' => 'Year',
  'LBL_LAST_CALCULATION_TIME' => 'Last Calculation Time',
);