<?php

require_once 'modules/AOP_Case_Updates/AOP_Case_Updates.php';
require_once 'include/SugarPHPMailer.php';
require_once("modules/emqu_dtbc_Email_Queue/emqu_dtbc_Email_Queue.php");

class CustomAOP_Case_Updates extends AOP_Case_Updates {

	private $originalObj;
	
	public function setOriginalObj($inputObj) {
		$this->originalObj = $inputObj;
	}
	
	private function populateTemplate(EmailTemplate $template, $addDelimiter = true, $contactId = null)
    {
        global $app_strings, $sugar_config;

        $user = $this->originalObj->getUpdateUser();
        if (!$user) {
            $this->originalObj->getUser();
        }
        $beans = array('Contacts' => $contactId, 'Cases' => $this->originalObj->getCase()->id, 'Users' => $user->id, 'AOP_Case_Updates' => $this->originalObj->id);
        $ret = array();
        $ret['subject'] = from_html(aop_parse_template($template->subject, $beans));
        $body = aop_parse_template(str_replace('$sugarurl', $sugar_config['site_url'], $template->body_html), $beans);
        $bodyAlt = aop_parse_template(str_replace('$sugarurl', $sugar_config['site_url'], $template->body), $beans);
        if ($addDelimiter) {
            $body = $app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'].$body;
            $bodyAlt = $app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'].$bodyAlt;
        }
        $ret['body'] = from_html($body);
        $ret['body_alt'] = strip_tags(from_html($bodyAlt));

        return $ret;
    }

    /**
     * @param array $emails
     * @param EmailTemplate $template
     * @param array $signature
     * @param null $caseId
     * @param bool $addDelimiter
     * @param null $contactId
     *
     * @return bool
     */
    public function sendEmail(
        $emails,
        $template,
        $signature = array(),
        $caseId = null,
        $addDelimiter = true,
        $contactId = null
    ) {
		$admin = new Administration();
        $admin->retrieveSettings();
		
		// Get signatures
		$signatureHTML = '';
        if ($signature && array_key_exists('signature_html', $signature)) {
            $signatureHTML = from_html($signature['signature_html']);
        }
        $signaturePlain = '';
        if ($signature && array_key_exists('signature', $signature)) {
            $signaturePlain = $signature['signature'];
        }
		
		$emailSettings = getPortalEmailSettings();
		$text = $this->populateTemplate($template, $addDelimiter, $contactId);
		
		// Set Queue item to send the email later
		$newQueue = new emqu_dtbc_Email_Queue();
		$newQueue->name = 'Email from ' . $emailSettings['from_address'];
		$newQueue->email_subject_c = $text['subject'];
		$newQueue->email_content_c = $text['body_alt'] . $signaturePlain;
		$newQueue->email_content_html_c = $text['body'] . $signatureHTML;
		$newQueue->from_address_c = $emailSettings['from_address'];
		$newQueue->from_name_c = $emailSettings['from_name'];
		$newQueue->to_address_c = implode(',', $emails);
		$newQueue->cc_address_c = "";
		$newQueue->bcc_address_c = "";
		if ($caseId) {
			$newQueue->related_bean_type_c = "Cases";
			$newQueue->related_bean_id_c = $caseId;
		}
		$newQueue->email_template_id_c = $template->id;
		$newQueue->smtp_config_key_c = "AOP_Case_Updates";
		$newQueue->aow_workflow_id_c = "";
		$newQueue->save();

        return true;
    }
}
