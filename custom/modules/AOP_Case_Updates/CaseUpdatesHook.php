<?php
require_once('modules/AOP_Case_Updates/CaseUpdatesHook.php');
require_once('custom/modules/AOP_Case_Updates/CustomAOP_Case_Updates.php');
require_once('custom/include/dtbc/helpers.php');

/**
 * Class CaseUpdatesHook
 */
class CustomCaseUpdatesHook extends CaseUpdatesHook {

    public function sendCaseUpdate(AOP_Case_Updates $caseUpdate)
    {
        if ($_REQUEST['module'] === 'Import') {
            //Don't send email on import
            return;
        }
        if (!isAOPEnabled()) {
            return;
        }
		
		$this->updateCaseFields();
		
		// Send email notification only when Internal update is off and Send Notification is on
        if ($this->isEmailSendingMandatory()) {
			global $current_user, $sugar_config;
			
			$email_template = new EmailTemplate();
            $signature = array();
			$addDelimiter = true;
			$aop_config = $sugar_config['aop'];
			
			if (!empty($caseUpdate->assigned_user_id)) {
				if ($aop_config['contact_email_template_id']) {
					$email_template = $email_template->retrieve($aop_config['contact_email_template_id']);
					$signature = $current_user->getDefaultSignature();
				}
				if (strlen($aop_config['contact_email_template_id']) > 0) {
					$case = $caseUpdate->getCase();
					if (strlen($case->contact_created_by_id) == 0)
						return;
					$contact = BeanFactory::getBean('Contacts', $case->contact_created_by_id);
					$emails = array();
					$emails[] = $contact->emailAddress->getPrimaryAddress($contact);
					$emailQueueHandler = new CustomAOP_Case_Updates();
					$emailQueueHandler->setOriginalObj($caseUpdate);
					$res = $emailQueueHandler->sendEmail(
						$emails,
						$email_template,
						$signature,
						$caseUpdate->case_id,
						$addDelimiter,
						$contact->id
					);
					if (!$res)
						$GLOBALS['log']->Error('AOPCaseUpdates: could not sent email to contact');
				}
			} else {
				$emails = $caseUpdate->getEmailForUser();
				if ($aop_config['user_email_template_id']) {
					$email_template = $email_template->retrieve($aop_config['user_email_template_id']);
				}
				$addDelimiter = false;
				if (count($emails) > 0 && strlen($aop_config['user_email_template_id']) > 0) {
					$GLOBALS['log']->info('AOPCaseUpdates: Calling send email');
					$emailQueueHandler = new CustomAOP_Case_Updates();
					$emailQueueHandler->setOriginalObj($caseUpdate);
					$res = $emailQueueHandler->sendEmail(
						$emails,
						$email_template,
						$signature,
						$caseUpdate->case_id,
						$addDelimiter,
						$caseUpdate->contact_id
					);
					if (!$res)
						$GLOBALS['log']->Error('AOPCaseUpdates: could not sent email to assigned user');
				}
			}
        }
    }
	
	private function isEmailSendingMandatory() {
		return $_REQUEST['internal'] == "false" && $_REQUEST['sendnoti'] == "true";
	}
	
	private function updateCaseFields() {
		$isStatusUpdateMandatory = isset($_REQUEST['changestatus']) && $_REQUEST['changestatus'] == "true";
		$isEmailSendingMandatory = $this->isEmailSendingMandatory();
		
		if (!$isStatusUpdateMandatory && !$isEmailSendingMandatory)
			return;
		
		$bean = BeanFactory::getBean($_REQUEST['module'], $_REQUEST['record']);
		
		if ($isStatusUpdateMandatory)
			$bean->case_status_c = 3; // Waiting Customer Feedback
		
		if ($isEmailSendingMandatory)
			$bean->alert_override_c = ''; // Remove envelope after email sending
		
		$bean->save();
	}
}
