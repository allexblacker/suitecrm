<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-08-31 12:53:12
$dictionary["UZC1_USA_ZIP_Codes"]["fields"]["uzc1_usa_zip_codes_leads"] = array (
  'name' => 'uzc1_usa_zip_codes_leads',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_leads',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_LEADS_FROM_LEADS_TITLE',
);


// created: 2017-08-31 12:53:12
$dictionary["UZC1_USA_ZIP_Codes"]["fields"]["uzc1_usa_zip_codes_cases"] = array (
  'name' => 'uzc1_usa_zip_codes_cases',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_cases',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_CASES_TITLE',
);

?>