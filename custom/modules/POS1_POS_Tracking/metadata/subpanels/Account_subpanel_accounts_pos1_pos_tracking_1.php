<?php
// created: 2017-10-30 15:30:07
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'distributor_name' => 
  array (
    'type' => 'varchar',
    'vname' => 'Distributor_Name',
    'width' => '10%',
    'default' => true,
  ),
  'kw' => 
  array (
    'type' => 'decimal',
    'vname' => 'KW',
    'width' => '10%',
    'default' => true,
  ),
  'transaction_date' => 
  array (
    'type' => 'date',
    'vname' => 'Transaction_Date',
    'width' => '10%',
    'default' => true,
  ),
);