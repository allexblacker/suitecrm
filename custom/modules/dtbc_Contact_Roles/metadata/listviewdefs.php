<?php
$module_name = 'dtbc_Contact_Roles';
$listViewDefs [$module_name] = 
array (
  'CONTACTS_DTBC_CONTACT_ROLES_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_DTBC_CONTACT_ROLES_1CONTACTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'OPPORTUNITIES_DTBC_CONTACT_ROLES_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_OPPORTUNITIES_TITLE',
    'id' => 'OPPORTUNITIES_DTBC_CONTACT_ROLES_1OPPORTUNITIES_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'ROLE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_ROLE',
    'width' => '10%',
  ),
  'IS_PRIMARY_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_IS_PRIMARY',
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => false,
    'link' => true,
  ),
);
?>
