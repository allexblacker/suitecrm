<?php
$module_name = 'dtbc_Contact_Roles';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'contacts_dtbc_contact_roles_1_name',
          ),
          1 => 
          array (
            'name' => 'opportunities_dtbc_contact_roles_1_name',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'role_c',
            'studio' => 'visible',
            'label' => 'LBL_ROLE',
          ),
          1 => 
          array (
            'name' => 'is_primary_c',
            'label' => 'LBL_IS_PRIMARY',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'email_c',
            'label' => 'LBL_EMAIL',
          ),
          1 => 
          array (
            'name' => 'phone_c',
            'label' => 'LBL_PHONE',
          ),
        ),
      ),
    ),
  ),
);
?>
