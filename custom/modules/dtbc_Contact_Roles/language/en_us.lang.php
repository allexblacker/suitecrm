<?php
// created: 2017-10-05 17:10:29
$mod_strings = array (
  'LBL_EMAIL' => 'Email',
  'LBL_PHONE' => 'Phone',
  'LBL_ROLE' => 'Role',
  'LBL_EDITVIEW_PANEL1' => 'Contact Details',
  'LBL_IS_PRIMARY' => 'Primary',
);