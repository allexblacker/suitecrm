<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-10-04 15:26:52
$dictionary["dtbc_Contact_Roles"]["fields"]["contacts_dtbc_contact_roles_1"] = array (
  'name' => 'contacts_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_dtbc_contact_roles_1contacts_ida',
);
$dictionary["dtbc_Contact_Roles"]["fields"]["contacts_dtbc_contact_roles_1_name"] = array (
  'name' => 'contacts_dtbc_contact_roles_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_dtbc_contact_roles_1contacts_ida',
  'link' => 'contacts_dtbc_contact_roles_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["dtbc_Contact_Roles"]["fields"]["contacts_dtbc_contact_roles_1contacts_ida"] = array (
  'name' => 'contacts_dtbc_contact_roles_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_contact_roles_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);


 // created: 2017-10-04 16:29:30
$dictionary['dtbc_Contact_Roles']['fields']['phone_c']['inline_edit']='1';
$dictionary['dtbc_Contact_Roles']['fields']['phone_c']['labelValue']='Phone';

 

// created: 2017-10-04 15:28:36
$dictionary["dtbc_Contact_Roles"]["fields"]["opportunities_dtbc_contact_roles_1"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_dtbc_contact_roles_1opportunities_ida',
);
$dictionary["dtbc_Contact_Roles"]["fields"]["opportunities_dtbc_contact_roles_1_name"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_dtbc_contact_roles_1opportunities_ida',
  'link' => 'opportunities_dtbc_contact_roles_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["dtbc_Contact_Roles"]["fields"]["opportunities_dtbc_contact_roles_1opportunities_ida"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_contact_roles_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);


 // created: 2017-10-05 17:10:29
$dictionary['dtbc_Contact_Roles']['fields']['is_primary_c']['inline_edit']='1';
$dictionary['dtbc_Contact_Roles']['fields']['is_primary_c']['labelValue']='Primary';

 

 // created: 2017-10-04 16:29:10
$dictionary['dtbc_Contact_Roles']['fields']['email_c']['inline_edit']='1';
$dictionary['dtbc_Contact_Roles']['fields']['email_c']['labelValue']='Email';

 

 // created: 2017-10-04 16:34:55
$dictionary['dtbc_Contact_Roles']['fields']['role_c']['inline_edit']='1';
$dictionary['dtbc_Contact_Roles']['fields']['role_c']['labelValue']='Role';

 
?>