<?php
	define('sugarEntry', true);
	sugar_chdir('../..');
	require_once('include/entryPoint.php');
	global $db;
	$db = DBManagerFactory::getInstance();
	$username = isset($_POST['username']) ? $_POST['username'] : '';
	$password = isset($_POST['password']) ? $_POST['password'] : '';
	if (!login($username, $password)) {
		if ($sessionId) {
			session_id($sessionId);
			session_start();
			if(empty($_SESSION['is_valid_session'])) {
				echo "Invalid session";
				exit;
			}
		} else {
			echo "Invalid username/password";
			exit;
		}
	}


	function login($username, $password)
	{
		global $server, $current_user, $sugar_config, $system_config, $db;
		$system_config = new Administration();
		$system_config->retrieveSettings('system');
		$user = new User();
		$result = $db->limitQuery('SELECT id, user_name, user_hash from users where user_name=\''.$username.'\'', 0, 1, false);
		$row = $db->fetchByAssoc($result);
		if($row)
		{
			if ($password && $row['user_hash'] === $password ||	crypt($password, $row['user_hash']) === $row['user_hash'])
			{
	
				$user->retrieve($row['id']);
				$current_user = $user;
				return true;
			}
			else
			{
				$GLOBALS['log']->fatal("SECURITY: failed attempted login for $username using SOAP api");
				return false;
			}
		}
		else
		{
			$GLOBALS['log']->fatal("SECURITY: Invalid user name specified: $username");
			return false;
		}
	}
