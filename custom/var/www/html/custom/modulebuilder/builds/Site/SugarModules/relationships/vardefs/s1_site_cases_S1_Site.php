<?php
// created: 2017-08-31 12:51:23
$dictionary["S1_Site"]["fields"]["s1_site_cases"] = array (
  'name' => 's1_site_cases',
  'type' => 'link',
  'relationship' => 's1_site_cases',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_CASES_FROM_CASES_TITLE',
);
