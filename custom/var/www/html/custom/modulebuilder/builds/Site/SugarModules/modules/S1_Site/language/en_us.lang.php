<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Site List',
  'LBL_MODULE_NAME' => 'Site',
  'LBL_MODULE_TITLE' => 'Site',
  'LBL_HOMEPAGE_TITLE' => 'My Site',
  'LNK_NEW_RECORD' => 'Create Site',
  'LNK_LIST' => 'View Site',
  'LNK_IMPORT_S1_SITE' => 'Import Site',
  'LBL_SEARCH_FORM_TITLE' => ' Site',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_S1_SITE_SUBPANEL_TITLE' => 'Site',
  'LBL_NEW_FORM_TITLE' => 'New Site',
  'Name' => 'Site Name',
  'Currency' => 'Currency',
  'Current_Software_Version' => 'Current Software Version',
  'default_panel_model' => 'default panel model',
  'Error_log' => 'Error log',
  'Existing_Site' => 'Existing Site',
  'field_magnitude' => 'field magnitude',
  'field_type' => 'field type',
  'First_Telemetry_Date' => 'First Telemetry Date',
  'guid' => 'guid',
  'installation_date' => 'installation date',
  'Installer' => 'Installer',
  'Internal_notes' => 'Internal notes',
  'is_public' => 'is public',
  'Last_reporting_time' => 'Last reporting time',
  'Last_Telemetry_Date' => 'Last Telemetry Date',
  'latitude' => 'latitude',
  'layout_file_name' => 'layout file name',
  'location_id' => 'location id',
  'longitude' => 'longitude',
  'Module_type' => 'Module type',
  'monitoring_account_id' => 'monitoring account id',
  'Monitoring_creation_time' => 'Monitoring creation time',
  'notes' => 'Monitoring Server Notes',
  'Monitoring_Site_ID' => 'Monitoring Site ID',
  'Monitoring_site_last_update' => 'Monitoring site last update',
  'Peak_power_kw_h_peak' => 'Peak power(kw/h peak)',
  'Phone_number' => 'Phone number',
  'public_modules' => 'public modules',
  'Service_instruction' => 'Service instruction',
  'Site_Additional_Info' => 'Site Additional Info',
  'Site_Address' => 'Site Address',
  'Site_City' => 'Site City',
  'Site_is_Monitored' => 'Site is Monitored',
  'Site_zip' => 'Site zip',
  'solar_field_id' => 'solar field id',
  'Special_Service_Instruction' => 'Special Service Instruction',
  'special_service_instruction' => 'Special Service Instruction',
  'Status' => 'Status',
  'url_name' => 'url name',
  'Workplan_Remarks' => 'Workplan Remarks',
  'Site_State' => 'Site State',
  'Site_Country' => 'Site Country',
  'Panel_Type' => 'Panel Type',
  'Roof_Type' => 'Roof Type',
  'LBL_DETAILVIEW_PANEL1' => 'Site Detail',
  'LBL_DETAILVIEW_PANEL2' => 'Address Information',
  'LBL_DETAILVIEW_PANEL3' => 'Additional Information',
  'LBL_EDITVIEW_PANEL2' => 'Site Detail',
  'LBL_EDITVIEW_PANEL1' => 'Address Information',
  'LBL_EDITVIEW_PANEL3' => 'Additional Information',
  'LBL_EDITVIEW_PANEL4' => 'System Information',
  'LBL_ACCOUNT_NAME_ACCOUNT_ID' => 'account name (related Account ID)',
  'Account_Name' => 'account name',
);