<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


class S1_Site extends Basic
{
    public $new_schema = true;
    public $module_dir = 'S1_Site';
    public $object_name = 'S1_Site';
    public $table_name = 's1_site';
    public $importable = true;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
    public $site_name;
    public $currency;
    public $current_software_version;
    public $default_panel_model;
    public $error_log;
    public $existing_site;
    public $field_magnitude;
    public $field_type;
    public $first_telemetry_date;
    public $guid;
    public $installation_date;
    public $installer;
    public $internal_notes;
    public $is_public;
    public $last_reporting_time;
    public $last_telemetry_date;
    public $latitude;
    public $layout_file_name;
    public $location_id;
    public $longitude;
    public $module_type;
    public $monitoring_account_id;
    public $monitoring_creation_time;
    public $monitoring_site_id;
    public $monitoring_site_last_update;
    public $peak_power_kw_h_peak;
    public $phone_number;
    public $public_modules;
    public $service_instruction;
    public $site_additional_info;
    public $site_address;
    public $site_city;
    public $site_is_monitored;
    public $site_zip;
    public $solar_field_id;
    public $special_service_instruction;
    public $status;
    public $url_name;
    public $workplan_remarks;
    public $notes;
    public $site_state;
    public $site_country;
    public $panel_type;
    public $roof_type;
	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }
	
}