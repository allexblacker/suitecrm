<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer ID',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_SECURITYGROUPS' => 'Sicherheitsgruppen',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Sicherheitsgruppen',
  'LBL_CREATED' => 'Erstellt von',
  'LBL_CREATED_ID' => 'Erstellt von Id',
  'LBL_CREATED_USER' => 'Erstellt von Benutzer',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Name',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von Id',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_MODIFIED_USER' => 'Geändert von Benutzer',
  'LBL_NAME' => 'Name',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_LIST_FORM_TITLE' => 'Serial Number Liste',
  'LBL_MODULE_NAME' => 'Serial Number',
  'LBL_MODULE_TITLE' => 'Serial Number',
  'LBL_HOMEPAGE_TITLE' => 'Mein Serial Number',
  'LNK_NEW_RECORD' => 'Erstellen Serial Number',
  'LNK_LIST' => 'Ansicht Serial Number',
  'LNK_IMPORT_SN1_SERIAL_NUMBER' => 'Import Serial Number',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Serial Number',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf anzeigen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten',
  'LBL_SN1_SERIAL_NUMBER_SUBPANEL_TITLE' => 'Serial Number',
  'LBL_NEW_FORM_TITLE' => 'Neue Serial Number',
  'Fault_Description' => 'Fault Description',
  'Alternative_iBolt_Status' => 'Alternative iBolt Status',
  'Attaching_Board_config_file_is_mandatory' => 'Attaching Board config file is mandatory',
  'BSUF_file_is_attached' => 'BSUF file is attached',
  'CE_ServiceAdmin_for_WFs' => 'CE ServiceAdmin for WFs',
  'CRD' => 'CRD',
  'details_for_fault_ategory_other' => 'Details for Fault sub-category - Other',
  'Digital_PCB_Version' => 'Digital PCB Version',
  'Distributor_Name' => 'Distributor Name',
  'Distributor_Number' => 'Distributor Number',
  'DOA' => 'DOA',
  'DOA_Type' => 'DOA Type',
  'End_of_RMA' => 'End of RMA',
  'Exception' => 'Exception',
  'FA_By_air' => 'FA By air',
  'FA_Date' => 'FA Date',
  'FA_Fault' => 'FA Fault',
  'Failed_Board' => 'Failed Board',
  'Failed_Item_SN' => 'Failed Item SN',
  'FA_is_done' => 'FA is done',
  'Family' => 'Family',
  'Fault_Category' => 'Fault Category',
  'Fault_sub_category' => 'Fault sub category',
  'Gen' => 'Gen',
  'iBolt_Status' => 'iBolt Status',
  'Inv_assy_AC_DC_conduit' => 'Inv assy AC/DC conduit	',
  'Inv_assy_Heat_Sink' => 'Inv assy Heat-Sink',
  'Manufacturing_Related_Fault' => 'Manufacturing Related Fault',
  'M40' => 'Minus 40	',
  'Model' => 'Model',
  'Need_FA_immediate_return' => 'Need FA immediate return',
  'Need_Immediate_Return_Time_Stamp' => 'Need Immediate Return TimeStamp',
  'Note' => 'Note',
  'Part_Description' => 'Part Description',
  'Product_New' => 'Part Number (new)',
  'Product_old' => 'Product old',
  'Part_Number' => 'Part Number (old) n',
  'Portia_Memory' => 'Portia Memory',
  'QA_Employee_Name' => 'QA Employee Name',
  'RCA' => 'RCA?',
  'reason_for_not_in_warranty' => 'reason for not in warranty',
  'Replaced_Type' => 'Replaced Type',
  'Failure_Type' => 'Failure Type',
  'Return_asked_by' => 'Return asked by',
  'RMA' => 'RMA',
  'RMA_in_ERP' => 'RMA # in ERP',
  'RMA_Product' => 'RMA Product',
  'RML' => 'RML #',
  'Serial_Number_new' => 'Serial Number (new)',
  'Warrant_Extension' => 'Warrant Extension?',
  'Warranty_End_Date' => 'Warranty End Date',
  'Warranty_Extension' => 'Warranty Extension?',
  'LBL_CASE1' => 'case1',
  'LBL_CASE1_ACASE_ID' => 'Case (related Case ID)',
  'Case1' => 'Case',
);