<?php
// created: 2017-10-18 12:46:47
$dictionary["p1_project_tasks_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'p1_project_tasks_1' => 
    array (
      'lhs_module' => 'P1_Project',
      'lhs_table' => 'p1_project',
      'lhs_key' => 'id',
      'rhs_module' => 'Tasks',
      'rhs_table' => 'tasks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'p1_project_tasks_1_c',
      'join_key_lhs' => 'p1_project_tasks_1p1_project_ida',
      'join_key_rhs' => 'p1_project_tasks_1tasks_idb',
    ),
  ),
  'table' => 'p1_project_tasks_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'p1_project_tasks_1p1_project_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'p1_project_tasks_1tasks_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'p1_project_tasks_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'p1_project_tasks_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'p1_project_tasks_1p1_project_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'p1_project_tasks_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'p1_project_tasks_1tasks_idb',
      ),
    ),
  ),
);