<?php
// created: 2017-11-01 02:12:01
$dictionary["os1_outside_sales_activity_tasks_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'os1_outside_sales_activity_tasks_1' => 
    array (
      'lhs_module' => 'OS1_Outside_Sales_Activity',
      'lhs_table' => 'os1_outside_sales_activity',
      'lhs_key' => 'id',
      'rhs_module' => 'Tasks',
      'rhs_table' => 'tasks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'os1_outside_sales_activity_tasks_1_c',
      'join_key_lhs' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
      'join_key_rhs' => 'os1_outside_sales_activity_tasks_1tasks_idb',
    ),
  ),
  'table' => 'os1_outside_sales_activity_tasks_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'os1_outside_sales_activity_tasks_1tasks_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'os1_outside_sales_activity_tasks_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'os1_outside_sales_activity_tasks_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'os1_outside_sales_activity_tasks_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'os1_outside_sales_activity_tasks_1tasks_idb',
      ),
    ),
  ),
);