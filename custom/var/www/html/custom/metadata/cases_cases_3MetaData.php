<?php
// created: 2017-09-15 13:18:31
$dictionary["cases_cases_3"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'cases_cases_3' => 
    array (
      'lhs_module' => 'Cases',
      'lhs_table' => 'cases',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_cases_3_c',
      'join_key_lhs' => 'cases_cases_3cases_ida',
      'join_key_rhs' => 'cases_cases_3cases_idb',
    ),
  ),
  'table' => 'cases_cases_3_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cases_cases_3cases_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cases_cases_3cases_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cases_cases_3spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cases_cases_3_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_cases_3cases_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cases_cases_3_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_cases_3cases_idb',
      ),
    ),
  ),
);