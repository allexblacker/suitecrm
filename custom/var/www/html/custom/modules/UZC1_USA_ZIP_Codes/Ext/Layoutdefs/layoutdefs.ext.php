<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-08-31 12:53:12
$layout_defs["UZC1_USA_ZIP_Codes"]["subpanel_setup"]['uzc1_usa_zip_codes_leads'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_UZC1_USA_ZIP_CODES_LEADS_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'uzc1_usa_zip_codes_leads',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-08-31 12:53:12
$layout_defs["UZC1_USA_ZIP_Codes"]["subpanel_setup"]['uzc1_usa_zip_codes_cases'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_CASES_TITLE',
  'get_subpanel_data' => 'uzc1_usa_zip_codes_cases',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>