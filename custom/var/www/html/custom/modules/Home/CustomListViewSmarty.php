<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/ListView/ListViewSmarty.php');

class CustomListViewSmarty extends ListViewSmarty {
	public function setup($seed, $file, $where, $params = array(), $offset = 0, $limit = -1,  $filter_fields = array(), $id_field = 'id', $id = null) {
        $this->should_process = true;
        if(isset($seed->module_dir) && !$this->shouldProcess($seed->module_dir)){
        		return false;
        }
        if(isset($params['export'])) {
          $this->export = $params['export'];
        }
        if(!empty($params['multiSelectPopup'])) {
		  $this->multi_select_popup = $params['multiSelectPopup'];
        }
		if(!empty($params['massupdate']) && $params['massupdate'] != false) {
			$this->show_mass_update_form = true;
			$this->mass = $this->getMassUpdate();
			$this->mass->setSugarBean($seed);
			if(!empty($params['handleMassupdate']) || !isset($params['handleMassupdate'])) {
                $this->mass->handleMassUpdate();
            }
		}
		$this->seed = $seed;

        $filter_fields = $this->setupFilterFields($filter_fields);

		foreach ($where as $inputWhere) {
			$temp = $this->lvd->getListViewData($seed, $inputWhere, $offset, $limit, $filter_fields, $params, $id_field, true, $id);
			$data = $this->mergeData($temp, $data);
		}

        $this->fillDisplayColumnsWithVardefs();

		$this->process($file, $data, $seed->object_name);
		return true;
	}
	
	private function mergeData($newData, $oldData) {
		if (empty($oldData))
			return $newData;
		
		$retval = $oldData;
		$pageDataIdx = 0;
		
		foreach ($newData['data'] as $newDataInput) {
			$found = false;
			foreach ($retval['data'] as $oldData) {
				if ($oldData['ID'] == $newDataInput['ID']) {
					$found = true;
					break;
				}					
			}
			if (!$found) {
				$retval['data'][] = $newDataInput;
				$retval['pageData']['idIndex'][$newDataInput['ID']] = array(
					"0" => count($retval['data']) - 1,
				);
				$retval['pageData']['offsets']['total']++;
				$retval['pageData']['rowAccess'][] = $newData['pageData']['rowAccess'][$pageDataIdx];
				$retval['pageData']['tag'][] = $newData['pageData']['tag'][$pageDataIdx];
			}
			
			$pageDataIdx++;
		}
		
		return $retval;
	}
}

?>
