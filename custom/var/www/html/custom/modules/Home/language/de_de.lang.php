<?php

$mod_strings['LBL_ABOUT'] = 'Über';
$mod_strings['LBL_ABOUT_SUITE'] = 'Über SuiteCRM';
$mod_strings['LBL_ABOUT_SUITE_1'] = 'SuiteCRM ist eine Abspaltung von SugarCRM. Sie finden im Web viele Artikel, die beschreiben warum diese Abspaltung notwendig wurde.';
$mod_strings['LBL_ABOUT_SUITE_2'] = 'SuiteCRM ist unter der Open Source Lizenz GPL3 veröffentlicht.';
$mod_strings['LBL_ABOUT_SUITE_3'] = 'SuiteCRM ist kompatibel mit SugarCRM 6.5. X';
$mod_strings['LBL_ABOUT_SUITE_4'] = 'Der gesamte Code von SuiteCRM wird als Open Source unter GPL3 veröffentlicht.';
$mod_strings['LBL_ABOUT_SUITE_5'] = 'SuiteCRM Support ist in einer freien und in einer bezahlten Variante erhältlich.';
$mod_strings['LBL_CONTRIBUTORS'] = 'Beitragende';
$mod_strings['LBL_CONTRIBUTOR_CONSCIOUS'] = 'SuiteCRM LOGO von Conscious Solutions';
$mod_strings['LBL_CONTRIBUTOR_JJW_GMAPS'] = 'JJWDesign Google Maps von Jeffrey J. Walters';
$mod_strings['LBL_CONTRIBUTOR_QUICKCRM'] = 'QuickCRM Mobile von Benoit Luquet';
$mod_strings['LBL_CONTRIBUTOR_RESPONSETAP'] = 'Beitrag zu SuiteCRM Release 7.3 von ResponseTap';
$mod_strings['LBL_CONTRIBUTOR_SECURITY_SUITE'] = 'SecuritySuite von Jason Eggers';
$mod_strings['LBL_CONTRIBUTOR_SUITECRM'] = 'Die kostenlose Open-Source-Alternative zur SugarCRM Professional Edition.';
$mod_strings['LBL_FEATURING'] = 'AOS, AOW, AOR, AOP, AOE und Wiedervorlagemodul von SalesAgility.';
$mod_strings['LBL_LANGUAGE_RUSSIAN'] = 'Russische Übersetzung von likhobory';
$mod_strings['LBL_LANGUAGE_SPANISH'] = 'Spanische Übersetzung von Disytel openConsulting';
$mod_strings['LBL_PARTNERS'] = 'Partner';
$mod_strings['LBL_SUITE_PARTNERS'] = 'Für eine komplette Liste unserer Partner gehen Sie bitte auf unserer Webseite.';
