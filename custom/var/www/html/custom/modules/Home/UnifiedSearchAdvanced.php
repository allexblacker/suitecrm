<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once('modules/Home/UnifiedSearchAdvanced.php');

class CustomUnifiedSearchAdvanced extends UnifiedSearchAdvanced {

	private function getCustomModules($unified_search_modules) {
		$retval = $unified_search_modules;
		
		// Custom modules
		$retval['SN1_Serial_Number'] = array(
			'fields' => array(
				'name' => array(
					'query_type' => 'default',
				),
				'serial_number_new' => array(
					'query_type' => 'default',
				),
				'product_old' => array(
					'query_type' => 'default',
				),
			),
			'default' => true,
		);
		$retval['S1_Site'] = array(
			'fields' => array(
				'name' => array(
					'query_type' => 'default',
				),
			),
			'default' => true,
		);
		
		// Core modules
		$retval['Cases']['fields']['rma_in_erp_c'] = array( 'query_type'=>'default');
		$retval['Cases']['fields']['tier_2_region_c'] = array( 'query_type'=>'default');
		$retval['Cases']['fields']['tier_2_assignee_c'] = array( 'query_type'=>'default');
		$retval['Opportunities']['fields']['oppnum_c'] = array( 'query_type'=>'default');
		
		return $retval;
	}
	
	private function getCustomModulesForDisplay($unified_search_modules_display) {
		$retval = $unified_search_modules_display;
		$retval['SN1_Serial_Number'] = array(
			'visible' => 1,
		);
		$retval['S1_Site'] = array(
			'visible' => 1,
		);
		return $retval;
	}
	
	private function getCustomModulesForSearch($modules_to_search) {
		$retval = $modules_to_search;
		
		$retval['SN1_Serial_Number'] = 'SN1_Serial_Number';
		$retval['S1_Site'] = 'S1_Site';
		
		return $retval;
	}

	function search() {

        $unified_search_modules = $this->getUnifiedSearchModules();
		$unified_search_modules_display = $this->getUnifiedSearchModulesDisplay();

		$unified_search_modules = $this->getCustomModules($unified_search_modules);
		$unified_search_modules_display = $this->getCustomModulesForDisplay($unified_search_modules_display);
		
		require_once 'custom/modules/Home/CustomListViewSmarty.php';

		global $modListHeader, $beanList, $beanFiles, $current_language, $app_strings, $current_user, $mod_strings;
		$home_mod_strings = return_module_language($current_language, 'Home');

		$this->query_string = securexss(from_html(clean_string($this->query_string, 'UNIFIED_SEARCH')));
		$modules_to_search = array();
		$modules_to_search = $this->getCustomModulesForSearch($modules_to_search);

		if(!empty($_REQUEST['advanced']) && $_REQUEST['advanced'] != 'false') {
			
			if(!empty($_REQUEST['search_modules']))
			{
			    foreach(explode (',', $_REQUEST['search_modules'] ) as $key)
	            {
                    if (isset($unified_search_modules_display[$key]) && !empty($unified_search_modules_display[$key]['visible']))
                    {
                        $modules_to_search[$key] = $beanList[$key];
                    }
	            }
			}

			$current_user->setPreference('showGSDiv', isset($_REQUEST['showGSDiv']) ? $_REQUEST['showGSDiv'] : 'no', 0, 'search');
			$current_user->setPreference('globalSearch', $modules_to_search, 0, 'search'); // save selections to user preference
		} else {
			$users_modules = $current_user->getPreference('globalSearch', 'search');

			if(!empty($users_modules)) {
				// use user's previous selections
			    foreach ( $users_modules as $key => $value ) {
			    	if (isset($unified_search_modules_display[$key]) && !empty($unified_search_modules_display[$key]['visible'])) {
		            	$modules_to_search[$key] = $beanList[$key];
		        	}
			    }
			} else {
				foreach($unified_search_modules_display as $module=>$data) {
				    if (!empty($data['visible']) ) {
				        $modules_to_search[$module] = $beanList[$module];
				    }
				}
			}
			$current_user->setPreference('globalSearch', $modules_to_search, 'search');
		}


		$templateFile = 'modules/Home/UnifiedSearchAdvancedForm.tpl';
		if(file_exists('custom/' . $templateFile))
		{
		   $templateFile = 'custom/'.$templateFile;
		}

		echo $this->getDropDownDiv($templateFile);

		$module_results = array();
		$module_counts = array();
		$has_results = false;

		if(!empty($this->query_string)) {
			foreach($modules_to_search as $moduleName => $beanName) {
                require_once $beanFiles[$beanName] ;
                $seed = new $beanName();

                $lv = new CustomListViewSmarty();
                $lv->lvd->additionalDetails = false;
                $mod_strings = return_module_language($current_language, $seed->module_dir);

                //retrieve the original list view defs and store for processing in case of custom layout changes
                require('modules/'.$seed->module_dir.'/metadata/listviewdefs.php');
				$orig_listViewDefs = $listViewDefs;

                if(file_exists('custom/modules/'.$seed->module_dir.'/metadata/listviewdefs.php'))
                {
                    require('custom/modules/'.$seed->module_dir.'/metadata/listviewdefs.php');
                }

                if ( !isset($listViewDefs) || !isset($listViewDefs[$seed->module_dir]) )
                {
                    continue;
                }

			    $unifiedSearchFields = array () ;
                $innerJoins = array();
                foreach ( $unified_search_modules[ $moduleName ]['fields'] as $field=>$def )
                {
                	$listViewCheckField = strtoupper($field);
                	//check to see if the field is in listview defs
					if ( empty($listViewDefs[$seed->module_dir][$listViewCheckField]['default']) ) {
						//check to see if field is in original list view defs (in case we are using custom layout defs)
						if (!empty($orig_listViewDefs[$seed->module_dir][$listViewCheckField]['default']) ) {
							//if we are here then the layout has been customized, but the field is still needed for query creation
							$listViewDefs[$seed->module_dir][$listViewCheckField] = $orig_listViewDefs[$seed->module_dir][$listViewCheckField];
						}

					}

                    //bug: 34125 we might want to try to use the LEFT JOIN operator instead of the INNER JOIN in the case we are
                    //joining against a field that has not been populated.
                    if(!empty($def['innerjoin']) )
                    {
                        if (empty($def['db_field']) )
                        {
                            continue;
                        }
                        $innerJoins[$field] = $def;
                        $def['innerjoin'] = str_replace('INNER', 'LEFT', $def['innerjoin']);
                    }

                    if(isset($seed->field_defs[$field]['type']))
                    {
                        $type = $seed->field_defs[$field]['type'];
                        if($type == 'int' && !is_numeric($this->query_string))
                        {
                            continue;
                        }
                    }

                    $unifiedSearchFields[ $moduleName ] [ $field ] = $def ;
                    $unifiedSearchFields[ $moduleName ] [ $field ][ 'value' ] = $this->query_string;
                }

                /*
                 * Use searchForm2->generateSearchWhere() to create the search query, as it can generate SQL for the full set of comparisons required
                 * generateSearchWhere() expects to find the search conditions for a field in the 'value' parameter of the searchFields entry for that field
                 */
                require_once $beanFiles[$beanName] ;
                $seed = new $beanName();
                
				require_once $this->searchFormPath;
                $searchForm = new $this->searchFormClass ( $seed, $moduleName ) ;

                $searchForm->setup (array ( $moduleName => array() ) , $unifiedSearchFields , '' , 'saved_views' /* hack to avoid setup doing further unwanted processing */ ) ;
                $where_clauses = $searchForm->generateSearchWhere() ;
				
                //add inner joins back into the where clause
                $params = array('custom_select' => "");
                foreach($innerJoins as $field=>$def) {
                    if (isset($def['db_field'])) {
                      foreach($def['db_field'] as $dbfield)
                          $where_clauses[] = $dbfield . " LIKE '" . $GLOBALS['db']->quote($this->query_string) . "%'";
                          $params['custom_select'] .= ", $dbfield";
                          $params['distinct'] = true;
                          //$filterFields[$dbfield] = $dbfield;
                    }
                }

                $displayColumns = array();
                foreach($listViewDefs[$seed->module_dir] as $colName => $param)
                {
                    if(!empty($param['default']) && $param['default'] == true)
                    {
                        $param['url_sort'] = true;//bug 27933
                        $displayColumns[$colName] = $param;
                    }
                }

                if(count($displayColumns) > 0)
                {
                	$lv->displayColumns = $displayColumns;
                } else {
                	$lv->displayColumns = $listViewDefs[$seed->module_dir];
                }

                $lv->export = false;
                $lv->mergeduplicates = false;
                $lv->multiSelect = false;
                $lv->delete = false;
                $lv->select = false;
                $lv->showMassupdateFields = false;
                $lv->email = false;
				
				$where = count($where_clauses) > 0 ? $where_clauses : '';
				
                $lv->setup($seed, 'include/ListView/ListViewNoMassUpdate.tpl', $this->handleDropDowns($seed, $where), $params, 0, 10);

                $module_results[$moduleName] = '<br /><br />' . get_form_header($GLOBALS['app_list_strings']['moduleList'][$seed->module_dir] . ' (' . $lv->data['pageData']['offsets']['total'] . ')', '', false);
                $module_counts[$moduleName] = $lv->data['pageData']['offsets']['total'];

                if($lv->data['pageData']['offsets']['total'] == 0) {
                    //$module_results[$moduleName] .= "<li class='noBullet' id='whole_subpanel_{$moduleName}'><div id='div_{$moduleName}'><h2>" . $home_mod_strings['LBL_NO_RESULTS_IN_MODULE'] . '</h2></div></li>';
                    $module_results[$moduleName] .= '<h2>' . $home_mod_strings['LBL_NO_RESULTS_IN_MODULE'] . '</h2>';
                } else {
                    $has_results = true;
                    //$module_results[$moduleName] .= "<li class='noBullet' id='whole_subpanel_{$moduleName}'><div id='div_{$moduleName}'>" . $lv->display(false, false) . '</div></li>';
                    $module_results[$moduleName] .= $lv->display(false, false);
                }

			}
		}

		if($has_results) {
			foreach($module_counts as $name=>$value) {
				echo $module_results[$name];
			}
		} else if(empty($_REQUEST['form_only'])) {
			echo $home_mod_strings['LBL_NO_RESULTS'];
			echo $home_mod_strings['LBL_NO_RESULTS_TIPS'];
		}

	}
	
	private function handleDropDowns($bean, $where) {
		if (!is_array($where))
			return $where;
		
		$retval = $where;
		
		for ($i = 0; $i < count($retval); $i++) {
			$q = $retval[$i];
			if (strpos($q, "tier_2_region_c") !== false) {
				$retval[$i] = $this->getInClause('tier_2_region_list', 'cases_cstm.tier_2_region_c', $retval[$i]);
			}
			if (strpos($q, "tier_2_assignee_c") !== false) {
				$retval[$i] = $this->getInClause('tier_2_assignee_list', 'cases_cstm.tier_2_assignee_c', $retval[$i]);
			}
		}

		return $retval;
	}
	
	private function getInClause($listName, $fieldName, $originalValue) {
		global $app_list_strings, $db;
		$temp = "";
		foreach ($app_list_strings[$listName] as $key => $value) {
			if (strpos($value, $this->query_string) !== false) {
				if (strlen($temp) > 0)
					$temp .= ',';
				$temp .= $db->quoted($key);
			}
		}
		if (strlen($temp) > 0) {
			return $fieldName . " IN (" . $temp . ")";
		}
		
		return $originalValue;
	}

}
