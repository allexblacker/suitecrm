<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-21 15:43:03
$dictionary['dtbc_Year_Alliance_Points']['fields']['expired_points_c']['inline_edit']='1';
$dictionary['dtbc_Year_Alliance_Points']['fields']['expired_points_c']['labelValue']='Expired Points';

 

 // created: 2017-09-21 15:42:03
$dictionary['dtbc_Year_Alliance_Points']['fields']['kw_installed_c']['inline_edit']='1';
$dictionary['dtbc_Year_Alliance_Points']['fields']['kw_installed_c']['labelValue']='KW Installed';

 

 // created: 2017-10-12 11:02:10
$dictionary['dtbc_Year_Alliance_Points']['fields']['last_calculation_time_c']['inline_edit']='';
$dictionary['dtbc_Year_Alliance_Points']['fields']['last_calculation_time_c']['labelValue']='Last Calculation Time';

 

 // created: 2017-09-21 15:42:48
$dictionary['dtbc_Year_Alliance_Points']['fields']['available_points_c']['inline_edit']='1';
$dictionary['dtbc_Year_Alliance_Points']['fields']['available_points_c']['labelValue']='Available Points';

 

// created: 2017-09-21 16:32:04
$dictionary["dtbc_Year_Alliance_Points"]["fields"]["accounts_dtbc_year_alliance_points_1"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1',
  'type' => 'link',
  'relationship' => 'accounts_dtbc_year_alliance_points_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_dtbc_year_alliance_points_1accounts_ida',
);
$dictionary["dtbc_Year_Alliance_Points"]["fields"]["accounts_dtbc_year_alliance_points_1_name"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_dtbc_year_alliance_points_1accounts_ida',
  'link' => 'accounts_dtbc_year_alliance_points_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["dtbc_Year_Alliance_Points"]["fields"]["accounts_dtbc_year_alliance_points_1accounts_ida"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_dtbc_year_alliance_points_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE',
);


 // created: 2017-09-21 15:42:34
$dictionary['dtbc_Year_Alliance_Points']['fields']['gift_value_c']['inline_edit']='1';
$dictionary['dtbc_Year_Alliance_Points']['fields']['gift_value_c']['labelValue']='Gift Value';

 

 // created: 2017-09-21 15:43:32
$dictionary['dtbc_Year_Alliance_Points']['fields']['date_expiration_c']['inline_edit']='1';
$dictionary['dtbc_Year_Alliance_Points']['fields']['date_expiration_c']['labelValue']='Date Expiration';

 

 // created: 2017-09-25 16:07:24
$dictionary['dtbc_Year_Alliance_Points']['fields']['year_c']['inline_edit']='';
$dictionary['dtbc_Year_Alliance_Points']['fields']['year_c']['labelValue']='Year';

 

 // created: 2017-09-22 17:04:20
$dictionary['dtbc_Year_Alliance_Points']['fields']['name']['inline_edit']=true;
$dictionary['dtbc_Year_Alliance_Points']['fields']['name']['duplicate_merge']='disabled';
$dictionary['dtbc_Year_Alliance_Points']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['dtbc_Year_Alliance_Points']['fields']['name']['merge_filter']='disabled';
$dictionary['dtbc_Year_Alliance_Points']['fields']['name']['unified_search']=false;

 

 // created: 2017-09-21 15:42:19
$dictionary['dtbc_Year_Alliance_Points']['fields']['total_points_c']['inline_edit']='1';
$dictionary['dtbc_Year_Alliance_Points']['fields']['total_points_c']['labelValue']='Total Points';

 
?>