<?php
$module_name = 'dtbc_Year_Alliance_Points';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'available_points_c',
            'label' => 'LBL_AVAILABLE_POINTS',
          ),
          1 => 
          array (
            'name' => 'accounts_dtbc_year_alliance_points_1_name',
            'label' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_ACCOUNTS_TITLE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'total_points_c',
            'label' => 'LBL_TOTAL_POINTS',
          ),
          1 => 
          array (
            'name' => 'gift_value_c',
            'label' => 'LBL_GIFT_VALUE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'date_expiration_c',
            'label' => 'LBL_DATE_EXPIRATION',
          ),
          1 => 
          array (
            'name' => 'expired_points_c',
            'label' => 'LBL_EXPIRED_POINTS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'last_calculation_time_c',
            'label' => 'LBL_LAST_CALCULATION_TIME',
          ),
          1 => 
          array (
            'name' => 'kw_installed_c',
            'label' => 'LBL_KW_INSTALLED',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
      ),
    ),
  ),
);
?>
