<?php
$module_name = 'AP1_Alternative_Part';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'alternative_part_name',
            'label' => 'Name',
          ),
          1 => 
          array (
            'name' => 'serial_number_c',
            'studio' => 'visible',
            'label' => 'LBL_SERIAL_NUMBER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'bin',
            'label' => 'Bin',
          ),
          1 => 
          array (
            'name' => 'stock_level',
            'label' => 'Stock_Level',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'priority',
            'label' => 'Priority',
          ),
          1 => 
          array (
            'name' => 'warehouse',
            'label' => 'Warehouse',
          ),
        ),
      ),
    ),
  ),
);
?>
