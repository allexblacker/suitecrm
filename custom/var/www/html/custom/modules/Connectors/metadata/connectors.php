<?php
// created: 2017-11-25 12:45:27
$connectors = array (
  'ext_rest_facebook' => 
  array (
    'id' => 'ext_rest_facebook',
    'name' => 'Facebook',
    'enabled' => true,
    'directory' => 'modules/Connectors/connectors/sources/ext/rest/facebook',
    'eapm' => false,
    'modules' => 
    array (
      0 => 'Accounts',
      1 => 'Contacts',
      2 => 'Leads',
    ),
  ),
  'ext_rest_insideview' => 
  array (
    'id' => 'ext_rest_insideview',
    'name' => 'InsideView&#169;',
    'enabled' => true,
    'directory' => 'modules/Connectors/connectors/sources/ext/rest/insideview',
    'eapm' => false,
    'modules' => 
    array (
      0 => 'Accounts',
      1 => 'Contacts',
      2 => 'Leads',
      3 => 'Opportunities',
    ),
  ),
  'ext_rest_twitter' => 
  array (
    'id' => 'ext_rest_twitter',
    'name' => 'Twitter',
    'enabled' => true,
    'directory' => 'modules/Connectors/connectors/sources/ext/rest/twitter',
    'eapm' => false,
    'modules' => 
    array (
      0 => 'Accounts',
      1 => 'Contacts',
      2 => 'Leads',
    ),
  ),
  'ext_rest_acton' => 
  array (
    'id' => 'ext_rest_acton',
    'name' => 'Act-On Integrator',
    'enabled' => true,
    'directory' => 'custom/modules/Connectors/connectors/sources/ext/rest/acton',
    'eapm' => 
    array (
      'enabled' => true,
    ),
    'modules' => 
    array (
    ),
  ),
);