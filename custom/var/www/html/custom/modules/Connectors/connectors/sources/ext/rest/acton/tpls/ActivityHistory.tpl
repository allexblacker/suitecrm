{*

*}
<script type="text/javascript">
function getActOnEmail()
{ldelim}
	YAHOO.util.Connect.asyncRequest('GET', 'index.php?module=Connectors&action=CallConnectorFunc&source_id=ext_rest_acton&source_func=actonSessionToken', {ldelim} success: getActOnSessionTokenEmailCallback, failure: getActOnSessionTokenEmailFailure {rdelim}, null);
{rdelim}

function getActOnSessionTokenEmailCallback(msg)
{ldelim}
	buttonLaunchSendEmail(msg.responseText.replace(/\"/g, ""));
{rdelim}

function getActOnSessionTokenEmailFailure(msg)
{ldelim}
{rdelim}

function getActOnHistory()
{ldelim}
	YAHOO.util.Connect.asyncRequest('GET', 'index.php?module=Connectors&action=CallConnectorFunc&source_id=ext_rest_acton&source_func=actonSessionToken', {ldelim} success: getActOnSessionTokenHistoryCallback, failure: getActOnSessionTokenHistoryFailure {rdelim}, null);
{rdelim}

function getActOnSessionTokenHistoryCallback(msg)
{ldelim}
	showActivityHistorySubPanel(msg.responseText.replace(/\"/g, ""));
{rdelim}

function getActOnSessionTokenHistoryFailure(msg)
{ldelim}
{rdelim}

function showActivityHistorySubPanel(sessionToken)
{ldelim}
	document.getElementById('actonActivityHistoryFrame').src = '{$REMOTE_URL}' + '&sessionToken=' + encodeURIComponent(sessionToken);
    document.getElementById('hide_link_acton').style.display='block';
    document.getElementById('show_link_acton').style.display='none';
    showSubPanel('activityhistory');
{rdelim}

function hideActivityHistorySubPanel()
{ldelim}
	document.getElementById('actonActivityHistoryFrame').src = "";
    document.getElementById('hide_link_acton').style.display='none';
    document.getElementById('show_link_acton').style.display='block';
    hideSubPanel('activityhistory');
{rdelim}

function buttonLaunchSendEmail(sessionToken)
{ldelim}
	window.open('{$EMAIL_URL}' + '&sessionToken=' + encodeURIComponent(sessionToken));
{rdelim}
</script>
<div id='actonActivityHistory' style='width:100%' class="doNotPrint">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="formHeader h3Row">
        <tbody>
            <tr>
                <td align="left" valign="top">
					<span id="show_link_acton" style="display: block; padding-top: 5px">
						<a class="utilsLink" href="#" onclick="current_child_field = 'activityhistory';markSubPanelLoaded('activityhistory');getActOnHistory();return false;">{sugar_getimage name='advanced_search' attr='border="0" align="absmiddle"' ext='.gif' alt=$APP.LBL_SHOW }</a>
					</span>
					<span id="hide_link_acton" style="display: none; padding-top: 5px">
						<a class="utilsLink" href="#" onclick="hideActivityHistorySubPanel();return false;">{sugar_getimage name='basic_search' attr='border="0" align="absmiddle"' ext='.gif' alt=$APP.LBL_HIDE }</a>
					</span>
                </td>
                <td align="left" valign="top">
					<h3><nobr>Act-On Integrator</nobr></h3>
				</td>
                <td width="100%" align="left">
					<div id="subpanel_activityhistory" style="display: none">
						<div style="padding-top: 5px;"><a href="#" onclick="getActOnEmail();">Send an Act-On Email</a></div>
						<iframe id="actonActivityHistoryFrame" src="" style="border: 0px; width: 900px; height: 345px"></iframe>
					</div>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" colspan="3">
				<!-- Static Buttons Go Here -->
				</td>
            </tr>
        </tbody>
    </table>
</div>