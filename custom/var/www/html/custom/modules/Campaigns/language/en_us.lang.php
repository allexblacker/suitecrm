<?php
// created: 2017-11-16 15:26:42
$mod_strings = array (
  'LBL_EDITVIEW_PANEL3' => 'Planning',
  'LBL_EDITVIEW_PANEL4' => 'Description Information',
  'LBL_DETAILVIEW_PANEL3' => 'Description Information',
  'LBL_DETAILVIEW_PANEL4' => 'Planning',
  'LBL_OPPORTUNITY_SUBPANEL_TITLE' => 'Opportunities',
  'LBL_CAMPAIGN_NAME' => 'Name:',
  'LBL_CAMPAIGN_STATUS' => 'Status: ',
  'LBL_CAMPAIGN_TYPE' => 'Type: ',
);