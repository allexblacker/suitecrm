<?php
$module_name = 'dtbc_Login_Logger';
$listViewDefs [$module_name] = 
array (
  'LOGIN_DATE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_LOGIN_DATE',
    'width' => '10%',
  ),
  'LOGGED_USER_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_LOGGED_USER',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'USERID_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_USERID',
    'width' => '10%',
  ),
  'USERNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_USERNAME',
    'width' => '10%',
  ),
  'IP_ADDRESS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_IP_ADDRESS',
    'width' => '10%',
  ),
);
?>
