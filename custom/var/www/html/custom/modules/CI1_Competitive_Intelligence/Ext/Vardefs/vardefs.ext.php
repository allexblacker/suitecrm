<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-11-03 23:51:03

 

// created: 2017-10-24 19:32:31
$dictionary["CI1_Competitive_Intelligence"]["fields"]["accounts_ci1_competitive_intelligence_1"] = array (
  'name' => 'accounts_ci1_competitive_intelligence_1',
  'type' => 'link',
  'relationship' => 'accounts_ci1_competitive_intelligence_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_ci1_competitive_intelligence_1accounts_ida',
);
$dictionary["CI1_Competitive_Intelligence"]["fields"]["accounts_ci1_competitive_intelligence_1_name"] = array (
  'name' => 'accounts_ci1_competitive_intelligence_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_ci1_competitive_intelligence_1accounts_ida',
  'link' => 'accounts_ci1_competitive_intelligence_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CI1_Competitive_Intelligence"]["fields"]["accounts_ci1_competitive_intelligence_1accounts_ida"] = array (
  'name' => 'accounts_ci1_competitive_intelligence_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_ci1_competitive_intelligence_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE',
);

?>