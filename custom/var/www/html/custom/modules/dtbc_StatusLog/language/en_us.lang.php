<?php
// created: 2017-10-11 20:02:12
$mod_strings = array (
  'LBL_USERID' => 'User ID',
  'LBL_CASE_STATUS' => 'Case status',
  'LBL_CASE_SUB_STATUS' => 'Case sub-status',
  'LBL_DATE_START' => 'Date start',
  'LBL_DATE_END' => 'Date end',
  'LBL_TIME_SPENT' => 'Time spent',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_NAME' => 'Name',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_DATE_ENTERED' => 'Date Created',
);