<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-11 19:58:55
$dictionary['dtbc_StatusLog']['fields']['case_sub_status_c']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['case_sub_status_c']['labelValue']='Case sub-status';

 

// created: 2017-09-07 14:41:36
$dictionary["dtbc_StatusLog"]["fields"]["cases_dtbc_statuslog_1"] = array (
  'name' => 'cases_dtbc_statuslog_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_statuslog_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_CASES_TITLE',
  'id_name' => 'cases_dtbc_statuslog_1cases_ida',
);
$dictionary["dtbc_StatusLog"]["fields"]["cases_dtbc_statuslog_1_name"] = array (
  'name' => 'cases_dtbc_statuslog_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_dtbc_statuslog_1cases_ida',
  'link' => 'cases_dtbc_statuslog_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["dtbc_StatusLog"]["fields"]["cases_dtbc_statuslog_1cases_ida"] = array (
  'name' => 'cases_dtbc_statuslog_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_dtbc_statuslog_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE',
);


 // created: 2017-10-11 20:00:42
$dictionary['dtbc_StatusLog']['fields']['description']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['description']['comments']='Full text of the note';
$dictionary['dtbc_StatusLog']['fields']['description']['merge_filter']='disabled';
$dictionary['dtbc_StatusLog']['fields']['description']['audited']=true;

 

 // created: 2017-10-11 20:01:53
$dictionary['dtbc_StatusLog']['fields']['date_end_c']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['date_end_c']['options']='date_range_search_dom';
$dictionary['dtbc_StatusLog']['fields']['date_end_c']['labelValue']='Date end';
$dictionary['dtbc_StatusLog']['fields']['date_end_c']['enable_range_search']='1';

 

 // created: 2017-10-11 20:02:04
$dictionary['dtbc_StatusLog']['fields']['date_modified']['audited']=true;
$dictionary['dtbc_StatusLog']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['dtbc_StatusLog']['fields']['date_modified']['merge_filter']='disabled';

 

 // created: 2017-10-11 20:01:02
$dictionary['dtbc_StatusLog']['fields']['time_spent_c']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['time_spent_c']['labelValue']='Time spent';

 

 // created: 2017-10-11 20:02:12
$dictionary['dtbc_StatusLog']['fields']['date_entered']['audited']=true;
$dictionary['dtbc_StatusLog']['fields']['date_entered']['comments']='Date record created';
$dictionary['dtbc_StatusLog']['fields']['date_entered']['merge_filter']='disabled';

 

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$dictionary['dtbc_StatusLog']['fields']['cases_dtbc_statuslog_1_name']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['case_sub_status_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_end_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['time_spent_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_start_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['case_status_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['userid_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_entered']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_modified']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['name']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['description']['inline_edit']='0';


 // created: 2017-10-11 20:01:42
$dictionary['dtbc_StatusLog']['fields']['date_start_c']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['date_start_c']['options']='date_range_search_dom';
$dictionary['dtbc_StatusLog']['fields']['date_start_c']['labelValue']='Date start';
$dictionary['dtbc_StatusLog']['fields']['date_start_c']['enable_range_search']='1';

 

 // created: 2017-10-11 20:01:17
$dictionary['dtbc_StatusLog']['fields']['name']['audited']=true;
$dictionary['dtbc_StatusLog']['fields']['name']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['name']['duplicate_merge']='disabled';
$dictionary['dtbc_StatusLog']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['dtbc_StatusLog']['fields']['name']['merge_filter']='disabled';
$dictionary['dtbc_StatusLog']['fields']['name']['unified_search']=false;

 

 // created: 2017-10-11 19:59:13
$dictionary['dtbc_StatusLog']['fields']['case_status_c']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['case_status_c']['labelValue']='Case status';

 

 // created: 2017-10-11 19:59:44
$dictionary['dtbc_StatusLog']['fields']['userid_c']['inline_edit']='';
$dictionary['dtbc_StatusLog']['fields']['userid_c']['labelValue']='User ID';

 
?>