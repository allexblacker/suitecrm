<?php
$module_name = 'dtbc_StatusLog';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'CASES_DTBC_STATUSLOG_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_CASES_TITLE',
    'id' => 'CASES_DTBC_STATUSLOG_1CASES_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'CASE_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CASE_STATUS',
    'width' => '10%',
  ),
  'CASE_SUB_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CASE_SUB_STATUS',
    'width' => '10%',
  ),
  'DATE_START_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_DATE_START',
    'width' => '10%',
  ),
  'DATE_END_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_DATE_END',
    'width' => '10%',
  ),
  'TIME_SPENT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_TIME_SPENT',
    'width' => '10%',
  ),
  'USERID_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_USERID',
    'width' => '10%',
  ),
);
?>
