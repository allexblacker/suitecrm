<?php
// created: 2017-11-09 10:42:51
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Type',
    'width' => '10%',
    'default' => true,
  ),
  'contact_c' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_CONTACT',
    'id' => 'CONTACT_ID_C',
    'link' => true,
    'width' => '10%',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contact_id_c',
  ),
  'call_c' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_CALL_C',
    'width' => '10%',
    'default' => true,
  ),
  'role' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Role',
    'width' => '10%',
    'default' => true,
  ),
  'decision_maker' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Decision_Maker',
    'width' => '10%',
    'default' => true,
  ),
  'call_to_action' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'Call_to_Action',
    'width' => '10%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'notes' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'Notes',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
);