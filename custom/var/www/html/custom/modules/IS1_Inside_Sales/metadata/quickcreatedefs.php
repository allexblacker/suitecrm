<?php
$module_name = 'IS1_Inside_Sales';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'role',
            'studio' => 'visible',
            'label' => 'Role',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accounts_is1_inside_sales_1_name',
            'label' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_ACCOUNTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'Type',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'call_c',
            'studio' => 'visible',
            'label' => 'LBL_CALL_C',
          ),
          1 => 
          array (
            'name' => 'notes',
            'studio' => 'visible',
            'label' => 'Notes',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'call_to_action',
            'studio' => 'visible',
            'label' => 'Call_to_Action',
          ),
          1 => 
          array (
            'name' => 'ist_appt_set',
            'label' => 'IST_APPT_set',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'decision_maker',
            'studio' => 'visible',
            'label' => 'Decision_Maker',
          ),
          1 => 
          array (
            'name' => 'contact_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACT',
          ),
        ),
      ),
    ),
  ),
);
?>
