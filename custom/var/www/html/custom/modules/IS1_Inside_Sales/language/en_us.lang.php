<?php
// created: 2017-11-03 23:39:32
$mod_strings = array (
  'LBL_ACCOUNT' => 'Account',
  'ROLE' => 'Role',
  'LBL_CALL_C' => 'Call',
  'CALL_TO_ACTION' => 'Call to Action',
  'DECISION_MAKER' => 'Decision Maker',
  'TYPE' => 'Type',
  'LBL_CONTACT_CONTACT_ID' => 'Contact (related Contact ID)',
  'LBL_CONTACT' => 'Contact',
);