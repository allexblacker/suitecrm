<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');

class IS1_Inside_SalesViewEdit extends ViewEdit{

	public function display(){
		
		parent::display();
		echo getVersionedScript('custom/modules/IS1_Inside_Sales/dtbc/validations.js');
	}
}