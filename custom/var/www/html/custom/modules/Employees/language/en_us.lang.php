<?php
// created: 2017-11-17 01:52:32
$mod_strings = array (
  'LBL_TIER3' => 'Tier 3',
  'LBL_DIVISION' => 'Division',
  'LBL_SALESFORCE_ID' => 'SalesForce ID',
  'LBL_REGION' => 'Region',
  'LBL_ROLE' => 'Role',
  'LBL_ADDRESS_COUNTRY' => 'Country',
  'LBL_SUITE_ID' => 'suite id',
);