<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/AOS_Quotes/views/view.detail.php');

class CustomAOS_QuotesViewDetail extends AOS_QuotesViewDetail {

	function display(){
		parent::display();
		echo getVersionedScript("custom/modules/AOS_Quotes/dtbc/detailview.js");
		echo '<input type="hidden" id="dtbc_selected_pricebook_name" value="' . $this->getPricebookName() . '"/>';
		echo "<input type='hidden' id='dtbc_document_templates' value='" . $this->getDocumentTemplates() . "'/>";
	}
	
	private function getDocumentTemplates() {
		global $db;
		
		$sql = "SELECT id, document_name
				FROM dha_plantillasdocumentos
				WHERE deleted = 0 AND
				modulo = " . $db->quoted('AOS_Quotes');
				
		$res = $db->query($sql);
		
		$retval = array();
		
		while ($row = $db->fetchByAssoc($res)) {
			$retval[] = array(
				"id" => $row['id'],
				"name" => $row['document_name'],
			);
		}
		
		return json_encode($retval);
	}

	private function getPricebookName() {
		global $db;
		
		$sql = "SELECT dtbc_pricebook.name
				FROM dtbc_pricebook
				JOIN opportunities_cstm ON opportunities_cstm.dtbc_pricebook_id_c = dtbc_pricebook.id
				WHERE dtbc_pricebook.deleted = 0 AND
				opportunities_cstm.id_c = " . $db->quoted($this->bean->opportunity_id);
		
		$res = $db->fetchOne($sql);
		
		if ($res != null && is_array($res) && count($res) > 0)
			return $res['name'];
		
		return "";
	}
}
?>
