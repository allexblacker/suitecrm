<?php 
 //WARNING: The contents of this file are auto-generated



$hook_array['after_relationship_delete'][] = array(
	10,
	'Sync line items',
	'custom/include/dtbc/hooks/quotesLineItemSyncAndCopy.php',
	'LineItemCopySync',
	'after_relationship_delete_from_quote'
);

$hook_array['before_save'][] = array(
	1,
	'Sync and/or copy line items',
	'custom/include/dtbc/hooks/quotesLineItemSyncAndCopy.php',
	'LineItemCopySync',
	'before_save_from_quote'
);

$hook_array['after_save'][] = array(
	1,
	'Sync and/or copy line items',
	'custom/include/dtbc/hooks/quotesLineItemSyncAndCopy.php',
	'LineItemCopySync',
	'after_save_from_quote'
);
?>