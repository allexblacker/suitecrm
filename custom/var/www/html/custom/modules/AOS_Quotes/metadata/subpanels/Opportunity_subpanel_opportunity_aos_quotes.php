<?php
// created: 2017-10-30 15:56:58
$subpanel_layout['list_fields'] = array (
  'number' => 
  array (
    'width' => '5%',
    'vname' => 'LBL_LIST_NUM',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '25%',
    'default' => true,
  ),
  'is_synced_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_IS_SYNCED',
    'width' => '10%',
  ),
  'expiration' => 
  array (
    'width' => '15%',
    'vname' => 'LBL_EXPIRATION',
    'default' => true,
  ),
  'subtotal_amount' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_SUBTOTAL_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'lineitem_totalprice' => 
  array (
    'type' => 'function',
    'studio' => 'visible',
    'vname' => 'LBL_LINEITEM_TOTALPRICE',
    'width' => '10%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'currency_id' => 
  array (
    'usage' => 'query_only',
  ),
);