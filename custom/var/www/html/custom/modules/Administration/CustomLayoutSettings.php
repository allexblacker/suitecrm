<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/ACLRoles/ACLRole.php');
require_once("custom/include/dtbc/helpers.php");

class CustomLayoutSettings {
	
	private $db;

	function __construct() {
		$this->db = DBManagerFactory::getInstance();
	}
	
	public function generate() {
		global $mod_strings;
				
		$smarty = new Sugar_Smarty();
		
		$smarty->assign('roles', $this->getAllRoles());
		$smarty->assign('modules', $this->getAllModules());
		$smarty->assign('layouts', $this->getAllLayouts());
		
		$smarty->assign('selModuleLabel', $mod_strings['LBL_ADMIN_SELECT_MODULE']);
		$smarty->assign('saveButtonLabel', $mod_strings['LBL_ADMIN_SAVE_BUTTON']);
		
		$smarty->display('custom/modules/Administration/tpls/CustomLayoutSettings.tpl');

	}
	
	private function getAllLayouts() {
		global $app_list_strings;
		$retval = array();
		foreach ($app_list_strings['moduleList'] as $key => $value) {
			$retval[] = array(
				"id" => $key,
				"name" => $value,
			);
		}
		return $this->array_sort($retval, "name");
	}
	
	private function getAllModules() {
		global $app_list_strings;
		$retval = array();
		foreach ($app_list_strings['moduleList'] as $key => $value) {
			$retval[] = array(
				"id" => $key,
				"name" => $value,
			);
		}
		return $this->array_sort($retval, "name");
	}
	
	private function getAllRoles() {
		$retval = array();
		$roles = ACLRole::getAllRoles();
		foreach ($roles as $role) {
			$retval[] = array(
				"id" => $role->id,
				"name" => $role->name,
			);
		}
		return $this->array_sort($retval, "name");
	}
	
	private function array_sort($array, $on, $order=SORT_ASC) {
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
				break;
				case SORT_DESC:
					arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
	
}