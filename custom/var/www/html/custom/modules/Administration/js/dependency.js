	function addBlankValueToDropDown(dropDown){
		dropDown.prepend("<option value='' selected='selected'></option>");
	}
	
	$(function(){
		addBlankValueToDropDown($("#modules"));
    });
	
	function disableOption(object, value){
		object.find('option:disabled').prop('disabled', false);
		object.find('option[value="' + value + '"]').prop('disabled', true);
	}
	
	function emptySettings(){
		$("#settings").html('');
	}
	
	function hideButton(){
		$("#saveButton").addClass('hide');
	}

	function getSettings(id, value){

		if(value == ''){
			emptySettings();
			hideButton()
			return;
		}
		
		var dropDownController = $("#dropDownController");
		var dropDownControlled = $("#dropDownControlled");
		
		if(id == 'dropDownController')
			disableOption(dropDownControlled,value);
		else
			disableOption(dropDownController,value);
			
		if(dropDownController.val() == '' || dropDownControlled.val() == ''){
			hideButton()
			return;
		}
		
		$("#saveButton").removeClass('hide');
		
		var fieldController = dropDownController.find('option:selected').text();
		var fieldControlled = dropDownControlled.find('option:selected').text();
		var module = $("#modules");
		
		$.ajax({
			method: 'POST',
			url: 'index.php?module=Administration&action=getDependencySettings',
			data: {
					controllerListName : dropDownController.val(),
					controlledListName : dropDownControlled.val(),
					fieldController : fieldController,
					fieldControlled : fieldControlled,
					moduleName : module.val()
					}
		}).done(function (dataJSON) {
			var data = JSON.parse(dataJSON);
			var valuesController = data[0];
			var valuesControlled = data[1];
			var existingSettings = data[2];
			var topRow = getTopRow(valuesController);
			
			var row = '';
			$.each(valuesControlled, function(keyControlled, valueControlled){
				row += "<tr><td>" + valueControlled + "</td>";
				$.each(valuesController, function(keyController, valueController){
					var id = keyController + "|" + keyControlled;
					if(jQuery.inArray(id, existingSettings) == -1)
						row += "<td><input type='checkbox' id='" + id + "' />";
					else
						row += "<td><input type='checkbox' checked id='" + id + "' />";
						
				});
				row += "</tr>";
			});
			
			var settingsTable = $("#settings");
			settingsTable.html(topRow + row);
			settingsTable.tableHeadFixer();
			settingsTable.tableHeadFixer();
			settingsTable.tableHeadFixer({'left' : 1});
			
			if(settingsTable.height() < 500)
				$("#container").css('height',settingsTable.height() + 15);
			 
		}).fail(function (xhr) {
			console.log(xhr);
		});
	}
	
	function getTopRow(object){
		var topRow = '<thead><tr><th></th>';
		$.each(object, function(key, value){
				topRow += '<th>' + value + '</th>';
		});
		return topRow + '</tr></thead>';
	}
	
	function setSettings(){
		var selected = [];
		$('#settings input:checked').each(function() {
			selected.push($(this).attr('id'));
		});
		$.ajax({
			method: 'POST',
			url: 'index.php?module=Administration&action=setDependencySettings',
			data: {
				selectedValues: selected,
				moduleName: $("#modules").val(),
				controller: $("#dropDownController").find('option:selected').text(),
				controllerListName: $("#dropDownController").val(),
				controlled: $("#dropDownControlled").find('option:selected').text(),
				controlledListName: $("#dropDownControlled").val()
			} 
		}).done(function (data) {
			alert(data);
		}).fail(function (xhr) {
			console.log(xhr);
		});
	}
	
	function removeAddHideClass(add,className){
		if(add){
			$("#dropDownController").removeClass(className);;
			$("#controllerLabel").removeClass(className);;
			$("#dropDownControlled").removeClass(className);
			$("#controlledLabel").removeClass(className);;
		}
		else{
			$("#controllerLabel").addClass(className);;
			$("#dropDownController").addClass(className);
			$("#dropDownControlled").addClass(className);
			$("#controlledLabel").addClass(className);;
		}
	}
	
	function emptyDropDowns(){
		$("#dropDownController option").remove();
		$("#dropDownControlled option").remove();
		addBlankValueToDropDown($("#dropDownController"));
		addBlankValueToDropDown($("#dropDownControlled"));
	}
	
	function getDropDownValues(){
		var module = $("#modules").val();
		emptySettings();
		hideButton();
		$("#saveButton")
		if(module == '')
			removeAddHideClass(false,'hide');
		else
			removeAddHideClass(true,'hide');
		
		$.ajax({
			method: 'POST',
			url: 'index.php?module=Administration&action=getModuleDropDowns',
			data: {
				moduleName: module
			} 
		}).done(function (enumsJSON) {

			if(enumsJSON == ''){
				emptyDropDowns()
				return;
			}
				
			var enums = JSON.parse(enumsJSON);
			emptyDropDowns()
			$.each(enums, function(key, value){
				addToDropDown($('#dropDownController'), value, key);
				addToDropDown($('#dropDownControlled'), value, key);
			});
		}).fail(function (xhr) {
			console.log(xhr);
		});
	}