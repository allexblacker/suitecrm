<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$smarty = new Sugar_Smarty();
global $mod_strings, $app_list_strings;
$moduleList = $app_list_strings['moduleList'];
asort($moduleList);
$smarty->assign('saveButton', $mod_strings['LBL_ADMIN_SAVE_BUTTON']);
$smarty->assign('moduleList', $moduleList);
$smarty->assign('module', $mod_strings['LBL_DEPENDENCY_MODULE']);
$smarty->assign('controller', $mod_strings['LBL_DEPENDENCY_CONTROLLER']);
$smarty->assign('controlled', $mod_strings['LBL_DEPENDENCY_CONTROLLED']);
$smarty->display('custom/modules/Administration/tpls/DependecySettings.tpl');