<?php

require_once('include/MVC/View/views/view.edit.php');

class dtbc_Project_CommentsViewEdit extends ViewEdit{
	
	public function preDisplay(){
		
		parent::preDisplay();
		
		echo '<script type="text/javascript" src="include/javascript/tiny_mce/tiny_mce.js"></script>';
		echo '<script type="text/javascript">tinyMCE.init({ mode: "exact", elements : "description", theme : "advanced", theme_advanced_buttons3_add : "fullpage" });</script>';
	}
}