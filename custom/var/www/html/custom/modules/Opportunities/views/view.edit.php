<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once('custom/include/dtbc/JSLoader.php');
require_once("custom/include/dtbc/helpers.php");

class OpportunitiesViewEdit extends ViewEdit {

    function __construct(){
        parent::__construct();
    }
	
	function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();
		
		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

        $this->ev = $this->getEditView();
        $this->ev->ss =& $this->ss;
        $this->ev->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/EditView/EditView.tpl'));
	}
	
	public function display() {
		global $sugar_config, $current_user;
		parent::display();
		includeJavaScriptFile("custom/modules/Opportunities/dtbc/validations.js");
		echo '<input type="hidden" id="currentUserId" value="' . $current_user->id . '"/>';
		
		$allowedUsers = $sugar_config['solaredge']['opportunities']['AllowedUserToEditBooked'];
		$allowed = 0;
		if(in_array($current_user->id, $allowedUsers))
			$allowed = 1;
		echo '<input type="hidden" id="allowedUser" value="' . $allowed . '"/>';
	}
}
