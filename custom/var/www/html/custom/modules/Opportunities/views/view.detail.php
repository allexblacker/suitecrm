<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class OpportunitiesViewDetail extends ViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailView2();
 	    $this->dv->ss =&  $this->ss;
        
		$this->bean->stageindicator_c = html_entity_decode($this->bean->stageindicator_c);
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
	public function display() {
		parent::display();
		echo getVersionedScript("custom/modules/Opportunities/dtbc/detailview.js");
		echo '<input type="hidden" id="dtbc_selected_pricebook_name" value="' . $this->bean->pricebook_c . '"/>';
	}
}
