function Change_Stage_to_Booked(formName) {
	if ($('#sales_stage').length) {
		if ($('#sales_stage').val() == 'Booked' && $('#allowedUser').val() == 0) {
			add_error_style_custom(formName, 'sales_stage', SUGAR.language.get('Opportunities', 'LBL_ERR_ONLY_DANIEL_CAN_BOOK'));
			return false;
		}
	}
	
	return true;
}

function estimated_KW_is_mandatory(formName) {
	if ($('#installation_type_c').length && $('#estimated_kw_c').length) {
		if ($('#installation_type_c').val() == '1' && ($('#estimated_kw_c').val() == '' || $('#estimated_kw_c').val() == '0')) {
			add_error_style_custom(formName, 'estimated_kw_c', SUGAR.language.get('Opportunities', 'LBL_ERR_ESTIMATED_KW_EMPTY'));
			return false;
		}
	}
	
	return true;
}

function Too_Much_Split(formName) {
	if ($('#total_split_c').length) {
		if ($('#total_split_c').val() > 1) {
			add_error_style_custom(formName, 'total_split_c', SUGAR.language.get('Opportunities', 'LBL_ERR_TOTAL_SPLIT'));
			return false;
		}
	}
	
	return true;
}

function Why_lost(formName) {
	if ($('#sales_stage').length && $('#why_lost_c').length) {
		if ($('#sales_stage').val() == 'Lost' && $('#why_lost_c').val().length == 0) {
			add_error_style_custom(formName, 'why_lost_c', SUGAR.language.get('Opportunities', 'LBL_ERR_WHY_LOST'));
			return false;
		}
	}
	
	return true;
}


function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation() {
	clear_all_errors();
	var formName = 'EditView';
	var checkForm = check_form(formName); 
	var retval = Change_Stage_to_Booked(formName);
	retval = calcRetval(retval, estimated_KW_is_mandatory(formName));
	retval = calcRetval(retval, Too_Much_Split(formName));
	retval = calcRetval(retval, Why_lost(formName));

	// Original validations
	var _form = document.getElementById('EditView');
	_form.action.value='Save';
	if (checkForm && retval){
		SUGAR.ajaxUI.submitForm(_form);
		return true;
	}

	scrollToError();
	return false;
}

$(document).ready(function() {
	jQuery.getScript('custom/include/dtbc/js/scrollingAfterAddingErrorStyle.js');
	$("#save_and_continue").remove();
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	$("<input title='" + SUGAR.language.get('Opportunities','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation();' type='button' name='button' value='" + SUGAR.language.get('Opportunities','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
});