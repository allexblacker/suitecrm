<?php 
 //WARNING: The contents of this file are auto-generated


if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!empty($_REQUEST['record']) && strtolower($_REQUEST['action']) == 'detailview' && strtolower($_REQUEST['module']) == 'opportunities') {

	require_once('custom/include/dtbc/router.php');
	
	$url = Router::getCreateProjectWithPrefillRoute($_REQUEST['record']);
	$module_menu []= array($url, $mod_strings['LBL_CREATE_NEW_PROJECT']);
	
	$baseUrl = "index.php?module=";
	$module_menu []= array("index.php?module=Opportunities&action=clone&mode=wprod&old_record=" . $_REQUEST['record'], $mod_strings['LBL_CLONE_WITH_PRODUCTS']);
	$module_menu []= array("index.php?module=Opportunities&action=clone&mode=woprod&old_record=" . $_REQUEST['record'], $mod_strings['LBL_CLONE_WITHOUT_PRODUCTS']);
	$module_menu []= array("index.php?module=Opportunities&action=clone&mode=wprice&old_record=" . $_REQUEST['record'], $mod_strings['LBL_CLONE_WITH_PRICING']);
}
?>