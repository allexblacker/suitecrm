<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/BeanDuplicator.php');
require_once('custom/include/dtbc/helpers.php');
require_once('custom/include/dtbc/router.php');

class OpportunitiesController extends SugarController {
	
	public function action_clone() {
		$duplicator = new BeanDuplicator();
		$fields = $duplicator->getModuleFieldDefs("Opportunities");
		$doNotCopy = array(
			"id",
			"date_entered",
			"date_modified",
			"modified_user_id",
			"modified_by_name",
			"created_by",
			"created_by_name",
		);
		$newOpportunity = $duplicator->getDuplicatedBean($_REQUEST['old_record'], "Opportunities", $fields['fields'], $doNotCopy);
		
		// Copy selected account also
		$oldOpportunity = BeanFactory::getBean("Opportunities", $_REQUEST['old_record']);
		$newOpportunity->account_id = $oldOpportunity->account_id;
		$newOpportunity->save();
		
		if ($_REQUEST['mode'] != "woprod") {
			// Clone products
			$this->cloneLineItems($oldOpportunity->id, $_REQUEST['mode'], $newOpportunity->id);
		}
		
		Router::returnToDetailView("Opportunities", $newOpportunity->id);
	}
	
	// Clone line item values with relation
	private function cloneLineItems($oldOpportunityId, $mode, $newOpportunityId) {
		global $db;
		// Get line items
		$sql = "SELECT dtbc_line_item.*, dtbc_line_item_cstm.*
				FROM dtbc_line_item
				LEFT JOIN dtbc_line_item_cstm ON id_c = dtbc_line_item.id
				JOIN opportunities_dtbc_line_item_1_c ON opportunities_dtbc_line_item_1opportunities_ida = " . $db->quoted($oldOpportunityId) . " AND opportunities_dtbc_line_item_1_c.deleted = 0  AND opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id
				WHERE dtbc_line_item.deleted = 0";

		$res = $db->query($sql);
		while ($row = $db->fetchByAssoc($res)) {
			$fieldNames = "";
			$fieldValues = "";
			$lineId = "";
			foreach ($row as $fieldName => $value) {
				if ($fieldName == "id_c") {
					// Run query and reset values
					$db->query($this->makeSql($fieldNames, $fieldValues, false));
					$fieldNames = "";
					$fieldValues = "";
				}
				
				$fieldNames = $this->appendString($fieldNames, $fieldName);
				
				if ($fieldName != "id_c" && $fieldName != "id") {
					$setValue = $this->getValue($fieldName, $value, $mode, $row['dtbc_pricebook_product_id_c']);
					$fieldValues = $this->appendString($fieldValues, strlen($setValue) > 0 ? $db->quoted($setValue) : "NULL");
					continue;
				}
				
				if ($fieldName == "id")
					$lineId = create_guid();
				
				$fieldValues = $this->appendString($fieldValues, $db->quoted($lineId));				
			}
			$db->query($this->makeSql($fieldNames, $fieldValues, true));
			$db->query($this->makeRelation($lineId, $newOpportunityId));
		}
	}
	
	// Calculate the price value. In "Clone with Price" mode, we don't need to do anything. Else, we should copy the price from the PBP module, and recalc the total value also.
	private function getValue($fieldName, $value, $mode, $pbpId) {	
		if (trim(strtolower($mode)) == "wprice" || $fieldName != 'list_price_c')
			return $value;

		// Get "list price" from the "Pricebook Product" (alias PBP module)
		$retval = "NULL";
		
		global $db;
		
		$sql = "SELECT list_price_c, standard_price_c, use_standard_price_c 
				FROM dtbc_pricebook_product_cstm 
				WHERE id_c = " . $db->quoted($pbpId);
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0) {
			// Standard price is active or not
			if ($res['use_standard_price_c'] == 1)
				$retval = $res['standard_price_c'];
			else
				$retval = $res['list_price_c'];
		}

		return $retval;
	}
	
	// Insert Line Item - Opportunities relation
	private function makeRelation($recordId, $opportunityId) {
		global $db;
		$sql = "INSERT INTO opportunities_dtbc_line_item_1_c 
				(id, date_modified, deleted, opportunities_dtbc_line_item_1opportunities_ida, opportunities_dtbc_line_item_1dtbc_line_item_idb) 
				VALUES (" . $db->quoted(create_guid()) . ", NOW(), 0, " . $db->quoted($opportunityId) . ", " . $db->quoted($recordId) . ")";
		return $sql;
	}
	
	// Make line item SQLs
	private function makeSql($fieldNames, $fieldValues, $isCstm) {
		$tableName = $isCstm ? "dtbc_line_item_cstm" : "dtbc_line_item";
		$retval = "INSERT INTO " . $tableName;
		$retval .= " (" . $fieldNames . ") VALUES (" . $fieldValues . ")";
		return $retval;
	}
	
	private function appendString($string, $newValue, $separator = ",") {
		if (strlen($string) > 0)
			return $string . $separator . $newValue;
		
		return $string . $newValue;
	}

	public function action_selectPricebook() {
		global $db;
		
		// Set pricebook
		$focus = BeanFactory::getBean("Opportunities", $_REQUEST['record']);
		$focus->dtbc_pricebook_id_c = $_REQUEST['pricebook'];
		$focus->save();
		
		// Delete related line items
		$sql = "UPDATE dtbc_line_item
				SET deleted = 1
				WHERE id IN (
					SELECT opportunities_dtbc_line_item_1dtbc_line_item_idb 
					FROM opportunities_dtbc_line_item_1_c
					WHERE opportunities_dtbc_line_item_1opportunities_ida = 'b9ef997a-c2d1-64b0-5e11-599c1b5cdc9f'
				)";
				
		$db->query($sql);
		
		// Delete relations
		$sql = "UPDATE opportunities_dtbc_line_item_1_c
				SET deleted = 1
				WHERE opportunities_dtbc_line_item_1opportunities_ida = " . $db->quoted($_REQUEST['record']);
		
		$db->query($sql);
		
		Router::returnToDetailView("Opportunities", $_REQUEST['record']);
	}
	
	public function action_getPricebooks() {
		global $db;
		
		$retval = array();
		
		$sql = "SELECT id, name
				FROM dtbc_pricebook
				WHERE deleted = 0
				ORDER BY name ASC";
				
		$res = $db->query($sql);
		
		while ($row = $db->fetchByAssoc($res)) {
			$retval[] = array(
				"id" => $row['id'],
				"name" => $row['name'],
			);
		}
		
		echo json_encode($retval);
		die();
	}
}