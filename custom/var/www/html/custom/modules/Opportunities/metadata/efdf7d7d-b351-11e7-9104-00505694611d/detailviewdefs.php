<?php
$viewdefs ['Opportunities'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'amount',
            'label' => '{$MOD.LBL_AMOUNT} ({$CURRENCY})',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'so_number_c',
            'label' => 'SO_NUMBER',
          ),
          1 => 
          array (
            'name' => 'po_sum_c',
            'label' => 'PO_SUM',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'currency_c',
            'studio' => 'visible',
            'label' => 'CURRENCY',
          ),
          1 => 'date_closed',
        ),
        3 => 
        array (
          0 => 'name',
          1 => 'sales_stage',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'nl2br' => true,
          ),
          1 => 
          array (
            'name' => 'commit_level_c',
            'studio' => 'visible',
            'label' => 'COMMIT_LEVEL',
          ),
        ),
        5 => 
        array (
          0 => 'account_name',
          1 => 'probability',
        ),
        6 => 
        array (
          0 => 'opportunity_type',
          1 => 
          array (
            'name' => 'why_lost_c',
            'studio' => 'visible',
            'label' => 'WHY_LOST',
          ),
        ),
        7 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'distributor_status_c',
            'studio' => 'visible',
            'label' => 'DISTRIBUTOR_STATUS',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'COUNTRY',
          ),
          1 => 
          array (
            'name' => 'why_lost_at_distributor_c',
            'label' => 'WHY_LOST_AT_DISTRIBUTOR',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'us_region_c',
            'studio' => 'visible',
            'label' => 'US_REGION',
          ),
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'installation_type_c',
            'studio' => 'visible',
            'label' => 'INSTALLATION_TYPE',
          ),
          1 => '',
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'open_booked_c',
            'studio' => 'visible',
            'label' => 'OPEN_BOOKED',
          ),
          1 => '',
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'nam_ship_from_c',
            'studio' => 'visible',
            'label' => 'NAM_SHIP_FROM',
          ),
          1 => '',
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'erp_customer_number_c',
            'label' => 'ERP_CUSTOMER_NUMBER',
          ),
          1 => '',
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'erp_cust_name_c',
            'label' => 'ERP_CUST_NAME',
          ),
          1 => '',
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'bklog_c',
            'label' => 'BKLOG',
          ),
          1 => '',
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'shipped_q_c',
            'label' => 'SHIPPED_Q',
          ),
          1 => '',
        ),
        17 => 
        array (
          0 => '',
          1 => '',
        ),
        18 => 
        array (
          0 => '',
          1 => '',
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'backlog_to_date_c',
            'label' => 'BACKLOG_TO_DATE',
          ),
          1 => '',
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'shipped_todate_c',
            'label' => 'SHIPPED_TODATE',
          ),
          1 => '',
        ),
        21 => 
        array (
          0 => '',
          1 => '',
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'backlog_for_next_q_c',
            'label' => 'BACKLOG_FOR_NEXT_Q',
          ),
          1 => '',
        ),
        23 => 
        array (
          0 => 
          array (
            'name' => 'customer_poc_c',
            'label' => 'CUSTOMER_POC',
          ),
          1 => '',
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'customer_contact_info_c',
            'label' => 'CUSTOMER_CONTACT_INFO',
          ),
          1 => '',
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'opportunity_type1_c',
            'studio' => 'visible',
            'label' => 'OPPORTUNITY_TYPE',
          ),
          1 => '',
        ),
        26 => 
        array (
          0 => '',
          1 => '',
        ),
        27 => 
        array (
          0 => 
          array (
            'name' => 'cae_c',
            'studio' => 'visible',
            'label' => 'LBL_CAE',
          ),
          1 => '',
        ),
        28 => 
        array (
          0 => 
          array (
            'name' => 'estimated_kw_c',
            'label' => 'ESTIMATED_KW',
          ),
          1 => '',
        ),
        29 => 
        array (
          0 => '',
          1 => '',
        ),
        30 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'q1_backlog_c',
            'label' => 'Q1_BACKLOG',
          ),
          1 => 
          array (
            'name' => 'q3_backlog_c',
            'label' => 'Q3_BACKLOG',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'q1_shipped_c',
            'label' => 'Q1_SHIPPED',
          ),
          1 => 
          array (
            'name' => 'q3_shipped_c',
            'label' => 'Q3_SHIPPED',
          ),
        ),
        2 => 
        array (
          0 => '',
          1 => '',
        ),
        3 => 
        array (
          0 => '',
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'q2_backlog_c',
            'label' => 'Q2_BACKLOG',
          ),
          1 => 
          array (
            'name' => 'q4_backlog_c',
            'label' => 'Q4_BACKLOG',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'q2_shipped_c',
            'label' => 'Q2_SHIPPED',
          ),
          1 => 
          array (
            'name' => 'q4_shipped_c',
            'label' => 'Q4_SHIPPED',
          ),
        ),
        6 => 
        array (
          0 => '',
          1 => '',
        ),
        7 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'account_name_c',
            'studio' => 'visible',
            'label' => 'ACCOUNT_NAME',
          ),
          1 => 
          array (
            'name' => 'jjwg_maps_address_c',
            'label' => 'LBL_JJWG_MAPS_ADDRESS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
          1 => 
          array (
            'name' => 'booking_date_c',
            'label' => 'BOOKING_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'campaign_name',
            'label' => 'LBL_CAMPAIGN',
          ),
          1 => 
          array (
            'name' => 'closed_year_c',
            'label' => 'LBL_CLOSED_YEAR',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'closed_q_c',
            'label' => 'LBL_CLOSED_Q',
          ),
          1 => 
          array (
            'name' => 'comments_c',
            'studio' => 'visible',
            'label' => 'COMMENTS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'commit_level_last_modified_c',
            'label' => 'COMMIT_LEVEL_LAST_MODIFIED',
          ),
          1 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'duration_c',
            'studio' => 'visible',
            'label' => 'LBL_DURATION',
          ),
          1 => 
          array (
            'name' => 'expected_revenue_c',
            'label' => 'LBL_EXPECTED_REVENUE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_geocode_status_c',
            'label' => 'LBL_JJWG_MAPS_GEOCODE_STATUS',
          ),
          1 => 
          array (
            'name' => 'geographic_area_c',
            'studio' => 'visible',
            'label' => 'GEOGRAPHIC_AREA',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'inactive_items_c',
            'studio' => 'visible',
            'label' => 'INACTIVE_ITEMS',
          ),
          1 => 
          array (
            'name' => 'inverters_qty_neto_c',
            'label' => 'LBL_INVERTERS_QTY_NETO',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'q1_total_price_c',
            'label' => 'LBL_FUNCTION_Q1_TOTAL_PRICE',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'q2_total_price_c',
            'label' => 'LBL_FUNCTION_Q2_TOTAL_PRICE',
            'studio' => 'visible',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'q3_total_price_c',
            'label' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'total_inverters_c',
            'label' => 'LBL_FUNCTION_TOTAL_INVERTERS',
            'studio' => 'visible',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'total_kw2_c',
            'label' => 'LBL_FUNCTION_TOTAL_KW2',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'total_options_c',
            'label' => 'LBL_FUNCTION_TOTAL_OPTIONS',
            'studio' => 'visible',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'total_others_c',
            'label' => 'LBL_FUNCTION_TOTAL_OTHERS',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'total_price_c',
            'label' => 'LBL_FUNCTION_TOTAL_PRICE',
            'studio' => 'visible',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'total_products_c',
            'label' => 'LBL_FUNCTION_TOTAL_PRODUCTS',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'jjwg_maps_lat_c',
            'label' => 'LBL_JJWG_MAPS_LAT',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'lead_source',
            'comment' => 'Source of the opportunity',
            'label' => 'LBL_LEAD_SOURCE',
          ),
          1 => 
          array (
            'name' => 'jjwg_maps_lng_c',
            'label' => 'LBL_JJWG_MAPS_LNG',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => 
          array (
            'name' => 'monthly_c',
            'label' => 'LBL_MONTHLY',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'next_step',
            'comment' => 'The next step in the sales process',
            'label' => 'LBL_NEXT_STEP',
          ),
          1 => 
          array (
            'name' => 'open_closed_c',
            'label' => 'LBL_OPEN_CLOSED',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'oppnum_c',
            'label' => 'LBL_OPPNUM',
          ),
          1 => 
          array (
            'name' => 'opportunity_expected_revenue_c',
            'label' => 'LBL_OPPORTUNITY_EXPECTED_REVENUE',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'options_qty_neto_c',
            'label' => 'LBL_OPTIONS_QTY_NETO',
          ),
          1 => 
          array (
            'name' => 'others_qty_neto_c',
            'label' => 'LBL_OTHERS_QTY_NETO',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'power_optimizer_qty_neto_c',
            'label' => 'LBL_POWER_OPTIMIZER_QTY_NETO',
          ),
          1 => 
          array (
            'name' => 'pricebook_c',
            'studio' => 'visible',
            'label' => 'LBL_PRICEBOOK',
          ),
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'q1_likely_c',
            'label' => 'LBL_Q1_LIKELY',
          ),
          1 => 
          array (
            'name' => 'q1_shipped_bl_c',
            'label' => 'LBL_Q1_SHIPPED_BL_C',
          ),
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'q1_weighted_price_c',
            'label' => 'LBL_Q1_WEIGHTED_PRICE',
          ),
          1 => 
          array (
            'name' => 'q2_likely_c',
            'label' => 'LBL_Q2_LIKELY',
          ),
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'q2_shipped_bl_c',
            'label' => 'LBL_Q2_SHIPPED_BL',
          ),
          1 => 
          array (
            'name' => 'q2_weighted_price_c',
            'label' => 'LBL_Q2_WEIGHTED_PRICE_C',
          ),
        ),
        23 => 
        array (
          0 => 
          array (
            'name' => 'q3_likely_c',
            'label' => 'LBL_Q3_LIKELY',
          ),
          1 => 
          array (
            'name' => 'q3_shipped_bl_c',
            'label' => 'LBL_Q3_SHIPPED_BL',
          ),
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'q3_weighted_price_c',
            'label' => 'LBL_Q3_WEIGHTED_PRICE_C',
          ),
          1 => 
          array (
            'name' => 'q4_likely_c',
            'label' => 'LBL_Q4_LIKELY',
          ),
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'q4_shipped_bl_c',
            'label' => 'LBL_Q4_SHIPPED_BL',
          ),
          1 => 
          array (
            'name' => 'q4_weighted_price_c',
            'label' => 'LBL_Q4_WEIGHTED_PRICE_C',
          ),
        ),
        26 => 
        array (
          0 => 
          array (
            'name' => 'region_c',
            'label' => 'REGION',
          ),
          1 => 
          array (
            'name' => 'requested_delivery_c',
            'label' => 'REQUESTED_DELIVERY',
          ),
        ),
        27 => 
        array (
          0 => 
          array (
            'name' => 'listview_security_group',
            'label' => 'LBL_LISTVIEW_GROUP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'sendingwhs_c',
            'label' => 'SENDINGWHS',
          ),
        ),
        28 => 
        array (
          0 => 
          array (
            'name' => 'shortcommitmentlevel_c',
            'label' => 'LBL_SHORTCOMMITMENTLEVEL',
          ),
          1 => 
          array (
            'name' => 'stageindicator_c',
            'label' => 'LBL_STAGEINDICATOR',
          ),
        ),
        29 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'STATE',
          ),
          1 => 
          array (
            'name' => 'status_c',
            'studio' => 'visible',
            'label' => 'STATUS',
          ),
        ),
        30 => 
        array (
          0 => 
          array (
            'name' => 'targername_c',
            'label' => 'LBL_TARGERNAME',
          ),
          1 => 
          array (
            'name' => 'owner_target_c',
            'label' => 'LBL_OWNER_TARGET',
          ),
        ),
        31 => 
        array (
          0 => 
          array (
            'name' => 'total_split_c',
            'label' => 'LBL_TOTAL_SPLIT',
          ),
          1 => 
          array (
            'name' => 'weighted_for_current_q_c',
            'label' => 'LBL_WEIGHTED_FOR_CURRENT_Q',
          ),
        ),
        32 => 
        array (
          0 => 
          array (
            'name' => 'aging_c',
            'label' => 'LBL_AGING',
          ),
          1 => 
          array (
            'name' => 'confirmed_ship_date_c',
            'label' => 'CONFIRMED_SHIP_DATE',
          ),
        ),
      ),
    ),
  ),
);
?>
