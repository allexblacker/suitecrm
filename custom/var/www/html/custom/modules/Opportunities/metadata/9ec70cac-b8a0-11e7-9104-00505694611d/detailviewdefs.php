<?php
$viewdefs ['Opportunities'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
        'LBL_DETAILVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 'account_name',
          1 => 
          array (
            'name' => 'amount',
            'label' => '{$MOD.LBL_AMOUNT} ({$CURRENCY})',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'COUNTRY',
          ),
          1 => 'date_closed',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'STATE',
          ),
          1 => 
          array (
            'name' => 'total_kw2_c',
            'label' => 'LBL_FUNCTION_TOTAL_KW2',
            'studio' => 'visible',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'oppnum_c',
            'label' => 'LBL_OPPNUM',
          ),
          1 => 'sales_stage',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'so_number_c',
            'label' => 'SO_NUMBER',
          ),
          1 => 
          array (
            'name' => 'commit_level_c',
            'studio' => 'visible',
            'label' => 'COMMIT_LEVEL',
          ),
        ),
        5 => 
        array (
          0 => 'name',
          1 => 'probability',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'nl2br' => true,
          ),
          1 => 
          array (
            'name' => 'why_lost_c',
            'studio' => 'visible',
            'label' => 'WHY_LOST',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'currency_c',
            'studio' => 'visible',
            'label' => 'CURRENCY',
          ),
          1 => 
          array (
            'name' => 'installation_type_c',
            'studio' => 'visible',
            'label' => 'INSTALLATION_TYPE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'region_c',
            'label' => 'REGION',
          ),
          1 => 
          array (
            'name' => 'cpm_c',
            'studio' => 'visible',
            'label' => 'LBL_CPM',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'customer_poc_c',
            'label' => 'CUSTOMER_POC',
          ),
          1 => 
          array (
            'name' => 'cae_c',
            'studio' => 'visible',
            'label' => 'LBL_CAE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'customer_contact_info_c',
            'label' => 'CUSTOMER_CONTACT_INFO',
          ),
          1 => 'next_step',
        ),
        11 => 
        array (
          0 => 'opportunity_type',
          1 => 
          array (
            'name' => 'estimated_kw_c',
            'label' => 'ESTIMATED_KW',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'nam_ship_from_c',
            'studio' => 'visible',
            'label' => 'NAM_SHIP_FROM',
          ),
          1 => '',
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'open_closed_c',
            'label' => 'LBL_OPEN_CLOSED',
          ),
          1 => '',
        ),
      ),
      'lbl_detailview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'po_sum_c',
            'label' => 'PO_SUM',
          ),
          1 => 
          array (
            'name' => 'shipped_q_c',
            'label' => 'SHIPPED_Q',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'shipped_todate_c',
            'label' => 'SHIPPED_TODATE',
          ),
          1 => 
          array (
            'name' => 'backlog_for_next_q_c',
            'label' => 'BACKLOG_FOR_NEXT_Q',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'backlog_to_date_c',
            'label' => 'BACKLOG_TO_DATE',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'q1_shipped_c',
            'label' => 'Q1_SHIPPED',
          ),
          1 => 
          array (
            'name' => 'q1_backlog_c',
            'label' => 'Q1_BACKLOG',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'q2_shipped_c',
            'label' => 'Q2_SHIPPED',
          ),
          1 => 
          array (
            'name' => 'q2_backlog_c',
            'label' => 'Q2_BACKLOG',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'q3_shipped_c',
            'label' => 'Q3_SHIPPED',
          ),
          1 => 
          array (
            'name' => 'q3_backlog_c',
            'label' => 'Q3_BACKLOG',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'q4_shipped_c',
            'label' => 'Q4_SHIPPED',
          ),
          1 => 
          array (
            'name' => 'q4_backlog_c',
            'label' => 'Q4_BACKLOG',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => '',
        ),
      ),
      'lbl_detailview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'inactive_items_c',
            'studio' => 'visible',
            'label' => 'INACTIVE_ITEMS',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
