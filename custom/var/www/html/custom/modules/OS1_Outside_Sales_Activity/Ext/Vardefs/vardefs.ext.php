<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-11-01 02:12:01
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["os1_outside_sales_activity_tasks_1"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1',
  'type' => 'link',
  'relationship' => 'os1_outside_sales_activity_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_TASKS_TITLE',
);


 // created: 2017-10-24 19:22:14

 

 // created: 2017-12-01 02:03:36
$dictionary['OS1_Outside_Sales_Activity']['fields']['meeting_date']['required']=true;

 

 // created: 2017-10-24 19:22:31

 

 // created: 2017-11-27 21:58:39
$dictionary['OS1_Outside_Sales_Activity']['fields']['meeting_type']['options']='meeting_type_list';

 

// created: 2017-10-25 18:47:12
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["accounts_os1_outside_sales_activity_1"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1',
  'type' => 'link',
  'relationship' => 'accounts_os1_outside_sales_activity_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
);
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["accounts_os1_outside_sales_activity_1_name"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
  'link' => 'accounts_os1_outside_sales_activity_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["OS1_Outside_Sales_Activity"]["fields"]["accounts_os1_outside_sales_activity_1accounts_ida"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_os1_outside_sales_activity_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
);

?>