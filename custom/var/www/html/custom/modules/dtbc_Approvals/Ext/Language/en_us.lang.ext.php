<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_ENTRY_CRITERIAS'] = 'Entry criterias';
$mod_strings['LBL_INITIAL_SUBMISSION_ACTIONS'] = 'Initial submission actions';
$mod_strings['LBL_APPROVAL_STEPS'] = 'Approval steps';
$mod_strings['LBL_APPROVAL_ACTIONS'] = 'Approval actions';
$mod_strings['LBL_REJECTION_ACTIONS'] = 'Rejection actions';
$mod_strings['LBL_RECALL_ACTIONS'] = 'Recall actions';
$mod_strings['LBL_EDIT'] = 'Edit';
$mod_strings['LBL_REMOVE'] = 'Remove';
$mod_strings['LBL_SET_CRITERIA'] = 'Set criteria';
$mod_strings['LBL_UP'] = 'Up';
$mod_strings['LBL_DOWN'] = 'Down';
$mod_strings['LBL_USER'] = 'User';
$mod_strings['LBL_GROUP'] = 'Group';
$mod_strings['LBL_SECURITY_GROUP'] = 'Security group';
$mod_strings['LBL_UPDATE'] = 'Update';
$mod_strings['LBL_EMAIL_ALERT'] = 'Email alert';
$mod_strings['LBL_FIELD_UPDATE'] = 'Field update';
$mod_strings['LBL_MANAGE'] = 'Manage';
$mod_strings['LBL_TYPE'] = 'Type';
$mod_strings['LBL_ACTION'] = 'Action';
$mod_strings['LBL_DESCRIPTION'] = 'Description';
$mod_strings['LBL_CRITERIA'] = 'Criteria';
$mod_strings['LBL_ORDER'] = 'Order';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_APPROVERS'] = 'Approvers';
$mod_strings['LBL_ELSE'] = 'ELSE';
$mod_strings['LBL_BEHAVIOURS_ARRAY']['approve'] = 'Approve record';
$mod_strings['LBL_BEHAVIOURS_ARRAY']['next'] = 'Go to next step';
$mod_strings['LBL_BEHAVIOURS_ARRAY']['reject'] = 'Reject record';
$mod_strings['LBL_ACTION_TYPE_ARRAY']['fieldupdate'] = 'Field update';
$mod_strings['LBL_ACTION_TYPE_ARRAY']['emailalert'] = 'Email alert';
$mod_strings['LBL_STEP'] = 'Step';
$mod_strings['LBL_NEW_STEP'] = 'New step';
$mod_strings['LBL_NEW_FIELD_UPDATE'] = 'New field update';
$mod_strings['LBL_NEW_EMAIL_ALERT'] = 'New email alert';
$mod_strings['LBL_NO_ENTRY_CRITERIAS'] = 'No entry criterias set. All entities from the module will enter the approval.';
$mod_strings['LBL_NO_STEPS'] = 'No steps set.';
$mod_strings['LBL_NO_ACTIONS'] = 'No actions set.';
$mod_strings['LBL_CRITERIAS'] = 'Criterias';
$mod_strings['LBL_ID'] = 'ID';
$mod_strings['LBL_MODULE'] = 'Module';
$mod_strings['LBL_FIELD'] = 'Field';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE'] = 'Value';
$mod_strings['LBL_FORMULA'] = 'Formula';
$mod_strings['LBL_EXAMPLE_FORMULA'] = 'Example formula';
$mod_strings['LBL_IF_CRITERIA_NOT_MET'] = 'If criteria not met';
$mod_strings['LBL_SAVE'] = 'Save';
$mod_strings['LBL_CANCEL'] = 'Cancel';
$mod_strings['LBL_NEW_CRITERIA'] = 'New criteria';
$mod_strings['LBL_REALLY_DELETE'] = 'Do you really want to delete this element?';
$mod_strings['LBL_EMAIL_TEMPLATE'] = 'E-mail template';
$mod_strings['LBL_RECIPIENTS'] = 'Recipients';
$mod_strings['LBL_ADD'] = 'Add';
$mod_strings['LBL_BASIC'] = 'Basic';
$mod_strings['LBL_RECIPIENT_TYPE_ARRAY']['creator'] = 'Creator user';
$mod_strings['LBL_RECIPIENT_TYPE_ARRAY']['assigned'] = 'Assigned user';
$mod_strings['LBL_RECIPIENT_TYPE_ARRAY']['user'] = 'User';
$mod_strings['LBL_RECIPIENT_TYPE_ARRAY']['email'] = 'E-mail address';
$mod_strings['LBL_APPROVAL_METHOD'] = 'Approval method';
$mod_strings['LBL_APPROVAL_METHOD_ARRAY']['single'] = 'Only one approval is enough';
$mod_strings['LBL_APPROVAL_METHOD_ARRAY']['all'] = "Everyone's approval is needed";

?>