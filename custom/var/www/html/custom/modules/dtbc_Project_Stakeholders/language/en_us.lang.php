<?php
// created: 2017-09-26 13:52:55
$mod_strings = array (
  'LBL_EMAIL_ADDRESS' => 'Email Address',
  'LBL_NOTE' => 'Note',
  'LBL_PHONE' => 'Phone',
  'LBL_POSITION' => 'Position',
  'LBL_COMPANY' => 'Company',
);