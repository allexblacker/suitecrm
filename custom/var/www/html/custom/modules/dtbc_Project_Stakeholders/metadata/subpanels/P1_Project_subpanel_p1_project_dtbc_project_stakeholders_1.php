<?php
// created: 2017-10-17 15:08:40
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'company_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_COMPANY',
    'width' => '10%',
  ),
  'position_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_POSITION',
    'width' => '10%',
  ),
  'phone_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PHONE',
    'width' => '10%',
  ),
  'email_address_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_EMAIL_ADDRESS',
    'width' => '10%',
  ),
  'note_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_NOTE',
    'width' => '10%',
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'dtbc_Project_Stakeholders',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'dtbc_Project_Stakeholders',
    'width' => '5%',
    'default' => true,
  ),
);