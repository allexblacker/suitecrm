<?php

require_once('modules/Tasks/Task.php');

class CustomTask extends Task {
	
	function fill_in_additional_parent_fields()
	{
		parent::fill_in_additional_parent_fields();
		
		if ($this->parent_type != "Cases")
			return;
		
		global $db;
		
		$sql = "SELECT case_number
				FROM cases
				WHERE id = " . $db->quoted($this->parent_id);
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0)
			$this->parent_name = $res['case_number'];		
	}
}

?>