<?php

require_once('custom/modules/Tasks/customTask.php');

class CustomTasksController extends SugarController {
	

	public function loadBean()
	{
		$this->bean = new CustomTask();
		if(!empty($this->record)){
			$this->bean->retrieve($this->record);
			if($this->bean)
				$GLOBALS['FOCUS'] = $this->bean;
		}
	}
}
?>