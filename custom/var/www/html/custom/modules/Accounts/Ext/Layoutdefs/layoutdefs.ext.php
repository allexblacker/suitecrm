<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-12 13:42:50
$layout_defs["Accounts"]["subpanel_setup"]['accounts_fsk1_field_service_kit_1'] = array (
  'order' => 100,
  'module' => 'FSK1_Field_Service_Kit',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
  'get_subpanel_data' => 'accounts_fsk1_field_service_kit_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
	$layout_defs['Accounts']['subpanel_setup']['ins_sales'] = array(
		'order' => 100,
		'module' => 'IS1_Inside_Sales',
		'get_subpanel_data'=>'function:getInsideSales',
		'sort_order' => 'asc',
		'sort_by' => 'name',
		'subpanel_name' => 'default',
		'title_key' => 'LBL_INS_SALES', 
		'generate_select' => false,
		'function_parameters' => array(
			'import_function_file' => 'custom/modules/Accounts/accountSubPanels.php',
			'account_id' => $this->_focus->id,
		)
	);
        
        $layout_defs['Accounts']['subpanel_setup']['outs_sales'] = array(
                'order' => 100,
                'module' => 'OS1_Outside_Sales_Activity',
                'get_subpanel_data'=>'function:getOutsideSales',
                'sort_order' => 'asc',
                'sort_by' => 'name',
                'subpanel_name' => 'default',
                'title_key' => 'LBL_OUTS_SALES', 
                'generate_select' => false,
                'function_parameters' => array(
                        'import_function_file' => 'custom/modules/Accounts/accountSubPanels.php',
                        'account_id' => $this->_focus->id,
                )
        );*/
        
        $layout_defs['Accounts']['subpanel_setup']['sites'] = array(
                'order' => 100,
                'module' => 'S1_Site',
                'get_subpanel_data'=>'function:getAccountSites',
                'sort_order' => 'asc',
                'sort_by' => 'name',
                'subpanel_name' => 'default',
                'title_key' => 'LBL_ACCOUNT_SITE', 
                'generate_select' => false,
                'function_parameters' => array(
                        'import_function_file' => 'custom/modules/Accounts/accountSubPanels.php',
                        'account_id' => $this->_focus->id,
                )
        );       

 // created: 2017-09-20 16:42:55
$layout_defs["Accounts"]["subpanel_setup"]['accounts_al1_alliance_transaction_1'] = array (
  'order' => 100,
  'module' => 'AL1_Alliance_Transaction',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE',
  'get_subpanel_data' => 'accounts_al1_alliance_transaction_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-09-25 16:58:45
$layout_defs["Accounts"]["subpanel_setup"]['accounts_p1_project_1'] = array (
  'order' => 100,
  'module' => 'P1_Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
  'get_subpanel_data' => 'accounts_p1_project_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-25 18:47:53
$layout_defs["Accounts"]["subpanel_setup"]['accounts_is1_inside_sales_1'] = array (
  'order' => 100,
  'module' => 'IS1_Inside_Sales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE',
  'get_subpanel_data' => 'accounts_is1_inside_sales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-25 18:47:11
$layout_defs["Accounts"]["subpanel_setup"]['accounts_os1_outside_sales_activity_1'] = array (
  'order' => 100,
  'module' => 'OS1_Outside_Sales_Activity',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
  'get_subpanel_data' => 'accounts_os1_outside_sales_activity_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-24 19:32:30
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ci1_competitive_intelligence_1'] = array (
  'order' => 100,
  'module' => 'CI1_Competitive_Intelligence',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE',
  'get_subpanel_data' => 'accounts_ci1_competitive_intelligence_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-18 10:13:09
$layout_defs["Accounts"]["subpanel_setup"]['accounts_pos1_pos_tracking_1'] = array (
  'order' => 100,
  'module' => 'POS1_POS_Tracking',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE',
  'get_subpanel_data' => 'accounts_pos1_pos_tracking_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



unset($layout_defs['Accounts']['subpanel_setup']['bugs']);
unset($layout_defs['Accounts']['subpanel_setup']['project']);




 // created: 2017-09-21 16:32:04
$layout_defs["Accounts"]["subpanel_setup"]['accounts_dtbc_year_alliance_points_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Year_Alliance_Points',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE',
  'get_subpanel_data' => 'accounts_dtbc_year_alliance_points_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



$layout_defs['Accounts']['subpanel_setup']['accounts_al1_alliance_transaction_1']['top_buttons'] = array (
    0 => array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_p1_project_1']['override_subpanel_name'] = 'Account_subpanel_accounts_p1_project_1';



if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$bean = BeanFactory::getBean("Cases", $_REQUEST['record']);

$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = null;

if($bean->case_record_type_c != "compensation"){
	unset($layout_defs['Cases']['subpanel_setup']['cases_cases_1cases_ida']);
}

$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1']['top_buttons'] = array();


$layout_defs['Accounts']['subpanel_setup']['accounts_dtbc_year_alliance_points_1']['top_buttons'][] = array (
      'widget_class' => 'SubPanelTopRefreshAccountButton',
    );

//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['cases']['override_subpanel_name'] = 'Account_subpanel_cases';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['contacts']['override_subpanel_name'] = 'Account_subpanel_contacts';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['opportunities']['override_subpanel_name'] = 'Account_subpanel_opportunities';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_is1_inside_sales_1']['override_subpanel_name'] = 'Account_subpanel_accounts_is1_inside_sales_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_pos1_pos_tracking_1']['override_subpanel_name'] = 'Account_subpanel_accounts_pos1_pos_tracking_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_os1_outside_sales_activity_1']['override_subpanel_name'] = 'Account_subpanel_accounts_os1_outside_sales_activity_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_ci1_competitive_intelligence_1']['override_subpanel_name'] = 'Account_subpanel_accounts_ci1_competitive_intelligence_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_al1_alliance_transaction_1']['override_subpanel_name'] = 'Account_subpanel_accounts_al1_alliance_transaction_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['accounts_fsk1_field_service_kit_1']['override_subpanel_name'] = 'Account_subpanel_accounts_fsk1_field_service_kit_1';


//auto-generated file DO NOT EDIT
$layout_defs['Accounts']['subpanel_setup']['sites']['override_subpanel_name'] = 'Account_subpanel_sites';

?>