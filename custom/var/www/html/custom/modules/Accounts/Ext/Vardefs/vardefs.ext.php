<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-19 15:08:36
$dictionary['Account']['fields']['kw_factor__c']['inline_edit']='1';
$dictionary['Account']['fields']['kw_factor__c']['labelValue']='KW Factor';

 

// created: 2017-10-12 13:42:50
$dictionary["Account"]["fields"]["accounts_fsk1_field_service_kit_1"] = array (
  'name' => 'accounts_fsk1_field_service_kit_1',
  'type' => 'link',
  'relationship' => 'accounts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'module' => 'FSK1_Field_Service_Kit',
  'bean_name' => 'FSK1_Field_Service_Kit',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
);


 // created: 2017-10-19 15:05:26
$dictionary['Account']['fields']['vendor_number_c']['inline_edit']='1';
$dictionary['Account']['fields']['vendor_number_c']['labelValue']='Vendor Number';

 

 // created: 2017-11-19 17:02:51
$dictionary['Account']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2017-10-10 12:14:36
$dictionary['Account']['fields']['account_target_formula_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_target_formula_c']['labelValue']='Account Target Formula';

 

 // created: 2017-08-29 14:12:54
$dictionary['Account']['fields']['county_c']['inline_edit']='1';
$dictionary['Account']['fields']['county_c']['labelValue']='County';

 

 // created: 2017-10-19 15:24:42
$dictionary['Account']['fields']['sunrun_c']['inline_edit']='1';
$dictionary['Account']['fields']['sunrun_c']['labelValue']='Sunrun';

 

 // created: 2017-10-19 15:15:04
$dictionary['Account']['fields']['account_target_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_target_c']['labelValue']='Account Target';

 

 // created: 2017-08-23 12:28:50
$dictionary['Account']['fields']['state_c']['inline_edit']='1';
$dictionary['Account']['fields']['state_c']['labelValue']='State';

 

 // created: 2017-10-09 13:24:14
$dictionary['Account']['fields']['us_county_c']['inline_edit']='1';
$dictionary['Account']['fields']['us_county_c']['labelValue']='US County';

 

 // created: 2017-10-19 15:16:13
$dictionary['Account']['fields']['crews_c']['inline_edit']='1';
$dictionary['Account']['fields']['crews_c']['labelValue']='# crews';

 

 // created: 2017-10-16 14:48:52
$dictionary['Account']['fields']['type_of_installation_c']['inline_edit']='1';
$dictionary['Account']['fields']['type_of_installation_c']['labelValue']='Type Of Installation';

 

 // created: 2017-08-14 15:51:55
$dictionary['Account']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-08-14 15:51:55
$dictionary['Account']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-10-19 15:20:17
$dictionary['Account']['fields']['training_tier_2_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['training_tier_2_date_c']['labelValue']='SOP Training Date';

 

 // created: 2017-10-09 13:18:34
$dictionary['Account']['fields']['vip_customer_c']['inline_edit']='1';
$dictionary['Account']['fields']['vip_customer_c']['labelValue']='Sunpower Flag';

 

 // created: 2017-11-29 01:19:58
$dictionary['Account']['fields']['inactive_reason_c']['inline_edit']='1';
$dictionary['Account']['fields']['inactive_reason_c']['labelValue']='Inactive Reason';

 

 // created: 2017-10-17 18:30:29
$dictionary['Account']['fields']['best_selling_inverter_c']['inline_edit']='1';
$dictionary['Account']['fields']['best_selling_inverter_c']['labelValue']='Best selling inverter';

 

 // created: 2017-11-28 06:09:20
$dictionary['Account']['fields']['account_currency_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_currency_c']['labelValue']='Account Currency';

 

 // created: 2017-10-19 15:05:06
$dictionary['Account']['fields']['last_year_installed_kwp_c']['inline_edit']='1';
$dictionary['Account']['fields']['last_year_installed_kwp_c']['labelValue']='last year installed kWp';

 

 // created: 2017-10-19 15:04:47
$dictionary['Account']['fields']['vendor_name_c']['inline_edit']='1';
$dictionary['Account']['fields']['vendor_name_c']['labelValue']='Vendor Name';

 

 // created: 2017-10-19 15:13:57
$dictionary['Account']['fields']['activity_focus_c']['inline_edit']='1';
$dictionary['Account']['fields']['activity_focus_c']['labelValue']='Activity focus';

 

 // created: 2017-10-19 15:22:42
$dictionary['Account']['fields']['not_currently_satisfied_c']['inline_edit']='1';
$dictionary['Account']['fields']['not_currently_satisfied_c']['labelValue']='Not currently satisfied';

 

 // created: 2017-10-19 15:04:10
$dictionary['Account']['fields']['project_month_c']['inline_edit']='1';
$dictionary['Account']['fields']['project_month_c']['labelValue']='project month';

 

 // created: 2017-10-30 15:02:14
$dictionary['Account']['fields']['account_source_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_source_c']['labelValue']='Account Source';

 

 // created: 2017-09-22 11:48:39
$dictionary['Account']['fields']['allow_alliance_plan_email_c']['inline_edit']='1';
$dictionary['Account']['fields']['allow_alliance_plan_email_c']['labelValue']='Allow Alliance Plan Email';

 

 // created: 2017-10-09 11:57:01
$dictionary['Account']['fields']['days_without_alliance_transa_c']['inline_edit']='1';
$dictionary['Account']['fields']['days_without_alliance_transa_c']['labelValue']='Days Without Alliance Transaction';

 

 // created: 2017-10-19 15:03:50
$dictionary['Account']['fields']['monitoring_id_c']['inline_edit']='1';
$dictionary['Account']['fields']['monitoring_id_c']['labelValue']='Monitoring ID';

 

 // created: 2017-11-29 01:19:28
$dictionary['Account']['fields']['customer_funnel_c']['inline_edit']='1';
$dictionary['Account']['fields']['customer_funnel_c']['labelValue']='Prospect Funnel';

 

 // created: 2017-10-26 15:48:44
$dictionary['Account']['fields']['shipping_address_street']['len']='255';
$dictionary['Account']['fields']['shipping_address_street']['inline_edit']=true;
$dictionary['Account']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['Account']['fields']['shipping_address_street']['merge_filter']='disabled';

 

 // created: 2017-08-14 15:51:55
$dictionary['Account']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-10-19 15:03:23
$dictionary['Account']['fields']['support_comments_c']['inline_edit']='1';
$dictionary['Account']['fields']['support_comments_c']['labelValue']='Support comments';

 

 // created: 2017-10-09 13:16:46
$dictionary['Account']['fields']['service_level_flag_c']['inline_edit']='1';
$dictionary['Account']['fields']['service_level_flag_c']['labelValue']='Service Level Flag';

 

 // created: 2017-10-19 15:14:42
$dictionary['Account']['fields']['phone_c']['inline_edit']='1';
$dictionary['Account']['fields']['phone_c']['labelValue']='Phone';

 

// created: 2017-09-20 16:42:55
$dictionary["Account"]["fields"]["accounts_al1_alliance_transaction_1"] = array (
  'name' => 'accounts_al1_alliance_transaction_1',
  'type' => 'link',
  'relationship' => 'accounts_al1_alliance_transaction_1',
  'source' => 'non-db',
  'module' => 'AL1_Alliance_Transaction',
  'bean_name' => 'AL1_Alliance_Transaction',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE',
);


// created: 2017-09-25 16:58:45
$dictionary["Account"]["fields"]["accounts_p1_project_1"] = array (
  'name' => 'accounts_p1_project_1',
  'type' => 'link',
  'relationship' => 'accounts_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-08-23 12:28:02
$dictionary['Account']['fields']['recalculate_alliance_c']['inline_edit']='1';
$dictionary['Account']['fields']['recalculate_alliance_c']['labelValue']='Recalculate Alliance';

 

 // created: 2017-10-19 15:04:26
$dictionary['Account']['fields']['survey_200_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_200_c']['labelValue']='Survey 200';

 

 // created: 2017-10-19 15:07:58
$dictionary['Account']['fields']['financing_partners_c']['inline_edit']='1';
$dictionary['Account']['fields']['financing_partners_c']['labelValue']='Financing Partners';

 

 // created: 2017-10-16 14:50:10
$dictionary['Account']['fields']['segment_c']['inline_edit']='1';
$dictionary['Account']['fields']['segment_c']['labelValue']='Segment';

 

 // created: 2017-10-19 15:22:24
$dictionary['Account']['fields']['mass_update_admin_c']['inline_edit']='1';
$dictionary['Account']['fields']['mass_update_admin_c']['labelValue']='MASS update ADMIN';

 

 // created: 2017-10-19 15:03:04
$dictionary['Account']['fields']['decision_maker_c']['inline_edit']='1';
$dictionary['Account']['fields']['decision_maker_c']['labelValue']='Decision maker';

 

 // created: 2017-10-19 15:19:37
$dictionary['Account']['fields']['op_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['op_date_c']['labelValue']='$/OP DATE';

 

 // created: 2017-10-19 15:22:05
$dictionary['Account']['fields']['sunpower_c']['inline_edit']='1';
$dictionary['Account']['fields']['sunpower_c']['labelValue']='SunPower';

 

 // created: 2017-10-19 15:02:47
$dictionary['Account']['fields']['survey_10_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_10_c']['labelValue']='SPA date';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Account']['fields']['email_points_total'] = array(
	'name' => 'email_points_total',
	'label' => 'LBL_POINTS_TOTAL',
	'vname' => 'LBL_POINTS_TOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_points_total',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	), 
);

$dictionary['Account']['fields']['email_points_table'] = array(
	'name' => 'email_points_table',
	'label' => 'LBL_POINTS_TABLE',
	'vname' => 'LBL_POINTS_TABLE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_points_table',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	), 
);

 // created: 2017-10-19 15:07:36
$dictionary['Account']['fields']['inverter_brands_c']['inline_edit']='1';
$dictionary['Account']['fields']['inverter_brands_c']['labelValue']='Inverter brands';

 

 // created: 2017-11-04 00:04:54
$dictionary['Account']['fields']['exclude_from_forecast_c']['inline_edit']='1';
$dictionary['Account']['fields']['exclude_from_forecast_c']['labelValue']='Exclude from forecast';

 

 // created: 2017-10-19 15:02:23
$dictionary['Account']['fields']['last_year_tot_installed_c']['inline_edit']='1';
$dictionary['Account']['fields']['last_year_tot_installed_c']['labelValue']='last year total systems installed';

 

 // created: 2017-10-23 15:04:01
$dictionary['Account']['fields']['account_type']['len']=100;
$dictionary['Account']['fields']['account_type']['required']=true;
$dictionary['Account']['fields']['account_type']['inline_edit']=true;
$dictionary['Account']['fields']['account_type']['comments']='The Company is of this type';
$dictionary['Account']['fields']['account_type']['merge_filter']='disabled';
$dictionary['Account']['fields']['account_type']['audited']=true;

 

 // created: 2017-10-19 15:15:30
$dictionary['Account']['fields']['marketing_comment_timestamp_c']['inline_edit']='1';
$dictionary['Account']['fields']['marketing_comment_timestamp_c']['labelValue']='Marketing Comment TimeStamp';

 

 // created: 2017-10-19 15:06:47
$dictionary['Account']['fields']['inverter_competitors_c']['inline_edit']='1';
$dictionary['Account']['fields']['inverter_competitors_c']['labelValue']='Inverter Competitors';

 

 // created: 2017-08-24 10:04:06
$dictionary['Account']['fields']['city_c']['inline_edit']='1';
$dictionary['Account']['fields']['city_c']['labelValue']='City';

 

 // created: 2017-10-30 15:25:03
$dictionary['Account']['fields']['csr_c']['inline_edit']='1';
$dictionary['Account']['fields']['csr_c']['labelValue']='CSR';

 

 // created: 2017-10-19 15:02:07
$dictionary['Account']['fields']['welcome_kit_c']['inline_edit']='1';
$dictionary['Account']['fields']['welcome_kit_c']['labelValue']='Welcome Kit';

 

 // created: 2017-10-19 15:01:50
$dictionary['Account']['fields']['customer_name_in_priority_c']['inline_edit']='1';
$dictionary['Account']['fields']['customer_name_in_priority_c']['labelValue']='Customer Name In Priority';

 

 // created: 2017-10-19 15:05:51
$dictionary['Account']['fields']['marketing_comments_c']['inline_edit']='1';
$dictionary['Account']['fields']['marketing_comments_c']['labelValue']='Marketing Comments';

 

 // created: 2017-10-19 15:01:33
$dictionary['Account']['fields']['module_share_in_c']['inline_edit']='1';
$dictionary['Account']['fields']['module_share_in_c']['labelValue']='Module share in %';

 

 // created: 2017-10-12 15:11:38
$dictionary['Account']['fields']['pos_transaction_year_c']['inline_edit']='1';
$dictionary['Account']['fields']['pos_transaction_year_c']['labelValue']='POS Transaction year';

 

 // created: 2017-11-29 01:16:46
$dictionary['Account']['fields']['us_region_c']['inline_edit']='1';
$dictionary['Account']['fields']['us_region_c']['labelValue']='US Region';

 

 // created: 2017-10-09 13:17:18
$dictionary['Account']['fields']['solarcity_account_flag_c']['inline_edit']='1';
$dictionary['Account']['fields']['solarcity_account_flag_c']['labelValue']='Solarcity Account Flag';

 

 // created: 2017-10-09 16:19:56
$dictionary['Account']['fields']['allow_loyalty_plan_email_c']['inline_edit']='1';
$dictionary['Account']['fields']['allow_loyalty_plan_email_c']['labelValue']='Allow Loyalty Plan email';

 

 // created: 2017-08-22 11:37:18
$dictionary['Account']['fields']['inside_sales_owner_c']['inline_edit']='1';
$dictionary['Account']['fields']['inside_sales_owner_c']['labelValue']='Inside Sales Owner';

 

 // created: 2017-10-19 15:19:15
$dictionary['Account']['fields']['first_install_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['first_install_date_c']['labelValue']='First Install Date';

 

 // created: 2017-10-19 14:45:05
$dictionary['Account']['fields']['customer_number_in_priority_c']['inline_edit']='1';
$dictionary['Account']['fields']['customer_number_in_priority_c']['labelValue']='Customer Number In Priority';

 

 // created: 2017-10-19 14:44:45
$dictionary['Account']['fields']['installer_c']['inline_edit']='1';
$dictionary['Account']['fields']['installer_c']['labelValue']='Installer';

 

 // created: 2017-10-19 14:55:40
$dictionary['Account']['fields']['avg_proj_kw_c']['inline_edit']='1';
$dictionary['Account']['fields']['avg_proj_kw_c']['labelValue']='Avg Project KW (Res/Com)';

 

 // created: 2017-10-19 14:44:01
$dictionary['Account']['fields']['other_inverter_brands_c']['inline_edit']='1';
$dictionary['Account']['fields']['other_inverter_brands_c']['labelValue']='Other inverter brands';

 

 // created: 2017-10-19 15:21:11
$dictionary['Account']['fields']['solaredge_optimized_partner_c']['inline_edit']='1';
$dictionary['Account']['fields']['solaredge_optimized_partner_c']['labelValue']='SolarEdge Optimized Partner';

 

 // created: 2017-10-16 14:50:41
$dictionary['Account']['fields']['region_c']['inline_edit']='1';
$dictionary['Account']['fields']['region_c']['labelValue']='Region';

 

// created: 2017-10-25 18:47:53
$dictionary["Account"]["fields"]["accounts_is1_inside_sales_1"] = array (
  'name' => 'accounts_is1_inside_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_is1_inside_sales_1',
  'source' => 'non-db',
  'module' => 'IS1_Inside_Sales',
  'bean_name' => 'IS1_Inside_Sales',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE',
);


 // created: 2017-10-15 19:28:43
$dictionary['Account']['fields']['certified_installer_tesla_c']['inline_edit']='1';
$dictionary['Account']['fields']['certified_installer_tesla_c']['labelValue']='Certified Installer Tesla';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Account']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-10-19 15:06:15
$dictionary['Account']['fields']['main_panel_brands_c']['inline_edit']='1';
$dictionary['Account']['fields']['main_panel_brands_c']['labelValue']='Main panel brands';

 

// created: 2017-10-25 18:47:12
$dictionary["Account"]["fields"]["accounts_os1_outside_sales_activity_1"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1',
  'type' => 'link',
  'relationship' => 'accounts_os1_outside_sales_activity_1',
  'source' => 'non-db',
  'module' => 'OS1_Outside_Sales_Activity',
  'bean_name' => 'OS1_Outside_Sales_Activity',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
);


 // created: 2017-10-19 14:55:16
$dictionary['Account']['fields']['sedg_c']['inline_edit']='1';
$dictionary['Account']['fields']['sedg_c']['labelValue']='% SEDG (Res/Com)';

 

 // created: 2017-11-29 01:18:09
$dictionary['Account']['fields']['active_spa_c']['inline_edit']='1';
$dictionary['Account']['fields']['active_spa_c']['labelValue']='Active SPA';

 

 // created: 2017-10-19 15:18:56
$dictionary['Account']['fields']['welcome_kit_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['welcome_kit_date_c']['labelValue']='Welcome Kit Date';

 

 // created: 2017-10-09 11:53:18
$dictionary['Account']['fields']['service_lvl_is_missing_c']['inline_edit']='1';
$dictionary['Account']['fields']['service_lvl_is_missing_c']['labelValue']='Account Service lvl is missing';

 

 // created: 2017-10-19 15:18:37
$dictionary['Account']['fields']['survey_01_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_01_date_c']['labelValue']='Survey 01 Date';

 

 // created: 2017-10-19 15:18:15
$dictionary['Account']['fields']['outside_sales_appt_set_by_c']['inline_edit']='1';
$dictionary['Account']['fields']['outside_sales_appt_set_by_c']['labelValue']='Outside Sales Appt set by IST';

 

 // created: 2017-11-19 17:02:51
$dictionary['Account']['fields']['parent_account_c']['inline_edit']='1';
$dictionary['Account']['fields']['parent_account_c']['labelValue']='parent account';

 

 // created: 2017-10-09 12:00:51
$dictionary['Account']['fields']['language_c']['inline_edit']='1';
$dictionary['Account']['fields']['language_c']['labelValue']='Language';

 

 // created: 2017-10-15 19:27:40
$dictionary['Account']['fields']['business_entity_c']['inline_edit']='1';
$dictionary['Account']['fields']['business_entity_c']['labelValue']='Business Entity';

 

 // created: 2017-10-09 12:01:41
$dictionary['Account']['fields']['pos_last_transaction_y_and_q_c']['inline_edit']='1';
$dictionary['Account']['fields']['pos_last_transaction_y_and_q_c']['labelValue']='Last POS transaction Quarter & Year';

 

// created: 2017-10-24 19:32:31
$dictionary["Account"]["fields"]["accounts_ci1_competitive_intelligence_1"] = array (
  'name' => 'accounts_ci1_competitive_intelligence_1',
  'type' => 'link',
  'relationship' => 'accounts_ci1_competitive_intelligence_1',
  'source' => 'non-db',
  'module' => 'CI1_Competitive_Intelligence',
  'bean_name' => 'CI1_Competitive_Intelligence',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE',
);


 // created: 2017-10-19 14:41:23
$dictionary['Account']['fields']['sales_manager_c']['inline_edit']='1';
$dictionary['Account']['fields']['sales_manager_c']['labelValue']='Sales manager';

 

 // created: 2017-10-19 15:17:57
$dictionary['Account']['fields']['survey_100_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_100_date_c']['labelValue']='Survey 100 Date';

 


$dictionary["Account"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-10-19 15:09:17
$dictionary['Account']['fields']['se_training_visited_c']['inline_edit']='1';
$dictionary['Account']['fields']['se_training_visited_c']['labelValue']='SE training visited';

 

 // created: 2017-10-09 13:26:52
$dictionary['Account']['fields']['target_m_c']['inline_edit']='1';
$dictionary['Account']['fields']['target_m_c']['labelValue']='Target ($M)';

 

 // created: 2017-10-09 11:55:01
$dictionary['Account']['fields']['customer_satisfaction_rating_c']['inline_edit']='1';
$dictionary['Account']['fields']['customer_satisfaction_rating_c']['labelValue']='Customer Satisfaction Rating';

 
 

$dictionary["Account"]["fields"]["Loyalty_Plan_start_date__c"] = array(
	'name' => 'Loyalty_Plan_start_date__c',
	'label' => 'LBL_FUNCTION_LOYALTY_START',
	'vname' => 'LBL_FUNCTION_LOYALTY_START',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_loyaltyStart',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Balance__c"] = array(
	'name' => 'Balance__c',
	'label' => 'LBL_FUNCTION_BALANCE',
	'vname' => 'LBL_FUNCTION_BALANCE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_balance',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["KW__c"] = array(
	'name' => 'KW__c',
	'label' => 'LBL_FUNCTION_TOTAL_KW',
	'vname' => 'LBL_FUNCTION_TOTAL_KW',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalKw',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Extra__c"] = array(
	'name' => 'Extra__c',
	'label' => 'LBL_FUNCTION_EXTRA',
	'vname' => 'LBL_FUNCTION_EXTRA',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_extra',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["No_of_unique_sites__c"] = array(
	'name' => 'No_of_unique_sites__c',
	'label' => 'LBL_FUNCTION_UNIQUE_SITES',
	'vname' => 'LBL_FUNCTION_UNIQUE_SITES',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_uniqueSites',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Last_Alliance_Transaction__c"] = array(
	'name' => 'Last_Alliance_Transaction__c',
	'label' => 'LBL_FUNCTION_LAST_TRANSACTION',
	'vname' => 'LBL_FUNCTION_LAST_TRANSACTION',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lastTransaction',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2013_KW__c"] = array(
	'name' => 'Year_2013_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2013',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2013',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2013',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2014_KW__c"] = array(
	'name' => 'Year_2014_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2014',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2014',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2014',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2015_KW__c"] = array(
	'name' => 'Year_2015_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2015',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2015',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2015',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2016_KW__c"] = array(
	'name' => 'Year_2016_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2016',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2016',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2016',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["Year_2017_KW__c"] = array(
	'name' => 'Year_2017_KW__c',
	'label' => 'LBL_FUNCTION_YEAR_KW_2017',
	'vname' => 'LBL_FUNCTION_YEAR_KW_2017',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_totalYearKw',
		'returns' => 'html',
		'params' => '2017',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);


$dictionary["Account"]["fields"]["last_pos_transaction_c"] = array(
	'name' => 'last_pos_transaction_c',
	'label' => 'LBL_FUNCTION_LAST_POS_TRANSACTION',
	'vname' => 'LBL_FUNCTION_LAST_POS_TRANSACTION',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_lastPOSTransaction',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["of_opportunities_c"] = array(
	'name' => 'of_opportunities_c',
	'label' => 'LBL_FUNCTION_OF_OPPORTUNITIES',
	'vname' => 'LBL_FUNCTION_OF_OPPORTUNITIES',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_ofOpportunities',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["checkiffirstopp_c"] = array(
	'name' => 'checkiffirstopp_c',
	'label' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
	'vname' => 'LBL_FUNCTION_CHECKIFFIRSTOPP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_checkIfFirstOpp',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["service_kit_c"] = array(
	'name' => 'service_kit_c',
	'label' => 'LBL_FUNCTION_SERVICE_KIT',
	'vname' => 'LBL_FUNCTION_SERVICE_KIT',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_serviceKit',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

$dictionary["Account"]["fields"]["new_account_button_c"] = array(
	'name' => 'new_account_button_c',
	'label' => 'LBL_FUNCTION_NEW_ACCOUNT_BUTTON',
	'vname' => 'LBL_FUNCTION_NEW_ACCOUNT_BUTTON',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_newAccountButton',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/accountsFunctionFields.php',
	),
);

 // created: 2017-10-19 14:40:59
$dictionary['Account']['fields']['tax_id_c']['inline_edit']='1';
$dictionary['Account']['fields']['tax_id_c']['labelValue']='tax ID';

 

 // created: 2017-10-09 13:16:24
$dictionary['Account']['fields']['location_c']['inline_edit']='1';
$dictionary['Account']['fields']['location_c']['labelValue']='Location';

 

 // created: 2017-10-19 14:40:04
$dictionary['Account']['fields']['inside_sales_comments_c']['inline_edit']='1';
$dictionary['Account']['fields']['inside_sales_comments_c']['labelValue']='Inside Sales Comments';

 

 // created: 2017-09-03 15:26:18
$dictionary['Account']['fields']['solaredge_service_kit_holder_c']['inline_edit']='1';
$dictionary['Account']['fields']['solaredge_service_kit_holder_c']['labelValue']='solaredge service kit holder';

 

 // created: 2017-10-09 12:00:39
$dictionary['Account']['fields']['department_c']['inline_edit']='1';
$dictionary['Account']['fields']['department_c']['labelValue']='Department';

 

 // created: 2017-08-29 14:13:34
$dictionary['Account']['fields']['place_name_c']['inline_edit']='1';
$dictionary['Account']['fields']['place_name_c']['labelValue']='Place Name (Canada only):';

 

 // created: 2017-10-19 14:39:37
$dictionary['Account']['fields']['street_c']['inline_edit']='1';
$dictionary['Account']['fields']['street_c']['labelValue']='Street';

 

 // created: 2017-10-09 13:20:06
$dictionary['Account']['fields']['usa_zip_code_c']['inline_edit']='1';
$dictionary['Account']['fields']['usa_zip_code_c']['labelValue']='USA Zip code';

 

 // created: 2017-10-17 18:22:27
$dictionary['Account']['fields']['account_owner_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_owner_c']['labelValue']='Account Owner';

 

 // created: 2017-10-19 15:15:52
$dictionary['Account']['fields']['target_kw_c']['inline_edit']='1';
$dictionary['Account']['fields']['target_kw_c']['labelValue']='Target kW';

 

 // created: 2017-10-19 15:17:36
$dictionary['Account']['fields']['promotion_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['promotion_date_c']['labelValue']='Promotion Date';

 

 // created: 2017-10-09 13:20:06
$dictionary['Account']['fields']['uzc1_usa_zip_codes_id_c']['inline_edit']=1;

 

// created: 2017-10-18 10:13:10
$dictionary["Account"]["fields"]["accounts_pos1_pos_tracking_1"] = array (
  'name' => 'accounts_pos1_pos_tracking_1',
  'type' => 'link',
  'relationship' => 'accounts_pos1_pos_tracking_1',
  'source' => 'non-db',
  'module' => 'POS1_POS_Tracking',
  'bean_name' => 'POS1_POS_Tracking',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE',
);


 // created: 2017-10-19 14:39:20
$dictionary['Account']['fields']['first_install_c']['inline_edit']='1';
$dictionary['Account']['fields']['first_install_c']['labelValue']='Site Designer Date';

 

 // created: 2017-10-19 14:38:54
$dictionary['Account']['fields']['next_year_target_kwp_c']['inline_edit']='1';
$dictionary['Account']['fields']['next_year_target_kwp_c']['labelValue']='next year target kWp';

 

 // created: 2017-11-07 17:54:16
$dictionary['Account']['fields']['australian_state_c']['inline_edit']='1';
$dictionary['Account']['fields']['australian_state_c']['labelValue']='Australian State';

 

 // created: 2017-10-16 14:47:46
$dictionary['Account']['fields']['canadian_province_c']['inline_edit']='1';
$dictionary['Account']['fields']['canadian_province_c']['labelValue']='Canadian Province';

 

 // created: 2017-10-19 15:17:19
$dictionary['Account']['fields']['case_study_date__c']['inline_edit']='1';
$dictionary['Account']['fields']['case_study_date__c']['labelValue']='Case Study Date';

 

 // created: 2017-10-19 14:38:30
$dictionary['Account']['fields']['zip_postal_code_c']['inline_edit']='1';
$dictionary['Account']['fields']['zip_postal_code_c']['labelValue']='Zip Postal Code';

 

 // created: 2017-10-19 14:54:53
$dictionary['Account']['fields']['promotion_c']['inline_edit']='1';
$dictionary['Account']['fields']['promotion_c']['labelValue']='TIYLI';

 

 // created: 2017-10-19 14:38:10
$dictionary['Account']['fields']['kw_per_month_resi_com_c']['inline_edit']='1';
$dictionary['Account']['fields']['kw_per_month_resi_com_c']['labelValue']='kW per month resi com';

 

 // created: 2017-10-22 17:40:47
$dictionary['Account']['fields']['name']['inline_edit']=true;
$dictionary['Account']['fields']['name']['comments']='Name of the Company';
$dictionary['Account']['fields']['name']['merge_filter']='disabled';

 

 // created: 2017-11-29 01:18:44
$dictionary['Account']['fields']['tier_c']['inline_edit']='1';
$dictionary['Account']['fields']['tier_c']['labelValue']='Tier';

 

 // created: 2017-10-19 15:16:54
$dictionary['Account']['fields']['survey_200_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_200_date_c']['labelValue']='Survey 200 Date';

 

 // created: 2017-10-09 12:02:03
$dictionary['Account']['fields']['pos_transaction_quarter_c']['inline_edit']='1';
$dictionary['Account']['fields']['pos_transaction_quarter_c']['labelValue']='POS Transaction Quarter';

 

 // created: 2017-10-19 14:37:05
$dictionary['Account']['fields']['other_module_brands_c']['inline_edit']='1';
$dictionary['Account']['fields']['other_module_brands_c']['labelValue']='Other Module Brands';

 

 // created: 2017-10-19 14:36:50
$dictionary['Account']['fields']['distribution_partners_c']['inline_edit']='1';
$dictionary['Account']['fields']['distribution_partners_c']['labelValue']='Distribution Partners';

 

 // created: 2017-10-19 14:36:10
$dictionary['Account']['fields']['next_year_total_systems_c']['inline_edit']='1';
$dictionary['Account']['fields']['next_year_total_systems_c']['labelValue']='next year total systems target';

 

 // created: 2017-08-29 14:15:27
$dictionary['Account']['fields']['canadian_province1_c']['inline_edit']='1';
$dictionary['Account']['fields']['canadian_province1_c']['labelValue']='canadian province';

 

 // created: 2017-10-19 14:35:36
$dictionary['Account']['fields']['case_study__c']['inline_edit']='1';
$dictionary['Account']['fields']['case_study__c']['labelValue']='Case Study';

 

 // created: 2017-10-16 14:46:44
$dictionary['Account']['fields']['service_level_c']['inline_edit']='1';
$dictionary['Account']['fields']['service_level_c']['labelValue']='Service Level';

 

 // created: 2017-08-14 15:51:55
$dictionary['Account']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-11-13 12:06:57
$dictionary['Account']['fields']['account_monitoring_id_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_monitoring_id_c']['labelValue']='Account Monitoring ID';

 

 // created: 2017-10-19 14:35:14
$dictionary['Account']['fields']['survey_01_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_01_c']['labelValue']='Survey 01';

 

// created: 2017-09-21 16:32:04
$dictionary["Account"]["fields"]["accounts_dtbc_year_alliance_points_1"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1',
  'type' => 'link',
  'relationship' => 'accounts_dtbc_year_alliance_points_1',
  'source' => 'non-db',
  'module' => 'dtbc_Year_Alliance_Points',
  'bean_name' => 'dtbc_Year_Alliance_Points',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE',
);


 // created: 2017-10-19 15:20:44
$dictionary['Account']['fields']['enphase_c']['inline_edit']='1';
$dictionary['Account']['fields']['enphase_c']['labelValue']='Enphase';

 

 // created: 2017-09-19 08:18:20
$dictionary['Account']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2017-10-19 15:16:35
$dictionary['Account']['fields']['training_tier_1_date_c']['inline_edit']='1';
$dictionary['Account']['fields']['training_tier_1_date_c']['labelValue']='RoadshowTier 1 (Date)';

 

 // created: 2017-10-19 14:34:56
$dictionary['Account']['fields']['number_of_sop_employees_c']['inline_edit']='1';
$dictionary['Account']['fields']['number_of_sop_employees_c']['labelValue']='Number of SOP Employees';

 

 // created: 2017-10-17 19:00:57
$dictionary['Account']['fields']['survey_100_c']['inline_edit']='1';
$dictionary['Account']['fields']['survey_100_c']['labelValue']='Survey 100';

 

 // created: 2017-10-16 14:32:28
$dictionary['Account']['fields']['country_c']['inline_edit']='1';
$dictionary['Account']['fields']['country_c']['labelValue']='Country';

 

 // created: 2017-08-23 12:29:13
$dictionary['Account']['fields']['timestamp_recalculate_c']['inline_edit']='1';
$dictionary['Account']['fields']['timestamp_recalculate_c']['labelValue']='timestamp recalculate alliance points';

 

 // created: 2017-08-22 11:37:18
$dictionary['Account']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2017-10-31 14:50:02
$dictionary['Account']['fields']['distributor_status_c']['inline_edit']='1';
$dictionary['Account']['fields']['distributor_status_c']['labelValue']='Distributor Status';

 
?>