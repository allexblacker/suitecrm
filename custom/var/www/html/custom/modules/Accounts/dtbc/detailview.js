function dtbc_refreshpoints() {
	$.ajax({
		method: "POST",
		url: "index.php?module=accounts&action=refreshPoints&to_pdf=1",
		data: { 
			record_id: $('#formDetailView [name="record"]').val(),
		},
	})
	.done(function (response) {
		showSubPanel('accounts_dtbc_year_alliance_points_1', null, true);
	})
	.fail(function (e) {
		console.log(e);
	}); 
}

