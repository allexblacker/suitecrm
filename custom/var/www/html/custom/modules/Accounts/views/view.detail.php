<?php

require_once('modules/Accounts/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/dtbc/JSLoader.php');

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class CustomAccountsViewDetail extends AccountsViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailView2();
 	    $this->dv->ss =&  $this->ss;
        
		$fieldsToHTMLdecode = array('service_level_flag_c','service_lvl_is_missing_c','customer_satisfaction_rating_c','location_c','solarcity_account_flag_c','vip_customer_c');
		foreach($fieldsToHTMLdecode as $field){
			$this->bean->$field = html_entity_decode($this->bean->$field);
		}
		
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
	public function display(){
        parent::display();
		includeJavaScriptFile('custom/modules/Accounts/dtbc/detailview.js');
		echo "<input type='hidden' id='recordid' value='" . $_REQUEST['record'] . "'/>";
    }
	
}
