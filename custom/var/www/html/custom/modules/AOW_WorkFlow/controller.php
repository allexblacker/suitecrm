<?php

require_once("modules/AOW_WorkFlow/controller.php");

class CustomAOW_WorkFlowController extends AOW_WorkFlowController {

    protected function action_getModuleOperatorField(){

        global $app_list_strings, $beanFiles, $beanList;

        if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
            $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        } else {
            $module = $_REQUEST['aow_module'];
        }
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';


        require_once($beanFiles[$beanList[$module]]);
        $focus = new $beanList[$module];
        $vardef = $focus->getFieldDefinition($fieldname);

        if($vardef){

            switch($vardef['type']) {
                case 'double':
                case 'decimal':
                case 'float':
                case 'currency':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null','is_not_null');
                    break;
                case 'uint':
                case 'ulong':
                case 'long':
                case 'short':
                case 'tinyint':
                case 'int':
				case 'function':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null','is_not_null');
                    break;
                case 'date':
                case 'datetime':
                case 'datetimecombo':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null','is_not_null');
                    break;
                case 'enum':
                case 'multienum':
                $valid_opp = array('Equal_To','Not_Equal_To','is_null','is_not_null');
                    break;
                default:
                $valid_opp = array('Equal_To','Not_Equal_To','Contains', 'Starts_With', 'Ends_With','is_null', 'Not_Contains','is_not_null');
                    break;
            }

            foreach($app_list_strings['aow_operator_list'] as $key => $keyValue){
                if(!in_array($key, $valid_opp)){
                    unset($app_list_strings['aow_operator_list'][$key]);
                }
            }

            $app_list_strings['aow_operator_list'];
            if($view == 'EditView'){
                echo "<select type='text' name='$aow_field' id='$aow_field ' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_operator_list'], $value) ."</select>";
            }else{
                echo $app_list_strings['aow_operator_list'][$value];
            }
        }
        die;
    }
	
	protected function action_getEmailField(){
		
        $module = $_REQUEST['aow_module'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        switch($_REQUEST['aow_type']) {
            case 'Record Email';
                echo '';
                break;
            case 'Related Field':
                $rel_field_list = getRelatedEmailableFields($module);
                if($view == 'EditView'){
                    echo "<select type='text'  name='$aow_field' id='$aow_field' title='' tabindex='116'>". get_select_options_with_id($rel_field_list, $value) ."</select>";
                }else{
                    echo $rel_field_list[$value];
                }
                break;
            case 'Specify User':
                echo getModuleField('Accounts','assigned_user_name', $aow_field, $view, $value);
                break;
            case 'Users':
                echo getAssignField($aow_field, $view, $value);
                break;
			case 'Module_Field':
				echo "<select type='text' name='$aow_field' id='$aow_field' title='' tabindex='116'>" . getModuleFields($module,$view,$value) . "</select>";
				break;
            case 'Email Address':
            default:
                if($view == 'EditView'){
                    echo "<input type='text' name='$aow_field' id='$aow_field' size='25' title='' tabindex='116' value='$value'>";
                }else{
                    echo $value;
                }
                break;
        }
        die;
    }
	
	protected function action_customGetModuleFields(){
		
		if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != '')
            $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        else
            $module = $_REQUEST['aow_module'];
		
		$view = $_REQUEST['view'];
		$aowValue = $_REQUEST['aow_value'];
		$fields = array();
		
		if (empty($module) || empty($view) || empty($aowValue))
			die();
		
		echo getModuleFields($module, $view, $aowValue);
		$this->getFunctionFields($module, $view, $aowValue);
		
		die();
	}
	
	protected function action_customGetModuleFieldType(){
		
		if($this->isFunctionField()){
			if(isset($_REQUEST['aow_value']))
				$value = $_REQUEST['aow_value'];
			else
				$value = '';
			
			$name = $_REQUEST['aow_newfieldname'];
			echo "<input type='text' name='$name' id='$name' value='$value' />";
			die();
		}
		else
			parent::action_getModuleFieldType();
	}
	
	private function isFunctionField(){
		
		if(!isset($_REQUEST['aow_module']) || !isset($_REQUEST['aow_fieldname']) || !isset($_REQUEST['aow_newfieldname']))
			die();
		
		$module = $_REQUEST['aow_module'];
		$fieldName = $_REQUEST['aow_fieldname'];
		global $beanList;
		$mod = new $beanList[$module]();
		$defs = $mod->field_defs;
		
		if($defs[$fieldName]['type'] == 'function' && $defs[$fieldName]['source'] == 'non-db')
			return true;
		
		return false;
		
	}
	
	private function getFunctionFields($module, $view, $aowValue){
		global $beanList;
		if(isset($beanList[$module]) && $beanList[$module]){
			$mod = new $beanList[$module]();
			foreach($mod->field_defs as $name => $arr){
				if(ACLController::checkAccess($mod->module_dir, 'list', true)) {
					if($arr['type'] == 'function' && ((!isset($arr['source']) || $arr['source'] == 'non-db'))){
						if(isset($arr['vname']) && $arr['vname'] != ''){
							$fields[$name] = rtrim(translate($arr['vname'],$mod->module_dir), ':');
						} else {
							$fields[$name] = $name;
						}
					}
				}
			}
		}
    	
		if($view == 'EditView' && !empty($fields)){
			$options = get_select_options_with_id($fields, $aowValue);
			$find = "</OPTION>";
			$length = strlen($find);
			$pos = strpos($options, $find);
			echo substr($options, strpos($options, $find) + $length);
		}
	}
}
