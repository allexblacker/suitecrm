<?php

require_once("custom/include/dtbc/helpers.php");

class CustomAOW_WorkFlow extends AOW_WorkFlow {
	
	public function __construct(){
		parent::__construct();
	}
	
	function run_bean_flows(SugarBean &$bean){
        if(!isset($_REQUEST['module']) || $_REQUEST['module'] != 'Import'){

			$query = "SELECT id FROM aow_workflow WHERE aow_workflow.flow_module = '".$bean->module_dir."' AND aow_workflow.status = 'Active' AND (aow_workflow.run_when = 'Always' OR aow_workflow.run_when = 'On_Save' OR aow_workflow.run_when = 'Create') AND aow_workflow.deleted = 0 ORDER BY custom_order_c ASC ";

            $result = $this->db->query($query, false);
            $flow = new CustomAOW_WorkFlow();
            while (($row = $bean->db->fetchByAssoc($result)) != null){
                $flow ->retrieve($row['id']);
                if($flow->check_valid_bean($bean))
                    $flow->run_actions($bean, true);
            }
        }
        return true;
    }

    function build_query_where(AOW_Condition $condition, $module, $query = array()){
        global $beanList, $app_list_strings, $sugar_config, $timedate;
        $path = unserialize(base64_decode($condition->module_path));
		
        $condition_module = $module;
        $table_alias = $condition_module->table_name;
        if(isset($path[0]) && $path[0] != $module->module_dir){
            foreach($path as $rel){
                $query = $this->build_flow_query_join($rel, $condition_module, 'relationship', $query);
                $condition_module = new $beanList[getRelatedModule($condition_module->module_dir,$rel)];
                $table_alias = $rel;
            }
        }

        if(isset($app_list_strings['aow_sql_operator_list'][$condition->operator])){
            $where_set = false;

            $data = $condition_module->field_defs[$condition->field];

            if($data['type'] == 'relate' && isset($data['id_name'])) {
                $condition->field = $data['id_name'];
            }
            if(  (isset($data['source']) && $data['source'] == 'custom_fields')) {
                $field = $table_alias.'_cstm.'.$condition->field;
                $query = $this->build_flow_query_join($table_alias.'_cstm', $condition_module, 'custom', $query);
				// Set the proper alias - Fixed by B
				// In some cases the main table alias is the name of the relation, but in the cstm table the query uses the original name, not the alias
				// eg: 
				// LEFT JOIN contacts contact_created_by ON cases.contact_created_by_id=contact_created_by.id AND contact_created_by.deleted=0
				// LEFT JOIN contacts_cstm contact_created_by_cstm ON contacts.id = contact_created_by_cstm.id_c
				// The query will failed becase of the alias. The following few lines will figure out the real alias and replace it in the join.
				if (isset($query['join'][$table_alias])) {
					$subQuery1 = substr($query['join'][$table_alias], strlen("LEFT JOIN " . $condition_module->table_name));
					if (substr($subQuery1, 0, 1) == " ") {
						$tableAlias = trim(substr($subQuery1, 1, strpos($subQuery1, " ON")));
						if (strlen($tableAlias) > 0) {
							$query['join'][$table_alias . '_cstm'] = str_replace($condition_module->table_name . ".", $tableAlias . ".", $query['join'][$table_alias . '_cstm']);
						}
					}					
				}			
            } else {
                $field = $table_alias.'.'.$condition->field;
            }

            if($condition->operator == 'is_null'){
                $query['where'][] = '('.$field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator].' OR '.$field.' '.$app_list_strings['aow_sql_operator_list']['Equal_To']." '')";
                return $query;
            }
			
			if($condition->operator == 'is_not_null'){
                $query['where'][] = '('.$field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator].' AND '.$field.' '.$app_list_strings['aow_sql_operator_list']['Not_Equal_To']." '')";
                return $query;
            }

            switch($condition->value_type) {
                case 'Field':
                    $data = $module->field_defs[$condition->value];

                    if($data['type'] == 'relate' && isset($data['id_name'])) {
                        $condition->value = $data['id_name'];
                    }
                    if(  (isset($data['source']) && $data['source'] == 'custom_fields')) {
                        $value = $module->table_name.'_cstm.'.$condition->value;
                        $query = $this->build_flow_query_join($module->table_name.'_cstm', $module, 'custom', $query);
                    } else {
                        $value = $module->table_name.'.'.$condition->value;
                    }
                    break;
                case 'Any_Change':
                    //can't detect in scheduler so return
                    return array();
                case 'Date':
                    $params =  unserialize(base64_decode($condition->value));
                    if($params[0] == 'now'){
                        if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                            $value  = 'GetUTCDate()';
                        } else {
                            $value = 'UTC_TIMESTAMP()';
                        }
                    } else if($params[0] == 'today'){
                        if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                            //$field =
                            $value  = 'CAST(GETDATE() AS DATE)';
                        } else {
                            $field = 'DATE('.$field.')';
                            $value = 'Curdate()';
                        }
                    } else {
                        $data = $module->field_defs[$params[0]];
                        if(  (isset($data['source']) && $data['source'] == 'custom_fields')) {
                            $value = $module->table_name.'_cstm.'.$params[0];
                            $query = $this->build_flow_query_join($module->table_name.'_cstm', $module, 'custom', $query);
                        } else {
                            $value = $module->table_name.'.'.$params[0];
                        }
                    }

                    if($params[1] != 'now'){
                        switch($params[3]) {
                            case 'business_hours';
                                if(file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php') && $params[0] == 'now'){
                                    require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

                                    $businessHours = new AOBH_BusinessHours();

                                    $amount = $params[2];

                                    if($params[1] != "plus"){
                                        $amount = 0-$amount;
                                    }
                                    $value = $businessHours->addBusinessHours($amount);
                                    $value = "'".$timedate->asDb( $value )."'";
                                    break;
                                }
                                //No business hours module found - fall through.
                                $params[3] = 'hour';
                            default:
                                if($sugar_config['dbconfig']['db_type'] == 'mssql'){
                                    $value = "DATEADD(".$params[3].",  ".$app_list_strings['aow_date_operator'][$params[1]]." $params[2], $value)";
                                } else {
                                    $value = "DATE_ADD($value, INTERVAL ".$app_list_strings['aow_date_operator'][$params[1]]." $params[2] ".$params[3].")";
                                }
                                break;
                        }
                    }
                    break;

                case 'Multi':
                    $sep = ' AND ';
                    if($condition->operator == 'Equal_To') $sep = ' OR ';
                    $multi_values = unencodeMultienum($condition->value);
                    if(!empty($multi_values)){
                        $value = '(';
                        if($data['type'] == 'multienum'){
                            $multi_operator =  $condition->operator == 'Equal_To' ? 'LIKE' : 'NOT LIKE';
                            foreach($multi_values as $multi_value){
                                if($value != '(') $value .= $sep;
                                $value .= $field." $multi_operator '%^".$multi_value."^%'";
                            }
                        }
                        else {
                            foreach($multi_values as $multi_value){
                                if($value != '(') $value .= $sep;
                                $value .= $field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator]." '".$multi_value."'";
                            }
                        }
                        $value .= ')';
                        $query['where'][] = $value;
                    }
                    $where_set = true;
                    break;
                case 'SecurityGroup':
                    $sgModule = $condition_module->module_dir;
                    if (isset($data['module']) && $data['module'] !== '') {
                        $sgModule = $data['module'];
                    }
                    $sql = 'EXISTS (SELECT 1 FROM securitygroups_records WHERE record_id = ' . $field . " AND module = '" . $sgModule . "' AND securitygroup_id = '" . $condition->value . "' AND deleted=0)";
                    if ($sgModule === 'Users') {
                        $sql = 'EXISTS (SELECT 1 FROM securitygroups_users WHERE user_id = ' . $field . " AND securitygroup_id = '" . $condition->value . "' AND deleted=0)";
                    }
                    $query['where'][] = $sql;
                    $where_set = true;
                    break;
                case 'Value':
                default:
                    $value = "'".$condition->value."'";
                    break;
            }

            //handle like conditions
            Switch($condition->operator) {
                case 'Contains':
                    $value = "CONCAT('%', ".$value." ,'%')";
                    break;
				case 'Not_Contains':
                    $value = "CONCAT('%', ".$value." ,'%')";
                    break;
                case 'Starts_With':
                    $value = "CONCAT(".$value." ,'%')";
                    break;
                case 'Ends_With':
                    $value = "CONCAT('%', ".$value.")";
                    break;
            }


            if(!$where_set) $query['where'][] = $field.' '.$app_list_strings['aow_sql_operator_list'][$condition->operator].' '.$value;
        }

        return $query;

    }

    function compare_condition($var1, $var2, $operator = 'Equal_To'){
        switch ($operator) {
            case "Not_Equal_To": return $var1 != $var2;
            case "Greater_Than":  return $var1 >  $var2;
            case "Less_Than":  return $var1 <  $var2;
            case "Greater_Than_or_Equal_To": return $var1 >= $var2;
            case "Less_Than_or_Equal_To": return $var1 <= $var2;
            case "Contains" : return strpos($var1,$var2) !== FALSE;
            case "Not_Contains" : return strpos($var1,$var2) === FALSE;
            case "Starts_With" : return strrpos($var1,$var2, -strlen($var1));
            case "Ends_With" : return strpos($var1,$var2,strlen($var1) - strlen($var2));
            case "is_null": return $var1 == '';
            case "is_not_null": return $var1 != '' && $var1 != null;
            case "One_of":
                if(is_array($var1)){
                    foreach($var1 as $var){
                        if(in_array($var,$var2)) return true;
                    }
                    return false;
                }
                else return in_array($var1,$var2);
            case "Not_One_of":
                if(is_array($var1)){
                    foreach($var1 as $var){
                        if(in_array($var,$var2)) return false;
                    }
                    return true;
                }
                else return !in_array($var1,$var2);
            case "Equal_To":
            default: return $var1 == $var2;
        }
    }

}
?>
