<?php
// created: 2017-09-11 11:16:16
$mod_strings = array (
  'LBL_INITIAL_RESPONSE_TIME' => 'Initial response time and overall availability',
  'LBL_PROFESSIONALISM_AND_SERVICE' => 'Professionalism and service-oriented conduct of the staff',
  'LBL_TECHNICAL_KNOWLEDGE' => 'Technical knowledge of the staff',
  'LBL_TIME_UNTIL_ISSUE_RESOLVED' => 'Time until issue was resolved',
);