<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/JSLoader.php');

class dtbc_CasesSurveyViewEdit extends ViewEdit
{
    private $caseBean;
    
    public function __construct(){
        
        $this->caseBean = BeanFactory::getBean('Cases',$_REQUEST['caseId']);
    }
    
    public function preDisplay(){
        parent::preDisplay();
		$this->ev->defs['templateMeta']['form']['buttons'] = array('' => '');
        
        echo getVersionedScript('include/javascript/jquery/jquery-min.js');
        includeCssFile('themes/SolarEdgeP/css/style.css');
        includeCssFile('custom/modules/dtbc_CasesSurvey/dtbc/css/survey.css');
        
        $smarty = new Sugar_Smarty();		
        $smarty->assign('caseNumber', $this->caseBean->case_number);
        $smarty->assign('caseSubject', $this->caseBean->name);
        $smarty->assign('caseNumberText', translate('LBL_CASE_NUMBER',$this->module));		
        $smarty->assign('caseSubjectText', translate('LBL_CASE_SUBJECT',$this->module));		
        $smarty->assign('saveMessageText', translate('LBL_SAVE_MESSAGE',$this->module));		
		$smarty->display('custom/modules/dtbc_CasesSurvey/dtbc/tpls/SurveyHeader.tpl');
    }
    
    public function display(){
		global $app_list_strings, $current_language;
		$app_list_strings = return_app_list_strings_language($current_language);

		parent::display();
    }
    
    public function getMetaDataFile(){
        
        if($this->caseBean->load_relationship('accounts')){
            
            $accounts = $this->caseBean->accounts->getBeans();
            $account = reset($accounts);
            global $sugar_config;
            $layout = $sugar_config['solaredge']['survey']['layout'][$account->country_c];
            
            $defaultFile = "custom/modules/dtbc_CasesSurvey/metadata/editviewdefs.php";
            if(empty($layout) || $layout == "default")
                return $defaultFile;
                
            $editViewFile = "custom/modules/dtbc_CasesSurvey/metadata/$layout/editviewdefs.php"; 
            
            if(file_exists($editViewFile))
                return $editViewFile;
            else
                return $defaultFile;
        }
        
        die(translate('LBL_NOT_EXISTING_RELATIONSHIP_CASES_ACCOUNTS', $this->module));
	}
    
    public function displayProcess($caseId,$contactId){
        
        $this->preDisplay();
        $this->display();
        
        $saveText = translate('LBL_SAVE', $this->module);
        echo "<input type='button' value='$saveText' id='saveButton'/>";
        echo "<input type='hidden' value='$caseId' id='caseId'/>";
        echo "<input type='hidden' value='$contactId' id='contactId'/>";
    }
}