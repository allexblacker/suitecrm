<?php

require_once('include/MVC/View/views/view.detail.php');

class dtbc_CasesSurveyViewDetail extends ViewDetail{
    
    public function getMetaDataFile(){
        
        global $mod_strings;
        if(!$this->bean->load_relationship('cases_dtbc_casessurvey_1'))
            return $mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_CASES_SURVEY'];
            
        $cases = $this->bean->cases_dtbc_casessurvey_1->getBeans();
        $caseBean = reset($cases);
            
        if(!$caseBean->load_relationship('accounts'))
            return $mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_CASES_ACCOUNTS'];
        
        $accounts = $caseBean->accounts->getBeans();
        $account = reset($accounts);
            
        global $sugar_config;
        $layout = $sugar_config['solaredge']['survey']['layout'][$account->country_c];
        $defaultFile = "custom/modules/dtbc_CasesSurvey/metadata/detailviewdefs.php";    
            
        if(empty($layout) || $layout == "default")
            return $defaultFile;
            
        $detalViewFile = "custom/modules/dtbc_CasesSurvey/metadata/$layout/detailviewdefs.php";
        if(file_exists($detalViewFile))
            return $detalViewFile;
        else
            return $defaultFile;
    }
}