{literal}
<script>
$(function(){

var container = $("#EditView_tabs");
container.find("br").remove();    
var message = '{/literal}{$saveMessageText}{literal}';
	
	$("#saveButton").click(function(){

		if(confirm(message)){

			var radioButtons = [];
			container.find("input:checked").each(function() {
				var array = [];
				radioObject = {
					field: $(this).attr('name'),
					value: $(this).val()
				}
				radioButtons.push(radioObject);
			});

			var json = JSON.stringify(radioButtons);

			$.ajax({
				url: "index.php?entryPoint=saveSurvey",
				type: "POST",
				data:{
					  questions: json,
					  description: $("#description").val(),
					  contactId: $('#contactId').val(),
					  caseId: $('#caseId').val(),
					},
				success: function(result){
					$("body").html("<div style='text-align: center;'><h1>" + result + "</h1></div>");
				},
				error: function(request, status, error){
					alert(request.responseText);
				}
			});
		}
	});        
});
</script>
{/literal}

<div id="headerSurvey">
<hr/>
<h3>{$caseNumberText}: {$caseNumber}</h3> 
<h3>{$caseSubjectText}: {$caseSubject}</h3>
<hr/>
</div>