<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class LanguageChanger{
    
    public static function changeLanguage($caseBean){
        
        if($caseBean->load_relationship('accounts')){
            
            $accounts = $caseBean->accounts->getBeans();
            $account = reset($accounts);
            global $sugar_config;
            return $sugar_config['solaredge']['survey']['language'][$account->country_c];
        }
        die('Not existing relationship between cases and accounts!');
    }
}