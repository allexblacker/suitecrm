<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if(!(isset($_REQUEST['caseId']) || isset($_REQUEST['contactId'])))
   die('Invalid parameters!');

$caseId = $_REQUEST['caseId'];
$contactId = $_REQUEST['contactId'];
$module = "dtbc_CasesSurvey";

$caseBean = BeanFactory::getBean('Cases', $caseId);
isEmpty($caseBean,translate('LBL_NOT_EXISTING_CASE', $module));

require_once('custom/modules/dtbc_CasesSurvey/dtbc/classes/LanguageChanger.php');
global $current_language;
$current_language = LanguageChanger::changeLanguage($caseBean);

$contactBean = BeanFactory::getBean('Contacts',$contactId);
isEmpty($contactBean,translate('LBL_NOT_EXISTING_CONTACT',$module));    

require_once('custom/modules/dtbc_CasesSurvey/dtbc/classes/RelationshipChecker.php');
RelationshipChecker::CheckRelationBetweenCaseAndContact($caseBean,$contactId);

isAlreadyFilled($caseId,$contactId);

require_once('include/MVC/View/ViewFactory.php');

$view = ViewFactory::loadView('edit', $module);
$view->displayProcess($caseId,$contactId);

function isEmpty($value, $errorMessage){

    if(empty($value))
        die($errorMessage);
}

function isAlreadyFilled($caseId, $contactId){
    
    global $db;
    $sql = "SELECT cases_dtbc_casessurvey_1_c.cases_dtbc_casessurvey_1cases_ida,
            dtbc_casessurvey.id,
            contacts_dtbc_casessurvey_1_c.contacts_dtbc_casessurvey_1contacts_ida
            FROM cases_dtbc_casessurvey_1_c
            INNER JOIN dtbc_casessurvey
            ON dtbc_casessurvey.id = cases_dtbc_casessurvey_1_c.cases_dtbc_casessurvey_1dtbc_casessurvey_idb AND dtbc_casessurvey.deleted = 0
            INNER JOIN contacts_dtbc_casessurvey_1_c
            ON contacts_dtbc_casessurvey_1_c.contacts_dtbc_casessurvey_1dtbc_casessurvey_idb = dtbc_casessurvey.id AND contacts_dtbc_casessurvey_1_c.deleted = 0
            WHERE cases_dtbc_casessurvey_1_c.deleted = 0
            AND cases_dtbc_casessurvey_1_c.cases_dtbc_casessurvey_1cases_ida LIKE '$caseId' 
            AND contacts_dtbc_casessurvey_1_c.contacts_dtbc_casessurvey_1contacts_ida LIKE '$contactId';";
    
    $result = $db->query($sql);
    if($result->num_rows != 0)
        die(translate('LBL_FILLED_SURVEY',$module));
}