<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class RelationshipChecker{
    
    public static function CheckRelationBetweenCaseAndContact($caseBean,$contactId){
        
        global $mod_strings;
        if($caseBean->contact_created_by_id != $contactId)
            die($mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_THIS_CASE_CONTACT']);
    }
}