<?php
// created: 2017-10-17 15:24:22
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'initial_response_time_c' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_INITIAL_RESPONSE_TIME',
    'width' => '10%',
  ),
  'professionalism_and_service_c' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PROFESSIONALISM_AND_SERVICE',
    'width' => '10%',
  ),
  'time_until_issue_resolved_c' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_TIME_UNTIL_ISSUE_RESOLVED',
    'width' => '10%',
  ),
  'technical_knowledge_c' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_TECHNICAL_KNOWLEDGE',
    'width' => '10%',
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'dtbc_CasesSurvey',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'dtbc_CasesSurvey',
    'width' => '5%',
    'default' => true,
  ),
);