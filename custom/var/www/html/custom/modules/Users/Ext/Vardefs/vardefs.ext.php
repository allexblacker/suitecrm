<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-11-17 01:52:32
$dictionary['User']['fields']['suite_id_c']['inline_edit']='';
$dictionary['User']['fields']['suite_id_c']['labelValue']='suite id';

 

// created: 2017-09-25 21:15:05
$dictionary["User"]["fields"]["users_p1_project_1"] = array (
  'name' => 'users_p1_project_1',
  'type' => 'link',
  'relationship' => 'users_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-11-16 10:48:24
$dictionary['User']['fields']['address_country']['inline_edit']=true;
$dictionary['User']['fields']['address_country']['merge_filter']='disabled';

 

 // created: 2017-10-09 13:29:10
$dictionary['User']['fields']['division_c']['inline_edit']='1';
$dictionary['User']['fields']['division_c']['labelValue']='Division';

 

 // created: 2017-11-16 10:43:13
$dictionary['User']['fields']['region_c']['inline_edit']='1';
$dictionary['User']['fields']['region_c']['labelValue']='Region';

 

 // created: 2017-11-16 10:45:53
$dictionary['User']['fields']['role_c']['inline_edit']='1';
$dictionary['User']['fields']['role_c']['labelValue']='Role';

 

 // created: 2017-11-09 19:22:22
$dictionary['User']['fields']['salesforce_id_c']['inline_edit']='';
$dictionary['User']['fields']['salesforce_id_c']['labelValue']='SalesForce ID';

 

 // created: 2017-09-15 10:19:10
$dictionary['User']['fields']['tier3_c']['inline_edit']='1';
$dictionary['User']['fields']['tier3_c']['labelValue']='Tier 3';

 

// created: 2017-09-25 21:15:11
$dictionary["User"]["fields"]["users_p1_project_2"] = array (
  'name' => 'users_p1_project_2',
  'type' => 'link',
  'relationship' => 'users_p1_project_2',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_P1_PROJECT_TITLE',
);

?>