<?php 
 //WARNING: The contents of this file are auto-generated




$job_strings['CalcLeadAgeJob'] = 'custom_cron_job';



function custom_cron_job(){
//    require_once 'custom/include/customScheduledTasks.php';
    //do the function here.
    $bean = BeanFactory::getBean('Leads');
    $query = "leads.status <> 'Converted' and leads.status <> 'Dead'";
    $actLeads = $bean->get_full_list('',$query);
    foreach($actLeads as $actLead){
       
       //Mark each meeting as Not Held
       $tmp = new DateTime();
       $sToday = $tmp->format("Y-m-d");
       $Today = new DateTime($sToday);
       $sEnterDate = $actLead->date_entered; 
       $tmpEnterDate = new DateTime($sEnterDate) ;
       $sEnterDate = $tmpEnterDate->format("Y-m-d");
       $EnterDate = new DateTime($sEnterDate);
       $dateDifference = $Today->diff($EnterDate);   
       $actLead->lead_age_c = $dateDifference->days;
       $actLead->save();
        
    }

    return true; // Remember to return the result of successful execution whether this be from a function or not.
    ///return my_custom_function_from_my_included_file();
}



array_push($job_strings, 'custom_workflow_process');

function custom_workflow_process() {
	require_once('custom/modules/AOW_WorkFlow/AOW_WorkFlow.php');
    $workflow = new CustomAOW_WorkFlow();
    return $workflow->run_flows();
}




array_push($job_strings, 'calculate_yearly_points');

function calculate_yearly_points() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->calculateYearlyPoints();
}




array_push($job_strings, 'case_job_5days');

function case_job_5days() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->caseStatuses("case_job_5days");
}




array_push($job_strings, 'email_to_case');

function email_to_case() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->emailToCase();
}




array_push($job_strings, 'systemWorkflows');

function systemWorkflows() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->systemWorkflows();
}




array_push($job_strings, 'case_job_3days');

function case_job_3days() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->caseStatuses("case_job_3days");
}




array_push($job_strings, 'custom_inbound_emails_reset');

function custom_inbound_emails_reset() {
	require_once("custom/include/dtbc/InboundEmailScheduler.php");
	
	$temp = new InboundEmailScheduler();
	
	return $temp->resetScheduler(true);
}




array_push($job_strings, 'custom_inbound_emails');

function custom_inbound_emails() {
	require_once("custom/include/dtbc/InboundEmailScheduler.php");
	
	$temp = new InboundEmailScheduler();
	
	return $temp->runScheduler();
}




array_push($job_strings, 'case_job_2days');

function case_job_2days() {
	require_once("custom/include/dtbc/schedulers.php");
	
	$temp = new DtbcSchedulers();
	
	return $temp->customerNotifier2days();
}


?>