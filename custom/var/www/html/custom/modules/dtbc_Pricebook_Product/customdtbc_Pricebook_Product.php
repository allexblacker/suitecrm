<?php

require_once('modules/dtbc_Pricebook_Product/dtbc_Pricebook_Product.php');
require_once('custom/include/dtbc/helpers.php');

class Customdtbc_Pricebook_Product extends dtbc_Pricebook_Product {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function create_new_list_query($order_by, $where,$filter=array(),$params=array(), $show_deleted = 0,$join_type='', $return_array = false,$parentbean=null, $singleSelect = false, $ifListForExport = false) {
		$retval = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);
	
		if ($_REQUEST['action'] == "Popup") {
			if (!isset($_REQUEST['opportunityId']))
				return $retval;
			
			// Get pricebook and currency from opportunity
			$opPricebookId = "";
			$opCurrencyId = "";
			global $db;
			$sql = "SELECT currency_c, dtbc_pricebook_id_c
					FROM opportunities_cstm
					WHERE id_c = " . $db->quoted($_REQUEST['opportunityId']);
					
			$res = $db->fetchOne($sql);
			
			if (!empty($res) && is_array($res) && count($res) > 0) {
				$opPricebookId = $res['dtbc_pricebook_id_c'];
				$opCurrencyId = $res['currency_c'];
			}
			
			// Prepare currency and pricebook filtering
			$retval['from'] .= " LEFT JOIN dtbc_pricebook_dtbc_pricebook_product_1_c ON dtbc_priced32cproduct_idb = dtbc_pricebook_product.id AND  dtbc_pricebook_dtbc_pricebook_product_1_c.deleted = 0";
			$retval['where'] .= " AND dtbc_pricebook_product_cstm.currency_c = " . $db->quoted($opCurrencyId) . " AND dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida = " . $db->quoted($opPricebookId);
		}
		
		
		return $retval;
	}

}

?>