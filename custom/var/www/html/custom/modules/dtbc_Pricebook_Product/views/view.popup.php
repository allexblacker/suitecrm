<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.popup.php');
require_once('custom/include/dtbc/helpers.php');

class dtbc_Pricebook_ProductViewPopup extends ViewPopup {
    function display() {
        parent::display();
		echo getVersionedScript("custom/modules/dtbc_Pricebook_Product/dtbc/popupview.js");
		echo "<input type='hidden' id='custom_opportunity_id' value='" . $_REQUEST['opportunityId'] . "'/>";
    }
}
?>
