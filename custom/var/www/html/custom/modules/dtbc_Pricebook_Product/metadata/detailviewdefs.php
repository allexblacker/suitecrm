<?php
$module_name = 'dtbc_Pricebook_Product';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aos_products_dtbc_pricebook_product_1_name',
          ),
          1 => 
          array (
            'name' => 'dtbc_pricebook_dtbc_pricebook_product_1_name',
          ),
        ),
        1 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'currency_c',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'active_c',
            'label' => 'LBL_ACTIVE',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'list_price_c',
            'label' => 'LBL_LIST_PRICE',
          ),
          1 => 
          array (
            'name' => 'standard_price_c',
            'label' => 'LBL_STANDARD_PRICE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'use_standard_price_c',
            'label' => 'LBL_USE_STANDARD_PRICE',
          ),
          1 => 
          array (
            'name' => 'product_code_c',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_CODE',
          ),
        ),
      ),
    ),
  ),
);
?>
