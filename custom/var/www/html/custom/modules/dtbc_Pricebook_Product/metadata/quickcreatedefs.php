<?php
$module_name = 'dtbc_Pricebook_Product';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'aos_products_dtbc_pricebook_product_1_name',
            'label' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_PRODUCTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'dtbc_pricebook_dtbc_pricebook_product_1_name',
            'label' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_TITLE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
