function productPopup() {
	open_popup(
		"AOS_Products", 
		600, 
		400, 
		"", 
		true, 
		false, 
		{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"aos_products_dtbc_pricebook_product_1aos_products_ida","name":"aos_products_dtbc_pricebook_product_1_name", "standard_price_c":"standard_price_c"}}, 
		"single", 
		true
	);
}

$(function() {
	$("#btn_aos_products_dtbc_pricebook_product_1_name").removeAttr("onclick");
    $("#btn_aos_products_dtbc_pricebook_product_1_name").prop('onclick', null).off('click');
    $('#btn_aos_products_dtbc_pricebook_product_1_name').on('click', function () {
        productPopup();
    });
});