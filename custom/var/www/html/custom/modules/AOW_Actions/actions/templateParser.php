<?php

require_once 'custom/modules/AOS_PDF_Templates/templateParser.php';


class CustomAowTemplateParser extends CustomTemplateParser {

	static function parse_template($string, $bean_arr) {
		global $beanList;

		$person = array();

		foreach($bean_arr as $bean_name => $bean_id) {
			$focus = BeanFactory::getBean($bean_name, $bean_id);
			$string = CustomAowTemplateParser::parse_template_bean($string, strtolower($beanList[$bean_name]), $focus);

			if($focus instanceof Person){
				$person[] = $focus;
			}				
		}

		if(!empty($person)){
			$focus = $person[0];
		} else {
			$focus = new Contact();
		}
		$string = CustomAowTemplateParser::parse_template_bean($string, 'contact', $focus);

		return $string;
	}
}
?>
