<?php
// created: 2017-11-30 11:40:07
$searchFields['Cases'] = array (
  'searchform_security_group' => 
  array (
    'query_type' => 'default',
    'operator' => 'subquery',
    'subquery' => 'SELECT cases.id FROM securitygroups_records
					INNER JOIN cases
					ON cases.id = securitygroups_records.record_id
						AND cases.deleted = 0
					INNER JOIN securitygroups
					ON securitygroups.id = securitygroups_records.securitygroup_id
						AND securitygroups.deleted = 0
					WHERE securitygroups_records.deleted = 0
						AND securitygroups_records.module = \'Cases\'
						AND securitygroups.name LIKE',
    'db_field' => 
    array (
      0 => 'id',
    ),
    'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
  ),
  'name' => 
  array (
    'query_type' => 'default',
  ),
  'account_name' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'accounts.name',
    ),
  ),
  'status' => 
  array (
    'query_type' => 'default',
    'options' => 'case_status_dom',
    'template_var' => 'STATUS_OPTIONS',
  ),
  'priority' => 
  array (
    'query_type' => 'default',
    'options' => 'case_priority_dom',
    'template_var' => 'PRIORITY_OPTIONS',
    'options_add_blank' => true,
  ),
  'case_number' => 
  array (
    'query_type' => 'default',
    'operator' => 'in',
  ),
  'current_user_only' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'assigned_user_id',
    ),
    'my_items' => true,
    'vname' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
  ),
  'assigned_user_id' => 
  array (
    'query_type' => 'default',
  ),
  'open_only' => 
  array (
    'query_type' => 'default',
    'db_field' => 
    array (
      0 => 'status',
    ),
    'operator' => 'not in',
    'closed_values' => 
    array (
      0 => 'Closed',
      1 => 'Rejected',
      2 => 'Duplicate',
      3 => 'Closed_Closed',
      4 => 'Closed_Rejected',
      5 => 'Closed_Duplicate',
    ),
    'type' => 'bool',
  ),
  'range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'state' => 
  array (
    'query_type' => 'default',
  ),
  'favorites_only' => 
  array (
    'query_type' => 'format',
    'operator' => 'subquery',
    'checked_only' => true,
    'subquery' => 'SELECT favorites.parent_id FROM favorites
			                    WHERE favorites.deleted = 0
			                        and favorites.parent_type = \'Cases\'
			                        and favorites.assigned_user_id = \'{1}\'',
    'db_field' => 
    array (
      0 => 'id',
    ),
  ),
  'type' => 
  array (
    'query_type' => 'default',
  ),
  'date_entered' => 
  array (
    'query_type' => 'default',
  ),
  'date_modified' => 
  array (
    'query_type' => 'default',
  ),
  'dtbc_notify_customer_date' => 
  array (
    'query_type' => 'default',
  ),
  'range_date_time_battery_escalation' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_time_battery_escalation' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_time_battery_escalation' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_tesla_lg_escalation_timestam' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_tesla_lg_escalation_timestam' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_tesla_lg_escalation_timestam' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_time_battery_escalation_c' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_time_battery_escalation_c' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_time_battery_escalation_c' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_tesla_lg_escalation_timestam_c' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_tesla_lg_escalation_timestam_c' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_tesla_lg_escalation_timestam_c' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
);