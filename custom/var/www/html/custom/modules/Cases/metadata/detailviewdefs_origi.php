<?php
$viewdefs ['Cases'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_CASE_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL5' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL6' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_AOP_CASE_UPDATES' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_case_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'case_number',
            'label' => 'LBL_CASE_NUMBER',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'support_comments_cc_c',
            'label' => 'SUPPORT_COMMENTS_CC',
          ),
        ),
        2 => 
        array (
          0 => 'type',
          1 => 
          array (
            'name' => 'site_contact_person_phone_c',
            'label' => 'SITE_CONTACT_PERSON_PHONE',
          ),
        ),
        3 => 
        array (
          0 => 'status',
          1 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'symptom_c',
            'studio' => 'visible',
            'label' => 'SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'ack_service_instruction_c',
            'label' => 'ACK_SERVICE_INSTRUCTION',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'sub_symptom_c',
            'studio' => 'visible',
            'label' => 'SUB_SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'internal',
            'studio' => 'visible',
            'label' => 'LBL_INTERNAL',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
          array (
            'name' => 'jira_fa_c',
            'label' => 'JIRA_FA',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'any_inverter_serial_req_for_c',
            'label' => 'ANY_INVERTER_SERIAL_REQ_FOR_TECH_ISSUE',
          ),
          1 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_address_c',
            'label' => 'LBL_JJWG_MAPS_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'tier_2_assignee_c',
            'studio' => 'visible',
            'label' => 'TIER_2_ASSIGNEE',
          ),
          1 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'tier_2_region_c',
            'studio' => 'visible',
            'label' => 'TIER_2_REGION',
          ),
          1 => 
          array (
            'name' => 'panel_c',
            'studio' => 'visible',
            'label' => 'PANEL',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'alternative_ibolt_status_c',
            'studio' => 'visible',
            'label' => 'ALTERNATIVE_IBOLT_STATUS',
          ),
          1 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'uzc1_usa_zip_codes_cases_name',
            'label' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
          ),
          1 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'uzc1_usa_zip_codes_cases_name',
            'label' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
          ),
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 'description',
        ),
      ),
      'lbl_detailview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'distributer_number_c',
            'label' => 'DISTRIBUTER_NUMBER',
          ),
          1 => 
          array (
            'name' => 'refurbish_c',
            'label' => 'REFURBISH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'distributor_c',
            'label' => 'DISTRIBUTOR',
          ),
          1 => 
          array (
            'name' => 'as_part_number_c',
            'label' => 'AS_PART_NUMBER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'ibolt_status_c',
            'studio' => 'visible',
            'label' => 'IBOLT_STATUS',
          ),
          1 => 
          array (
            'name' => 'cpu_firmware_c',
            'label' => 'CPU_FIRMWARE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'valid_c',
            'studio' => 'visible',
            'label' => 'VALID',
          ),
          1 => 
          array (
            'name' => 'dsp1_c',
            'label' => 'DSP1',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'warranty_end_date_c',
            'label' => 'WARRANTY_END_DATE',
          ),
          1 => 
          array (
            'name' => 'dsp2_c',
            'label' => 'DSP2',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'warranty_extention_c',
            'label' => 'WARRANTY_EXTENTION',
          ),
          1 => 
          array (
            'name' => 'activation_code_c',
            'label' => 'ACTIVATION_CODE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'part_number_c',
            'label' => 'PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'traceability_c',
            'studio' => 'visible',
            'label' => 'TRACEABILITY',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'part_description_c',
            'label' => 'PART_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'reason_for_not_in_warranty_c',
            'label' => 'REASON_FOR_NOT_IN_WARRANTY',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'family_c',
            'label' => 'FAMILY',
          ),
          1 => 
          array (
            'name' => 'current_software_version_c',
            'label' => 'CURRENT_SOFTWARE_VERSION',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'product_group_c',
            'label' => 'PRODUCT_GROUP',
          ),
          1 => 
          array (
            'name' => 'first_telemetry_date_c',
            'label' => 'FIRST_TELEMETRY_DATE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'power_c',
            'label' => 'POWER',
          ),
          1 => 
          array (
            'name' => 'last_telemetry_date_c',
            'label' => 'LAST_TELEMETRY_DATE',
          ),
        ),
      ),
      'lbl_detailview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'any_battery_serial_number_c',
            'label' => 'ANY_BATTERY_SERIAL_NUMBER',
          ),
          1 => 
          array (
            'name' => 'se_battery_support_c',
            'label' => 'SE_BATTERY_SUPPORT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'battery_part_number_c',
            'label' => 'BATTERY_PART_NUMBER',
          ),
          1 => 
          array (
            'name' => 'battery_is_monitored_c',
            'label' => 'BATTERY_IS_MONITORED',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'battery_part_description_c',
            'label' => 'BATTERY_PART_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'battery_installation_date_c',
            'label' => 'BATTERY_INSTALLATION_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'battery_distributor_c',
            'label' => 'BATTERY_DISTRIBUTOR',
          ),
          1 => 
          array (
            'name' => 'tesla_case_number_c',
            'label' => 'TESLA_CASE_NUMBER',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'battery_distributor_number_c',
            'label' => 'BATTERY_DISTRIBUTOR_NUMBER',
          ),
          1 => 
          array (
            'name' => 'battery_ibolt_status_c',
            'studio' => 'visible',
            'label' => 'BATTERY_IBOLT_STATUS',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'battery_tesla_authorized_c',
            'label' => 'BATTERY_TESLA_AUTHORIZED_RESELLER_ID',
          ),
          1 => 
          array (
            'name' => 'escalation_to_tesla_c',
            'studio' => 'visible',
            'label' => 'ESCALATION_TO_TESLA',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'battery_fault_description_c',
            'studio' => 'visible',
            'label' => 'BATTERY_FAULT_DESCRIPTION',
          ),
        ),
      ),
      'lbl_detailview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'doa_c',
            'studio' => 'visible',
            'label' => 'DOA',
          ),
          1 => 'priority',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'doa_type_c',
            'studio' => 'visible',
            'label' => 'DOA_TYPE',
          ),
          1 => 
          array (
            'name' => 'severity_c',
            'studio' => 'visible',
            'label' => 'SEVERITY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'customer_should_go_to_the_c',
            'label' => 'CUSTOMER_SHOULD_GO_TO_THE_SITE',
          ),
          1 => 
          array (
            'name' => 'customer_complaint_c',
            'label' => 'CUSTOMER_COMPLAINT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'communication_board_was_c',
            'label' => 'COMMUNICATION_BOARD_WAS_REPLACED',
          ),
          1 => 
          array (
            'name' => 'digital_board_was_replaced_c',
            'label' => 'DIGITAL_BOARD_WAS_REPLACED',
          ),
        ),
      ),
      'lbl_detailview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'rma_country_c',
            'studio' => 'visible',
            'label' => 'RMA_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'notes_rma_team_us_c',
            'studio' => 'visible',
            'label' => 'NOTES_RMA_TEAM_US',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'rma_state_c',
            'studio' => 'visible',
            'label' => 'RMA_STATE',
          ),
          1 => 
          array (
            'name' => 'rma_city_c',
            'label' => 'RMA_CITY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case_address_c',
            'label' => 'CASE_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'case_zip_c',
            'label' => 'CASE_ZIP',
          ),
        ),
      ),
      'lbl_detailview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'field_engineer_c',
            'studio' => 'visible',
            'label' => 'FIELD_ENGINEER',
          ),
          1 => 
          array (
            'name' => 'site_contact_name_c',
            'label' => 'SITE_CONTACT_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'field_engineer_name_c',
            'studio' => 'visible',
            'label' => 'FIELD_ENGINEER_NAME',
          ),
          1 => 
          array (
            'name' => 'site_contact_email_c',
            'label' => 'SITE_CONTACT_EMAIL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'time_of_visit_c',
            'label' => 'TIME_OF_VISIT',
          ),
          1 => 
          array (
            'name' => 'site_contact_phone_c',
            'label' => 'SITE_CONTACT_PHONE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'visit_order_c',
            'studio' => 'visible',
            'label' => 'VISIT_ORDER',
          ),
          1 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE_C',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'workplan_remarks_c',
            'studio' => 'visible',
            'label' => 'WORKPLAN_REMARKS',
          ),
          1 => 
          array (
            'name' => 'case_city_c',
            'label' => 'CASE_CITY',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'rma_street_c',
            'label' => 'RMA_STREET',
          ),
          1 => 
          array (
            'name' => 'rma_zip_postal_code_c',
            'label' => 'RMA_ZIP_POSTAL_CODE',
          ),
        ),
      ),
      'lbl_detailview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
      'lbl_detailview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'change_details_c',
            'studio' => 'visible',
            'label' => 'CHANGE_DETAILS',
          ),
        ),
      ),
      'LBL_AOP_CASE_UPDATES' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aop_case_updates_threaded',
            'studio' => 'visible',
            'label' => 'LBL_AOP_CASE_UPDATES_THREADED',
          ),
        ),
      ),
    ),
  ),
);
?>
