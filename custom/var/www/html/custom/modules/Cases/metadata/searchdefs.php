<?php
$searchdefs ['Cases'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'open_only' => 
      array (
        'name' => 'open_only',
        'label' => 'LBL_OPEN_ITEMS',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      'change_indicator_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CHANGE_INDICATOR',
        'width' => '10%',
        'name' => 'change_indicator_c',
      ),
    ),
    'advanced_search' => 
    array (
      'case_number' => 
      array (
        'name' => 'case_number',
        'default' => true,
        'width' => '10%',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'account_name' => 
      array (
        'name' => 'account_name',
        'default' => true,
        'width' => '10%',
      ),
      'case_origin_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CASE_ORIGIN',
        'width' => '10%',
        'name' => 'case_origin_c',
      ),
      'case_record_type_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CASE_RECORD_TYPE',
        'width' => '10%',
        'name' => 'case_record_type_c',
      ),
      'case_type_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CASE_TYPE',
        'width' => '10%',
        'name' => 'case_type_c',
      ),
      'case_status_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CASE_STATUS',
        'width' => '10%',
        'name' => 'case_status_c',
      ),
      'case_state_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CASE_STATE',
        'width' => '10%',
        'name' => 'case_state_c',
      ),
      'region_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_REGION',
        'width' => '10%',
        'name' => 'region_c',
      ),
      'case_country_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'CASE_COUNTRY',
        'width' => '10%',
        'name' => 'case_country_c',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'case_sub_status_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'CASE_SUB_STATUS',
        'width' => '10%',
        'name' => 'case_sub_status_c',
      ),
      'tier3_escalation_owner_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'TIER3_ESCALATION_OWNER',
        'width' => '10%',
        'name' => 'tier3_escalation_owner_c',
      ),
      'priority' => 
      array (
        'name' => 'priority',
        'default' => true,
        'width' => '10%',
      ),
      'change_indicator_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CHANGE_INDICATOR',
        'width' => '10%',
        'name' => 'change_indicator_c',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'searchform_security_group' => 
      array (
        'name' => 'searchform_security_group',
        'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
        'default' => true,
        'type' => 'enum',
        'displayParams' => 
        array (
          'size' => 1,
        ),
        'width' => '10%',
      ),
      'doa_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'DOA',
        'width' => '10%',
        'name' => 'doa_c',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
