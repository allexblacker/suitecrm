<?php
$viewdefs ['Cases'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_DESCRIPTION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ADDITIONAL_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_CASE_DETAIL' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_BATTERY_VALIDATION_DATA' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_WEB_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'service_level_c',
            'label' => 'LBL_SERVICE_LEVEL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
          1 => 
          array (
            'name' => 'contact_created_by_name',
            'label' => 'LBL_CONTACT_CREATED_BY_NAME',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
          1 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
          1 => 
          array (
            'name' => 'ack_service_instruction_c',
            'label' => 'ACK_SERVICE_INSTRUCTION',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'symptom_c',
            'studio' => 'visible',
            'label' => 'SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'case_record_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_RECORD_TYPE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'internal_comment_c',
            'label' => 'LBL_INTERNAL_COMMENT',
          ),
          1 => 
          array (
            'name' => 'sub_symptom_c',
            'studio' => 'visible',
            'label' => 'SUB_SYMPTOM',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'inverter_serial_c',
            'label' => 'LBL_INVERTER_SERIAL',
          ),
          1 => 
          array (
            'name' => 'error_code_number_c',
            'label' => 'ERROR_CODE_NUMBER',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'case_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_REASON',
          ),
          1 => 
          array (
            'name' => 'parent_case_c',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_CASE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'link_to_monitoring_c',
            'label' => 'LBL_LINK_TO_MONITORING',
          ),
          1 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'country_of_contact_c',
            'label' => 'COUNTRY_OF_CONTACT',
          ),
          1 => 
          array (
            'name' => 'panel_c',
            'studio' => 'visible',
            'label' => 'PANEL',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'escalation_to_tesla_c',
            'studio' => 'visible',
            'label' => 'ESCALATION_TO_TESLA',
          ),
          1 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'tracking_number_date_c',
            'label' => 'TRACKING_NUMBER_DATE',
          ),
          1 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'change_indicator_c',
            'label' => 'LBL_CHANGE_INDICATOR',
          ),
          1 => 
          array (
            'name' => 'details_for_fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'jira_fa_c',
            'label' => 'JIRA_FA',
          ),
          1 => 
          array (
            'name' => 'account_support_comments_c',
            'label' => 'LBL_ACCOUNT_SUPPORT_COMMENTS',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'tier_3_escalation_completed_c',
            'label' => 'TIER_3_ESCALATION_COMPLETED_TIMESTAMP',
          ),
          1 => 
          array (
            'name' => 'tier_2_assigneee_email_c',
            'label' => 'TIER_2_ASSIGNEEE_EMAIL_ADDRESS_FOR_WF',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'aop_case_updates_threaded_custom',
            'studio' => 'visible',
            'label' => 'LBL_AOP_CASE_UPDATES_THREADED',
          ),
        ),
      ),
      'LBL_PANEL_DESCRIPTION' => 
      array (
        0 => 
        array (
          0 => 'description',
        ),
      ),
      'LBL_PANEL_ADDITIONAL_INFORMATION' => 
      array (
        0 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'resolution1_c',
            'studio' => 'visible',
            'label' => 'LBL_RESOLUTION1',
          ),
          1 => 'priority',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'grid_c',
            'studio' => 'visible',
            'label' => 'GRID',
          ),
          1 => 
          array (
            'name' => 'severity_c',
            'studio' => 'visible',
            'label' => 'SEVERITY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'whole_shipping_address_c',
            'label' => 'LBL_WHOLE_SHIPPING_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'customer_complaint_c',
            'label' => 'CUSTOMER_COMPLAINT',
          ),
        ),
      ),
      'LBL_PANEL_CASE_DETAIL' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
        1 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'cpu_firmware_c',
            'label' => 'CPU_FIRMWARE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'solution_statement__c',
            'label' => 'LBL_SOLUTION_STATEMENT_',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'rca_c',
            'label' => 'LBL_RCA',
          ),
          1 => '',
        ),
      ),
      'LBL_PANEL_BATTERY_VALIDATION_DATA' => 
      array (
        0 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'battery_end_warranty_date_c',
            'label' => 'BATTERY_END_WARRANTY_DATE',
          ),
        ),
      ),
      'LBL_PANEL_WEB_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'certification_level_c',
            'label' => 'CERTIFICATION_LEVEL',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'certification_expiry_date_c',
            'label' => 'CERTIFICATION_EXPIRY_DATE',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
