<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/bindWithDelay.js',
        ),
        1 => 
        array (
          'file' => 'modules/AOK_KnowledgeBase/AOK_KnowledgeBase_SuggestionBox.js',
        ),
        2 => 
        array (
          'file' => 'include/javascript/qtip/jquery.qtip.min.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_DESCRIPTION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ADDITIONAL_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'contact_created_by_name',
            'label' => 'LBL_CONTACT_CREATED_BY_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
          1 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'symptom_c',
            'studio' => 'visible',
            'label' => 'SYMPTOM',
          ),
          1 => 
          array (
            'name' => 'sub_symptom_c',
            'studio' => 'visible',
            'label' => 'SUB_SYMPTOM',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 's1_site_cases_name',
            'label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
          ),
          1 => 
          array (
            'name' => 'case_site_c',
            'label' => 'CASE_SITE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'error_code_number_c',
            'label' => 'ERROR_CODE_NUMBER',
          ),
          1 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'inverter_serial_c',
            'label' => 'LBL_INVERTER_SERIAL',
          ),
          1 => 
          array (
            'name' => 'ack_service_instruction_c',
            'label' => 'ACK_SERVICE_INSTRUCTION',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'jira_fa_c',
            'label' => 'JIRA_FA',
          ),
          1 => 
          array (
            'name' => 'case_reason_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_REASON',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
          1 => '',
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'case_record_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_RECORD_TYPE',
          ),
          1 => 
          array (
            'name' => 'tracking_number_date_c',
            'label' => 'TRACKING_NUMBER_DATE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'resolution1_c',
            'studio' => 'visible',
            'label' => 'LBL_RESOLUTION1',
          ),
          1 => 
          array (
            'name' => 'fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'FAULT_SUB_TYPE',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'inverter_model_c',
            'studio' => 'visible',
            'label' => 'INVERTER_MODEL',
          ),
          1 => 
          array (
            'name' => 'error_event_code_c',
            'studio' => 'visible',
            'label' => 'ERROR_EVENT_CODE',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'details_for_fault_sub_type_c',
            'studio' => 'visible',
            'label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER',
          ),
          1 => 
          array (
            'name' => 'panel_c',
            'studio' => 'visible',
            'label' => 'PANEL',
          ),
        ),
      ),
      'LBL_PANEL_DESCRIPTION' => 
      array (
        0 => 
        array (
          0 => 'description',
          1 => '',
        ),
      ),
      'LBL_PANEL_ADDITIONAL_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'doa_c',
            'studio' => 'visible',
            'label' => 'DOA',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'doa_type_c',
            'studio' => 'visible',
            'label' => 'DOA_TYPE',
          ),
          1 => 'priority',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'operating_period_c',
            'studio' => 'visible',
            'label' => 'LBL_OPERATING_PERIOD',
          ),
          1 => 
          array (
            'name' => 'severity_c',
            'studio' => 'visible',
            'label' => 'SEVERITY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'rca_c',
            'label' => 'LBL_RCA',
          ),
          1 => 
          array (
            'name' => 'customer_complaint_c',
            'label' => 'CUSTOMER_COMPLAINT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'customer_should_go_to_the_c',
            'label' => 'CUSTOMER_SHOULD_GO_TO_THE_SITE',
          ),
          1 => 
          array (
            'name' => 'apac_poc_c',
            'label' => 'LBL_APAC_POC',
          ),
        ),
      ),
    ),
  ),
);
?>
