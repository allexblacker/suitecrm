<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/bindWithDelay.js',
        ),
        1 => 
        array (
          'file' => 'modules/AOK_KnowledgeBase/AOK_KnowledgeBase_SuggestionBox.js',
        ),
        2 => 
        array (
          'file' => 'include/javascript/qtip/jquery.qtip.min.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_DESCRIPTION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_SUNPOWER_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'LBL_PANEL_CASE_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'case_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_TYPE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_STATUS',
          ),
          1 => 
          array (
            'name' => 'support_comments_cc_c',
            'label' => 'SUPPORT_COMMENTS_CC',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'case_sub_status_c',
            'studio' => 'visible',
            'label' => 'CASE_SUB_STATUS',
          ),
          1 => 
          array (
            'name' => 'new_internal_comment_c',
            'label' => 'LBL_NEW_INTERNAL_COMMENT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'mass_update_admin_c',
            'label' => 'MASS_UPDATE_ADMIN',
          ),
          1 => 
          array (
            'name' => 'case_record_type_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_RECORD_TYPE',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'alert_override_c',
            'label' => 'LBL_ALERT_OVERRIDE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'case_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ORIGIN',
          ),
          1 => 
          array (
            'name' => 'confirm_contact_address_c',
            'label' => 'CONFIRM_CONTACT_ADDRESS',
          ),
        ),
        7 => 
        array (
          0 => 'priority',
          1 => '',
        ),
      ),
      'LBL_PANEL_DESCRIPTION' => 
      array (
        0 => 
        array (
          0 => 'description',
        ),
      ),
      'LBL_PANEL_SUNPOWER_INFORMATION' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'date_sunpower_partner_c',
            'label' => 'DATE_SUNPOWER_PARTNER_CONTACTED_BY_SEDG',
          ),
          1 => 
          array (
            'name' => 'comments_for_sunpower_c',
            'label' => 'COMMENTS_FOR_SUNPOWER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'sunpower_partner_c',
            'label' => 'LBL_SUNPOWER_PARTNER',
          ),
          1 => 
          array (
            'name' => 'date_rma_approval_c',
            'label' => 'DATE_RMA_APPROVAL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'date_rma_unit_shipped_c',
            'label' => 'LBL_DATE_RMA_UNIT_SHIPPED',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
