{literal}
<style>
	#container {
		padding-left: 20px;
	}
	
	#buttons {
		padding-top: 10px;
		text-align: center;
		width: 50%;
	}
</style>

<script>
	function btnRedirect(btnType) {
		// Default link is cancel
		var link = "index.php?module=Cases&action=index";
		if (btnType == 1) {
			// Continue
			link = "index.php?module=Cases&action=EditView&return_module=Cases&return_action=DetailView&caseType=" + $('#caseType').val() + "&parent_type=" + $('#parent_type').val() + "&parent_id=" + $('#parent_id').val();
		}
		// Create link
		var tIndex = document.location.href.indexOf("index.php");
		var url = document.location.href.substr(0, tIndex);
		window.location = url + link;
		return false;
	}
</script>
{/literal}

<h2>{$title}</h2>
<div id="container">
	<input type="hidden" id="parent_type" value="{$parent_type}"/>
	<input type="hidden" id="parent_id" value="{$parent_id}"/>
	<div id="fields">
		<label>{$label}</label>
		<select id="caseType">
			{foreach from=$options item=label key=key}
				<option value='{$key}'>{$label}</option>
			{/foreach}		
		</select>
	</div>
	<div id="buttons">
		<input type="submit" onclick="return btnRedirect(1);" value="{$btnContinue}" />
		<input type="submit"  onclick="return btnRedirect(2);" value="{$btnCancel}" />
	</div>
</div>