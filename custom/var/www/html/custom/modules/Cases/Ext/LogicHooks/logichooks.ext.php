<?php 
 //WARNING: The contents of this file are auto-generated



if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	100,
	'Updating tier 2 assignees email address',
	'custom/include/dtbc/hooks/update_tier_2_assignee_email_address.php',
	'Tier2AssigneeEmailAddressUpdater',
	'before_save'
);


$hook_array['before_save'][] = array(
	10,
	'Handle Return Merchandise Hooks',
	'custom/include/dtbc/returnmerchandise.php',
	'ReturnMerchandise',
	'before_save'
);


if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	100,
	'Updating field engineer email field',
	'custom/include/dtbc/hooks/update_field_engineer_email.php',
	'FieldEngineerEmailUpdater',
	'before_save'
);


$hook_array['after_save'][] = array(
	10,
	'Logging how much time spent in each statuses',
	'custom/include/dtbc/statuslogger.php',
	'StatusLogger',
	'after_save'
);


$hook_array['after_save'][] = array(
	30,
	'When "Notify Customer Date" is entered then the Contact will automatically receive an email with the Notify Email Template.',
	'custom/include/dtbc/notifycustomerdate.php',
	'NotifyCustomerDate',
	'after_save'
);


$hook_array['after_save'][] = array(
	20,
	'When the Case Sub-Status is changed to ‘Pending Agent Call’ case owner will receive an email',
	'custom/include/dtbc/pendingagentcall.php',
	'PendingAgentCall',
	'after_save'
);


$hook_array['process_record'][] = array(
	10,
	'Display additional infos on the list view',
	'custom/include/dtbc/caseListView.php',
	'ProcessRecordsCases',
	'process_record'
);


$hook_array['before_save'][] = array(
	1,
	'Fill all the formula fields on Cases',
	'custom/include/dtbc/formulafields/formulafields_cases.php',
	'FormulaFields',
	'before_save'
);

 
$hook_array['after_save'][] = array(
	175,
	'Remove Assigned user if the case is newly created from an email',
	'custom/include/dtbc/hooks/75_remove_assigned_user.php',
	'RemoveAssignedUser',
	'after_save'
);


if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	100,
	'DATE will be updated automatically once the field sub status is updated workflow',
	'custom/include/dtbc/hooks/workflow_date_will_be_updated_automatically_once_the_field_sub_status_is_updated.php',
	'SubStatudDateUpdater',
	'before_save'
);


$hook_array['after_relationship_add'][] = array(
	10,
	'Automatically fill acase_id field for SolarEdge team',
	'custom/include/dtbc/hooks/caseRelationShips.php',
	'HandleRelationShips',
	'after_relationship_add'
);

$hook_array['after_relationship_delete'][] = array(
	10,
	'Automatically remove acase_id field\'s value for SolarEdge team',
	'custom/include/dtbc/hooks/caseRelationShips.php',
	'HandleRelationShips',
	'after_relationship_delete'
);

 
$hook_array['after_save'][] = array(
	70,
	'If the date entered is midnight, update it to current date',
	'custom/include/dtbc/hooks/70_update_date_entered.php',
	'UpdateDateEntered',
	'after_save'
);


$hook_array['after_save'][] = array(
	41,
	'Run only for service partner cases',
	'custom/include/dtbc/spcase.php',
	'SpCase',
	'after_save'
);


$hook_array['after_save'][] = array(
	40,
	'Run only for compensation cases - set status and send email',
	'custom/include/dtbc/compensationcase.php',
	'CompensationCase',
	'after_save'
);


if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	21,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_eu'
);

$hook_array['before_save'][] = array(
	22,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_il'
);

$hook_array['before_save'][] = array(
	23,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_asia'
);

$hook_array['before_save'][] = array(
	24,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_aus'
);
?>