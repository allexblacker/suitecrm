<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-23 15:16:06
$dictionary['Case']['fields']['contact_name_c']['inline_edit']='1';
$dictionary['Case']['fields']['contact_name_c']['labelValue']='contact name';

 

 // created: 2017-09-28 15:52:08
$dictionary['Case']['fields']['date_rma_unit_shipped_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_rma_unit_shipped_c']['labelValue']='Date RMA unit shipped';

 

 // created: 2017-09-01 18:29:42
$dictionary['Case']['fields']['vendor_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['vendor_number_c']['labelValue']='Vendor Number';

 

 // created: 2017-12-01 03:41:38
$dictionary['Case']['fields']['optimizer_inverter_pn_c']['inline_edit']='1';
$dictionary['Case']['fields']['optimizer_inverter_pn_c']['labelValue']='optimizer inverter pn';

 

 // created: 2017-11-10 14:36:58
$dictionary['Case']['fields']['reason_for_not_in_warranty_c']['inline_edit']='1';
$dictionary['Case']['fields']['reason_for_not_in_warranty_c']['labelValue']='reason for not in warranty';

 

 // created: 2017-11-09 17:00:16
$dictionary['Case']['fields']['case_origin_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_origin_c']['labelValue']='Case Origin';

 

 // created: 2017-09-28 15:52:25
$dictionary['Case']['fields']['last_telemetry_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['last_telemetry_date_c']['labelValue']='Last telemetry date';

 

 // created: 2017-08-29 20:22:09
$dictionary['Case']['fields']['case_state_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_state_c']['labelValue']='State';

 

 // created: 2017-10-19 17:55:21
$dictionary['Case']['fields']['installer_s_grade_c']['inline_edit']='1';
$dictionary['Case']['fields']['installer_s_grade_c']['labelValue']='Contact\'s Grade';

 

 // created: 2017-11-19 14:44:37
$dictionary['Case']['fields']['doa_c']['inline_edit']='1';
$dictionary['Case']['fields']['doa_c']['labelValue']='DOA';

 

 // created: 2017-10-19 17:49:42
$dictionary['Case']['fields']['site_contact_person_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_contact_person_email_c']['labelValue']='Site Contact Person Email';

 

// created: 2017-09-07 14:41:36
$dictionary["Case"]["fields"]["cases_dtbc_statuslog_1"] = array (
  'name' => 'cases_dtbc_statuslog_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_statuslog_1',
  'source' => 'non-db',
  'module' => 'dtbc_StatusLog',
  'bean_name' => 'dtbc_StatusLog',
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_STATUSLOG_1_FROM_DTBC_STATUSLOG_TITLE',
);


 // created: 2017-10-15 18:01:12
$dictionary['Case']['fields']['case_sub_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_sub_status_c']['labelValue']='Case sub Status';

 

 // created: 2017-11-10 13:40:04
$dictionary['Case']['fields']['receiving_tracking_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['receiving_tracking_number_c']['labelValue']='Receiving Tracking Number';

 

 // created: 2017-11-11 11:21:10
$dictionary['Case']['fields']['emailtocase_notificationsent_c']['inline_edit']='1';
$dictionary['Case']['fields']['emailtocase_notificationsent_c']['labelValue']='emailtocase notificationsent';

 

 // created: 2017-10-24 16:37:45
$dictionary['Case']['fields']['battery_warranty_still_valid_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_warranty_still_valid_c']['labelValue']='Battery Warranty Is Still Valid';

 

 // created: 2017-08-29 18:34:16
$dictionary['Case']['fields']['county_c']['inline_edit']='1';
$dictionary['Case']['fields']['county_c']['labelValue']='County';

 

 // created: 2017-09-07 19:43:51
$dictionary['Case']['fields']['rma_contant_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_contant_email_c']['labelValue']='RMA Contact Email';

 

 // created: 2017-11-16 17:35:53
$dictionary['Case']['fields']['support_comments_cc_c']['inline_edit']='1';
$dictionary['Case']['fields']['support_comments_cc_c']['labelValue']='Support Comments';

 

 // created: 2017-10-19 17:52:50
$dictionary['Case']['fields']['visit_order_c']['inline_edit']='1';
$dictionary['Case']['fields']['visit_order_c']['labelValue']='Visit Order';

 

 // created: 2017-10-15 18:01:55
$dictionary['Case']['fields']['inverter_model_c']['inline_edit']='1';
$dictionary['Case']['fields']['inverter_model_c']['labelValue']='Model';

 

 // created: 2017-10-18 13:17:24

 

 // created: 2017-09-06 11:25:34
$dictionary['Case']['fields']['tesla_expiration_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['tesla_expiration_date_c']['labelValue']='Tesla Expiration Date';

 

 // created: 2017-09-28 16:00:18
$dictionary['Case']['fields']['waiting_for_tier_1_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['waiting_for_tier_1_timestamp_c']['labelValue']='Waiting for Tier 1 timestamp';

 

 // created: 2017-11-16 11:09:52
$dictionary['Case']['fields']['sub_symptom_c']['inline_edit']='1';
$dictionary['Case']['fields']['sub_symptom_c']['labelValue']='Sub Category';

 

 // created: 2017-09-29 15:58:57
$dictionary['Case']['fields']['testfield1_c']['inline_edit']='1';
$dictionary['Case']['fields']['testfield1_c']['labelValue']='TestField1';

 

 // created: 2017-10-04 13:33:57
$dictionary['Case']['fields']['acase_id_c']['inline_edit']=1;

 

 // created: 2017-11-08 17:32:22
$dictionary['Case']['fields']['transferred_to_finance_c']['inline_edit']='1';
$dictionary['Case']['fields']['transferred_to_finance_c']['labelValue']='Transferred To Finance';

 

 // created: 2017-08-29 17:07:52
$dictionary['Case']['fields']['cpu_firmware_c']['inline_edit']='1';
$dictionary['Case']['fields']['cpu_firmware_c']['labelValue']='CPU Firmware';

 

 // created: 2017-09-29 19:14:17
$dictionary['Case']['fields']['priority']['inline_edit']=true;
$dictionary['Case']['fields']['priority']['options']='case_priority_list';
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';

 

 // created: 2017-09-08 11:41:47
$dictionary['Case']['fields']['link_to_monitoring_c']['inline_edit']='1';
$dictionary['Case']['fields']['link_to_monitoring_c']['labelValue']='Link to Monitoring';

 

 // created: 2017-10-19 18:17:53
$dictionary['Case']['fields']['severity_c']['inline_edit']='1';
$dictionary['Case']['fields']['severity_c']['labelValue']='Severity';

 

 // created: 2017-10-19 17:43:41
$dictionary['Case']['fields']['any_battery_serial_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['any_battery_serial_number_c']['labelValue']='Any Battery Serial Number';

 

 // created: 2017-10-19 17:43:18
$dictionary['Case']['fields']['pr_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['pr_number_c']['labelValue']='Compensation PR number';

 

 // created: 2017-11-10 15:06:42
$dictionary['Case']['fields']['comments_for_sunpower_c']['inline_edit']='1';
$dictionary['Case']['fields']['comments_for_sunpower_c']['labelValue']='Comments for Sunpower';

 

 // created: 2017-10-10 11:38:42
$dictionary['Case']['fields']['any_inverter_serial_req_for_c']['inline_edit']='1';
$dictionary['Case']['fields']['any_inverter_serial_req_for_c']['labelValue']='any inverter serial# (req for tech issue)';

 

 // created: 2017-11-22 17:06:09
$dictionary['Case']['fields']['rca_need_imm_return_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['rca_need_imm_return_email_c']['labelValue']='RCA and need immediate return email addr';

 

 // created: 2017-11-17 08:38:10
$dictionary['Case']['fields']['case_reason_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_reason_c']['labelValue']='Fault Category';

 

 // created: 2017-10-08 13:57:02
$dictionary['Case']['fields']['case_number']['comments']='Visual unique identifier';
$dictionary['Case']['fields']['case_number']['merge_filter']='disabled';
$dictionary['Case']['fields']['case_number']['enable_range_search']=false;
$dictionary['Case']['fields']['case_number']['autoinc_next']='117';
$dictionary['Case']['fields']['case_number']['min']=false;
$dictionary['Case']['fields']['case_number']['max']=false;

 

 // created: 2017-10-19 18:16:27
$dictionary['Case']['fields']['state_c']['inline_edit']='1';
$dictionary['Case']['fields']['state_c']['labelValue']='state';

 

 // created: 2017-10-19 18:32:41
$dictionary['Case']['fields']['send_email_close_c']['inline_edit']='1';
$dictionary['Case']['fields']['send_email_close_c']['labelValue']='Send Email & Close';

 

 // created: 2017-11-15 20:22:15
$dictionary['Case']['fields']['rma_approved_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_approved_date_c']['labelValue']='RMA Approved Date';

 

 // created: 2017-10-19 17:42:36
$dictionary['Case']['fields']['ship_to_name_c']['inline_edit']='1';
$dictionary['Case']['fields']['ship_to_name_c']['labelValue']='Ship To Name';

 

 // created: 2017-10-24 14:36:42
$dictionary['Case']['fields']['case_site_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_site_c']['labelValue']='Monitoring Site Name';

 

 // created: 2017-10-30 18:28:11
$dictionary['Case']['fields']['auto_assigned_c']['inline_edit']='';
$dictionary['Case']['fields']['auto_assigned_c']['labelValue']='auto assigned';

 

 // created: 2017-10-19 17:32:03
$dictionary['Case']['fields']['cause_for_not_approved_c']['inline_edit']='1';
$dictionary['Case']['fields']['cause_for_not_approved_c']['labelValue']='Reason for not approved';

 

 // created: 2017-10-19 17:27:17
$dictionary['Case']['fields']['tesla_case_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['tesla_case_number_c']['labelValue']='Battery supplier case number';

 

 // created: 2017-12-01 03:24:02
$dictionary['Case']['fields']['doa_rma_c']['inline_edit']='1';
$dictionary['Case']['fields']['doa_rma_c']['labelValue']='doa rma';

 

 // created: 2017-10-19 17:52:27
$dictionary['Case']['fields']['change_details_c']['inline_edit']='1';
$dictionary['Case']['fields']['change_details_c']['labelValue']='Change Details';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['dtbc_notify_customer_date'] = array(
	'name' => 'dtbc_notify_customer_date',
	'vname' => 'LBL_NOTIFY_CUSTOMER_DATE',
	'type' => 'datetimecombo',
	'dbType' => 'datetime',
	'comment' => 'Notify Customer Date',
	'importable' => 'true',
	'required' => false,
);

 // created: 2017-08-29 19:01:20
$dictionary['Case']['fields']['return_asked_by_c']['inline_edit']='1';
$dictionary['Case']['fields']['return_asked_by_c']['labelValue']='Return asked by';

 

 // created: 2017-08-30 19:21:07
$dictionary['Case']['fields']['battery_valid_for_service_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_valid_for_service_c']['labelValue']='Battery Valid For Service';

 

 // created: 2017-10-19 18:15:58
$dictionary['Case']['fields']['address_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['address_type_c']['labelValue']='Address type';

 

 // created: 2017-10-19 17:31:37
$dictionary['Case']['fields']['distributer_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['distributer_number_c']['labelValue']='Distributer Number';

 

 // created: 2017-11-10 14:39:01
$dictionary['Case']['fields']['battery_part_description_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_part_description_c']['labelValue']='Battery Part Description';

 

 // created: 2017-11-20 11:51:13
$dictionary['Case']['fields']['estimated_pay_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['estimated_pay_date_c']['labelValue']='Estimated Pay Date';

 

 // created: 2017-12-01 02:32:44
$dictionary['Case']['fields']['doa_early_mortality_c']['inline_edit']='1';
$dictionary['Case']['fields']['doa_early_mortality_c']['labelValue']='doa early mortality';

 

 // created: 2017-08-29 18:37:25
$dictionary['Case']['fields']['operating_period_c']['inline_edit']='1';
$dictionary['Case']['fields']['operating_period_c']['labelValue']='Operating Period';

 

 // created: 2017-11-20 15:16:09
$dictionary['Case']['fields']['escalated_to_t2_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['escalated_to_t2_timestamp_c']['labelValue']='escalated to tier 2 timestamp';

 

 // created: 2017-08-29 20:11:14
$dictionary['Case']['fields']['site_address_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_address_c']['labelValue']='Site Address';

 

 // created: 2017-09-28 15:53:09
$dictionary['Case']['fields']['date_sunpower_partner_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_sunpower_partner_c']['labelValue']='Date SunPower-Partner contacted by SEDG';

 

 // created: 2017-08-29 18:35:32
$dictionary['Case']['fields']['new_internal_comment_c']['inline_edit']='1';
$dictionary['Case']['fields']['new_internal_comment_c']['labelValue']='New Internal Comment';

 

 // created: 2017-10-24 13:46:58
$dictionary['Case']['fields']['lg_tesla_for_email_template_c']['inline_edit']='1';
$dictionary['Case']['fields']['lg_tesla_for_email_template_c']['labelValue']='LG/Tesla for email template';

 

 // created: 2017-09-28 15:54:59
$dictionary['Case']['fields']['tier_1_germany_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_1_germany_timestamp_c']['labelValue']='Tier 1 Germany timestamp';

 

 // created: 2017-08-29 17:09:19
$dictionary['Case']['fields']['activation_code_c']['inline_edit']='1';
$dictionary['Case']['fields']['activation_code_c']['labelValue']='Activation Code';

 

// created: 2017-09-11 18:28:20
$dictionary["Case"]["fields"]["cases_sn1_serial_number_1"] = array (
  'name' => 'cases_sn1_serial_number_1',
  'type' => 'link',
  'relationship' => 'cases_sn1_serial_number_1',
  'source' => 'non-db',
  'module' => 'SN1_Serial_Number',
  'bean_name' => 'SN1_Serial_Number',
  'side' => 'right',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE',
);


 // created: 2017-10-19 17:30:33
$dictionary['Case']['fields']['return_label_awb_c']['inline_edit']='1';
$dictionary['Case']['fields']['return_label_awb_c']['labelValue']='Return Tracking Number';

 

 // created: 2017-08-24 18:06:14
$dictionary['Case']['fields']['alert_override_c']['inline_edit']='1';
$dictionary['Case']['fields']['alert_override_c']['labelValue']='alert override';

 

 // created: 2017-10-19 17:29:32
$dictionary['Case']['fields']['inverter_s_size_s_c']['inline_edit']='1';
$dictionary['Case']['fields']['inverter_s_size_s_c']['labelValue']='Inverter Sizes (for US Digital)';

 

 // created: 2017-09-08 11:43:01
$dictionary['Case']['fields']['link_to_ups_2_c']['inline_edit']='1';
$dictionary['Case']['fields']['link_to_ups_2_c']['labelValue']='Link to UPS 2';

 

 // created: 2017-10-19 18:15:24
$dictionary['Case']['fields']['shipping_service_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['shipping_service_type_c']['labelValue']='Shipping Service Type';

 

 // created: 2017-08-29 20:23:58
$dictionary['Case']['fields']['vip_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['vip_flag_c']['labelValue']='SunPower Flag';

 

// created: 2017-08-31 12:53:12
$dictionary["Case"]["fields"]["uzc1_usa_zip_codes_cases"] = array (
  'name' => 'uzc1_usa_zip_codes_cases',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_cases',
  'source' => 'non-db',
  'module' => 'UZC1_USA_ZIP_Codes',
  'bean_name' => 'UZC1_USA_ZIP_Codes',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
  'id_name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
);
$dictionary["Case"]["fields"]["uzc1_usa_zip_codes_cases_name"] = array (
  'name' => 'uzc1_usa_zip_codes_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE',
  'save' => true,
  'id_name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
  'link' => 'uzc1_usa_zip_codes_cases',
  'table' => 'uzc1_usa_zip_codes',
  'module' => 'UZC1_USA_ZIP_Codes',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida"] = array (
  'name' => 'uzc1_usa_zip_codes_casesuzc1_usa_zip_codes_ida',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_cases',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_CASES_TITLE',
);


 // created: 2017-10-19 18:32:19
$dictionary['Case']['fields']['tier_3_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_3_flag_c']['labelValue']='Tier 3 Flag';

 

 // created: 2017-11-03 18:00:31
$dictionary['Case']['fields']['tracking_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['tracking_number_c']['labelValue']='Tracking Number';

 

 // created: 2017-10-28 02:00:45
$dictionary['Case']['fields']['distributor_c']['inline_edit']='1';
$dictionary['Case']['fields']['distributor_c']['labelValue']='Distributor Name';

 

 // created: 2017-11-20 16:49:03
$dictionary['Case']['fields']['reason_for_not_eligible_c']['inline_edit']='1';
$dictionary['Case']['fields']['reason_for_not_eligible_c']['labelValue']='Reason for not Eligible';

 

 // created: 2017-10-28 02:04:24
$dictionary['Case']['fields']['tracking_number_link_c']['inline_edit']='1';
$dictionary['Case']['fields']['tracking_number_link_c']['labelValue']='Tracking Number link';

 

 // created: 2017-10-19 17:28:53
$dictionary['Case']['fields']['certification_level_c']['inline_edit']='1';
$dictionary['Case']['fields']['certification_level_c']['labelValue']='Certification Level';

 

 // created: 2017-08-29 17:46:19
$dictionary['Case']['fields']['inverter_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['inverter_type_c']['labelValue']='Inverter Type';

 

 // created: 2017-11-26 12:50:14
$dictionary['Case']['fields']['status']['required']=true;
$dictionary['Case']['fields']['status']['inline_edit']=true;
$dictionary['Case']['fields']['status']['comments']='The status of the case';
$dictionary['Case']['fields']['status']['merge_filter']='disabled';

 

 // created: 2017-08-22 15:35:11
$dictionary['Case']['fields']['tile_roof_c']['inline_edit']='1';
$dictionary['Case']['fields']['tile_roof_c']['labelValue']='Tile Roof';

 

 // created: 2017-10-19 18:31:56
$dictionary['Case']['fields']['if_eligible_was_true_c']['inline_edit']='1';
$dictionary['Case']['fields']['if_eligible_was_true_c']['labelValue']='If Eligible was true';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['contact_email_c'] = array(
	'required' => false,
	'name' => 'contact_email_c',
	'vname' => 'LBL_CONTACT_EMAIL',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);
$dictionary['Case']['fields']['contact_phone_c'] = array(
	'required' => false,
	'name' => 'contact_phone_c',
	'vname' => 'LBL_CONTACT_PHONE',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['web_email_c'] = array(
	'required' => false,
	'name' => 'web_email_c',
	'vname' => 'LBL_WEB_EMAIL',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['web_name_c'] = array(
	'required' => false,
	'name' => 'web_name_c',
	'vname' => 'LBL_WEB_NAME',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['field_engineer_email_c'] = array(
	'required' => false,
	'name' => 'field_engineer_email_c',
	'vname' => 'LBL_FIELD_ENGINEER_EMAIL',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['origin_email_address_c'] = array(
	'required' => false,
	'name' => 'origin_email_address_c',
	'vname' => 'LBL_FIELD_ORIGIN_EMAIL',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['is_email_to_case'] = array(
	'required' => false,
	'name' => 'is_email_to_case',
	'vname' => 'LBL_FIELD_IS_EMAIL_TO_CASE',
	'type' => 'bool',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['last_sub_status_change'] = array(
	'required' => false,
	'name' => 'last_sub_status_change',
	'vname' => 'LBL_FIELD_LAST_SUBSTATUSCHANGE',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);



 // created: 2017-10-19 18:31:37
$dictionary['Case']['fields']['certification_data_updated_c']['inline_edit']='1';
$dictionary['Case']['fields']['certification_data_updated_c']['labelValue']='Certification Data Updated';

 

 // created: 2017-12-01 19:12:26
$dictionary['Case']['fields']['tier_2_assignee_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_2_assignee_email_c']['labelValue']='Tier 2 Assignee email';

 

 // created: 2017-10-19 18:30:58
$dictionary['Case']['fields']['battery_is_monitored_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_is_monitored_c']['labelValue']='Battery Is Monitored';

 

 // created: 2017-08-29 20:10:07
$dictionary['Case']['fields']['service_level_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['service_level_flag_c']['labelValue']='Service Level Flag';

 

 // created: 2017-08-29 17:26:24
$dictionary['Case']['fields']['inverter_id_c']['inline_edit']='1';
$dictionary['Case']['fields']['inverter_id_c']['labelValue']='Inverter ID';

 

 // created: 2017-10-27 15:47:07
$dictionary['Case']['fields']['alliance_region_c']['inline_edit']='1';
$dictionary['Case']['fields']['alliance_region_c']['labelValue']='Alliance Region';

 

 // created: 2017-10-19 17:28:27
$dictionary['Case']['fields']['warranty_end_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['warranty_end_date_c']['labelValue']='Warranty End Date';

 

 // created: 2017-11-22 14:41:01
$dictionary['Case']['fields']['rma_country_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_country_c']['labelValue']='RMA Country';

 

 // created: 2017-10-19 18:12:58
$dictionary['Case']['fields']['field_engineer_c']['inline_edit']='1';
$dictionary['Case']['fields']['field_engineer_c']['labelValue']='Field Engineer Country';

 

 // created: 2017-08-29 17:49:17
$dictionary['Case']['fields']['name_of_person_who_approves_c']['inline_edit']='1';
$dictionary['Case']['fields']['name_of_person_who_approves_c']['labelValue']='Name of Person who Approves RMA';

 

 // created: 2017-11-20 15:18:04
$dictionary['Case']['fields']['escalated_to_tesla_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['escalated_to_tesla_timestamp_c']['labelValue']='escalated to tesla timestamp';

 

 // created: 2017-11-20 12:16:25
$dictionary['Case']['fields']['fault_category_c']['inline_edit']='1';
$dictionary['Case']['fields']['fault_category_c']['labelValue']='Fault Category old';

 

 // created: 2017-10-19 18:30:37
$dictionary['Case']['fields']['communication_board_was_c']['inline_edit']='1';
$dictionary['Case']['fields']['communication_board_was_c']['labelValue']='Communication board was replaced?';

 

 // created: 2017-10-19 18:29:01
$dictionary['Case']['fields']['change_checkbox_c']['inline_edit']='1';
$dictionary['Case']['fields']['change_checkbox_c']['labelValue']='Change Checkbox';

 

 // created: 2017-10-15 18:02:31
$dictionary['Case']['fields']['panel_c']['inline_edit']='1';
$dictionary['Case']['fields']['panel_c']['labelValue']='Panel manufacturer';

 

 // created: 2017-10-24 18:25:36
$dictionary['Case']['fields']['date_time_closed_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_time_closed_c']['labelValue']='date time closed';

 

 // created: 2017-10-19 18:20:18
$dictionary['Case']['fields']['paid_amount_c']['inline_edit']='1';
$dictionary['Case']['fields']['paid_amount_c']['labelValue']='Paid Amount';

 

 // created: 2017-11-21 13:08:02
$dictionary['Case']['fields']['new_email_comment_for_wf_c']['inline_edit']='1';
$dictionary['Case']['fields']['new_email_comment_for_wf_c']['labelValue']='New email comment for wf';

 

 // created: 2017-09-28 13:59:39
$dictionary['Case']['fields']['doa_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['doa_type_c']['labelValue']='DOA Type';

 

 // created: 2017-08-24 18:00:35
$dictionary['Case']['fields']['customer_complaint_c']['inline_edit']='1';
$dictionary['Case']['fields']['customer_complaint_c']['labelValue']='Customer Complaint';

 

 // created: 2017-11-22 13:39:27
$dictionary['Case']['fields']['rca_and_need_immediate_ret_c']['inline_edit']='1';
$dictionary['Case']['fields']['rca_and_need_immediate_ret_c']['labelValue']='RCA and need immediate return';

 

 // created: 2017-10-19 18:28:40
$dictionary['Case']['fields']['mass_update_admin_c']['inline_edit']='1';
$dictionary['Case']['fields']['mass_update_admin_c']['labelValue']='MASS update ADMIN';

 

 // created: 2017-09-28 19:02:42
$dictionary['Case']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2017-08-30 18:13:03
$dictionary['Case']['fields']['account_contact_s_comments_c']['inline_edit']='1';
$dictionary['Case']['fields']['account_contact_s_comments_c']['labelValue']='Account/Contact Support Comments';

 

 // created: 2017-08-29 17:09:04
$dictionary['Case']['fields']['dsp2_c']['inline_edit']='1';
$dictionary['Case']['fields']['dsp2_c']['labelValue']='DSP2';

 

 // created: 2017-12-01 03:05:34
$dictionary['Case']['fields']['optimizer_inverter_c']['inline_edit']='1';
$dictionary['Case']['fields']['optimizer_inverter_c']['labelValue']='optimizer inverter';

 

 // created: 2017-10-19 17:50:41
$dictionary['Case']['fields']['battery_fault_description_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_fault_description_c']['labelValue']='Battery Fault description';

 

 // created: 2017-08-29 18:21:18
$dictionary['Case']['fields']['solaredge_code_for_c']['inline_edit']='1';
$dictionary['Case']['fields']['solaredge_code_for_c']['labelValue']='SolarEdge Code for Certification';

 

 // created: 2017-10-19 17:29:56
$dictionary['Case']['fields']['refurbish_c']['inline_edit']='1';
$dictionary['Case']['fields']['refurbish_c']['labelValue']='refurbish?';

 

 // created: 2017-10-28 02:05:49
$dictionary['Case']['fields']['site_contact_person_phone_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_contact_person_phone_c']['labelValue']='RMA Contact Phone';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['email_serial_numbers'] = array(
	'name' => 'email_serial_numbers',
	'label' => 'LBL_POINTS_TOTAL',
	'vname' => 'LBL_POINTS_TOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_serial_numbers',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_serial_numbers.php',
	), 
);


 // created: 2017-08-23 12:35:39
$dictionary['Case']['fields']['rma_street_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_street_c']['labelValue']='RMA Street';

 

 // created: 2017-10-19 18:19:00
$dictionary['Case']['fields']['points_balance_on_case_open_c']['inline_edit']='1';
$dictionary['Case']['fields']['points_balance_on_case_open_c']['labelValue']='Points Balance on Case Open Date';

 

 // created: 2017-09-28 15:55:10
$dictionary['Case']['fields']['first_telemetry_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['first_telemetry_date_c']['labelValue']='First Telemetry Date';

 

 // created: 2017-10-02 16:19:17
$dictionary['Case']['fields']['internal_comments_c']['inline_edit']='1';
$dictionary['Case']['fields']['internal_comments_c']['labelValue']='Site Comment';

 

 // created: 2017-10-28 02:11:45
$dictionary['Case']['fields']['tracking_number_fa_c']['inline_edit']='1';
$dictionary['Case']['fields']['tracking_number_fa_c']['labelValue']='Tracking number-FA';

 

 // created: 2017-11-29 20:18:05
$dictionary['Case']['fields']['urgency_c']['inline_edit']='1';
$dictionary['Case']['fields']['urgency_c']['labelValue']='Urgency';

 

 // created: 2017-09-28 15:55:22
$dictionary['Case']['fields']['date_taken_from_tier_1_us_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_taken_from_tier_1_us_c']['labelValue']='Date taken from Tier 1 US';

 

 // created: 2017-11-09 18:32:23
$dictionary['Case']['fields']['case_type_c']['inline_edit']='';
$dictionary['Case']['fields']['case_type_c']['labelValue']='Type';

 

 // created: 2017-10-19 17:52:01
$dictionary['Case']['fields']['notes_rma_team_us_c']['inline_edit']='1';
$dictionary['Case']['fields']['notes_rma_team_us_c']['labelValue']='Special Instructions for RMA team';

 

// created: 2017-08-29 20:14:53
$dictionary["Case"]["fields"]["cases_cases_2"] = array (
  'name' => 'cases_cases_2',
  'type' => 'link',
  'relationship' => 'cases_cases_2',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_2_FROM_CASES_L_TITLE',
  'id_name' => 'cases_cases_2cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_2_name"] = array (
  'name' => 'cases_cases_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_2_FROM_CASES_L_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_2cases_ida',
  'link' => 'cases_cases_2',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_2cases_ida"] = array (
  'name' => 'cases_cases_2cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_CASES_2_FROM_CASES_R_TITLE',
);



if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['aop_case_updates_threaded_custom'] = array(
	'required' => false,
	'name' => 'aop_case_updates_threaded_custom',
	'vname' => 'LBL_AOP_CASE_UPDATES_THREADED',
	'type' => 'function',
	'source' => 'non-db',
	'massupdate' => 0,
	'studio' => 'visible',
	'importable' => 'false',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => 0,
	'audited' => false,
	'reportable' => false,
	'inline_edit' => 0,
	'function' => array(
		'name' => 'display_updates_custom',
		'returns' => 'html',
		'include' => 'custom/modules/AOP_Case_Updates/Case_Updates.php',
	),
);


 // created: 2017-11-09 11:24:05
$dictionary['Case']['fields']['case_record_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_record_type_c']['labelValue']='Case Record Type';

 

 // created: 2017-09-06 16:10:05
$dictionary['Case']['fields']['need_fa_immediate_return_c']['inline_edit']='1';
$dictionary['Case']['fields']['need_fa_immediate_return_c']['labelValue']='Need FA Immediate return';

 

 // created: 2017-09-28 16:00:33
$dictionary['Case']['fields']['dtbc_notify_customer_date']['display_default']='now&12:00am';
$dictionary['Case']['fields']['dtbc_notify_customer_date']['inline_edit']=true;
$dictionary['Case']['fields']['dtbc_notify_customer_date']['comments']='Notify Customer Date';
$dictionary['Case']['fields']['dtbc_notify_customer_date']['merge_filter']='disabled';
$dictionary['Case']['fields']['dtbc_notify_customer_date']['enable_range_search']=false;

 

 // created: 2017-11-22 10:12:02
$dictionary['Case']['fields']['chat_email_c']['inline_edit']='';
$dictionary['Case']['fields']['chat_email_c']['labelValue']='Chat Email';

 

 // created: 2017-10-08 18:56:12
$dictionary['Case']['fields']['battery_reason_for_not_in_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_reason_for_not_in_c']['labelValue']='Battery Reason For not in warranty';

 

 // created: 2017-10-24 13:51:19
$dictionary['Case']['fields']['immediate_escalation_to_batt_c']['inline_edit']='1';
$dictionary['Case']['fields']['immediate_escalation_to_batt_c']['labelValue']='Immediate escalation to Battery supplier';

 

 // created: 2017-08-30 18:13:11
$dictionary['Case']['fields']['account_support_comments_c']['inline_edit']='1';
$dictionary['Case']['fields']['account_support_comments_c']['labelValue']='Account Support Comments';

 

 // created: 2017-11-10 15:13:08
$dictionary['Case']['fields']['battery_distributor_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_distributor_c']['labelValue']='Battery Distributor';

 

 // created: 2017-10-19 18:12:29
$dictionary['Case']['fields']['site_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_type_c']['labelValue']='Site Type';

 

 // created: 2017-11-17 11:49:16
$dictionary['Case']['fields']['battery_software_version_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_software_version_c']['labelValue']='Battery Software Version';

 

 // created: 2017-10-19 17:28:01
$dictionary['Case']['fields']['return_label_created_c']['inline_edit']='1';
$dictionary['Case']['fields']['return_label_created_c']['labelValue']='Return Label Created';

 

 // created: 2017-09-18 17:49:57
$dictionary['Case']['fields']['resolution1_c']['inline_edit']='1';
$dictionary['Case']['fields']['resolution1_c']['labelValue']='resolution1';

 

 // created: 2017-10-19 17:26:47
$dictionary['Case']['fields']['compensation_po_n_c']['inline_edit']='1';
$dictionary['Case']['fields']['compensation_po_n_c']['labelValue']='Compensation PO Number';

 

 // created: 2017-11-20 17:19:09
$dictionary['Case']['fields']['web_email_c']['default']='';

 

 // created: 2017-09-06 11:15:04
$dictionary['Case']['fields']['battery_shipment_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_shipment_date_c']['labelValue']='Battery Shipment Date';

 

 // created: 2017-11-21 10:15:01
$dictionary['Case']['fields']['fault_sub_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['fault_sub_type_c']['labelValue']='Fault sub-category';

 

 // created: 2017-08-29 20:13:05
$dictionary['Case']['fields']['site_zip_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_zip_c']['labelValue']='Site Zip';

 

 // created: 2017-08-24 18:36:52
$dictionary['Case']['fields']['eligible_for_compensation_c']['inline_edit']='1';
$dictionary['Case']['fields']['eligible_for_compensation_c']['labelValue']='Eligible for Compensation';

 

 // created: 2017-11-10 12:16:50
$dictionary['Case']['fields']['rma_in_erp_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_in_erp_c']['labelValue']='RMA # in ERP';

 

 // created: 2017-10-19 17:25:49
$dictionary['Case']['fields']['battery_installer_id_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_installer_id_c']['labelValue']='Battery Installer ID';

 

 // created: 2017-08-14 15:51:57
$dictionary['Case']['fields']['rma_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_c']['labelValue']='RMA?';

 

 // created: 2017-10-19 18:27:47
$dictionary['Case']['fields']['paid_by_ltd_c']['inline_edit']='1';
$dictionary['Case']['fields']['paid_by_ltd_c']['labelValue']='paid by LTD';

 

 // created: 2017-08-29 17:10:12
$dictionary['Case']['fields']['current_software_version_c']['inline_edit']='1';
$dictionary['Case']['fields']['current_software_version_c']['labelValue']='Current Software version';

 

 // created: 2017-08-22 12:31:33
$dictionary['Case']['fields']['serial_numbers_c']['inline_edit']='1';
$dictionary['Case']['fields']['serial_numbers_c']['labelValue']='Serial Numbers (comma delimited)';

 

 // created: 2017-08-29 20:07:59
$dictionary['Case']['fields']['roof_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['roof_type_c']['labelValue']='Roof Type';

 

 // created: 2017-11-02 22:00:40
$dictionary['Case']['fields']['latest_validation_error_c']['inline_edit']='1';
$dictionary['Case']['fields']['latest_validation_error_c']['labelValue']='Latest Validation Error';

 

 // created: 2017-09-28 15:56:05
$dictionary['Case']['fields']['certification_expiry_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['certification_expiry_date_c']['labelValue']='Certification expiry date';

 

 // created: 2017-09-28 16:00:45
$dictionary['Case']['fields']['timestamp_of_approval_c']['inline_edit']='1';
$dictionary['Case']['fields']['timestamp_of_approval_c']['labelValue']='Timestamp of Approval';

 

// created: 2017-09-08 20:38:49
$dictionary["Case"]["fields"]["cases_dtbc_casessurvey_1"] = array (
  'name' => 'cases_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'dtbc_CasesSurvey',
  'bean_name' => 'dtbc_CasesSurvey',
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);


 // created: 2017-08-30 18:13:42
$dictionary['Case']['fields']['internal_comment_c']['inline_edit']='1';
$dictionary['Case']['fields']['internal_comment_c']['labelValue']='Internal comment';

 

 // created: 2017-11-20 16:49:38
$dictionary['Case']['fields']['reason_for_not_eligible_cert_c']['inline_edit']='1';
$dictionary['Case']['fields']['reason_for_not_eligible_cert_c']['labelValue']='Reason For Not Eligible  (Certification)';

 

 // created: 2017-11-26 14:58:22
$dictionary['Case']['fields']['type']['len']=100;
$dictionary['Case']['fields']['type']['audited']=true;
$dictionary['Case']['fields']['type']['inline_edit']='';
$dictionary['Case']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Case']['fields']['type']['merge_filter']='disabled';
$dictionary['Case']['fields']['type']['required']=true;

 

 // created: 2017-10-19 17:51:36
$dictionary['Case']['fields']['workplan_remarks_c']['inline_edit']='1';
$dictionary['Case']['fields']['workplan_remarks_c']['labelValue']='Workplan remarks';

 

 // created: 2017-10-04 13:33:57
$dictionary['Case']['fields']['parent_case_c']['inline_edit']='1';
$dictionary['Case']['fields']['parent_case_c']['labelValue']='Parent Case';

 

// created: 2017-09-15 13:18:31
$dictionary["Case"]["fields"]["cases_cases_3"] = array (
  'name' => 'cases_cases_3',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
  'id_name' => 'cases_cases_3cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_3"] = array (
  'name' => 'cases_cases_3',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_R_TITLE',
  'id_name' => 'cases_cases_3cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_3_name"] = array (
  'name' => 'cases_cases_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_R_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_3cases_ida',
  'link' => 'cases_cases_3',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_3_name"] = array (
  'name' => 'cases_cases_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_3cases_ida',
  'link' => 'cases_cases_3',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_3cases_ida"] = array (
  'name' => 'cases_cases_3cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_R_TITLE',
);
$dictionary["Case"]["fields"]["cases_cases_3cases_ida"] = array (
  'name' => 'cases_cases_3cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
);


 // created: 2017-11-16 11:39:29
$dictionary['Case']['fields']['symptom_c']['inline_edit']='1';
$dictionary['Case']['fields']['symptom_c']['labelValue']='Category';

 

 // created: 2017-09-12 17:44:52
$dictionary['Case']['fields']['whole_shipping_address_c']['inline_edit']='1';
$dictionary['Case']['fields']['whole_shipping_address_c']['labelValue']='Whole Shipping Address';

 

 // created: 2017-11-17 18:05:22
$dictionary['Case']['fields']['tier_2_assigneee_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_2_assigneee_email_c']['labelValue']='tier 2 assigneee email address for WF';

 

 // created: 2017-11-28 10:15:18
$dictionary['Case']['fields']['region_c']['inline_edit']='1';
$dictionary['Case']['fields']['region_c']['labelValue']='Region';

 

 // created: 2017-09-28 16:01:07
$dictionary['Case']['fields']['tier_1_germany_start_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_1_germany_start_c']['labelValue']='Tier 1 Germany start timestamp';

 

 // created: 2017-08-29 18:19:53
$dictionary['Case']['fields']['site_contact_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_contact_email_c']['labelValue']='Site Contact Email';

 

 // created: 2017-10-24 13:44:57
$dictionary['Case']['fields']['date_time_battery_escalation_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_time_battery_escalation_c']['options']='date_range_search_dom';
$dictionary['Case']['fields']['date_time_battery_escalation_c']['labelValue']='Date Time Battery escalation canceled';
$dictionary['Case']['fields']['date_time_battery_escalation_c']['enable_range_search']='1';

 

 // created: 2017-11-15 15:42:53
$dictionary['Case']['fields']['rma_state_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_state_c']['labelValue']='RMA State';

 

 // created: 2017-10-19 16:33:58
$dictionary['Case']['fields']['product_group_c']['inline_edit']='1';
$dictionary['Case']['fields']['product_group_c']['labelValue']='Product Group';

 

 // created: 2017-10-19 18:11:08
$dictionary['Case']['fields']['tier_2_region_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_2_region_c']['labelValue']='Tier 2 region';

 

 // created: 2017-08-29 17:05:10
$dictionary['Case']['fields']['warrant_extension_c']['inline_edit']='1';
$dictionary['Case']['fields']['warrant_extension_c']['labelValue']='Warrant Extension?';

 

 // created: 2017-11-22 13:40:28
$dictionary['Case']['fields']['doa_creation_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['doa_creation_date_c']['labelValue']='DOA creation date';

 

 // created: 2017-10-19 18:27:26
$dictionary['Case']['fields']['ack_service_instruction_c']['inline_edit']='1';
$dictionary['Case']['fields']['ack_service_instruction_c']['labelValue']='ACK service instruction';

 

 // created: 2017-08-30 19:25:19
$dictionary['Case']['fields']['certified_installer_tesla_c']['inline_edit']='1';
$dictionary['Case']['fields']['certified_installer_tesla_c']['labelValue']='Certified Installer (Tesla)';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['listview_envelope'] = array(
	'name' => 'listview_envelope',
	'label' => 'LBL_LISTVIEW_ENVELOPE',
	'vname' => 'LBL_LISTVIEW_ENVELOPE',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

$dictionary['Case']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-09-28 16:00:04
$dictionary['Case']['fields']['date_modified']['display_default']='now&12:00am';
$dictionary['Case']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Case']['fields']['date_modified']['merge_filter']='disabled';

 

 // created: 2017-10-19 16:33:10
$dictionary['Case']['fields']['se_battery_support_c']['inline_edit']='1';
$dictionary['Case']['fields']['se_battery_support_c']['labelValue']='Battery Service Package Type';

 

 // created: 2017-11-08 12:11:18
$dictionary['Case']['fields']['region_for_email_template_c']['inline_edit']='1';
$dictionary['Case']['fields']['region_for_email_template_c']['labelValue']='region for email template';

 

 // created: 2017-09-05 17:21:07
$dictionary['Case']['fields']['receiving_tracking_link_c']['inline_edit']='1';
$dictionary['Case']['fields']['receiving_tracking_link_c']['labelValue']='Receiving Tracking Link';

 

 // created: 2017-10-19 16:32:49
$dictionary['Case']['fields']['battery_buyer_id_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_buyer_id_c']['labelValue']='Battery Buyer ID';

 

 // created: 2017-10-19 18:27:02
$dictionary['Case']['fields']['rma_pending_approval_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_pending_approval_c']['labelValue']='RMA Pending Approval';

 

 // created: 2017-11-10 14:45:06
$dictionary['Case']['fields']['inverter_serial_c']['inline_edit']='1';
$dictionary['Case']['fields']['inverter_serial_c']['labelValue']='Inverter Serial (Tech. Issues Mandatory)';

 

 // created: 2017-09-28 16:01:45
$dictionary['Case']['fields']['tier_2_escalation_completed_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_2_escalation_completed_c']['labelValue']='Tier 2 escalation completed';

 

 // created: 2017-11-06 11:47:46
$dictionary['Case']['fields']['sub_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['sub_type_c']['labelValue']='Sub Type';

 

 // created: 2017-08-29 20:17:31
$dictionary['Case']['fields']['special_service_instruction_c']['inline_edit']='1';
$dictionary['Case']['fields']['special_service_instruction_c']['labelValue']='Special Service Instruction';

 

 // created: 2017-09-18 14:46:01
$dictionary['Case']['fields']['receiving_forwarder_url_c']['inline_edit']='1';
$dictionary['Case']['fields']['receiving_forwarder_url_c']['labelValue']='Receiving Forwarder URL';

 

 // created: 2017-10-19 16:32:33
$dictionary['Case']['fields']['battery_distributor_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_distributor_number_c']['labelValue']='Battery Distributor Number';

 

 // created: 2017-08-30 19:21:28
$dictionary['Case']['fields']['field_service_kit_holder_c']['inline_edit']='1';
$dictionary['Case']['fields']['field_service_kit_holder_c']['labelValue']='Field Service Kit Holder';

 

 // created: 2017-08-23 12:33:37
$dictionary['Case']['fields']['escalation_to_tesla_c']['inline_edit']='1';
$dictionary['Case']['fields']['escalation_to_tesla_c']['labelValue']='Escalation to Battery Supplier';

 

 // created: 2017-10-19 18:09:42
$dictionary['Case']['fields']['monitored_c']['inline_edit']='1';
$dictionary['Case']['fields']['monitored_c']['labelValue']='Monitored';

 

 // created: 2017-09-28 16:01:58
$dictionary['Case']['fields']['time_of_visit_c']['inline_edit']='1';
$dictionary['Case']['fields']['time_of_visit_c']['labelValue']='Time of Visit';

 

 // created: 2017-09-28 16:02:20
$dictionary['Case']['fields']['date_taken_from_tier_2_us_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_taken_from_tier_2_us_c']['labelValue']='Date Taken from Tier 2 US';

 

 // created: 2017-09-12 18:04:16
$dictionary['Case']['fields']['traceability_c']['inline_edit']='1';
$dictionary['Case']['fields']['traceability_c']['labelValue']='Traceability';

 

 // created: 2017-11-20 11:51:48
$dictionary['Case']['fields']['immediate_escalation_to_c']['inline_edit']='1';
$dictionary['Case']['fields']['immediate_escalation_to_c']['labelValue']='Immediate Escalation To';

 

 // created: 2017-09-28 15:58:18
$dictionary['Case']['fields']['date_entered']['display_default']='now&12:00am';
$dictionary['Case']['fields']['date_entered']['comments']='Date record created';
$dictionary['Case']['fields']['date_entered']['merge_filter']='disabled';

 

 // created: 2017-08-29 20:25:38
$dictionary['Case']['fields']['tracking_number_f_c']['inline_edit']='1';
$dictionary['Case']['fields']['tracking_number_f_c']['labelValue']='Tracking Number';

 

 // created: 2017-10-19 16:32:14
$dictionary['Case']['fields']['handling_time_eu_assignee_c']['inline_edit']='1';
$dictionary['Case']['fields']['handling_time_eu_assignee_c']['labelValue']='Handling time EU Assignee';

 

 // created: 2017-10-19 16:31:54
$dictionary['Case']['fields']['site_contact_phone_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_contact_phone_c']['labelValue']='Site Contact Phone';

 

 // created: 2017-09-07 14:42:44
$dictionary['Case']['fields']['axiplus_indicator_c']['inline_edit']='1';
$dictionary['Case']['fields']['axiplus_indicator_c']['labelValue']='Axiplus Indicator';

 

 // created: 2017-10-19 16:31:33
$dictionary['Case']['fields']['site_contact_person_name_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_contact_person_name_c']['labelValue']='RMA Contact Name';

 


$dictionary["Case"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);



if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['dtbc_2days_noti_sent_c'] = array(
	'required' => false,
	'name' => 'dtbc_2days_noti_sent_c',
	'vname' => 'LBL_HIDDEN_2DAYS',
	'type' => 'bool',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['dtbc_3days_noti_sent_c'] = array(
	'required' => false,
	'name' => 'dtbc_3days_noti_sent_c',
	'vname' => 'LBL_HIDDEN_3DAYS',
	'type' => 'bool',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['dtbc_5days_noti_sent_c'] = array(
	'required' => false,
	'name' => 'dtbc_5days_noti_sent_c',
	'vname' => 'LBL_HIDDEN_5DAYS',
	'type' => 'bool',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);

$dictionary['Case']['fields']['dtbc_agent_status_noti_sent_c'] = array(
	'required' => false,
	'name' => 'dtbc_agent_status_noti_sent_c',
	'vname' => 'LBL_HIDDEN_AFTERSAVE',
	'type' => 'bool',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '255',
	'size' => '20',
);



 // created: 2017-11-22 17:08:21
$dictionary['Case']['fields']['rca_and_need_imm_ret_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['rca_and_need_imm_ret_email_c']['labelValue']='RCA and need imm ret email';

 

 // created: 2017-09-08 11:42:30
$dictionary['Case']['fields']['link_to_ups_c']['inline_edit']='1';
$dictionary['Case']['fields']['link_to_ups_c']['labelValue']='Link to UPS';

 

// created: 2017-08-29 18:26:46
$dictionary["Case"]["fields"]["cases_cases_1"] = array (
  'name' => 'cases_cases_1',
  'type' => 'link',
  'relationship' => 'cases_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_1_FROM_CASES_L_TITLE',
  'id_name' => 'cases_cases_1cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_1_name"] = array (
  'name' => 'cases_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_1_FROM_CASES_L_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_1cases_ida',
  'link' => 'cases_cases_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_1cases_ida"] = array (
  'name' => 'cases_cases_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_CASES_1_FROM_CASES_R_TITLE',
);


 // created: 2017-10-19 17:50:09
$dictionary['Case']['fields']['details_for_fault_sub_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['details_for_fault_sub_type_c']['labelValue']='Details for Fault sub-type - Other';

 

 // created: 2017-10-19 18:26:40
$dictionary['Case']['fields']['video_chat_c']['inline_edit']='1';
$dictionary['Case']['fields']['video_chat_c']['labelValue']='Video Chat';

 

 // created: 2017-11-19 14:25:35
$dictionary['Case']['fields']['new_case_notification_sent_c']['inline_edit']='1';
$dictionary['Case']['fields']['new_case_notification_sent_c']['labelValue']='New Case Notification Sent';

 

 // created: 2017-10-19 16:26:16
$dictionary['Case']['fields']['return_label_pickup_c']['inline_edit']='1';
$dictionary['Case']['fields']['return_label_pickup_c']['labelValue']='Return Label Pickup';

 

 // created: 2017-09-08 11:43:40
$dictionary['Case']['fields']['service_instruction_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['service_instruction_flag_c']['labelValue']='Service Instruction flag';

 

 // created: 2017-10-19 16:26:33
$dictionary['Case']['fields']['case_zip_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_zip_c']['labelValue']='Zip';

 

 // created: 2017-09-28 15:56:26
$dictionary['Case']['fields']['sub_status_updated_c']['inline_edit']='1';
$dictionary['Case']['fields']['sub_status_updated_c']['labelValue']='Sub status updated';

 


if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$dictionary["Case"]["fields"]["today_c"] = array(
	'name' => 'today_c',
	'label' => 'LBL_CASE_TODAY',
	'vname' => 'LBL_CASE_TODAY',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getToday',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

$dictionary["Case"]["fields"]["case_age_c"] = array(
	'name' => 'case_age_c',
	'label' => 'LBL_CASE_AGE',
	'vname' => 'LBL_CASE_AGE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getCaseAge',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

$dictionary["Case"]["fields"]["support_team_for_template_c"] = array(
	'name' => 'support_team_for_template_c',
	'label' => 'LBL_CASE_SUPPORT_TEAM',
	'vname' => 'LBL_CASE_SUPPORT_TEAM',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getSupportTeamForTemplate',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

$dictionary["Case"]["fields"]["last_modified_by_c"] = array(
	'name' => 'last_modified_by_c',
	'label' => 'LBL_CASE_LAST_MODIFIED_BY',
	'vname' => 'LBL_CASE_LAST_MODIFIED_BY',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'getLastModifiedBy',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_template_variables.php',
	),
);

 // created: 2017-10-19 16:25:46
$dictionary['Case']['fields']['case_city_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_city_c']['labelValue']='City';

 

 // created: 2017-11-08 18:21:28
$dictionary['Case']['fields']['solution_statement__c']['inline_edit']='1';
$dictionary['Case']['fields']['solution_statement__c']['labelValue']='solution statement';

 

 // created: 2017-09-18 19:37:38
$dictionary['Case']['fields']['solaredge_service_kit_holder_c']['inline_edit']='1';
$dictionary['Case']['fields']['solaredge_service_kit_holder_c']['labelValue']='Solaredge Service Kit Holder';

 

 // created: 2017-11-28 02:03:05
$dictionary['Case']['fields']['error_event_code_c']['inline_edit']='1';
$dictionary['Case']['fields']['error_event_code_c']['labelValue']='Fault Description';

 

 // created: 2017-10-19 16:24:34
$dictionary['Case']['fields']['battery_installation_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_installation_date_c']['labelValue']='Battery Installation Date';

 

 // created: 2017-08-22 18:00:22
$dictionary['Case']['fields']['rca_c']['inline_edit']='1';
$dictionary['Case']['fields']['rca_c']['labelValue']='RCA?';

 

 // created: 2017-10-19 17:58:31
$dictionary['Case']['fields']['case_country_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_country_c']['labelValue']='Country';

 

 // created: 2017-08-29 16:56:25
$dictionary['Case']['fields']['sunpower_partner_c']['inline_edit']='1';
$dictionary['Case']['fields']['sunpower_partner_c']['labelValue']='SunPower-Partner';

 

 // created: 2017-09-28 16:02:34
$dictionary['Case']['fields']['in_process_timestamp_eu_c']['inline_edit']='1';
$dictionary['Case']['fields']['in_process_timestamp_eu_c']['labelValue']='In Process timestamp EU';

 

 // created: 2017-09-18 14:48:59
$dictionary['Case']['fields']['forwarder_url_c']['inline_edit']='1';
$dictionary['Case']['fields']['forwarder_url_c']['labelValue']='Shipping Forwarder URL';

 

 // created: 2017-09-29 14:48:38
$dictionary['Case']['fields']['contact_email_c']['inline_edit']='1';
$dictionary['Case']['fields']['contact_email_c']['labelValue']='Contact Email';

 

 // created: 2017-10-19 16:31:03
$dictionary['Case']['fields']['rma_city_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_city_c']['labelValue']='RMA City';

 

 // created: 2017-08-29 17:07:31
$dictionary['Case']['fields']['as_part_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['as_part_number_c']['labelValue']='AS Part Number';

 

 // created: 2017-10-19 18:26:03
$dictionary['Case']['fields']['in_process_timestamp_eu_step_c']['inline_edit']='1';
$dictionary['Case']['fields']['in_process_timestamp_eu_step_c']['labelValue']='In Process timestamp EU step 1';

 

 // created: 2017-10-19 18:25:45
$dictionary['Case']['fields']['digital_board_was_replaced_c']['inline_edit']='1';
$dictionary['Case']['fields']['digital_board_was_replaced_c']['labelValue']='Digital board was replaced?';

 

 // created: 2017-09-28 14:38:19
$dictionary['Case']['fields']['alternative_ibolt_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['alternative_ibolt_status_c']['labelValue']='Alternative Ibolt Status';

 

 // created: 2017-09-08 17:20:37
$dictionary['Case']['fields']['valid_for_service_indication_c']['inline_edit']='1';
$dictionary['Case']['fields']['valid_for_service_indication_c']['labelValue']='Inverter Validation';

 

 // created: 2017-08-29 20:24:21
$dictionary['Case']['fields']['thread_id_c']['inline_edit']='1';
$dictionary['Case']['fields']['thread_id_c']['labelValue']='Thread ID';

 

 // created: 2017-11-22 18:41:19
$dictionary['Case']['fields']['rma_lines_in_case_c']['inline_edit']='';
$dictionary['Case']['fields']['rma_lines_in_case_c']['labelValue']='rma lines in case';

 

 // created: 2017-09-15 18:16:31
$dictionary['Case']['fields']['link_to_tesla_rma_c']['inline_edit']='1';
$dictionary['Case']['fields']['link_to_tesla_rma_c']['labelValue']='Link To Tesla RMA';

 

 // created: 2017-10-19 18:25:24
$dictionary['Case']['fields']['jira_fa_c']['inline_edit']='1';
$dictionary['Case']['fields']['jira_fa_c']['labelValue']='JIRA FA';

 

 // created: 2017-10-24 13:52:38
$dictionary['Case']['fields']['tesla_lg_escalation_timestam_c']['inline_edit']='1';
$dictionary['Case']['fields']['tesla_lg_escalation_timestam_c']['options']='date_range_search_dom';
$dictionary['Case']['fields']['tesla_lg_escalation_timestam_c']['labelValue']='Tesla/LG escalation timestamp';
$dictionary['Case']['fields']['tesla_lg_escalation_timestam_c']['enable_range_search']='1';

 

 // created: 2017-11-26 14:42:33
$dictionary['Case']['fields']['state']['inline_edit']=true;
$dictionary['Case']['fields']['state']['comments']='The state of the case (i.e. open/closed)';

 

 // created: 2017-08-30 18:14:11
$dictionary['Case']['fields']['usa_county_c']['inline_edit']='1';
$dictionary['Case']['fields']['usa_county_c']['labelValue']='County';

 

 // created: 2017-10-25 15:49:41
$dictionary['Case']['fields']['tracking_number_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['tracking_number_date_c']['labelValue']='Tracking number date';

 

 // created: 2017-12-01 19:05:24
$dictionary['Case']['fields']['tier_2_assignee_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_2_assignee_c']['labelValue']='Tier 2 Assignee';

 

 // created: 2017-10-19 18:18:23
$dictionary['Case']['fields']['error_code_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['error_code_number_c']['labelValue']='Error Code Number';

 

 // created: 2017-08-29 20:12:03
$dictionary['Case']['fields']['site_country_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_country_c']['labelValue']='Site Country';

 

 // created: 2017-08-30 19:26:40
$dictionary['Case']['fields']['solarcity_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['solarcity_flag_c']['labelValue']='SolarCity Flag';

 

 // created: 2017-10-19 17:56:54
$dictionary['Case']['fields']['reason_for_additional_rma_c']['inline_edit']='1';
$dictionary['Case']['fields']['reason_for_additional_rma_c']['labelValue']='Reason for additional RMA';

 

 // created: 2017-10-19 18:25:02
$dictionary['Case']['fields']['replenishment_c']['inline_edit']='1';
$dictionary['Case']['fields']['replenishment_c']['labelValue']='Replenishment';

 

 // created: 2017-10-19 17:56:23
$dictionary['Case']['fields']['installation_rules_c']['inline_edit']='1';
$dictionary['Case']['fields']['installation_rules_c']['labelValue']='Site Design According to SE Guidelines?';

 

 // created: 2017-11-10 14:30:39
$dictionary['Case']['fields']['power_c']['inline_edit']='1';
$dictionary['Case']['fields']['power_c']['labelValue']='power';

 

 // created: 2017-08-22 18:11:53
$dictionary['Case']['fields']['receiving_forwarder_c']['inline_edit']='1';
$dictionary['Case']['fields']['receiving_forwarder_c']['labelValue']='Receiving Forwarder';

 

 // created: 2017-09-12 13:56:52
$dictionary['Case']['fields']['part_description_c']['inline_edit']='1';
$dictionary['Case']['fields']['part_description_c']['labelValue']='Part Description';

 

 // created: 2017-10-19 18:24:35
$dictionary['Case']['fields']['do_not_send_survey_c']['inline_edit']='1';
$dictionary['Case']['fields']['do_not_send_survey_c']['labelValue']='Do Not Send Survey';

 

 // created: 2017-08-29 17:06:35
$dictionary['Case']['fields']['peak_power_kw_c']['inline_edit']='1';
$dictionary['Case']['fields']['peak_power_kw_c']['labelValue']='Peak Power KW H Peak';

 

 // created: 2017-10-19 16:25:00
$dictionary['Case']['fields']['rma_zip_postal_code_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_zip_postal_code_c']['labelValue']='RMA Zip\\\\Postal Code';

 

 // created: 2017-10-19 18:24:17
$dictionary['Case']['fields']['taken_from_tier_2_us_c']['inline_edit']='1';
$dictionary['Case']['fields']['taken_from_tier_2_us_c']['labelValue']='Taken from Tier 2 US';

 

 // created: 2017-10-19 16:24:13
$dictionary['Case']['fields']['customer_case_c']['inline_edit']='1';
$dictionary['Case']['fields']['customer_case_c']['labelValue']='Customer case#';

 

 // created: 2017-11-10 13:37:51
$dictionary['Case']['fields']['actions_c']['inline_edit']='1';
$dictionary['Case']['fields']['actions_c']['labelValue']='Actions';

 

 // created: 2017-10-25 19:13:58
$dictionary['Case']['fields']['alliance_case_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['alliance_case_status_c']['labelValue']='Alliance Case Status';

 

 // created: 2017-11-03 17:59:16
$dictionary['Case']['fields']['case_address_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_address_c']['labelValue']='Address';

 

 // created: 2017-08-29 20:23:20
$dictionary['Case']['fields']['state_zip_c']['inline_edit']='1';
$dictionary['Case']['fields']['state_zip_c']['labelValue']='State/Country';

 

 // created: 2017-11-08 12:30:38
$dictionary['Case']['fields']['rma_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_type_c']['labelValue']='RMA Type';

 

 // created: 2017-11-01 14:52:14
$dictionary['Case']['fields']['customer_should_go_to_the_c']['inline_edit']='1';
$dictionary['Case']['fields']['customer_should_go_to_the_c']['labelValue']='Customer should go to the site';

 

 // created: 2017-11-10 15:00:58
$dictionary['Case']['fields']['shipping_tracking_number_2_c']['inline_edit']='1';
$dictionary['Case']['fields']['shipping_tracking_number_2_c']['labelValue']='Shipping Tracking Number 2';

 

 // created: 2017-10-19 18:16:55
$dictionary['Case']['fields']['rma_total_time_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_total_time_c']['labelValue']='RMA Total time';

 

 // created: 2017-10-22 16:35:50
$dictionary['Case']['fields']['field_engineer_name_c']['inline_edit']='1';
$dictionary['Case']['fields']['field_engineer_name_c']['labelValue']='Field Engineer Name';

 

 // created: 2017-09-12 11:25:28
$dictionary['Case']['fields']['ibolt_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['ibolt_status_c']['labelValue']='Validation Check';

 

 // created: 2017-08-30 18:13:26
$dictionary['Case']['fields']['canadian_province_c']['inline_edit']='1';
$dictionary['Case']['fields']['canadian_province_c']['labelValue']='Canadian Province';

 

 // created: 2017-10-19 18:23:53
$dictionary['Case']['fields']['taken_from_tier_1_us_c']['inline_edit']='1';
$dictionary['Case']['fields']['taken_from_tier_1_us_c']['labelValue']='Taken from Tier 1 US';

 

 // created: 2017-10-19 18:23:29
$dictionary['Case']['fields']['need_pre_configuration_c']['inline_edit']='1';
$dictionary['Case']['fields']['need_pre_configuration_c']['labelValue']='Need Pre-Configuration?';

 

 // created: 2017-08-29 17:25:08
$dictionary['Case']['fields']['internal_notes_c']['inline_edit']='1';
$dictionary['Case']['fields']['internal_notes_c']['labelValue']='Internal notes';

 

 // created: 2017-11-22 18:36:57
$dictionary['Case']['fields']['countif_portia_rs485_c']['inline_edit']='';
$dictionary['Case']['fields']['countif_portia_rs485_c']['labelValue']='countif portia rs485';

 

 // created: 2017-11-10 14:35:13
$dictionary['Case']['fields']['battery_part_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_part_number_c']['labelValue']='Battery Part Number';

 

 // created: 2017-10-19 18:23:06
$dictionary['Case']['fields']['compensation_approved_c']['inline_edit']='1';
$dictionary['Case']['fields']['compensation_approved_c']['labelValue']='Compensation approved';

 

 // created: 2017-10-19 18:22:44
$dictionary['Case']['fields']['tier_1_germany_start_timesta_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_1_germany_start_timesta_c']['labelValue']='Tier 1 Germany start timestamp step 1';

 

// created: 2017-08-31 12:51:23
$dictionary["Case"]["fields"]["s1_site_cases"] = array (
  'name' => 's1_site_cases',
  'type' => 'link',
  'relationship' => 's1_site_cases',
  'source' => 'non-db',
  'module' => 'S1_Site',
  'bean_name' => 'S1_Site',
  'vname' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
  'id_name' => 's1_site_casess1_site_ida',
);
$dictionary["Case"]["fields"]["s1_site_cases_name"] = array (
  'name' => 's1_site_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
  'save' => true,
  'id_name' => 's1_site_casess1_site_ida',
  'link' => 's1_site_cases',
  'table' => 's1_site',
  'module' => 'S1_Site',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["s1_site_casess1_site_ida"] = array (
  'name' => 's1_site_casess1_site_ida',
  'type' => 'link',
  'relationship' => 's1_site_cases',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_CASES_FROM_CASES_TITLE',
);


 // created: 2017-09-29 15:59:41
$dictionary['Case']['fields']['testfield2_c']['inline_edit']='1';
$dictionary['Case']['fields']['testfield2_c']['labelValue']='testfield2';

 

 // created: 2017-11-10 14:41:04
$dictionary['Case']['fields']['part_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['part_number_c']['labelValue']='Part Number';

 

 // created: 2017-08-29 20:12:46
$dictionary['Case']['fields']['site_state_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_state_c']['labelValue']='Site State';

 

 // created: 2017-10-19 17:51:06
$dictionary['Case']['fields']['rma_comments_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_comments_c']['labelValue']='RMA Comments';

 

 // created: 2017-08-22 18:00:07
$dictionary['Case']['fields']['rca_location_c']['inline_edit']='1';
$dictionary['Case']['fields']['rca_location_c']['labelValue']='RCA location';

 

 // created: 2017-10-23 15:19:42
$dictionary['Case']['fields']['case_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['case_status_c']['labelValue']='Status';

 

 // created: 2017-10-19 18:21:49
$dictionary['Case']['fields']['get_address_confirmation_c']['inline_edit']='1';
$dictionary['Case']['fields']['get_address_confirmation_c']['labelValue']='Get address confirmation';

 

 // created: 2017-08-30 19:25:04
$dictionary['Case']['fields']['contact_support_comments_c']['inline_edit']='1';
$dictionary['Case']['fields']['contact_support_comments_c']['labelValue']='Contact support comments';

 

 // created: 2017-11-07 11:10:32
$dictionary['Case']['fields']['change_indicator_c']['inline_edit']='';
$dictionary['Case']['fields']['change_indicator_c']['labelValue']='Change Indicator';

 

 // created: 2017-10-19 16:23:53
$dictionary['Case']['fields']['job_number_us_only_c']['inline_edit']='1';
$dictionary['Case']['fields']['job_number_us_only_c']['labelValue']='Job Number (US only)';

 

 // created: 2017-10-24 15:49:06
$dictionary['Case']['fields']['battery_ibolt_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_ibolt_status_c']['labelValue']='Battery iBolt Status';

 

 // created: 2017-09-08 11:39:48
$dictionary['Case']['fields']['action_log_c']['inline_edit']='1';
$dictionary['Case']['fields']['action_log_c']['labelValue']='Action log';

 

 // created: 2017-09-28 16:02:48
$dictionary['Case']['fields']['waiting_for_tier_2_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['waiting_for_tier_2_timestamp_c']['labelValue']='Waiting for Tier 2 timestamp';

 

 // created: 2017-11-02 20:05:11
$dictionary['Case']['fields']['record_type_is_rma_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['record_type_is_rma_timestamp_c']['labelValue']='record type is rma timestamp';

 

 // created: 2017-08-30 19:25:37
$dictionary['Case']['fields']['place_name_canada_only_c']['inline_edit']='1';
$dictionary['Case']['fields']['place_name_canada_only_c']['labelValue']='Place Name (Canada only)';

 

 // created: 2017-08-29 19:02:11
$dictionary['Case']['fields']['rma_yes_no_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_yes_no_c']['labelValue']='RMA (Yes/No)';

 

 // created: 2017-08-29 20:09:01
$dictionary['Case']['fields']['service_level_c']['inline_edit']='1';
$dictionary['Case']['fields']['service_level_c']['labelValue']='Service Level';

 

 // created: 2017-08-29 17:54:29
$dictionary['Case']['fields']['public_modules_c']['inline_edit']='1';
$dictionary['Case']['fields']['public_modules_c']['labelValue']='Public Modules';

 

 // created: 2017-10-19 18:21:25
$dictionary['Case']['fields']['compensation_exceptional_c']['inline_edit']='1';
$dictionary['Case']['fields']['compensation_exceptional_c']['labelValue']='Compensation exceptional';

 

 // created: 2017-09-08 17:27:03
$dictionary['Case']['fields']['priority_indicator_c']['inline_edit']='1';
$dictionary['Case']['fields']['priority_indicator_c']['labelValue']='Priority Indicator';

 

 // created: 2017-10-18 12:00:15
$dictionary['Case']['fields']['tier_3_escalation_completed_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier_3_escalation_completed_c']['labelValue']='Tier 3 escalation completed';

 

 // created: 2017-08-23 12:36:05
$dictionary['Case']['fields']['send_to_priority_c']['inline_edit']='1';
$dictionary['Case']['fields']['send_to_priority_c']['labelValue']='Send to ERP';

 

 // created: 2017-10-19 17:54:36
$dictionary['Case']['fields']['created_by_address_c']['inline_edit']='1';
$dictionary['Case']['fields']['created_by_address_c']['labelValue']='Created By Address';

 

 // created: 2017-10-28 02:17:23
$dictionary['Case']['fields']['shipping_tracking_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['shipping_tracking_number_c']['labelValue']='Shipping Tracking Number';

 

 // created: 2017-09-28 15:57:03
$dictionary['Case']['fields']['date_rma_approval_c']['inline_edit']='1';
$dictionary['Case']['fields']['date_rma_approval_c']['labelValue']='Date RMA approval';

 

 // created: 2017-10-19 16:03:46
$dictionary['Case']['fields']['battery_tesla_authorized_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_tesla_authorized_c']['labelValue']='Battery Tesla Authorized Reseller ID';

 

 // created: 2017-10-19 18:21:05
$dictionary['Case']['fields']['send_zach_vm_notification_c']['inline_edit']='1';
$dictionary['Case']['fields']['send_zach_vm_notification_c']['labelValue']='send zach VM notification mail wf';

 

 // created: 2017-10-24 15:46:20
$dictionary['Case']['fields']['battery_end_warranty_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['battery_end_warranty_date_c']['labelValue']='Battery End Warranty Date';

 

 // created: 2017-08-29 17:08:47
$dictionary['Case']['fields']['dsp1_c']['inline_edit']='1';
$dictionary['Case']['fields']['dsp1_c']['labelValue']='DSP1';

 

 // created: 2017-10-19 16:23:33
$dictionary['Case']['fields']['family_c']['inline_edit']='1';
$dictionary['Case']['fields']['family_c']['labelValue']='Family';

 

 // created: 2017-10-19 16:03:15
$dictionary['Case']['fields']['warranty_extention_c']['inline_edit']='1';
$dictionary['Case']['fields']['warranty_extention_c']['labelValue']='Warranty Extention';

 

 // created: 2017-10-19 17:54:42
$dictionary['Case']['fields']['reason_for_not_approved_list_c']['inline_edit']='1';
$dictionary['Case']['fields']['reason_for_not_approved_list_c']['labelValue']='Reason for not approved- list';

 

 // created: 2017-11-26 15:37:02
$dictionary['Case']['fields']['tier3_escalation_owner_c']['inline_edit']='1';
$dictionary['Case']['fields']['tier3_escalation_owner_c']['labelValue']='Tier3 Escalation Owner';

 

 // created: 2017-09-28 15:57:19
$dictionary['Case']['fields']['time_stamp_rma_approved_c']['inline_edit']='1';
$dictionary['Case']['fields']['time_stamp_rma_approved_c']['labelValue']='Time Stamp RMA Approved';

 

 // created: 2017-08-29 20:07:31
$dictionary['Case']['fields']['rma_pr_number_c']['inline_edit']='1';
$dictionary['Case']['fields']['rma_pr_number_c']['labelValue']='RMA PR Number';

 

 // created: 2017-08-29 20:11:43
$dictionary['Case']['fields']['site_city_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_city_c']['labelValue']='Site City';

 

 // created: 2017-08-29 18:42:04
$dictionary['Case']['fields']['reason_for_delay_c']['inline_edit']='1';
$dictionary['Case']['fields']['reason_for_delay_c']['labelValue']='Reason for Delay';

 

 // created: 2017-10-16 14:44:53
$dictionary['Case']['fields']['grid_c']['inline_edit']='1';
$dictionary['Case']['fields']['grid_c']['labelValue']='Grid';

 

 // created: 2017-11-28 02:52:40
$dictionary['Case']['fields']['valid_c']['inline_edit']='1';
$dictionary['Case']['fields']['valid_c']['labelValue']='Valid For Service?';

 

 // created: 2017-11-10 14:32:32
$dictionary['Case']['fields']['guidelines_deviation_reason_c']['inline_edit']='1';
$dictionary['Case']['fields']['guidelines_deviation_reason_c']['labelValue']='Guidelines deviation reason';

 

 // created: 2017-11-20 15:15:40
$dictionary['Case']['fields']['escalated_to_t3_timestamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['escalated_to_t3_timestamp_c']['labelValue']='escalated to tier 3 timestamp';

 

 // created: 2017-10-25 14:42:56
$dictionary['Case']['fields']['gen_c']['inline_edit']='1';
$dictionary['Case']['fields']['gen_c']['labelValue']='Gen';

 

 // created: 2017-11-27 19:45:40
$dictionary['Case']['fields']['replacement_type_c']['inline_edit']='1';
$dictionary['Case']['fields']['replacement_type_c']['labelValue']='Replacement type';

 

 // created: 2017-10-27 20:42:42
$dictionary['Case']['fields']['country_of_contact_c']['inline_edit']='1';
$dictionary['Case']['fields']['country_of_contact_c']['labelValue']='Country Of Contact';

 

 // created: 2017-10-19 16:00:16
$dictionary['Case']['fields']['site_contact_name_c']['inline_edit']='1';
$dictionary['Case']['fields']['site_contact_name_c']['labelValue']='Site Contact Name';

 

 // created: 2017-10-28 02:00:59
$dictionary['Case']['fields']['faulty_inverter_c']['inline_edit']='1';
$dictionary['Case']['fields']['faulty_inverter_c']['labelValue']='Faulty Inverter';

 

 // created: 2017-10-19 18:20:44
$dictionary['Case']['fields']['confirm_contact_address_c']['inline_edit']='1';
$dictionary['Case']['fields']['confirm_contact_address_c']['labelValue']='Confirm Contact Address';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['cases_cases_1']['rname'] = 'case_number';

$dictionary["Case"]["fields"]["cases_cases_1_name"]['rname'] = 'case_number';
?>