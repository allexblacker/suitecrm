function dtbc_getSelectedIds() {
	var chkBoxes = $('[id^=checkrma_]');
	var retval = [];
	for (var i = 0; i < chkBoxes.length; i++) {
		if (chkBoxes[i].checked == true)
			retval.push(chkBoxes[i].id.substring(9));
	}
	return retval;
}

function checkRma() {
	$.ajax({
		method: "POST",
		url: "index.php?module=Cases&action=checkRma&to_pdf=1",
		data: { 
			selectedRecords: dtbc_getSelectedIds(),
			record: $('[name=acase_id]').val(),
		},
	})
	.done(function (response) {
		showSubPanel('cases_sn1_serial_number_1', null, true);
	})
	.fail(function (e) {
		console.log(e);
	}); 
}

