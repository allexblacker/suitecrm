var dtbc_changedFields = new Array();
var dtbc_originalSiteId = "";

// https://cs86.salesforce.com/_ui/common/config/entity/ValidationFormulaListUI/d?retURL=%2F03dD00000006BjB%3Fsetupid%3DCaseValidations&tableEnumOrId=Case&setupid=CaseValidations
// https://gunnicom.wordpress.com/2015/09/21/suitecrm-sugarcrm-6-5-add-custom-javascript-field-validation/

function Account_and_Contact_mandatory(formName) {
	
	if ($('#case_type_c').val() == '2') {
		addRequired('contact_created_by_name', formName);
		addRequired('account_name', formName);
	} else {
		removeRequired('contact_created_by_name', formName);
		removeRequired('account_name', formName);
	}
}

function Site_Mandatory_for_Technical_Issues(formName) {
	if ($('#case_type_c').length && $('#case_site_c').length && $('#s1_site_cases_name').length) {
		if ($('#case_type_c').val() == 2 && $('#case_site_c').val().length == 0 && $('#s1_site_cases_name').val().length == 0 && ($("#validation_dateCreatedBigger2").val() == 1 || isNewRecord() )) {
			add_error_style_custom(formName, 's1_site_cases_name', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_SITE_MANDATORY'));
			return false;
		}
	}

	return true;
}

function Cant_close_a_case_without_a_site_name(formname) {
	if ($('#case_status_c').length && $('#s1_site_cases_name').length && $('#case_site_c').length && $('#case_country_c').length) {
		var site = $('#s1_site_cases_name').val();
		var site2 = $('#case_site_c').val();
		var country = $('#case_country_c').val();
		
		if (isClosed() && site.length == 0 && site2.length == 0 && country == 'Unitedstates') {
			add_error_style_custom(formname, 'case_status_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_SITENAME'));
			return false;
		}
	}
	
	return true;
}

function Block_Send_to_ERP(formname) {
	if ($('#ibolt_status_c').length && $('#send_to_priority_c').length) {
		var priority = $('#send_to_priority_c').val();
		var ibolt = $('#ibolt_status_c').val();
		
		if (ibolt.toLowerCase() != 'success' && priority.toLowerCase() == 'yes') {
			add_error_style_custom(formname, 'ibolt_status_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_INVERTER'));
			return false;
		}
	}
	
	return true;
}

function Cant_Open_RMA_if_battery_not_valid_for_s(formname) {
	if ($('#battery_valid_for_service_c').length && $('#send_to_priority_c').length && $('#any_battery_serial_number_c').length && $('#valid_c').length) {
		var valid = $('#valid_c').val();
		var serial = $('#any_battery_serial_number_c').val();
		var priority = $('#send_to_priority_c').val();
		var service = $('#battery_valid_for_service_c').val();
		
		if (valid == 2 && serial.length > 0 && priority == 'Yes' && service == 'No') {
			add_error_style_custom(formname, 'battery_valid_for_service_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_BATTERY'));
			return false;
		}
	}
	
	return true;
}

function Details_for_Fault_sub_category_other_M(formname) {
	if ($('#details_for_fault_sub_type_c').length && $('#fault_category_c').length && $('#fault_sub_type_c').length && $('#error_event_code_c').length) {
		var details = $('#details_for_fault_sub_type_c').val();
		var faultc = $('#fault_category_c').val();
		var faults = $('#fault_sub_type_c').val();
		var error = $('#error_event_code_c').val();
		
		if (details.length == 0 && faultc == 19 && (faults == "Damaged goods" || faults == "Immersion heater controller" || faults == "Plug" || faults == "AC switch" || faults == "AC dry contact") && error == "Other") {
			add_error_style_custom(formname, 'details_for_fault_sub_type_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_FAULTSUB'));
			return false;
		}
	}
	
	return true;
}

function Details_for_fault_sub_type_mandatory(formname) {
	if ($('#error_event_code_c').length && $('#details_for_fault_sub_type_c').length) {
		var error = $('#error_event_code_c').val();
		var details = $('#details_for_fault_sub_type_c').val();
		
		if (error == 'Duplication' && details.length == 0) {
			add_error_style_custom(formname, 'details_for_fault_sub_type_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_FAULTSUBEMPTY'));
			return false;
		}
	}
	
	return true;
}

function error_code_number_is_mandatory(formName) {
	if ($('#sub_symptom_c').val() == '1') {
		addRequired('error_code_number_c', formName);
	} else {
		var checkFields = ($('#sub_symptom_c').val() == '13' && !isClosed());
		if (!checkFields) {
			removeRequired('error_code_number_c', formName);
		}
	}
}

function Error_Code_Number_value_1_and_9999(formname) {
	if ($('#error_code_number_c').length) {
		var error = parseInt($('#error_code_number_c').val());

		if (!isNaN(error) && (error < 1 || error > 9999)) {
			add_error_style_custom(formname, 'error_code_number_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_ERRCODEVALUE'));
			return false;
		}
	}
	
	return true;
}

function Fault_sub_category_and_Fault_description(formname) {
	if ($('#fault_sub_type_c').length && $('#error_event_code_c').length && $('#case_type_c').length && $('#case_status_c').length) {
		var fault = $('#fault_sub_type_c').val();
		var error = $('#error_event_code_c').val();
		var ctype = $('#case_type_c').val();
		var cstatus = $('#case_status_c').val();
		
		if (ctype == '2' && cstatus == '7' && 
			(fault.length == 0 || error.length == 0) &&
			(dtbc_changedFields.indexOf('fault_sub_type_c') == -1 ||
			dtbc_changedFields.indexOf('error_event_code_c') == -1 ||
			dtbc_changedFields.indexOf('case_type_c') == -1 ||
			dtbc_changedFields.indexOf('case_status_c') == -1 ||
			isNewRecord())) {
			add_error_style_custom(formname, 'error_event_code_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_FAULTVALUES'));
			return false;
		}
	}
	
	return true;
}

function If_DOA_is_YES_must_enter_the_operating_p(formName) {
	if ($('#doa_c').val() == '1') {
		addRequired('operating_period_c', formName);
		addRequired('doa_type_c', formName);
	} else {
		removeRequired('operating_period_c', formName);
		removeRequired('doa_type_c', formName);
		$('#doa_type_c').val("");
	}
}

function Inverter_Validaite_Mand_Tech_Issue_SF(formname) {
	if ($('#case_type_c').length && $('#inverter_serial_c').length) {
		var ctype = $('#case_type_c').val();
		var inverter = $('#inverter_serial_c').val();
		
		if (ctype == '2' && inverter.length == '' && 
			(isNewRecord() || $('#validation_dateCreatedBigger').val() == 1)) {
			add_error_style_custom(formname, 'inverter_serial_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_INVERTERSERIAL'));
			return false;
		}
	}
	
	return true;
}

function Inverter_Validaiton_format_check(formname) {
	if ($('#inverter_serial_c').length) {
		var inverter = $('#inverter_serial_c').val();
		
		if (inverter.length > 0 &&
			(isNewRecord() || $('#validation_origSerial').val() != inverter) &&
			(inverter.length < 10 || inverter.substr(inverter.length - 3).substr(0, 1) != "-")) {
			add_error_style_custom(formname, 'inverter_serial_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_INVERTERSERIALFORMAT'));
			return false;
		}
	}
	
	return true;
}

function Mandate_Address_fields_for_Alliance_type(formname) {
	if ($('#case_type_c').length && $('#case_country_c').length && $('#case_state_c').length && $('#case_city_c').length && $('#case_address_c').length && $('#case_zip_c').length) {
		var ctype = $('#case_type_c').val();
		var country = $('#case_country_c').val();
		var state = $('#case_state_c').val();
		var city = $('#case_city_c').val();
		var address = $('#case_address_c').val();
		var zip = $('#case_zip_c').val();
		
		if (ctype == 4 && (country.length == 0 || state.length == 0 || city.length == 0 || address.length == 0 || zip.length == 0)) {
			add_error_style_custom(formname, 'case_country_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_state_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_city_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_address_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_zip_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			return false;
		}
	}
	
	return true;
}

function Address_is_Mandatory_for_Redemption(formname) {
	if ($('#sub_type_c').length && $('#case_country_c').length && $('#case_state_c').length && $('#case_city_c').length && $('#case_address_c').length && $('#case_zip_c').length) {
		var ctype = $('#sub_type_c').val();
		var country = $('#case_country_c').val();
		var state = $('#case_state_c').val();
		var city = $('#case_city_c').val();
		var address = $('#case_address_c').val();
		var zip = $('#case_zip_c').val();
		
		if (ctype == 'Redemption_of_points' && (country.length == 0 || state.length == 0 || city.length == 0 || address.length == 0 || zip.length == 0)) {
			add_error_style_custom(formname, 'case_country_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_state_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_city_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_address_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			add_error_style_custom(formname, 'case_zip_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CASEADDRESS'));
			return false;
		}
	}
	
	return true;
}

function No_PO_box(formname) {
	if ($('#rma_street_c').length && $('#rma_country_c').length) {
		var street = $('#rma_street_c').val().toLowerCase().trim();
		var rmaCountry = $('#rma_country_c').val()
		
		if ((street.indexOf("p.o. box") !== -1 || street.indexOf("po box") !== -1 || street.indexOf("p.o box") !== -1) && rmaCountry != 'Israel') {
			add_error_style_custom(formname, 'rma_street_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_RMASTREET'));
			return false;
		}
	}
	
	return true;
}

function other_fault_sub_type(formname) {
	/*
	if ($('#fault_sub_type_c').length && $('#details_for_fault_sub_type_c').length && $('#case_status_c').length) {
		var faultsub = $('#fault_sub_type_c').val();
		var faultdetails = $('#details_for_fault_sub_type_c').val();
		
		if (faultsub == 'Other' && faultdetails.length == 0 && dtbc_changedFields.indexOf('fault_sub_type_c') > -1 && !isClosed()) {
			add_error_style_custom(formname, 'fault_sub_type_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_FAULTSUBTYPE'));
			return false;
		}
	}
	*/
	return true;
}

function Paid_Amount_is_mandatory(formName) {
	if ($('#case_sub_status_c').val() == '19') {
		addRequired("paid_amount_c", formName);
	} else {
		removeRequired("paid_amount_c", formName);
	}
}

function Power_board_failure_fault_sub_type(formname) {
	/*
	if ($('#fault_sub_type_c').length && $('#details_for_fault_sub_type_c').length && $('#case_status_c').length) {
		var faultsub = $('#fault_sub_type_c').val();
		var faultdetails = $('#details_for_fault_sub_type_c').val();
		
		if (faultsub == "Power board" && faultdetails.length == 0 && dtbc_changedFields.indexOf('fault_sub_type_c') > -1 && !isClosed()) {
			add_error_style_custom(formname, 'fault_sub_type_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_FAULTSUBTYPE2'));
			return false;
		}
	}
	*/
	return true;
}

function Resolution_is_mandatory_when_closing_a_c(formname) {
	if ($('#case_type_c').length && $('#resolution1_c').length && $('#case_status_c').length && $('#case_reason_c').length) {
		var ctype = $('#case_type_c').val();
		var resolution = $('#resolution1_c').val();
		var subtype = $('#case_reason_c').val();
		
		if (ctype == 2 && resolution.length == 0 && isClosed() && subtype != "Duplication") {
			add_error_style_custom(formname, 'resolution1_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_RESOLUTION'));
			return false;
		}
	}
	
	return true;
}

function RMAType_WO_fault_type(formname) {
	/*
	if ($('#case_record_type_c').length && $('#fault_sub_type_c').length && $('#case_sub_status_c').length && $('#fault_sub_type_c').length && $('#case_reason_c').length) {
		var ctype = $('#case_record_type_c').val();
		var cstatus = $('#case_sub_status_c').val();
		var fsubtype = $('#fault_sub_type_c').val();
		var reason = $('#case_reason_c').val();
		
		if (ctype == 'rma' && (fsubtype.length == 0 || reason.length == 0) && cstatus == 10) {
			add_error_style_custom(formname, 'fault_sub_type_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_FAULTSUBTYPE3'));
			return false;
		}
	}
	*/
	return true;
}

function Service_instruction_flag_in_site(formname) {
	if ($('#service_instruction_flag_c').length && $('#ack_service_instruction_c').length && $('#s1_site_casess1_site_ida').length) {
		var insflag = $('#service_instruction_flag_c').val();
		var ackins = $('#ack_service_instruction_c').val();
		var siteId = $('#s1_site_casess1_site_ida').val();
		
		if (insflag.length > 0 && ackins == 0 && $('#dtbc_originalSiteId').val() == siteId) {
			add_error_style_custom(formname, 'ack_service_instruction_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_SERVICEINSTR'));
			return false;
		}
	}
	return true;
}

function Solar_City_required_field(formname) {
	if ($('#account_name').length && $('#customer_case_c').length && $('#case_record_type_c').length) {
		var accname = $('#account_name').val();
		var ccase = $('#customer_case_c').val();
		var crecord = $('#case_record_type_c').val();
		var acountry = $('#validation_accountCountry').val();
		var recId = $('input[name="record"]').val();
		
		if (accname.indexOf("SolarCity") !== -1 && ccase.length == 0 && crecord == 'rma') {
			$.ajax({
				method: "POST",
				url: "index.php",
				data: {
					'record': 		 recId,
					'module': 		 'Cases',
					'return_module': 'Cases',
					'action': 		 'validationFieldsCheck',
					'return_id': 	 recId,
					'return_action': 'DetailView',
					'relate_to': 	 'Cases',
					'relate_id': 	 recId,
					'offset': 		 1,
					'accountId': 	 $('#account_id').val(),
				}
			})
			.done(function (datastring) {
				if (datastring == "false") {
					add_error_style_custom(formname, 'customer_case_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_CUSTOMERCASE'));
					return false;
				} else {
					return true;
				}
			})
			.fail(function (e) {
				console.log(e);
				$('#myloading').hide();
			});
			return false;
		}
	}
	return true;
}

function Spam_email(formname) {
	if ($('#name').length) {
		var subject = $('#name').val();
		
		if (subject.indexOf("Friendly Reminder: Your Corporate Password has expired") !== -1) {
			add_error_style_custom(formname, 'name', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_SPAM'));
			return false;
		}
	}
	return true;
}

function Sub_Symptom_mandatory_for_Support_cases(formName) {
	if ($('#symptom_c').length && $('#case_type_c').length && $('#sub_symptom_c').length) {
		if (($('#case_type_c').val() == '1' || $('#case_type_c').val() == '2') && $('#symptom_c').val().length > 0 && $('#sub_symptom_c').val().length == 0) {
			add_error_style_custom(formName, 'sub_symptom_c', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_SUB_SYMPTOM'));
			return false;
		}
	}
	return true;
}

function TEMPLATE_CASE_NO_CHANGES(formname) {
	if ($('#account_name').length) {
		var accName = $('#account_name').val();
		
		if (accName == "Template Case" && !isNewRecord()) {
			add_error_style_custom(formname, 'account_name', SUGAR.language.get('Cases','LBL_VALIDATION_ERR_TEMPLATECASE'));
			return false;
		}
	}
	return true;
}

function Validate_Guidelines_deviation_reason(formName) {
	if ($('#installation_rules_c').val() == '2') {
		addRequired("guidelines_deviation_reason_c", formName);
	} else {
		removeRequired("guidelines_deviation_reason_c", formName);
	}
}

function When_Error_code_Number_mandate_Error(formName) {
	if ($('#sub_symptom_c').val() == '13' && !isClosed()) {
		addRequired("error_code_number_c", formName);
	} else if ($('#sub_symptom_c').val() != '1') {
		removeRequired("error_code_number_c", formName);
	}
}

function model_is_mandatory_for_submitted_for_RMA(formName) {
	var fieldName = 'inverter_model_c';
	if ($('#case_status_c').length && $('#' + fieldName).length && $('#case_reason_c').length) {
		if ($('#case_status_c').val() == '16' && $('#case_reason_c').val() == 'Inverter' && $('#' + fieldName).val() == '') {
			add_error_style_custom(formName, fieldName, SUGAR.language.get('Cases','LBL_VALIDATION_ERR_MODEL'));
			return false;
		}
	}
	return true;
}

function Inverter_Validaiton_Mandatory_Tech_Issue(formName) {
	if ($('#case_type_c').val() == '2') {
		// Technical Issue
		addRequired("inverter_serial_c", formName);
	} else {
		removeRequired("inverter_serial_c", formName);
	}
}

function Battery_fault_description_is_mandatory(formName) {
	if ($('#escalation_to_tesla_c').length && $('#immediate_escalation_to_c').length && $('#battery_fault_description_c').length) {
		if ($('#escalation_to_tesla_c').val().length > 0 || $('#immediate_escalation_to_c').val().length > 0) {
			addRequired("battery_fault_description_c", formName);
		} else {
			removeRequired("battery_fault_description_c", formName);
		}
	}
}

function Escalated_field_engineer_must_enter_cou(formName) {
	if ($('#case_sub_status_c').length && 
		$('#field_engineer_c').length && 
		$('#site_contact_name_c').length &&
		$('#site_contact_phone_c').length &&
		$('#case_site_c').length &&
		$('#tile_roof_c').length &&
		$('#case_address_c').length &&
		$('#case_city_c').length &&
		$('#case_country_c').length) {
		if ($('#case_sub_status_c').val() == 1) {
			addRequired('field_engineer_c', formName);
			addRequired('site_contact_name_c', formName);
			addRequired('site_contact_phone_c', formName);
			addRequired('case_site_c', formName);
			addRequired('tile_roof_c', formName);
			addRequired('case_address_c', formName);
			addRequired('case_city_c', formName);
			addRequired('case_country_c', formName);
		} else {
			removeRequired('field_engineer_c', formName);
			removeRequired('site_contact_name_c', formName);
			removeRequired('site_contact_phone_c', formName);
			removeRequired('case_site_c', formName);
			removeRequired('tile_roof_c', formName);
			removeRequired('case_address_c', formName);
			removeRequired('case_city_c', formName);
			removeRequired('case_country_c', formName);
		}
	}
}

function RecejtionList_TechnicalIssue(formName) {
	if ($('#case_type_c').val() == 2) {
		if ($('#case_reason_c').length)
			addRequired('case_reason_c', formName);
		if ($('#fault_category_c').length)
			addRequired('fault_category_c', formName);
		if ($('#fault_sub_type_c').length)
			addRequired('fault_sub_type_c', formName);
		if ($('#error_event_code_c').length)
			addRequired('error_event_code_c', formName);
	} else if ($('#case_type_c').val() != 2 && isNewRecord()) {
		if ($('#case_reason_c').length)
			removeRequired('case_reason_c', formName);
		if ($('#fault_category_c').length)
			removeRequired('fault_category_c', formName);
		if ($('#fault_sub_type_c').length)
			removeRequired('fault_sub_type_c', formName);
		if ($('#error_event_code_c').length)
			removeRequired('error_event_code_c', formName);
	}
}

function addRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex == -1) {
			$().html(current.substring(0, myIndex));
			$('[dtbc-data=' + labelName + ']').append('<span class="required">*</span>');
			addToValidate(formName, fieldName, fieldName, true);
		}
	}	
}

function removeRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex != -1) {
			$('[dtbc-data=' + labelName + ']').html(current.substring(0, myIndex));
			removeFromValidate(formName, fieldName);
		}
	}
}

function isClosed() {
	if ($('#case_status_c').length) {
		var cstatus = $('#case_status_c').val();
		return cstatus == 7;
	}
	return true;
}

function isNewRecord() {
	return $('input[name="record"]').val().length == 0;
}

function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation() {
	clear_all_errors();
	var formName = 'EditView';
	var retval = check_form(formName);
	retval = calcRetval(retval, Block_Send_to_ERP(formName));
	retval = calcRetval(retval, Cant_close_a_case_without_a_site_name(formName));
	retval = calcRetval(retval, Cant_Open_RMA_if_battery_not_valid_for_s(formName));
	retval = calcRetval(retval, Details_for_Fault_sub_category_other_M(formName));
	retval = calcRetval(retval, Details_for_fault_sub_type_mandatory(formName));
	retval = calcRetval(retval, Error_Code_Number_value_1_and_9999(formName));
	retval = calcRetval(retval, Fault_sub_category_and_Fault_description(formName));
	retval = calcRetval(retval, Inverter_Validaite_Mand_Tech_Issue_SF(formName));
	retval = calcRetval(retval, Inverter_Validaiton_format_check(formName));
	retval = calcRetval(retval, Mandate_Address_fields_for_Alliance_type(formName));
	retval = calcRetval(retval, Address_is_Mandatory_for_Redemption(formName));
	retval = calcRetval(retval, other_fault_sub_type(formName));
	retval = calcRetval(retval, Power_board_failure_fault_sub_type(formName));
	retval = calcRetval(retval, No_PO_box(formName));
	retval = calcRetval(retval, Resolution_is_mandatory_when_closing_a_c(formName));
	retval = calcRetval(retval, RMAType_WO_fault_type(formName));
	retval = calcRetval(retval, Service_instruction_flag_in_site(formName));
	retval = calcRetval(retval, Solar_City_required_field(formName));
	retval = calcRetval(retval, Spam_email(formName));
	retval = calcRetval(retval, TEMPLATE_CASE_NO_CHANGES(formName));
	retval = calcRetval(retval, Site_Mandatory_for_Technical_Issues(formName));
	retval = calcRetval(retval, model_is_mandatory_for_submitted_for_RMA(formName));
	retval = calcRetval(retval, Sub_Symptom_mandatory_for_Support_cases(formName));

	// Original validations
	if (retval) {
		var _form = document.getElementById(formName);
	    _form.action.value = 'Save';
	    SUGAR.ajaxUI.submitForm(_form);

	    return true;
	}
	scrollToError();
	
	return false;
}

function fieldChanged(fieldName) {
	if (dtbc_changedFields.indexOf(fieldName) == -1)
		dtbc_changedFields.push(fieldName);
}

$(document).ready(function() {
	jQuery.getScript('custom/include/dtbc/js/scrollingAfterAddingErrorStyle.js');
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	$("<input title='" + SUGAR.language.get('Cases','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation();' type='button' name='button' value='" + SUGAR.language.get('Cases','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
	
	var editFormName = 'EditView';
		
	if ($("#sub_symptom_c").length) {
		$("#sub_symptom_c").change(function() {
			error_code_number_is_mandatory(editFormName);
			When_Error_code_Number_mandate_Error(editFormName);
		});
		error_code_number_is_mandatory(editFormName);
		When_Error_code_Number_mandate_Error(editFormName);
	}
		
	
	if ($("#doa_c").length) {
		$("#doa_c").change(function() {
			If_DOA_is_YES_must_enter_the_operating_p(editFormName);
		});
		If_DOA_is_YES_must_enter_the_operating_p(editFormName);
	}
		
	
	if ($("#case_sub_status_c").length) {
		$("#case_sub_status_c").change(function() {
			Paid_Amount_is_mandatory(editFormName);
		});
		Paid_Amount_is_mandatory(editFormName);
	}
		
	
	if ($("#fault_sub_type_c").length) {
		$("#fault_sub_type_c").change(function() {
			fieldChanged('fault_sub_type_c');
		});
	}
		
	
	if ($("#error_event_code_c").length) {
		$("#error_event_code_c").change(function() {
			fieldChanged('error_event_code_c');
		});
	}
		
	if ($("#case_type_c").length) {
		$("#case_type_c").change(function() {
			fieldChanged('case_type_c');
			Account_and_Contact_mandatory(editFormName);
			Inverter_Validaiton_Mandatory_Tech_Issue(editFormName);
			RecejtionList_TechnicalIssue(editFormName);
		});
		Account_and_Contact_mandatory(editFormName);
		Inverter_Validaiton_Mandatory_Tech_Issue(editFormName);
	}		
	
	if ($("#installation_rules_c").length) {
		$("#installation_rules_c").change(function() {
			fieldChanged('installation_rules_c');
		});
		Validate_Guidelines_deviation_reason(editFormName);
	}
		
	if ($("#escalation_to_tesla_c").length) {
		$("#escalation_to_tesla_c").change(function() {
			fieldChanged('escalation_to_tesla_c');
			Battery_fault_description_is_mandatory(editFormName);
		});
		Battery_fault_description_is_mandatory(editFormName);
	}
		
	if ($("#immediate_escalation_to_c").length) {
		$("#immediate_escalation_to_c").change(function() {
			fieldChanged('immediate_escalation_to_c');
			Battery_fault_description_is_mandatory(editFormName);
		});
		Battery_fault_description_is_mandatory(editFormName);
	}
	
	if ($("#case_sub_status_c").length) {
		$("#case_sub_status_c").change(function() {
			fieldChanged('case_sub_status_c');
			Escalated_field_engineer_must_enter_cou(editFormName);
		});
		Escalated_field_engineer_must_enter_cou(editFormName);
	}
	
	if ($("#s1_site_casess1_site_ida").length)
		dtbc_originalSiteId = $("#s1_site_casess1_site_ida").val();
	
	if ($("#validation_workingrecord").length) {
		$('<input>').attr({
			type: 'hidden',
			name: 'workingrecord',
			value: $("#validation_workingrecord").val()
		}).appendTo("#" + editFormName);
	}
	
	$("#btn_contact_created_by_name").removeAttr("onclick");
    $("#btn_contact_created_by_name").prop('onclick', null).off('click');
    $('#btn_contact_created_by_name').on('click', function () {
        open_popup(
			"Contacts", 
			600, 
			400, 
			"&accountId=" + $('#account_id').val(), 
			true, 
			false, 
			{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"contact_created_by_id","first_name":"contact_created_by_name"}}, 
			"single", 
			true
		);
    });

	
	// Number validations
	//if ($("#case_zip_c").length)
	//	$("#case_zip_c").keydown(dtbc_allowNumbersOnly);
	//if ($("#rma_zip_postal_code_c").length)
	//	$("#rma_zip_postal_code_c").keydown(dtbc_allowNumbersOnly);
	//if ($("#site_zip_c").length)
	//	$("#site_zip_c").keydown(dtbc_allowNumbersOnly);
	//$("#state_zip_c").keydown(dtbc_allowNumbersOnly);
	if ($("#site_contact_person_phone_c").length)
		$("#site_contact_person_phone_c").keydown(dtbc_allowNumbersOnly);
	if ($("#contact_phone_c").length)
		$("#contact_phone_c").keydown(dtbc_allowNumbersOnly);
	if ($("#site_contact_phone_c").length)
		$("#site_contact_phone_c").keydown(dtbc_allowNumbersOnly);
});