function setRequiredRmaFields(formName) {
	if ($("#doa_c").length) {
		addRequired('doa_c', formName);
		if ($("#doa_c").val() == '1') {
			addRequired('operating_period_c', formName);
		} else {
			removeRequired('operating_period_c', formName);
		}
	}
	
	addRequired('replacement_type_c', formName);
	addRequired('ship_to_name_c', formName);
	addRequired('rma_street_c', formName);
	addRequired('rma_city_c', formName);
	addRequired('rma_country_c', formName);
	addRequired('rma_zip_postal_code_c', formName);
	addRequired('fault_category_c', formName);
	addRequired('fault_sub_type_c', formName);
	addRequired('site_contact_person_name_c', formName);
	addRequired('site_contact_person_phone_c', formName);
	addRequired('installation_rules_c', formName);
	addRequired('monitored_c', formName);
}

function addRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex == -1) {
			$().html(current.substring(0, myIndex));
			$('[dtbc-data=' + labelName + ']').append('<span class="required">*</span>');
			addToValidate(formName, fieldName, fieldName, true);
		}
	}	
}

function removeRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex != -1) {
			$('[dtbc-data=' + labelName + ']').html(current.substring(0, myIndex));
			removeFromValidate(formName, fieldName);
		}
	}
}

$(document).ready(function() {
	var formName = 'EditView';	
	
	setRequiredRmaFields(formName);
	
	// Add new rma indicator field to form
	$('<input />').attr('type', 'hidden')
	  .attr('name', "newrma")
	  .attr('value', "1")
	  .appendTo('#' + formName);
	
	if ($("#doa_c").length)
		$("#doa_c").change(function() {
			if ($("#doa_c").val() == '1') {
				addRequired('operating_period_c', formName)
			} else {
				removeRequired('operating_period_c', formName)
			}
		});
});