$(document).ready(function() {
	var formName = 'EditView';	
	
	// Add spcase indicator field to form
	$('<input />').attr('type', 'hidden')
	  .attr('name', "spcase")
	  .attr('value', "1")
	  .appendTo('#' + formName);
	  
	$('<input />').attr('type', 'hidden')
	  .attr('name', "workingrecord")
	  .attr('value', $("#validation_workingrecord").val())
	  .appendTo('#' + formName);
});