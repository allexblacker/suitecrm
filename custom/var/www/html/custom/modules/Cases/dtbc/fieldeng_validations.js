function setRequiredFields(formName) {

	addRequired('field_engineer_c', formName);
	addRequired('case_site_c', formName);
	addRequired('case_country_c', formName);
	addRequired('case_address_c', formName);
	addRequired('case_city_c', formName);
	addRequired('tile_roof_c', formName);
	addRequired('site_contact_name_c', formName);
	addRequired('site_contact_phone_c', formName);
}

function addRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex == -1) {
			$().html(current.substring(0, myIndex));
			$('[dtbc-data=' + labelName + ']').append('<span class="required">*</span>');
			addToValidate(formName, fieldName, fieldName, true);
		}
	}	
}

$(document).ready(function() {
	var formName = 'EditView';	
	
	setRequiredFields(formName);
	
	// Add new rma indicator field to form
	$('<input />').attr('type', 'hidden')
	  .attr('name', "fieldengineer")
	  .attr('value', "1")
	  .appendTo('#' + formName);
});