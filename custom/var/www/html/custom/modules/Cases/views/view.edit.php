<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('include/SugarTinyMCE.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");

class CasesViewEdit extends ViewEdit {
	
	var $useForSubpanel = true;

    function __construct(){
        parent::__construct();
    }
	
	function preDisplay() {
		// Skip in quick create view
		if (!empty($_REQUEST['target_action']) && $_REQUEST['target_action'] == "QuickCreate")
			return;
		
		// Remove cache
		$cacheFileName = "cache/themes/" . $GLOBALS['theme'] . "/modules/" . $_REQUEST['module'] . "/EditView.tpl";
		if (file_exists($cacheFileName))
			unlink($cacheFileName);
		
		// Get record type from url or from bean
		$recordType = "";
		if (strlen($this->bean->id) > 0) {
			if (!empty($_REQUEST['fieldengineer']) && isset($_REQUEST['fieldengineer']) && $_REQUEST['fieldengineer'] == 1) {
				$this->prefillSiteAddress();
				$recordType = 'fieldengineer';
			} else if (!empty($_REQUEST['newrma']) && isset($_REQUEST['newrma']) && $_REQUEST['newrma'] == 2) {
				$this->prefillRmaAddress();
				$recordType = 'newrmacreation';
				$this->bean->get_address_confirmation_c = $_REQUEST['confValue'] == 0 ? "1" : "0";
			} else if (!empty($_REQUEST['closecase']) && isset($_REQUEST['closecase']) && $_REQUEST['closecase'] == 1) {
				$this->bean->case_status_c = 7; // Closed
				$recordType = $this->bean->case_record_type_c == "rma" ? "rmaclosecase" : "closecase";
			} else {
				$recordType = TplCalculator::getRecordType($this->bean->case_record_type_c, isset($_REQUEST['caseType']) ? $_REQUEST['caseType'] : "");
			}
		} else if (!empty($_REQUEST['caseType']) && isset($_REQUEST['caseType'])) {
			$recordType = $_REQUEST['caseType'];
			$parentBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
			$this->bean->parent_case_c = $parentBean->case_number;
			$this->bean->acase_id_c = $_REQUEST['workingrecord'];
			$this->bean->case_record_type_c = $_REQUEST['caseType'];
		} else if (!empty($_REQUEST['spcase']) && isset($_REQUEST['spcase']) && $_REQUEST['spcase'] == 1) {
			$parentBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
			$this->bean->parent_case_c = $parentBean->case_number;
			$this->bean->acase_id_c = $_REQUEST['workingrecord'];
			$this->prefillSpCaseFields();
			$recordType = 'spcase';
		}
		
		// Set fields from parent
		if (!empty($_REQUEST['parent_type']) && isset($_REQUEST['parent_type']) &&
			!empty($_REQUEST['parent_id']) && isset($_REQUEST['parent_id'])) {
				$this->setSitePredefinedValues($_REQUEST['parent_type'], $_REQUEST['parent_id']);
		}

		// Get Custom TPL based on the custom layout logic
        $metadataFile = TplCalculator::getMetadata($_REQUEST['module'], $recordType, "editviewdefs.php");

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

        $this->ev = $this->getEditView();
        $this->ev->ss =& $this->ss;
        $this->ev->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/EditView/EditView.tpl'));
	}

    function display() {
        parent::display();
		
		// Quick create view "mode"
		if (!empty($_REQUEST['target_action']) && $_REQUEST['target_action'] == "QuickCreate") {
			if (!empty($_REQUEST['parent_type']) && $_REQUEST['parent_type'] == "S1_Site")
				$this->setSitePredefinedValues($_REQUEST['parent_type'], $_REQUEST['parent_id']);
			return;
		}

		$this->prefillFromCase();
		
        global $sugar_config;
        $new = empty($this->bean->id);
        if ($new) {
            ?>
            <script>
                $(document).ready(function(){
                    $('#update_text').closest('.edit-view-row-item').html('');
                    $('#update_text_label').closest('.edit-view-row-item').html('');
                    $('#internal').closest('.edit-view-row-item').html('');
                    $('#internal_label').closest('.edit-view-row-item').html('');
                    $('#addFileButton').closest('.edit-view-row-item').html('');
                    $('#case_update_form_label').closest('.edit-view-row-item').html('');
                });
            </script>
			<?php
        }
		if (!empty($_REQUEST['spcase']) && isset($_REQUEST['spcase']) && $_REQUEST['spcase'] == 1) {
			echo getVersionedScript("custom/modules/Cases/dtbc/spcase_settings.js");
		}
		if (!empty($_REQUEST['fieldengineer']) && isset($_REQUEST['fieldengineer']) && $_REQUEST['fieldengineer'] == 1) {
			echo getVersionedScript("custom/modules/Cases/dtbc/fieldeng_validations.js");
		}
		if (!empty($_REQUEST['newrma']) && isset($_REQUEST['newrma']) && $_REQUEST['newrma'] == 2) {
			echo getVersionedScript("custom/modules/Cases/dtbc/rma_validations.js");
			echo '<input type="hidden" id="validation_newrma" value="1" />';			
		}
		echo '<input type="hidden" id="validation_workingrecord" value="' . $_REQUEST['workingrecord'] . '" />';
		echo getVersionedScript("custom/modules/Cases/dtbc/validations.js");
		echo getVersionedScript("custom/include/dtbc/js/commonvalidations.js");
		if (strlen($this->bean->id) > 0) {
			$createdDate = new DateTime($this->bean->date_entered);			
			$oldDate = new DateTime("2012-08-09");
			$value = $oldDate < $createdDate ? 1 : 0;
			echo '<input type="hidden" id="validation_dateCreatedBigger" value="' . $value . '" />';
			echo '<input type="hidden" id="validation_origSerial" value="' . $this->bean->inverter_serial_c . '" />';
			
			$oldDate = new DateTime("2012-09-12");
			$value = $oldDate < $createdDate ? 1 : 0;
			echo '<input type="hidden" id="validation_dateCreatedBigger2" value="' . $value . '" />';
		}
    }
	
	private function setSitePredefinedValues($moduleType, $recordId) {
		$site = BeanFactory::getBean($moduleType, $recordId);
		$this->bean->account_name = $site->account_name;
		$this->bean->account_id = $site->account_id_c;
		$this->bean->case_site_c = $site->site_name;
		echo '<input type="hidden" id="dtbc_account_name" value="' . $site->account_name . '" />';
		echo '<input type="hidden" id="dtbc_account_id" value="' . $site->account_id_c . '" />';
		echo '<input type="hidden" id="dtbc_site_name" value="' . $site->site_name . '" />';
		echo getVersionedScript("custom/modules/Cases/dtbc/quickcreate.js");
	}
	
	private function prefillSpCaseFields() {
		global $sugar_config;
		// Get original case
		$origBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
		
		// Save values
		$temp = array(
			"case_type_c" => $origBean->case_type_c,
			"symptom_c" => $origBean->symptom_c,
			"sub_symptom_c" => $origBean->sub_symptom_c,
			"error_code_number_c" => $origBean->error_code_number_c,
			"inverter_serial_c" => $origBean->inverter_serial_c,
			"case_site_c" => $origBean->case_site_c,
			"fault_category_c" => $origBean->fault_category_c,
			"fault_sub_type_c" => $origBean->fault_sub_type_c,
			"inverter_model_c" => $origBean->inverter_model_c,
			"panel_c" => $origBean->panel_c,
			"site_contact_name_c" => $origBean->site_contact_name_c,
			"site_contact_phone_c" => $origBean->site_contact_phone_c,
			"error_event_code_c" => $origBean->error_event_code_c,
			"case_record_type_c" => "spcase",
			"name" => "SP case " . $origBean->case_number,
			"contact_created_by" => $sugar_config['solaredge']['spcase']['ContactId'],
			"contact_created_by_id" => $sugar_config['solaredge']['spcase']['ContactId'],
			"contact_created_by_name" => $sugar_config['solaredge']['spcase']['ContactName'],
		);
		
		// Prefill values
		foreach ($temp as $key => $value)
			$this->bean->$key = $value;
	}
	
	private function prefillFromCase() {
		if (isset($_REQUEST['caseType']) && $_REQUEST['caseType'] == 'compensation' && !empty($_REQUEST['workingrecord'])) {
			$fromBean = BeanFactory::getBean("Cases", $_REQUEST['workingrecord']);
						
			if ($fromBean != null) {
				global $current_user;
				
				echo '<input type="hidden" id="prefill_account_id" value="' . $fromBean->account_id . '" />'; // [account_id] => 7e80456a-ad6b-b9f5-0372-59958eae04b1
				echo '<input type="hidden" id="prefill_account_name" value="' . $fromBean->account_name . '" />'; // [account_name] => TestAccount
				echo '<input type="hidden" id="prefill_contact_id" value="' . $fromBean->contact_id . '" />'; // [contact_id] => 
				echo '<input type="hidden" id="prefill_contact_name" value="" />'; // None
				echo '<input type="hidden" id="prefill_case_record_type" value="compensation" />'; // [case_record_type_c]
				echo '<input type="hidden" id="prefill_case_owner_id" value="' . $current_user->id . '" />'; // [assigned_user_id]
				echo '<input type="hidden" id="prefill_case_owner_name" value="' . $current_user->name . '" />'; // [assigned_user_name]
				echo '<input type="hidden" id="prefill_priority" value="3" />'; // [priority]
				echo '<input type="hidden" id="prefill_type" value="5" />'; // [case_type_c]
				$caseSubject = "Compensation For " . $fromBean->rma_in_erp_c;
				echo '<input type="hidden" id="prefill_subject" value="' . $caseSubject . '" />';
				echo '<input type="hidden" id="prefill_old_case_id" value="' . $_REQUEST['workingrecord'] . '" />';
				echo '<input type="hidden" id="prefill_description" value="Compensation RMU" />';
				
				?>
					<script>
						$(document).ready(function(){
							$('#account_name').val($('#prefill_account_name').val());
							$('#account_id').val($('#prefill_account_id').val());
							$('#assigned_user_name').val($('#prefill_case_owner_name').val());
							$('#assigned_user_id').val($('#prefill_case_owner_id').val());
							$('#case_type_c').val($('#prefill_type').val());
							$('#priority').val($('#prefill_priority').val());
							$('#name').val($('#prefill_subject').val());
							$('#description').val($('#prefill_description').val());
							$('#case_record_type_c').val($('#prefill_case_record_type').val());
						});
					</script>
				<?php
			}
		}
	}
	
	private function prefillRmaAddress() {
		if (!empty($this->bean->account_id) && strlen($this->bean->account_id) > 0) {
			$selAccount = BeanFactory::getBean("Accounts", $this->bean->account_id);
			$this->bean->rma_street_c = $selAccount->street_c;
			$this->bean->rma_city_c = $selAccount->city_c;
			$this->bean->rma_zip_postal_code_c = $selAccount->zip_postal_code_c;
			$this->bean->rma_state_c = $selAccount->state_c;
			$this->bean->rma_country_c = $selAccount->country_c;
		}
	}
	
	private function prefillSiteAddress() {
		if (!empty($this->bean->s1_site_casess1_site_ida) && strlen($this->bean->s1_site_casess1_site_ida) > 0) {
			$selSite = BeanFactory::getBean("S1_Site", $this->bean->s1_site_casess1_site_ida);
			$this->bean->case_address_c = $selSite->site_address;
			$this->bean->case_city_c = $selSite->site_city;
			$this->bean->case_zip_c = $selSite->site_zip;
			$this->bean->case_state_c = $selSite->site_state;
			$this->bean->case_country_c = $selSite->site_country;
		}
	}
	
}
