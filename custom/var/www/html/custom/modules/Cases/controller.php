<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/Cases/controller.php');
require_once('modules/SecurityGroups/SecurityGroup.php');
require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/router.php");

class CustomCasesController extends CasesController {
	
	public function action_checkRma() {
		if (isset($_REQUEST['selectedRecords']) && is_array($_REQUEST['selectedRecords']) && count($_REQUEST['selectedRecords']) > 0) {
			foreach ($_REQUEST['selectedRecords'] as $recordId)
				$this->invertRmaState($recordId);
		}
		die();
	}
	
	private function invertRmaState($recordId) {
		global $db;
		
		// Get RMA state
		$sql = "SELECT rma FROM sn1_serial_number WHERE id = " . $db->quoted($recordId);
		$res = $db->fetchOne($sql);
		if ($res == null || !is_array($res) || !isset($res['rma']))
			return false;
		
		// Set RMA state to the opposite
		$newValue = $res['rma'] == '1' ? 0 : 1;
		
		$sql = "UPDATE sn1_serial_number SET rma = " . $newValue . " WHERE id = " . $db->quoted($recordId);
		$db->query($sql);
		
		return true;
	}
	
	public function action_changeStatus() {
		$bean = BeanFactory::getBean($_REQUEST['module'], $_REQUEST['record']);
		$bean->case_status_c = 3; // Waiting Customer Feedback
		$bean->save();
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_closeCase() {
		$this->view = "edit";
	}
	
	public function action_createspcase() {
		$this->view = "edit";
	}
	
	public function action_fieldEngineer() {
		$this->view = "edit";
	}
	
	public function action_newRma() {
		if (isset($_REQUEST['newrma']) && $_REQUEST['newrma'] == 1)
			$this->view = "creatermastep1";
		else
			$this->view = "list";
	}
	
	public function action_validationFieldsCheck() {
		if (isset($_REQUEST['accountId']) && strlen($_REQUEST['accountId']) > 0) {
			$accountBean = BeanFactory::getBean("Accounts", $_REQUEST['accountId']);
			if ($accountBean->country_c == "Unitedstates")
				echo "true";
		}
		echo "false";
		die();
	}
	
	public function action_newCompensation() {
		SugarApplication::redirect('index.php?' . http_build_query(array('module' => $_REQUEST['module'], 
																		'action' => 'EditView', 
																		'workingrecord' => $_REQUEST['workingrecord'],
																		'caseType' => 'compensation')));
	}

	public function action_EditView() {

		if ((isset($_REQUEST['record']) && strlen($_REQUEST['record']) > 0) ||
			(isset($_REQUEST['caseType']) && strlen($_REQUEST['caseType']) > 0)){
			$this->view = "edit";
		} else {
			$this->view = "createstep1";
		}

	}

	public function action_changeTier2() {
		$this->bean->tier_2_assignee_c = 6;
		$this->bean->case_sub_status_c = 8;
		$this->bean->save();
		//$this->changeTier(2);
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_changeTier3() {
		$this->bean->tier3_escalation_owner_c = 1;
		$this->bean->case_sub_status_c = 9;
		$this->bean->save();
		//$this->changeTier(3);
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	private function changeTier($tierNumber) {
		global $sugar_config, $db;
		$secGroups = new SecurityGroup();
		$groupId = $tierNumber == 2 ? $sugar_config['solaredge']['tier2_group_id'] : $sugar_config['solaredge']['tier3_group_id'];
		$module = $_REQUEST['module'];
		$record = $_REQUEST['record'];
		
		// Remove security group
		$sql = "SELECT * FROM securitygroups_records
				WHERE deleted = 0 AND
				module = " . $db->quoted($module) . " AND 
				record_id = " . $db->quoted($record);
				
		$oldGroup = $db->fetchOne($sql);
		
		$secGroups->removeGroupFromRecord($module, $record, $oldGroup['securitygroup_id']);
		
		// Set new Group
		$secGroups->addGroupToRecord($module, $record, $groupId);
		
		// Return to the detail view
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_approveCompensation() {
		// TODO: update the relevant fields in the ERP
		$module = $_REQUEST['workingmodule'];
		$record = $_REQUEST['workingrecord'];
		
		// Send email to customer "Compensation Approved Template"
		require_once("custom/include/dtbc/EmailSender.php");
		global $sugar_config;
		$emailSender = new EmailSender();
		$emailAddress = $emailSender->getEmailAddresFromCase($this->bean);
		$emailId = $sugar_config['solaredge']['compensation_approved_email_id'];
		$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $this->bean->id, $emailId);
		
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_declineCompensation() {
		$focus = BeanFactory::getBean("Cases", $_REQUEST['record']);
		$focus->case_status_c = 18; // Close – Declined
		$focus->save();
		
		// Send email to customer "Compensation Declined Template"
		require_once("custom/include/dtbc/EmailSender.php");
		global $sugar_config;
		$emailSender = new EmailSender();
		$emailAddress = $emailSender->getEmailAddresFromCase($this->bean);
		$emailId = $sugar_config['solaredge']['compensation_decline_email_id'];
		$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $this->bean->id, $emailId);
		
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_check_serial() {
		$this->bean->ibolt_status_c = 'Start';
		$this->bean->save();
		
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_check_battery() {
		$this->bean->battery_ibolt_status_c = 'Start';
		$this->bean->save();
		
		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_check_alternative() {
		$this->bean->alternative_ibolt_status_c = "Start";
		$this->bean->save();	

		Router::returnToDetailView("Cases", $_REQUEST['record']);
	}
	
	public function action_changeIndicator() {
		if(empty($_REQUEST['record']))
			Router::returnToDetailView("Cases", $_REQUEST['record']);
		
		$caseBean = BeanFactory::getBean('Cases', $_REQUEST['record']);
		
		global $db;
		
		$sql = "UPDATE cases_cstm
				SET alert_override_c = '', change_indicator_c = ''
				WHERE id_c = " . $db->quoted($_REQUEST['record']);
				
		if ($caseBean->alert_override_c == null || strlen(trim($caseBean->alert_override_c)) == 0)
			$sql = "UPDATE cases_cstm
					SET alert_override_c = 'red', change_indicator_c = " . $db->quoted(htmlentities(DtbcHelpers::getImageTagForFormulaFields('New_red_envelope.png','Change has been made',true,false,15,25))) . "
					WHERE id_c = " . $db->quoted($_REQUEST['record']);

		$res = $db->query($sql);
        
		Router::returnToDetailView("Cases", $_REQUEST['record']);
    }

	// SolarEdge codes from here
	// Kobi
	public function action_SendToERP() {
        $bean = BeanFactory::getBean('Cases', $_REQUEST['record']);
        $bean->send_to_priority_c = "Yes";
        $bean->save();
        Router::returnToDetailView("Cases", $_REQUEST['record']);
    }
    
}
