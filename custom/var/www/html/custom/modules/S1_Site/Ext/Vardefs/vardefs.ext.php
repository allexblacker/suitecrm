<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-11-19 18:45:34
$dictionary['S1_Site']['fields']['link_to_monitoring_c']['inline_edit']='';
$dictionary['S1_Site']['fields']['link_to_monitoring_c']['labelValue']='link to monitoring';

 

// created: 2017-09-25 20:31:20
$dictionary["S1_Site"]["fields"]["s1_site_p1_project_1"] = array (
  'name' => 's1_site_p1_project_1',
  'type' => 'link',
  'relationship' => 's1_site_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-10-16 12:37:39
$dictionary['S1_Site']['fields']['monitoring_site_id']['required']=true;

 

 // created: 2017-09-14 14:49:43

 

 // created: 2017-10-17 18:58:52
$dictionary['S1_Site']['fields']['account_time_c']['inline_edit']='1';
$dictionary['S1_Site']['fields']['account_time_c']['labelValue']='Account Time';

 

 // created: 2017-10-16 15:05:51

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['S1_Site']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);


$dictionary["S1_Site"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-10-16 15:06:11

 

 // created: 2017-10-16 12:37:19
$dictionary['S1_Site']['fields']['name']['required']=true;
$dictionary['S1_Site']['fields']['name']['full_text_search']=array (
);

 

// created: 2017-08-31 12:51:23
$dictionary["S1_Site"]["fields"]["s1_site_cases"] = array (
  'name' => 's1_site_cases',
  'type' => 'link',
  'relationship' => 's1_site_cases',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_CASES_FROM_CASES_TITLE',
);

?>