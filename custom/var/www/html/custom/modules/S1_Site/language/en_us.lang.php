<?php
// created: 2017-11-19 18:19:42
$mod_strings = array (
  'LBL_EDITVIEW_PANEL5' => 'New Panel 5',
  'SITE_STATE' => 'Site State',
  'LBL_NAME' => 'Name',
  'MONITORING_SITE_ID' => 'Monitoring Site ID',
  'ROOF_TYPE' => 'Roof Type',
  'PANEL_TYPE' => 'Panel Type',
  'ACCOUNT_TIME' => 'Account Time',
  'LBL_LINK_TO_MONITORING' => 'link to monitoring',
);