<?php

$mod_strings['LBL_DAYS'] = 'Tage';
$mod_strings['LBL_DESCRIPTION'] = 'Notizen';
$mod_strings['LBL_HOURS'] = 'Stunden';
$mod_strings['LBL_LAG'] = 'Verzögerung';
$mod_strings['LBL_LIST_ASSIGNED_USER_ID'] = 'Ressource';
$mod_strings['LBL_LIST_PERCENT_COMPLETE'] = '% Abgeschlossen';
$mod_strings['LBL_PARENT_TASK_ID'] = 'Übergeordnete Aufgaben-ID';
$mod_strings['LBL_PERCENT_COMPLETE'] = '% vollständig';
$mod_strings['LBL_PREDECESSORS'] = 'Vorgänger';
$mod_strings['LBL_RELATIONSHIP_TYPE'] = 'Typ der Geschäftsbeziehung';
$mod_strings['LBL_SUBTASK'] = 'Unter-Aufgabe';
