<?php
// created: 2017-11-22 13:33:11
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'product_c' => 
  array (
    'vname' => 'LBL_PRODUCT',
    'width' => '15%',
    'default' => true,
  ),
  'discount_c' => 
  array (
    'vname' => 'LBL_DISCOUNT',
    'width' => '15%',
    'default' => true,
  ),
  'quantity_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_QUANTITY',
    'width' => '10%',
  ),
  'shipment_date_c' => 
  array (
    'vname' => 'LBL_SHIPMENT_DATE',
    'width' => '15%',
    'default' => true,
  ),
  'total_price_c' => 
  array (
    'vname' => 'LBL_TOTAL_PRICE',
    'width' => '15%',
    'default' => true,
  ),
  'total_kw_c' => 
  array (
    'vname' => 'LBL_TOTAL_KW',
    'width' => '15%',
    'default' => true,
  ),
  'unit_price_after_discount_c' => 
  array (
    'vname' => 'LBL_UNIT_PRICE_AFTER_DISCOUNT',
    'width' => '15%',
    'default' => true,
  ),
  'sale_type_c' => 
  array (
    'vname' => 'LBL_SALE_TYPE',
    'width' => '15%',
    'default' => true,
  ),
);