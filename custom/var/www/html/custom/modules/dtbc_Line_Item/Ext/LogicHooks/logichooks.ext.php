<?php 
 //WARNING: The contents of this file are auto-generated



$hook_array['before_save'][] = array(
	10,
	'Total price calculation',
	'custom/include/dtbc/hooks/10_LineItem_Calculations.php',
	'LineItemCalculations',
	'before_save'
);


$hook_array['after_save'][] = array(
	1,
	'Sync and/or copy line items',
	'custom/include/dtbc/hooks/quotesLineItemSyncAndCopy.php',
	'LineItemCopySync',
	'after_save_from_line_item'
);


$hook_array['after_save'][] = array(
	10,
	'Refresh opportunity amount',
	'custom/include/dtbc/hooks/10_refresh_opportunity_amount.php',
	'RefreshOpportunity',
	'after_save'
);
?>