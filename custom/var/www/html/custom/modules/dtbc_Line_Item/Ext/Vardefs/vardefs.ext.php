<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-05 18:57:18
$dictionary['dtbc_Line_Item']['fields']['total_price_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['total_price_c']['labelValue']='Total Price';

 

 // created: 2017-10-27 14:38:17
$dictionary['dtbc_Line_Item']['fields']['input_connector_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['input_connector_c']['labelValue']='Input Connector';

 

 // created: 2017-10-05 19:04:42
$dictionary['dtbc_Line_Item']['fields']['sale_type_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['sale_type_c']['labelValue']='Sale Type';

 

 // created: 2017-11-09 11:23:23
$dictionary['dtbc_Line_Item']['fields']['discount_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['discount_c']['labelValue']='Discount(%)';

 

 // created: 2017-10-16 14:10:47
$dictionary['dtbc_Line_Item']['fields']['forecast_q_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['forecast_q_c']['labelValue']='Forecast Q';

 

 // created: 2017-10-05 19:01:06
$dictionary['dtbc_Line_Item']['fields']['total_kw_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['total_kw_c']['labelValue']='Total KW';

 

 // created: 2017-10-05 15:21:33
$dictionary['dtbc_Line_Item']['fields']['dtbc_pricebook_product_id_c']['inline_edit']=1;

 

// created: 2017-10-04 15:48:02
$dictionary["dtbc_Line_Item"]["fields"]["opportunities_dtbc_line_item_1"] = array (
  'name' => 'opportunities_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_dtbc_line_item_1opportunities_ida',
);
$dictionary["dtbc_Line_Item"]["fields"]["opportunities_dtbc_line_item_1_name"] = array (
  'name' => 'opportunities_dtbc_line_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_dtbc_line_item_1opportunities_ida',
  'link' => 'opportunities_dtbc_line_item_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["dtbc_Line_Item"]["fields"]["opportunities_dtbc_line_item_1opportunities_ida"] = array (
  'name' => 'opportunities_dtbc_line_item_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_line_item_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);


 // created: 2017-10-05 18:55:14
$dictionary['dtbc_Line_Item']['fields']['shipment_date_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['shipment_date_c']['labelValue']='Shipment Date';

 

// created: 2017-10-16 12:24:53
$dictionary["dtbc_Line_Item"]["fields"]["aos_quotes_dtbc_line_item_1"] = array (
  'name' => 'aos_quotes_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_AOS_QUOTES_TITLE',
  'id_name' => 'aos_quotes_dtbc_line_item_1aos_quotes_ida',
);
$dictionary["dtbc_Line_Item"]["fields"]["aos_quotes_dtbc_line_item_1_name"] = array (
  'name' => 'aos_quotes_dtbc_line_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_AOS_QUOTES_TITLE',
  'save' => true,
  'id_name' => 'aos_quotes_dtbc_line_item_1aos_quotes_ida',
  'link' => 'aos_quotes_dtbc_line_item_1',
  'table' => 'aos_quotes',
  'module' => 'AOS_Quotes',
  'rname' => 'name',
);
$dictionary["dtbc_Line_Item"]["fields"]["aos_quotes_dtbc_line_item_1aos_quotes_ida"] = array (
  'name' => 'aos_quotes_dtbc_line_item_1aos_quotes_ida',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_line_item_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);


 // created: 2017-10-09 17:06:56
$dictionary['dtbc_Line_Item']['fields']['list_price_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['list_price_c']['labelValue']='List Price';

 

 // created: 2017-11-15 14:05:42
$dictionary['dtbc_Line_Item']['fields']['currency_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['currency_c']['labelValue']='Currency';

 

 // created: 2017-10-11 13:59:17
$dictionary['dtbc_Line_Item']['fields']['product_family_from_product_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['product_family_from_product_c']['labelValue']='Product family from product';

 

 // created: 2017-11-15 14:29:19
$dictionary['dtbc_Line_Item']['fields']['inverter_configuration_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['inverter_configuration_c']['labelValue']='inverter configuration';

 

 // created: 2017-10-05 15:21:33
$dictionary['dtbc_Line_Item']['fields']['product_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['product_c']['labelValue']='Product';

 

 // created: 2017-10-05 18:54:30
$dictionary['dtbc_Line_Item']['fields']['quantity_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['quantity_c']['labelValue']='Quantity';

 

 // created: 2017-10-17 16:36:26
$dictionary['dtbc_Line_Item']['fields']['dtbc_line_item_id_c']['inline_edit']=1;

 

 // created: 2017-11-15 15:44:37
$dictionary['dtbc_Line_Item']['fields']['unit_price_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['unit_price_c']['labelValue']='unit price';

 

 // created: 2017-11-15 14:27:14
$dictionary['dtbc_Line_Item']['fields']['product_line_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['product_line_c']['labelValue']='Product line';

 

 // created: 2017-10-11 13:59:28
$dictionary['dtbc_Line_Item']['fields']['kwforop_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['kwforop_c']['labelValue']='KWforOp';

 

 // created: 2017-10-05 19:01:56
$dictionary['dtbc_Line_Item']['fields']['unit_price_after_discount_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['unit_price_after_discount_c']['labelValue']='Unit Price After Discount';

 

 // created: 2017-10-17 16:36:26
$dictionary['dtbc_Line_Item']['fields']['line_item_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['line_item_c']['labelValue']='Line Item';

 

 // created: 2017-11-15 16:29:23
$dictionary['dtbc_Line_Item']['fields']['kwforopp_string_c']['inline_edit']='1';
$dictionary['dtbc_Line_Item']['fields']['kwforopp_string_c']['labelValue']='kwforopp string';

 
?>