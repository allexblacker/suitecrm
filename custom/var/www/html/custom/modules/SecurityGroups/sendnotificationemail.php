<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class SendNotificationEmail {
	
	function after_relationship_add($bean, $event, $arguments) {
		/*
		require_once("custom/include/dtbc/EmailSender.php");
		global $db;
						
		$sql = "SELECT email_addresses.email_address FROM securitygroups_users
				JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = securitygroups_users.user_id AND email_addr_bean_rel.deleted = 0
				JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id AND email_addresses.deleted = 0
				WHERE securitygroups_users.securitygroup_id = " . $db->quoted($arguments['id']) . " AND
				securitygroups_users.deleted = 0 AND
				email_addr_bean_rel.bean_module = 'Users'";
		
		$res = $db->query($sql);

		$emailSender = new EmailSender();
		
		while($row = $db->fetchByAssoc($res)) {
			$emailSender->sendNotificationOnNewRecordInGroup($row['email_address'], $arguments['related_module'], $arguments['related_id'], $this->getEmailInfo($arguments['related_module'], $arguments['related_id']));
		}
		*/
	}
	
	private function getEmailInfo($moduleName, $beanId) {
		global $sugar_config;
		return $sugar_config['site_url'] . "/index.php?module=" . $moduleName . "&action=DetailView&record=" . $beanId;
	}
	
}