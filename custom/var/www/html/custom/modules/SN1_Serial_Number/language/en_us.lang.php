<?php
// created: 2017-11-21 10:16:50
$mod_strings = array (
  'LBL_NAME' => 'Serial Number (old)',
  'PART_DESCRIPTION' => 'Part Description',
  'RETURN_ASKED_BY' => 'Return asked by',
  'NEED_IMMEDIATE_RETURN_TIME_STAMP' => 'Need Immediate Return TimeStamp',
  'LBL_EDITVIEW_PANEL5' => 'New Panel 5',
  'PORTIA_MEMORY' => 'Portia Memory',
  'DIGITAL_PCB_VERSION' => 'Digital PCB Version',
  'M40' => 'Minus 40	',
  'MODEL' => 'Model',
  'INV_ASSY_AC_DC_CONDUIT' => 'Inv assy AC/DC conduit	',
  'INV_ASSY_HEAT_SINK' => 'Inv assy Heat-Sink',
  'REPLACED_TYPE' => 'Replaced Type',
  'FAULT_DESCRIPTION' => 'Fault Description',
  'FAULT_SUB_CATEGORY' => 'Fault sub category',
  'LBL_CASE1_ACASE_ID' => 'Case (related Case ID)',
  'CASE1' => 'Case',
  'LBL_ALT_PART' => 'Alternative Part',
  'FAULT_CATEGORY' => 'Fault Category',
);