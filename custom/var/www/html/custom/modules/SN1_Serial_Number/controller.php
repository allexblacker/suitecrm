<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class CustomSN1_Serial_NumberController extends SugarController {
	
	public function action_isSerialUsed() {
		global $db;
		
		$sql = "SELECT COUNT(*) AS number
				FROM sn1_serial_number
				WHERE deleted = 0 AND
				serial_number_new LIKE " . $db->quoted($_REQUEST['newSerial']);
		
		$res = $db->fetchOne($sql);
		
		if ($res != null && is_array($res) && count($res) > 0 && $res['number'] > 0)
			echo 1;
		else
			echo 0;
		
		die();
	}
    
}
