<?php
// created: 2017-11-22 13:58:27
$subpanel_layout['list_fields'] = array (
  'subpanel_check_rma_c' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'vname' => 'LBL_CHECK_RMA_CHECKBOX',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'product_old' => 
  array (
    'vname' => 'Product_old',
    'width' => '45%',
    'default' => true,
  ),
  'serial_number_new' => 
  array (
    'type' => 'varchar',
    'vname' => 'Serial_Number_new',
    'width' => '10%',
    'default' => true,
  ),
  'product_new' => 
  array (
    'vname' => 'Product_New',
    'width' => '45%',
    'default' => true,
  ),
  'warranty_end_date' => 
  array (
    'type' => 'varchar',
    'vname' => 'Warranty_End_Date',
    'width' => '10%',
    'default' => true,
  ),
  'reason_for_not_in_warranty' => 
  array (
    'type' => 'varchar',
    'vname' => 'reason_for_not_in_warranty',
    'width' => '10%',
    'default' => true,
  ),
  'ibolt_status' => 
  array (
    'type' => 'varchar',
    'vname' => 'iBolt_Status',
    'width' => '10%',
    'default' => true,
  ),
  'rma' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'RMA',
    'width' => '10%',
  ),
  'rma_alert_c' => 
  array (
    'type' => 'function',
    'studio' => 'visible',
    'vname' => 'LBL_RMA_ALERT',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);