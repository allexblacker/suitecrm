<?php
$module_name = 'SN1_Serial_Number';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'serial_number_new',
            'label' => 'Serial_Number_new',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'product_old',
            'label' => 'Product_old',
          ),
          1 => 
          array (
            'name' => 'product_new',
            'label' => 'Product_New',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'case1',
            'studio' => 'visible',
            'label' => 'Case1',
          ),
          1 => 
          array (
            'name' => 'need_fa_immediate_return',
            'label' => 'Need_FA_immediate_return',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'rml',
            'label' => 'RML',
          ),
          1 => 
          array (
            'name' => 'alternative_ibolt_status',
            'label' => 'Alternative_iBolt_Status',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'fa_by_air',
            'label' => 'FA_By_air',
          ),
          1 => 
          array (
            'name' => 'return_asked_by',
            'studio' => 'visible',
            'label' => 'Return_asked_by',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
