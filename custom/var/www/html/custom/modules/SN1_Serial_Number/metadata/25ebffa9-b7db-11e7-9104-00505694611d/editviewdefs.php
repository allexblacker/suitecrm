<?php
$module_name = 'SN1_Serial_Number';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'product_old',
            'label' => 'Product_old',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'serial_number_new',
            'label' => 'Serial_Number_new',
          ),
          1 => 
          array (
            'name' => 'product_new',
            'label' => 'Product_New',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cases_sn1_serial_number_1_name',
            'label' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
          ),
          1 => 
          array (
            'name' => 'fault_category',
            'studio' => 'visible',
            'label' => 'Fault_Category',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'family',
            'label' => 'Family',
          ),
          1 => 
          array (
            'name' => 'model',
            'studio' => 'visible',
            'label' => 'Model',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'gen',
            'label' => 'Gen',
          ),
          1 => 
          array (
            'name' => 'fault_sub_category',
            'studio' => 'visible',
            'label' => 'Fault_sub_category',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'warrant_extension',
            'label' => 'Warrant_Extension',
          ),
          1 => 
          array (
            'name' => 'fault_description',
            'studio' => 'visible',
            'label' => 'Fault_Description',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'reason_for_not_in_warranty',
            'label' => 'reason_for_not_in_warranty',
          ),
          1 => 
          array (
            'name' => 'details_for_fault_ategory',
            'studio' => 'visible',
            'label' => 'details_for_fault_ategory_other',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'ibolt_status',
            'label' => 'iBolt_Status',
          ),
          1 => 
          array (
            'name' => 'rma',
            'label' => 'RMA',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'rml',
            'label' => 'RML',
          ),
          1 => 
          array (
            'name' => 'distributor_name',
            'label' => 'Distributor_Name',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'need_fa_immediate_return',
            'label' => 'Need_FA_immediate_return',
          ),
          1 => 
          array (
            'name' => 'distributor_number',
            'label' => 'Distributor_Number',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'alternative_ibolt_status',
            'label' => 'Alternative_iBolt_Status',
          ),
          1 => 
          array (
            'name' => 'rma_product',
            'studio' => 'visible',
            'label' => 'RMA_Product',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'return_asked_by',
            'studio' => 'visible',
            'label' => 'Return_asked_by',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
