<?php
$module_name = 'SN1_Serial_Number';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cases_sn1_serial_number_1_name',
            'label' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
          ),
          1 => 
          array (
            'name' => 'fault_category',
            'studio' => 'visible',
            'label' => 'Fault_Category',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'rma_product',
            'studio' => 'visible',
            'label' => 'RMA_Product',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
