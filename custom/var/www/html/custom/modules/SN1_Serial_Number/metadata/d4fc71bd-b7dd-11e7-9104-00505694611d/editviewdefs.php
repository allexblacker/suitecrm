<?php
$module_name = 'SN1_Serial_Number';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'serial_number_new',
            'label' => 'Serial_Number_new',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'product_old',
            'label' => 'Product_old',
          ),
          1 => 
          array (
            'name' => 'product_new',
            'label' => 'Product_New',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cases_sn1_serial_number_1_name',
            'label' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
          ),
          1 => 
          array (
            'name' => 'need_fa_immediate_return',
            'label' => 'Need_FA_immediate_return',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'rml',
            'label' => 'RML',
          ),
          1 => 
          array (
            'name' => 'alternative_ibolt_status',
            'label' => 'Alternative_iBolt_Status',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'fa_by_air',
            'label' => 'FA_By_air',
          ),
          1 => 
          array (
            'name' => 'return_asked_by',
            'studio' => 'visible',
            'label' => 'Return_asked_by',
          ),
        ),
      ),
    ),
  ),
);
?>
