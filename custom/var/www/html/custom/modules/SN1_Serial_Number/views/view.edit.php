<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/JSLoader.php');
require_once('custom/include/dtbc/TplCalculator.php');

class SN1_Serial_NumberViewEdit extends ViewEdit {
	
	function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();
		
		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

        $this->ev = $this->getEditView();
        $this->ev->ss =& $this->ss;
        $this->ev->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/EditView/EditView.tpl'));
	}

    function display() {
        parent::display();
		
		includeJavaScriptFile("custom/modules/SN1_Serial_Number/js/editview.js");
		echo "<input type='hidden' id='createddateprodnew' value='" . $this->dateIsBiggerThan("2016-07-10") . "' />";
		
    }
	
	function dateIsBiggerThan($compareDate) {
		if (isset($_REQUEST['record']) && !empty($_REQUEST['record']) && strlen($_REQUEST['record']) > 0) {
			$createdDate = new DateTime($this->bean->date_entered);	
			$oldDate = new DateTime($compareDate);
			return $oldDate < $createdDate ? 1 : 0;
		}
		
		return "1";
	}
	
}
