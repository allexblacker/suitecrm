<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getAltParts($params) {
    $db = DBManagerFactory::getInstance();

    $sql = "SELECT ap1_alternative_part.*
            FROM ap1_alternative_part
            JOIN ap1_alternative_part_cstm ON ap1_alternative_part.id = ap1_alternative_part_cstm.id_c AND sn1_serial_number_id_c = " . $db->quoted($params['serial_id']) . 
            "WHERE ap1_alternative_part.deleted = 0";
    return $sql;
}
