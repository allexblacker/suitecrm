<?php 
 //WARNING: The contents of this file are auto-generated



if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['SN1_Serial_Number']['fields']['case1']['rname'] = 'case_number';

$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1_name"]['rname'] = 'case_number';

 // created: 2017-10-16 15:10:46

 

 // created: 2017-09-12 15:47:13
$dictionary['SN1_Serial_Number']['fields']['part_description']['len']='60';

 

 // created: 2017-11-21 17:00:45
$dictionary['SN1_Serial_Number']['fields']['fault_description']['options']='error_event_code_list';

 

 // created: 2017-10-16 14:43:11

 

 // created: 2017-09-26 17:28:30

 

 // created: 2017-10-16 14:42:52

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['SN1_Serial_Number']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

$dictionary['SN1_Serial_Number']['fields']['subpanel_check_rma_c'] = array(
	'name' => 'subpanel_check_rma_c',
	'vname' => 'LBL_CHECK_RMA_CHECKBOX',
	'label' => 'LBL_CHECK_RMA_CHECKBOX',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-11-15 17:33:56

 


$dictionary["SN1_Serial_Number"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-11-21 10:16:50
$dictionary['SN1_Serial_Number']['fields']['fault_category']['default']='None';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['SN1_Serial_Number']['fields']['rma_alert_c'] = array(
	'required' => false,
	'name' => 'rma_alert_c',
	'vname' => 'LBL_RMA_ALERT',
	'type' => 'function',
	'source' => 'non-db',
	'massupdate' => 0,
	'studio' => 'visible',
	'importable' => 'false',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => 0,
	'audited' => false,
	'reportable' => false,
	'inline_edit' => 0,
	'function' => array(
		'name' => 'dtbc_display_rma_alert_image',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/rma_alert_c.php',
	),
);

 // created: 2017-10-24 16:19:32
$dictionary['SN1_Serial_Number']['fields']['case1']['required']=true;

 

 // created: 2017-09-28 15:12:58
$dictionary['SN1_Serial_Number']['fields']['need_immediate_return_time']['display_default']='now';

 

 // created: 2017-11-21 10:14:31
$dictionary['SN1_Serial_Number']['fields']['fault_sub_category']['default']='None';

 

 // created: 2017-10-08 18:28:38
$dictionary['SN1_Serial_Number']['fields']['name']['required']=true;
$dictionary['SN1_Serial_Number']['fields']['name']['full_text_search']=array (
);

 

// created: 2017-09-11 18:28:20
$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1"] = array (
  'name' => 'cases_sn1_serial_number_1',
  'type' => 'link',
  'relationship' => 'cases_sn1_serial_number_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
  'id_name' => 'cases_sn1_serial_number_1cases_ida',
);
$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1_name"] = array (
  'name' => 'cases_sn1_serial_number_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_sn1_serial_number_1cases_ida',
  'link' => 'cases_sn1_serial_number_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1cases_ida"] = array (
  'name' => 'cases_sn1_serial_number_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_sn1_serial_number_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE',
);


 // created: 2017-10-16 14:43:26

 

 // created: 2017-11-15 17:25:11

 

 // created: 2017-11-21 13:00:09
$dictionary['SN1_Serial_Number']['fields']['model']['required']=false;

 
?>