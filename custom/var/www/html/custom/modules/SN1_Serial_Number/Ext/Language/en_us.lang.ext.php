<?php 
 //WARNING: The contents of this file are auto-generated


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE'] = 'Cases';

 

$mod_strings['LBL_ERR_ATTACH_BOARDING'] = 'IMPORTANT: adding a board configuration file is mandatory when the customer uses his own stock (service kit), please tick the field:"Attaching Board config file is mandatory" for acknowledging this alert.';
$mod_strings['LBL_SAVEBUTTON'] = 'Save';
$mod_strings['LBL_ERR_FAULTDESCRIPTION'] = 'Please insert a value in Details for Fault sub-category – Other';
$mod_strings['LBL_ERR_FAULTSUBCATEGORY'] = 'Please elaborate on fault sub-category';
$mod_strings['LBL_ERR_PARTNUMBERREQ'] = 'Please insert a value in Part Number New before marking this serial for RMA';
$mod_strings['LBL_ERR_RETURNASKEDBY'] = 'Please select a value in the field: Return Asked By';
$mod_strings['LBL_ERR_RMAPRODREQ'] = 'Please select a value in the field RMA Product';
$mod_strings["LBL_ERR_SERIAL_IS_USED"] = "This serial is already in use";

$mod_strings["LBL_LISTVIEW_GROUP"] = "Security Group";
$mod_strings["LBL_LISTVIEW_ENVELOPE"] = " ";
$mod_strings['LBL_SEARCHFORM_SECURITY_GROUP'] = 'Security Group';
$mod_strings['LBL_RMA_ALERT'] = 'RMA Alert';

$mod_strings['LBL_CHECK_RMA'] = 'Check RMA';
$mod_strings['LBL_CHECK_RMA_CHECKBOX'] = ' ';



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$mod_strings['LBL_ALT_PART'] = 'Alternative Part';

?>