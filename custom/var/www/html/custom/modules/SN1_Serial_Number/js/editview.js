function dtbc_IsSerialUsedBefore() {
	$.ajax({
		method: "POST",
		url: "index.php?module=SN1_Serial_Number&action=isSerialUsed&to_pdf=1",
		data: { 
			newSerial: $("#serial_number_new").val(),
		},
	})
	.done(function (response) {
		if (response == "1" && !$('#NewSerialIsInUse').length) {
			$('div [field=serial_number_new]').append("<div class='required validation-message' id='NewSerialIsInUse'>" + SUGAR.language.get('SN1_Serial_Number','LBL_ERR_SERIAL_IS_USED') + "</div>");
		} else if (response == "0" && $('#NewSerialIsInUse').length) {
			$('#NewSerialIsInUse').remove();
		}
	})
	.fail(function (e) {
		console.log(e);
	});
}

function dtbc_PartNumberRequired(formName) {
	if ($('#rma_product').val() == 'Accessories' && $('#serial_number_new').val().length > 0) {
		addRequired('product_new', formName);
	} else {
		removeRequired('product_new', formName);
	}
}

function Attaching_Board_config_Replaced_Type(formName) {
	if ($("#attaching_board_config_file").length && $("#rma_product").length && $("#replaced_type").length) {
		if (!$("#attaching_board_config_file").prop('checked') && $("#rma_product").val() == 'Board' && ($("#replaced_type").val() == '1' || $("#replaced_type").val() == '2')) {
			$("#attaching_board_config_file").after("&nbsp;&nbsp;<span style='color: #FF0000'>" + SUGAR.language.get('SN1_Serial_Number','LBL_ERR_ATTACH_BOARDING') + "</span>");
			return false;
		}
	}
	
	return true;
}

function Details_for_Fault_sub_category_Other_m(formName) {
	if ($("#fault_category").length && $("#fault_sub_category").length && $("#details_for_fault_ategory").length && $("#fault_description").length) {
	
		var subCat = $("#fault_sub_category").val();
		
		if ($("#fault_category").val() == 'Home Automation' &&
			$("#details_for_fault_ategory").val().length == 0 &&
			$("#fault_description").val() == 'Other' &&
			(subCat == 'Damaged goods' ||
			subCat == 'Immersion heater controller' ||
			subCat == 'Plug' ||
			subCat == 'AC switch' ||
			subCat == 'AC dry contact'
			)
		) {
			add_error_style_custom(formName, 'details_for_fault_ategory', SUGAR.language.get('SN1_Serial_Number','LBL_ERR_FAULTDESCRIPTION'));
			return false;
		}
	}
	
	return true;
}

function other_fault_sub_category(formName) {
	if ($("#fault_sub_category").length && $("#details_for_fault_ategory").length) {
	
		var subCat = $("#fault_sub_category").val();
		
		if ($("#fault_sub_category").val() == 'Other' && $("#details_for_fault_ategory").val().length == 0) {
			add_error_style_custom(formName, 'details_for_fault_ategory', SUGAR.language.get('SN1_Serial_Number','LBL_ERR_FAULTSUBCATEGORY'));
			return false;
		}
	}
	
	return true;
}

function Part_Number_New_mandatory(formName) {
	if ($("#product_new").length && $("#rma").length) {
		if ($("#product_new").val().length == 0  && $("#rma").prop('checked') && $("#createddateprodnew").val() == 1) {
			add_error_style_custom(formName, 'product_new', SUGAR.language.get('SN1_Serial_Number','LBL_ERR_PARTNUMBERREQ'));
			return false;
		}
	}
	
	return true;
}

function return_asked_by_is_mandatory_when_FA_tru(formName) {
	if ($("#return_asked_by").length && $("#need_fa_immediate_return").length) {
		if ($("#return_asked_by").val().length == 0  && $("#need_fa_immediate_return").prop('checked')) {
			add_error_style_custom(formName, 'return_asked_by', SUGAR.language.get('SN1_Serial_Number','LBL_ERR_RETURNASKEDBY'));
			return false;
		}
	}
	
	return true;
}

function RMA_Product_is_mandatory_if_reason_inver(formName) {
	if ($("#fault_category").length && $("#rma_product").length) {
		if ($("#fault_category").val() == 'Inverter' && $("#rma_product").val().length == 0) {
			add_error_style_custom(formName, 'rma_product', SUGAR.language.get('SN1_Serial_Number','LBL_ERR_RMAPRODREQ'));
			return false;
		}
	}
	
	return true;
}

function addRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex == -1) {
			$().html(current.substring(0, myIndex));
			$('[dtbc-data=' + labelName + ']').append('<span class="required">*</span>');
			addToValidate(formName, fieldName, fieldName, true);
		}
	}	
}

function removeRequired(fieldName, formName) {
	if ($("#" + fieldName).length) {
		var labelName = fieldName + "_label";
		var current = $('[dtbc-data=' + labelName + ']').html();
		if (current == undefined)
			console.log(fieldName);
			
		var myIndex = current.indexOf("<span");
		if (myIndex != -1) {
			$('[dtbc-data=' + labelName + ']').html(current.substring(0, myIndex));
			removeFromValidate(formName, fieldName);
		}
	}
}

function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation() {
	clear_all_errors();
	var formName = 'EditView';
	
	var retval = check_form(formName);
	retval = calcRetval(retval, Attaching_Board_config_Replaced_Type(formName));
	retval = calcRetval(retval, Details_for_Fault_sub_category_Other_m(formName));
	retval = calcRetval(retval, other_fault_sub_category(formName));
	retval = calcRetval(retval, Part_Number_New_mandatory(formName));
	retval = calcRetval(retval, return_asked_by_is_mandatory_when_FA_tru(formName));
	retval = calcRetval(retval, RMA_Product_is_mandatory_if_reason_inver(formName));
	
	if (retval){
		var _form = document.getElementById(formName);
		_form.action.value = 'Save';
		SUGAR.ajaxUI.submitForm(_form);
		return true;
	}
	
	scrollToError();
	return false;
}

$(document).ready(function() {
	jQuery.getScript('custom/include/dtbc/js/scrollingAfterAddingErrorStyle.js');
	var formName = "EditView";
	
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	$("<input title='" + SUGAR.language.get('SN1_Serial_Number','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation();' type='button' name='button' value='" + SUGAR.language.get('SN1_Serial_Number','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
	
	$("#serial_number_new").blur(function() {
		dtbc_PartNumberRequired(formName);
		dtbc_IsSerialUsedBefore();
	});
	
	$("#rma_product").change(function() {
		dtbc_PartNumberRequired(formName);
	});
});