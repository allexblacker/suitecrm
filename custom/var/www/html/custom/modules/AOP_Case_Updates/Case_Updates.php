<?php

function display_updates_custom($focus)
{
    global $mod_strings, $sugar_config;

    $hideImage = SugarThemeRegistry::current()->getImageURL('basic_search.gif');
    $showImage = SugarThemeRegistry::current()->getImageURL('advanced_search.gif');
	$siteUrl = $sugar_config['site_url'];
	$recordId = $focus->id;

    //Javascript for Asynchronous update
    $html = <<<A
<script>
var hideUpdateImage = '$hideImage';
var showUpdateImage = '$showImage';
function collapseAllUpdates(){
    $('.caseUpdateImage').attr("src",showUpdateImage);
    $('.caseUpdate').slideUp('fast');
}
function expandAllUpdates(){
    $('.caseUpdateImage').attr("src",hideUpdateImage);
    $('.caseUpdate').slideDown('fast');
}
function toggleCaseUpdate(updateId){
    var id = 'caseUpdate'+updateId;
    var updateElem = $('#'+id);
    var imageElem = $('#'+id+"Image");

    if(updateElem.is(":visible")){
        imageElem.attr("src",showUpdateImage);
    }else{
        imageElem.attr("src",hideUpdateImage);
    }
    updateElem.slideToggle('fast');
}
function caseUpdates2(record) {
    // Calculate internal message settings
	var chgtatus_cb = $("#changestatus").prop("checked");

	var internal_cb = $("#internal2").prop("checked");

	var sendnoti_cb = $("#sendnotification").prop("checked");

	// If not internal and not required to send email, then it's internal
	if (!internal_cb && !sendnoti_cb)
		internal_cb = true;
    
	if ($('#update_text_input').val().length == 0 && chgtatus_cb) {
		window.location.href = "$siteUrl/index.php?action=changeStatus&module=Cases&record=" + record;
		return;
	}
	
	var mydiv = $('<div />').appendTo('body');
	mydiv.attr('id', 'myloading');
	mydiv.attr('style', 'top:0px;left:0px;position:fixed;height:100%;width:100%;background:#000000;opacity:0.5;display:block;vertical-align:middle;text-align:center;z-index:9998;cursor:wait');

	$.ajax({
        method: "POST",
        url: "index.php",
        data: {
            'record': 		 record,
			'module': 		 'Cases',
			'return_module': 'Cases',
			'action': 		 'Save',
			'return_id': 	 record,
			'return_action': 'DetailView',
			'relate_to': 	 'Cases',
			'relate_id': 	 record,
			'offset': 		 1,
			'update_text': 	 $('#update_text_input').val(),
			'internal': 	 internal_cb,
			'sendnoti':		 sendnoti_cb,
			'changestatus':	 chgtatus_cb,
        }
    })
	.done(function (datastring) {
		window.location.reload(false);
	})
	.fail(function (e) {
	    console.log(e);
		$('#myloading').hide();
	});
	
}
</script>
A;

    $updates = $focus->get_linked_beans('aop_case_updates', 'AOP_Case_Updates');
    if (!$updates || is_null($focus->id)) {
        $html .= quick_edit_case_updates2($focus);

        return $html;
    }

    $html .= <<<EOD
<script>
$(document).ready(function(){
    collapseAllUpdates();
    var id = $('.caseUpdate').last().attr('id');
    if(id){
        toggleCaseUpdate(id.replace('caseUpdate',''));
    }
});
</script>
<a href='' onclick='collapseAllUpdates(); return false;'>{$mod_strings['LBL_CASE_UPDATES_COLLAPSE_ALL']}</a>
<a href='' onclick='expandAllUpdates(); return false;'>{$mod_strings['LBL_CASE_UPDATES_EXPAND_ALL']}</a>
<div>
EOD;

    usort(
        $updates,
        function ($a, $b) {
            $aDate = $a->fetched_row['date_entered'];
            $bDate = $b->fetched_row['date_entered'];
            if ($aDate < $bDate) {
                return -1;
            } elseif ($aDate > $bDate) {
                return 1;
            }

            return 0;
        }
    );

    foreach ($updates as $update) {
        $html .= display_single_update2($update, $hideImage);
    }
    $html .= '</div>';
    $html .= quick_edit_case_updates2($focus);

    return $html;
}

/**
 * @param SugarBean $update
 *
 * @return string - html to be displayed
 */
function getUpdateDisplayHead2(SugarBean $update)
{
    global $mod_strings;
    if ($update->contact_id) {
        $name = $update->getUpdateContact()->name;
    } elseif ($update->assigned_user_id) {
        $name = $update->getUpdateUser()->name;
    } else {
        $name = 'Unknown';
    }
    $html = "<a href='' onclick='toggleCaseUpdate(\"" . $update->id . "\");return false;'>";
    $html .= "<img  id='caseUpdate" .
             $update->id .
             "Image' class='caseUpdateImage' src='" .
             SugarThemeRegistry::current()->getImageURL('basic_search.gif') .
             "'>";
    $html .= '</a>';
    $html .= '<span>' .
			 ($update->internal ? '<strong>' . $mod_strings['LBL_INTERNAL_CUSTOM'] . '</strong> ' : '') .
             $name .
             ' ' .
             $update->date_entered .
             '</span><br>';
    $notes = $update->get_linked_beans('notes', 'Notes');
    if ($notes) {
        $html .= $mod_strings['LBL_AOP_CASE_ATTACHMENTS'];
        foreach ($notes as $note) {
            $html .= "<a href='index.php?module=Notes&action=DetailView&record={$note->id}'>{$note->filename}</a>&nbsp;";
        }
    }

    return $html;
}

/**
 * Gets a single update and returns it.
 *
 * @param AOP_Case_Updates $update
 *
 * @return string - the html for the update
 */
function display_single_update2(AOP_Case_Updates $update)
{

    /*if assigned user*/
    if ($update->assigned_user_id) {
        /*if internal update*/
        if ($update->internal) {
            $html = "<div id='caseStyleInternal'>" . getUpdateDisplayHead2($update);
            $html .= "<div id='caseUpdate" . $update->id . "' class='caseUpdate'>";
            $html .= nl2br(html_entity_decode($update->description));
            $html .= '</div></div>';

            return $html;
        } /*if standard update*/ else {
            $html = "<div id='lessmargin'><div id='caseStyleUser'>" . getUpdateDisplayHead2($update);
            $html .= "<div id='caseUpdate" . $update->id . "' class='caseUpdate'>";
            $html .= nl2br(html_entity_decode($update->description));
            $html .= '</div></div></div>';

            return $html;
        }
    }

    /*if contact user*/
    if ($update->contact_id) {
        $html = "<div id='extramargin'><div id='caseStyleContact'>" . getUpdateDisplayHead2($update);
        $html .= "<div id='caseUpdate" . $update->id . "' class='caseUpdate'>";
        $html .= nl2br(html_entity_decode($update->description));
        $html .= '</div></div></div>';

        return $html;
    }
}


/**
 * The Quick edit for case updates which appears under update stream
 * Also includes the javascript for AJAX update.
 *
 * @param $case
 *
 * @return string - the html to be displayed and javascript
 */
function quick_edit_case_updates2($case)
{
    global $action, $app_strings, $mod_strings;

    //on DetailView only
    if ($action !== 'DetailView') {
        return;
    }

    //current record id
    $record = $_GET['record'];

    //Get Users roles
    require_once 'modules/ACLRoles/ACLRole.php';
    $user = $GLOBALS['current_user'];
    $id = $user->id;
    $acl = new ACLRole();
    $roles = $acl->getUserRoles($id);

    //Return if user cannot edit cases
    if ($roles === 'no edit cases' || in_array('no edit cases', $roles)) {
        return '';
    }
    
    $saveBtn = $app_strings['LBL_SAVE_BUTTON_LABEL'];
    $saveTitle = $app_strings['LBL_SAVE_BUTTON_TITLE'];

    $html = <<< EOD
    <form id='case_updates' enctype="multipart/form-data">

    <div><label for="update_text_input">{$mod_strings['LBL_UPDATE_TEXT']}</label></div>
    <textarea id="update_text_input" name="update_text" cols="80" rows="4"></textarea>

    <div><label>{$mod_strings['LBL_INTERNAL_CUSTOM']}</label>
    <input id='internal2' type='checkbox' name='internal' tabindex=0 title='' value='1' checked />
    </div>
	<div><label>{$mod_strings['LBL_SEND_NOTIFICATION']}</label>
    <input id='sendnotification' type='checkbox' name='sendnotification' tabindex=0 title='' value='1' />
    </div>
	<div><label>{$mod_strings['LBL_CHANGE_CASE_STATUS']}</label>
    <input id='changestatus' type='checkbox' name='changestatus' tabindex=0 title='' value='1' />
    </div>
    <input type='button' value='$saveBtn' onclick="caseUpdates2('$record')" title="$saveTitle" name="button"/>


    </br>
    </form>


EOD;

    return $html;
}
