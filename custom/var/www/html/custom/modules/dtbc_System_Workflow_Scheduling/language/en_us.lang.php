<?php
// created: 2017-10-20 10:31:05
$mod_strings = array (
  'LBL_CASE_ID' => 'Case',
  'LBL_TRIGGER_TIME' => 'Trigger time',
  'LBL_TRIGGERED' => 'Triggered',
  'LBL_FLEX_RELATE' => 'Related bean',
);