<?php

require_once('modules/Contacts/Contact.php');
require_once('custom/include/dtbc/helpers.php');

class CustomContacts extends Contact {
	
	public function create_new_list_query($order_by, $where,$filter=array(),$params=array(), $show_deleted = 0,$join_type='', $return_array = false,$parentbean=null, $singleSelect = false, $ifListForExport = false) {	
		$retval = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);
	
		if (!isset($_REQUEST['accountId']))
			return $retval;
		
		global $db;
		// There is a selected Account, we need to display only the contacts related to the account
		if (strlen($retval['where']) > 0)
			$retval['where'] .= " AND";
		$retval['where'] .= " accounts.id = " . $db->quoted($_REQUEST['accountId']);
		
		return $retval;
	}
	
}

?>