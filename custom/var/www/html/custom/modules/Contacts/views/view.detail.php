<?php

require_once('include/MVC/View/views/view.detail.php');
require_once('custom/include/dtbc/TplCalculator.php');
require_once("custom/include/dtbc/helpers.php");

/**
 * Default view class for handling DetailViews
 *
 * @package MVC
 * @category Views
 */
class ContactsViewDetail extends ViewDetail {
    
	public function preDisplay() {
		$metadataFile = TplCalculator::getOneDimensionView();

		if ($metadataFile == null) {
			$metadataFile = $this->getMetaDataFile();
		}

 	    $this->dv = new DetailView2();
 	    $this->dv->ss =&  $this->ss;
        
		$fieldsToHTMLDecode = array("cont_type_acc_service_is_mi_c","certified_contact_c","google_search_c","linkedin_search_c","location_c","xing_search_c");
		foreach($fieldsToHTMLDecode as $field){
			$this->bean->$field = html_entity_decode($this->bean->$field);
		}
		
 	    $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/DetailView/DetailView.tpl'));
    }
	
}
