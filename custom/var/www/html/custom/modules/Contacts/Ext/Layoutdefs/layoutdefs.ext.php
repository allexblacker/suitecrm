<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-25 18:21:08
$layout_defs["Contacts"]["subpanel_setup"]['contacts_p1_project_1'] = array (
  'order' => 100,
  'module' => 'P1_Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
  'get_subpanel_data' => 'contacts_p1_project_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);




unset($layout_defs['Contacts']['subpanel_setup']['bugs']);
unset($layout_defs['Contacts']['subpanel_setup']['project']);



 // created: 2017-09-08 20:40:19
$layout_defs["Contacts"]["subpanel_setup"]['contacts_dtbc_casessurvey_1'] = array (
  'order' => 100,
  'module' => 'dtbc_CasesSurvey',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
  'get_subpanel_data' => 'contacts_dtbc_casessurvey_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

	$layout_defs['Contacts']['subpanel_setup']['lms'] = array(
		'order' => 100,
		'module' => 'TLA1_Training_LMS_Absorb',
		'get_subpanel_data'=>'function:getTrainingLMS',
		'sort_order' => 'asc',
		'sort_by' => 'training_name',
		'subpanel_name' => 'default',
		'title_key' => 'LBL_LMS', 
		'generate_select' => false,
		'function_parameters' => array(
			'import_function_file' => 'custom/modules/Contacts/contactSubPanels.php',
			'contact_id' => $this->_focus->id,
			//'sex' => 'Male',
		)
	);

 // created: 2017-10-04 15:26:52
$layout_defs["Contacts"]["subpanel_setup"]['contacts_dtbc_contact_roles_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Contact_Roles',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
  'get_subpanel_data' => 'contacts_dtbc_contact_roles_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-11-09 11:47:52
$layout_defs["Contacts"]["subpanel_setup"]['contacts_fsk1_field_service_kit_1'] = array (
  'order' => 100,
  'module' => 'FSK1_Field_Service_Kit',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
  'get_subpanel_data' => 'contacts_fsk1_field_service_kit_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['cases']['override_subpanel_name'] = 'Contact_subpanel_cases';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_fsk1_field_service_kit_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_fsk1_field_service_kit_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_p1_project_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_p1_project_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['lms']['override_subpanel_name'] = 'Contact_subpanel_lms';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['opportunities']['override_subpanel_name'] = 'Contact_subpanel_opportunities';

?>