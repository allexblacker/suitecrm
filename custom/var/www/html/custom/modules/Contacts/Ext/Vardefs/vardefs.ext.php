<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-10-26 21:10:33
$dictionary['Contact']['fields']['primary_address_street']['len']='255';
$dictionary['Contact']['fields']['primary_address_street']['inline_edit']=true;
$dictionary['Contact']['fields']['primary_address_street']['comments']='Street address for primary address';
$dictionary['Contact']['fields']['primary_address_street']['merge_filter']='disabled';

 

 // created: 2017-08-21 12:47:10
$dictionary['Contact']['fields']['state_c']['inline_edit']='1';
$dictionary['Contact']['fields']['state_c']['labelValue']='State';

 

 // created: 2017-10-26 21:08:18
$dictionary['Contact']['fields']['alt_address_street']['len']='255';
$dictionary['Contact']['fields']['alt_address_street']['inline_edit']=true;
$dictionary['Contact']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Contact']['fields']['alt_address_street']['merge_filter']='disabled';

 

 // created: 2017-10-09 16:21:33
$dictionary['Contact']['fields']['certified_contact_c']['inline_edit']='1';
$dictionary['Contact']['fields']['certified_contact_c']['labelValue']='Certified Contact';

 

 // created: 2017-08-14 15:51:58
$dictionary['Contact']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-08-14 15:51:59
$dictionary['Contact']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-10-19 15:32:28
$dictionary['Contact']['fields']['mobile_c']['inline_edit']='1';
$dictionary['Contact']['fields']['mobile_c']['labelValue']='Mobile';

 

 // created: 2017-10-19 15:30:20
$dictionary['Contact']['fields']['description_c']['inline_edit']='1';
$dictionary['Contact']['fields']['description_c']['labelValue']='Description';

 

 // created: 2017-10-09 16:22:43
$dictionary['Contact']['fields']['xing_search_c']['inline_edit']='1';
$dictionary['Contact']['fields']['xing_search_c']['labelValue']='Xing Search';

 

 // created: 2017-10-19 15:31:17
$dictionary['Contact']['fields']['home_phone_c']['inline_edit']='1';
$dictionary['Contact']['fields']['home_phone_c']['labelValue']='Home Phone';

 

 // created: 2017-10-19 15:33:37
$dictionary['Contact']['fields']['leadsource_c']['inline_edit']='1';
$dictionary['Contact']['fields']['leadsource_c']['labelValue']='Lead Source';

 

// created: 2017-09-25 18:21:08
$dictionary["Contact"]["fields"]["contacts_p1_project_1"] = array (
  'name' => 'contacts_p1_project_1',
  'type' => 'link',
  'relationship' => 'contacts_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);


 // created: 2017-10-19 15:33:16
$dictionary['Contact']['fields']['certification_level_c']['inline_edit']='1';
$dictionary['Contact']['fields']['certification_level_c']['labelValue']='Certification Level';

 

 // created: 2017-10-09 16:21:45
$dictionary['Contact']['fields']['google_search_c']['inline_edit']='1';
$dictionary['Contact']['fields']['google_search_c']['labelValue']='Google Search';

 

 // created: 2017-08-14 15:51:58
$dictionary['Contact']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-10-25 10:41:59
$dictionary['Contact']['fields']['locale_c']['inline_edit']='1';
$dictionary['Contact']['fields']['locale_c']['labelValue']='Locale';

 

 // created: 2017-10-19 15:30:59
$dictionary['Contact']['fields']['phone_c']['inline_edit']='1';
$dictionary['Contact']['fields']['phone_c']['labelValue']='Phone';

 

 // created: 2017-10-09 16:20:53
$dictionary['Contact']['fields']['cont_type_acc_service_is_mi_c']['inline_edit']='1';
$dictionary['Contact']['fields']['cont_type_acc_service_is_mi_c']['labelValue']='Account Service lvl/Contact Type missing';

 

 // created: 2017-09-27 15:40:58
$dictionary['Contact']['fields']['receive_blog_update_c']['inline_edit']='1';
$dictionary['Contact']['fields']['receive_blog_update_c']['labelValue']='Receive Blog Update';

 

 // created: 2017-10-19 15:34:34
$dictionary['Contact']['fields']['campaign_score_c']['inline_edit']='1';
$dictionary['Contact']['fields']['campaign_score_c']['labelValue']='Campaign Score';

 

 // created: 2017-10-19 15:33:57
$dictionary['Contact']['fields']['lead_score_c']['inline_edit']='1';
$dictionary['Contact']['fields']['lead_score_c']['labelValue']='Lead Score';

 

 // created: 2017-10-20 15:14:44
$dictionary['Contact']['fields']['w_9_c']['inline_edit']='1';
$dictionary['Contact']['fields']['w_9_c']['labelValue']='W-9';

 

 // created: 2017-10-19 15:30:02
$dictionary['Contact']['fields']['city_c']['inline_edit']='1';
$dictionary['Contact']['fields']['city_c']['labelValue']='City';

 

 // created: 2017-10-19 15:36:53
$dictionary['Contact']['fields']['newsletter_c']['inline_edit']='1';
$dictionary['Contact']['fields']['newsletter_c']['labelValue']='Newsletter';

 

 // created: 2017-10-09 16:21:14
$dictionary['Contact']['fields']['allow_loyalty_plan_email_c']['inline_edit']='1';
$dictionary['Contact']['fields']['allow_loyalty_plan_email_c']['labelValue']='Allow Loyalty Plan email';

 

 // created: 2017-10-19 15:30:39
$dictionary['Contact']['fields']['fax_c']['inline_edit']='1';
$dictionary['Contact']['fields']['fax_c']['labelValue']='Fax';

 

// created: 2017-09-08 20:40:19
$dictionary["Contact"]["fields"]["contacts_dtbc_casessurvey_1"] = array (
  'name' => 'contacts_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'dtbc_CasesSurvey',
  'bean_name' => 'dtbc_CasesSurvey',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);


 // created: 2017-10-19 15:34:53
$dictionary['Contact']['fields']['certification_expiry_date_c']['inline_edit']='1';
$dictionary['Contact']['fields']['certification_expiry_date_c']['labelValue']='Certification expiry date';

 

 // created: 2017-08-29 12:59:15
$dictionary['Contact']['fields']['converted_lead_c']['inline_edit']='1';
$dictionary['Contact']['fields']['converted_lead_c']['labelValue']='Converted Lead';

 

 // created: 2017-08-14 15:51:58
$dictionary['Contact']['fields']['guid_c']['inline_edit']='1';
$dictionary['Contact']['fields']['guid_c']['labelValue']='GUID';

 

// created: 2017-10-04 15:26:52
$dictionary["Contact"]["fields"]["contacts_dtbc_contact_roles_1"] = array (
  'name' => 'contacts_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'dtbc_Contact_Roles',
  'bean_name' => 'dtbc_Contact_Roles',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);



if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Contact']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-08-24 10:08:50
$dictionary['Contact']['fields']['phone_work']['required']=true;
$dictionary['Contact']['fields']['phone_work']['inline_edit']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';

 

 // created: 2017-10-19 15:36:33
$dictionary['Contact']['fields']['poc_technical_c']['inline_edit']='1';
$dictionary['Contact']['fields']['poc_technical_c']['labelValue']='POC Technical';

 

 // created: 2017-10-24 16:08:09
$dictionary['Contact']['fields']['first_name']['required']=true;
$dictionary['Contact']['fields']['first_name']['inline_edit']=true;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';

 


$dictionary["Contact"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-10-19 15:36:15
$dictionary['Contact']['fields']['certified_installer_c']['inline_edit']='1';
$dictionary['Contact']['fields']['certified_installer_c']['labelValue']='Certified installer';

 

 // created: 2017-11-20 11:51:55
$dictionary['Contact']['fields']['location_c']['inline_edit']='1';
$dictionary['Contact']['fields']['location_c']['labelValue']='Location';

 


$dictionary["Contact"]["fields"]["add_new_contact_c"] = array(
	'name' => 'add_new_contact_c',
	'label' => 'LBL_FUNCTION_ADD_NEW_CONTACT',
	'vname' => 'LBL_FUNCTION_ADD_NEW_CONTACT',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_AddContactButton',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/contactsFunctionFields.php',
	),
);

 // created: 2017-10-19 15:29:31
$dictionary['Contact']['fields']['street_c']['inline_edit']='1';
$dictionary['Contact']['fields']['street_c']['labelValue']='Street';

 

 // created: 2017-10-19 15:32:59
$dictionary['Contact']['fields']['type_c']['inline_edit']='1';
$dictionary['Contact']['fields']['type_c']['labelValue']='Type';

 

 // created: 2017-10-24 16:11:54
$dictionary['Contact']['fields']['email_c']['inline_edit']='1';
$dictionary['Contact']['fields']['email_c']['labelValue']='Email';

 

 // created: 2017-10-09 16:22:10
$dictionary['Contact']['fields']['linkedin_search_c']['inline_edit']='1';
$dictionary['Contact']['fields']['linkedin_search_c']['labelValue']='Linkedin Search';

 

 // created: 2017-10-24 16:08:02
$dictionary['Contact']['fields']['last_name']['required']=true;
$dictionary['Contact']['fields']['last_name']['inline_edit']=true;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']='1';

 

 // created: 2017-11-08 11:20:59
$dictionary['Contact']['fields']['acton_lead_score_c']['inline_edit']=1;

 

 // created: 2017-08-24 10:08:13
$dictionary['Contact']['fields']['name_c']['inline_edit']='1';
$dictionary['Contact']['fields']['name_c']['labelValue']='Name';

 

 // created: 2017-10-19 15:28:19
$dictionary['Contact']['fields']['zip_postal_code_c']['inline_edit']='1';
$dictionary['Contact']['fields']['zip_postal_code_c']['labelValue']='Zip Postal Code';

 

 // created: 2017-10-19 15:36:00
$dictionary['Contact']['fields']['poc_commercial_c']['inline_edit']='1';
$dictionary['Contact']['fields']['poc_commercial_c']['labelValue']='POC Commercial';

 

 // created: 2017-08-24 10:11:21
$dictionary['Contact']['fields']['email1']['audited']=true;
$dictionary['Contact']['fields']['email1']['inline_edit']=true;
$dictionary['Contact']['fields']['email1']['merge_filter']='disabled';
$dictionary['Contact']['fields']['email1']['required']=true;

 

 // created: 2017-10-19 15:35:40
$dictionary['Contact']['fields']['master_contact_c']['inline_edit']='1';
$dictionary['Contact']['fields']['master_contact_c']['labelValue']='Master contact';

 

 // created: 2017-10-19 15:28:05
$dictionary['Contact']['fields']['contact_support_comments_c']['inline_edit']='1';
$dictionary['Contact']['fields']['contact_support_comments_c']['labelValue']='Contact Support Comments';

 

 // created: 2017-08-14 15:51:58
$dictionary['Contact']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

// created: 2017-11-09 11:47:52
$dictionary["Contact"]["fields"]["contacts_fsk1_field_service_kit_1"] = array (
  'name' => 'contacts_fsk1_field_service_kit_1',
  'type' => 'link',
  'relationship' => 'contacts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'module' => 'FSK1_Field_Service_Kit',
  'bean_name' => 'FSK1_Field_Service_Kit',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
);


// created: 2017-11-03 23:37:27
$dictionary["Contact"]["fields"]["is1_inside_sales_contacts_1"] = array (
  'name' => 'is1_inside_sales_contacts_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_contacts_1',
  'source' => 'non-db',
  'module' => 'IS1_Inside_Sales',
  'bean_name' => 'IS1_Inside_Sales',
  'vname' => 'LBL_IS1_INSIDE_SALES_CONTACTS_1_FROM_IS1_INSIDE_SALES_TITLE',
  'id_name' => 'is1_inside_sales_contacts_1is1_inside_sales_ida',
);
$dictionary["Contact"]["fields"]["is1_inside_sales_contacts_1_name"] = array (
  'name' => 'is1_inside_sales_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IS1_INSIDE_SALES_CONTACTS_1_FROM_IS1_INSIDE_SALES_TITLE',
  'save' => true,
  'id_name' => 'is1_inside_sales_contacts_1is1_inside_sales_ida',
  'link' => 'is1_inside_sales_contacts_1',
  'table' => 'is1_inside_sales',
  'module' => 'IS1_Inside_Sales',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["is1_inside_sales_contacts_1is1_inside_sales_ida"] = array (
  'name' => 'is1_inside_sales_contacts_1is1_inside_sales_ida',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_contacts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_CONTACTS_1_FROM_CONTACTS_TITLE',
);


 // created: 2017-10-09 16:23:01
$dictionary['Contact']['fields']['total_lead_score_c']['inline_edit']='1';
$dictionary['Contact']['fields']['total_lead_score_c']['labelValue']='Total Lead Score';

 

 // created: 2017-10-16 14:53:10
$dictionary['Contact']['fields']['contact_type_c']['inline_edit']='1';
$dictionary['Contact']['fields']['contact_type_c']['labelValue']='Contact Type';

 

 // created: 2017-10-19 15:35:15
$dictionary['Contact']['fields']['product_definition_c']['inline_edit']='1';
$dictionary['Contact']['fields']['product_definition_c']['labelValue']='Product Definition';

 

 // created: 2017-10-26 21:06:00
$dictionary['Contact']['fields']['title']['len']='255';
$dictionary['Contact']['fields']['title']['inline_edit']=true;
$dictionary['Contact']['fields']['title']['comments']='The title of the contact';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';

 

 // created: 2017-10-22 17:50:25
$dictionary['Contact']['fields']['country_c']['inline_edit']='1';
$dictionary['Contact']['fields']['country_c']['labelValue']='Country';

 
?>