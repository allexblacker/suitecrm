<?php

require_once('modules/Contacts/controller.php');
require_once('custom/modules/Contacts/customContacts.php');

class CustomContactsController extends ContactsController {
	
	public function action_Popup() {
		$this->bean = new CustomContacts();
		parent::action_Popup();
	}

}
?>