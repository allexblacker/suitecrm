<?php
$viewdefs ['Leads'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 
          array (
            'customCode' => '{if $bean->aclAccess("edit") && !$DISABLE_CONVERT_ACTION}<input title="{$MOD.LBL_CONVERTLEAD_TITLE}" accessKey="{$MOD.LBL_CONVERTLEAD_BUTTON_KEY}" type="button" class="button" onClick="document.location=\'index.php?module=Leads&action=ConvertLead&record={$fields.id.value}\'" name="convert" value="{$MOD.LBL_CONVERTLEAD}">{/if}',
            'sugar_html' => 
            array (
              'type' => 'button',
              'value' => '{$MOD.LBL_CONVERTLEAD}',
              'htmlOptions' => 
              array (
                'title' => '{$MOD.LBL_CONVERTLEAD_TITLE}',
                'accessKey' => '{$MOD.LBL_CONVERTLEAD_BUTTON_KEY}',
                'class' => 'button',
                'onClick' => 'document.location=\'index.php?module=Leads&action=ConvertLead&record={$fields.id.value}\'',
                'name' => 'convert',
                'id' => 'convert_lead_button',
              ),
              'template' => '{if $bean->aclAccess("edit") && !$DISABLE_CONVERT_ACTION}[CONTENT]{/if}',
            ),
          ),
          4 => 'FIND_DUPLICATES',
          5 => 
          array (
            'customCode' => '<input title="{$APP.LBL_MANAGE_SUBSCRIPTIONS}" class="button" onclick="this.form.return_module.value=\'Leads\'; this.form.return_action.value=\'DetailView\';this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'Subscriptions\'; this.form.module.value=\'Campaigns\'; this.form.module_tab.value=\'Leads\';" type="submit" name="Manage Subscriptions" value="{$APP.LBL_MANAGE_SUBSCRIPTIONS}">',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
              'htmlOptions' => 
              array (
                'title' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
                'class' => 'button',
                'id' => 'manage_subscriptions_button',
                'onclick' => 'this.form.return_module.value=\'Leads\'; this.form.return_action.value=\'DetailView\';this.form.return_id.value=\'{$fields.id.value}\'; this.form.action.value=\'Subscriptions\'; this.form.module.value=\'Campaigns\'; this.form.module_tab.value=\'Leads\';',
                'name' => '{$APP.LBL_MANAGE_SUBSCRIPTIONS}',
              ),
            ),
          ),
          'AOS_GENLET' => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_GENERATE_LETTER}">',
          ),
        ),
        'headerTpl' => 'modules/Leads/tpls/DetailViewHeader.tpl',
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Leads/Lead.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => '',
          1 => 'status',
        ),
        1 => 
        array (
          0 => '',
          1 => '',
        ),
        2 => 
        array (
          0 => 'lead_source',
          1 => 
          array (
            'name' => 'lead_score_c',
            'label' => 'LBL_LEAD_SCORE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'first_name',
            'comment' => 'First name of the contact',
            'label' => 'LBL_FIRST_NAME',
          ),
          1 => 
          array (
            'name' => 'last_name',
            'comment' => 'Last name of the contact',
            'label' => 'LBL_LAST_NAME',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'company_c',
            'label' => 'COMPANY',
          ),
          1 => 
          array (
            'name' => 'assistant_phone',
            'comment' => 'Phone number of the assistant of the contact',
            'label' => 'LBL_ASSISTANT_PHONE',
          ),
        ),
        5 => 
        array (
          0 => 'title',
          1 => 'phone_mobile',
        ),
        6 => 
        array (
          0 => 'description',
          1 => 'phone_work',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'campaign_name',
            'label' => 'LBL_CAMPAIGN',
          ),
          1 => 'email1',
        ),
        8 => 
        array (
          0 => '',
          1 => 'phone_fax',
        ),
        9 => 
        array (
          0 => '',
          1 => 'website',
        ),
        10 => 
        array (
          0 => '',
          1 => 'refered_by',
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'type_c',
            'studio' => 'visible',
            'label' => 'TYPE',
          ),
          1 => '',
        ),
        12 => 
        array (
          0 => '',
          1 => '',
        ),
        13 => 
        array (
          0 => '',
          1 => '',
        ),
        14 => 
        array (
          0 => '',
          1 => '',
        ),
        15 => 
        array (
          0 => '',
          1 => '',
        ),
        16 => 
        array (
          0 => '',
          1 => '',
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'business_type_c',
            'studio' => 'visible',
            'label' => 'LBL_BUSINESS_TYPE',
          ),
          1 => '',
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'request_c',
            'label' => 'REQUEST',
          ),
          1 => '',
        ),
        19 => 
        array (
          0 => '',
          1 => '',
        ),
        20 => 
        array (
          0 => '',
          1 => '',
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'newsletter_c',
            'label' => 'LBL_NEWSLETTER',
          ),
          1 => '',
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'converted',
            'comment' => 'Has Lead been converted to a Contact (and other Sugar objects)',
            'label' => 'LBL_CONVERTED',
          ),
          1 => '',
        ),
        23 => 
        array (
          0 => '',
          1 => '',
        ),
        24 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_address_c',
            'label' => 'LBL_JJWG_MAPS_ADDRESS',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'primary_address_city',
            'comment' => 'City for primary address',
            'label' => 'LBL_PRIMARY_ADDRESS_CITY',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'canadian_province_c',
            'label' => 'LBL_CANADIAN_PROVINCE',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'zip_code_c',
            'label' => 'LBL_ZIP_CODE',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'country_c',
            'studio' => 'visible',
            'label' => 'LBL_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'state_c',
            'studio' => 'visible',
            'label' => 'LBL_STATE',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'classification_c',
            'studio' => 'visible',
            'label' => 'LBL_CLASSIFICATION',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'distributor_name_c',
            'studio' => 'visible',
            'label' => 'LBL_DISTRIBUTOR_NAME',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => '',
          1 => '',
        ),
        3 => 
        array (
          0 => '',
          1 => '',
        ),
        4 => 
        array (
          0 => '',
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'alt_address_street',
            'comment' => 'Street address for alternate address',
            'label' => 'LBL_ALT_ADDRESS_STREET',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => '',
          1 => '',
        ),
        7 => 
        array (
          0 => '',
          1 => '',
        ),
        8 => 
        array (
          0 => '',
          1 => '',
        ),
        9 => 
        array (
          0 => '',
          1 => '',
        ),
        10 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'customer_acquisition_activit_c',
            'studio' => 'visible',
            'label' => 'LBL_CUSTOMER_ACQUISITION_ACTIVIT',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'campaign_text_c',
            'label' => 'LBL_CAMPAIGN_TEXT',
          ),
          1 => '',
        ),
        12 => 
        array (
          0 => '',
          1 => '',
        ),
        13 => 
        array (
          0 => '',
          1 => '',
        ),
        14 => 
        array (
          0 => '',
          1 => '',
        ),
        15 => 
        array (
          0 => '',
          1 => '',
        ),
        16 => 
        array (
          0 => '',
          1 => '',
        ),
        17 => 
        array (
          0 => '',
          1 => '',
        ),
        18 => 
        array (
          0 => '',
          1 => '',
        ),
        19 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
        1 => 
        array (
          0 => '',
          1 => '',
        ),
        2 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'accept_status_name',
            'label' => 'LBL_LIST_ACCEPT_STATUS',
          ),
          1 => 
          array (
            'name' => 'account_description',
            'comment' => 'Description of lead account',
            'label' => 'LBL_ACCOUNT_DESCRIPTION',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'account_name',
            'comment' => 'Account name for lead',
            'label' => 'LBL_ACCOUNT_NAME',
          ),
          1 => 
          array (
            'name' => 'alt_address_city',
            'comment' => 'City for alternate address',
            'label' => 'LBL_ALT_ADDRESS_CITY',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'alt_address_country',
            'comment' => 'Country for alternate address',
            'label' => 'LBL_ALT_ADDRESS_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'alt_address_postalcode',
            'comment' => 'Postal code for alternate address',
            'label' => 'LBL_ALT_ADDRESS_POSTALCODE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'alt_address_state',
            'comment' => 'State for alternate address',
            'label' => 'LBL_ALT_ADDRESS_STATE',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'assistant',
            'comment' => 'Name of the assistant of the contact',
            'label' => 'LBL_ASSISTANT',
          ),
          1 => 
          array (
            'name' => 'birthdate',
            'comment' => 'The birthdate of the contact',
            'label' => 'LBL_BIRTHDATE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'campaign_score_c',
            'label' => 'LBL_CAMPAIGN_SCORE',
          ),
          1 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'department',
            'comment' => 'Department the lead belongs to',
            'label' => 'LBL_DEPARTMENT',
          ),
          1 => 
          array (
            'name' => 'do_not_call',
            'comment' => 'An indicator of whether contact can be called',
            'label' => 'LBL_DO_NOT_CALL',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_geocode_status_c',
            'label' => 'LBL_JJWG_MAPS_GEOCODE_STATUS',
          ),
          1 => 
          array (
            'name' => 'phone_home',
            'comment' => 'Home phone number of the contact',
            'label' => 'LBL_HOME_PHONE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'jjwg_maps_lat_c',
            'label' => 'LBL_JJWG_MAPS_LAT',
          ),
          1 => 
          array (
            'name' => 'lead_priority_c',
            'label' => 'LBL_LEAD_PRIORITY',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'lead_source_description',
            'comment' => 'Description of the lead source',
            'label' => 'LBL_LEAD_SOURCE_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'jjwg_maps_lng_c',
            'label' => 'LBL_JJWG_MAPS_LNG',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => 
          array (
            'name' => 'full_name',
            'studio' => 
            array (
              'listview' => false,
            ),
            'label' => 'LBL_NAME',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'opportunity_amount',
            'comment' => 'Amount of the opportunity',
            'label' => 'LBL_OPPORTUNITY_AMOUNT',
          ),
          1 => 
          array (
            'name' => 'opportunity_name',
            'comment' => 'Opportunity name associated with lead',
            'label' => 'LBL_OPPORTUNITY_NAME',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'phone_other',
            'comment' => 'Other phone number for the contact',
            'label' => 'LBL_OTHER_PHONE',
          ),
          1 => 
          array (
            'name' => 'photo',
            'studio' => 
            array (
              'listview' => true,
            ),
            'label' => 'LBL_PHOTO',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'primary_address_country',
            'comment' => 'Country for primary address',
            'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
          ),
          1 => 
          array (
            'name' => 'best_selling_inverter_c',
            'studio' => 'visible',
            'label' => 'LBL_BEST_SELLING_INVERTER',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'primary_address_state',
            'comment' => 'State for primary address',
            'label' => 'LBL_PRIMARY_ADDRESS_STATE',
          ),
          1 => 
          array (
            'name' => 'primary_address_street',
            'comment' => 'Street address for primary address',
            'label' => 'LBL_PRIMARY_ADDRESS_STREET',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'report_to_name',
            'label' => 'LBL_REPORTS_TO',
          ),
          1 => 
          array (
            'name' => 'salutation',
            'comment' => 'Contact salutation (e.g., Mr, Ms)',
            'label' => 'LBL_SALUTATION',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'total_lead_score_c',
            'label' => 'LBL_TOTAL_LEAD_SCORE',
          ),
          1 => 
          array (
            'name' => 'status_description',
            'comment' => 'Description of the status of the lead',
            'label' => 'LBL_STATUS_DESCRIPTION',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'listview_security_group',
            'label' => 'LBL_LISTVIEW_GROUP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'county_c',
            'label' => 'COUNTY',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'usa_zip_code_c',
            'studio' => 'visible',
            'label' => 'LBL_USA_ZIP_CODE',
          ),
          1 => 
          array (
            'name' => 'xing_search_c',
            'label' => 'LBL_XING_SEARCH',
          ),
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'activity_focus_c',
            'studio' => 'visible',
            'label' => 'LBL_ACTIVITY_FOCUS',
          ),
          1 => 
          array (
            'name' => 'google_search_c',
            'label' => 'LBL_GOOGLE_SEARCH',
          ),
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'google_search_company_c',
            'label' => 'LBL_GOOGLE_SEARCH_COMPANY',
          ),
          1 => 
          array (
            'name' => 'lead_age_c',
            'label' => 'LBL_LEAD_AGE',
          ),
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'linkedin_search_c',
            'label' => 'LBL_LINKEDIN_SEARCH',
          ),
          1 => 
          array (
            'name' => 'place_name_canada_only_c',
            'label' => 'LBL_PLACE_NAME_CANADA_ONLY',
          ),
        ),
      ),
    ),
  ),
);
?>
