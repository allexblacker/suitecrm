<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-11-02 18:13:50
$dictionary['Lead']['fields']['converted_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['converted_date_c']['labelValue']='Converted Date';

 

 // created: 2017-10-19 18:41:40
$dictionary['Lead']['fields']['request_c']['inline_edit']='1';
$dictionary['Lead']['fields']['request_c']['labelValue']='Request';

 

 // created: 2017-08-14 15:51:59
$dictionary['Lead']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2017-10-31 14:11:43
$dictionary['Lead']['fields']['se_training_attended_c']['inline_edit']='1';
$dictionary['Lead']['fields']['se_training_attended_c']['labelValue']='SE training attended';

 

 // created: 2017-11-08 17:24:47
$dictionary['Lead']['fields']['primary_address_street_back_c']['inline_edit']='1';
$dictionary['Lead']['fields']['primary_address_street_back_c']['labelValue']='primary address street back';

 

 // created: 2017-10-17 15:56:19
$dictionary['Lead']['fields']['county_c']['inline_edit']='1';
$dictionary['Lead']['fields']['county_c']['labelValue']='US County';

 

 // created: 2017-10-31 14:03:35
$dictionary['Lead']['fields']['house_exhibitions_c']['inline_edit']='1';
$dictionary['Lead']['fields']['house_exhibitions_c']['labelValue']='House Exhibitions';

 

 // created: 2017-11-08 14:57:17
$dictionary['Lead']['fields']['comments_c']['inline_edit']='1';
$dictionary['Lead']['fields']['comments_c']['labelValue']='Comments';

 

 // created: 2017-11-08 14:59:43
$dictionary['Lead']['fields']['primary_address_street']['len']='255';
$dictionary['Lead']['fields']['primary_address_street']['inline_edit']=true;
$dictionary['Lead']['fields']['primary_address_street']['comments']='Street address for primary address';
$dictionary['Lead']['fields']['primary_address_street']['merge_filter']='disabled';

 

 // created: 2017-11-03 14:38:17
$dictionary['Lead']['fields']['zip_c']['inline_edit']='1';
$dictionary['Lead']['fields']['zip_c']['labelValue']='zip';

 

 // created: 2017-08-29 11:40:52
$dictionary['Lead']['fields']['zip_code_c']['inline_edit']='1';
$dictionary['Lead']['fields']['zip_code_c']['labelValue']='zip code';

 

 // created: 2017-08-20 17:12:43
$dictionary['Lead']['fields']['state_c']['inline_edit']='1';
$dictionary['Lead']['fields']['state_c']['labelValue']='State';

 

 // created: 2017-10-17 18:48:51
$dictionary['Lead']['fields']['website']['inline_edit']=true;
$dictionary['Lead']['fields']['website']['comments']='URL of website for the company';
$dictionary['Lead']['fields']['website']['merge_filter']='disabled';

 

 // created: 2017-10-31 14:21:28
$dictionary['Lead']['fields']['type_of_installation_c']['inline_edit']='1';
$dictionary['Lead']['fields']['type_of_installation_c']['labelValue']='Type Of Installation';

 

 // created: 2017-08-14 15:51:59
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-11-03 14:38:32
$dictionary['Lead']['fields']['state_province_c']['inline_edit']='1';
$dictionary['Lead']['fields']['state_province_c']['labelValue']='state province';

 

 // created: 2017-08-14 15:51:59
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-11-02 18:42:22
$dictionary['Lead']['fields']['mobile_c']['inline_edit']='1';
$dictionary['Lead']['fields']['mobile_c']['labelValue']='Mobile';

 

 // created: 2017-10-31 15:18:34
$dictionary['Lead']['fields']['best_selling_inverter_c']['inline_edit']='1';
$dictionary['Lead']['fields']['best_selling_inverter_c']['labelValue']='Best Selling Inverter';

 

 // created: 2017-10-17 16:50:41
$dictionary['Lead']['fields']['xing_search_c']['inline_edit']='1';
$dictionary['Lead']['fields']['xing_search_c']['labelValue']='Xing Search';

 

 // created: 2017-10-19 18:44:22
$dictionary['Lead']['fields']['activity_focus_c']['inline_edit']='1';
$dictionary['Lead']['fields']['activity_focus_c']['labelValue']='activity focus';

 

// created: 2017-08-31 12:53:12
$dictionary["Lead"]["fields"]["uzc1_usa_zip_codes_leads"] = array (
  'name' => 'uzc1_usa_zip_codes_leads',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_leads',
  'source' => 'non-db',
  'module' => 'UZC1_USA_ZIP_Codes',
  'bean_name' => 'UZC1_USA_ZIP_Codes',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_LEADS_FROM_UZC1_USA_ZIP_CODES_TITLE',
);


 // created: 2017-09-05 18:01:17
$dictionary['Lead']['fields']['google_search_c']['inline_edit']='1';
$dictionary['Lead']['fields']['google_search_c']['labelValue']='google search';

 

 // created: 2017-10-31 13:43:29
$dictionary['Lead']['fields']['status']['required']=true;
$dictionary['Lead']['fields']['status']['inline_edit']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['options']='lead_status_list';

 

 // created: 2017-08-14 15:51:59
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-10-31 13:57:26
$dictionary['Lead']['fields']['geographic_area_c']['inline_edit']='1';
$dictionary['Lead']['fields']['geographic_area_c']['labelValue']='Geographic Area';

 

 // created: 2017-11-02 18:48:29
$dictionary['Lead']['fields']['email_2_c']['inline_edit']='1';
$dictionary['Lead']['fields']['email_2_c']['labelValue']='Email 2';

 

 // created: 2017-11-02 18:52:04
$dictionary['Lead']['fields']['decision_maker_c']['inline_edit']='1';
$dictionary['Lead']['fields']['decision_maker_c']['labelValue']='Decision Maker';

 

 // created: 2017-08-23 12:39:37
$dictionary['Lead']['fields']['converted']['audited']=true;
$dictionary['Lead']['fields']['converted']['inline_edit']=true;
$dictionary['Lead']['fields']['converted']['comments']='Has Lead been converted to a Contact (and other Sugar objects)';
$dictionary['Lead']['fields']['converted']['merge_filter']='disabled';

 

 // created: 2017-10-17 16:49:38
$dictionary['Lead']['fields']['campaign_score_c']['inline_edit']='1';
$dictionary['Lead']['fields']['campaign_score_c']['labelValue']='Campaign Score';

 

 // created: 2017-10-19 18:44:40
$dictionary['Lead']['fields']['lead_score_c']['inline_edit']='1';
$dictionary['Lead']['fields']['lead_score_c']['labelValue']='lead score';

 

// created: 2017-11-15 18:23:49
$dictionary["Lead"]["fields"]["leads_documents_1"] = array (
  'name' => 'leads_documents_1',
  'type' => 'link',
  'relationship' => 'leads_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


 // created: 2017-11-03 14:38:05
$dictionary['Lead']['fields']['city_c']['inline_edit']='1';
$dictionary['Lead']['fields']['city_c']['labelValue']='City';

 

 // created: 2017-09-03 15:20:48
$dictionary['Lead']['fields']['phone_mobile']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';

 

 // created: 2017-10-31 13:17:08
$dictionary['Lead']['fields']['industry_c']['inline_edit']='1';
$dictionary['Lead']['fields']['industry_c']['labelValue']='Industry';

 

 // created: 2017-10-19 18:45:18
$dictionary['Lead']['fields']['newsletter_c']['inline_edit']='1';
$dictionary['Lead']['fields']['newsletter_c']['labelValue']='Newsletter';

 

 // created: 2017-10-19 18:41:03
$dictionary['Lead']['fields']['campaign_text_c']['inline_edit']='1';
$dictionary['Lead']['fields']['campaign_text_c']['labelValue']='Campaign (text)';

 

 // created: 2017-10-17 18:42:53
$dictionary['Lead']['fields']['customer_acquisition_activit_c']['inline_edit']='1';
$dictionary['Lead']['fields']['customer_acquisition_activit_c']['labelValue']='customer Acquisition Activity';

 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Lead']['fields']['listview_security_group'] = array(
	'name' => 'listview_security_group',
	'label' => 'LBL_LISTVIEW_GROUP',
	'vname' => 'LBL_LISTVIEW_GROUP',
	'type' => 'text',
	'source' => 'non-db',
	'studio' => 'visible',
);

 // created: 2017-11-10 13:30:38
$dictionary['Lead']['fields']['main_panel_brands_c']['inline_edit']='1';
$dictionary['Lead']['fields']['main_panel_brands_c']['labelValue']='Main Panel Brands';

 

// created: 2017-08-03 11:47:59
$dictionary["Lead"]["fields"]["leads_leads_1"] = array (
  'name' => 'leads_leads_1',
  'type' => 'link',
  'relationship' => 'leads_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_LEADS_1_FROM_LEADS_L_TITLE',
);
$dictionary["Lead"]["fields"]["leads_leads_1"] = array (
  'name' => 'leads_leads_1',
  'type' => 'link',
  'relationship' => 'leads_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_LEADS_1_FROM_LEADS_R_TITLE',
);


 // created: 2017-10-24 16:13:01
$dictionary['Lead']['fields']['first_name']['required']=true;
$dictionary['Lead']['fields']['first_name']['inline_edit']=true;
$dictionary['Lead']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';

 


$dictionary["Lead"]["fields"]["searchform_security_group"] = array(
	'name' => 'searchform_security_group',
	'label' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'vname' => 'LBL_SEARCHFORM_SECURITY_GROUP',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'listSecurityGroups',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/securitygroup.php',
	),
);


 // created: 2017-08-23 11:57:56
$dictionary['Lead']['fields']['lead_age_c']['inline_edit']='1';
$dictionary['Lead']['fields']['lead_age_c']['labelValue']='lead age';

 

 // created: 2017-11-03 14:37:43
$dictionary['Lead']['fields']['street_c']['inline_edit']='1';
$dictionary['Lead']['fields']['street_c']['labelValue']='Street';

 

 // created: 2017-10-17 16:55:15
$dictionary['Lead']['fields']['usa_zip_code_c']['inline_edit']='1';
$dictionary['Lead']['fields']['usa_zip_code_c']['labelValue']='USA Zip Code';

 

 // created: 2017-10-17 16:50:26
$dictionary['Lead']['fields']['lead_priority_c']['inline_edit']='1';
$dictionary['Lead']['fields']['lead_priority_c']['labelValue']='Lead Priority';

 

 // created: 2017-08-29 12:50:59
$dictionary['Lead']['fields']['type_c']['inline_edit']='1';
$dictionary['Lead']['fields']['type_c']['labelValue']='Type';

 

 // created: 2017-10-17 16:55:14
$dictionary['Lead']['fields']['uzc1_usa_zip_codes_id_c']['inline_edit']=1;

 

 // created: 2017-10-19 18:45:00
$dictionary['Lead']['fields']['classification_c']['inline_edit']='1';
$dictionary['Lead']['fields']['classification_c']['labelValue']='Classification';

 

 // created: 2017-10-19 18:44:02
$dictionary['Lead']['fields']['distributor_name_c']['inline_edit']='1';
$dictionary['Lead']['fields']['distributor_name_c']['labelValue']='distributor name';

 

 // created: 2017-08-24 12:57:00
$dictionary['Lead']['fields']['linkedin_search_c']['inline_edit']='1';
$dictionary['Lead']['fields']['linkedin_search_c']['labelValue']='linkedin search';

 

 // created: 2017-10-24 16:13:07
$dictionary['Lead']['fields']['last_name']['required']=true;
$dictionary['Lead']['fields']['last_name']['inline_edit']=true;
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']='1';

 

 // created: 2017-11-08 11:20:59
$dictionary['Lead']['fields']['acton_lead_score_c']['inline_edit']=1;

 

 // created: 2017-10-24 16:13:29
$dictionary['Lead']['fields']['company_c']['inline_edit']='1';
$dictionary['Lead']['fields']['company_c']['labelValue']='Company';

 

 // created: 2017-10-31 15:22:55
$dictionary['Lead']['fields']['australian_state_c']['inline_edit']='1';
$dictionary['Lead']['fields']['australian_state_c']['labelValue']='Australian State';

 

 // created: 2017-08-29 11:51:32
$dictionary['Lead']['fields']['canadian_province_c']['inline_edit']='1';
$dictionary['Lead']['fields']['canadian_province_c']['labelValue']='Canadian Province';

 

 // created: 2017-10-15 19:33:23
$dictionary['Lead']['fields']['business_type_c']['inline_edit']='1';
$dictionary['Lead']['fields']['business_type_c']['labelValue']='Business Type';

 

 // created: 2017-11-02 18:10:51
$dictionary['Lead']['fields']['rating_c']['inline_edit']='1';
$dictionary['Lead']['fields']['rating_c']['labelValue']='Rating';

 

 // created: 2017-08-29 11:52:08
$dictionary['Lead']['fields']['place_name_canada_only_c']['inline_edit']='1';
$dictionary['Lead']['fields']['place_name_canada_only_c']['labelValue']='place name canada only';

 

 // created: 2017-08-14 15:51:59
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-10-16 10:59:52
$dictionary['Lead']['fields']['google_search_company_c']['inline_edit']='1';
$dictionary['Lead']['fields']['google_search_company_c']['labelValue']='google search company';

 

 // created: 2017-11-02 18:51:25
$dictionary['Lead']['fields']['technical_buyer_c']['inline_edit']='1';
$dictionary['Lead']['fields']['technical_buyer_c']['labelValue']='Technical Buyer';

 

 // created: 2017-10-17 16:49:57
$dictionary['Lead']['fields']['total_lead_score_c']['inline_edit']='1';
$dictionary['Lead']['fields']['total_lead_score_c']['labelValue']='Total Lead Score';

 

 // created: 2017-11-03 14:40:03
$dictionary['Lead']['fields']['address_c']['inline_edit']='1';
$dictionary['Lead']['fields']['address_c']['labelValue']='Address';

 

 // created: 2017-10-31 15:36:16
$dictionary['Lead']['fields']['commit_level_c']['inline_edit']='1';
$dictionary['Lead']['fields']['commit_level_c']['labelValue']='Commit Level';

 

 // created: 2017-11-09 13:05:24
$dictionary['Lead']['fields']['title']['len']='255';
$dictionary['Lead']['fields']['title']['inline_edit']=true;
$dictionary['Lead']['fields']['title']['comments']='The title of the contact';
$dictionary['Lead']['fields']['title']['merge_filter']='disabled';

 

 // created: 2017-11-06 14:04:41
$dictionary['Lead']['fields']['lead_source']['inline_edit']=true;
$dictionary['Lead']['fields']['lead_source']['options']='leadsource_list';
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';

 

 // created: 2017-10-15 19:37:50
$dictionary['Lead']['fields']['country_c']['inline_edit']='1';
$dictionary['Lead']['fields']['country_c']['labelValue']='Country';

 

 // created: 2017-10-31 14:51:36
$dictionary['Lead']['fields']['distributor_status_c']['inline_edit']='1';
$dictionary['Lead']['fields']['distributor_status_c']['labelValue']='Distributor Status';

 
?>