<?php
$module_name = 'P1_Project';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accounts_p1_project_1_name',
            'label' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_ACCOUNTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'opportunities_p1_project_1_name',
            'label' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_OPPORTUNITIES_TITLE',
          ),
          1 => 
          array (
            'name' => 'currencyisocode_c',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCYISOCODE',
          ),
        ),
        3 => 
        array (
          0 => '',
          1 => '',
        ),
        4 => 
        array (
          0 => '',
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'us_region_c',
            'label' => 'LBL_US_REGION',
          ),
          1 => 
          array (
            'name' => 'fse_picklist',
            'studio' => 'visible',
            'label' => 'FSE_picklist',
          ),
        ),
        6 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'fse_email',
            'studio' => 'visible',
            'label' => 'FSE_Email',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'contacts_p1_project_1_name',
            'label' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_CONTACTS_TITLE',
          ),
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 's1_site_p1_project_1_name',
            'label' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_S1_SITE_TITLE',
          ),
          1 => 
          array (
            'name' => 'project_status',
            'studio' => 'visible',
            'label' => 'Project_Status',
          ),
        ),
        9 => 
        array (
          0 => '',
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'site_address',
            'label' => 'Site_Address',
          ),
          1 => 
          array (
            'name' => 'site_country',
            'studio' => 'visible',
            'label' => 'Site_Country',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_city',
            'label' => 'Site_City',
          ),
          1 => 
          array (
            'name' => 'site_state',
            'studio' => 'visible',
            'label' => 'Site_State',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'site_zip',
            'label' => 'Site_ZIP',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'kick_off_meeting_internal',
            'label' => 'Kick_off_meeting_Internal',
          ),
          1 => 
          array (
            'name' => 'kick_off_meeting_external',
            'label' => 'Kick_off_meeting_External',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'kick_off_meeting_internal_date',
            'label' => 'Kick_off_meeting_Internal_date',
          ),
          1 => 
          array (
            'name' => 'kick_off_meeting_external_date',
            'label' => 'Kick_off_meeting_External_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'welcome_to_sedg_email',
            'label' => 'Welcome_to_SEDG_Email',
          ),
          1 => 
          array (
            'name' => 'material_delivery',
            'label' => 'Material_Delivery',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'welcome_to_sedg_email_date',
            'label' => 'Welcome_to_SEDG_Email_Date',
          ),
          1 => 
          array (
            'name' => 'material_delivery_date',
            'label' => 'Material_Delivery_Date',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'offsite_training_scheduled',
            'label' => 'Offsite_Training_scheduled_as_necessar',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'offsite_training_scheduled_as',
            'label' => 'Offsite_Training_scheduled_as_necessar_D',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'project_start_date',
            'label' => 'Project_Start_Date',
          ),
          1 => 
          array (
            'name' => 'pre_commissioning_site_visit',
            'label' => 'Pre_Commissioning_Site_Visit',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'project_start_date2',
            'label' => 'Project_Start_Date2',
          ),
          1 => 
          array (
            'name' => 'pre_commissioning_site_visit_d',
            'label' => 'Pre_Commissioning_Site_Visit_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'initial_site_visit',
            'label' => 'Initial_Site_Visit',
          ),
          1 => 
          array (
            'name' => 'commissioning',
            'label' => 'Commissioning',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'site_visit_date',
            'label' => 'Site_Visit_Date',
          ),
          1 => 
          array (
            'name' => 'commissioning_date',
            'label' => 'Commissioning_Date',
          ),
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'site_evaluation',
            'label' => 'Site_Evaluation',
          ),
          1 => 
          array (
            'name' => 'welcome_to_support_email',
            'label' => 'Welcome_to_Support_Email',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_evaluation_date',
            'label' => 'Site_Evaluation_Date',
          ),
          1 => 
          array (
            'name' => 'welcome_to_support_email_date',
            'label' => 'Welcome_to_Support_Email_Date',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'final_report_external',
            'label' => 'Final_Report_External',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'final_report_external_date',
            'label' => 'Final_Report_External_Date',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'all_equipment_arrived_c',
            'label' => 'LBL_ALL_EQUIPMENT_ARRIVED',
          ),
          1 => 
          array (
            'name' => 'cae_c',
            'label' => 'LBL_CAE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cpm_c',
            'label' => 'LBL_CPM',
          ),
          1 => 
          array (
            'name' => 'communication_c',
            'label' => 'LBL_COMMUNICATION',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'communication_type_c',
            'studio' => 'visible',
            'label' => 'LBL_COMMUNICATION_TYPE',
          ),
          1 => 
          array (
            'name' => 'country_c',
            'label' => 'LBL_COUNTRY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'dc_to_ac_ratio_c',
            'label' => 'LBL_DC_TO_AC_RATIO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'done_c',
            'label' => 'LBL_DONE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'epc_kit_c',
            'label' => 'LBL_EPC_KIT',
          ),
          1 => 
          array (
            'name' => 'est_kw_c',
            'label' => 'LBL_EST_KW',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'estimated_kw_c',
            'label' => 'LBL_ESTIMATED_KW',
          ),
          1 => 
          array (
            'name' => 'evaulation_c',
            'studio' => 'visible',
            'label' => 'LBL_EVAULATION',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'fse_email_address_for_wf',
            'label' => 'FSE_Email_address_for_WF',
          ),
          1 => 
          array (
            'name' => 'final_inspection_complete_c',
            'label' => 'LBL_FINAL_INSPECTION_COMPLETE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'first_system_energize_c',
            'label' => 'LBL_FIRST_SYSTEM_ENERGIZE',
          ),
          1 => 
          array (
            'name' => 'first_system_energize_done_c',
            'label' => 'LBL_FIRST_SYSTEM_ENERGIZE_DONE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'inspection_c',
            'label' => 'LBL_INSPECTION',
          ),
          1 => 
          array (
            'name' => 'lesson_learned_c',
            'studio' => 'visible',
            'label' => 'LBL_LESSON_LEARNED',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'lesson_learnt_issued_c',
            'label' => 'LBL_LESSON_LEARNT_ISSUED',
          ),
          1 => 
          array (
            'name' => 'link_to_maps_c',
            'label' => 'LBL_LINK_TO_MAPS',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'link_to_monitoring_c',
            'label' => 'LBL_LINK_TO_MONITORING',
          ),
          1 => 
          array (
            'name' => 'link_to_projects_c',
            'label' => 'LBL_LINK_TO_PROJECTS',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'mapping_c',
            'studio' => 'visible',
            'label' => 'LBL_MAPPING',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'module_watt_power_c',
            'label' => 'LBL_MODULE_WATT_POWER',
          ),
          1 => 
          array (
            'name' => 'monitoring_account_c',
            'label' => 'LBL_MONITORING_ACCOUNT',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'monitoring_site_open_c',
            'label' => 'LBL_MONITORING_SITE_OPEN',
          ),
          1 => 
          array (
            'name' => 'number_of_modules_c',
            'label' => 'LBL_NUMBER_OF_MODULES',
          ),
        ),
        16 => 
        array (
          0 => 
          array (
            'name' => 'opportunity_owner_c',
            'label' => 'LBL_OPPORTUNITY_OWNER',
          ),
          1 => 
          array (
            'name' => 'opportunity_stage_c',
            'label' => 'LBL_ OPPORTUNITY_STAGE',
          ),
        ),
        17 => 
        array (
          0 => 
          array (
            'name' => 'optimizer_configuration_c',
            'studio' => 'visible',
            'label' => 'LBL_OPTIMIZER_CONFIGURATION',
          ),
          1 => 
          array (
            'name' => 'optimizers_per_string_c',
            'label' => 'LBL_OPTIMIZERS_PER_STRING',
          ),
        ),
        18 => 
        array (
          0 => 
          array (
            'name' => 'physical_layout_c',
            'studio' => 'visible',
            'label' => 'LBL_PHYSICAL_LAYOUT',
          ),
          1 => 
          array (
            'name' => 'plant_type_c',
            'studio' => 'visible',
            'label' => 'LBL_PLANT_TYPE',
          ),
        ),
        19 => 
        array (
          0 => 
          array (
            'name' => 'pre_commissioning_checklist_c',
            'label' => 'LBL_PRE_COMMISSIONING_CHECKLIST',
          ),
          1 => 
          array (
            'name' => 'project_evaulation_needed_c',
            'studio' => 'visible',
            'label' => 'LBL_PROJECT_EVAULATION_NEEDED',
          ),
        ),
        20 => 
        array (
          0 => 
          array (
            'name' => 'project_notice_to_proceed_po_c',
            'label' => 'LBL_PROJECT_NOTICE_TO_PROCEED_PO',
          ),
          1 => 
          array (
            'name' => 'project_notice_to_proceed_c',
            'label' => 'LBL_PROJECT_NOTICE_TO_PROCEED',
          ),
        ),
        21 => 
        array (
          0 => 
          array (
            'name' => 'project_phase_c',
            'studio' => 'visible',
            'label' => 'LBL_PROJECT_PHASE',
          ),
          1 => 
          array (
            'name' => 'punch_list_complete_c',
            'label' => 'LBL_PUNCH_LIST_COMPLETE',
          ),
        ),
        22 => 
        array (
          0 => 
          array (
            'name' => 'rec_updatec_c',
            'label' => 'LBL_REC_UPDATEC',
          ),
          1 => 
          array (
            'name' => 'region_c',
            'label' => 'LBL_REGION',
          ),
        ),
        23 => 
        array (
          0 => 
          array (
            'name' => 'listview_security_group',
            'label' => 'LBL_LISTVIEW_GROUP',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'system_installation_starts_c',
            'label' => 'LBL_SYSTEM_INSTALLATION_STARTS',
          ),
        ),
        24 => 
        array (
          0 => 
          array (
            'name' => 'state_c',
            'label' => 'LBL_STATE',
          ),
          1 => 
          array (
            'name' => 'system_additional_desc_c',
            'studio' => 'visible',
            'label' => 'LBL_SYSTEM_ADDITIONAL_DESC',
          ),
        ),
        25 => 
        array (
          0 => 
          array (
            'name' => 'system_energized_c',
            'label' => 'LBL_SYSTEM_ENERGIZED',
          ),
          1 => 
          array (
            'name' => 'system_handover_to_support_c',
            'label' => 'LBL_SYSTEM_HANDOVER_TO_SUPPORT',
          ),
        ),
        26 => 
        array (
          0 => 
          array (
            'name' => 'system_handover_to_supp_done_c',
            'label' => 'LBL_SYSTEM_HANDOVER_TO_SUPP_DONE',
          ),
          1 => 
          array (
            'name' => 'system_installation_done_c',
            'label' => 'LBL_SYSTEM_INSTALLATION_DONE',
          ),
        ),
        27 => 
        array (
          0 => 
          array (
            'name' => 'training_c',
            'label' => 'LBL_TRAINING',
          ),
          1 => 
          array (
            'name' => 'users_p1_project_1_name',
            'label' => 'LBL_USERS_P1_PROJECT_1_FROM_USERS_TITLE',
          ),
        ),
        28 => 
        array (
          0 => 
          array (
            'name' => 'users_p1_project_2_name',
            'label' => 'LBL_USERS_P1_PROJECT_2_FROM_USERS_TITLE',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
