<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-09-26 12:28:23
$layout_defs["P1_Project"]["subpanel_setup"]['p1_project_dtbc_project_stakeholders_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Project_Stakeholders',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_P1_PROJECT_DTBC_PROJECT_STAKEHOLDERS_1_FROM_DTBC_PROJECT_STAKEHOLDERS_TITLE',
  'get_subpanel_data' => 'p1_project_dtbc_project_stakeholders_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-18 12:46:47
$layout_defs["P1_Project"]["subpanel_setup"]['p1_project_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_P1_PROJECT_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'p1_project_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-09-26 11:42:36
$layout_defs["P1_Project"]["subpanel_setup"]['p1_project_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_P1_PROJECT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'p1_project_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-09-26 12:27:38
$layout_defs["P1_Project"]["subpanel_setup"]['p1_project_dtbc_project_comments_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Project_Comments',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_DTBC_PROJECT_COMMENTS_TITLE',
  'get_subpanel_data' => 'p1_project_dtbc_project_comments_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['P1_Project']['subpanel_setup']['p1_project_dtbc_project_comments_1']['override_subpanel_name'] = 'P1_Project_subpanel_p1_project_dtbc_project_comments_1';


//auto-generated file DO NOT EDIT
$layout_defs['P1_Project']['subpanel_setup']['p1_project_dtbc_project_stakeholders_1']['override_subpanel_name'] = 'P1_Project_subpanel_p1_project_dtbc_project_stakeholders_1';

?>