<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.edit.php');
require_once('custom/include/dtbc/JSLoader.php');
require_once("custom/include/dtbc/helpers.php");

class AL1_Alliance_TransactionViewEdit extends ViewEdit {
	var $useForSubpanel = true;

    function display() {
        parent::display();

		// Not include on quick create
		if (empty($_REQUEST['target_action']) || $_REQUEST['target_action'] != "QuickCreate")
			includeJavaScriptFile("custom/modules/AL1_Alliance_Transaction/dtbc/validations.js");
    }
}
