function isBalancePositive(formName) {
	if ($("#" + formName + " #collect_redeem").val() == 1)
		return true;
	
	var accountId = "";
	
	if ($('#form_SubpanelQuickCreate_AL1_Alliance_Transaction div.buttons').length)
		accountId = $("#recordid").val();
	else
		accountId = $("#accounts_al1_alliance_transaction_1accounts_ida").val();
	
	$.ajax({
		method: "POST",
		url: "index.php?module=AL1_Alliance_Transaction&action=checkPoints&to_pdf=1",
		async: false,
		data: { 
			points: Math.abs($("#" + formName + " #points").val()),
			record: accountId,
		},
	})
	.done(function (response) {
		// response == 0 -> no points sent
		// response == 1 -> ok, you can add these points
		// response == 2 -> oohh no, to much points!
		if (response == 2) {
			$('[dtbc-data=points_label]').append("<div class='required validation-message' id='InvalidPoints'>" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_INVALID_POINTS') + "</div>");
			return false;
		}
		
		if ($('#InvalidPoints').length)
			$('#InvalidPoints').remove();
		
		return true;
	})
	.fail(function (e) {
		console.log(e);
	});
	
	return false;
}

function calcRetval(retValue, functionResult) {
	if (!retValue)
		return false;
	return functionResult;
}

function customValidation(formName) {
	var retval = isBalancePositive(formName);
	var checkForm = check_form(formName);
	// Original validations
	var _form = document.getElementById(formName);
	_form.action.value='Save';
	if (checkForm && retval)
		SUGAR.ajaxUI.submitForm(_form);

	return false;
}

$(document).ready(function() {
	$("#save_and_continue").remove();
	// Hide default submit buttons
	$("input[type=submit]").hide();
	// Add new submit buttons with custom validation script
	if ($('#form_SubpanelQuickCreate_AL1_Alliance_Transaction div.buttons').length)
		$("<input title='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation(\"form_SubpanelQuickCreate_AL1_Alliance_Transaction\");' type='button' name='button' value='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("#form_SubpanelQuickCreate_AL1_Alliance_Transaction div.buttons");
	else
		$("<input title='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' accesskey='a' class='button primary' onclick='return customValidation(\"EditView\");' type='button' name='button' value='" + SUGAR.language.get('AL1_Alliance_Transaction','LBL_SAVEBUTTON') + "' id='CUSTOM_SAVE'>").prependTo("div.buttons");
});