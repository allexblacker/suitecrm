<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/schedulers.php");

class CustomAL1_Alliance_TransactionController extends SugarController {
	
	public function action_checkPoints() {
		
		if (empty($_REQUEST['points']) || empty($_REQUEST['record']) || !$this->isAccountValid($_REQUEST['record']))
			$this->returnToUi(0);
		
		global $db;
		
		$schedulers = new DtbcSchedulers();
		
		// Get points data for only one account, without yearly point refresh - so this is only a check
		$data = $schedulers->calculateYearlyPoints(" AND accounts_al1_alliance_transaction_1accounts_ida = " . $db->quoted($_REQUEST['record']), true);
		
		$this->returnToUi($this->isBalancePositiveWithNewValue($data, $_REQUEST['points']));
	}
	
	private function isBalancePositiveWithNewValue($data, $points) {
		foreach ($data as $a_id => $years) {
			return $years->getAvailablePoints() - $points > 0 ? "1" : "2";
		}
		return 0 - $points > 0 ? "1" : "2";
	}
	
	private function isAccountValid($recordId) {
		global $db;
		
		$sql = "SELECT *
				FROM accounts
				WHERE id = " . $db->quoted($recordId);
				
		$res = $db->fetchOne($sql);
		
		return !empty($res) && is_array($res) && count($res) > 0 && strlen($res['id']) > 0;
	}
	
	private function returnToUi($value) {
		echo $value;
		die();
	}
    
}
