<?php
$module_name = 'AL1_Alliance_Transaction';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'accounts_al1_alliance_transaction_1_name',
            'label' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_ACCOUNTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'monitoring_site_id',
            'label' => 'Monitoring_Site_id',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'site_name',
            'label' => 'Site_Name',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'Type',
          ),
          1 => 
          array (
            'name' => 'points',
            'label' => 'Points',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'kw',
            'label' => 'KW',
          ),
          1 => 
          array (
            'name' => 'collect_redeem',
            'studio' => 'visible',
            'label' => 'Collect_redeem',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'date1',
            'label' => 'Date',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'iscalculated',
            'label' => 'isCalculated',
          ),
          1 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
?>
