<?php 
 //WARNING: The contents of this file are auto-generated


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Project_Comments'] = 'Project Comments';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Line_Item'] = 'Line Item';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['IS1_Inside_Sales'] = 'Inside Sales';
$app_list_strings['call_c_list'][1] = '	Conversation';
$app_list_strings['call_c_list'][2] = 'Voice Message';
$app_list_strings['call_c_list'][3] = 'No Contact';
$app_list_strings['call_c_list'][4] = 'TRAINING-WEB BASED';
$app_list_strings['call_c_list'][5] = 'HOMEOWNER';
$app_list_strings['call_c_list'][6] = 'Connected';
$app_list_strings['call_c_list'][7] = 'INBOUND CONVERSATION';
$app_list_strings['call_to_action_list'][1] = 'APPT-DISTI';
$app_list_strings['call_to_action_list'][2] = 'APPT-INSIDE';
$app_list_strings['call_to_action_list'][3] = 'APPT-OUTSIDE';
$app_list_strings['call_to_action_list'][4] = 'APPT-PENDING';
$app_list_strings['call_to_action_list'][5] = 'N/A';
$app_list_strings['call_to_action_list'][6] = 'PURCH-CC';
$app_list_strings['call_to_action_list'][7] = 'PURCH-TIYLI';
$app_list_strings['call_to_action_list'][8] = 'TOOL-SITE DESIGNER';
$app_list_strings['call_to_action_list'][9] = 'TRAIN-FIELD TRAIN';
$app_list_strings['call_to_action_list'][10] = 'TRAIN-MONITOR SETUP';
$app_list_strings['call_to_action_list'][11] = 'TRAIN-PAPER';
$app_list_strings['call_to_action_list'][12] = 'TRAIN-PRODUCT';
$app_list_strings['call_to_action_list'][13] = 'TRAIN-ROADSHOW';
$app_list_strings['call_to_action_list'][14] = 'TRAIN-SITE DESIGNER';
$app_list_strings['call_to_action_list'][15] = 'TRAIN-SITE MAPPER';
$app_list_strings['call_to_action_list'][16] = 'TRAIN-WEBINAR';
$app_list_strings['customer_funnel_0'][1] = 'Discovered	';
$app_list_strings['customer_funnel_0'][2] = 'Educated';
$app_list_strings['customer_funnel_0'][3] = 'Designed';
$app_list_strings['customer_funnel_0'][4] = 'Quoted';
$app_list_strings['customer_funnel_0'][5] = 'Active';
$app_list_strings['customer_funnel_0'][6] = 'Lost';
$app_list_strings['decision_maker_list'][1] = 'Yes';
$app_list_strings['decision_maker_list'][2] = 'No';
$app_list_strings['decision_maker_list'][3] = 'N/A';
$app_list_strings['decision_maker_list'][4] = 'DISTI';
$app_list_strings['role_list'][1] = 'Management/Owner';
$app_list_strings['role_list'][2] = 'Design/Engineer';
$app_list_strings['role_list'][3] = 'Install/O+M/Purchase';
$app_list_strings['role_list'][4] = 'Sales';
$app_list_strings['role_list'][5] = 'N/A';
$app_list_strings['role_list'][6] = 'GATEKEEPER';
$app_list_strings['type_0'][1] = 'LEAD-NEW	';
$app_list_strings['type_0'][2] = 'LEAD-LOST	';
$app_list_strings['type_0'][3] = 'ROADSHOW	';
$app_list_strings['type_0'][4] = 'ONBOARD	';
$app_list_strings['type_0'][5] = 'RETAIN AND GROW	';
$app_list_strings['type_0'][6] = 'HOMEOWNER';
$app_list_strings['type_0'][7] = 'DISTI';


/*********************************************************************************
 * The contents of this file are subject to the SugarCRM Enterprise Subscription
 * Agreement ("License") which can be viewed at
 * http://www.sugarcrm.com/crm/products/sugar-enterprise-eula.html
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * among other things: 1) sublicense, resell, rent, lease, redistribute, assign
 * or otherwise transfer Your rights to the Software, and 2) use the Software
 * for timesharing or service bureau purposes such as hosting the Software for
 * commercial gain and/or for the benefit of a third party.  Use of the Software
 * may be subject to applicable fees and any use of the Software without first
 * paying applicable fees is strictly prohibited.  You do not have the right to
 * remove SugarCRM copyrights from the source code or user interface.
 *
 * All copies of the Covered Code must include on each user interface screen:
 *  (i) the "Powered by SugarCRM" logo and
 *  (ii) the SugarCRM copyright notice
 * in the same form as they appear in the distribution.  See full license for
 * requirements.
 *
 * Your Warranty, Limitations of liability and Indemnity are expressly stated
 * in the License.  Please refer to the License for the specific language
 * governing these rights and limitations under the License.  Portions created
 * by SugarCRM are Copyright (C) 2004-2011 SugarCRM, Inc.; All Rights Reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['ActOn_Integrator'] = 'Act-On Hot Prospects';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['TLA1_Training_LMS_Absorb'] = 'Training LMS Absorb';
$app_list_strings['country_1']['United_States'] = 'United States';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Project_Stakeholders'] = 'Project Stakeholders';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Year_Alliance_Points'] = 'Year Alliance Points';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['S1_Site'] = 'Site';
$app_list_strings['state_0'][1] = 'Alabama';
$app_list_strings['state_0'][2] = 'Alaska';
$app_list_strings['state_0'][3] = 'Arizona';
$app_list_strings['state_0'][4] = 'Arkansas';
$app_list_strings['state_0'][5] = 'California';
$app_list_strings['state_0'][6] = 'Colorado';
$app_list_strings['state_0'][7] = 'Connecticut';
$app_list_strings['state_0'][8] = 'Delaware';
$app_list_strings['state_0'][9] = 'Florida';
$app_list_strings['state_0'][10] = 'Georgia';
$app_list_strings['state_0'][11] = 'Hawaii';
$app_list_strings['state_0'][12] = 'Idaho';
$app_list_strings['state_0'][13] = 'Illionois';
$app_list_strings['state_0'][14] = 'Indiana';
$app_list_strings['state_0'][15] = 'Iowa';
$app_list_strings['state_0'][16] = 'Kansas';
$app_list_strings['state_0'][17] = 'Kentucky';
$app_list_strings['state_0'][18] = 'Louisiana';
$app_list_strings['state_0'][19] = 'Maine';
$app_list_strings['state_0'][20] = 'Maryland';
$app_list_strings['state_0'][21] = 'Massachusetts';
$app_list_strings['state_0'][22] = 'Michigan';
$app_list_strings['state_0'][23] = 'Minnesota';
$app_list_strings['state_0'][24] = 'Mississippi';
$app_list_strings['state_0'][25] = 'Missouri';
$app_list_strings['state_0'][26] = 'Montana';
$app_list_strings['state_0'][27] = 'Nebraska';
$app_list_strings['state_0'][28] = 'Nevada';
$app_list_strings['state_0'][29] = 'New Hampshire';
$app_list_strings['state_0'][30] = 'New Jersey';
$app_list_strings['state_0'][31] = 'New Mexico';
$app_list_strings['state_0'][32] = 'New York';
$app_list_strings['state_0'][33] = 'North Carolina';
$app_list_strings['state_0'][34] = 'North Dakota';
$app_list_strings['state_0'][35] = 'Ohio';
$app_list_strings['state_0'][36] = 'Oklahoma';
$app_list_strings['state_0'][37] = 'Oregon';
$app_list_strings['state_0'][38] = 'Pennsylvania';
$app_list_strings['state_0'][39] = 'Rhode Island';
$app_list_strings['state_0'][40] = 'South Carolina';
$app_list_strings['state_0'][41] = 'South Dakota';
$app_list_strings['state_0'][42] = 'Tennessee';
$app_list_strings['state_0'][43] = 'Texas';
$app_list_strings['state_0'][44] = 'Utah';
$app_list_strings['state_0'][45] = 'Vermont';
$app_list_strings['state_0'][46] = 'Virginia';
$app_list_strings['state_0'][47] = 'Washington';
$app_list_strings['state_0'][48] = 'West Virginia';
$app_list_strings['state_0'][49] = 'Wisconsin';
$app_list_strings['state_0'][50] = 'Wyoming';
$app_list_strings['state_0'][51] = 'District of Colombia';
$app_list_strings['country_0'][1] = 'United States';
$app_list_strings['country_0'][2] = 'Afghanistan';
$app_list_strings['country_0'][3] = 'Albania';
$app_list_strings['country_0'][4] = 'Algeria';
$app_list_strings['country_0'][5] = 'American Samoa';
$app_list_strings['country_0'][6] = 'Andorra';
$app_list_strings['country_0'][7] = 'Angola';
$app_list_strings['country_0'][8] = 'Anguilla';
$app_list_strings['country_0'][9] = 'Antigua';
$app_list_strings['country_0'][10] = 'Argentina';
$app_list_strings['country_0'][11] = 'Armenia';
$app_list_strings['country_0'][12] = 'Aruba';
$app_list_strings['country_0'][13] = 'Australia';
$app_list_strings['country_0'][14] = 'Austria';
$app_list_strings['country_0'][15] = 'Azerbaijan';
$app_list_strings['country_0'][16] = 'Bahamas';
$app_list_strings['country_0'][17] = 'Bahrain';
$app_list_strings['country_0'][18] = 'Bangladesh';
$app_list_strings['country_0'][19] = 'Barbados';
$app_list_strings['country_0'][20] = 'Barbuda';
$app_list_strings['country_0'][21] = 'Belarus';
$app_list_strings['country_0'][22] = 'Belgium';
$app_list_strings['country_0'][23] = 'Belize';
$app_list_strings['country_0'][24] = 'Benin';
$app_list_strings['country_0'][25] = 'Bermuda';
$app_list_strings['country_0'][26] = 'Bhutan';
$app_list_strings['country_0'][27] = 'Bolivia';
$app_list_strings['country_0'][28] = 'Bonaire';
$app_list_strings['country_0'][29] = 'Bosnia-Herzegovina';
$app_list_strings['country_0'][30] = 'Botswana';
$app_list_strings['country_0'][31] = 'Brazil';
$app_list_strings['country_0'][32] = 'British Virgin Island';
$app_list_strings['country_0'][33] = 'Brunei';
$app_list_strings['country_0'][34] = 'Bulgaria';
$app_list_strings['country_0'][35] = 'Burkina Faso';
$app_list_strings['country_0'][36] = 'Burundi';
$app_list_strings['country_0'][37] = 'Cambodia';
$app_list_strings['country_0'][38] = 'Cameroon';
$app_list_strings['country_0'][39] = 'Canada';
$app_list_strings['country_0'][40] = 'Cape Verde';
$app_list_strings['country_0'][41] = 'Cayman Island';
$app_list_strings['country_0'][42] = 'Central African Republic';
$app_list_strings['country_0'][43] = 'Chad';
$app_list_strings['country_0'][44] = 'Channel Islands';
$app_list_strings['country_0'][45] = 'Chile';
$app_list_strings['country_0'][46] = 'China';
$app_list_strings['country_0'][47] = 'Colombia';
$app_list_strings['country_0'][48] = 'Comoros';
$app_list_strings['country_0'][49] = 'Congo';
$app_list_strings['country_0'][50] = 'Congo - Brazzaville';
$app_list_strings['country_0'][51] = 'Costa Rica';
$app_list_strings['country_0'][52] = 'Côte d';
$app_list_strings['country_0'][53] = 'Croatia';
$app_list_strings['country_0'][54] = 'Cuba';
$app_list_strings['country_0'][55] = 'Cyprus';
$app_list_strings['country_0'][56] = 'Czech Republic';
$app_list_strings['country_0'][57] = 'Denmark';
$app_list_strings['country_0'][58] = 'Djibouti';
$app_list_strings['country_0'][59] = 'Dominica';
$app_list_strings['country_0'][60] = 'Dominican Republic';
$app_list_strings['country_0'][61] = 'Ecuador';
$app_list_strings['country_0'][62] = 'Egypt';
$app_list_strings['country_0'][63] = 'El Salvador';
$app_list_strings['country_0'][64] = 'Equatorial Guinea';
$app_list_strings['country_0'][65] = 'Eritrea';
$app_list_strings['country_0'][66] = 'Estonia';
$app_list_strings['country_0'][67] = 'Ethiopia';
$app_list_strings['country_0'][68] = 'Fiji';
$app_list_strings['country_0'][69] = 'Finland';
$app_list_strings['country_0'][70] = 'France';
$app_list_strings['country_0'][71] = 'French Guinea';
$app_list_strings['country_0'][72] = 'Gabon';
$app_list_strings['country_0'][73] = 'Gambia';
$app_list_strings['country_0'][74] = 'Georgia';
$app_list_strings['country_0'][75] = 'Germany';
$app_list_strings['country_0'][76] = 'Ghana';
$app_list_strings['country_0'][77] = 'Gibraltar';
$app_list_strings['country_0'][78] = 'Greece';
$app_list_strings['country_0'][80] = 'Greenland';
$app_list_strings['country_0'][81] = 'Grenada';
$app_list_strings['country_0'][82] = 'Guatemala';
$app_list_strings['country_0'][83] = 'Guinea';
$app_list_strings['country_0'][84] = 'Guyana';
$app_list_strings['country_0'][85] = 'Haiti';
$app_list_strings['country_0'][86] = 'Honduras';
$app_list_strings['country_0'][87] = 'Hong Kong';
$app_list_strings['country_0'][88] = 'Hungary';
$app_list_strings['country_0'][89] = 'Iceland';
$app_list_strings['country_0'][90] = 'Iceland';
$app_list_strings['country_0'][91] = 'India';
$app_list_strings['country_0'][92] = 'Indonesia';
$app_list_strings['country_0'][93] = 'Iran';
$app_list_strings['country_0'][94] = 'Iraq';
$app_list_strings['country_0'][95] = 'Ireland';
$app_list_strings['country_0'][96] = 'Israel';
$app_list_strings['country_0'][97] = 'Italy';
$app_list_strings['country_0'][98] = 'Ivory Coast';
$app_list_strings['country_0'][99] = 'Jamaica';
$app_list_strings['country_0'][100] = 'Japan';
$app_list_strings['country_0'][101] = 'Jordan';
$app_list_strings['country_0'][102] = 'Kazakhstan';
$app_list_strings['country_0'][103] = 'Kenya';
$app_list_strings['country_0'][104] = 'Kuwait';
$app_list_strings['country_0'][105] = 'Kyrgyzstan';
$app_list_strings['country_0'][106] = 'Laos';
$app_list_strings['country_0'][107] = 'Latvia';
$app_list_strings['country_0'][108] = 'Lebanon';
$app_list_strings['country_0'][109] = 'Lesotho';
$app_list_strings['country_0'][110] = 'Liberia';
$app_list_strings['country_0'][111] = 'Libya';
$app_list_strings['country_0'][112] = 'Liechtenstein';
$app_list_strings['country_0'][113] = 'Lithuania';
$app_list_strings['country_0'][114] = 'Luxembourg';
$app_list_strings['country_0'][115] = 'Macedonia';
$app_list_strings['country_0'][116] = 'Madagascar';
$app_list_strings['country_0'][117] = 'Malawi';
$app_list_strings['country_0'][118] = 'Malaysia';
$app_list_strings['country_0'][119] = 'Maldives';
$app_list_strings['country_0'][120] = 'Mali';
$app_list_strings['country_0'][121] = 'Malta';
$app_list_strings['country_0'][122] = 'Marshall Islands';
$app_list_strings['country_0'][123] = 'Martinique';
$app_list_strings['country_0'][124] = 'Mauritania';
$app_list_strings['country_0'][125] = 'Mauritius';
$app_list_strings['country_0'][126] = 'Mexico';
$app_list_strings['country_0'][127] = 'Micronesia';
$app_list_strings['country_0'][128] = 'Moldova';
$app_list_strings['country_0'][129] = 'Monaco';
$app_list_strings['country_0'][130] = 'Mongolia';
$app_list_strings['country_0'][131] = 'Montenegro';
$app_list_strings['country_0'][132] = 'Monsterrat';
$app_list_strings['country_0'][133] = 'Morocco';
$app_list_strings['country_0'][134] = 'Mozambique';
$app_list_strings['country_0'][135] = 'Myanmar/Burma';
$app_list_strings['country_0'][136] = 'Namibia';
$app_list_strings['country_0'][137] = 'Nauru';
$app_list_strings['country_0'][138] = 'Nepal';
$app_list_strings['country_0'][139] = 'Netherlands';
$app_list_strings['country_0'][140] = 'New Zealand';
$app_list_strings['country_0'][141] = 'Nicaragua';
$app_list_strings['country_0'][142] = 'Niger';
$app_list_strings['country_0'][143] = 'Nigeria';
$app_list_strings['country_0'][144] = 'North Korea';
$app_list_strings['country_0'][145] = 'Norway';
$app_list_strings['country_0'][146] = 'Oman';
$app_list_strings['country_0'][147] = 'Pakistan';
$app_list_strings['country_0'][148] = 'Palau';
$app_list_strings['country_0'][149] = 'Panama';
$app_list_strings['country_0'][150] = 'Papua New Guinea';
$app_list_strings['country_0'][151] = 'Paraguay';
$app_list_strings['country_0'][152] = 'Peru';
$app_list_strings['country_0'][153] = 'Philippines';
$app_list_strings['country_0'][154] = 'Poland';
$app_list_strings['country_0'][155] = 'Portugal';
$app_list_strings['country_0'][156] = 'Puerto Rico';
$app_list_strings['country_0'][157] = 'Qatar';
$app_list_strings['country_0'][158] = 'Réunion';
$app_list_strings['country_0'][159] = 'Romania';
$app_list_strings['country_0'][160] = 'Russia';
$app_list_strings['country_0'][161] = 'Rwanda';
$app_list_strings['country_0'][162] = 'San Marino';
$app_list_strings['country_0'][163] = 'Saudi Arabia';
$app_list_strings['country_0'][164] = 'Senegal';
$app_list_strings['country_0'][165] = 'Serbia';
$app_list_strings['country_0'][166] = 'Seychelles';
$app_list_strings['country_0'][167] = 'Sierra Leone';
$app_list_strings['country_0'][168] = 'Singapore';
$app_list_strings['country_0'][169] = 'Slovak Republic';
$app_list_strings['country_0'][170] = 'Slovenia';
$app_list_strings['country_0'][171] = 'Solomon Islands';
$app_list_strings['country_0'][172] = 'Somalia';
$app_list_strings['country_0'][173] = 'South Africa';
$app_list_strings['country_0'][174] = 'South Korea';
$app_list_strings['country_0'][175] = 'Spain';
$app_list_strings['country_0'][176] = 'Sri Lanka';
$app_list_strings['country_0'][177] = 'St.Kittis and Nevis';
$app_list_strings['country_0'][178] = 'St.Lucia';
$app_list_strings['country_0'][179] = 'St. Maarten/St. Martin';
$app_list_strings['country_0'][180] = 'St.Vincent';
$app_list_strings['country_0'][181] = 'Sudan';
$app_list_strings['country_0'][182] = 'Suriname';
$app_list_strings['country_0'][183] = 'Swaziland';
$app_list_strings['country_0'][184] = 'Sweden';
$app_list_strings['country_0'][185] = 'Switzerland';
$app_list_strings['country_0'][186] = 'Syria';
$app_list_strings['country_0'][187] = 'Taiwan';
$app_list_strings['country_0'][188] = 'Tajikistan';
$app_list_strings['country_0'][189] = 'Tanzania';
$app_list_strings['country_0'][190] = 'Thailand';
$app_list_strings['country_0'][191] = 'Togo';
$app_list_strings['country_0'][192] = 'Tonga';
$app_list_strings['country_0'][193] = 'Tortola';
$app_list_strings['country_0'][194] = 'Trinidad and Tobago';
$app_list_strings['country_0'][195] = 'Tunisia';
$app_list_strings['country_0'][196] = 'Turkey';
$app_list_strings['country_0'][197] = 'Turkmenistan';
$app_list_strings['country_0'][198] = 'Tuvalu';
$app_list_strings['country_0'][199] = 'Uganda';
$app_list_strings['country_0'][200] = 'Ukraine';
$app_list_strings['country_0'][201] = 'United Arab Emirates';
$app_list_strings['country_0'][202] = 'United Kingdom';
$app_list_strings['country_0'][203] = 'Uruguay';
$app_list_strings['country_0'][204] = 'Uzbekistan';
$app_list_strings['country_0'][205] = 'Vanuatu';
$app_list_strings['country_0'][206] = 'Vatican City';
$app_list_strings['country_0'][207] = 'Venezuela';
$app_list_strings['country_0'][208] = 'Vietnam';
$app_list_strings['country_0'][209] = 'Western Sahara';
$app_list_strings['country_0'][210] = 'Yemen';
$app_list_strings['country_0'][211] = 'Zambia';
$app_list_strings['panel_type_list'][1] = 'Panel 1	';
$app_list_strings['panel_type_list'][2] = 'Panel 2	';
$app_list_strings['roof_type_list'][1] = 'Flat roof	';
$app_list_strings['roof_type_list'][2] = 'Ground Mounted	';
$app_list_strings['roof_type_list'][3] = 'In roof system	';
$app_list_strings['roof_type_list'][4] = 'Tile roof	';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['P1_Project'] = 'Project';
$app_list_strings['fse_picklist_list']['AB'] = 'Andre Basler	';
$app_list_strings['fse_picklist_list']['BH'] = 'Brandon Hanson	';
$app_list_strings['fse_picklist_list']['BJ'] = 'Bryan Johnson	';
$app_list_strings['fse_picklist_list']['CC'] = 'Chris Carrera	';
$app_list_strings['fse_picklist_list']['EDLT'] = 'Eddie De La Torre	';
$app_list_strings['fse_picklist_list']['EL'] = 'Eric Larson	';
$app_list_strings['fse_picklist_list']['FR'] = 'Francisco Reyes	';
$app_list_strings['fse_picklist_list']['JD'] = 'Joe Diette	';
$app_list_strings['fse_picklist_list']['KR'] = 'Kristopher Roscoe	';
$app_list_strings['fse_picklist_list']['SH'] = 'Sara Hambleton	';
$app_list_strings['fse_picklist_list']['TJ'] = 'Travis Jones	';
$app_list_strings['fse_picklist_list']['AS'] = 'Adam Schilling	';
$app_list_strings['fse_picklist_list']['CK'] = 'Cliff Kalinowski	';
$app_list_strings['fse_picklist_list']['TB'] = 'Tripp Bannister	';
$app_list_strings['fse_picklist_list']['YP'] = 'Yaron Pony	';
$app_list_strings['fse_picklist_list']['AR'] = 'Adi Raziel	';
$app_list_strings['fse_email_list'][1] = 'andre.b@solaredge.com	';
$app_list_strings['fse_email_list'][2] = 'Brandon.Hanson@solaredge.com	';
$app_list_strings['fse_email_list'][3] = 'bryan.j@solaredge.com	';
$app_list_strings['fse_email_list'][4] = 'Chris.Carrera@solaredge.com	';
$app_list_strings['fse_email_list'][5] = 'Eddie.DeLaTorre@solaredge.com	';
$app_list_strings['fse_email_list'][6] = 'Eric.Larson@solaredge.com	';
$app_list_strings['fse_email_list'][7] = 'Francisco.Reyes@solaredge.com	';
$app_list_strings['fse_email_list'][8] = 'joe.d@solaredge.com	';
$app_list_strings['fse_email_list'][9] = 'kristopher.r@solaredge.com	';
$app_list_strings['fse_email_list'][10] = 'Sara.Hambleton@solaredge.com	';
$app_list_strings['fse_email_list'][11] = 'Travis.Jones@solaredge.com	';
$app_list_strings['fse_email_list'][12] = 'Adam.Schilling@solaredge.com	';
$app_list_strings['fse_email_list'][13] = 'Cliff.Kalinowski@solaredge.com	';
$app_list_strings['fse_email_list'][14] = 'Tripp.Bannister@solaredge.com	';
$app_list_strings['fse_email_list'][15] = 'Yaron.Pony@solaredge.com	';
$app_list_strings['fse_email_list'][16] = 'Adi.raziel@solaredge.com	';
$app_list_strings['project_status_list']['PreConstruction'] = 'Pre-Construction	';
$app_list_strings['project_status_list']['Construction'] = 'Construction';
$app_list_strings['project_status_list']['SiteEvaluation'] = 'Site Evaluation';
$app_list_strings['project_status_list']['Complete'] = 'Complete';
$app_list_strings['project_status_list']['Abandoned'] = 'Abandoned';
$app_list_strings['project_status_list']['tba'] = 'To be Assigned	';
$app_list_strings['country_0'][1] = 'United States';
$app_list_strings['country_0'][2] = 'Afghanistan';
$app_list_strings['country_0'][3] = 'Albania';
$app_list_strings['country_0'][4] = 'Algeria';
$app_list_strings['country_0'][5] = 'American Samoa';
$app_list_strings['country_0'][6] = 'Andorra';
$app_list_strings['country_0'][7] = 'Angola';
$app_list_strings['country_0'][8] = 'Anguilla';
$app_list_strings['country_0'][9] = 'Antigua';
$app_list_strings['country_0'][10] = 'Argentina';
$app_list_strings['country_0'][11] = 'Armenia';
$app_list_strings['country_0'][12] = 'Aruba';
$app_list_strings['country_0'][13] = 'Australia';
$app_list_strings['country_0'][14] = 'Austria';
$app_list_strings['country_0'][15] = 'Azerbaijan';
$app_list_strings['country_0'][16] = 'Bahamas';
$app_list_strings['country_0'][17] = 'Bahrain';
$app_list_strings['country_0'][18] = 'Bangladesh';
$app_list_strings['country_0'][19] = 'Barbados';
$app_list_strings['country_0'][20] = 'Barbuda';
$app_list_strings['country_0'][21] = 'Belarus';
$app_list_strings['country_0'][22] = 'Belgium';
$app_list_strings['country_0'][23] = 'Belize';
$app_list_strings['country_0'][24] = 'Benin';
$app_list_strings['country_0'][25] = 'Bermuda';
$app_list_strings['country_0'][26] = 'Bhutan';
$app_list_strings['country_0'][27] = 'Bolivia';
$app_list_strings['country_0'][28] = 'Bonaire';
$app_list_strings['country_0'][29] = 'Bosnia-Herzegovina';
$app_list_strings['country_0'][30] = 'Botswana';
$app_list_strings['country_0'][31] = 'Brazil';
$app_list_strings['country_0'][32] = 'British Virgin Island';
$app_list_strings['country_0'][33] = 'Brunei';
$app_list_strings['country_0'][34] = 'Bulgaria';
$app_list_strings['country_0'][35] = 'Burkina Faso';
$app_list_strings['country_0'][36] = 'Burundi';
$app_list_strings['country_0'][37] = 'Cambodia';
$app_list_strings['country_0'][38] = 'Cameroon';
$app_list_strings['country_0'][39] = 'Canada';
$app_list_strings['country_0'][40] = 'Cape Verde';
$app_list_strings['country_0'][41] = 'Cayman Island';
$app_list_strings['country_0'][42] = 'Central African Republic';
$app_list_strings['country_0'][43] = 'Chad';
$app_list_strings['country_0'][44] = 'Channel Islands';
$app_list_strings['country_0'][45] = 'Chile';
$app_list_strings['country_0'][46] = 'China';
$app_list_strings['country_0'][47] = 'Colombia';
$app_list_strings['country_0'][48] = 'Comoros';
$app_list_strings['country_0'][49] = 'Congo';
$app_list_strings['country_0'][50] = 'Congo - Brazzaville';
$app_list_strings['country_0'][51] = 'Costa Rica';
$app_list_strings['country_0'][52] = 'Côte d';
$app_list_strings['country_0'][53] = 'Croatia';
$app_list_strings['country_0'][54] = 'Cuba';
$app_list_strings['country_0'][55] = 'Cyprus';
$app_list_strings['country_0'][56] = 'Czech Republic';
$app_list_strings['country_0'][57] = 'Denmark';
$app_list_strings['country_0'][58] = 'Djibouti';
$app_list_strings['country_0'][59] = 'Dominica';
$app_list_strings['country_0'][60] = 'Dominican Republic';
$app_list_strings['country_0'][61] = 'Ecuador';
$app_list_strings['country_0'][62] = 'Egypt';
$app_list_strings['country_0'][63] = 'El Salvador';
$app_list_strings['country_0'][64] = 'Equatorial Guinea';
$app_list_strings['country_0'][65] = 'Eritrea';
$app_list_strings['country_0'][66] = 'Estonia';
$app_list_strings['country_0'][67] = 'Ethiopia';
$app_list_strings['country_0'][68] = 'Fiji';
$app_list_strings['country_0'][69] = 'Finland';
$app_list_strings['country_0'][70] = 'France';
$app_list_strings['country_0'][71] = 'French Guinea';
$app_list_strings['country_0'][72] = 'Gabon';
$app_list_strings['country_0'][73] = 'Gambia';
$app_list_strings['country_0'][74] = 'Georgia';
$app_list_strings['country_0'][75] = 'Germany';
$app_list_strings['country_0'][76] = 'Ghana';
$app_list_strings['country_0'][77] = 'Gibraltar';
$app_list_strings['country_0'][78] = 'Greece';
$app_list_strings['country_0'][80] = 'Greenland';
$app_list_strings['country_0'][81] = 'Grenada';
$app_list_strings['country_0'][82] = 'Guatemala';
$app_list_strings['country_0'][83] = 'Guinea';
$app_list_strings['country_0'][84] = 'Guyana';
$app_list_strings['country_0'][85] = 'Haiti';
$app_list_strings['country_0'][86] = 'Honduras';
$app_list_strings['country_0'][87] = 'Hong Kong';
$app_list_strings['country_0'][88] = 'Hungary';
$app_list_strings['country_0'][89] = 'Iceland';
$app_list_strings['country_0'][90] = 'Iceland';
$app_list_strings['country_0'][91] = 'India';
$app_list_strings['country_0'][92] = 'Indonesia';
$app_list_strings['country_0'][93] = 'Iran';
$app_list_strings['country_0'][94] = 'Iraq';
$app_list_strings['country_0'][95] = 'Ireland';
$app_list_strings['country_0'][96] = 'Israel';
$app_list_strings['country_0'][97] = 'Italy';
$app_list_strings['country_0'][98] = 'Ivory Coast';
$app_list_strings['country_0'][99] = 'Jamaica';
$app_list_strings['country_0'][100] = 'Japan';
$app_list_strings['country_0'][101] = 'Jordan';
$app_list_strings['country_0'][102] = 'Kazakhstan';
$app_list_strings['country_0'][103] = 'Kenya';
$app_list_strings['country_0'][104] = 'Kuwait';
$app_list_strings['country_0'][105] = 'Kyrgyzstan';
$app_list_strings['country_0'][106] = 'Laos';
$app_list_strings['country_0'][107] = 'Latvia';
$app_list_strings['country_0'][108] = 'Lebanon';
$app_list_strings['country_0'][109] = 'Lesotho';
$app_list_strings['country_0'][110] = 'Liberia';
$app_list_strings['country_0'][111] = 'Libya';
$app_list_strings['country_0'][112] = 'Liechtenstein';
$app_list_strings['country_0'][113] = 'Lithuania';
$app_list_strings['country_0'][114] = 'Luxembourg';
$app_list_strings['country_0'][115] = 'Macedonia';
$app_list_strings['country_0'][116] = 'Madagascar';
$app_list_strings['country_0'][117] = 'Malawi';
$app_list_strings['country_0'][118] = 'Malaysia';
$app_list_strings['country_0'][119] = 'Maldives';
$app_list_strings['country_0'][120] = 'Mali';
$app_list_strings['country_0'][121] = 'Malta';
$app_list_strings['country_0'][122] = 'Marshall Islands';
$app_list_strings['country_0'][123] = 'Martinique';
$app_list_strings['country_0'][124] = 'Mauritania';
$app_list_strings['country_0'][125] = 'Mauritius';
$app_list_strings['country_0'][126] = 'Mexico';
$app_list_strings['country_0'][127] = 'Micronesia';
$app_list_strings['country_0'][128] = 'Moldova';
$app_list_strings['country_0'][129] = 'Monaco';
$app_list_strings['country_0'][130] = 'Mongolia';
$app_list_strings['country_0'][131] = 'Montenegro';
$app_list_strings['country_0'][132] = 'Monsterrat';
$app_list_strings['country_0'][133] = 'Morocco';
$app_list_strings['country_0'][134] = 'Mozambique';
$app_list_strings['country_0'][135] = 'Myanmar/Burma';
$app_list_strings['country_0'][136] = 'Namibia';
$app_list_strings['country_0'][137] = 'Nauru';
$app_list_strings['country_0'][138] = 'Nepal';
$app_list_strings['country_0'][139] = 'Netherlands';
$app_list_strings['country_0'][140] = 'New Zealand';
$app_list_strings['country_0'][141] = 'Nicaragua';
$app_list_strings['country_0'][142] = 'Niger';
$app_list_strings['country_0'][143] = 'Nigeria';
$app_list_strings['country_0'][144] = 'North Korea';
$app_list_strings['country_0'][145] = 'Norway';
$app_list_strings['country_0'][146] = 'Oman';
$app_list_strings['country_0'][147] = 'Pakistan';
$app_list_strings['country_0'][148] = 'Palau';
$app_list_strings['country_0'][149] = 'Panama';
$app_list_strings['country_0'][150] = 'Papua New Guinea';
$app_list_strings['country_0'][151] = 'Paraguay';
$app_list_strings['country_0'][152] = 'Peru';
$app_list_strings['country_0'][153] = 'Philippines';
$app_list_strings['country_0'][154] = 'Poland';
$app_list_strings['country_0'][155] = 'Portugal';
$app_list_strings['country_0'][156] = 'Puerto Rico';
$app_list_strings['country_0'][157] = 'Qatar';
$app_list_strings['country_0'][158] = 'Réunion';
$app_list_strings['country_0'][159] = 'Romania';
$app_list_strings['country_0'][160] = 'Russia';
$app_list_strings['country_0'][161] = 'Rwanda';
$app_list_strings['country_0'][162] = 'San Marino';
$app_list_strings['country_0'][163] = 'Saudi Arabia';
$app_list_strings['country_0'][164] = 'Senegal';
$app_list_strings['country_0'][165] = 'Serbia';
$app_list_strings['country_0'][166] = 'Seychelles';
$app_list_strings['country_0'][167] = 'Sierra Leone';
$app_list_strings['country_0'][168] = 'Singapore';
$app_list_strings['country_0'][169] = 'Slovak Republic';
$app_list_strings['country_0'][170] = 'Slovenia';
$app_list_strings['country_0'][171] = 'Solomon Islands';
$app_list_strings['country_0'][172] = 'Somalia';
$app_list_strings['country_0'][173] = 'South Africa';
$app_list_strings['country_0'][174] = 'South Korea';
$app_list_strings['country_0'][175] = 'Spain';
$app_list_strings['country_0'][176] = 'Sri Lanka';
$app_list_strings['country_0'][177] = 'St.Kittis and Nevis';
$app_list_strings['country_0'][178] = 'St.Lucia';
$app_list_strings['country_0'][179] = 'St. Maarten/St. Martin';
$app_list_strings['country_0'][180] = 'St.Vincent';
$app_list_strings['country_0'][181] = 'Sudan';
$app_list_strings['country_0'][182] = 'Suriname';
$app_list_strings['country_0'][183] = 'Swaziland';
$app_list_strings['country_0'][184] = 'Sweden';
$app_list_strings['country_0'][185] = 'Switzerland';
$app_list_strings['country_0'][186] = 'Syria';
$app_list_strings['country_0'][187] = 'Taiwan';
$app_list_strings['country_0'][188] = 'Tajikistan';
$app_list_strings['country_0'][189] = 'Tanzania';
$app_list_strings['country_0'][190] = 'Thailand';
$app_list_strings['country_0'][191] = 'Togo';
$app_list_strings['country_0'][192] = 'Tonga';
$app_list_strings['country_0'][193] = 'Tortola';
$app_list_strings['country_0'][194] = 'Trinidad and Tobago';
$app_list_strings['country_0'][195] = 'Tunisia';
$app_list_strings['country_0'][196] = 'Turkey';
$app_list_strings['country_0'][197] = 'Turkmenistan';
$app_list_strings['country_0'][198] = 'Tuvalu';
$app_list_strings['country_0'][199] = 'Uganda';
$app_list_strings['country_0'][200] = 'Ukraine';
$app_list_strings['country_0'][201] = 'United Arab Emirates';
$app_list_strings['country_0'][202] = 'United Kingdom';
$app_list_strings['country_0'][203] = 'Uruguay';
$app_list_strings['country_0'][204] = 'Uzbekistan';
$app_list_strings['country_0'][205] = 'Vanuatu';
$app_list_strings['country_0'][206] = 'Vatican City';
$app_list_strings['country_0'][207] = 'Venezuela';
$app_list_strings['country_0'][208] = 'Vietnam';
$app_list_strings['country_0'][209] = 'Western Sahara';
$app_list_strings['country_0'][210] = 'Yemen';
$app_list_strings['country_0'][211] = 'Zambia';
$app_list_strings['state_0'][1] = 'Alabama';
$app_list_strings['state_0'][2] = 'Alaska';
$app_list_strings['state_0'][3] = 'Arizona';
$app_list_strings['state_0'][4] = 'Arkansas';
$app_list_strings['state_0'][5] = 'California';
$app_list_strings['state_0'][6] = 'Colorado';
$app_list_strings['state_0'][7] = 'Connecticut';
$app_list_strings['state_0'][8] = 'Delaware';
$app_list_strings['state_0'][9] = 'Florida';
$app_list_strings['state_0'][10] = 'Georgia';
$app_list_strings['state_0'][11] = 'Hawaii';
$app_list_strings['state_0'][12] = 'Idaho';
$app_list_strings['state_0'][13] = 'Illionois';
$app_list_strings['state_0'][14] = 'Indiana';
$app_list_strings['state_0'][15] = 'Iowa';
$app_list_strings['state_0'][16] = 'Kansas';
$app_list_strings['state_0'][17] = 'Kentucky';
$app_list_strings['state_0'][18] = 'Louisiana';
$app_list_strings['state_0'][19] = 'Maine';
$app_list_strings['state_0'][20] = 'Maryland';
$app_list_strings['state_0'][21] = 'Massachusetts';
$app_list_strings['state_0'][22] = 'Michigan';
$app_list_strings['state_0'][23] = 'Minnesota';
$app_list_strings['state_0'][24] = 'Mississippi';
$app_list_strings['state_0'][25] = 'Missouri';
$app_list_strings['state_0'][26] = 'Montana';
$app_list_strings['state_0'][27] = 'Nebraska';
$app_list_strings['state_0'][28] = 'Nevada';
$app_list_strings['state_0'][29] = 'New Hampshire';
$app_list_strings['state_0'][30] = 'New Jersey';
$app_list_strings['state_0'][31] = 'New Mexico';
$app_list_strings['state_0'][32] = 'New York';
$app_list_strings['state_0'][33] = 'North Carolina';
$app_list_strings['state_0'][34] = 'North Dakota';
$app_list_strings['state_0'][35] = 'Ohio';
$app_list_strings['state_0'][36] = 'Oklahoma';
$app_list_strings['state_0'][37] = 'Oregon';
$app_list_strings['state_0'][38] = 'Pennsylvania';
$app_list_strings['state_0'][39] = 'Rhode Island';
$app_list_strings['state_0'][40] = 'South Carolina';
$app_list_strings['state_0'][41] = 'South Dakota';
$app_list_strings['state_0'][42] = 'Tennessee';
$app_list_strings['state_0'][43] = 'Texas';
$app_list_strings['state_0'][44] = 'Utah';
$app_list_strings['state_0'][45] = 'Vermont';
$app_list_strings['state_0'][46] = 'Virginia';
$app_list_strings['state_0'][47] = 'Washington';
$app_list_strings['state_0'][48] = 'West Virginia';
$app_list_strings['state_0'][49] = 'Wisconsin';
$app_list_strings['state_0'][50] = 'Wyoming';
$app_list_strings['state_0'][51] = 'District of Colombia';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Approvals'] = 'Approvals';
$app_list_strings['dtbc_approval_modules'][''] = '';
$app_list_strings['dtbc_approval_email_template_list'][''] = '';

 
$app_list_strings['business_entity_list'] = array (
  1 => 'Ltd',
  2 => 'Inc',
  3 => 'GMBH',
);$app_list_strings['Elastic_boost_options'] = array (
  0 => 'Australian Capital Territory ',
  1 => 'New South Wales ',
  2 => 'New Zealand ',
  3 => 'Northern Territory ',
  4 => 'Queensland',
  5 => 'South Australia',
  6 => 'Tasmania',
  7 => 'Victoria',
  8 => 'Western Australia',
);$app_list_strings['activity_focus_0'] = array (
  1 => 'PV',
  2 => 'Sanitary',
  3 => 'Healing',
  4 => 'Electrical Installations ',
);$app_list_strings['best_selling_inverter_0'] = array (
  1 => 'ABB',
  2 => 'Enphase',
  3 => 'Fronius',
  4 => 'Huawei',
  5 => 'Kaco',
  6 => 'PowerOne',
  7 => 'Samil',
  8 => 'SMA',
  9 => 'SolarEdge',
  10 => 'Sungrow',
);$app_list_strings['crews_c_list'] = array (
  0 => '0',
  1 => '1',
  2 => '2-3',
  3 => '4-6',
  4 => '7-9',
  5 => '10+',
);$app_list_strings['state_0'] = array (
  1 => 'Alabama',
  2 => 'Alaska',
  3 => 'Arizona',
  4 => 'Arkansas',
  5 => 'California',
  6 => 'Colorado',
  7 => 'Connecticut',
  8 => 'Delaware',
  9 => 'Florida',
  10 => 'Georgia',
  11 => 'Hawaii',
  12 => 'Idaho',
  13 => 'Illionois',
  14 => 'Indiana',
  15 => 'Iowa',
  16 => 'Kansas',
  17 => 'Kentucky',
  18 => 'Louisiana',
  19 => 'Maine',
  20 => 'Maryland',
  21 => 'Massachusetts',
  22 => 'Michigan',
  23 => 'Minnesota',
  24 => 'Mississippi',
  25 => 'Missouri',
  26 => 'Montana',
  27 => 'Nebraska',
  28 => 'Nevada',
  29 => 'New Hampshire',
  30 => 'New Jersey',
  31 => 'New Mexico',
  32 => 'New York',
  33 => 'North Carolina',
  34 => 'North Dakota',
  35 => 'Ohio',
  36 => 'Oklahoma',
  37 => 'Oregon',
  38 => 'Pennsylvania',
  39 => 'Rhode Island',
  40 => 'South Carolina',
  41 => 'South Dakota',
  42 => 'Tennessee',
  43 => 'Texas',
  44 => 'Utah',
  45 => 'Vermont',
  46 => 'Virginia',
  47 => 'Washington',
  48 => 'West Virginia',
  49 => 'Wisconsin',
  50 => 'Wyoming',
  51 => 'District of Colombia',
);$app_list_strings['us_region_list'] = array (
  1 => 'Central',
  2 => 'East',
  3 => 'West',
);$app_list_strings['inverter_brands_list'] = array (
  1 => 'ABB',
  2 => 'Enphase',
  3 => 'Fronius',
  4 => 'Huawei',
  5 => 'Kaco',
  6 => 'PowerOne',
  7 => 'Samil',
  8 => 'SMA',
  9 => 'Sungrow',
  10 => 'SolarEdge',
  11 => 'Kostal',
  12 => 'Nedap',
  13 => 'Steca',
  14 => 'Delta',
  15 => 'Solarmax',
  16 => 'Omnik',
  17 => 'Growatt',
);$app_list_strings['inverter_competitors_list'] = array (
  1 => 'ABB',
  2 => 'AP Systems',
  3 => 'Chilicon',
  4 => 'Chint',
  5 => 'Enphase',
  6 => 'Fronius',
  7 => 'Ginlong',
  8 => 'Growatt',
  9 => 'HiQ',
  10 => 'Huawei',
  11 => 'Kaco',
  12 => 'Outback',
  13 => 'Samil',
  14 => 'Schneider',
  15 => 'SEDG 100%',
  16 => 'SMA',
  17 => 'Solectria',
  18 => 'Sungrow',
  19 => 'SunPower',
  20 => 'Unknown',
);$app_list_strings['financing_partners_list'] = array (
  1 => 'Blue Wave',
  2 => 'Cal First / Renew Financial',
  3 => 'Dividend Solar',
  4 => 'EGIA',
  5 => 'Enerbank',
  6 => 'GreenSky',
  7 => 'Mass solar loan',
  8 => 'Matador',
  9 => 'Mosaic',
  10 => 'Renovate America',
  11 => 'Spruce',
  12 => 'Sunbanc',
  13 => 'Sungage',
  14 => 'Sunlender / RAMCO',
  15 => 'Sunlight Financial',
  16 => 'Sunnova',
  17 => 'SunPower Lease',
  18 => 'Sunrun',
  19 => 'Ygrene',
);$app_list_strings['canadian_province_list'] = array (
  1 => 'Alberta',
  2 => 'British Columbia',
  3 => 'Manitoba',
  4 => 'New Brunswick',
  5 => 'Newfoundland and Labrador',
  6 => 'Nova Scotia ',
  7 => 'Nunavut',
  8 => 'Ontario',
  9 => 'Prince Edward Island ',
  10 => 'Quebec',
  11 => 'Saskatchewan',
  12 => 'Yukon',
);$app_list_strings['se_training_visited_list'] = array (
  1 => 'Yes',
  2 => 'No',
);$app_list_strings['distributor_status_list'] = array (
  1 => 'Open',
  2 => 'Contacted',
  3 => 'Won',
  4 => 'Lost',
);$app_list_strings['customer_funnel_list'] = array (
  1 => 'Discovery',
  2 => 'Education',
  3 => 'Design',
  4 => 'Quote',
  5 => 'Close',
);$app_list_strings['country_0'] = array (
  1 => 'United States',
  2 => 'Afghanistan',
  3 => 'Albania',
  4 => 'Algeria',
  5 => 'American Samoa',
  6 => 'Andorra',
  7 => 'Angola',
  8 => 'Anguilla',
  9 => 'Antigua',
  10 => 'Argentina',
  11 => 'Armenia',
  12 => 'Aruba',
  13 => 'Australia',
  14 => 'Austria',
  15 => 'Azerbaijan',
  16 => 'Bahamas',
  17 => 'Bahrain',
  18 => 'Bangladesh',
  19 => 'Barbados',
  20 => 'Barbuda',
  21 => 'Belarus',
  22 => 'Belgium',
  23 => 'Belize',
  24 => 'Benin',
  25 => 'Bermuda',
  26 => 'Bhutan',
  27 => 'Bolivia',
  28 => 'Bonaire',
  29 => 'Bosnia-Herzegovina',
  30 => 'Botswana',
  31 => 'Brazil',
  32 => 'British Virgin Island',
  33 => 'Brunei',
  34 => 'Bulgaria',
  35 => 'Burkina Faso',
  36 => 'Burundi',
  37 => 'Cambodia',
  38 => 'Cameroon',
  39 => 'Canada',
  40 => 'Cape Verde',
  41 => 'Cayman Island',
  42 => 'Central African Republic',
  43 => 'Chad',
  44 => 'Channel Islands',
  45 => 'Chile',
  46 => 'China',
  47 => 'Colombia',
  48 => 'Comoros',
  49 => 'Congo',
  50 => 'Congo - Brazzaville',
  51 => 'Costa Rica',
  52 => 'Côte d',
  53 => 'Croatia',
  54 => 'Cuba',
  55 => 'Cyprus',
  56 => 'Czech Republic',
  57 => 'Denmark',
  58 => 'Djibouti',
  59 => 'Dominica',
  60 => 'Dominican Republic',
  61 => 'Ecuador',
  62 => 'Egypt',
  63 => 'El Salvador',
  64 => 'Equatorial Guinea',
  65 => 'Eritrea',
  66 => 'Estonia',
  67 => 'Ethiopia',
  68 => 'Fiji',
  69 => 'Finland',
  70 => 'France',
  71 => 'French Guinea',
  72 => 'Gabon',
  73 => 'Gambia',
  74 => 'Georgia',
  75 => 'Germany',
  76 => 'Ghana',
  77 => 'Gibraltar',
  78 => 'Greece',
  80 => 'Greenland',
  81 => 'Grenada',
  82 => 'Guatemala',
  83 => 'Guinea',
  84 => 'Guyana',
  85 => 'Haiti',
  86 => 'Honduras',
  87 => 'Hong Kong',
  88 => 'Hungary',
  89 => 'Iceland',
  90 => 'Iceland',
  91 => 'India',
  92 => 'Indonesia',
  93 => 'Iran',
  94 => 'Iraq',
  95 => 'Ireland',
  96 => 'Israel',
  97 => 'Italy',
  98 => 'Ivory Coast',
  99 => 'Jamaica',
  100 => 'Japan',
  101 => 'Jordan',
  102 => 'Kazakhstan',
  103 => 'Kenya',
  104 => 'Kuwait',
  105 => 'Kyrgyzstan',
  106 => 'Laos',
  107 => 'Latvia',
  108 => 'Lebanon',
  109 => 'Lesotho',
  110 => 'Liberia',
  111 => 'Libya',
  112 => 'Liechtenstein',
  113 => 'Lithuania',
  114 => 'Luxembourg',
  115 => 'Macedonia',
  116 => 'Madagascar',
  117 => 'Malawi',
  118 => 'Malaysia',
  119 => 'Maldives',
  120 => 'Mali',
  121 => 'Malta',
  122 => 'Marshall Islands',
  123 => 'Martinique',
  124 => 'Mauritania',
  125 => 'Mauritius',
  126 => 'Mexico',
  127 => 'Micronesia',
  128 => 'Moldova',
  129 => 'Monaco',
  130 => 'Mongolia',
  131 => 'Montenegro',
  132 => 'Monsterrat',
  133 => 'Morocco',
  134 => 'Mozambique',
  135 => 'Myanmar/Burma',
  136 => 'Namibia',
  137 => 'Nauru',
  138 => 'Nepal',
  139 => 'Netherlands',
  140 => 'New Zealand',
  141 => 'Nicaragua',
  142 => 'Niger',
  143 => 'Nigeria',
  144 => 'North Korea',
  145 => 'Norway',
  146 => 'Oman',
  147 => 'Pakistan',
  148 => 'Palau',
  149 => 'Panama',
  150 => 'Papua New Guinea',
  151 => 'Paraguay',
  152 => 'Peru',
  153 => 'Philippines',
  154 => 'Poland',
  155 => 'Portugal',
  156 => 'Puerto Rico',
  157 => 'Qatar',
  158 => 'Réunion',
  159 => 'Romania',
  160 => 'Russia',
  161 => 'Rwanda',
  162 => 'San Marino',
  163 => 'Saudi Arabia',
  164 => 'Senegal',
  165 => 'Serbia',
  166 => 'Seychelles',
  167 => 'Sierra Leone',
  168 => 'Singapore',
  169 => 'Slovak Republic',
  170 => 'Slovenia',
  171 => 'Solomon Islands',
  172 => 'Somalia',
  173 => 'South Africa',
  174 => 'South Korea',
  175 => 'Spain',
  176 => 'Sri Lanka',
  177 => 'St.Kittis and Nevis',
  178 => 'St.Lucia',
  179 => 'St. Maarten/St. Martin',
  180 => 'St.Vincent',
  181 => 'Sudan',
  182 => 'Suriname',
  183 => 'Swaziland',
  184 => 'Sweden',
  185 => 'Switzerland',
  186 => 'Syria',
  187 => 'Taiwan',
  188 => 'Tajikistan',
  189 => 'Tanzania',
  190 => 'Thailand',
  191 => 'Togo',
  192 => 'Tonga',
  193 => 'Tortola',
  194 => 'Trinidad and Tobago',
  195 => 'Tunisia',
  196 => 'Turkey',
  197 => 'Turkmenistan',
  198 => 'Tuvalu',
  199 => 'Uganda',
  200 => 'Ukraine',
  201 => 'United Arab Emirates',
  202 => 'United Kingdom',
  203 => 'Uruguay',
  204 => 'Uzbekistan',
  205 => 'Vanuatu',
  206 => 'Vatican City',
  207 => 'Venezuela',
  208 => 'Vietnam',
  209 => 'Western Sahara',
  210 => 'Yemen',
  211 => 'Zambia',
);$app_list_strings['active_spa_c_list'] = array (
  1 => 'COMPLETE-GROWTH ',
  2 => 'COMPLETE-LOST',
  3 => 'COMPLETE -NEW BUSINESS',
  4 => 'COMPLETE-RETAIN ',
  5 => 'NOT NEEDED',
  6 => 'NOT PROPOSED ',
  7 => 'PROPOSED-REJECTED ',
);$app_list_strings['service_level_list'] = array (
  1 => 'VIP',
  2 => 'EPC',
  3 => 'Standard',
  4 => 'Finance Hold',
  5 => 'Service Hold',
);$app_list_strings['inactive_reason_list'] = array (
  1 => 'NA: ',
  2 => 'NA: 3rd party Selection',
  3 => 'NA: Customer Selection',
  4 => 'NA: Invalid contact',
  5 => 'NA: Not Monitoring',
  6 => 'NA: Off-Grid Installer',
  7 => 'NA: Out of Business',
  8 => 'NA: Slow Business',
  9 => 'Product Issues: Communications',
  10 => 'Product Issues: Inverter',
  11 => 'Product Issues: Optimizer',
  12 => 'Product Issues: SEDG Service',
  13 => 'Training: Enphase Customer',
  14 => 'Training: General',
  15 => 'TRAINING: Niche',
  16 => 'Training: SunPower',
  17 => 'Training: Troubleshooting',
);$app_list_strings['certified_installer_tesla_list'] = array (
  1 => 'Yes',
  2 => 'No',
);$app_list_strings['region_list'] = array (
  1 => 'North America',
  2 => 'Italy',
  3 => 'Netherlands',
  4 => 'Israel',
  5 => 'UK',
  6 => 'China',
  7 => 'Australia',
  8 => 'Japan',
  9 => 'ROW',
  10 => 'MEA',
  11 => 'DACH',
  12 => 'France',
  13 => 'APAC N',
  14 => 'APAC J',
  15 => 'ROE',
);$app_list_strings['main_panel_brands_list'] = array (
  1 => 'Yingli',
  2 => 'Trina',
  3 => 'Aleo Solar ',
  4 => 'Solar Frontier ',
  5 => 'Jinko',
  6 => 'Kioto',
);$app_list_strings['tier_list'] = array (
  1 => 'VIP',
  2 => 'Strategetic',
  3 => 'Long Tail',
  4 => 'Tier 2',
);$app_list_strings['csr_list'] = array (
  1 => 'Marry Ann Elvina',
  2 => 'Cynthia Dolan',
  3 => 'Sheri Martin',
  4 => 'Jodie Lea',
  5 => 'Florence Cardozo',
  6 => 'Lianne Broyles',
  7 => 'Carolyn Springer',
);$app_list_strings['state__c_list'] = array (
  'New_Jersey' => 'New Jersey',
  'New_York' => 'New York',
  'Texas' => 'Texas',
  'Virginia' => 'Virginia',
  'Nevada' => 'Nevada',
);$app_list_strings['segment_list'] = array (
  1 => 'Residential',
  2 => 'Commercial',
  3 => 'Residential and Commercial',
  4 => 'Utility',
);$app_list_strings['type_of_installation_list'] = array (
  1 => 'Residential',
  2 => 'Commercial',
  3 => 'Utilities',
  4 => 'Retrofitting',
  6 => 'Enterprise',
  5 => 'All',
);$app_list_strings['site_type_list'] = array (
  1 => 'Residential',
  2 => 'Commercial',
);$app_list_strings['tier_2_region_list'] = array (
  1 => 'EU',
  2 => 'APAC',
  3 => 'Japan',
  4 => 'IL',
  5 => '-',
);$app_list_strings['doa_type_list'] = array (
  1 => 'Reliability',
  2 => 'Quality',
);$app_list_strings['ibolt_status_list'] = array (
  1 => 'Start',
  2 => 'Success',
  3 => 'Error',
  4 => 'Warning',
);$app_list_strings['created_by_address_list'] = array (
  1 => 'Axiplus',
  2 => 'canadiansolar',
);$app_list_strings['battery_ibolt_status_list'] = array (
  1 => 'Start',
  2 => 'Success',
  3 => 'Error',
  4 => 'Warning',
);$app_list_strings['installer_s_grade_list'] = array (
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
  7 => '7',
  8 => '8',
  9 => '9',
  10 => '10',
);$app_list_strings['panel_list'] = array (
  1 => 'Advanceng/Megasol',
  2 => 'Axitec',
  3 => 'Holleis',
  4 => 'JA',
  5 => 'Jinko',
  6 => 'Jinko',
  7 => 'Kioto',
  8 => 'PhonoSolar',
  9 => 'Solon',
  10 => 'Suisse Soleil',
  11 => 'Sveigaard',
  12 => 'Upsolar',
  13 => 'Viasolis',
  14 => 'Canadian Solar',
  15 => 'Other',
);$app_list_strings['case_sub_status_list'] = array (
  1 => 'Escalated to field engineer',
  2 => 'Escalated to IS',
  3 => 'Escalated to Logistics',
  4 => 'Escalated to Manager',
  5 => 'Escalated to PM',
  6 => 'Escalated to QA',
  7 => 'Escalated to R&D',
  8 => 'Escalated to Tier 2',
  9 => 'Escalated to Tier 3',
  10 => 'RMA',
  11 => 'Site builders',
  12 => 'Site Visit Completed',
  13 => 'PR opened',
  14 => 'PR approved',
  15 => 'not approved',
  16 => 'in process',
  17 => 'Payment approved',
  18 => 'Payment not approved',
  19 => 'Waiting for Tier 1',
  20 => 'Waiting for Tier 2',
  21 => 'Tier 2 escalation completed',
);$app_list_strings['reason_for_additional_rma_list'] = array (
  1 => 'Previous RMA did not solve the issue ',
  2 => 'Wrong shipping address ',
  3 => 'First part did not solve the problem ',
  4 => 'Wrong part was sent ',
  5 => 'Additional failure on site, DOA ',
);$app_list_strings['address_type_list'] = array (
  1 => 'RMA Address',
  2 => 'Shipping Address for Gift ',
);$app_list_strings['tier3_escalation_owner_list'] = array (
  1 => 'Tier 3',
  2 => 'Miki',
  3 => 'Tomer',
  4 => 'Amit',
  5 => 'Nethanel',
);$app_list_strings['country_list'] = array (
  'Afghanistan' => 'Afghanistan',
  'Albania' => 'Albania',
  'Algeria' => 'Algeria',
  'American_Samoa' => 'American Samoa',
  'Andorra' => 'Andorra',
  'Angola' => 'Angola',
  'Anguilla' => 'Anguilla',
  'Antigua' => 'Antigua',
  'Argentina' => 'Argentina',
  'Armenia' => 'Armenia',
  'Aruba' => 'Aruba',
  'Australia' => 'Australia',
  'Austria' => 'Austria',
  'Azerbaijan' => 'Azerbaijan',
  'Bahamas' => 'Bahamas',
  'Bahrain' => 'Bahrain',
  'Bangladesh' => 'Bangladesh',
  'Barbados' => 'Barbados',
  'Barbuda' => 'Barbuda',
  'Belarus' => 'Belarus',
  'Belgium' => 'Belgium',
  'Belize' => 'Belize',
  'Benin' => 'Benin',
  'Bermuda' => 'Bermuda',
  'Bhutan' => 'Bhutan',
  'Bolivia' => 'Bolivia',
  'Bonaire' => 'Bonaire',
  'Bosnia_Herzegovina' => 'Bosnia-Herzegovina',
  'Botswana' => 'Botswana',
  'Brazil' => 'Brazil',
  'British_Virgin_Island' => 'British Virgin Island',
  'Brunei' => 'Brunei',
  'Bulgaria' => 'Bulgaria',
  'Burkina_Faso' => 'Burkina Faso',
  'Burundi' => 'Burundi',
  'Cambodia' => 'Cambodia',
  'Cameroon' => 'Cameroon',
  'Canada' => 'Canada',
  'Cape_Verde' => 'Cape Verde',
  'Cayman_Island' => 'Cayman Island',
  'Central_African_Republic' => 'Central African Republic',
  'Chad' => 'Chad',
  'Channel_Islands' => 'Channel Islands',
  'Chile' => 'Chile',
  'China' => 'China',
  'Colombia' => 'Colombia',
  'Comoros' => 'Comoros',
  'Congo' => 'Congo',
  'Congo_Brazzaville' => 'Congo - Brazzaville',
  'Costa_Rica' => 'Costa Rica',
  'Cote_dIvoire' => 'Côte d\'Ivoire',
  'Croatia' => 'Croatia',
  'Cuba' => 'Cuba',
  'Cyprus' => 'Cyprus',
  'Czech_Republic' => 'Czech Republic',
  'Denmark' => 'Denmark',
  'Djibouti' => 'Djibouti',
  'Dominica' => 'Dominica',
  'Dominican_Republic' => 'Dominican Republic',
  'Ecuador' => 'Ecuador',
  'Egypt' => 'Egypt',
  'El_Salvador' => 'El Salvador',
  'Equatorial_Guinea' => 'Equatorial Guinea',
  'Eritrea' => 'Eritrea',
  'Estonia' => 'Estonia',
  'Ethiopia' => 'Ethiopia',
  'Fiji' => 'Fiji',
  'Finland' => 'Finland',
  'France' => 'France',
  'French_Guinea' => 'French Guinea',
  'Gabon' => 'Gabon',
  'Gambia' => 'Gambia',
  'Georgia' => 'Georgia',
  'Germany' => 'Germany',
  'Ghana' => 'Ghana',
  'Gibraltar' => 'Gibraltar',
  'Greece' => 'Greece',
  'Greenland' => 'Greenland',
  'Grenada' => 'Grenada',
  'Guatemala' => 'Guatemala',
  'Guinea' => 'Guinea',
  'Guinea_Bissau' => 'Guinea-Bissau',
  'Guyana' => 'Guyana',
  'Haiti' => 'Haiti',
  'Honduras' => 'Honduras',
  'Hong_Kong' => 'Hong Kong',
  'Hungary' => 'Hungary',
  'Iceland' => 'Iceland',
  'India' => 'India',
  'Indonesia' => 'Indonesia',
  'Iran' => 'Iran',
  'Iraq' => 'Iraq',
  'Ireland' => 'Ireland',
  'Israel' => 'Israel',
  'Italy' => 'Italy',
  'Ivory_Coast' => 'Ivory Coast',
  'Jamaica' => 'Jamaica',
  'Japan' => 'Japan',
  'Jordan' => 'Jordan',
  'Kazakhstan' => 'Kazakhstan',
  'Kenya' => 'Kenya',
  'Kuwait' => 'Kuwait',
  'Kyrgyzstan' => 'Kyrgyzstan',
  'Laos' => 'Laos',
  'Latvia' => 'Latvia',
  'Lebanon' => 'Lebanon',
  'Lesotho' => 'Lesotho',
  'Liberia' => 'Liberia',
  'Libya' => 'Libya',
  'Liechtenstein' => 'Liechtenstein',
  'Lithuania' => 'Lithuania',
  'Luxembourg' => 'Luxembourg',
  'Macedonia' => 'Macedonia',
  'Madagascar' => 'Madagascar',
  'Malawi' => 'Malawi',
  'Malaysia' => 'Malaysia',
  'Maldives' => 'Maldives',
  'Mali' => 'Mali',
  'Malta' => 'Malta',
  'Marshall_Islands' => 'Marshall Islands',
  'Martinique' => 'Martinique',
  'Mauritania' => 'Mauritania',
  'Mauritius' => 'Mauritius',
  'Mexico' => 'Mexico',
  'Micronesia' => 'Micronesia',
  'Moldova' => 'Moldova',
  'Monaco' => 'Monaco',
  'Mongolia' => 'Mongolia',
  'Montenegro' => 'Montenegro',
  'Montserrat' => 'Montserrat',
  'Morocco' => 'Morocco',
  'Mozambique' => 'Mozambique',
  'Myanmar_Burma' => 'Myanmar/Burma',
  'Namibia' => 'Namibia',
  'Nauru' => 'Nauru',
  'Nepal' => 'Nepal',
  'Netherlands' => 'Netherlands',
  'New_Zealand' => 'New Zealand',
  'Nicaragua' => 'Nicaragua',
  'Niger' => 'Niger',
  'Nigeria' => 'Nigeria',
  'North_Korea' => 'North Korea',
  'Norway' => 'Norway',
  'Oman' => 'Oman',
  'Pakistan' => 'Pakistan',
  'Palau' => 'Palau',
  'Panama' => 'Panama',
  'Papua_New_Guinea' => 'Papua New Guinea',
  'Paraguay' => 'Paraguay',
  'Peru' => 'Peru',
  'Philippines' => 'Philippines',
  'Poland' => 'Poland',
  'Portugal' => 'Portugal',
  'Puerto_Rico' => 'Puerto Rico',
  'Qatar' => 'Qatar',
  'Reunion' => 'Réunion',
  'Romania' => 'Romania',
  'Russia' => 'Russia',
  'Rwanda' => 'Rwanda',
  'Samoa' => 'Samoa',
  'San_Marino' => 'San Marino',
  'Saudi_Arabia' => 'Saudi Arabia',
  'Senegal' => 'Senegal',
  'Serbia' => 'Serbia',
  'Seychelles' => 'Seychelles',
  'Sierra_Leone' => 'Sierra Leone',
  'Singapore' => 'Singapore',
  'Slovak_Republic' => 'Slovak Republic',
  'Slovenia' => 'Slovenia',
  'Solomon_Islands' => 'Solomon Islands',
  'Somalia' => 'Somalia',
  'South_Africa' => 'South Africa',
  'South_Korea' => 'South Korea',
  'Spain' => 'Spain',
  'Sri_Lanka' => 'Sri Lanka',
  'St.Kitts_and_Nevis' => 'St.Kitts and Nevis',
  'St.Lucia' => 'St.Lucia',
  'St_Maarten_St_Martin' => 'St. Maarten/St. Martin',
  'St.Vincent' => 'St.Vincent',
  'Sudan' => 'Sudan',
  'Suriname' => 'Suriname',
  'Swaziland' => 'Swaziland',
  'Sweden' => 'Sweden',
  'Switzerland' => 'Switzerland',
  'Syria' => 'Syria',
  'Taiwan' => 'Taiwan',
  'Tajikistan' => 'Tajikistan',
  'Tanzania' => 'Tanzania',
  'Thailand' => 'Thailand',
  'Togo' => 'Togo',
  'Tonga' => 'Tonga',
  'Tortola' => 'Tortola',
  'Trinidad_and_Tobago' => 'Trinidad and Tobago',
  'Tunisia' => 'Tunisia',
  'Turkey' => 'Turkey',
  'Turkmenistan' => 'Turkmenistan',
  'Tuvalu' => 'Tuvalu',
  'Uganda' => 'Uganda',
  'Ukraine' => 'Ukraine',
  'United_Arab_Emirates' => 'United Arab Emirates',
  'United_Kingdom' => 'United Kingdom',
  'United_states' => 'United states',
  'Uruguay' => 'Uruguay',
  'Uzbekistan' => 'Uzbekistan',
  'Vanuatu' => 'Vanuatu',
  'Vatican_City' => 'Vatican City',
  'Venezuela' => 'Venezuela',
  'Vietnam' => 'Vietnam',
  'Western_Sahara' => 'Western Sahara',
  'Yemen' => 'Yemen',
  'Zambia' => 'Zambia',
);$app_list_strings['grid_list'] = array (
  1 => 'Greece Co',
  2 => 'General',
  3 => 'Australia',
  4 => 'France',
  5 => 'Germany',
  6 => 'Greece Is',
  7 => 'Israel',
  8 => 'Italy',
  9 => 'Spain',
  10 => 'UK 230V',
  11 => 'US Auto',
  12 => 'US 208V',
  13 => 'US 240V',
  14 => 'US 208V-No N',
  15 => 'US 240V-No N',
  16 => 'Bulgaria',
  17 => 'CzechRep',
  18 => 'Cypress',
  19 => 'Belgium',
  20 => 'Netherlands',
  21 => 'Portugal',
  22 => 'Austria',
  23 => 'Thailand MEA',
  24 => 'Singapore',
  25 => 'Korea',
  26 => 'Japan Auto',
  27 => 'Japan 50Hz',
  28 => 'Japan 60Hz',
  29 => 'Taiwan',
  30 => 'Denmark',
  31 => 'Sweden',
  32 => 'Thailand PEA',
  33 => 'Sri-Lanka',
  34 => 'Mauritius',
  35 => 'Denmark Res',
  36 => 'US 277V',
  37 => 'Slovenia',
  38 => 'Poland',
  39 => 'Germany MVGC',
  40 => 'UK 240V',
  41 => 'Lithuania',
  42 => 'China',
  43 => 'Philippines',
  44 => 'Brazil',
  45 => 'Mexico 220V',
  46 => 'Mexico 277V',
  47 => 'Romania',
  48 => 'Latvia',
  49 => 'South Africa',
  50 => 'Turkey',
  51 => 'Italy (No SPI)',
  52 => 'US (HI) Auto',
  53 => 'US (HI) 208V',
  54 => 'US (HI) 240V',
  55 => 'US (HI) 208V-No N',
  56 => 'US (HI) 240V-No N',
  57 => 'US (HI) 277V',
  58 => 'Switzerland',
  59 => 'Custom',
  60 => 'India',
  61 => 'Croatia',
  62 => 'Jamaica 240V',
  63 => 'Jamaica 220V (No N) - NA',
  64 => 'Barbados 230V (No N) - NA',
  65 => 'St. Lucia - NA',
  66 => 'Australia QLD',
  67 => 'Denmark VDE',
  68 => 'Denmark VDE Res',
  69 => 'Ireland',
  70 => 'US (Kauai) Auto',
  71 => 'US (Kauai) 208V',
  72 => 'US (Kauai) 240V',
  73 => 'US (Kauai) 208V-No N',
  74 => 'US (Kauai) 240V-No N',
  75 => 'US (Kauai) 277V',
  76 => 'Cypress 240V',
  77 => 'Curacao',
  78 => 'N.Cypress 240V',
  79 => 'Israel - Commercial',
  80 => 'Aruba 220V',
  81 => 'Mexico 240V',
  82 => 'Barbados 115V',
  83 => 'Malaysia',
  84 => 'New- Unlisted',
);$app_list_strings['visit_order_list'] = array (
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
  7 => '7',
  8 => '8',
  9 => '9',
  10 => '10',
);$app_list_strings['alternative_ibolt_status_list'] = array (
  1 => 'Start',
  2 => 'Success',
  3 => 'Error',
  4 => 'Warning',
  5 => 'There is no serial in this case',
  6 => 'Error in one or more of the serial',
  7 => 'numbers',
  8 => 'InProcess',
);$app_list_strings['replacement_type_list'] = array (
  1 => 'SE field engineer ',
  2 => 'From customer stock ',
  3 => 'Without stock',
);$app_list_strings['escalation_to_tesla_list'] = array (
  1 => 'Tesla',
  2 => 'LG',
);$app_list_strings['symptom_list'] = array (
  1 => 'Site Registration and Update',
  2 => 'Alerts',
  3 => 'Data presentation',
  4 => 'Logical Layout',
  5 => 'Password',
  6 => 'Communication',
  7 => 'Inverter',
  8 => 'Optimizer',
  9 => 'Safety and Monitoring Interface (SMI)',
  10 => 'Installer tools – SW',
  11 => 'Gemini',
  12 => 'Accessories',
  13 => 'Retrofit',
  14 => 'Product information',
  15 => 'Sales orders',
  16 => 'Sales opportunity',
  17 => 'Alternative installer request',
  18 => 'Site design assistant',
  19 => 'API - User related',
  20 => 'API - SolarEdge related',
  21 => 'Other',
  22 => 'Battery-StorEdge',
  23 => 'Upgrade inverter for StorEdge',
  24 => 'StorEdge Upgrade',
  25 => 'None',
);$app_list_strings['valid_list'] = array (
  1 => 'Yes',
  2 => 'No',
);$app_list_strings['tier_2_assignee_list'] = array (
  1 => 'Andreas',
  2 => 'Eyal S',
  3 => 'Jacques',
  4 => 'Nadav',
  5 => 'Nadav K',
  6 => 'Tier 2',
  7 => 'Tim',
  8 => 'Tokuo',
  9 => 'Oded',
  10 => 'Nisso',
  11 => 'Florian H',
);$app_list_strings['field_engineer_list'] = array (
  1 => 'Belgium',
  2 => 'France',
  3 => 'Germany',
  4 => 'Italy',
  5 => 'SEA',
  6 => 'UK',
  7 => 'US',
  8 => 'Israel',
  9 => 'Asia',
);$app_list_strings['severity_list'] = array (
  1 => '1- High',
  2 => '2- Medium',
  3 => '3- Low',
);$app_list_strings['resolution1_list'] = array (
  1 => 'RMA Inverter',
  2 => 'RMA Board',
  3 => 'RMA Optimizer',
  4 => 'RMA',
  5 => 'Configuration Change / Reconfiguration',
  6 => 'Communication board SW Upgrade',
  7 => 'Digital board SW Upgrade',
  8 => 'both Digital and communicating SW ',
  9 => 'Upgrade',
  10 => 'Reselect country',
  11 => 'Remote / Local Pairing',
  12 => 'Reset',
  13 => 'Activation',
  14 => 'Parameter Change',
  15 => 'Instruction given to the Customer',
  16 => 'Monitoring configuration',
  17 => 'Inquiry',
  18 => 'No fault found',
);$app_list_strings['rma_type_list'] = array (
  1 => 'Advance',
  2 => 'Regular',
);$app_list_strings['installation_rules_list'] = array (
  1 => 'Yes',
  2 => 'No',
  3 => 'Unknown',
);$app_list_strings['monitored_list'] = array (
  1 => 'Yes',
  2 => 'No',
);$app_list_strings['field_engineer_name_list'] = array (
  1 => 'Idan Maktubi',
  2 => 'Ronald Riahi',
  3 => 'Giambattista Zorzan',
  4 => 'Tokuo Ishizuki',
  5 => 'Nadav Ratzkowski',
  6 => 'GP-Zion',
  7 => 'GP-Netanel',
  8 => 'GP-Tzvi',
  9 => 'GP',
  10 => 'Michael.B',
  11 => 'T.J.',
  12 => 'Mathieu Moloney',
  13 => 'Paul Belanger',
  14 => 'Ralf Arnold',
  15 => 'Noah Tuthill',
  16 => 'Roy',
  17 => 'Richard Scott',
  18 => 'Stephen Speer',
  19 => 'Mac Elvina',
  20 => 'Glenn Byrne',
  21 => 'Eddie Dela Trre',
  22 => 'Jeffrey Pound',
  23 => 'Sam Garcia',
  24 => 'Jeff Laughy',
  25 => 'Jeff Morosetti',
  26 => 'Szih Wei Chia',
  27 => 'Sara Hambleton',
  28 => 'Tripp Bannister',
  29 => 'Brandon Hanson',
  30 => 'Francisco Reyes',
  31 => 'Travis Jones',
  32 => 'Adam Schilling',
  33 => 'Eric Larson',
  34 => 'Florian Weber',
  35 => 'Florian Hauf',
  36 => 'Stephane le Rouzic',
  37 => 'Jason Kirrage',
  38 => 'Jacques Van der Bijl',
  39 => 'Frank Bakker',
  40 => 'GP – leoned',
  41 => 'GT-Adiel',
);$app_list_strings['reason_for_not_approved_list_list'] = array (
  1 => 'Wrong compensation case ',
  2 => 'Already paid',
  3 => 'Other',
);$app_list_strings['sub_type_list'] = array (
  'Balance_Inquiry' => 'Balance Inquiry',
  'Redemption_of_points' => 'Redemption of points ',
  'Alliance_Code' => 'Alliance Code ',
);$app_list_strings['error_event_code_list'] = array (
  1 => 'Missing bracket',
  2 => 'Missing bracket screws',
  3 => 'Missing GND washers',
  4 => 'Switch failure',
  5 => 'Broken Connectors',
  6 => 'Volt',
  7 => 'Volt> 1V, dead optimizer',
  8 => 'Vin = Vout',
  9 => 'Isolation failure',
  10 => 'Diode fault input',
  11 => 'Diode fault output',
  12 => 'Loose PEM bolt',
  13 => 'Multy resets',
  14 => 'Low production',
  15 => 'Pairing issue',
  16 => 'Humming Optimizer',
  17 => 'RFI',
  18 => 'Water penetration',
  19 => 'Boost failure',
  20 => 'Connectors failure',
  21 => 'Damaged goods',
  22 => 'Full site optimizer replacement',
  23 => 'ID mismatch',
  24 => 'Input Short',
  25 => 'Output Short',
  26 => 'OPJ-melted',
  27 => 'OPJ-melted',
  28 => 'Working optimizer, not reporting',
  29 => 'Workmanship',
  30 => 'PLC issue',
  31 => 'Requested for investigation',
  32 => 'Wrong FW',
  33 => 'Zep bracket',
  34 => '49; 0x08000010; Communication',
  35 => '52; 0x08000013; Vin Decrease Timeout',
  36 => '53; 0x08000014; Multiple Errors',
  37 => '54; 0x08000015; JM Sync Timeout',
  38 => '55; 0x08000016; Capcitor estimation',
  39 => '56; 0x08000017; RsrvdMngrErr 3',
  40 => '57; 0x08000018; RsrvdMngrErr 4',
  41 => '58; 0x08000019; V-L1 Max1',
  42 => '59; 0x0800001A; V-L2 Max1',
  43 => '60; 0x0800001B; V-L3Max1',
  44 => '61; 0x0800001C; V-L1 Min1',
  45 => '62; 0x0800001D; V-L2 Min1',
  46 => '63; 0x0800001E; V-L3 Min1',
  47 => '64; 0x0800001F; V-L1 Max2',
  48 => '65; 0x08000020; V-L2 Max2',
  49 => '66; 0x08000021; V-L3 Max2',
  50 => '67; 0x08000022; V-L1 Min2',
  51 => '68; 0x08000023; V-L2 Min2',
  52 => '69; 0x08000024; V-L3 Min2',
  53 => '71; 0x08000026; V-Line Min',
  54 => '72; 0x08000027; I-ACDC L1',
  55 => '73; 0x08000028; Unknown',
  56 => '74; 0x08000029; I-ACDC L2',
  '057' => '75; 0x0800002A; I-ACDC L3',
  58 => '76; 0x0800002B; I-RCD 1',
  59 => '77; 0x0800002C; I-RCD 2',
  60 => '78; 0x0800002D; JP Sync Timeout',
  61 => '79; 0x0800002E; F-L1 Max',
  62 => '81; 0x08000030; F-L3 Max',
  63 => '82; 0x08000031; F-L1 Min',
  64 => '83; 0x08000032; F-L2 Min',
  65 => '84; 0x08000033; F-L3 Min',
  66 => '85; 0x08000034; VinP Max',
  67 => '86; 0x08000035; VinM Max',
  68 => '87; 0x08000036; Active anti islanding',
  69 => '88; 0x08000037; Vin Max',
  70 => '90; 0x08000039; TZ RCD',
  71 => '91; 0x0800003A; TZ L1',
  72 => '92; 0x0800003B; TZ L2',
  73 => '93; 0x0800003C; TZ L3',
  74 => '96; 0x0800003F; Iac L1 Max',
  75 => '97; 0x08000040; Iac L2 Max',
  76 => '98; 0x08000041; Iac L3 Max',
  77 => '99; 0x08000042; Vsrg L1 Max (Vfilter spike)',
  78 => '100; 0x08000043; Vsrg L2 Max (Vfilter spike)',
  79 => '101; 0x08000044; Vsrg L3 Max (Vfilter spike)',
  80 => '102; 0x08000045; VDC Pluse Max, (spike)',
  81 => '103; 0x08000046; VDC Minus Max, (spike)',
  82 => '104; 0x08000047; Overtemp',
  83 => '105; 0x08000048; Undertemp',
  84 => '112; 0x0800004F; Wrong Ac connection',
  85 => '113; 0x08000050; Main State State',
  86 => '115; 0x08000052; VDC Cap banks unbalanced',
  87 => '116; 0x08000053; DC Charged',
  88 => '118; 0x08000055; GRM',
  89 => '119; 0x08000056; INIT',
  90 => '120; 0x08000057; Temp Sensor',
  91 => '121; 0x08000058; Isolation',
  92 => '122; 0x08000059; Relay Test',
  '093' => '123; 0x0800005A; Ofset Gain',
  94 => '124; 0x0800005B; RCD Test',
  95 => '128; 0x0800005F; RsrvdInit2',
  96 => '129; 0x08000060; RsrvdInit3',
  97 => '0; 0x02000000; Wakeup',
  98 => '1; 0x02000001; turn off',
  99 => '2; 0x02000002; Night mode',
  100 => '3; 0x02000003; Reset',
  101 => '4; 0x02000004; DSP1 DSP2 comm',
  102 => '5; 0x02000005; DSP2 unexpected mode',
  103 => '6; 0x02000006; DSP1 Grid Error.',
  104 => '7; 0x02000007; VDC slow discharge',
  105 => '8; 0x02000008; Unknown DSP2 Error',
  106 => '9; 0x02000009; Iout TZ',
  107 => '10; 0x0200000A; RCD TZ',
  108 => '12; 0x0200000C; Obs',
  109 => '13; 0x0200000D; Iout max',
  110 => '14; 0x0200000E; Vac surge',
  111 => '15; 0x0200000F; VDC surge',
  112 => '16; 0x02000010; Vref1V or Vref2V > +/-3%',
  113 => '17; 0x02000011; Temprature Max',
  114 => '18; 0x02000012; WD error',
  115 => '19; 0x02000013; SW error',
  116 => '20; 0x02000014; SW error',
  117 => '21; 0x02000015; SW error',
  118 => '22; 0x02000016; SW error',
  119 => '23; 0x02000017; VNN error',
  120 => '24; 0x02000018; Temprature Min',
  121 => '25; 0x02000019; Isolation error',
  122 => '26; 0x0200001A; Relay test error',
  123 => '27; 0x0200001B; Obs',
  124 => '28; 0x0200001C; RCD test error',
  125 => '29; 0x0200001D; VLN max',
  126 => '30; 0x0200001E; VLN min',
  127 => '31; 0x0200001F; Vg max',
  128 => '32; 0x02000020; Vg min',
  129 => '33; 0x02000021; Vg2 max',
  130 => '34; 0x02000022; F max',
  131 => '35; 0x02000023; F min',
  132 => '36; 0x02000024; Iacdc',
  133 => '37; 0x02000025; IRCD 1',
  134 => '38; 0x02000026; IRCD 2',
  135 => '39; 0x02000027; PLL Sync error',
  136 => '40; 0x02000028; Islanding (DSP1)',
  137 => '41; 0x02000029; Vg2 min',
  138 => '42; 0x0200002A; Unknown Error',
  139 => '43; 0x0200002B; obs',
  140 => '44; 0x0200002C; No country selected',
  141 => '45; 0x0200002D; Portia - DSP1 comm error',
  142 => '132; 0x02000084; Exceeded 24H error',
  143 => '134; 0x02000086; DSP1- DSP2 VDC diff max',
  144 => '141; 0x0200008D; Switch on',
  145 => '142; 0x0200008E; Switch off',
  146 => '145; 0x02000091; VDC max',
  147 => '148; 0x02000094; Low power',
  148 => '149; 0x02000095; Power train error',
  149 => '150; 0x02000096; Arc-DSP1',
  150 => '151; 0x02000097; Arc-DSP2',
  151 => '152; 0x02000098; Tx test (AKA arc test error)',
  152 => '153; 0x02000099; Vfilter discharge',
  153 => '154; 0x0200009A; Arc -debug error',
  154 => '156; 0x0200009C; DRV TZ false alarm',
  155 => '256; 0x02000100; Fan 1 is not working',
  156 => '257; 0x02000101; Fan 2 is not working',
  157 => '262; 0x02000106; Fan controller comm error',
  158 => '269; 0x0200010D; External',
  159 => 'Cables/connectors/instalation',
  160 => 'Local router',
  161 => 'DHCP issue',
  162 => 'Router Connection to the Internet',
  163 => 'Ethernet port on comm board',
  164 => 'Configuration',
  165 => 'Network Failure',
  166 => 'Modem Fault',
  167 => 'Module failure',
  168 => 'Port on comm board failure',
  169 => 'Lightning protection device',
  170 => 'Can\'t connect to master',
  171 => 'Out of range',
  172 => 'Router out of range',
  173 => 'Can\'t connect to the router',
  174 => 'Power Failure',
  175 => 'Damaged Sensor port',
  176 => 'Eth Cables/connectors/installation',
  177 => 'Cellular Modem Failure',
  178 => 'Cellular Network Failure',
  179 => 'RS485 Cables/connectors/installation',
  180 => 'Zigbee Out of range',
  181 => 'WiFi Out of range',
  182 => 'Voltage out of spec',
  183 => 'Frequency out of spec',
  184 => 'Unstable grid',
  185 => 'Reverse DC connection',
  186 => 'Wrong string length',
  187 => 'DC cabling',
  188 => 'AC cabling',
  189 => 'DC fuse box',
  190 => 'AC Fuse / Circuit breaker',
  191 => 'Grounding',
  192 => 'Lack of training/knowledge',
  193 => 'Other Inverter',
  194 => 'Faulty Panel',
  195 => 'Panel do not match optimizer',
  196 => 'Communication board failure',
  197 => 'RTC Failure',
  198 => 'RTC Battery Failure',
  199 => 'SD card doesn\'t work',
  200 => 'SD Card missing',
  201 => 'SD card-wrong SN',
  202 => 'Firmware not up-to-date',
  203 => 'Display Off',
  204 => 'Unclear display',
  205 => 'Wrong parameters',
  206 => 'Wrong Firmware',
  207 => 'Night mode',
  208 => 'Digital Fan Failure',
  209 => 'Fuse fault',
  210 => 'Power supply failure',
  211 => 'DC capacitor leakage',
  212 => 'Burnt component',
  213 => 'Component Failure',
  214 => 'Leakage from chocks',
  215 => 'Reversed SN sticker',
  216 => 'Reverse cable sticker',
  217 => 'Fan not working',
  218 => 'Reverse cable polarity',
  219 => 'Net is touching the fan',
  220 => 'Wrong wiring',
  221 => 'Add physical layout',
  222 => 'Alerts',
  223 => 'Data presentation problem',
  224 => 'Fix physical layout',
  225 => 'General question on monitoring site',
  226 => 'Known bug',
  227 => 'Logical layout',
  228 => 'Mobile devices',
  229 => 'Monitoring server is down',
  230 => 'User name / Password',
  231 => 'Permissions',
  232 => 'Register owner without installer',
  234 => 'Registration',
  235 => 'Site Mapper',
  236 => 'Site setup without physical',
  237 => 'Site setup with physical',
  238 => 'Update details',
  239 => 'Change account for the site',
  240 => 'Missing manual',
  241 => 'Missing quick installation guide',
  242 => 'Advertisement',
  243 => 'Damaged package',
  244 => 'No fault found',
  245 => 'Request for compensation',
);$app_list_strings['doa_list'] = array (
  1 => 'Yes',
  2 => 'No',
);$app_list_strings['fault_sub_type_list'] = array (
  1 => 'Event/Error code',
  2 => '3rd party inverter com problem',
  3 => 'Motherboard failure',
  4 => 'Communication board failure',
  5 => 'Pairing issue',
  6 => 'Mechanical',
  7 => 'Ethernet',
  8 => 'Embedded GSM',
  9 => 'Embedded CDMA',
  10 => 'RS485',
  11 => 'WiFi',
  12 => 'CCG',
  13 => 'Communication',
  14 => 'Grid',
  15 => 'Installation',
  16 => 'Other Inverter',
  17 => 'Panels',
  18 => 'Comm board',
  19 => 'Digital',
  20 => 'Event/Error code 1 Phase',
  21 => 'Event/Error code 3 Phase',
  22 => 'Event/Error Code- HD wave',
  23 => 'Power board',
  24 => 'Relay board',
  25 => 'External Fan',
  26 => 'Internal fan',
  27 => 'Humming inverter',
  28 => '1:1 Configuration',
  29 => '2:1 Configuration',
  30 => '3:1 Configuration',
  31 => '4:1 Configuration',
  32 => 'Add physical layout',
  33 => 'Alerts',
  34 => 'Data presentation problem',
  35 => 'Fix physical layout',
  36 => 'Known bug',
  37 => 'Logical layout',
  38 => 'Mobile devices',
  39 => 'Monitoring server is down',
  40 => 'User name / Password',
  41 => 'Permissions',
  42 => 'Register owner without installer',
  43 => 'Registration',
  44 => 'Site Mapper',
  45 => 'Site setup without physical',
  46 => 'Site setup with physical',
  47 => 'Update details',
  48 => 'Change account for the site',
  49 => 'Missing Item',
  50 => 'Advertisement',
  51 => 'Damaged package',
  52 => 'No fault found',
  53 => 'Request for compensation',
  54 => 'Sales opportunity',
  55 => 'SF password/registration issue',
  56 => 'Shipment details',
  57 => 'Stickers',
  58 => 'System design assistance',
  59 => 'Training request',
  60 => 'Wrong Shipment/Packaging',
  61 => 'Warranty',
  62 => 'Site designer',
  63 => 'Android mapper app',
  64 => 'iphone mapper app',
  65 => 'Configuration tool',
  66 => 'Solaredge Key',
  67 => 'Duplication',
  68 => 'No reply from the customer',
  69 => 'DCD A/B issue',
  70 => 'Does not line up with inverter',
  71 => 'Workmanship Failure',
  72 => 'Power Failure',
  73 => 'Damaged Sensore port',
  74 => 'faulty CCG',
  75 => 'Sensors/Meters',
  76 => 'Faulty FFG',
  77 => 'Event/Error code (string)',
  78 => 'Event/Error code (combi)',
  79 => 'Monitoring',
  80 => 'IEC Certification',
  81 => 'Firmware',
  82 => 'No communication to site',
  83 => 'Button Fault',
  84 => 'Remote operation',
  85 => 'Site config',
  86 => 'Manual Activation',
  87 => 'General Tech Question',
  88 => 'General question on monitoring site',
  89 => 'Water penetration',
  90 => 'RGM',
  91 => 'API request',
  92 => 'Maintenance',
  93 => 'Continuously in wakeup loop',
  94 => 'Damaged goods',
  95 => 'High temperature',
  96 => 'Low production',
  97 => 'Burnt component',
  98 => 'Faulty DCD',
  99 => 'Standard Compliance',
  100 => 'Requested for investigation',
  101 => 'Zigbee-card',
  102 => 'Zigbee-Home gateway',
  103 => 'Act Of God',
  104 => 'Combi',
  105 => 'Damaged Inverter',
  106 => 'Damaged Item',
  107 => 'Browser issue',
  108 => 'Other',
  109 => 'Interface',
  110 => 'Error Code',
  111 => 'Firmware Upgrade',
  112 => 'Battery Fault- RMA',
  113 => 'Interface- HW Failure',
  114 => 'Interface- Fuse failure',
  115 => 'Interface- Missing parts',
  116 => 'Battery Error Code',
  117 => 'Backup',
  118 => 'Configuration',
  119 => 'Meter',
  120 => 'StorEdge Mechanical',
  121 => 'StorEdge Communication',
  122 => 'StorEdge Installation',
  123 => 'General Questions',
  124 => 'Zigbee manager',
  125 => 'Immersion heater controller',
  126 => 'Plug',
  127 => 'AC switch',
  128 => 'AC dry contact',
  129 => 'Damaged Device',
);$app_list_strings['shipping_service_type_list'] = array (
  'STD' => 'STD',
  'EXP' => 'EXP',
);$app_list_strings['sub_symptom_list'] = array (
  1 => 'Error code',
  2 => 'Add physical layout to existing site',
  3 => 'Firefighter Gateway',
  4 => 'Cellular GSM',
  5 => 'Cellular CDMA',
  6 => 'Configuration Tool',
  7 => 'Control and Communication Gateway ',
  8 => '(CCG)',
  9 => 'Current transformer',
  10 => 'Damaged part',
  11 => 'Electricity Meter',
  12 => 'Environmental Sensors',
  13 => 'Error code number',
  14 => 'Ethernet',
  15 => 'IEC Upgrade',
  16 => 'IndOP optimizers',
  17 => 'Low production',
  18 => 'Missing part',
  19 => 'No production',
  20 => 'Not reporting in monitoring portal',
  21 => 'Owner registration with no installer',
  22 => 'Pok',
  23 => 'Packaging',
  24 => 'Pairing',
  25 => 'Permissions',
  26 => 'Replace system component',
  27 => 'RS485',
  28 => 'S0 meter interface',
  29 => 'Safety Switch (NA only)',
  30 => 'Site designer',
  31 => 'Site Mapper',
  32 => 'Site registration without physical layout',
  33 => 'Site registration with physical layout',
  34 => 'SolarEdge Key',
  35 => 'Update site details',
  36 => 'Wi-Fi',
  37 => 'Wrong part',
  38 => 'Zigbee',
  39 => 'Close Code',
  40 => 'WiFi',
  41 => 'User interface',
  42 => 'Damaged goods',
  43 => 'Communication',
  44 => 'Configuration',
  45 => 'Installation',
  46 => 'Other',
  47 => 'StorEdge Upgrade',
);$app_list_strings['inverter_model_list'] = array (
  1 => '1 phase',
  2 => '3 phase',
  3 => 'Add-on optimizer',
  4 => 'Embedded optimizer',
  5 => 'SMI 180',
  6 => 'SMI 35',
);$app_list_strings['certification_level_list'] = array (
  1 => '1',
  2 => '2',
);$app_list_strings['state_list'] = array (
  'Alabama' => 'Alabama',
  'Alaska' => 'Alaska',
  'Arizona' => 'Arizona',
  'Arkansas' => 'Arkansas',
  'California' => 'California',
  'Colorado' => 'Colorado',
  'Connecticut' => 'Connecticut',
  'Delaware' => 'Delaware',
  'District_of_Columbia' => 'District of Columbia',
  'Florida' => 'Florida',
  'Georgia' => 'Georgia',
  'Hawaii' => 'Hawaii',
  'Idaho' => 'Idaho',
  'Illinois' => 'Illinois',
  'Indiana' => 'Indiana',
  'Iowa' => 'Iowa',
  'Kansas' => 'Kansas',
  'Kentucky' => 'Kentucky',
  'Louisiana' => 'Louisiana',
  'Maine' => 'Maine',
  'Maryland' => 'Maryland',
  'Massachusetts' => 'Massachusetts',
  'Michigan' => 'Michigan',
  'Minnesota' => 'Minnesota',
  'Mississippi' => 'Mississippi',
  'Missouri' => 'Missouri',
  'Montana' => 'Montana',
  'Nebraska' => 'Nebraska',
  'Nevada' => 'Nevada',
  'New_Hampshire' => 'New Hampshire',
  'New_Jersey' => 'New Jersey',
  'New_Mexico' => 'New Mexico',
  'New_York' => 'New York',
  'North_Carolina' => 'North Carolina',
  'North_Dakota' => 'North Dakota',
  'Ohio' => 'Ohio',
  'Oklahoma' => 'Oklahoma',
  'Oregon' => 'Oregon',
  'Pennsylvania' => 'Pennsylvania',
  'Rhode_Island' => 'Rhode Island',
  'South_Carolina' => 'South Carolina',
  'South_Dakota' => 'South Dakota',
  'Tennessee' => 'Tennessee',
  'Texas' => 'Texas',
  'Utah' => 'Utah',
  'Vermont' => 'Vermont',
  'Virginia' => 'Virginia',
  'Washington' => 'Washington',
  'West_Virginia' => 'West Virginia',
  'Wisconsin' => 'Wisconsin',
  'Wyoming' => 'Wyoming',
  'Unites_states_Texas' => 'Texas',
  'United_states_Vermont' => 'Vermont',
);$app_list_strings['leadsource_list'] = array (
  1 => 'External Referral',
  2 => 'Trade Show',
  3 => 'Web',
  4 => 'Word of mouth',
  5 => 'Cold Call',
  6 => 'From Distributor',
  7 => 'Other',
);$app_list_strings['contact_type_list'] = array (
  1 => 'Installer',
  2 => 'Self-Installer',
  3 => 'Home owner',
  4 => 'Installation Manager',
  5 => 'Office Manager',
  6 => 'Monitoring Agent',
  7 => 'Logistics contact',
);$app_list_strings['type_list'] = array (
  1 => 'Silver',
  2 => 'Gold',
  3 => 'Bronze',
);$app_list_strings['activity_focus_list'] = array (
  'PV' => 'PV',
  'Sanitary' => 'Sanitary',
  'Electricalinstallations' => 'Electrical installations ',
);$app_list_strings['business_type_0'] = array (
  'Distributor' => 'Distributor',
  'Integrator' => 'Integrator',
  'Partner' => 'Partner',
  'Investor' => 'Investor',
  'Panel_Maker' => 'Panel Maker',
  'System_Owner' => 'System Owner',
  'Installer' => 'Installer',
  'Design_Engineer' => 'Design Engineer',
  'Utility' => 'Utility',
  'Other' => 'Other',
);$app_list_strings['classification_0'] = array (
  'Business_and_Marketing_Oriented' => 'Business and Marketing Oriented',
  'Business_Oriented' => 'Business Oriented',
  'Marketing_Oriented' => 'Marketing Oriented',
);$app_list_strings['customer_acquisition_activit_list'] = array (
  'Internet' => 'Internet',
  'Press' => 'Press',
  'Exhibitions' => 'Exhibitions',
  'Visits' => 'Visits',
);$app_list_strings['sales_stage_list'] = array (
  'Open' => 'Open',
  'Initial_technical_evaluation' => 'Initial technical evaluation	',
  'Advanced_technical_evaluation' => 'Advanced technical evaluation	',
  'Commercial_negotiation' => 'Commercial negotiation',
  'PO_received' => 'PO received',
  'Booked' => 'Booked',
  'On_Hold' => 'On Hold',
  'Transferred_to_distributor' => 'Transferred to distributor	',
  'Lost' => 'Lost',
  'Completed' => 'Completed',
  'Cancelled' => 'Cancelled',
);$app_list_strings['status_list'] = array (
  1 => 'First contact',
  2 => 'Presentation given',
  3 => 'F2F',
  4 => 'Sent quotation',
  5 => 'Negotiation',
  6 => 'PO recieved',
  7 => 'Booked',
  8 => 'Transferred to distributer',
  9 => 'Lost',
);$app_list_strings['us_region_0'] = array (
  1 => 'Canada',
  2 => 'Central',
  3 => 'East',
  4 => 'Mexico',
  5 => 'West',
);$app_list_strings['geographic_area_list'] = array (
  1 => 'Australia',
  2 => 'France',
  3 => 'Germany',
  4 => 'Israel',
  5 => 'Italy',
  6 => 'Japan',
  7 => 'North America',
  8 => 'Rest of Europe',
  9 => 'Rest of the world',
);$app_list_strings['installation_type_list'] = array (
  1 => 'Commercial',
  2 => 'Residential',
);$app_list_strings['distributor_status_0'] = array (
  1 => 'Open',
  2 => 'Contacted',
  3 => 'Won',
  4 => 'Lost',
);$app_list_strings['commit_level_list'] = array (
  1 => '0% Gone',
  2 => '20% Upside',
  3 => '40% Low probability',
  4 => '60% Medium probability',
  5 => '80% High probability',
  6 => '95% Very High probability',
  7 => '96% PO received',
  8 => '100% Booked',
);$app_list_strings['nam_ship_from_list'] = array (
  1 => 'Canada',
  2 => 'Fremont',
  3 => 'HI 3PL',
  4 => 'Zala',
  5 => 'JBL Direct',
  6 => 'MCCA (McCollister’s California)',
  7 => 'MCNY (McCollister’s New York)',
);$app_list_strings['open_booked_list'] = array (
  1 => 'Open',
  2 => 'Booked',
  3 => 'Lost',
);$app_list_strings['duration_list'] = array (
  6 => '6 Months',
  12 => '1 Year',
  24 => '2 Year',
);$app_list_strings['currency_list'] = array (
  1 => 'None',
  2 => 'Dollar',
  3 => 'Euro',
  4 => 'GBP',
  5 => 'ILS',
);$app_list_strings['opportunity_type1_0'] = array (
  1 => 'Distributor',
  2 => 'Forecast',
  3 => 'Direct Business',
  4 => 'Portfolio',
);






$app_list_strings['oppstates'][''] = '';
$app_list_strings['oppstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['oppstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['oppstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['oppstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['oppstates']['Unitedstates_California'] = 'California';
$app_list_strings['oppstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['oppstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['oppstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['oppstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['oppstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['oppstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['oppstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['oppstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['oppstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['oppstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['oppstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['oppstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['oppstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['oppstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['oppstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['oppstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['oppstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['oppstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['oppstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['oppstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['oppstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['oppstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['oppstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['oppstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['oppstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['oppstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['oppstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['oppstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['oppstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['oppstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['oppstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['oppstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['oppstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['oppstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['oppstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['oppstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['oppstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['oppstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['oppstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['oppstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['oppstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['oppstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['oppstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['oppstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['oppstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['oppstates']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['oppstates']['Canada_Alberta'] = 'Alberta';
$app_list_strings['oppstates']['Canada_British Columbia '] = 'British Columbia ';
$app_list_strings['oppstates']['Canada_Manitoba '] = 'Manitoba ';
$app_list_strings['oppstates']['Canada_NewBrunswick'] = 'New Brunswick';
$app_list_strings['oppstates']['Canada_NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['oppstates']['Canada_Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['oppstates']['Canada_Nunavut'] = 'Nunavut';
$app_list_strings['oppstates']['Canada_Ontario'] = 'Ontario';
$app_list_strings['oppstates']['Canada_PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['oppstates']['Canada_Quebec'] = 'Quebec';
$app_list_strings['oppstates']['Canada_Saskatchewan'] = 'Saskatchewan';
$app_list_strings['oppstates']['Canada_Yukon'] = 'Yukon';








$app_list_strings['accountstates'][''] = '';
$app_list_strings['accountstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['accountstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['accountstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['accountstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['accountstates']['Unitedstates_California'] = 'California';
$app_list_strings['accountstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['accountstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['accountstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['accountstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['accountstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['accountstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['accountstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['accountstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['accountstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['accountstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['accountstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['accountstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['accountstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['accountstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['accountstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['accountstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['accountstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['accountstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['accountstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['accountstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['accountstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['accountstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['accountstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['accountstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['accountstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['accountstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['accountstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['accountstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['accountstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['accountstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['accountstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['accountstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['accountstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['accountstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['accountstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['accountstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['accountstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['accountstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['accountstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['accountstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['accountstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['accountstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['accountstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['accountstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['accountstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['accountstates']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['accountstates']['Canada_Alberta'] = 'Alberta';
$app_list_strings['accountstates']['Canada_British Columbia '] = 'British Columbia ';
$app_list_strings['accountstates']['Canada_Manitoba '] = 'Manitoba ';
$app_list_strings['accountstates']['Canada_NewBrunswick'] = 'New Brunswick';
$app_list_strings['accountstates']['Canada_NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['accountstates']['Canada_Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['accountstates']['Canada_Nunavut'] = 'Nunavut';
$app_list_strings['accountstates']['Canada_Ontario'] = 'Ontario';
$app_list_strings['accountstates']['Canada_PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['accountstates']['Canada_Quebec'] = 'Quebec';
$app_list_strings['accountstates']['Canada_Saskatchewan'] = 'Saskatchewan';
$app_list_strings['accountstates']['Canada_Yukon'] = 'Yukon';



/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['POS1_POS_Tracking'] = 'POS Tracking';







$app_list_strings['sivstatelist'][''] = '';
$app_list_strings['sivstatelist']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['sivstatelist']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['sivstatelist']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['sivstatelist']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['sivstatelist']['Unitedstates_California'] = 'California';
$app_list_strings['sivstatelist']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['sivstatelist']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['sivstatelist']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['sivstatelist']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['sivstatelist']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['sivstatelist']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['sivstatelist']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['sivstatelist']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['sivstatelist']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['sivstatelist']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['sivstatelist']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['sivstatelist']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['sivstatelist']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['sivstatelist']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['sivstatelist']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['sivstatelist']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['sivstatelist']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['sivstatelist']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['sivstatelist']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['sivstatelist']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['sivstatelist']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['sivstatelist']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['sivstatelist']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['sivstatelist']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['sivstatelist']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['sivstatelist']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['sivstatelist']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['sivstatelist']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['sivstatelist']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['sivstatelist']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['sivstatelist']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['sivstatelist']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['sivstatelist']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['sivstatelist']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['sivstatelist']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['sivstatelist']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['sivstatelist']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['sivstatelist']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['sivstatelist']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['sivstatelist']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['sivstatelist']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['sivstatelist']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['sivstatelist']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['sivstatelist']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['sivstatelist']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['sivstatelist']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['sivstatelist']['Australia_AustralianCapitalTerritory'] = 'Australian Capital Territory';
$app_list_strings['sivstatelist']['Australia_NewSouthWales'] = 'New South Wales';
$app_list_strings['sivstatelist']['Australia_NewZealand'] = 'New Zealand';
$app_list_strings['sivstatelist']['Australia_NothernTerritory'] = 'Nothern Territory';
$app_list_strings['sivstatelist']['Australia_Queensland'] = 'Queensland';
$app_list_strings['sivstatelist']['Australia_SouthAustralia'] = 'South Australia';
$app_list_strings['sivstatelist']['Australia_Tasmania'] = 'Tasmania';
$app_list_strings['sivstatelist']['Australia_Victoria'] = 'Victoria';
$app_list_strings['sivstatelist']['Australia_Western Australia'] = 'Western Australia';




$app_strings['LBL_QUICK_CASE'] = 'Create Case';

/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Login_Logger'] = 'Login Logger';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['FSK1_Field_Service_Kit'] = 'Field Service Kit';

 
$app_list_strings['moduleList']['KReports'] = 'KReports v4.0';

$app_list_strings['kreportstatus'] = array(
	'1' => 'draft',
	'2' => 'limited release',
	'3' => 'general release'
);

$app_strings['LBL_KREPORTS_PRESENTATION_DASHLET'] = 'KReporter Presentation Dashlet';
$app_strings['LBL_KREPORTS_PRESENTATION_DESC'] = 'Displays the Presentation view of a Report';

$app_strings['LBL_KREPORTS_VISUALIZATION_DASHLET'] = 'KReporter Visualization Dashlet';
$app_strings['LBL_KREPORTS_VISUALIZATION_DESC'] = 'Displays the Visualization view of a Report';

/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['AL1_Alliance_Transaction'] = 'Alliance Transaction';
$app_list_strings['collect_redeem_list'][1] = 'Collect';
$app_list_strings['collect_redeem_list'][2] = 'Redeem';
$app_list_strings['type_0'][1] = 'Site';
$app_list_strings['type_0'][2] = 'Fault Site';
$app_list_strings['type_0'][3] = 'Trade Show';
$app_list_strings['type_0'][4] = 'Training';
$app_list_strings['type_0'][5] = 'Gift';
$app_list_strings['type_0'][6] = 'Other';
$app_list_strings['type_0'][7] = 'Refund';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Contact_Roles'] = 'Contact Roles';







$app_list_strings['casermastates'][''] = '';
$app_list_strings['casermastates']['Alabama'] = 'Alabama';
$app_list_strings['casermastates']['Alaska'] = 'Alaska';
$app_list_strings['casermastates']['Arizona'] = 'Arizona';
$app_list_strings['casermastates']['Arkansas'] = 'Arkansas';
$app_list_strings['casermastates']['California'] = 'California';
$app_list_strings['casermastates']['Colorado'] = 'Colorado';
$app_list_strings['casermastates']['Connecticut'] = 'Connecticut';
$app_list_strings['casermastates']['Delaware'] = 'Delaware';
$app_list_strings['casermastates']['DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['casermastates']['Florida'] = 'Florida';
$app_list_strings['casermastates']['Georgia'] = 'Georgia';
$app_list_strings['casermastates']['Hawaii'] = 'Hawaii';
$app_list_strings['casermastates']['Idaho'] = 'Idaho';
$app_list_strings['casermastates']['Illinois'] = 'Illinois';
$app_list_strings['casermastates']['Indiana'] = 'Indiana';
$app_list_strings['casermastates']['Iowa'] = 'Iowa';
$app_list_strings['casermastates']['Kansas'] = 'Kansas';
$app_list_strings['casermastates']['Kentucky'] = 'Kentucky';
$app_list_strings['casermastates']['Louisiana'] = 'Louisiana';
$app_list_strings['casermastates']['Maine'] = 'Maine';
$app_list_strings['casermastates']['Maryland'] = 'Maryland';
$app_list_strings['casermastates']['Massachusetts'] = 'Massachusetts';
$app_list_strings['casermastates']['Michigan'] = 'Michigan';
$app_list_strings['casermastates']['Minnesota'] = 'Minnesota';
$app_list_strings['casermastates']['Mississippi'] = 'Mississippi';
$app_list_strings['casermastates']['Missouri'] = 'Missouri';
$app_list_strings['casermastates']['Montana'] = 'Montana';
$app_list_strings['casermastates']['Nebraska'] = 'Nebraska';
$app_list_strings['casermastates']['Nevada'] = 'Nevada';
$app_list_strings['casermastates']['NewHampshire'] = 'New Hampshire';
$app_list_strings['casermastates']['NewJersey'] = 'New Jersey';
$app_list_strings['casermastates']['NewMexico'] = 'New Mexico';
$app_list_strings['casermastates']['NewYork'] = 'New York';
$app_list_strings['casermastates']['NorthCarolina'] = 'North Carolina';
$app_list_strings['casermastates']['NorthDakota'] = 'North Dakota';
$app_list_strings['casermastates']['Ohio'] = 'Ohio';
$app_list_strings['casermastates']['Oklahoma'] = 'Oklahoma';
$app_list_strings['casermastates']['Oregon'] = 'Oregon';
$app_list_strings['casermastates']['Pennsylvania'] = 'Pennsylvania';
$app_list_strings['casermastates']['RhodeIsland'] = 'Rhode Island';
$app_list_strings['casermastates']['SouthCarolina'] = 'South Carolina';
$app_list_strings['casermastates']['SouthDakota'] = 'South Dakota';
$app_list_strings['casermastates']['Tennessee'] = 'Tennessee';
$app_list_strings['casermastates']['Texas'] = 'Texas';
$app_list_strings['casermastates']['Utah'] = 'Utah';
$app_list_strings['casermastates']['Vermont'] = 'Vermont';
$app_list_strings['casermastates']['Virginia'] = 'Virginia';
$app_list_strings['casermastates']['Washington'] = 'Washington';
$app_list_strings['casermastates']['WestVirginia'] = 'West Virginia';
$app_list_strings['casermastates']['Wisconsin'] = 'Wisconsin';
$app_list_strings['casermastates']['Wyoming'] = 'Wyoming';
$app_list_strings['casermastates']['Alberta'] = 'Alberta';
$app_list_strings['casermastates']['British Columbia '] = 'British Columbia ';
$app_list_strings['casermastates']['Manitoba '] = 'Manitoba ';
$app_list_strings['casermastates']['NewBrunswick'] = 'New Brunswick';
$app_list_strings['casermastates']['NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['casermastates']['Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['casermastates']['Nunavut'] = 'Nunavut';
$app_list_strings['casermastates']['Ontario'] = 'Ontario';
$app_list_strings['casermastates']['PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['casermastates']['Quebec'] = 'Quebec';
$app_list_strings['casermastates']['Saskatchewan'] = 'Saskatchewan';
$app_list_strings['casermastates']['Yukon'] = 'Yukon';



/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['AP1_Alternative_Part'] = 'Alternative Part';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_System_Workflow_Scheduling'] = 'System Workflow Scheduling';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['UZC1_USA_ZIP_Codes'] = 'USA ZIP Codes';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['CI1_Competitive_Intelligence'] = 'Competitive Intelligence';
$app_list_strings['topic_list'][1] = 'Pricing';
$app_list_strings['topic_list'][2] = 'Personnel';
$app_list_strings['topic_list'][3] = 'Roadmap';
$app_list_strings['topic_list'][4] = 'Technology';
$app_list_strings['topic_list'][5] = 'Other';




$app_list_strings['moduleList']['DHA_PlantillasDocumentos'] = 'Mail Merge Reports';

$app_strings['LBL_YES'] = 'Yes';
$app_strings['LBL_NO'] = 'No';
$app_strings['LBL_CURRENCY_ID'] = 'Currency ID';
$app_strings['LBL_CURRENCY_NAME'] = 'Currency Name';
$app_strings['LBL_CURRENCY_SYMBOL'] = 'Currency Symbol';
$app_strings['LBL_CURRENCY_CODE'] = 'Currency Code';
$app_strings['LBL_CURRENCY_NAME_BASE'] = 'Currency Name (BC)';
$app_strings['LBL_CURRENCY_SYMBOL_BASE'] = 'Currency Symbol (BC)';
$app_strings['LBL_CURRENCY_CODE_BASE'] = 'Currency Code (BC)';
$app_strings['LBL_CURRENT_DATE'] = 'Current date';
$app_strings['LBL_CURRENT_WEEKDAY'] = 'Current weekday';
$app_strings['LBL_DIRECCION_COMPLETA'] = 'Address';
$app_strings['LBL_DIRECCION_ALTERNATIVA_COMPLETA'] = 'Alternate address';
$app_strings['LBL_EN_LETRAS'] = 'in words';
$app_strings['LBL_GENERAR_DOCUMENTO'] = 'Generate Document';
$app_strings['LBL_GENERAR_DOCUMENTO_PDF'] = 'Generate PDF Document';
$app_strings['LBL_ADJUNTAR_DOCUMENTO_GENERADO_A_EMAIL'] = 'Attach to Email';
$app_strings['LBL_ADJUNTAR_DOCUMENTO_GENERADO_A_NOTA'] = 'Attach to Note';
$app_strings['LBL_NO_HAY_PLANTILLAS_DISPONIBLES_PARA_GENERAR_DOCUMENTO'] = 'No templates available for Document Generation';


$app_list_strings['dha_boolean_list']=array (
  '' => '',  
  '1' => 'Yes',
  '0' => 'No',
);

$app_list_strings['dha_plantillasdocumentos_status_dom']=array (
  '' => '',
  'Active' => 'Active',
  'Draft' => 'Draft',
  'Under Review' => 'Under Review',
  'Pending' => 'Pending',    
  'Expired' => 'Expired',
  'Canceled' => 'Canceled',
);

$app_list_strings['dha_plantillasdocumentos_category_dom']=array (
  '' => '',
  'VENTAS' => 'Sales',
  'MARKETING' => 'Marketing',  
  'ACTIVIDADES' => 'Activities',
  'CALLCENTER' => 'Callcenter',
  'JURIDICO' => 'Legal',
  'ADMINISTRACION' => 'Administración',
  'COLABORACION' => 'Collaboration',  
  'CONFIGURACION' => 'Configuration', 
  'SOPORTE' => 'Support', 
  'OTROS' => 'Others', 
);

$app_list_strings['dha_plantillasdocumentos_subcategory_dom']=array (
  '' => '',
);

// Nota: esta lista se rellenará dinamicamente
$app_list_strings['dha_plantillasdocumentos_module_dom']=array (
  '' => '',
);

// Nota: esta lista se rellenará dinamicamente
$app_list_strings['dha_plantillasdocumentos_roles_dom']=array (
  '' => '',
);


$app_list_strings['dha_plantillasdocumentos_idiomas_dom']=array (
  'es' => 'Spanish',
  'ca' => 'Catalan',
  'es_AR' => 'Spanish (Argentina)',
  'es_MX' => 'Spanish (Mexico)',  
  'en_US' => 'English (United States)',
  'en_GB' => 'English (United Kingdom)',
  'de' => 'German',
  'fr' => 'French',
  'fr_BE' => 'French (Belgium)',
  'it_IT' => 'Italian',
  'pt_BR' => 'Portuguese (Brazil)',  
  'nl' => 'Dutch',
  'dk' => 'Danish',
  'ru' => 'Russian',
  'sv' => 'Swedish',
  'pl' => 'Polish',
  'bg' => 'Bulgarian',
  'hu_HU' => 'Hungarian',
  'cs' => 'Czech',
  'et' => 'Estonian',
  'lt' => 'Lithuanian',
  'tr_TR' => 'Turkish',
  'he' => 'Hebrew (Israel)',
  'id' => 'Indonesian',
  'sk_SK' => 'Slovak',
);

$app_list_strings['dha_plantillasdocumentos_formatos_ficheros_dom']=array (
  'DOCX' => 'Office Open XML - Ms Word (.docx)',
  'ODT' => 'OpenDocument - Writer Document (.odt)',
  'DOCM' => 'Office Open XML - Ms Word Macros Enabled (.docm)',
  'XLSX' => 'Office Open XML - Ms Excel (.xlsx)',
  'ODS' => 'OpenDocument - Calc Spreadsheet (.ods)',
  'XLSM' => 'Office Open XML - Ms Excel Macros Enabled (.xlsm)',
);

$app_list_strings['dha_plantillasdocumentos_enable_roles_dom']=array (
  'ALLOW_ALL' => 'All',
  'ALLOW_DOCX' => 'Only DOCX/ODT/XLSX/ODS',  
  'ALLOW_PDF' => 'Only PDF',
  'ALLOW_NONE' => 'None',
);


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Pricebook'] = 'Pricebook';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_CasesSurvey'] = 'Cases survey';







$app_list_strings['contactstates'][''] = '';
$app_list_strings['contactstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['contactstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['contactstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['contactstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['contactstates']['Unitedstates_California'] = 'California';
$app_list_strings['contactstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['contactstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['contactstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['contactstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['contactstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['contactstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['contactstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['contactstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['contactstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['contactstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['contactstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['contactstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['contactstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['contactstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['contactstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['contactstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['contactstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['contactstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['contactstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['contactstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['contactstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['contactstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['contactstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['contactstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['contactstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['contactstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['contactstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['contactstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['contactstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['contactstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['contactstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['contactstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['contactstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['contactstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['contactstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['contactstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['contactstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['contactstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['contactstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['contactstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['contactstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['contactstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['contactstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['contactstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['contactstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['contactstates']['Unitedstates_Wyoming'] = 'Wyoming';




/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['OS1_Outside_Sales_Activity'] = 'Outside Sales Activity';
$app_list_strings['distri_ride_along_list'][1] = 'Yes';
$app_list_strings['distri_ride_along_list'][2] = 'No';
$app_list_strings['f2f_meeting_set_by_inside_sale_list'][1] = 'Yes';
$app_list_strings['f2f_meeting_set_by_inside_sale_list'][2] = 'No';
$app_list_strings['meeting_type_list'][1] = 'Current Installer Client- F2F	';
$app_list_strings['meeting_type_list'][2] = 'Current Installer Client- Phone	';
$app_list_strings['meeting_type_list'][3] = 'Installer Prospects- F2F	';
$app_list_strings['meeting_type_list'][4] = 'Installer Prospects- Phone	';
$app_list_strings['meeting_type_list'][5] = 'Installer Lost Account- F2F	';
$app_list_strings['meeting_type_list'][6] = 'Installer Lost Account- Phone	';
$app_list_strings['meeting_type_list'][7] = 'Distri Meeting – F2F	';
$app_list_strings['meeting_type_list'][8] = 'Distri Meeting – Phone	';
$app_list_strings['meeting_type_list'][9] = 'Other Meeting – F2F	';
$app_list_strings['meeting_type_list'][10] = 'Other Meeting – Phone	';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_StatusLog'] = 'Status Log';


$app_strings['LBL_COMMON_MENU_TAKE'] = 'Take';
$app_strings['LBL_STATUS_LOG_DAYS'] = 'days';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['dtbc_Pricebook_Product'] = 'Pricebook Product';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['SN1_Serial_Number'] = 'Serial Number';
$app_list_strings['fault_description_list'][1] = 'Cables/connectors/instalation	';
$app_list_strings['fault_description_list'][2] = 'Local router	';
$app_list_strings['fault_description_list'][3] = 'DHCP issue	';
$app_list_strings['fault_description_list'][4] = 'Router Connection to the Internet	';
$app_list_strings['fault_description_list'][5] = 'Ethernet port on comm board	';
$app_list_strings['fault_description_list'][6] = 'Network Failure	';
$app_list_strings['fault_description_list'][7] = 'Modem Fault	';
$app_list_strings['fault_description_list'][8] = 'Module failure	';
$app_list_strings['fault_description_list'][9] = 'Port on comm board failure	';
$app_list_strings['fault_description_list'][10] = 'Lightning protection device	';
$app_list_strings['fault_description_list'][11] = 'Can';
$app_list_strings['fault_description_list'][12] = 'Out of range	';
$app_list_strings['fault_description_list'][13] = 'Router out of range	';
$app_list_strings['fault_description_list'][14] = 'Can';
$app_list_strings['fault_description_list'][15] = 'Power Failure	';
$app_list_strings['fault_description_list'][16] = 'Damaged Sensor port	';
$app_list_strings['fault_description_list'][17] = 'Eth Cables/connectors/installation	';
$app_list_strings['fault_description_list'][18] = 'Cellular Modem Failure	';
$app_list_strings['fault_description_list'][19] = 'Cellular Network Failure	';
$app_list_strings['fault_description_list'][20] = 'RS485 Cables/connectors/installation	';
$app_list_strings['fault_description_list'][21] = 'Zigbee Out of range	';
$app_list_strings['fault_description_list'][22] = 'WiFi Out of range	';
$app_list_strings['fault_description_list'][23] = 'Voltage out of spec	';
$app_list_strings['fault_description_list'][24] = 'Frequency out of spec	';
$app_list_strings['fault_description_list'][25] = 'Unstable grid	';
$app_list_strings['fault_description_list'][26] = 'Reverse DC connection	';
$app_list_strings['fault_description_list'][27] = 'Wrong string length	';
$app_list_strings['fault_description_list'][28] = 'DC cabling	';
$app_list_strings['fault_description_list'][29] = 'AC cabling	';
$app_list_strings['fault_description_list'][30] = 'DC fuse box	';
$app_list_strings['fault_description_list'][31] = 'AC Fuse / Circuit breaker	';
$app_list_strings['fault_description_list'][32] = 'Grounding	';
$app_list_strings['fault_description_list'][33] = 'Lack of training/knowledge	';
$app_list_strings['fault_description_list'][34] = 'Other Inverter	';
$app_list_strings['fault_description_list'][35] = 'Faulty Panel	';
$app_list_strings['fault_description_list'][36] = 'Panel do not match optimizer	';
$app_list_strings['fault_description_list'][37] = 'Communication board failure	';
$app_list_strings['fault_description_list'][38] = 'RTC Failure	';
$app_list_strings['fault_description_list'][39] = 'RTC Battery Failure	';
$app_list_strings['fault_description_list'][40] = 'SD card doesn';
$app_list_strings['fault_description_list'][41] = 'SD Card missing	';
$app_list_strings['fault_description_list'][42] = 'SD card-wrong SN	';
$app_list_strings['fault_description_list'][43] = 'Firmware not up-to-date	';
$app_list_strings['fault_description_list'][44] = 'Display Off	';
$app_list_strings['fault_description_list'][45] = 'Wrong parameters	';
$app_list_strings['fault_description_list'][46] = 'PLC issue	';
$app_list_strings['fault_description_list'][47] = 'Pairing issue	';
$app_list_strings['fault_description_list'][48] = 'Wrong Firmware	';
$app_list_strings['fault_description_list'][49] = 'Night mode	';
$app_list_strings['fault_description_list'][50] = 'Digital Fan Failure	';
$app_list_strings['fault_description_list'][51] = 'Fuse fault	';
$app_list_strings['fault_description_list'][52] = 'Power supply failure	';
$app_list_strings['fault_description_list'][53] = 'DC capacitor leakage	';
$app_list_strings['fault_description_list'][54] = 'Burnt component	';
$app_list_strings['fault_description_list'][55] = 'Component Failure	';
$app_list_strings['fault_description_list'][56] = 'Component Failure	';
$app_list_strings['fault_description_list'][57] = 'Water penetration	';
$app_list_strings['fault_description_list'][58] = 'Workmanship';
$app_list_strings['fault_description_list'][59] = 'Switch failure	';
$app_list_strings['fault_description_list'][60] = 'Reversed SN sticker	';
$app_list_strings['fault_description_list'][61] = 'Reverse cable sticker	';
$app_list_strings['fault_description_list'][62] = 'Fan not working	';
$app_list_strings['fault_description_list'][63] = 'Reverse cable polarity	';
$app_list_strings['fault_description_list'][64] = 'Net is touching the fan	';
$app_list_strings['fault_description_list'][65] = 'Wrong wiring	';
$app_list_strings['fault_description_list'][66] = 'Boost failure	';
$app_list_strings['fault_description_list'][67] = 'Connectors failure	';
$app_list_strings['fault_description_list'][68] = 'Damaged goods	';
$app_list_strings['fault_description_list'][69] = 'Full site optimizer replacement	';
$app_list_strings['fault_description_list'][70] = 'ID mismatch	';
$app_list_strings['fault_description_list'][71] = 'Input Short	';
$app_list_strings['fault_description_list'][72] = 'Output Short	';
$app_list_strings['fault_description_list'][73] = 'Vin = Vout	';
$app_list_strings['fault_description_list'][74] = 'Isolation failure	';
$app_list_strings['fault_description_list'][75] = 'Loose PEM bolt	';
$app_list_strings['fault_description_list'][76] = 'Low production	';
$app_list_strings['fault_description_list'][77] = 'Multy resets	';
$app_list_strings['fault_description_list'][78] = 'OPJ-melted	';
$app_list_strings['fault_description_list'][79] = 'OPJ-not working	';
$app_list_strings['fault_description_list'][80] = 'RFI';
$app_list_strings['fault_description_list'][81] = 'Volt';
$app_list_strings['fault_description_list'][82] = 'Volt> 1V, dead optimizer	';
$app_list_strings['fault_description_list'][83] = 'Working optimizer, not reporting	';
$app_list_strings['fault_description_list'][84] = 'Wrong FW	';
$app_list_strings['fault_description_list'][85] = 'Zep bracket	';
$app_list_strings['fault_description_list'][86] = 'Add physical layout	';
$app_list_strings['fault_description_list'][87] = 'Alerts';
$app_list_strings['fault_description_list'][88] = 'Data presentation problem';
$app_list_strings['fault_description_list'][89] = 'Fix physical layout	';
$app_list_strings['fault_description_list'][90] = 'General question on monitoring site	';
$app_list_strings['fault_description_list'][91] = 'Known bug	';
$app_list_strings['fault_description_list'][92] = 'Logical layout	';
$app_list_strings['fault_description_list'][93] = 'Mobile devices	';
$app_list_strings['fault_description_list'][94] = 'Monitoring server is down	';
$app_list_strings['fault_description_list'][95] = 'User name / Password	';
$app_list_strings['fault_description_list'][96] = 'Permissions';
$app_list_strings['fault_description_list'][97] = 'Register owner without installer	';
$app_list_strings['fault_description_list'][98] = 'Registration';
$app_list_strings['fault_description_list'][99] = 'Site Mapper	';
$app_list_strings['fault_description_list'][100] = 'Site setup without physical	';
$app_list_strings['fault_description_list'][101] = 'Site setup with physical	';
$app_list_strings['fault_description_list'][102] = 'Update details	';
$app_list_strings['fault_description_list'][103] = 'Change account for the site	';
$app_list_strings['fault_description_list'][104] = 'Missing bracket	';
$app_list_strings['fault_description_list'][105] = 'Missing bracket screws	';
$app_list_strings['fault_description_list'][106] = 'Missing GND washers	';
$app_list_strings['fault_description_list'][107] = 'Missing manual	';
$app_list_strings['fault_description_list'][108] = 'Missing quick installation guide	';
$app_list_strings['fault_description_list'][109] = 'Advertisement';
$app_list_strings['fault_description_list'][110] = 'Damaged package	';
$app_list_strings['fault_description_list'][111] = 'No fault found	';
$app_list_strings['fault_description_list'][112] = 'Request for compensation	';
$app_list_strings['fault_description_list'][113] = 'Sales opportunity	';
$app_list_strings['fault_description_list'][114] = 'SF password/registration issue	';
$app_list_strings['fault_description_list'][115] = 'Shipment details	';
$app_list_strings['fault_description_list'][116] = 'Stickers	';
$app_list_strings['fault_description_list'][117] = 'System design assistance	';
$app_list_strings['fault_description_list'][118] = 'Training request	';
$app_list_strings['fault_description_list'][119] = 'Wrong Shipment/Packaging	';
$app_list_strings['fault_description_list'][120] = 'Warranty';
$app_list_strings['fault_description_list'][121] = 'Site designer	';
$app_list_strings['fault_description_list'][122] = 'Android mapper app	';
$app_list_strings['fault_description_list'][123] = 'iphone mapper app	';
$app_list_strings['fault_description_list'][124] = 'Configuration tool	';
$app_list_strings['fault_description_list'][125] = 'Solaredge key	';
$app_list_strings['fault_description_list'][126] = '3rd party inverter com problem	';
$app_list_strings['fault_description_list'][127] = 'Motherboard failure	';
$app_list_strings['fault_description_list'][128] = 'Duplication';
$app_list_strings['fault_description_list'][129] = 'No reply from the customer	';
$app_list_strings['fault_description_list'][130] = 'Faulty CCG	';
$app_list_strings['fault_description_list'][131] = 'Damaged ambient temperature sensor	';
$app_list_strings['fault_description_list'][132] = 'Damaged CT	';
$app_list_strings['fault_description_list'][133] = 'Meter fault	';
$app_list_strings['fault_description_list'][134] = 'Damaged module temperature sensor	';
$app_list_strings['fault_description_list'][135] = 'Damaged wind sensor';
$app_list_strings['fault_description_list'][136] = 'Reverse CT	';
$app_list_strings['fault_description_list'][137] = 'Faulty FFG	';
$app_list_strings['fault_description_list'][138] = '1; RCD Test	';
$app_list_strings['fault_description_list'][139] = '2; HS Relay Test	';
$app_list_strings['fault_description_list'][140] = '3; LS Relay Test	';
$app_list_strings['fault_description_list'][141] = '4; RCD Mon relays connected	';
$app_list_strings['fault_description_list'][142] = '5; RCD Mon relays disconnected	';
$app_list_strings['fault_description_list'][143] = '6; Relays Mon	';
$app_list_strings['fault_description_list'][144] = '7; HS Relay Mon	';
$app_list_strings['fault_description_list'][145] = '8; LS Relay Mon	';
$app_list_strings['fault_description_list'][146] = '1; Rcd Test Circuit fault	';
$app_list_strings['fault_description_list'][147] = '1; No alerts	';
$app_list_strings['fault_description_list'][148] = '2; Too many alerts	';
$app_list_strings['fault_description_list'][149] = '45; Communication Error	';
$app_list_strings['fault_description_list'][150] = '49; 0x08000010; Communication	';
$app_list_strings['fault_description_list'][151] = '52; 0x08000013; Vin Decrease Timeout	';
$app_list_strings['fault_description_list'][152] = '53; 0x08000014; Multiple Errors	';
$app_list_strings['fault_description_list'][153] = '54; 0x08000015; JM Sync Timeout	';
$app_list_strings['fault_description_list'][154] = '55; 0x08000016; Capcitor estimation	';
$app_list_strings['fault_description_list'][155] = '56; 0x08000017; RsrvdMngrErr 3	';
$app_list_strings['fault_description_list'][156] = '57; 0x08000018; RsrvdMngrErr 4	';
$app_list_strings['fault_description_list'][157] = '58; 0x08000019; V-L1 Max1	';
$app_list_strings['fault_description_list'][158] = '59; 0x0800001A; V-L2 Max1	';
$app_list_strings['fault_description_list'][159] = '60; 0x0800001B; V-L3Max1	';
$app_list_strings['fault_description_list'][160] = '61; 0x0800001C; V-L1 Min1	';
$app_list_strings['fault_description_list'][161] = '62; 0x0800001D; V-L2 Min1	';
$app_list_strings['fault_description_list'][162] = '63; 0x0800001E; V-L3 Min1	';
$app_list_strings['fault_description_list'][163] = '64; 0x0800001F; V-L1 Max2	';
$app_list_strings['fault_description_list'][164] = '65; 0x08000020; V-L2 Max2	';
$app_list_strings['fault_description_list'][165] = '66; 0x08000021; V-L3 Max2	';
$app_list_strings['fault_description_list'][166] = '67; 0x08000022; V-L1 Min2	';
$app_list_strings['fault_description_list'][167] = '68; 0x08000023; V-L2 Min2	';
$app_list_strings['fault_description_list'][168] = '69; 0x08000024; V-L3 Min2	';
$app_list_strings['fault_description_list'][169] = '71; 0x08000026; V-Line Min	';
$app_list_strings['fault_description_list'][170] = '72; 0x08000027; I-ACDC L1	';
$app_list_strings['fault_description_list'][171] = '73; 0x08000028; Unknown	';
$app_list_strings['fault_description_list'][172] = '74; 0x08000029; I-ACDC L2	';
$app_list_strings['fault_description_list'][173] = '75; 0x0800002A; I-ACDC L3	';
$app_list_strings['fault_description_list'][174] = '76; 0x0800002B; I-RCD 1	';
$app_list_strings['fault_description_list'][175] = '77; 0x0800002C; I-RCD 2	';
$app_list_strings['fault_description_list'][176] = '78; 0x0800002D; JP Sync Timeout	';
$app_list_strings['fault_description_list'][177] = '79; 0x0800002E; F-L1 Max	';
$app_list_strings['fault_description_list'][178] = '81; 0x08000030; F-L3 Max	';
$app_list_strings['fault_description_list'][179] = '82; 0x08000031; F-L1 Min	';
$app_list_strings['fault_description_list'][180] = '83; 0x08000032; F-L2 Min	';
$app_list_strings['fault_description_list'][181] = '84; 0x08000033; F-L3 Min	';
$app_list_strings['fault_description_list'][182] = '85; 0x08000034; VinP Max	';
$app_list_strings['fault_description_list'][183] = '86; 0x08000035; VinM Max	';
$app_list_strings['fault_description_list'][184] = '87; 0x08000036; Active anti islanding	';
$app_list_strings['fault_description_list'][185] = '88; 0x08000037; Vin Max	';
$app_list_strings['fault_description_list'][186] = '90; 0x08000039; TZ RCD	';
$app_list_strings['fault_description_list'][187] = '91; 0x0800003A; TZ L1	';
$app_list_strings['fault_description_list'][188] = '92; 0x0800003B; TZ L2	';
$app_list_strings['fault_description_list'][189] = '93; 0x0800003C; TZ L3	';
$app_list_strings['fault_description_list'][190] = '96; 0x0800003F; Iac L1 Max	';
$app_list_strings['fault_description_list'][191] = '97; 0x08000040; Iac L2 Max	';
$app_list_strings['fault_description_list'][192] = '98; 0x08000041; Iac L3 Max	';
$app_list_strings['fault_description_list'][193] = '99; 0x08000042; Vsrg L1 Max (Vfilter spike)	';
$app_list_strings['fault_description_list'][194] = '100; 0x08000043; Vsrg L2 Max (Vfilter spike)	';
$app_list_strings['fault_description_list'][195] = '101; 0x08000044; Vsrg L3 Max (Vfilter spike)	';
$app_list_strings['fault_description_list'][196] = '102; 0x08000045; VDC Pluse Max, (spike)	';
$app_list_strings['fault_description_list'][197] = '103; 0x08000046; VDC Minus Max, (spike)	';
$app_list_strings['fault_description_list'][198] = '104; 0x08000047; Overtemp	';
$app_list_strings['fault_description_list'][199] = '105; 0x08000048; Undertemp	';
$app_list_strings['fault_description_list'][200] = '112; 0x0800004F; Wrong Ac connection	';
$app_list_strings['fault_description_list'][201] = '113; 0x08000050; Main State State	';
$app_list_strings['fault_description_list'][202] = '115; 0x08000052; VDC Cap banks unbalanced	';
$app_list_strings['fault_description_list'][203] = '116; 0x08000053; DC Charged	';
$app_list_strings['fault_description_list'][204] = '118; 0x08000055; GRM	';
$app_list_strings['fault_description_list'][205] = '119; 0x08000056; INIT	';
$app_list_strings['fault_description_list'][206] = '120; 0x08000057; Temp Sensor	';
$app_list_strings['fault_description_list'][207] = '121; 0x08000058; Isolation	';
$app_list_strings['fault_description_list'][208] = '122; 0x08000059; Relay Test	';
$app_list_strings['fault_description_list'][209] = '123; 0x0800005A; Ofset Gain	';
$app_list_strings['fault_description_list'][210] = '124; 0x0800005B; RCD Test	';
$app_list_strings['fault_description_list'][211] = '128; 0x0800005F; RsrvdInit2	';
$app_list_strings['fault_description_list'][212] = '129; 0x08000060; RsrvdInit3	';
$app_list_strings['fault_description_list'][213] = '0; 0x02000000; Wakeup	';
$app_list_strings['fault_description_list'][214] = '1; 0x02000001; turn off	';
$app_list_strings['fault_description_list'][215] = '2; 0x02000002; Night mode	';
$app_list_strings['fault_description_list'][216] = '3; 0x02000003; Reset	';
$app_list_strings['fault_description_list'][217] = '4; 0x02000004; DSP1 DSP2 comm	';
$app_list_strings['fault_description_list'][218] = '5; 0x02000005; DSP2 unexpected mode	';
$app_list_strings['fault_description_list'][219] = '6; 0x02000006; DSP1 Grid Error.	';
$app_list_strings['fault_description_list'][220] = '7; 0x02000007; VDC slow discharge	';
$app_list_strings['fault_description_list'][221] = '8; 0x02000008; Unknown DSP2 Error	';
$app_list_strings['fault_description_list'][222] = '9; 0x02000009; Iout TZ	';
$app_list_strings['fault_description_list'][223] = '10; 0x0200000A; RCD TZ	';
$app_list_strings['fault_description_list'][224] = '12; 0x0200000C; Obs	';
$app_list_strings['fault_description_list'][225] = '13; 0x0200000D; Iout max	';
$app_list_strings['fault_description_list'][226] = '14; 0x0200000E; Vac surge	';
$app_list_strings['fault_description_list'][227] = '15; 0x0200000F; VDC surge	';
$app_list_strings['fault_description_list'][228] = '16; 0x02000010; Vref1V or Vref2V > +/-3%	';
$app_list_strings['fault_description_list'][229] = '17; 0x02000011; Temprature Max	';
$app_list_strings['fault_description_list'][230] = '18; 0x02000012; WD error	';
$app_list_strings['fault_description_list'][231] = '19; 0x02000013; SW error	';
$app_list_strings['fault_description_list'][232] = '20; 0x02000014; SW error	';
$app_list_strings['fault_description_list'][233] = '21; 0x02000015; SW error	';
$app_list_strings['fault_description_list'][234] = '22; 0x02000016; SW error	';
$app_list_strings['fault_description_list'][235] = '23; 0x02000017; VNN error	';
$app_list_strings['fault_description_list'][236] = '24; 0x02000018; Temprature Min	';
$app_list_strings['fault_description_list'][237] = '25; 0x02000019; Isolation error	';
$app_list_strings['fault_description_list'][238] = '26; 0x0200001A; Relay test error	';
$app_list_strings['fault_description_list'][239] = '27; 0x0200001B; Obs	';
$app_list_strings['fault_description_list'][240] = '28; 0x0200001C; RCD test error	';
$app_list_strings['fault_description_list'][241] = '29; 0x0200001D; VLN max	';
$app_list_strings['fault_description_list'][242] = '30; 0x0200001E; VLN min	';
$app_list_strings['fault_description_list'][243] = '31; 0x0200001F; Vg max	';
$app_list_strings['fault_description_list'][244] = '32; 0x02000020; Vg min	';
$app_list_strings['fault_description_list'][245] = '33; 0x02000021; Vg2 max	';
$app_list_strings['fault_description_list'][246] = '34; 0x02000022; F max	';
$app_list_strings['fault_description_list'][247] = '35; 0x02000023; F min	';
$app_list_strings['fault_description_list'][248] = '36; 0x02000024; Iacdc	';
$app_list_strings['fault_description_list'][249] = '37; 0x02000025; IRCD 1	';
$app_list_strings['fault_description_list'][250] = '38; 0x02000026; IRCD 2	';
$app_list_strings['fault_description_list'][251] = '39; 0x02000027; PLL Sync error	';
$app_list_strings['fault_description_list'][252] = '40; 0x02000028; Islanding (DSP1)	';
$app_list_strings['fault_description_list'][253] = '41; 0x02000029; Vg2 min	';
$app_list_strings['fault_description_list'][254] = '42; 0x0200002A; Unknown Error	';
$app_list_strings['fault_description_list'][255] = '43; 0x0200002B; obs	';
$app_list_strings['fault_description_list'][256] = '44; 0x0200002C; No country selected	';
$app_list_strings['fault_description_list'][257] = '45; 0x0200002D; Portia - DSP1 comm error	';
$app_list_strings['fault_description_list'][258] = '132; 0x02000084; Exceeded 24H error	';
$app_list_strings['fault_description_list'][259] = '134; 0x02000086; DSP1- DSP2 VDC diff max	';
$app_list_strings['fault_description_list'][260] = '141; 0x0200008D; Switch on	';
$app_list_strings['fault_description_list'][261] = '142; 0x0200008E; Switch off	';
$app_list_strings['fault_description_list'][262] = '145; 0x02000091; VDC max	';
$app_list_strings['fault_description_list'][263] = '148; 0x02000094; Low power	';
$app_list_strings['fault_description_list'][264] = '149; 0x02000095; Power train error	';
$app_list_strings['fault_description_list'][265] = '150; 0x02000096; Arc-DSP1	';
$app_list_strings['fault_description_list'][266] = '151; 0x02000097; Arc-DSP2	';
$app_list_strings['fault_description_list'][267] = '152; 0x02000098; Tx test (AKA arc test error)	';
$app_list_strings['fault_description_list'][268] = '153; 0x02000099; Vfilter discharge	';
$app_list_strings['fault_description_list'][269] = '154; 0x0200009A; Arc -debug error	';
$app_list_strings['fault_description_list'][270] = '156; 0x0200009C; DRV TZ false alarm	';
$app_list_strings['fault_description_list'][271] = '256; 0x02000100; Fan 1 is not working	';
$app_list_strings['fault_description_list'][272] = '257; 0x02000101; Fan 2 is not working	';
$app_list_strings['fault_description_list'][273] = '262; 0x02000106; Fan controller comm error	';
$app_list_strings['fault_description_list'][274] = '269; 0x0200010D; External	';
$app_list_strings['fault_description_list'][275] = 'DCD A/B issue	';
$app_list_strings['fault_description_list'][276] = 'Does not line up with inverter	';
$app_list_strings['fault_description_list'][277] = 'workmanship failure	';
$app_list_strings['fault_description_list'][278] = 'Wrong meter configuration	';
$app_list_strings['fault_description_list'][279] = 'Humming inverter	';
$app_list_strings['fault_description_list'][280] = 'Humming Optimizer	';
$app_list_strings['fault_description_list'][281] = 'Inverter not responding	';
$app_list_strings['fault_description_list'][282] = 'PID Issue	';
$app_list_strings['fault_description_list'][283] = 'IEC Certification	';
$app_list_strings['fault_description_list'][284] = 'Digital board upgrade	';
$app_list_strings['fault_description_list'][285] = 'Communication board upgrade	';
$app_list_strings['fault_description_list'][286] = 'Both Comm and Digital upgrade	';
$app_list_strings['fault_description_list'][287] = 'No communication to site	';
$app_list_strings['fault_description_list'][288] = 'Unclear display	';
$app_list_strings['fault_description_list'][289] = 'Buttons on the Portia fail	';
$app_list_strings['fault_description_list'][290] = 'Wrong ID	';
$app_list_strings['fault_description_list'][291] = 'ID 0	';
$app_list_strings['fault_description_list'][292] = 'Green Button	';
$app_list_strings['fault_description_list'][293] = 'DC Switch	';
$app_list_strings['fault_description_list'][294] = 'Parameter Change	';
$app_list_strings['fault_description_list'][295] = 'Country Select	';
$app_list_strings['fault_description_list'][296] = 'Remote pairing	';
$app_list_strings['fault_description_list'][297] = 'Config update	';
$app_list_strings['fault_description_list'][298] = 'Reset	';
$app_list_strings['fault_description_list'][299] = 'Activation/ Commissioning	';
$app_list_strings['fault_description_list'][300] = 'Kiosk config	';
$app_list_strings['fault_description_list'][301] = 'API config	';
$app_list_strings['fault_description_list'][302] = 'Public site	';
$app_list_strings['fault_description_list'][303] = 'Manual Activation	';
$app_list_strings['fault_description_list'][304] = 'General Tech Question	';
$app_list_strings['fault_description_list'][305] = 'RGM	';
$app_list_strings['fault_description_list'][306] = 'Slave detect fail	';
$app_list_strings['fault_description_list'][307] = 'Fan cleaning	';
$app_list_strings['fault_description_list'][308] = 'Portia HW upgrade	';
$app_list_strings['fault_description_list'][309] = 'WiFi/Zigbee	';
$app_list_strings['fault_description_list'][310] = 'Cellular Modem	';
$app_list_strings['fault_description_list'][311] = 'Digital board failure	';
$app_list_strings['fault_description_list'][312] = 'Wakeup loop	';
$app_list_strings['fault_description_list'][313] = 'Continuously in wakeup loop	';
$app_list_strings['fault_description_list'][314] = 'Broken chocks	';
$app_list_strings['fault_description_list'][315] = 'Wrong/ Broken Cover	';
$app_list_strings['fault_description_list'][316] = 'Labelling	';
$app_list_strings['fault_description_list'][317] = 'Connectors	';
$app_list_strings['fault_description_list'][318] = 'High temperature	';
$app_list_strings['fault_description_list'][319] = 'Faulty DCD	';
$app_list_strings['fault_description_list'][320] = 'Devices Compatibility	';
$app_list_strings['fault_description_list'][321] = 'iPhone app	';
$app_list_strings['fault_description_list'][322] = 'Android app	';
$app_list_strings['fault_description_list'][323] = 'Standard Compliance	';
$app_list_strings['fault_description_list'][324] = 'Faulty connector	';
$app_list_strings['fault_description_list'][325] = 'Faulty Bypass Diode	';
$app_list_strings['fault_description_list'][326] = 'Requested for investigation	';
$app_list_strings['fault_description_list'][327] = 'Damaged USB connector	';
$app_list_strings['fault_description_list'][328] = 'Power Supply	';
$app_list_strings['fault_description_list'][329] = 'Branch Cable	';
$app_list_strings['fault_description_list'][330] = 'Can';
$app_list_strings['fault_description_list'][331] = 'Can';
$app_list_strings['fault_description_list'][332] = 'Shaded panel	';
$app_list_strings['fault_description_list'][333] = 'Dirty panel	';
$app_list_strings['fault_description_list'][334] = 'RS485 port failure	';
$app_list_strings['fault_description_list'][335] = 'Ethernet port failure	';
$app_list_strings['fault_description_list'][336] = 'S0/PRI port failure	';
$app_list_strings['fault_description_list'][337] = 'Wrong Measurements	';
$app_list_strings['fault_description_list'][338] = 'Lightning strike	';
$app_list_strings['fault_description_list'][339] = 'Flood	';
$app_list_strings['fault_description_list'][340] = 'Storm damage	';
$app_list_strings['fault_description_list'][341] = 'Wrong Bracket	';
$app_list_strings['fault_description_list'][342] = 'No Communication to Portia	';
$app_list_strings['fault_description_list'][343] = 'Connector failure	';
$app_list_strings['fault_description_list'][344] = 'Damaged Inverter	';
$app_list_strings['fault_description_list'][345] = 'Damaged Item	';
$app_list_strings['fault_description_list'][346] = 'Browser issue	';
$app_list_strings['fault_description_list'][347] = 'NO S_OK	';
$app_list_strings['fault_description_list'][348] = 'Stuck packet	';
$app_list_strings['fault_description_list'][349] = 'TCP connect fail	';
$app_list_strings['fault_description_list'][350] = 'Temperature Sense Fault	';
$app_list_strings['fault_description_list'][351] = '161 ;0x08000075; Bank+ Over Voltage	';
$app_list_strings['fault_description_list'][352] = '162 ;0x08000076; Bank- Over Voltage	';
$app_list_strings['fault_description_list'][353] = '164; 0x08000078; L2-Over Temperature	';
$app_list_strings['fault_description_list'][354] = '166; 0x0800007A;L1-Over Current	';
$app_list_strings['fault_description_list'][355] = '167 ;0x0800007B; L2-Over Current	';
$app_list_strings['fault_description_list'][356] = '168; 0x0800007C L3-Over Current	';
$app_list_strings['fault_description_list'][357] = '169; 0x0800007D; L1-Driver Short	';
$app_list_strings['fault_description_list'][358] = '170; 0x0800007E; L2-Driver Short	';
$app_list_strings['fault_description_list'][359] = '171; 0x0800007F; L3-Driver Short	';
$app_list_strings['fault_description_list'][360] = '173; 0x08000081; RCD Over Current	';
$app_list_strings['fault_description_list'][361] = 'Shows Tesla error code list	';
$app_list_strings['fault_description_list'][362] = 'Burnt Fuse	';
$app_list_strings['fault_description_list'][363] = 'Reverse Polarity	';
$app_list_strings['fault_description_list'][364] = 'DC Power Supply Failure	';
$app_list_strings['fault_description_list'][365] = 'Missing Screws	';
$app_list_strings['fault_description_list'][366] = 'Enable Switch Failure	';
$app_list_strings['fault_description_list'][367] = 'DC Switch Failure	';
$app_list_strings['fault_description_list'][368] = 'Source Selector Fault	';
$app_list_strings['fault_description_list'][369] = 'Circuit Breaker Fault	';
$app_list_strings['fault_description_list'][370] = 'RGM Fault	';
$app_list_strings['fault_description_list'][371] = 'Communication to Meter Fault	';
$app_list_strings['fault_description_list'][372] = 'Communication to Battery Fault	';
$app_list_strings['fault_description_list'][373] = 'Local Configuration Issue	';
$app_list_strings['fault_description_list'][374] = 'Configuration from Server Issue (TOU)	';
$app_list_strings['fault_description_list'][375] = 'Green Button Connector Failure	';
$app_list_strings['fault_description_list'][376] = 'Communication board Stuck	';
$app_list_strings['fault_description_list'][377] = 'Missing feature	';
$app_list_strings['fault_description_list'][378] = 'Wrong country selection	';
$app_list_strings['fault_description_list'][379] = 'Noisy Fan	';
$app_list_strings['fault_description_list'][380] = '2072; Hardware Fault Asserted	';
$app_list_strings['fault_description_list'][381] = '2136; State Transition Condition Not Met	';
$app_list_strings['fault_description_list'][382] = '4126; Fan Fault	';
$app_list_strings['fault_description_list'][383] = '4161; Fan Alert	';
$app_list_strings['fault_description_list'][384] = '4162; Fan Alert	';
$app_list_strings['fault_description_list'][385] = '4163; Fan Alert	';
$app_list_strings['fault_description_list'][386] = '2085; Battery Over Temperature Fault	';
$app_list_strings['fault_description_list'][387] = '2086; Hardware Over Temperature Fault	';
$app_list_strings['fault_description_list'][388] = '2098; Over Temperature Alert	';
$app_list_strings['fault_description_list'][389] = '2056; Converter Over Temperature Fault	';
$app_list_strings['fault_description_list'][390] = '2076; Low Coolant Flow Fault	';
$app_list_strings['fault_description_list'][391] = '2057; Enable Line De-Asserted Fault	';
$app_list_strings['fault_description_list'][392] = '2132; Precharge Fault	';
$app_list_strings['fault_description_list'][393] = '4138; Pod MIA	';
$app_list_strings['fault_description_list'][394] = '4167; Communication Timeout	';
$app_list_strings['fault_description_list'][395] = '4209-4224; Update Failed (Blocking)	';
$app_list_strings['fault_description_list'][396] = '4133; Communication Alert	';
$app_list_strings['fault_description_list'][397] = '4134; Communication Alert	';
$app_list_strings['fault_description_list'][398] = '4137; Modbus Communication Fault	';
$app_list_strings['fault_description_list'][399] = '4139; CAN Timeout	';
$app_list_strings['fault_description_list'][400] = '4140; CAN bus off	';
$app_list_strings['fault_description_list'][401] = '4148; Communication Alert	';
$app_list_strings['fault_description_list'][402] = '4136; 12V Logic Supply Alert	';
$app_list_strings['fault_description_list'][403] = '4147; 12V Thermal Supply Alert	';
$app_list_strings['fault_description_list'][404] = '2123; Darkstart Timeout	';
$app_list_strings['fault_description_list'][405] = '2082; Hardware Self Test Failure Fault	';
$app_list_strings['fault_description_list'][406] = '2083; Hardware Self Test Failure Fault	';
$app_list_strings['fault_description_list'][407] = '2058; DC Bus Voltage Sensor Fault	';
$app_list_strings['fault_description_list'][408] = '2059; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][409] = '2060; Battery Current Sensor Fault	';
$app_list_strings['fault_description_list'][410] = '2061; Over Temperature Alert	';
$app_list_strings['fault_description_list'][411] = '2064; Battery Current Sensor Fault	';
$app_list_strings['fault_description_list'][412] = '2065; Battery Voltage Sensor Fault	';
$app_list_strings['fault_description_list'][413] = '2066; Battery Current Sensor Fault	';
$app_list_strings['fault_description_list'][414] = '2067; Battery Logic Power Fault	';
$app_list_strings['fault_description_list'][415] = '2070; Battery Isolation Sensor Fault	';
$app_list_strings['fault_description_list'][416] = '2071; Battery Current Sensor Fault	';
$app_list_strings['fault_description_list'][417] = '2074; Hardware Fault Asserted	';
$app_list_strings['fault_description_list'][418] = '2075; Hardware Fault Alert	';
$app_list_strings['fault_description_list'][419] = '2078; Battery Under Voltage Fault	';
$app_list_strings['fault_description_list'][420] = '2080; Battery Under Voltage Alert	';
$app_list_strings['fault_description_list'][421] = '2090; Processor Uncorrectable Error Fault	';
$app_list_strings['fault_description_list'][422] = '2095; Battery Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][423] = '2096; Voltage Sensor Fault	';
$app_list_strings['fault_description_list'][424] = '2097; Voltage Sensor Fault	';
$app_list_strings['fault_description_list'][425] = '2099; Voltage Sensor Fault	';
$app_list_strings['fault_description_list'][426] = '2100; Voltage Sensor Fault	';
$app_list_strings['fault_description_list'][427] = '2133; Processor  Communication Alert	';
$app_list_strings['fault_description_list'][428] = '2135; Voltage Sensor Alert	';
$app_list_strings['fault_description_list'][429] = '2084; Hardware Self Test Failure Fault	';
$app_list_strings['fault_description_list'][430] = '2092; Battery Under Voltage Warning	';
$app_list_strings['fault_description_list'][431] = '2093; Battery Over Voltage Warning	';
$app_list_strings['fault_description_list'][432] = '4; Discharge Power Limit Alert	';
$app_list_strings['fault_description_list'][433] = '2049; Battery Over Current Fault	';
$app_list_strings['fault_description_list'][434] = '2050; Battery Over Voltage Fault	';
$app_list_strings['fault_description_list'][435] = '2052; Battery Under Voltage Fault	';
$app_list_strings['fault_description_list'][436] = '2053; Battery Over Voltage Alert	';
$app_list_strings['fault_description_list'][437] = '2054; Battery Under Voltage Alert	';
$app_list_strings['fault_description_list'][438] = '2055; Battery Over Current Alert	';
$app_list_strings['fault_description_list'][439] = '2062; DC Bus Over Voltage Fault	';
$app_list_strings['fault_description_list'][440] = '2063; DC Bus Under Voltage Fault	';
$app_list_strings['fault_description_list'][441] = '2068; DC Bus Over Voltage Alert	';
$app_list_strings['fault_description_list'][442] = '2069; DC Bus Under Voltage Alert	';
$app_list_strings['fault_description_list'][443] = '2069; DC Bus Under Voltage Alert	';
$app_list_strings['fault_description_list'][444] = '2079; Battery Over Voltage Alert	';
$app_list_strings['fault_description_list'][445] = '2081; Hardware Reset Fault	';
$app_list_strings['fault_description_list'][446] = '2087; Processor Error Fault	';
$app_list_strings['fault_description_list'][447] = '2088; Processor Error Alert	';
$app_list_strings['fault_description_list'][448] = '2089; Processor Error Fault	';
$app_list_strings['fault_description_list'][449] = '2091; Battery Over Temperature Alert	';
$app_list_strings['fault_description_list'][450] = '2094; Battery Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][451] = '2101; Processor MIA Alert	';
$app_list_strings['fault_description_list'][452] = '2102; Processor Communication Alert	';
$app_list_strings['fault_description_list'][453] = '2105; Isolation Fault	';
$app_list_strings['fault_description_list'][454] = '2108; CAN Alert	';
$app_list_strings['fault_description_list'][455] = '2109; Processor Alert	';
$app_list_strings['fault_description_list'][456] = '2110; Cooling System Alert	';
$app_list_strings['fault_description_list'][457] = '2111; Battery Imbalance Fault	';
$app_list_strings['fault_description_list'][458] = '2113; Battery High Resistance Alert	';
$app_list_strings['fault_description_list'][459] = '2115; Not Enough Energy to Precharge	';
$app_list_strings['fault_description_list'][460] = '2117; HWID Mismatch	';
$app_list_strings['fault_description_list'][461] = '2118; HWID and OTP Mismatch	';
$app_list_strings['fault_description_list'][462] = '2119; Battery Isolation Warning	';
$app_list_strings['fault_description_list'][463] = '2124; Droop Alert	';
$app_list_strings['fault_description_list'][464] = '2127; Converter Over Current Fault	';
$app_list_strings['fault_description_list'][465] = '2128; Converter Over Current Fault	';
$app_list_strings['fault_description_list'][466] = '2129; Converter Over Current Fault	';
$app_list_strings['fault_description_list'][467] = '2130; Converter Over Current Fault	';
$app_list_strings['fault_description_list'][468] = '2131; Converter Sensor Alert	';
$app_list_strings['fault_description_list'][469] = '2134; Master / Slave Mismatch	';
$app_list_strings['fault_description_list'][470] = '2137; Battery Current Regulation	';
$app_list_strings['fault_description_list'][471] = '4097; Deadband Lower Limit Alert	';
$app_list_strings['fault_description_list'][472] = '4098; Deadband Upper Limit Alert	';
$app_list_strings['fault_description_list'][473] = '4099; Charge Power Limit Alert	';
$app_list_strings['fault_description_list'][474] = '4100; Discharge Power Limit Alert	';
$app_list_strings['fault_description_list'][475] = '4103; Communication Alert	';
$app_list_strings['fault_description_list'][476] = '4104; Communication Alert	';
$app_list_strings['fault_description_list'][477] = '4109; Pump 1 Alert	';
$app_list_strings['fault_description_list'][478] = '4110; Pump 2 Alert	';
$app_list_strings['fault_description_list'][479] = '4111; Pump Alert	';
$app_list_strings['fault_description_list'][480] = '4112; Pump Fault	';
$app_list_strings['fault_description_list'][481] = '4117; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][482] = '4118; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][483] = '4119; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][484] = '4120; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][485] = '4121; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][486] = '4122; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][487] = '4135; 5V Alert	';
$app_list_strings['fault_description_list'][488] = '4141; Temperature Sensor Fault	';
$app_list_strings['fault_description_list'][489] = '4149; Modbus Valid Data Ranges	';
$app_list_strings['fault_description_list'][490] = '4155; Pump Alert	';
$app_list_strings['fault_description_list'][491] = '4156; Pod Communication Alert	';
$app_list_strings['fault_description_list'][492] = '4157; Communication Alert	';
$app_list_strings['fault_description_list'][493] = '4158; Pump (Radiator) Alert	';
$app_list_strings['fault_description_list'][494] = '4159; Pump (Radiator) Alert	';
$app_list_strings['fault_description_list'][495] = '4160; Pump (Radiator) Alert	';
$app_list_strings['fault_description_list'][496] = '4172; Pump (Bypass) Alert	';
$app_list_strings['fault_description_list'][497] = '4173; Pump (Bypass) Alert	';
$app_list_strings['fault_description_list'][498] = '4174; Pump (Bypass) Alert	';
$app_list_strings['fault_description_list'][499] = '2073; Hardware Fault Asserted	';
$app_list_strings['fault_description_list'][500] = '2106; Firmware Alert	';
$app_list_strings['fault_description_list'][501] = '2107; Firmware Alert	';
$app_list_strings['fault_description_list'][502] = '2112; Battery Low SOH	';
$app_list_strings['fault_description_list'][503] = '2116; DC Bus Voltage Unexpected	';
$app_list_strings['fault_description_list'][504] = '2120; SOC Change Alert	';
$app_list_strings['fault_description_list'][505] = '2122; Firmware Fault	';
$app_list_strings['fault_description_list'][506] = '4102; Firmware Fault	';
$app_list_strings['fault_description_list'][507] = '4150; Firmware Alert	';
$app_list_strings['fault_description_list'][508] = '4151; Firmware Alert	';
$app_list_strings['fault_description_list'][509] = '4186; Firmware Warning	';
$app_list_strings['fault_description_list'][510] = '4187; UDS Request Overflow	';
$app_list_strings['fault_description_list'][511] = '4193; Update Failed (blocking)	';
$app_list_strings['fault_description_list'][512] = '4193-4208; Update Failed (Non-blocking)	';
$app_list_strings['fault_description_list'][513] = '2114; Low Coolant Flow Fault	';
$app_list_strings['fault_description_list'][514] = 'General Questions	';
$app_list_strings['fault_description_list'][515] = '44 ; 3xB ; No Country Selected	';
$app_list_strings['fault_description_list'][516] = '45 ; 3x2 ; SW Error	';
$app_list_strings['fault_description_list'][517] = '49 ; 8x10 ; Communication	';
$app_list_strings['fault_description_list'][518] = '52 ; 8x13 ; Vin Decrease Timeout	';
$app_list_strings['fault_description_list'][519] = '53 ; 8x14 ; Multi Errors	';
$app_list_strings['fault_description_list'][520] = '54 ; 8x15 ; JM Sync Timeout	';
$app_list_strings['fault_description_list'][521] = '56 ; 8x17 ; RsrvdMngrErr 3	';
$app_list_strings['fault_description_list'][522] = '57 ; 8x18 ; RsrvdMngrErr 4	';
$app_list_strings['fault_description_list'][523] = '58 ; 8x19 ; V-L1 Max1	';
$app_list_strings['fault_description_list'][524] = '59 ; 8x1A ; V-L2 Max1	';
$app_list_strings['fault_description_list'][525] = '60 ; 8x1B ; V-L3Max1	';
$app_list_strings['fault_description_list'][526] = '61 ; 8x1C ; V-L1 Min1	';
$app_list_strings['fault_description_list'][527] = '62 ; 8x1D ; V-L2 Min1	';
$app_list_strings['fault_description_list'][528] = '63 ; 8x1E ; V-L3 Min1	';
$app_list_strings['fault_description_list'][529] = '64 ; 8x1F ; V-L1 Max2	';
$app_list_strings['fault_description_list'][530] = '65 ; 8x20 ; V-L2 Max2	';
$app_list_strings['fault_description_list'][531] = '66 ; 8x21 ; V-L3 Max2	';
$app_list_strings['fault_description_list'][532] = '67 ; 8x22 ; V-L1 Min2	';
$app_list_strings['fault_description_list'][533] = '68 ; 8x23 ; V-L2 Min2	';
$app_list_strings['fault_description_list'][534] = '69 ; 8x24 ; V-L3 Min2	';
$app_list_strings['fault_description_list'][535] = '70 ; 8x25 ; V-Line Max	';
$app_list_strings['fault_description_list'][536] = '71 ; 8x26 ; V-Line Min	';
$app_list_strings['fault_description_list'][537] = '72 ; 8x27 ; I-ACDC L1	';
$app_list_strings['fault_description_list'][538] = '73 ; 8x28 ; Unknown	';
$app_list_strings['fault_description_list'][539] = '74 ; 8x29 ; I-ACDC L2	';
$app_list_strings['fault_description_list'][540] = '75 ; 8x2A ; I-ACDC L3	';
$app_list_strings['fault_description_list'][541] = '76 ; 8x2B ; I-RCD 1	';
$app_list_strings['fault_description_list'][542] = '77 ; 8x2C ; I-RCD 2	';
$app_list_strings['fault_description_list'][543] = '78 ; 8x2D ; JP Sync Timeout	';
$app_list_strings['fault_description_list'][544] = '79 ; 8x2E ; F-L1 Max	';
$app_list_strings['fault_description_list'][545] = '82 ; 8x31 ; F-L1 Min	';
$app_list_strings['fault_description_list'][546] = '83 ; 8x32 ; F-L2 Min	';
$app_list_strings['fault_description_list'][547] = '84 ; 8x33 ; F-L3 Min	';
$app_list_strings['fault_description_list'][548] = '85 ; 8x34 ; VinP Max	';
$app_list_strings['fault_description_list'][549] = '86 ; 8x35 ; VinM Max	';
$app_list_strings['fault_description_list'][550] = '88 ; 8x37 ; Vin Max	';
$app_list_strings['fault_description_list'][551] = '89 ; 8x38 ; Passive anti islanding	';
$app_list_strings['fault_description_list'][552] = '90 ; 8x39 ; TZ RCD	';
$app_list_strings['fault_description_list'][553] = '91 ; 8x3A ; TZ L1	';
$app_list_strings['fault_description_list'][554] = '92 ; 8x3B ; TZ L2	';
$app_list_strings['fault_description_list'][555] = '93 ; 8x3C ; TZ L3	';
$app_list_strings['fault_description_list'][556] = '94 ; 8x3D ; TZ ITO	';
$app_list_strings['fault_description_list'][557] = '96 ; 8x3F ; Iac L1 Max	';
$app_list_strings['fault_description_list'][558] = '97 ; 8x40 ; Iac L2 Max	';
$app_list_strings['fault_description_list'][559] = '98 ; 8x41 ; Iac L3 Max	';
$app_list_strings['fault_description_list'][560] = '99 ; 8x42 ; Vsrg L1 Max (Vfilter spike)	';
$app_list_strings['fault_description_list'][561] = '99 ; 8x42 ; Vsrg L1 Max (Vfilter spike)	';
$app_list_strings['fault_description_list'][562] = '101 ; 8x44 ; Vsrg L3 Max (Vfilter spike)	';
$app_list_strings['fault_description_list'][563] = '102 ; 8x45 ; Vdc Pluse Max (spike)	';
$app_list_strings['fault_description_list'][564] = '103 ; 8x46 ; Vdc Minus Max (spike)	';
$app_list_strings['fault_description_list'][565] = '104 ; 8x47 ; Overtemp	';
$app_list_strings['fault_description_list'][566] = '105 ; 8x48 ; Undertemp	';
$app_list_strings['fault_description_list'][567] = '112 ; 8x4F ; Wrong Ac connection	';
$app_list_strings['fault_description_list'][568] = '115 ; 8x52 ; VDC Cap banks unbalanced	';
$app_list_strings['fault_description_list'][569] = '116 ; 8x53 ; DC Charged	';
$app_list_strings['fault_description_list'][570] = '118 ; 8x55 ; GRM	';
$app_list_strings['fault_description_list'][571] = '119 ; 8x56 ; INIT	';
$app_list_strings['fault_description_list'][572] = '120 ; 8x57 ; Temp Sensor	';
$app_list_strings['fault_description_list'][573] = '121 ; 8x58 ; Isolation	';
$app_list_strings['fault_description_list'][574] = '122 ; 8x59 ; Relay Test	';
$app_list_strings['fault_description_list'][575] = '123 ; 8x5A ; Ofset Gain	';
$app_list_strings['fault_description_list'][576] = '124 ; 8x5B ; RCD Test	';
$app_list_strings['fault_description_list'][577] = '125 ; 8x5C ; Switch	';
$app_list_strings['fault_description_list'][578] = '0 ; 2x0 ; Wakeup	';
$app_list_strings['fault_description_list'][579] = '1 ; 2x1 ; turn off	';
$app_list_strings['fault_description_list'][580] = '2 ; 2x2 ; Night mode	';
$app_list_strings['fault_description_list'][581] = '4 ; 2x4 ; DSP1 DSP2 comm	';
$app_list_strings['fault_description_list'][582] = '5 ; 2x5 ; DSP2 unexpected mode	';
$app_list_strings['fault_description_list'][583] = '8 ; 2x8 ; Unknown DSP2 Error	';
$app_list_strings['fault_description_list'][584] = '9 ; 2x9 ; Iout TZ	';
$app_list_strings['fault_description_list'][585] = '10 ; 2xA ; RCD TZ	';
$app_list_strings['fault_description_list'][586] = '13 ; 2xD ; Iout max	';
$app_list_strings['fault_description_list'][587] = '14 ; 2xE ; Vac surge	';
$app_list_strings['fault_description_list'][588] = '15 ; 2xF ; VDC surge	';
$app_list_strings['fault_description_list'][589] = '16 ; 2x10 ; Vref1V or Vref2V > +/-3%	';
$app_list_strings['fault_description_list'][590] = '17 ; 2x11 ; Temperature Max	';
$app_list_strings['fault_description_list'][591] = '18 ; 2x12 ; WD error	';
$app_list_strings['fault_description_list'][592] = '19 ; 2x13 ; SW error	';
$app_list_strings['fault_description_list'][593] = '20 ; 2x14 ; RCD Max	';
$app_list_strings['fault_description_list'][594] = '21 ; 2x15 ; SW error	';
$app_list_strings['fault_description_list'][595] = '22 ; 2x16 ; SW error	';
$app_list_strings['fault_description_list'][596] = '23 ; 2x17 ; VNN error	';
$app_list_strings['fault_description_list'][597] = '24 ; 2x18 ; Temprature Min	';
$app_list_strings['fault_description_list'][598] = '25 ; 2x19 ; Isolation error	';
$app_list_strings['fault_description_list'][599] = '26 ; 2x1A ; Relay test error	';
$app_list_strings['fault_description_list'][600] = '28 ; 2x1C ; RCD test error	';
$app_list_strings['fault_description_list'][601] = '29 ; 2x1D ; VLN max	';
$app_list_strings['fault_description_list'][602] = '30 ; 2x1E ; VLN min	';
$app_list_strings['fault_description_list'][603] = '31 ; 2x1F ; Vg max	';
$app_list_strings['fault_description_list'][604] = '32 ; 2x20 ; Vg min	';
$app_list_strings['fault_description_list'][605] = '33 ; 2x21 ; Vg2 max	';
$app_list_strings['fault_description_list'][606] = '34 ; 2x22 ; F max	';
$app_list_strings['fault_description_list'][607] = '35 ; 2x23 ; F min	';
$app_list_strings['fault_description_list'][608] = '36 ; 2x24 ; Iacdc	';
$app_list_strings['fault_description_list'][609] = '37 ; 2x25 ; IRCD 1	';
$app_list_strings['fault_description_list'][610] = '38 ; 2x26 ; IRCD 2	';
$app_list_strings['fault_description_list'][611] = '39 ; 2x27 ; PLL Sync error	';
$app_list_strings['fault_description_list'][612] = '40 ; 2x28 ; Islanding (DSP2)	';
$app_list_strings['fault_description_list'][613] = '41 ; 2x29 ; Vg2 min	';
$app_list_strings['fault_description_list'][614] = '42 ; 2x2A ; Unknown Error	';
$app_list_strings['fault_description_list'][615] = '44 ; 2x2C ; No country selected	';
$app_list_strings['fault_description_list'][616] = '45 ; 2x2D ; Portia - DSP1 comm error	';
$app_list_strings['fault_description_list'][617] = '46 ; 3xA ; Phase Unbalance	';
$app_list_strings['fault_description_list'][618] = '48 ; 3xF ; SW Error	';
$app_list_strings['fault_description_list'][619] = '132 ; 2x84 ; Exceeded 24H error	';
$app_list_strings['fault_description_list'][620] = '141 ; 2x8D ; Switch on	';
$app_list_strings['fault_description_list'][621] = '142 ; 2x8E ; Switch off	';
$app_list_strings['fault_description_list'][622] = '144 ; 2x90 ; Anti - islanding (DSP1)	';
$app_list_strings['fault_description_list'][623] = '145 ; 2x91 ; VDC max	';
$app_list_strings['fault_description_list'][624] = '147 ; 3x11 ; Arc Detected	';
$app_list_strings['fault_description_list'][625] = '150 ; 8x11 ; ARC_DETECTED	';
$app_list_strings['fault_description_list'][626] = '151 ; 8x12 ; ARC_PWR_DETECT	';
$app_list_strings['fault_description_list'][627] = '148 ; 2x94 ; Low power	';
$app_list_strings['fault_description_list'][628] = '149 ; 2x95 ; Power train error	';
$app_list_strings['fault_description_list'][629] = '150 ; 2x96 ; Arc-DSP1	';
$app_list_strings['fault_description_list'][630] = '151 ; 2x97 ; Arc-DSP2	';
$app_list_strings['fault_description_list'][631] = '152 ; 2x98 ; Tx test (AKA arc test error)	';
$app_list_strings['fault_description_list'][632] = '153 ; 2x99 ; Vfilter discharge	';
$app_list_strings['fault_description_list'][633] = '154 ; 2x9A ; Arc -debug error	';
$app_list_strings['fault_description_list'][634] = '156 ; 2x9C ; DRV TZ false alarm	';
$app_list_strings['fault_description_list'][635] = '176 ; 3x6A ; Inverter Remotely locked	';
$app_list_strings['fault_description_list'][636] = '178 ; 3x6D ; Internal RGM Error	';
$app_list_strings['fault_description_list'][637] = '180 ; 2xB4 ; Iout Max	';
$app_list_strings['fault_description_list'][638] = '182 ; 2xB6 ; AT over Temp	';
$app_list_strings['fault_description_list'][639] = '181 ; 2xB5 ; Relay feedback	';
$app_list_strings['fault_description_list'][640] = '183 ; 2xB7 ; Storage module communication	';
$app_list_strings['fault_description_list'][641] = '185 ; 3x6E ; Meter Comm. Error	';
$app_list_strings['fault_description_list'][642] = '186 ; 3x6B ; Battery Comm. Error	';
$app_list_strings['fault_description_list'][643] = '187 ; 2xBB ; Grid sync test	';
$app_list_strings['fault_description_list'][644] = '256 ; 2x100 ; Fan 1 is not working	';
$app_list_strings['fault_description_list'][645] = '257 ; 2x101 ; Fan 2 is not working	';
$app_list_strings['fault_description_list'][646] = 'Firmware upgrade	';
$app_list_strings['fault_description_list'][647] = 'Configuration issue	';
$app_list_strings['fault_description_list'][648] = 'Faulty device	';
$app_list_strings['fault_description_list'][649] = '11 ; 18xB ; Vac Unstable	';
$app_list_strings['fault_description_list'][650] = '12 ; 18xC ; ARC_DETECTED	';
$app_list_strings['fault_description_list'][651] = '13 ; 18xD ; ARC_PWR_DETECT	';
$app_list_strings['fault_description_list'][652] = '14 ; 18xE ; RX_TX_STARTUP_TEST	';
$app_list_strings['fault_description_list'][653] = '15 ; 18xF ; ARC_DETECT_DEBUG	';
$app_list_strings['fault_description_list'][654] = '16 ; 18x10 ; Communication	';
$app_list_strings['fault_description_list'][655] = '17 ; 18x11 ; Unexpected Mode	';
$app_list_strings['fault_description_list'][656] = '18 ; 18x12 ; Supervise	';
$app_list_strings['fault_description_list'][657] = '19 ; 18x13 ; Vin Decrease Timeout	';
$app_list_strings['fault_description_list'][658] = '20 ; 18x14 ; InvPwr Err- No Code	';
$app_list_strings['fault_description_list'][659] = '21 ; 18x15 ; Sync Timeout	';
$app_list_strings['fault_description_list'][660] = '22 ; 18x16 ; Main Cap Est	';
$app_list_strings['fault_description_list'][661] = '23 ; 18x17 ; Main Cap error	';
$app_list_strings['fault_description_list'][662] = '24 ; 18x18 ; Pwr Boot error	';
$app_list_strings['fault_description_list'][663] = '25 ; 18x19 ; V-L1 Max1	';
$app_list_strings['fault_description_list'][664] = '28 ; 18x1C ; V-L1 Min1	';
$app_list_strings['fault_description_list'][665] = '31 ; 18x1F ; V-L1 Max2	';
$app_list_strings['fault_description_list'][666] = '34 ; 18x22 ; V-L1 Min2	';
$app_list_strings['fault_description_list'][667] = '55 ; 18x37 ; V-Line Max	';
$app_list_strings['fault_description_list'][668] = '56 ; 18x38 ; V-Line Min	';
$app_list_strings['fault_description_list'][669] = '57 ; 18x39 ; I-ACDC L1	';
$app_list_strings['fault_description_list'][670] = '61 ; 18x3D ; I-RCD STEP	';
$app_list_strings['fault_description_list'][671] = '62 ; 18x3E ; I-RCD MAX	';
$app_list_strings['fault_description_list'][672] = '63 ; 18x3F ; GRM Sync	';
$app_list_strings['fault_description_list'][673] = '64 ; 18x40 ; F-L1 Max1	';
$app_list_strings['fault_description_list'][674] = '67 ; 18x43 ; F-L1 Min1	';
$app_list_strings['fault_description_list'][675] = '70 ; 18x46 ; F-L1 Max2	';
$app_list_strings['fault_description_list'][676] = '73 ; 18x49 ; F-L1 Min2	';
$app_list_strings['fault_description_list'][677] = '94 ; 18x5E ; VinP Max	';
$app_list_strings['fault_description_list'][678] = '95 ; 18x5F ; VinM Max	';
$app_list_strings['fault_description_list'][679] = '96 ; 18x60 ; Islanding Trip1	';
$app_list_strings['fault_description_list'][680] = '97 ; 18x61 ; Vin Max	';
$app_list_strings['fault_description_list'][681] = '98 ; 18x62 ; Islanding Trip2	';
$app_list_strings['fault_description_list'][682] = '99 ; 18x63 ; TZ RCD	';
$app_list_strings['fault_description_list'][683] = '100 ; 18x64 ; TZ L1	';
$app_list_strings['fault_description_list'][684] = '101 ; 18x65 ; TZ L2	';
$app_list_strings['fault_description_list'][685] = '102 ; 18x66 ; TZ L3	';
$app_list_strings['fault_description_list'][686] = '103 ; 18x67 ; TZ ITO	';
$app_list_strings['fault_description_list'][687] = '104 ; 18x68 ; TZ Misc	';
$app_list_strings['fault_description_list'][688] = '105 ; 18x69 ; Iac L1 Leg1 Max	';
$app_list_strings['fault_description_list'][689] = '108 ; 18x6C ; Iac L1 Leg2 Max	';
$app_list_strings['fault_description_list'][690] = '111 ; 18x6F ; Vsrg L1 Max	';
$app_list_strings['fault_description_list'][691] = '114 ; 18x72 ; Vdc Pluse Max	';
$app_list_strings['fault_description_list'][692] = '115 ; 18x73 ; Vdc Minus Max	';
$app_list_strings['fault_description_list'][693] = '117 ; 18x75 ; Overtemp	';
$app_list_strings['fault_description_list'][694] = '118 ; 18x76 ; Undertemp	';
$app_list_strings['fault_description_list'][695] = '120 ; 18x78 ; WDogReset	';
$app_list_strings['fault_description_list'][696] = '123 ; 18x7B ; StartupError	';
$app_list_strings['fault_description_list'][697] = '124 ; 18x7C ; ISR16khzError	';
$app_list_strings['fault_description_list'][698] = '125 ; 18x7D ; PhaseError	';
$app_list_strings['fault_description_list'][699] = '127 ; 18x7F ; IRCDMax	';
$app_list_strings['fault_description_list'][700] = '128 ; 18x80 ; VDCBalanceError	';
$app_list_strings['fault_description_list'][701] = '129 ; 18x81 ; VNN ERROR	';
$app_list_strings['fault_description_list'][702] = '131 ; 18x83 ; GRM	';
$app_list_strings['fault_description_list'][703] = '132 ; 18x84 ; Init	';
$app_list_strings['fault_description_list'][704] = '133 ; 18x85 ; Temp Sensor fault	';
$app_list_strings['fault_description_list'][705] = '134 ; 18x86 ; Isolation	';
$app_list_strings['fault_description_list'][706] = '135 ; 18x87 ; Relay Test	';
$app_list_strings['fault_description_list'][707] = '136 ; 18x88 ; Ofset Gain	';
$app_list_strings['fault_description_list'][708] = '137 ; 18x89 ; RCD Test	';
$app_list_strings['fault_description_list'][709] = '141 ; 18x8D ; ArcDetectTest	';
$app_list_strings['fault_description_list'][710] = '142 ; 18x8E ; ArcDetection	';
$app_list_strings['fault_description_list'][711] = '143 ; 18x8F ; RsrvdInit4	';
$app_list_strings['fault_description_list'][712] = '163 ; 18xA3 ; Tz Over current 1	';
$app_list_strings['fault_description_list'][713] = '166 ; 18xA6 ; Tz Over voltage cap1	';
$app_list_strings['fault_description_list'][714] = '169 ; 18xA9 ; Tz Over current Rcd	';
$app_list_strings['fault_description_list'][715] = '170 ; 18xAA ; Tz Over voltage bridge cap	';
$app_list_strings['fault_description_list'][716] = '171 ; 18xAB ; Tz Over voltage Vin	';
$app_list_strings['fault_description_list'][717] = '172 ; 18xAC ; Tz Mngr Driver Dis	';
$app_list_strings['fault_description_list'][718] = '173 ; 18xAD ; Tz Pwr Driver Dis	';
$app_list_strings['fault_description_list'][719] = '174 ; 18xAE ; Tz OnOff switch dis	';
$app_list_strings['fault_description_list'][720] = '175 ; 18xAF ; Tz RSD	';
$app_list_strings['fault_description_list'][721] = '176 ; 18xB0 ; Tz NC	';
$app_list_strings['fault_description_list'][722] = '177 ; 18xB1 ; Tz Global	';
$app_list_strings['fault_description_list'][723] = '178 ; 18xB2 ; Vf1 surge	';
$app_list_strings['fault_description_list'][724] = '181 ; 18xB5 ; Vcap11 surge	';
$app_list_strings['fault_description_list'][725] = '182 ; 18xB6 ; Vcap12 surge	';
$app_list_strings['fault_description_list'][726] = '183 ; 18xB7 ; Vcap21 surge	';
$app_list_strings['fault_description_list'][727] = '184 ; 18xB8 ; Vcap22 surge	';
$app_list_strings['fault_description_list'][728] = '185 ; 18xB9 ; Vcap31 surge	';
$app_list_strings['fault_description_list'][729] = '186 ; 18xBA ; Vcap33 surge	';
$app_list_strings['fault_description_list'][730] = '187 ; 18xBB ; Vin min	';
$app_list_strings['fault_description_list'][731] = '188 ; 18xBC ; Vcap unbalanced	';
$app_list_strings['fault_description_list'][732] = '189 ; 18xBD ; Vfilter unbalanced	';
$app_list_strings['fault_description_list'][733] = '190 ; 18xBE ; Swing Rdiff overheat	';
$app_list_strings['fault_description_list'][734] = '191 ; 18xBF ; Swing Rcommon overheat	';
$app_list_strings['fault_description_list'][735] = '192 ; 18xC0 ; Sync problem during grid connection	';
$app_list_strings['fault_description_list'][736] = '193 ; 18xC1 ; FPGA Parameter Error	';
$app_list_strings['fault_description_list'][737] = '194 ; 18xC2 ; Swing MaxVdiff Timeout	';
$app_list_strings['fault_description_list'][738] = '195 ; 18xC3 ; VgAvg-L1 Max	';
$app_list_strings['fault_description_list'][739] = '198 ; 18xC6 ; Operating time Timeout	';
$app_list_strings['fault_description_list'][740] = '199 ; 18xC7 ; Rapid Shutdown	';
$app_list_strings['fault_description_list'][741] = '200 ; 18xC8 ; GB Event	';
$app_list_strings['fault_description_list'][742] = '201 ; 18xC9 ; Big Offset during calibration	';
$app_list_strings['fault_description_list'][743] = '0 ; ERROR CODE Zero	';
$app_list_strings['fault_description_list'][744] = 'Loose terminals	';
$app_list_strings['fault_description_list'][745] = 'Broken CT	';
$app_list_strings['fault_description_list'][746] = 'Broken antenna	';
$app_list_strings['fault_description_list'][747] = 'LED issues          	';
$app_list_strings['fault_description_list'][748] = 'Short	';
$app_list_strings['fault_description_list'][749] = 'ZB Association	';
$app_list_strings['fault_description_list'][750] = 'Missing S/N	';
$app_list_strings['fault_description_list'][751] = 'Power delivering	';
$app_list_strings['fault_description_list'][752] = 'Power measurements	';
$app_list_strings['fault_description_list'][753] = 'Malfunction relay	';
$app_list_strings['fault_description_list'][754] = 'wrong configuration	';
$app_list_strings['fault_description_list'][755] = 'wrong software	';
$app_list_strings['fault_description_list'][756] = 'Damaged Device	';
$app_list_strings['fault_description_list'][757] = 'Other	';
$app_list_strings['fault_description_list'][758] = '1; DCDC Converter_Battery Over Voltage	';
$app_list_strings['fault_description_list'][759] = '2; DCDC Converter_Battery Over Current	';
$app_list_strings['fault_description_list'][760] = '3; DCDC Converter_Link Over Voltage	';
$app_list_strings['fault_description_list'][761] = '4; DCDC Converter_Link Over Current	';
$app_list_strings['fault_description_list'][762] = '5; DCDC Converter_Over Temperature	';
$app_list_strings['fault_description_list'][763] = '6; DCDC Converter_BMS_Comm_Err	';
$app_list_strings['fault_description_list'][764] = '7; DCDC Converter_INV_Comm_Err	';
$app_list_strings['fault_description_list'][765] = '9; Over Voltage Fault	';
$app_list_strings['fault_description_list'][766] = '20; Over Voltage Fault2	';
$app_list_strings['fault_description_list'][767] = '10; Under Voltage Fault	';
$app_list_strings['fault_description_list'][768] = '21; Under Voltage Fault2	';
$app_list_strings['fault_description_list'][769] = '11; Deviation Voltage Fault	';
$app_list_strings['fault_description_list'][770] = '12; Over Temperature Fault	';
$app_list_strings['fault_description_list'][771] = '22; Over Temperature Fault2	';
$app_list_strings['fault_description_list'][772] = '13; Under Temperature Fault	';
$app_list_strings['fault_description_list'][773] = '23; Under Temperature Fault2	';
$app_list_strings['fault_description_list'][774] = '14; Deviation Temperature Fault	';
$app_list_strings['fault_description_list'][775] = '15; Over Charge Current Fault	';
$app_list_strings['fault_description_list'][776] = '24; Over Charge Current Fault2	';
$app_list_strings['fault_description_list'][777] = '16; Over Discharge Current Fault	';
$app_list_strings['fault_description_list'][778] = '25; Over Discharge Current Fault2	';
$app_list_strings['fault_description_list'][779] = '17; Over Charge Power Limit Fault	';
$app_list_strings['fault_description_list'][780] = '26; Over Charge Power Limit Fault2	';
$app_list_strings['fault_description_list'][781] = '18; Over Discharge Power Limit Fault	';
$app_list_strings['fault_description_list'][782] = '27; Over Discharge Power Limit Fault2	';
$app_list_strings['fault_description_list'][783] = '19; Battery Monitoring IC Loss Of Communication Fault	';
$app_list_strings['fault_description_list'][784] = '28; PCS Loss of Communication Fault2	';
$app_list_strings['fault_description_list'][785] = '29; Battery Monitoring IC Loss Of Communication Fault2	';
$app_list_strings['digital_pcb_version_list']['H'] = 'H';
$app_list_strings['digital_pcb_version_list']['J'] = 'J';
$app_list_strings['digital_pcb_version_list']['K'] = 'K';
$app_list_strings['doa_list'][1] = 'Yes';
$app_list_strings['doa_list'][2] = 'No';
$app_list_strings['doa_type_list'][1] = 'Reliability';
$app_list_strings['doa_type_list'][2] = 'Quality';
$app_list_strings['fa_fault_list'][1] = 'Component Failure-Burnt	';
$app_list_strings['fa_fault_list'][2] = 'Component Failure-Damage	';
$app_list_strings['fa_fault_list'][3] = 'Component Failure-Open	';
$app_list_strings['fa_fault_list'][4] = 'Component Failure-Short	';
$app_list_strings['fa_fault_list'][5] = 'Component Failure-Functional	';
$app_list_strings['fa_fault_list'][6] = 'NFF';
$app_list_strings['fa_fault_list'][7] = 'Contact Issue	';
$app_list_strings['fa_fault_list'][8] = 'Missing Component	';
$app_list_strings['fa_fault_list'][9] = 'SW issue	';
$app_list_strings['fa_fault_list'][10] = 'Wrong Parameters	';
$app_list_strings['fa_fault_list'][11] = 'Design issue	';
$app_list_strings['fault_category_list'][''] = '';
$app_list_strings['fault_category_list'][1] = 'Inverter';
$app_list_strings['fault_category_list'][2] = 'Optimizer';
$app_list_strings['fault_category_list'][3] = 'Site Communication';
$app_list_strings['fault_category_list'][4] = 'None SE';
$app_list_strings['fault_category_list'][5] = 'Monitoring';
$app_list_strings['fault_category_list'][6] = 'General';
$app_list_strings['fault_category_list'][7] = 'Shipping';
$app_list_strings['fault_category_list'][8] = 'Duplication';
$app_list_strings['fault_category_list'][9] = 'No reply from the customer';
$app_list_strings['fault_category_list'][10] = 'Installer tools- SW';
$app_list_strings['fault_category_list'][11] = 'Installer tools -HW';
$app_list_strings['fault_category_list'][12] = 'SMI';
$app_list_strings['fault_category_list'][13] = 'Communication gateway';
$app_list_strings['fault_category_list'][14] = 'Fire fighters gateway';
$app_list_strings['fault_category_list'][15] = 'DCD';
$app_list_strings['fault_category_list'][16] = 'Gemini';
$app_list_strings['fault_category_list'][17] = 'StorEdge';
$app_list_strings['fault_category_list'][18] = 'Meter';
$app_list_strings['fault_category_list'][19] = 'Home Automation';
$app_list_strings['fault_sub_category_list'][1] = 'Ethernet';
$app_list_strings['fault_sub_category_list'][2] = 'Embedded GSM	';
$app_list_strings['fault_sub_category_list'][3] = 'Embedded CDMA	';
$app_list_strings['fault_sub_category_list'][4] = 'RS485';
$app_list_strings['fault_sub_category_list'][5] = 'WiFi';
$app_list_strings['fault_sub_category_list'][6] = 'CCG';
$app_list_strings['fault_sub_category_list'][7] = 'Communication';
$app_list_strings['fault_sub_category_list'][8] = 'Grid';
$app_list_strings['fault_sub_category_list'][9] = 'Installation';
$app_list_strings['fault_sub_category_list'][10] = 'Other Inverter	';
$app_list_strings['fault_sub_category_list'][11] = 'Panels';
$app_list_strings['fault_sub_category_list'][12] = 'Comm board	';
$app_list_strings['fault_sub_category_list'][13] = 'Digital';
$app_list_strings['fault_sub_category_list'][14] = 'Event/Error code 1 Phase	';
$app_list_strings['fault_sub_category_list'][15] = 'Event/Error code 3 Phase	';
$app_list_strings['fault_sub_category_list'][16] = 'Event/Error Code- HD wave	';
$app_list_strings['fault_sub_category_list'][17] = 'Power board	';
$app_list_strings['fault_sub_category_list'][18] = 'Relay board	';
$app_list_strings['fault_sub_category_list'][19] = 'Mechanical';
$app_list_strings['fault_sub_category_list'][20] = 'External Fan	';
$app_list_strings['fault_sub_category_list'][21] = 'Internal fan	';
$app_list_strings['fault_sub_category_list'][22] = 'Humming inverter	';
$app_list_strings['fault_sub_category_list'][23] = '1:1 Configuration	';
$app_list_strings['fault_sub_category_list'][24] = '2:1 Configuration	';
$app_list_strings['fault_sub_category_list'][25] = '3:1 Configuration	';
$app_list_strings['fault_sub_category_list'][26] = '4:1 Configuration	';
$app_list_strings['fault_sub_category_list'][27] = 'Add physical layout	';
$app_list_strings['fault_sub_category_list'][28] = 'Alerts';
$app_list_strings['fault_sub_category_list'][29] = 'Data presentation problem	';
$app_list_strings['fault_sub_category_list'][30] = 'Fix physical layout	';
$app_list_strings['fault_sub_category_list'][31] = 'Known bug	';
$app_list_strings['fault_sub_category_list'][32] = 'Logical layout	';
$app_list_strings['fault_sub_category_list'][33] = 'Mobile devices	';
$app_list_strings['fault_sub_category_list'][34] = 'Monitoring server is down	';
$app_list_strings['fault_sub_category_list'][35] = 'User name / Password	';
$app_list_strings['fault_sub_category_list'][36] = 'Permissions	';
$app_list_strings['fault_sub_category_list'][37] = 'Register owner without installer	';
$app_list_strings['fault_sub_category_list'][38] = 'Registration';
$app_list_strings['fault_sub_category_list'][39] = 'Site Mapper	';
$app_list_strings['fault_sub_category_list'][40] = 'Site setup without physical	';
$app_list_strings['fault_sub_category_list'][41] = 'Site setup with physical	';
$app_list_strings['fault_sub_category_list'][42] = 'Update details	';
$app_list_strings['fault_sub_category_list'][43] = 'Change account for the site	';
$app_list_strings['fault_sub_category_list'][44] = 'Missing Item	';
$app_list_strings['fault_sub_category_list'][45] = 'Advertisement	';
$app_list_strings['fault_sub_category_list'][46] = 'Damaged package	';
$app_list_strings['fault_sub_category_list'][47] = 'No fault found	';
$app_list_strings['fault_sub_category_list'][48] = 'Request for compensation	';
$app_list_strings['fault_sub_category_list'][49] = 'Sales opportunity	';
$app_list_strings['fault_sub_category_list'][50] = 'SF password/registration issue	';
$app_list_strings['fault_sub_category_list'][51] = 'Shipment details	';
$app_list_strings['fault_sub_category_list'][52] = 'Stickers';
$app_list_strings['fault_sub_category_list'][53] = 'System design assistance	';
$app_list_strings['fault_sub_category_list'][54] = 'Training request	';
$app_list_strings['fault_sub_category_list'][55] = 'Wrong Shipment/Packaging	';
$app_list_strings['fault_sub_category_list'][56] = 'Warranty';
$app_list_strings['fault_sub_category_list'][57] = 'Site designer	';
$app_list_strings['fault_sub_category_list'][58] = 'Android mapper app	';
$app_list_strings['fault_sub_category_list'][59] = 'iphone mapper app	';
$app_list_strings['fault_sub_category_list'][60] = 'Configuration tool	';
$app_list_strings['fault_sub_category_list'][61] = 'Solaredge Key	';
$app_list_strings['fault_sub_category_list'][62] = 'Event/Error code	';
$app_list_strings['fault_sub_category_list'][63] = '3rd party inverter com problem	';
$app_list_strings['fault_sub_category_list'][64] = 'Motherboard failure	';
$app_list_strings['fault_sub_category_list'][65] = 'Communication board failure	';
$app_list_strings['fault_sub_category_list'][66] = 'Pairing issue	';
$app_list_strings['fault_sub_category_list'][67] = 'Duplication	';
$app_list_strings['fault_sub_category_list'][68] = 'No reply from the customer	';
$app_list_strings['fault_sub_category_list'][69] = 'DCD A/B issue	';
$app_list_strings['fault_sub_category_list'][70] = 'Does not line up with inverter	';
$app_list_strings['fault_sub_category_list'][71] = 'workmanship failure	';
$app_list_strings['fault_sub_category_list'][72] = 'Power Failure	';
$app_list_strings['fault_sub_category_list'][73] = 'Damaged Sensore port	';
$app_list_strings['fault_sub_category_list'][74] = 'faulty CCG	';
$app_list_strings['fault_sub_category_list'][75] = 'Sensors/Meters	';
$app_list_strings['fault_sub_category_list'][76] = 'Faulty FFG	';
$app_list_strings['fault_sub_category_list'][77] = 'Event/Error code (string)	';
$app_list_strings['fault_sub_category_list'][78] = 'Event/Error code (combi)	';
$app_list_strings['fault_sub_category_list'][79] = 'Monitoring';
$app_list_strings['fault_sub_category_list'][80] = 'IEC Certification	';
$app_list_strings['fault_sub_category_list'][81] = 'Firmware';
$app_list_strings['fault_sub_category_list'][82] = 'No communication to site	';
$app_list_strings['fault_sub_category_list'][83] = 'Button Fault	';
$app_list_strings['fault_sub_category_list'][84] = 'Remote operation	';
$app_list_strings['fault_sub_category_list'][85] = 'Site config	';
$app_list_strings['fault_sub_category_list'][86] = 'Manual Activation	';
$app_list_strings['fault_sub_category_list'][87] = 'General Tech Question	';
$app_list_strings['fault_sub_category_list'][88] = 'General question on monitoring site	';
$app_list_strings['fault_sub_category_list'][89] = 'Water penetration	';
$app_list_strings['fault_sub_category_list'][90] = 'RGM	';
$app_list_strings['fault_sub_category_list'][91] = 'Maintenance	';
$app_list_strings['fault_sub_category_list'][92] = 'Continuously in wakeup loop	';
$app_list_strings['fault_sub_category_list'][93] = 'Damaged goods	';
$app_list_strings['fault_sub_category_list'][94] = 'High temperature	';
$app_list_strings['fault_sub_category_list'][95] = 'Low production	';
$app_list_strings['fault_sub_category_list'][96] = 'Burnt component	';
$app_list_strings['fault_sub_category_list'][97] = 'Faulty DCD	';
$app_list_strings['fault_sub_category_list'][98] = 'Standard Compliance	';
$app_list_strings['fault_sub_category_list'][99] = 'Requested for investigation	';
$app_list_strings['fault_sub_category_list'][100] = 'ZigBee-card	';
$app_list_strings['fault_sub_category_list'][101] = 'Zigbee-Home gateway	';
$app_list_strings['fault_sub_category_list'][102] = 'Act Of God	';
$app_list_strings['fault_sub_category_list'][103] = 'Combi';
$app_list_strings['fault_sub_category_list'][104] = 'Damaged Inverter	';
$app_list_strings['fault_sub_category_list'][105] = 'Damaged Item	';
$app_list_strings['fault_sub_category_list'][106] = 'Browser issue	';
$app_list_strings['fault_sub_category_list'][107] = 'Other';
$app_list_strings['fault_sub_category_list'][108] = 'Battery error code- LG	';
$app_list_strings['fault_sub_category_list'][109] = 'Battery Error Code- Tesla	';
$app_list_strings['fault_sub_category_list'][110] = 'Interface	';
$app_list_strings['fault_sub_category_list'][111] = 'StorEdge Mechanical	';
$app_list_strings['fault_sub_category_list'][112] = 'Backup';
$app_list_strings['fault_sub_category_list'][113] = 'StorEdge Communication	';
$app_list_strings['fault_sub_category_list'][114] = 'Configuration';
$app_list_strings['fault_sub_category_list'][115] = 'StorEdge Installation	';
$app_list_strings['fault_sub_category_list'][116] = 'Meter';
$app_list_strings['fault_sub_category_list'][117] = 'General Questions	';
$app_list_strings['fault_sub_category_list'][118] = 'Zigbee manager	';
$app_list_strings['fault_sub_category_list'][119] = 'Immersion heater controller	';
$app_list_strings['fault_sub_category_list'][120] = 'Plug';
$app_list_strings['fault_sub_category_list'][121] = 'AC switch	';
$app_list_strings['fault_sub_category_list'][122] = 'AC dry contact	';
$app_list_strings['fault_sub_category_list'][123] = 'AC dry contact	';
$app_list_strings['inv_assy_ac_dc_conduit_list']['Short'] = 'Short';
$app_list_strings['inv_assy_heat_sink_list'][1] = 'Type 1	';
$app_list_strings['manufacturing_related_fault_list'][1] = 'Soldering- Open	';
$app_list_strings['manufacturing_related_fault_list'][2] = 'Soldering-Short	';
$app_list_strings['manufacturing_related_fault_list'][3] = 'Contamination	';
$app_list_strings['manufacturing_related_fault_list'][4] = 'Loose Part	';
$app_list_strings['m40_list']['Yes'] = 'Yes';
$app_list_strings['m40_0']['Yes'] = 'Yes';
$app_list_strings['m40_0']['No'] = 'No';
$app_list_strings['model_list'][1] = '1 phase	';
$app_list_strings['model_list'][2] = '3 phase	';
$app_list_strings['model_list'][3] = 'Add-on optimizer	';
$app_list_strings['model_list'][4] = 'Embedded optimizer	';
$app_list_strings['model_list'][5] = 'SMI 180	';
$app_list_strings['model_list'][6] = 'SMI 35	';
$app_list_strings['portia_memory_list']['Low'] = 'Low';
$app_list_strings['portia_memory_list']['High'] = 'High';
$app_list_strings['replaced_type_list'][1] = 'SE field engineer	';
$app_list_strings['replaced_type_list'][2] = 'From customer stock	';
$app_list_strings['replaced_type_list'][3] = 'Without stock	';
$app_list_strings['failure_type_list'][1] = 'Communication board Single phase Ven1	';
$app_list_strings['failure_type_list'][2] = 'Communication board Single phase Ven2	';
$app_list_strings['failure_type_list'][3] = 'Communication board 3 Phase	';
$app_list_strings['failure_type_list'][4] = 'Digital board Single phase Ven 1	';
$app_list_strings['failure_type_list'][5] = 'Digital board Single phase Ven 2	';
$app_list_strings['failure_type_list'][6] = 'Digital board 3 Phase	';
$app_list_strings['failure_type_list'][7] = 'Relay board 3 phase	';
$app_list_strings['failure_type_list'][8] = 'Fan unit external	';
$app_list_strings['failure_type_list'][9] = 'Fan unit internal	';
$app_list_strings['failure_type_list'][10] = 'Inverter';
$app_list_strings['failure_type_list'][11] = 'Optimizer	';
$app_list_strings['failure_type_list'][12] = 'On / Off switch	';
$app_list_strings['failure_type_list'][13] = 'Display switch	';
$app_list_strings['failure_type_list'][14] = 'GSM';
$app_list_strings['failure_type_list'][15] = 'Zigbee X2	';
$app_list_strings['failure_type_list'][16] = 'Zigbee Master	';
$app_list_strings['failure_type_list'][17] = 'Zigbee Slave	';
$app_list_strings['failure_type_list'][18] = 'DCD';
$app_list_strings['failure_type_list'][19] = 'SMI';
$app_list_strings['failure_type_list'][20] = 'Bracket Single phase	';
$app_list_strings['failure_type_list'][21] = 'Bracket 3 phase	';
$app_list_strings['failure_type_list'][22] = 'MC4 Connector Male	';
$app_list_strings['failure_type_list'][23] = 'MC4 Connector Female	';
$app_list_strings['failure_type_list'][24] = 'Battery for Communication board	';
$app_list_strings['failure_type_list'][25] = 'SD card	';
$app_list_strings['failure_type_list'][26] = 'Terminal block	';
$app_list_strings['failure_type_list'][27] = 'CDMA';
$app_list_strings['failure_type_list'][28] = 'Gateway kit	';
$app_list_strings['failure_type_list'][29] = 'Venus + digital board	';
$app_list_strings['return_asked_by_list'][1] = 'Amit';
$app_list_strings['return_asked_by_list'][2] = 'Bahat';
$app_list_strings['return_asked_by_list'][3] = 'Igor';
$app_list_strings['return_asked_by_list'][4] = 'Liron';
$app_list_strings['return_asked_by_list'][5] = 'Michael	';
$app_list_strings['return_asked_by_list'][6] = 'Nadav';
$app_list_strings['return_asked_by_list'][7] = 'Nethanel';
$app_list_strings['return_asked_by_list'][8] = 'Ofer';
$app_list_strings['return_asked_by_list'][9] = 'Tier2– APAC	';
$app_list_strings['return_asked_by_list'][10] = 'Tier2– EU	';
$app_list_strings['return_asked_by_list'][11] = 'Tier2– NA	';
$app_list_strings['return_asked_by_list'][12] = 'Tomer';
$app_list_strings['return_asked_by_list'][13] = 'Vitaly';
$app_list_strings['return_asked_by_list'][14] = 'Yoav';
$app_list_strings['return_asked_by_list'][15] = 'Auto FA Table	';
$app_list_strings['rma_product_list']['Optimizer'] = 'Optimizer';
$app_list_strings['rma_product_list']['Inverter'] = 'Inverter';
$app_list_strings['rma_product_list']['Accessories'] = 'Accessories';
$app_list_strings['rma_product_list']['Portia'] = 'Portia';

?>