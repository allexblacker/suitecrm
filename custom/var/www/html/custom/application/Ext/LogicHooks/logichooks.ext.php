<?php 
 //WARNING: The contents of this file are auto-generated



if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(50, '', 'custom/include/dtbc/hooks/50_groupnotifier.php','groupnotifier_class', 'groupnotifier_function'); 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(100, '', 'custom/include/dtbc/hooks/100_calculatepoints.php','calculatepoints_class', 'calculatepoints_function'); 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(90, '', 'custom/include/dtbc/hooks/90_formulafields.php','formulafields_class', 'formulafields_function');

/**
 * Advanced OpenWorkflow, Automating SugarCRM.
 * @package Advanced OpenWorkflow for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(99, 'CustomAOW_WorkFlow', 'custom/modules/AOW_WorkFlow/AOW_WorkFlow.php','CustomAOW_WorkFlow', 'run_bean_flows');


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['process_record']) || !is_array($hook_array['process_record'])) {
    $hook_array['process_record'] = array();
}
$hook_array['process_record'][] = Array(90, '', 'custom/include/dtbc/hooks/securitygroups.php','GlobalProcessRecordHooks', 'displaySecurityGroupOnListView');


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_entry_point']) || !is_array($hook_array['after_entry_point'])) {
    $hook_array['after_entry_point'] = array();
}
$hook_array['after_entry_point'][] = Array(50, '', 'custom/include/dtbc/hooks/50_loginlogger.php','loginlogger_class', 'loginlogger_function'); 


if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(500, '', 'modules/dtbc_Approvals/logic_hook.php', 'ApprovalsHook', 'after_save'); 
?>