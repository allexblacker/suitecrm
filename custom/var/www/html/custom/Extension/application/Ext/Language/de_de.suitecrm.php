<?php

$app_list_strings['collection_temp_list']['Calls'] = 'Anrufe';
$app_list_strings['collection_temp_list']['Emails'] = 'E-Mails';
$app_list_strings['collection_temp_list']['Meetings'] = 'Meetings';
$app_list_strings['collection_temp_list']['Notes'] = 'Notizen';
$app_list_strings['collection_temp_list']['Tasks'] = 'Aufgaben';
$app_strings['LBL_ADD_DASHBOARD_PAGE'] = 'Übersichtsseite hinzufügen';
$app_strings['LBL_ADD_TAB'] = 'Registerkarte hinzufügen';
$app_strings['LBL_COLLECTION_TYPE'] = 'Typ';
$app_strings['LBL_DELETE_DASHBOARD1'] = 'Wollen Sie dies wirklich löschen?';
$app_strings['LBL_DELETE_DASHBOARD2'] = 'Übersicht?';
$app_strings['LBL_DELETE_DASHBOARD_PAGE'] = 'Aktuelle Übersichtsseite entfernen';
$app_strings['LBL_DISCOVER_SUITECRM'] = 'Entdecken Sie SuiteCRM';
$app_strings['LBL_ENTER_DASHBOARD_NAME'] = 'Übersichtsnamen eingeben:';
$app_strings['LBL_NUMBER_OF_COLUMNS'] = 'Anzahl Spalten:';
$app_strings['LBL_QUICK_ACCOUNT'] = 'Firma erstellen';
$app_strings['LBL_QUICK_CALL'] = 'Anruf Log';
$app_strings['LBL_QUICK_CONTACT'] = 'Kontakt erstellen';
$app_strings['LBL_QUICK_DOCUMENT'] = 'Dokument erstellen';
$app_strings['LBL_QUICK_LEAD'] = 'Interessent erstellen';
$app_strings['LBL_QUICK_OPPORTUNITY'] = 'Verkaufschance erstellen';
$app_strings['LBL_QUICK_TASK'] = 'Aufgabe erstellen';
$app_strings['LBL_RENAME_DASHBOARD_PAGE'] = 'Übersichtsseite umbenennen';
$app_strings['LBL_SUITE_DASHBOARD'] = 'Suite Übersicht';
