<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['IS1_Inside_Sales'] = 'Inside Sales';
$app_list_strings['call_c_list'][1] = '	Conversation';
$app_list_strings['call_c_list'][2] = 'Voice Message';
$app_list_strings['call_c_list'][3] = 'No Contact';
$app_list_strings['call_c_list'][4] = 'TRAINING-WEB BASED';
$app_list_strings['call_c_list'][5] = 'HOMEOWNER';
$app_list_strings['call_c_list'][6] = 'Connected';
$app_list_strings['call_c_list'][7] = 'INBOUND CONVERSATION';
$app_list_strings['call_to_action_list'][1] = 'APPT-DISTI';
$app_list_strings['call_to_action_list'][2] = 'APPT-INSIDE';
$app_list_strings['call_to_action_list'][3] = 'APPT-OUTSIDE';
$app_list_strings['call_to_action_list'][4] = 'APPT-PENDING';
$app_list_strings['call_to_action_list'][5] = 'N/A';
$app_list_strings['call_to_action_list'][6] = 'PURCH-CC';
$app_list_strings['call_to_action_list'][7] = 'PURCH-TIYLI';
$app_list_strings['call_to_action_list'][8] = 'TOOL-SITE DESIGNER';
$app_list_strings['call_to_action_list'][9] = 'TRAIN-FIELD TRAIN';
$app_list_strings['call_to_action_list'][10] = 'TRAIN-MONITOR SETUP';
$app_list_strings['call_to_action_list'][11] = 'TRAIN-PAPER';
$app_list_strings['call_to_action_list'][12] = 'TRAIN-PRODUCT';
$app_list_strings['call_to_action_list'][13] = 'TRAIN-ROADSHOW';
$app_list_strings['call_to_action_list'][14] = 'TRAIN-SITE DESIGNER';
$app_list_strings['call_to_action_list'][15] = 'TRAIN-SITE MAPPER';
$app_list_strings['call_to_action_list'][16] = 'TRAIN-WEBINAR';
$app_list_strings['customer_funnel_0'][1] = 'Discovered	';
$app_list_strings['customer_funnel_0'][2] = 'Educated';
$app_list_strings['customer_funnel_0'][3] = 'Designed';
$app_list_strings['customer_funnel_0'][4] = 'Quoted';
$app_list_strings['customer_funnel_0'][5] = 'Active';
$app_list_strings['customer_funnel_0'][6] = 'Lost';
$app_list_strings['decision_maker_list'][1] = 'Yes';
$app_list_strings['decision_maker_list'][2] = 'No';
$app_list_strings['decision_maker_list'][3] = 'N/A';
$app_list_strings['decision_maker_list'][4] = 'DISTI';
$app_list_strings['role_list'][1] = 'Management/Owner';
$app_list_strings['role_list'][2] = 'Design/Engineer';
$app_list_strings['role_list'][3] = 'Install/O+M/Purchase';
$app_list_strings['role_list'][4] = 'Sales';
$app_list_strings['role_list'][5] = 'N/A';
$app_list_strings['role_list'][6] = 'GATEKEEPER';
$app_list_strings['type_0'][1] = 'LEAD-NEW	';
$app_list_strings['type_0'][2] = 'LEAD-LOST	';
$app_list_strings['type_0'][3] = 'ROADSHOW	';
$app_list_strings['type_0'][4] = 'ONBOARD	';
$app_list_strings['type_0'][5] = 'RETAIN AND GROW	';
$app_list_strings['type_0'][6] = 'HOMEOWNER';
$app_list_strings['type_0'][7] = 'DISTI';
