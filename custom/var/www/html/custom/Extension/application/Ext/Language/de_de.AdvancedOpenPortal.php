<?php

$app_list_strings['case_state_default_key'] = 'Offen';
$app_list_strings['case_state_dom']['Closed'] = 'Abgeschlossen';
$app_list_strings['case_state_dom']['Open'] = 'Offen';
$app_list_strings['case_status_default_key'] = 'Offen_Neu';
$app_list_strings['case_status_dom']['Closed_Closed'] = 'Abgeschlossen';
$app_list_strings['case_status_dom']['Closed_Duplicate'] = 'Duplizieren';
$app_list_strings['case_status_dom']['Closed_Rejected'] = 'Abgelehnt';
$app_list_strings['case_status_dom']['Open_Assigned'] = 'Zugewiesen';
$app_list_strings['case_status_dom']['Open_New'] = 'Neue';
$app_list_strings['case_status_dom']['Open_Pending Input'] = 'Rückmeldung ausstehend';
$app_list_strings['contact_portal_user_type_dom']['Account'] = 'Firmenbenutzer';
$app_list_strings['contact_portal_user_type_dom']['Single'] = 'Einzelner Benutzer';
$app_list_strings['dom_email_distribution_for_auto_create']['AOPDefault'] = 'Verwende AOP Vorgabewerte';
$app_list_strings['dom_email_distribution_for_auto_create']['leastBusy'] = 'Am wenigsten ausgelasteten';
$app_list_strings['dom_email_distribution_for_auto_create']['random'] = 'Zufällige';
$app_list_strings['dom_email_distribution_for_auto_create']['roundRobin'] = 'Round-Robin';
$app_list_strings['dom_email_distribution_for_auto_create']['singleUser'] = 'Einzelne Benutzer';
$app_list_strings['moduleList']['AOP_AOP_Case_Events'] = 'Ticket Ereignisse';
$app_list_strings['moduleList']['AOP_AOP_Case_Updates'] = 'Ticket Updates';
$app_list_strings['moduleList']['AOP_Case_Events'] = 'Ticket Ereignisse';
$app_list_strings['moduleList']['AOP_Case_Updates'] = 'Ticket Updates';
$app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'] = '========== Bitte über dieser Linie antworten ==========';
