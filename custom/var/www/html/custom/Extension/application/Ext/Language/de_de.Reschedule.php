<?php

$app_list_strings['call_reschedule_dom'][''] = '';
$app_list_strings['call_reschedule_dom']['In a Meeting'] = 'In einem Meeting';
$app_list_strings['call_reschedule_dom']['Out of Office'] = 'Außerhalb des Büros';
$app_strings['LBL_RESCHEDULE_COUNT'] = 'Rufen Sie Versuche';
$app_strings['LBL_RESCHEDULE_DATE'] = 'Datum:';
$app_strings['LBL_RESCHEDULE_ERROR1'] = 'Bitte ein gültiges Datum auswählen';
$app_strings['LBL_RESCHEDULE_ERROR2'] = 'Bitte einen Grund auswählen';
$app_strings['LBL_RESCHEDULE_HISTORY'] = 'Verlauf Anrufversuche';
$app_strings['LBL_RESCHEDULE_LABEL'] = 'Wiedervorlage';
$app_strings['LBL_RESCHEDULE_PANEL'] = 'Wiedervorlage';
$app_strings['LBL_RESCHEDULE_REASON'] = 'Grund:';
$app_strings['LBL_RESCHEDULE_TITLE'] = 'Bitte geben Sie die Wiedervorlage Information ein';
