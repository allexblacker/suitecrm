<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */



$app_list_strings['moduleList']['P1_Project'] = 'Project';
$app_list_strings['fse_picklist_list']['AB'] = 'Andre Basler	';
$app_list_strings['fse_picklist_list']['BH'] = 'Brandon Hanson	';
$app_list_strings['fse_picklist_list']['BJ'] = 'Bryan Johnson	';
$app_list_strings['fse_picklist_list']['CC'] = 'Chris Carrera	';
$app_list_strings['fse_picklist_list']['EDLT'] = 'Eddie De La Torre	';
$app_list_strings['fse_picklist_list']['EL'] = 'Eric Larson	';
$app_list_strings['fse_picklist_list']['FR'] = 'Francisco Reyes	';
$app_list_strings['fse_picklist_list']['JD'] = 'Joe Diette	';
$app_list_strings['fse_picklist_list']['KR'] = 'Kristopher Roscoe	';
$app_list_strings['fse_picklist_list']['SH'] = 'Sara Hambleton	';
$app_list_strings['fse_picklist_list']['TJ'] = 'Travis Jones	';
$app_list_strings['fse_picklist_list']['AS'] = 'Adam Schilling	';
$app_list_strings['fse_picklist_list']['CK'] = 'Cliff Kalinowski	';
$app_list_strings['fse_picklist_list']['TB'] = 'Tripp Bannister	';
$app_list_strings['fse_picklist_list']['YP'] = 'Yaron Pony	';
$app_list_strings['fse_picklist_list']['AR'] = 'Adi Raziel	';
$app_list_strings['fse_email_list'][1] = 'andre.b@solaredge.com	';
$app_list_strings['fse_email_list'][2] = 'Brandon.Hanson@solaredge.com	';
$app_list_strings['fse_email_list'][3] = 'bryan.j@solaredge.com	';
$app_list_strings['fse_email_list'][4] = 'Chris.Carrera@solaredge.com	';
$app_list_strings['fse_email_list'][5] = 'Eddie.DeLaTorre@solaredge.com	';
$app_list_strings['fse_email_list'][6] = 'Eric.Larson@solaredge.com	';
$app_list_strings['fse_email_list'][7] = 'Francisco.Reyes@solaredge.com	';
$app_list_strings['fse_email_list'][8] = 'joe.d@solaredge.com	';
$app_list_strings['fse_email_list'][9] = 'kristopher.r@solaredge.com	';
$app_list_strings['fse_email_list'][10] = 'Sara.Hambleton@solaredge.com	';
$app_list_strings['fse_email_list'][11] = 'Travis.Jones@solaredge.com	';
$app_list_strings['fse_email_list'][12] = 'Adam.Schilling@solaredge.com	';
$app_list_strings['fse_email_list'][13] = 'Cliff.Kalinowski@solaredge.com	';
$app_list_strings['fse_email_list'][14] = 'Tripp.Bannister@solaredge.com	';
$app_list_strings['fse_email_list'][15] = 'Yaron.Pony@solaredge.com	';
$app_list_strings['fse_email_list'][16] = 'Adi.raziel@solaredge.com	';
$app_list_strings['project_status_list']['PreConstruction'] = 'Pre-Construction	';
$app_list_strings['project_status_list']['Construction'] = 'Construction';
$app_list_strings['project_status_list']['SiteEvaluation'] = 'Site Evaluation';
$app_list_strings['project_status_list']['Complete'] = 'Complete';
$app_list_strings['project_status_list']['Abandoned'] = 'Abandoned';
$app_list_strings['project_status_list']['tba'] = 'To be Assigned	';
$app_list_strings['country_0'][1] = 'United States';
$app_list_strings['country_0'][2] = 'Afghanistan';
$app_list_strings['country_0'][3] = 'Albania';
$app_list_strings['country_0'][4] = 'Algeria';
$app_list_strings['country_0'][5] = 'American Samoa';
$app_list_strings['country_0'][6] = 'Andorra';
$app_list_strings['country_0'][7] = 'Angola';
$app_list_strings['country_0'][8] = 'Anguilla';
$app_list_strings['country_0'][9] = 'Antigua';
$app_list_strings['country_0'][10] = 'Argentina';
$app_list_strings['country_0'][11] = 'Armenia';
$app_list_strings['country_0'][12] = 'Aruba';
$app_list_strings['country_0'][13] = 'Australia';
$app_list_strings['country_0'][14] = 'Austria';
$app_list_strings['country_0'][15] = 'Azerbaijan';
$app_list_strings['country_0'][16] = 'Bahamas';
$app_list_strings['country_0'][17] = 'Bahrain';
$app_list_strings['country_0'][18] = 'Bangladesh';
$app_list_strings['country_0'][19] = 'Barbados';
$app_list_strings['country_0'][20] = 'Barbuda';
$app_list_strings['country_0'][21] = 'Belarus';
$app_list_strings['country_0'][22] = 'Belgium';
$app_list_strings['country_0'][23] = 'Belize';
$app_list_strings['country_0'][24] = 'Benin';
$app_list_strings['country_0'][25] = 'Bermuda';
$app_list_strings['country_0'][26] = 'Bhutan';
$app_list_strings['country_0'][27] = 'Bolivia';
$app_list_strings['country_0'][28] = 'Bonaire';
$app_list_strings['country_0'][29] = 'Bosnia-Herzegovina';
$app_list_strings['country_0'][30] = 'Botswana';
$app_list_strings['country_0'][31] = 'Brazil';
$app_list_strings['country_0'][32] = 'British Virgin Island';
$app_list_strings['country_0'][33] = 'Brunei';
$app_list_strings['country_0'][34] = 'Bulgaria';
$app_list_strings['country_0'][35] = 'Burkina Faso';
$app_list_strings['country_0'][36] = 'Burundi';
$app_list_strings['country_0'][37] = 'Cambodia';
$app_list_strings['country_0'][38] = 'Cameroon';
$app_list_strings['country_0'][39] = 'Canada';
$app_list_strings['country_0'][40] = 'Cape Verde';
$app_list_strings['country_0'][41] = 'Cayman Island';
$app_list_strings['country_0'][42] = 'Central African Republic';
$app_list_strings['country_0'][43] = 'Chad';
$app_list_strings['country_0'][44] = 'Channel Islands';
$app_list_strings['country_0'][45] = 'Chile';
$app_list_strings['country_0'][46] = 'China';
$app_list_strings['country_0'][47] = 'Colombia';
$app_list_strings['country_0'][48] = 'Comoros';
$app_list_strings['country_0'][49] = 'Congo';
$app_list_strings['country_0'][50] = 'Congo - Brazzaville';
$app_list_strings['country_0'][51] = 'Costa Rica';
$app_list_strings['country_0'][52] = 'Côte d';
$app_list_strings['country_0'][53] = 'Croatia';
$app_list_strings['country_0'][54] = 'Cuba';
$app_list_strings['country_0'][55] = 'Cyprus';
$app_list_strings['country_0'][56] = 'Czech Republic';
$app_list_strings['country_0'][57] = 'Denmark';
$app_list_strings['country_0'][58] = 'Djibouti';
$app_list_strings['country_0'][59] = 'Dominica';
$app_list_strings['country_0'][60] = 'Dominican Republic';
$app_list_strings['country_0'][61] = 'Ecuador';
$app_list_strings['country_0'][62] = 'Egypt';
$app_list_strings['country_0'][63] = 'El Salvador';
$app_list_strings['country_0'][64] = 'Equatorial Guinea';
$app_list_strings['country_0'][65] = 'Eritrea';
$app_list_strings['country_0'][66] = 'Estonia';
$app_list_strings['country_0'][67] = 'Ethiopia';
$app_list_strings['country_0'][68] = 'Fiji';
$app_list_strings['country_0'][69] = 'Finland';
$app_list_strings['country_0'][70] = 'France';
$app_list_strings['country_0'][71] = 'French Guinea';
$app_list_strings['country_0'][72] = 'Gabon';
$app_list_strings['country_0'][73] = 'Gambia';
$app_list_strings['country_0'][74] = 'Georgia';
$app_list_strings['country_0'][75] = 'Germany';
$app_list_strings['country_0'][76] = 'Ghana';
$app_list_strings['country_0'][77] = 'Gibraltar';
$app_list_strings['country_0'][78] = 'Greece';
$app_list_strings['country_0'][80] = 'Greenland';
$app_list_strings['country_0'][81] = 'Grenada';
$app_list_strings['country_0'][82] = 'Guatemala';
$app_list_strings['country_0'][83] = 'Guinea';
$app_list_strings['country_0'][84] = 'Guyana';
$app_list_strings['country_0'][85] = 'Haiti';
$app_list_strings['country_0'][86] = 'Honduras';
$app_list_strings['country_0'][87] = 'Hong Kong';
$app_list_strings['country_0'][88] = 'Hungary';
$app_list_strings['country_0'][89] = 'Iceland';
$app_list_strings['country_0'][90] = 'Iceland';
$app_list_strings['country_0'][91] = 'India';
$app_list_strings['country_0'][92] = 'Indonesia';
$app_list_strings['country_0'][93] = 'Iran';
$app_list_strings['country_0'][94] = 'Iraq';
$app_list_strings['country_0'][95] = 'Ireland';
$app_list_strings['country_0'][96] = 'Israel';
$app_list_strings['country_0'][97] = 'Italy';
$app_list_strings['country_0'][98] = 'Ivory Coast';
$app_list_strings['country_0'][99] = 'Jamaica';
$app_list_strings['country_0'][100] = 'Japan';
$app_list_strings['country_0'][101] = 'Jordan';
$app_list_strings['country_0'][102] = 'Kazakhstan';
$app_list_strings['country_0'][103] = 'Kenya';
$app_list_strings['country_0'][104] = 'Kuwait';
$app_list_strings['country_0'][105] = 'Kyrgyzstan';
$app_list_strings['country_0'][106] = 'Laos';
$app_list_strings['country_0'][107] = 'Latvia';
$app_list_strings['country_0'][108] = 'Lebanon';
$app_list_strings['country_0'][109] = 'Lesotho';
$app_list_strings['country_0'][110] = 'Liberia';
$app_list_strings['country_0'][111] = 'Libya';
$app_list_strings['country_0'][112] = 'Liechtenstein';
$app_list_strings['country_0'][113] = 'Lithuania';
$app_list_strings['country_0'][114] = 'Luxembourg';
$app_list_strings['country_0'][115] = 'Macedonia';
$app_list_strings['country_0'][116] = 'Madagascar';
$app_list_strings['country_0'][117] = 'Malawi';
$app_list_strings['country_0'][118] = 'Malaysia';
$app_list_strings['country_0'][119] = 'Maldives';
$app_list_strings['country_0'][120] = 'Mali';
$app_list_strings['country_0'][121] = 'Malta';
$app_list_strings['country_0'][122] = 'Marshall Islands';
$app_list_strings['country_0'][123] = 'Martinique';
$app_list_strings['country_0'][124] = 'Mauritania';
$app_list_strings['country_0'][125] = 'Mauritius';
$app_list_strings['country_0'][126] = 'Mexico';
$app_list_strings['country_0'][127] = 'Micronesia';
$app_list_strings['country_0'][128] = 'Moldova';
$app_list_strings['country_0'][129] = 'Monaco';
$app_list_strings['country_0'][130] = 'Mongolia';
$app_list_strings['country_0'][131] = 'Montenegro';
$app_list_strings['country_0'][132] = 'Monsterrat';
$app_list_strings['country_0'][133] = 'Morocco';
$app_list_strings['country_0'][134] = 'Mozambique';
$app_list_strings['country_0'][135] = 'Myanmar/Burma';
$app_list_strings['country_0'][136] = 'Namibia';
$app_list_strings['country_0'][137] = 'Nauru';
$app_list_strings['country_0'][138] = 'Nepal';
$app_list_strings['country_0'][139] = 'Netherlands';
$app_list_strings['country_0'][140] = 'New Zealand';
$app_list_strings['country_0'][141] = 'Nicaragua';
$app_list_strings['country_0'][142] = 'Niger';
$app_list_strings['country_0'][143] = 'Nigeria';
$app_list_strings['country_0'][144] = 'North Korea';
$app_list_strings['country_0'][145] = 'Norway';
$app_list_strings['country_0'][146] = 'Oman';
$app_list_strings['country_0'][147] = 'Pakistan';
$app_list_strings['country_0'][148] = 'Palau';
$app_list_strings['country_0'][149] = 'Panama';
$app_list_strings['country_0'][150] = 'Papua New Guinea';
$app_list_strings['country_0'][151] = 'Paraguay';
$app_list_strings['country_0'][152] = 'Peru';
$app_list_strings['country_0'][153] = 'Philippines';
$app_list_strings['country_0'][154] = 'Poland';
$app_list_strings['country_0'][155] = 'Portugal';
$app_list_strings['country_0'][156] = 'Puerto Rico';
$app_list_strings['country_0'][157] = 'Qatar';
$app_list_strings['country_0'][158] = 'Réunion';
$app_list_strings['country_0'][159] = 'Romania';
$app_list_strings['country_0'][160] = 'Russia';
$app_list_strings['country_0'][161] = 'Rwanda';
$app_list_strings['country_0'][162] = 'San Marino';
$app_list_strings['country_0'][163] = 'Saudi Arabia';
$app_list_strings['country_0'][164] = 'Senegal';
$app_list_strings['country_0'][165] = 'Serbia';
$app_list_strings['country_0'][166] = 'Seychelles';
$app_list_strings['country_0'][167] = 'Sierra Leone';
$app_list_strings['country_0'][168] = 'Singapore';
$app_list_strings['country_0'][169] = 'Slovak Republic';
$app_list_strings['country_0'][170] = 'Slovenia';
$app_list_strings['country_0'][171] = 'Solomon Islands';
$app_list_strings['country_0'][172] = 'Somalia';
$app_list_strings['country_0'][173] = 'South Africa';
$app_list_strings['country_0'][174] = 'South Korea';
$app_list_strings['country_0'][175] = 'Spain';
$app_list_strings['country_0'][176] = 'Sri Lanka';
$app_list_strings['country_0'][177] = 'St.Kittis and Nevis';
$app_list_strings['country_0'][178] = 'St.Lucia';
$app_list_strings['country_0'][179] = 'St. Maarten/St. Martin';
$app_list_strings['country_0'][180] = 'St.Vincent';
$app_list_strings['country_0'][181] = 'Sudan';
$app_list_strings['country_0'][182] = 'Suriname';
$app_list_strings['country_0'][183] = 'Swaziland';
$app_list_strings['country_0'][184] = 'Sweden';
$app_list_strings['country_0'][185] = 'Switzerland';
$app_list_strings['country_0'][186] = 'Syria';
$app_list_strings['country_0'][187] = 'Taiwan';
$app_list_strings['country_0'][188] = 'Tajikistan';
$app_list_strings['country_0'][189] = 'Tanzania';
$app_list_strings['country_0'][190] = 'Thailand';
$app_list_strings['country_0'][191] = 'Togo';
$app_list_strings['country_0'][192] = 'Tonga';
$app_list_strings['country_0'][193] = 'Tortola';
$app_list_strings['country_0'][194] = 'Trinidad and Tobago';
$app_list_strings['country_0'][195] = 'Tunisia';
$app_list_strings['country_0'][196] = 'Turkey';
$app_list_strings['country_0'][197] = 'Turkmenistan';
$app_list_strings['country_0'][198] = 'Tuvalu';
$app_list_strings['country_0'][199] = 'Uganda';
$app_list_strings['country_0'][200] = 'Ukraine';
$app_list_strings['country_0'][201] = 'United Arab Emirates';
$app_list_strings['country_0'][202] = 'United Kingdom';
$app_list_strings['country_0'][203] = 'Uruguay';
$app_list_strings['country_0'][204] = 'Uzbekistan';
$app_list_strings['country_0'][205] = 'Vanuatu';
$app_list_strings['country_0'][206] = 'Vatican City';
$app_list_strings['country_0'][207] = 'Venezuela';
$app_list_strings['country_0'][208] = 'Vietnam';
$app_list_strings['country_0'][209] = 'Western Sahara';
$app_list_strings['country_0'][210] = 'Yemen';
$app_list_strings['country_0'][211] = 'Zambia';
$app_list_strings['state_0'][1] = 'Alabama';
$app_list_strings['state_0'][2] = 'Alaska';
$app_list_strings['state_0'][3] = 'Arizona';
$app_list_strings['state_0'][4] = 'Arkansas';
$app_list_strings['state_0'][5] = 'California';
$app_list_strings['state_0'][6] = 'Colorado';
$app_list_strings['state_0'][7] = 'Connecticut';
$app_list_strings['state_0'][8] = 'Delaware';
$app_list_strings['state_0'][9] = 'Florida';
$app_list_strings['state_0'][10] = 'Georgia';
$app_list_strings['state_0'][11] = 'Hawaii';
$app_list_strings['state_0'][12] = 'Idaho';
$app_list_strings['state_0'][13] = 'Illionois';
$app_list_strings['state_0'][14] = 'Indiana';
$app_list_strings['state_0'][15] = 'Iowa';
$app_list_strings['state_0'][16] = 'Kansas';
$app_list_strings['state_0'][17] = 'Kentucky';
$app_list_strings['state_0'][18] = 'Louisiana';
$app_list_strings['state_0'][19] = 'Maine';
$app_list_strings['state_0'][20] = 'Maryland';
$app_list_strings['state_0'][21] = 'Massachusetts';
$app_list_strings['state_0'][22] = 'Michigan';
$app_list_strings['state_0'][23] = 'Minnesota';
$app_list_strings['state_0'][24] = 'Mississippi';
$app_list_strings['state_0'][25] = 'Missouri';
$app_list_strings['state_0'][26] = 'Montana';
$app_list_strings['state_0'][27] = 'Nebraska';
$app_list_strings['state_0'][28] = 'Nevada';
$app_list_strings['state_0'][29] = 'New Hampshire';
$app_list_strings['state_0'][30] = 'New Jersey';
$app_list_strings['state_0'][31] = 'New Mexico';
$app_list_strings['state_0'][32] = 'New York';
$app_list_strings['state_0'][33] = 'North Carolina';
$app_list_strings['state_0'][34] = 'North Dakota';
$app_list_strings['state_0'][35] = 'Ohio';
$app_list_strings['state_0'][36] = 'Oklahoma';
$app_list_strings['state_0'][37] = 'Oregon';
$app_list_strings['state_0'][38] = 'Pennsylvania';
$app_list_strings['state_0'][39] = 'Rhode Island';
$app_list_strings['state_0'][40] = 'South Carolina';
$app_list_strings['state_0'][41] = 'South Dakota';
$app_list_strings['state_0'][42] = 'Tennessee';
$app_list_strings['state_0'][43] = 'Texas';
$app_list_strings['state_0'][44] = 'Utah';
$app_list_strings['state_0'][45] = 'Vermont';
$app_list_strings['state_0'][46] = 'Virginia';
$app_list_strings['state_0'][47] = 'Washington';
$app_list_strings['state_0'][48] = 'West Virginia';
$app_list_strings['state_0'][49] = 'Wisconsin';
$app_list_strings['state_0'][50] = 'Wyoming';
$app_list_strings['state_0'][51] = 'District of Colombia';
