<?php





$app_list_strings['sivstatelist'][''] = '';
$app_list_strings['sivstatelist']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['sivstatelist']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['sivstatelist']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['sivstatelist']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['sivstatelist']['Unitedstates_California'] = 'California';
$app_list_strings['sivstatelist']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['sivstatelist']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['sivstatelist']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['sivstatelist']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['sivstatelist']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['sivstatelist']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['sivstatelist']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['sivstatelist']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['sivstatelist']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['sivstatelist']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['sivstatelist']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['sivstatelist']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['sivstatelist']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['sivstatelist']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['sivstatelist']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['sivstatelist']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['sivstatelist']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['sivstatelist']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['sivstatelist']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['sivstatelist']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['sivstatelist']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['sivstatelist']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['sivstatelist']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['sivstatelist']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['sivstatelist']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['sivstatelist']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['sivstatelist']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['sivstatelist']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['sivstatelist']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['sivstatelist']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['sivstatelist']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['sivstatelist']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['sivstatelist']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['sivstatelist']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['sivstatelist']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['sivstatelist']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['sivstatelist']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['sivstatelist']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['sivstatelist']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['sivstatelist']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['sivstatelist']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['sivstatelist']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['sivstatelist']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['sivstatelist']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['sivstatelist']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['sivstatelist']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['sivstatelist']['Australia_AustralianCapitalTerritory'] = 'Australian Capital Territory';
$app_list_strings['sivstatelist']['Australia_NewSouthWales'] = 'New South Wales';
$app_list_strings['sivstatelist']['Australia_NewZealand'] = 'New Zealand';
$app_list_strings['sivstatelist']['Australia_NothernTerritory'] = 'Nothern Territory';
$app_list_strings['sivstatelist']['Australia_Queensland'] = 'Queensland';
$app_list_strings['sivstatelist']['Australia_SouthAustralia'] = 'South Australia';
$app_list_strings['sivstatelist']['Australia_Tasmania'] = 'Tasmania';
$app_list_strings['sivstatelist']['Australia_Victoria'] = 'Victoria';
$app_list_strings['sivstatelist']['Australia_Western Australia'] = 'Western Australia';


