<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_entry_point']) || !is_array($hook_array['after_entry_point'])) {
    $hook_array['after_entry_point'] = array();
}
$hook_array['after_entry_point'][] = Array(50, '', 'custom/include/dtbc/hooks/50_loginlogger.php','loginlogger_class', 'loginlogger_function'); 