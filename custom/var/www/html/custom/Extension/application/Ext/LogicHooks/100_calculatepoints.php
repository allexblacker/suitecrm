<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_save']) || !is_array($hook_array['after_save'])) {
    $hook_array['after_save'] = array();
}
$hook_array['after_save'][] = Array(100, '', 'custom/include/dtbc/hooks/100_calculatepoints.php','calculatepoints_class', 'calculatepoints_function'); 