<?php

$app_list_strings['aos_invoices_type_dom'][''] = '';
$app_list_strings['aos_invoices_type_dom']['Analyst'] = 'Analyst';
$app_list_strings['aos_invoices_type_dom']['Competitor'] = 'Konkurrent';
$app_list_strings['aos_invoices_type_dom']['Customer'] = 'Kunden';
$app_list_strings['aos_invoices_type_dom']['Integrator'] = 'Integrator';
$app_list_strings['aos_invoices_type_dom']['Investor'] = 'Investor';
$app_list_strings['aos_invoices_type_dom']['Other'] = 'Andere';
$app_list_strings['aos_invoices_type_dom']['Partner'] = 'Partner';
$app_list_strings['aos_invoices_type_dom']['Press'] = 'Drücken Sie die EINGABETASTE';
$app_list_strings['aos_invoices_type_dom']['Prospect'] = 'Zielkontakt';
$app_list_strings['aos_invoices_type_dom']['Reseller'] = 'Fachhändler';
$app_list_strings['aos_quotes_type_dom'][''] = '';
$app_list_strings['aos_quotes_type_dom']['Analyst'] = 'Analyst';
$app_list_strings['aos_quotes_type_dom']['Competitor'] = 'Konkurrent';
$app_list_strings['aos_quotes_type_dom']['Customer'] = 'Kunden';
$app_list_strings['aos_quotes_type_dom']['Integrator'] = 'Integrator';
$app_list_strings['aos_quotes_type_dom']['Investor'] = 'Investor';
$app_list_strings['aos_quotes_type_dom']['Other'] = 'Andere';
$app_list_strings['aos_quotes_type_dom']['Partner'] = 'Partner';
$app_list_strings['aos_quotes_type_dom']['Press'] = 'Drücken Sie die EINGABETASTE';
$app_list_strings['aos_quotes_type_dom']['Prospect'] = 'Zielkontakt';
$app_list_strings['aos_quotes_type_dom']['Reseller'] = 'Fachhändler';
$app_list_strings['approval_status_dom'][''] = '';
$app_list_strings['approval_status_dom']['Approved'] = 'Angenommen';
$app_list_strings['approval_status_dom']['Not Approved'] = 'Nicht angenommen';
$app_list_strings['contract_status_list']['In Progress'] = 'In Bearbeitung';
$app_list_strings['contract_status_list']['Not Started'] = 'Nicht begonnen';
$app_list_strings['contract_status_list']['Signed'] = 'Signiert';
$app_list_strings['contract_type_list']['Type'] = 'Typ';
$app_list_strings['discount_list']['Amount'] = 'Betr.';
$app_list_strings['discount_list']['Percentage'] = '%';
$app_list_strings['invoice_status_dom'][''] = '';
$app_list_strings['invoice_status_dom']['Cancelled'] = 'Storniert';
$app_list_strings['invoice_status_dom']['Paid'] = 'Bezahlt';
$app_list_strings['invoice_status_dom']['Unpaid'] = 'Nicht bezahlt';
$app_list_strings['moduleList']['AOS_Contracts'] = 'Verträge';
$app_list_strings['moduleList']['AOS_Invoices'] = 'Rechnungen';
$app_list_strings['moduleList']['AOS_Line_Item_Groups'] = 'Zeilenelement Gruppen';
$app_list_strings['moduleList']['AOS_PDF_Templates'] = 'PDF Vorlagen';
$app_list_strings['moduleList']['AOS_Product_Categories'] = 'Produktkategorien';
$app_list_strings['moduleList']['AOS_Products'] = 'Produkte';
$app_list_strings['moduleList']['AOS_Products_Quotes'] = 'Zeilenelemente';
$app_list_strings['moduleList']['AOS_Quotes'] = 'Angebote';
$app_list_strings['pdf_template_sample_dom'][''] = '';
$app_list_strings['pdf_template_type_dom']['AOS_Contracts'] = 'Verträge';
$app_list_strings['pdf_template_type_dom']['AOS_Invoices'] = 'Rechnungen';
$app_list_strings['pdf_template_type_dom']['AOS_Quotes'] = 'Angebote';
$app_list_strings['pdf_template_type_dom']['Accounts'] = 'Firmen';
$app_list_strings['pdf_template_type_dom']['Contacts'] = 'Kontakte';
$app_list_strings['pdf_template_type_dom']['Leads'] = 'Interessenten';
$app_list_strings['product_category_dom'][''] = '';
$app_list_strings['product_category_dom']['Desktops'] = 'Desktops';
$app_list_strings['product_category_dom']['Laptops'] = 'Laptops';
$app_list_strings['product_code_dom']['XXXX'] = 'XXXX';
$app_list_strings['product_code_dom']['YYYY'] = 'JJJJ';
$app_list_strings['product_quote_parent_type_dom']['AOS_Contracts'] = 'Verträge';
$app_list_strings['product_quote_parent_type_dom']['AOS_Invoices'] = 'Rechnungen';
$app_list_strings['product_quote_parent_type_dom']['AOS_Quotes'] = 'Angebote';
$app_list_strings['product_type_dom']['Good'] = 'Ware';
$app_list_strings['product_type_dom']['Service'] = 'Service';
$app_list_strings['quote_invoice_status_dom']['Invoiced'] = 'Fakturiert';
$app_list_strings['quote_invoice_status_dom']['Not Invoiced'] = 'Nicht fakturiert';
$app_list_strings['quote_stage_dom']['Closed Accepted'] = 'Gewonnen';
$app_list_strings['quote_stage_dom']['Closed Dead'] = 'Wird nicht realisiert';
$app_list_strings['quote_stage_dom']['Closed Lost'] = 'Verloren';
$app_list_strings['quote_stage_dom']['Confirmed'] = 'Bestätigt';
$app_list_strings['quote_stage_dom']['Delivered'] = 'Geliefert';
$app_list_strings['quote_stage_dom']['Draft'] = 'Entwurf';
$app_list_strings['quote_stage_dom']['Negotiation'] = 'Verhandlung';
$app_list_strings['quote_stage_dom']['On Hold'] = 'Abwarten';
$app_list_strings['quote_term_dom'][''] = '';
$app_list_strings['quote_term_dom']['Net 15'] = 'Netto 15';
$app_list_strings['quote_term_dom']['Net 30'] = 'Netto 30';
$app_list_strings['template_ddown_c_list'][''] = '';
$app_list_strings['vat_list']['0.0'] = '0';
$app_list_strings['vat_list']['17.5'] = '0,175';
$app_list_strings['vat_list']['20.0'] = '0,2';
$app_list_strings['vat_list']['5.0'] = '0,05';
$app_list_strings['vat_list']['7.5'] = '0,075';
$app_strings['LBL_GENERATE_LETTER'] = 'Brief erzeugen';
$app_strings['LBL_NO_TEMPLATE'] = 'FEHLER:\nKeine Vorlagen gefunden.\nBitte gehen Sie zum PDF-Vorlagen-Modul und erstellen Sie eine.';
$app_strings['LBL_SELECT_TEMPLATE'] = 'Bitte Vorlage auswählen';
