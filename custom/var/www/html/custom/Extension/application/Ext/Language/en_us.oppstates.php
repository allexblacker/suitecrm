<?php





$app_list_strings['oppstates'][''] = '';
$app_list_strings['oppstates']['Unitedstates_Alabama'] = 'Alabama';
$app_list_strings['oppstates']['Unitedstates_Alaska'] = 'Alaska';
$app_list_strings['oppstates']['Unitedstates_Arizona'] = 'Arizona';
$app_list_strings['oppstates']['Unitedstates_Arkansas'] = 'Arkansas';
$app_list_strings['oppstates']['Unitedstates_California'] = 'California';
$app_list_strings['oppstates']['Unitedstates_Colorado'] = 'Colorado';
$app_list_strings['oppstates']['Unitedstates_Connecticut'] = 'Connecticut';
$app_list_strings['oppstates']['Unitedstates_Delaware'] = 'Delaware';
$app_list_strings['oppstates']['Unitedstates_DistrictofColumbia'] = 'District of Columbia';
$app_list_strings['oppstates']['Unitedstates_Florida'] = 'Florida';
$app_list_strings['oppstates']['Unitedstates_Georgia'] = 'Georgia';
$app_list_strings['oppstates']['Unitedstates_Hawaii'] = 'Hawaii';
$app_list_strings['oppstates']['Unitedstates_Idaho'] = 'Idaho';
$app_list_strings['oppstates']['Unitedstates_Illinois'] = 'Illinois';
$app_list_strings['oppstates']['Unitedstates_Indiana'] = 'Indiana';
$app_list_strings['oppstates']['Unitedstates_Iowa'] = 'Iowa';
$app_list_strings['oppstates']['Unitedstates_Kansas'] = 'Kansas';
$app_list_strings['oppstates']['Unitedstates_Kentucky'] = 'Kentucky';
$app_list_strings['oppstates']['Unitedstates_Louisiana'] = 'Louisiana';
$app_list_strings['oppstates']['Unitedstates_Maine'] = 'Maine';
$app_list_strings['oppstates']['Unitedstates_Maryland'] = 'Maryland';
$app_list_strings['oppstates']['Unitedstates_Massachusetts'] = 'Massachusetts';
$app_list_strings['oppstates']['Unitedstates_Michigan'] = 'Michigan';
$app_list_strings['oppstates']['Unitedstates_Minnesota'] = 'Minnesota';
$app_list_strings['oppstates']['Unitedstates_Mississippi'] = 'Mississippi';
$app_list_strings['oppstates']['Unitedstates_Missouri'] = 'Missouri';
$app_list_strings['oppstates']['Unitedstates_Montana'] = 'Montana';
$app_list_strings['oppstates']['Unitedstates_Nebraska'] = 'Nebraska';
$app_list_strings['oppstates']['Unitedstates_Nevada'] = 'Nevada';
$app_list_strings['oppstates']['Unitedstates_NewHampshire'] = 'New Hampshire';
$app_list_strings['oppstates']['Unitedstates_NewJersey'] = 'New Jersey';
$app_list_strings['oppstates']['Unitedstates_NewMexico'] = 'New Mexico';
$app_list_strings['oppstates']['Unitedstates_NewYork'] = 'New York';
$app_list_strings['oppstates']['Unitedstates_NorthCarolina'] = 'North Carolina';
$app_list_strings['oppstates']['Unitedstates_NorthDakota'] = 'North Dakota';
$app_list_strings['oppstates']['Unitedstates_Ohio'] = 'Ohio';
$app_list_strings['oppstates']['Unitedstates_Oklahoma'] = 'Oklahoma';
$app_list_strings['oppstates']['Unitedstates_Oregon'] = 'Oregon';
$app_list_strings['oppstates']['Unitedstates_Pennsylvania'] = 'Pennsylvania';
$app_list_strings['oppstates']['Unitedstates_RhodeIsland'] = 'Rhode Island';
$app_list_strings['oppstates']['Unitedstates_SouthCarolina'] = 'South Carolina';
$app_list_strings['oppstates']['Unitedstates_SouthDakota'] = 'South Dakota';
$app_list_strings['oppstates']['Unitedstates_Tennessee'] = 'Tennessee';
$app_list_strings['oppstates']['Unitedstates_Texas'] = 'Texas';
$app_list_strings['oppstates']['Unitedstates_Utah'] = 'Utah';
$app_list_strings['oppstates']['Unitedstates_Vermont'] = 'Vermont';
$app_list_strings['oppstates']['Unitedstates_Virginia'] = 'Virginia';
$app_list_strings['oppstates']['Unitedstates_Washington'] = 'Washington';
$app_list_strings['oppstates']['Unitedstates_WestVirginia'] = 'West Virginia';
$app_list_strings['oppstates']['Unitedstates_Wisconsin'] = 'Wisconsin';
$app_list_strings['oppstates']['Unitedstates_Wyoming'] = 'Wyoming';
$app_list_strings['oppstates']['Canada_Alberta'] = 'Alberta';
$app_list_strings['oppstates']['Canada_British Columbia '] = 'British Columbia ';
$app_list_strings['oppstates']['Canada_Manitoba '] = 'Manitoba ';
$app_list_strings['oppstates']['Canada_NewBrunswick'] = 'New Brunswick';
$app_list_strings['oppstates']['Canada_NewfoundlandandLabrador '] = 'Newfoundland and Labrador ';
$app_list_strings['oppstates']['Canada_Nova Scotia '] = 'Nova Scotia ';
$app_list_strings['oppstates']['Canada_Nunavut'] = 'Nunavut';
$app_list_strings['oppstates']['Canada_Ontario'] = 'Ontario';
$app_list_strings['oppstates']['Canada_PrinceEdwardIsland '] = 'Prince Edward Island ';
$app_list_strings['oppstates']['Canada_Quebec'] = 'Quebec';
$app_list_strings['oppstates']['Canada_Saskatchewan'] = 'Saskatchewan';
$app_list_strings['oppstates']['Canada_Yukon'] = 'Yukon';

