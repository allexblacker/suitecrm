<?php

$mod_strings['LBL_CONFIG_QUICKCRM'] = 'Definition von sichtbaren Modulen und Feldern';
$mod_strings['LBL_CONFIG_QUICKCRM_TITLE'] = 'Konfiguration von QuickCRM Mobile';
$mod_strings['LBL_ERR_DIR_MSG'] = 'Manche Dateien konnten nicht erstellt werden. Bitte überprüfen Sie die Schreibberechtigung für:';
$mod_strings['LBL_QUICKCRM'] = 'QuickCRM Mobile';
$mod_strings['LBL_UPDATE_MSG'] = '<strong>Die Applikation wurde für QuickCRM auf mobilen Endgeräten aktualisiert</strong><br/>Sie erreichen die mobile Version unter:';
$mod_strings['LBL_UPDATE_QUICKCRM'] = 'Erstellt Felddefinitionen neu';
$mod_strings['LBL_UPDATE_QUICKCRM_TITLE'] = 'QuickCRM Aktualisierung';
