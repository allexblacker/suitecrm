<?php

$mod_strings['LBL_CUSTOM_ADMIN_SETTINGS_TITLE'] = 'System custom settings';
$mod_strings['LBL_ADMIN_LAYOUT_SETTINGS'] = 'Module layout settings';
$mod_strings['LBL_ADMIN_LAYOUT_SETTINGS_DESC'] = 'Define custom layouts for modules based on User\'s roles and module\'s fields';

$mod_strings['LBL_ADMIN_SELECT_MODULE'] = 'Select a module';
$mod_strings['LBL_ADMIN_SAVE_BUTTON'] = 'Save';
$mod_strings['LBL_ADMIN_DEFAULT_LAYOUT'] = 'Default layout';

$mod_strings['LBL_SOLAREDGE_SURVEY_SETTINGS'] = 'Survey config settings';
$mod_strings['LBL_SOLAREDGE_SURVEY_SETTINGS_DESC'] = 'Set the layout to languages';

$mod_strings['LBL_DEPENDECY_SETTINGS'] = 'Dependency settings';
$mod_strings['LBL_DEPENDENCY_SETTINGS_DESC'] = 'Set dependency settings of dropdown lists';
$mod_strings['LBL_DEPENDENCY_MODULE'] = 'Module';
$mod_strings['LBL_DEPENDENCY_CONTROLLER'] = 'Controller';
$mod_strings['LBL_DEPENDENCY_CONTROLLED'] = 'Controlled';
$mod_strings['LBL_SAVE_MESSAGE'] = 'Changes have been saved!';