<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$admin_option_defs = array();

$admin_option_defs['Administration']['dtbc_layout_settings']= array('Administration', 'LBL_ADMIN_LAYOUT_SETTINGS', 'LBL_ADMIN_LAYOUT_SETTINGS_DESC', './index.php?module=Administration&action=dtbcCustomSettings&type=layoutSettings');
$admin_option_defs['Administration']['solaredge_survey_settings']= array('Administration','LBL_SOLAREDGE_SURVEY_SETTINGS','LBL_SOLAREDGE_SURVEY_SETTINGS_DESC','./index.php?module=Administration&action=SolaredgeSurveyConfigs');

$admin_option_defs['Administration']['dependency_settings']= array('Administration','LBL_DEPENDECY_SETTINGS','LBL_DEPENDENCY_SETTINGS_DESC','./index.php?module=Administration&action=DependencySettings');

$admin_group_header[]= array('LBL_CUSTOM_ADMIN_SETTINGS_TITLE', '', false, $admin_option_defs, '');

