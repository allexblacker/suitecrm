<?php
// created: 2017-09-25 20:31:20
$dictionary["S1_Site"]["fields"]["s1_site_p1_project_1"] = array (
  'name' => 's1_site_p1_project_1',
  'type' => 'link',
  'relationship' => 's1_site_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
