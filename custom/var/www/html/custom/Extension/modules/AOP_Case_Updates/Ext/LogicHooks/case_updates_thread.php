<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['after_save'][] = array(
	PHP_INT_MAX - 1,
	'Updating internal_comment_c, alert_override_c, change_indicator_c',
	'custom/include/dtbc/hooks/case_updates_thread_hook.php',
	'CaseUpdatesThreadUpdater',
	'after_save'
);