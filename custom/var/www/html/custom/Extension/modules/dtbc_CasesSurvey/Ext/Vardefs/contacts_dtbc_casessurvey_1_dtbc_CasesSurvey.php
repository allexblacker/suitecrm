<?php
// created: 2017-09-08 20:40:19
$dictionary["dtbc_CasesSurvey"]["fields"]["contacts_dtbc_casessurvey_1"] = array (
  'name' => 'contacts_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_dtbc_casessurvey_1contacts_ida',
);
$dictionary["dtbc_CasesSurvey"]["fields"]["contacts_dtbc_casessurvey_1_name"] = array (
  'name' => 'contacts_dtbc_casessurvey_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_dtbc_casessurvey_1contacts_ida',
  'link' => 'contacts_dtbc_casessurvey_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["dtbc_CasesSurvey"]["fields"]["contacts_dtbc_casessurvey_1contacts_ida"] = array (
  'name' => 'contacts_dtbc_casessurvey_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_casessurvey_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);
