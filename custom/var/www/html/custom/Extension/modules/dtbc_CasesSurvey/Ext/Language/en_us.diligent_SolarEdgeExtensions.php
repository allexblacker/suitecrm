<?php

$mod_strings['LBL_CASE_NUMBER'] = "Case Number";
$mod_strings['LBL_CASE_SUBJECT'] = "Subject";
$mod_strings['LBL_DESCRIPTION'] = "<b>Additional comments</b>";
$mod_strings['LBL_SAVE'] = "Save";
$mod_strings['LBL_RESPONSE'] = "Thank you for your response!";
$mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_CASES_ACCOUNTS'] = "Not existing relationship between cases and accounts!";
$mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_CASES_CONTACTS'] = "Not existing relationship between cases and contacts!";
$mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_CASES_SURVEY'] = "Not existing relationship between cases and survey!";
$mod_strings['LBL_NOT_EXISTING_RELATIONSHIP_THIS_CASE_CONTACT'] = "Not existing relationship between this case and contact!";
$mod_strings['LBL_NOT_EXISTING_CONTACT'] = "Not existing contact!";
$mod_strings['LBL_FILLED_SURVEY'] = "This survey has already been filled!";

$mod_strings['LBL_NOT_EXISTING_CASE'] = "Not existing case!";
$mod_strings['LBL_SAVE_MESSAGE'] = "Are you sure you want to save the survey?";