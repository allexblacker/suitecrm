<?php

$mod_strings['LBL_CLONE_WITH_PRODUCTS'] = 'Clone With Products';
$mod_strings['LBL_CLONE_WITHOUT_PRODUCTS'] = 'Clone Without Products';
$mod_strings['LBL_CREATE_NEW_PROJECT'] = 'Create New Project';
$mod_strings['LBL_CLONE_WITH_PRICING'] = 'Clone With Pricing';

$mod_strings['LBL_CREATE_OPPORTUNITY_BUTTON'] = 'Create';

$mod_strings['LBL_ERR_ONLY_DANIEL_CAN_BOOK'] = 'Only Daniel change Opportunity to stage Booked and edit a booked opportunity';
$mod_strings['LBL_ERR_ESTIMATED_KW_EMPTY'] = 'Please insert a value in Estimated KW';
$mod_strings['LBL_ERR_TOTAL_SPLIT'] = 'The total split exceeds 100% for this Opportunity';
$mod_strings['LBL_ERR_WHY_LOST'] = 'Please insert why lead lost';

$mod_strings['LBL_SAVEBUTTON'] = 'Save';
$mod_strings['LBL_SELECT_PRICE_BOOK_BTN'] = 'Select Pricebook';
$mod_strings['LBL_POPUP_TITLE'] = 'Select Pricebook';
$mod_strings['LBL_POPUP_BUTTON'] = 'Select';
$mod_strings['LBL_POPUP_DROPDOWN'] = 'Pricebook';
$mod_strings['LBL_POPUP_WARNING'] = 'Changing a selected price book will delete all existing products from this opportunity.';

$mod_strings["LBL_LISTVIEW_GROUP"] = "Security Group";
$mod_strings["LBL_LISTVIEW_ENVELOPE"] = " ";

$mod_strings['LBL_FUNCTION_TOTAL_INVERTERS'] = 'Total Inverters';
$mod_strings['LBL_FUNCTION_TOTAL_KW2'] = 'Total kW';
$mod_strings['LBL_FUNCTION_TOTAL_OPTIONS'] = 'Total Options';
$mod_strings['LBL_FUNCTION_TOTAL_PRICE'] = 'Total Price';
$mod_strings['LBL_FUNCTION_TOTAL_PRODUCTS'] = 'Total Products';
$mod_strings['LBL_FUNCTION_TOTAL_OTHERS'] = 'Total Others';
$mod_strings['LBL_FUNCTION_Q1_TOTAL_PRICE'] = 'Q1 Total Price';
$mod_strings['LBL_FUNCTION_Q2_TOTAL_PRICE'] = 'Q2 Total Price';
$mod_strings['LBL_FUNCTION_Q3_TOTAL_PRICE'] = 'Q3 Total Price';
$mod_strings['LBL_FUNCTION_Q4_TOTAL_PRICE'] = 'Q4 Total Price';
$mod_strings['LBL_SEARCHFORM_SECURITY_GROUP'] = 'Security Group';

$mod_strings['LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE'] = 'Products';