<?php 

$dictionary["Opportunity"]["fields"]["total_inverters_c"] = array(
	'name' => 'total_inverters_c',
	'label' => 'LBL_FUNCTION_TOTAL_INVERTERS',
	'vname' => 'LBL_FUNCTION_TOTAL_INVERTERS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalInverters',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_kw2_c"] = array(
	'name' => 'total_kw2_c',
	'label' => 'LBL_FUNCTION_TOTAL_KW2',
	'vname' => 'LBL_FUNCTION_TOTAL_KW2',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalKw2',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_options_c"] = array(
	'name' => 'total_options_c',
	'label' => 'LBL_FUNCTION_TOTAL_OPTIONS',
	'vname' => 'LBL_FUNCTION_TOTAL_OPTIONS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalOptions',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_products_c"] = array(
	'name' => 'total_products_c',
	'label' => 'LBL_FUNCTION_TOTAL_PRODUCTS',
	'vname' => 'LBL_FUNCTION_TOTAL_PRODUCTS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalProducts',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["total_others_c"] = array(
	'name' => 'total_others_c',
	'label' => 'LBL_FUNCTION_TOTAL_OTHERS',
	'vname' => 'LBL_FUNCTION_TOTAL_OTHERS',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getTotalOthers',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q1_total_price_c"] = array(
	'name' => 'q1_total_price_c',
	'label' => 'LBL_FUNCTION_Q1_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q1_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ1TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q2_total_price_c"] = array(
	'name' => 'q2_total_price_c',
	'label' => 'LBL_FUNCTION_Q2_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q2_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ2TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q3_total_price_c"] = array(
	'name' => 'q3_total_price_c',
	'label' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ3TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);

$dictionary["Opportunity"]["fields"]["q3_total_price_c"] = array(
	'name' => 'q4_total_price_c',
	'label' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'vname' => 'LBL_FUNCTION_Q3_TOTAL_PRICE',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_getQ4TotalPrice',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/opportunitiesFunctionFields.php',
	),
);