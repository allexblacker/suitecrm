<?php

$topButtons = array(
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateOpportunityButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
);

$layout_defs['Opportunities']['subpanel_setup']['opportunities_p1_project_1']['top_buttons'] = $topButtons;