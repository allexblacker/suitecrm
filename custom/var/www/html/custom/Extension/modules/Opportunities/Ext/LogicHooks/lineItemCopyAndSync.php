<?php

$hook_array['after_relationship_add'][] = array(
	10,
	'Sync line items',
	'custom/include/dtbc/hooks/quotesLineItemSyncAndCopy.php',
	'LineItemCopySync',
	'after_relationship_add_from_opportunity'
);

$hook_array['after_relationship_delete'][] = array(
	10,
	'Sync line items',
	'custom/include/dtbc/hooks/quotesLineItemSyncAndCopy.php',
	'LineItemCopySync',
	'after_relationship_delete_from_opportunity'
);

$hook_array['after_relationship_delete'][] = array(
	20,
	'Refresh opportunity amount',
	'custom/include/dtbc/hooks/10_refresh_opportunity_amount.php',
	'RefreshOpportunity',
	'after_relationship_delete'
);
