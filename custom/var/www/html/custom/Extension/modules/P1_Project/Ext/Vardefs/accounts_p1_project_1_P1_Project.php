<?php
// created: 2017-09-25 16:58:45
$dictionary["P1_Project"]["fields"]["accounts_p1_project_1"] = array (
  'name' => 'accounts_p1_project_1',
  'type' => 'link',
  'relationship' => 'accounts_p1_project_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_p1_project_1accounts_ida',
);
$dictionary["P1_Project"]["fields"]["accounts_p1_project_1_name"] = array (
  'name' => 'accounts_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_p1_project_1accounts_ida',
  'link' => 'accounts_p1_project_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["accounts_p1_project_1accounts_ida"] = array (
  'name' => 'accounts_p1_project_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
