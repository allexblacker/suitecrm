<?php
// created: 2017-10-18 12:46:47
$dictionary["P1_Project"]["fields"]["p1_project_tasks_1"] = array (
  'name' => 'p1_project_tasks_1',
  'type' => 'link',
  'relationship' => 'p1_project_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_TASKS_1_FROM_TASKS_TITLE',
);
