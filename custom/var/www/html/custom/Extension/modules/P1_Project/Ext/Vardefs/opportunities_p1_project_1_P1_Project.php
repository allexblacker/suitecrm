<?php
// created: 2017-09-25 18:12:29
$dictionary["P1_Project"]["fields"]["opportunities_p1_project_1"] = array (
  'name' => 'opportunities_p1_project_1',
  'type' => 'link',
  'relationship' => 'opportunities_p1_project_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_p1_project_1opportunities_ida',
);
$dictionary["P1_Project"]["fields"]["opportunities_p1_project_1_name"] = array (
  'name' => 'opportunities_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_p1_project_1opportunities_ida',
  'link' => 'opportunities_p1_project_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["opportunities_p1_project_1opportunities_ida"] = array (
  'name' => 'opportunities_p1_project_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
