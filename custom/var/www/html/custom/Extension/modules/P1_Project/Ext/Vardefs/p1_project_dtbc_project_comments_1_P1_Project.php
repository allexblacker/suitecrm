<?php
// created: 2017-09-26 12:27:38
$dictionary["P1_Project"]["fields"]["p1_project_dtbc_project_comments_1"] = array (
  'name' => 'p1_project_dtbc_project_comments_1',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_comments_1',
  'source' => 'non-db',
  'module' => 'dtbc_Project_Comments',
  'bean_name' => 'dtbc_Project_Comments',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_COMMENTS_1_FROM_DTBC_PROJECT_COMMENTS_TITLE',
);
