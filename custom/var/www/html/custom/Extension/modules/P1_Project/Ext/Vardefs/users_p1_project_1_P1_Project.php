<?php
// created: 2017-09-25 21:15:05
$dictionary["P1_Project"]["fields"]["users_p1_project_1"] = array (
  'name' => 'users_p1_project_1',
  'type' => 'link',
  'relationship' => 'users_p1_project_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_USERS_TITLE',
  'id_name' => 'users_p1_project_1users_ida',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_1_name"] = array (
  'name' => 'users_p1_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_p1_project_1users_ida',
  'link' => 'users_p1_project_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_1users_ida"] = array (
  'name' => 'users_p1_project_1users_ida',
  'type' => 'link',
  'relationship' => 'users_p1_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
