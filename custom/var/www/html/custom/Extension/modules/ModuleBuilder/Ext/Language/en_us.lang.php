<?php

$mod_strings['LBL_STUDIO_LAYOUT_NAME'] = "Name of the new layout";
$mod_strings['LBL_STUDIO_SAVE_BUTTON'] = "Save";
$mod_strings['LBL_STUDIO_ERROR_MESSAGE'] = "Please enter the name of the new layout";
