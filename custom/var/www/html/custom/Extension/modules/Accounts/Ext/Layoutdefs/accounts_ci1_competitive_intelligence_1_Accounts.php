<?php
 // created: 2017-10-24 19:32:30
$layout_defs["Accounts"]["subpanel_setup"]['accounts_ci1_competitive_intelligence_1'] = array (
  'order' => 100,
  'module' => 'CI1_Competitive_Intelligence',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CI1_COMPETITIVE_INTELLIGENCE_1_FROM_CI1_COMPETITIVE_INTELLIGENCE_TITLE',
  'get_subpanel_data' => 'accounts_ci1_competitive_intelligence_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
