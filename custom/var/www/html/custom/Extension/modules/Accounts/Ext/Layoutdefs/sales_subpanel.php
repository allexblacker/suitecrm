<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
	$layout_defs['Accounts']['subpanel_setup']['ins_sales'] = array(
		'order' => 100,
		'module' => 'IS1_Inside_Sales',
		'get_subpanel_data'=>'function:getInsideSales',
		'sort_order' => 'asc',
		'sort_by' => 'name',
		'subpanel_name' => 'default',
		'title_key' => 'LBL_INS_SALES', 
		'generate_select' => false,
		'function_parameters' => array(
			'import_function_file' => 'custom/modules/Accounts/accountSubPanels.php',
			'account_id' => $this->_focus->id,
		)
	);
        
        $layout_defs['Accounts']['subpanel_setup']['outs_sales'] = array(
                'order' => 100,
                'module' => 'OS1_Outside_Sales_Activity',
                'get_subpanel_data'=>'function:getOutsideSales',
                'sort_order' => 'asc',
                'sort_by' => 'name',
                'subpanel_name' => 'default',
                'title_key' => 'LBL_OUTS_SALES', 
                'generate_select' => false,
                'function_parameters' => array(
                        'import_function_file' => 'custom/modules/Accounts/accountSubPanels.php',
                        'account_id' => $this->_focus->id,
                )
        );*/
        
        $layout_defs['Accounts']['subpanel_setup']['sites'] = array(
                'order' => 100,
                'module' => 'S1_Site',
                'get_subpanel_data'=>'function:getAccountSites',
                'sort_order' => 'asc',
                'sort_by' => 'name',
                'subpanel_name' => 'default',
                'title_key' => 'LBL_ACCOUNT_SITE', 
                'generate_select' => false,
                'function_parameters' => array(
                        'import_function_file' => 'custom/modules/Accounts/accountSubPanels.php',
                        'account_id' => $this->_focus->id,
                )
        );       