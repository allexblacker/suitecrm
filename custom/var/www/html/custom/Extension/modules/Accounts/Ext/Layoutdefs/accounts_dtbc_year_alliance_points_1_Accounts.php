<?php
 // created: 2017-09-21 16:32:04
$layout_defs["Accounts"]["subpanel_setup"]['accounts_dtbc_year_alliance_points_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Year_Alliance_Points',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE',
  'get_subpanel_data' => 'accounts_dtbc_year_alliance_points_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
