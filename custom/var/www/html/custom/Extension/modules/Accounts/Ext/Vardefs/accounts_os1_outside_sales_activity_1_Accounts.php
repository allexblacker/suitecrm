<?php
// created: 2017-10-25 18:47:12
$dictionary["Account"]["fields"]["accounts_os1_outside_sales_activity_1"] = array (
  'name' => 'accounts_os1_outside_sales_activity_1',
  'type' => 'link',
  'relationship' => 'accounts_os1_outside_sales_activity_1',
  'source' => 'non-db',
  'module' => 'OS1_Outside_Sales_Activity',
  'bean_name' => 'OS1_Outside_Sales_Activity',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
);
