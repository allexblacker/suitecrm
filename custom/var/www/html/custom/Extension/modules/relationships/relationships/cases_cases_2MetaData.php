<?php
// created: 2017-08-29 20:14:53
$dictionary["cases_cases_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cases_cases_2' => 
    array (
      'lhs_module' => 'Cases',
      'lhs_table' => 'cases',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_cases_2_c',
      'join_key_lhs' => 'cases_cases_2cases_ida',
      'join_key_rhs' => 'cases_cases_2cases_idb',
    ),
  ),
  'table' => 'cases_cases_2_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cases_cases_2cases_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cases_cases_2cases_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cases_cases_2spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cases_cases_2_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_cases_2cases_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cases_cases_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cases_cases_2cases_idb',
      ),
    ),
  ),
);