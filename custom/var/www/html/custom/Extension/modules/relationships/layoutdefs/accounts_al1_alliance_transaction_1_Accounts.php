<?php
 // created: 2017-09-20 16:42:55
$layout_defs["Accounts"]["subpanel_setup"]['accounts_al1_alliance_transaction_1'] = array (
  'order' => 100,
  'module' => 'AL1_Alliance_Transaction',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AL1_ALLIANCE_TRANSACTION_1_FROM_AL1_ALLIANCE_TRANSACTION_TITLE',
  'get_subpanel_data' => 'accounts_al1_alliance_transaction_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
