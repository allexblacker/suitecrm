<?php
 // created: 2017-10-04 15:50:43
$layout_defs["dtbc_Pricebook"]["subpanel_setup"]['dtbc_pricebook_dtbc_pricebook_product_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Pricebook_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
  'get_subpanel_data' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
