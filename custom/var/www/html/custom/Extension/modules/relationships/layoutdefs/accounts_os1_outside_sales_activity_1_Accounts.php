<?php
 // created: 2017-10-25 18:47:11
$layout_defs["Accounts"]["subpanel_setup"]['accounts_os1_outside_sales_activity_1'] = array (
  'order' => 100,
  'module' => 'OS1_Outside_Sales_Activity',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_OS1_OUTSIDE_SALES_ACTIVITY_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
  'get_subpanel_data' => 'accounts_os1_outside_sales_activity_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
