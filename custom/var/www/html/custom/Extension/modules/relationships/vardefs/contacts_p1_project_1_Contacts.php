<?php
// created: 2017-09-25 18:21:08
$dictionary["Contact"]["fields"]["contacts_p1_project_1"] = array (
  'name' => 'contacts_p1_project_1',
  'type' => 'link',
  'relationship' => 'contacts_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
