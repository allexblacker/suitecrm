<?php
// created: 2017-09-11 18:28:20
$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1"] = array (
  'name' => 'cases_sn1_serial_number_1',
  'type' => 'link',
  'relationship' => 'cases_sn1_serial_number_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
  'id_name' => 'cases_sn1_serial_number_1cases_ida',
);
$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1_name"] = array (
  'name' => 'cases_sn1_serial_number_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_sn1_serial_number_1cases_ida',
  'link' => 'cases_sn1_serial_number_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["SN1_Serial_Number"]["fields"]["cases_sn1_serial_number_1cases_ida"] = array (
  'name' => 'cases_sn1_serial_number_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_sn1_serial_number_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE',
);
