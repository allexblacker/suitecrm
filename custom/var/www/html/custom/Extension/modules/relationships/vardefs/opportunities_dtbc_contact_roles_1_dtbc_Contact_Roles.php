<?php
// created: 2017-10-04 15:28:36
$dictionary["dtbc_Contact_Roles"]["fields"]["opportunities_dtbc_contact_roles_1"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_dtbc_contact_roles_1opportunities_ida',
);
$dictionary["dtbc_Contact_Roles"]["fields"]["opportunities_dtbc_contact_roles_1_name"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_dtbc_contact_roles_1opportunities_ida',
  'link' => 'opportunities_dtbc_contact_roles_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["dtbc_Contact_Roles"]["fields"]["opportunities_dtbc_contact_roles_1opportunities_ida"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_contact_roles_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);
