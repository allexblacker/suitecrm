<?php
// created: 2017-10-04 15:50:43
$dictionary["dtbc_Pricebook_Product"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook',
  'bean_name' => 'dtbc_Pricebook',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_TITLE',
  'id_name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1_name"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_TITLE',
  'save' => true,
  'id_name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
  'link' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'table' => 'dtbc_pricebook',
  'module' => 'dtbc_Pricebook',
  'rname' => 'name',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1dtbc_pricebook_ida',
  'type' => 'link',
  'relationship' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);
