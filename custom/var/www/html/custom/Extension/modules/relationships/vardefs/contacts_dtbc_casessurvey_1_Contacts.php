<?php
// created: 2017-09-08 20:40:19
$dictionary["Contact"]["fields"]["contacts_dtbc_casessurvey_1"] = array (
  'name' => 'contacts_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'contacts_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'dtbc_CasesSurvey',
  'bean_name' => 'dtbc_CasesSurvey',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);
