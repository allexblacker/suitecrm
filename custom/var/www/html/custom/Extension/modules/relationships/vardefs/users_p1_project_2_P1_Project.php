<?php
// created: 2017-09-25 21:15:11
$dictionary["P1_Project"]["fields"]["users_p1_project_2"] = array (
  'name' => 'users_p1_project_2',
  'type' => 'link',
  'relationship' => 'users_p1_project_2',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_USERS_TITLE',
  'id_name' => 'users_p1_project_2users_ida',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_2_name"] = array (
  'name' => 'users_p1_project_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_p1_project_2users_ida',
  'link' => 'users_p1_project_2',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["P1_Project"]["fields"]["users_p1_project_2users_ida"] = array (
  'name' => 'users_p1_project_2users_ida',
  'type' => 'link',
  'relationship' => 'users_p1_project_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_2_FROM_P1_PROJECT_TITLE',
);
