<?php
// created: 2017-10-18 10:13:10
$dictionary["Account"]["fields"]["accounts_pos1_pos_tracking_1"] = array (
  'name' => 'accounts_pos1_pos_tracking_1',
  'type' => 'link',
  'relationship' => 'accounts_pos1_pos_tracking_1',
  'source' => 'non-db',
  'module' => 'POS1_POS_Tracking',
  'bean_name' => 'POS1_POS_Tracking',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_POS1_POS_TRACKING_1_FROM_POS1_POS_TRACKING_TITLE',
);
