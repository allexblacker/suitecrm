<?php
 // created: 2017-10-04 15:26:52
$layout_defs["Contacts"]["subpanel_setup"]['contacts_dtbc_contact_roles_1'] = array (
  'order' => 100,
  'module' => 'dtbc_Contact_Roles',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
  'get_subpanel_data' => 'contacts_dtbc_contact_roles_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
