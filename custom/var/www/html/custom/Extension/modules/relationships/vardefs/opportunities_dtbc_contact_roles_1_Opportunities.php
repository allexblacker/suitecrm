<?php
// created: 2017-10-04 15:28:36
$dictionary["Opportunity"]["fields"]["opportunities_dtbc_contact_roles_1"] = array (
  'name' => 'opportunities_dtbc_contact_roles_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_contact_roles_1',
  'source' => 'non-db',
  'module' => 'dtbc_Contact_Roles',
  'bean_name' => 'dtbc_Contact_Roles',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_CONTACT_ROLES_1_FROM_DTBC_CONTACT_ROLES_TITLE',
);
