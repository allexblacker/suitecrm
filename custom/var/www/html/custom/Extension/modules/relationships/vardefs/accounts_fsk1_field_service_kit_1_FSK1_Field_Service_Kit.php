<?php
// created: 2017-10-12 13:42:50
$dictionary["FSK1_Field_Service_Kit"]["fields"]["accounts_fsk1_field_service_kit_1"] = array (
  'name' => 'accounts_fsk1_field_service_kit_1',
  'type' => 'link',
  'relationship' => 'accounts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_fsk1_field_service_kit_1accounts_ida',
);
$dictionary["FSK1_Field_Service_Kit"]["fields"]["accounts_fsk1_field_service_kit_1_name"] = array (
  'name' => 'accounts_fsk1_field_service_kit_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_fsk1_field_service_kit_1accounts_ida',
  'link' => 'accounts_fsk1_field_service_kit_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["FSK1_Field_Service_Kit"]["fields"]["accounts_fsk1_field_service_kit_1accounts_ida"] = array (
  'name' => 'accounts_fsk1_field_service_kit_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
);
