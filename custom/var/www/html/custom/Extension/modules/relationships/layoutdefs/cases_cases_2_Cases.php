<?php
 // created: 2017-08-29 20:14:53
$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_CASES_2_FROM_CASES_R_TITLE',
  'get_subpanel_data' => 'cases_cases_2cases_ida',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
