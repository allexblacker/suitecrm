<?php
// created: 2017-09-25 16:58:45
$dictionary["Account"]["fields"]["accounts_p1_project_1"] = array (
  'name' => 'accounts_p1_project_1',
  'type' => 'link',
  'relationship' => 'accounts_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
