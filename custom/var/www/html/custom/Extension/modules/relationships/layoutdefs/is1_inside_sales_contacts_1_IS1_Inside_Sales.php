<?php
 // created: 2017-11-03 23:37:26
$layout_defs["IS1_Inside_Sales"]["subpanel_setup"]['is1_inside_sales_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_IS1_INSIDE_SALES_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'is1_inside_sales_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
