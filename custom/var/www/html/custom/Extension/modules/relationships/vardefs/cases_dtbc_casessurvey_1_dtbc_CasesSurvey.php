<?php
// created: 2017-09-08 20:38:49
$dictionary["dtbc_CasesSurvey"]["fields"]["cases_dtbc_casessurvey_1"] = array (
  'name' => 'cases_dtbc_casessurvey_1',
  'type' => 'link',
  'relationship' => 'cases_dtbc_casessurvey_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_CASES_TITLE',
  'id_name' => 'cases_dtbc_casessurvey_1cases_ida',
);
$dictionary["dtbc_CasesSurvey"]["fields"]["cases_dtbc_casessurvey_1_name"] = array (
  'name' => 'cases_dtbc_casessurvey_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_dtbc_casessurvey_1cases_ida',
  'link' => 'cases_dtbc_casessurvey_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["dtbc_CasesSurvey"]["fields"]["cases_dtbc_casessurvey_1cases_ida"] = array (
  'name' => 'cases_dtbc_casessurvey_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_dtbc_casessurvey_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
);
