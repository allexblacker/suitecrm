<?php
// created: 2017-09-21 16:32:04
$dictionary["dtbc_Year_Alliance_Points"]["fields"]["accounts_dtbc_year_alliance_points_1"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1',
  'type' => 'link',
  'relationship' => 'accounts_dtbc_year_alliance_points_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_dtbc_year_alliance_points_1accounts_ida',
);
$dictionary["dtbc_Year_Alliance_Points"]["fields"]["accounts_dtbc_year_alliance_points_1_name"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_dtbc_year_alliance_points_1accounts_ida',
  'link' => 'accounts_dtbc_year_alliance_points_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["dtbc_Year_Alliance_Points"]["fields"]["accounts_dtbc_year_alliance_points_1accounts_ida"] = array (
  'name' => 'accounts_dtbc_year_alliance_points_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_dtbc_year_alliance_points_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_DTBC_YEAR_ALLIANCE_POINTS_1_FROM_DTBC_YEAR_ALLIANCE_POINTS_TITLE',
);
