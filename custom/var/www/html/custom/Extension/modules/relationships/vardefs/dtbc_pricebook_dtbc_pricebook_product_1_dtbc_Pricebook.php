<?php
// created: 2017-10-04 15:50:43
$dictionary["dtbc_Pricebook"]["fields"]["dtbc_pricebook_dtbc_pricebook_product_1"] = array (
  'name' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'dtbc_pricebook_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'dtbc_Pricebook_Product',
  'bean_name' => 'dtbc_Pricebook_Product',
  'side' => 'right',
  'vname' => 'LBL_DTBC_PRICEBOOK_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);
