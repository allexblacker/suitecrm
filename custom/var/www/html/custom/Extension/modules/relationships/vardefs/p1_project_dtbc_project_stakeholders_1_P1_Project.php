<?php
// created: 2017-09-26 12:28:23
$dictionary["P1_Project"]["fields"]["p1_project_dtbc_project_stakeholders_1"] = array (
  'name' => 'p1_project_dtbc_project_stakeholders_1',
  'type' => 'link',
  'relationship' => 'p1_project_dtbc_project_stakeholders_1',
  'source' => 'non-db',
  'module' => 'dtbc_Project_Stakeholders',
  'bean_name' => 'dtbc_Project_Stakeholders',
  'side' => 'right',
  'vname' => 'LBL_P1_PROJECT_DTBC_PROJECT_STAKEHOLDERS_1_FROM_DTBC_PROJECT_STAKEHOLDERS_TITLE',
);
