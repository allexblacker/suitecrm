<?php
// created: 2017-10-04 15:48:02
$dictionary["dtbc_Line_Item"]["fields"]["opportunities_dtbc_line_item_1"] = array (
  'name' => 'opportunities_dtbc_line_item_1',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_line_item_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_dtbc_line_item_1opportunities_ida',
);
$dictionary["dtbc_Line_Item"]["fields"]["opportunities_dtbc_line_item_1_name"] = array (
  'name' => 'opportunities_dtbc_line_item_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_dtbc_line_item_1opportunities_ida',
  'link' => 'opportunities_dtbc_line_item_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["dtbc_Line_Item"]["fields"]["opportunities_dtbc_line_item_1opportunities_ida"] = array (
  'name' => 'opportunities_dtbc_line_item_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_dtbc_line_item_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_DTBC_LINE_ITEM_1_FROM_DTBC_LINE_ITEM_TITLE',
);
