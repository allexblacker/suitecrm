<?php
// created: 2017-10-12 13:42:50
$dictionary["Account"]["fields"]["accounts_fsk1_field_service_kit_1"] = array (
  'name' => 'accounts_fsk1_field_service_kit_1',
  'type' => 'link',
  'relationship' => 'accounts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'module' => 'FSK1_Field_Service_Kit',
  'bean_name' => 'FSK1_Field_Service_Kit',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
);
