<?php
// created: 2017-11-09 11:47:52
$dictionary["FSK1_Field_Service_Kit"]["fields"]["contacts_fsk1_field_service_kit_1"] = array (
  'name' => 'contacts_fsk1_field_service_kit_1',
  'type' => 'link',
  'relationship' => 'contacts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_fsk1_field_service_kit_1contacts_ida',
);
$dictionary["FSK1_Field_Service_Kit"]["fields"]["contacts_fsk1_field_service_kit_1_name"] = array (
  'name' => 'contacts_fsk1_field_service_kit_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_fsk1_field_service_kit_1contacts_ida',
  'link' => 'contacts_fsk1_field_service_kit_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["FSK1_Field_Service_Kit"]["fields"]["contacts_fsk1_field_service_kit_1contacts_ida"] = array (
  'name' => 'contacts_fsk1_field_service_kit_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_fsk1_field_service_kit_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
);
