<?php
 // created: 2017-08-23 12:39:37
$dictionary['Lead']['fields']['converted']['audited']=true;
$dictionary['Lead']['fields']['converted']['inline_edit']=true;
$dictionary['Lead']['fields']['converted']['comments']='Has Lead been converted to a Contact (and other Sugar objects)';
$dictionary['Lead']['fields']['converted']['merge_filter']='disabled';

 ?>