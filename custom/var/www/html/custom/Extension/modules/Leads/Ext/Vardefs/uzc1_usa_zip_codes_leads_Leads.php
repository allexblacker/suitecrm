<?php
// created: 2017-08-31 12:53:12
$dictionary["Lead"]["fields"]["uzc1_usa_zip_codes_leads"] = array (
  'name' => 'uzc1_usa_zip_codes_leads',
  'type' => 'link',
  'relationship' => 'uzc1_usa_zip_codes_leads',
  'source' => 'non-db',
  'module' => 'UZC1_USA_ZIP_Codes',
  'bean_name' => 'UZC1_USA_ZIP_Codes',
  'vname' => 'LBL_UZC1_USA_ZIP_CODES_LEADS_FROM_UZC1_USA_ZIP_CODES_TITLE',
);
