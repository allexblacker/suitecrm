<?php
// created: 2017-09-25 21:15:05
$dictionary["User"]["fields"]["users_p1_project_1"] = array (
  'name' => 'users_p1_project_1',
  'type' => 'link',
  'relationship' => 'users_p1_project_1',
  'source' => 'non-db',
  'module' => 'P1_Project',
  'bean_name' => 'P1_Project',
  'side' => 'right',
  'vname' => 'LBL_USERS_P1_PROJECT_1_FROM_P1_PROJECT_TITLE',
);
