<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$dictionary['dtbc_StatusLog']['fields']['cases_dtbc_statuslog_1_name']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['case_sub_status_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_end_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['time_spent_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_start_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['case_status_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['userid_c']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_entered']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['date_modified']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['name']['inline_edit']='0';
$dictionary['dtbc_StatusLog']['fields']['description']['inline_edit']='0';
