<?php
 // created: 2017-11-01 02:12:01
$layout_defs["OS1_Outside_Sales_Activity"]["subpanel_setup"]['os1_outside_sales_activity_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'os1_outside_sales_activity_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
