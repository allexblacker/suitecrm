<?php

array_push($job_strings, 'custom_workflow_process');

function custom_workflow_process() {
	require_once('custom/modules/AOW_WorkFlow/AOW_WorkFlow.php');
    $workflow = new CustomAOW_WorkFlow();
    return $workflow->run_flows();
}

