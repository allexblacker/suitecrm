<?php

array_push($job_strings, 'custom_inbound_emails');

function custom_inbound_emails() {
	require_once("custom/include/dtbc/InboundEmailScheduler.php");
	
	$temp = new InboundEmailScheduler();
	
	return $temp->runScheduler();
}

