<?php


$job_strings['CalcLeadAgeJob'] = 'custom_cron_job';



function custom_cron_job(){
//    require_once 'custom/include/customScheduledTasks.php';
    //do the function here.
    $bean = BeanFactory::getBean('Leads');
    $query = "leads.status <> 'Converted' and leads.status <> 'Dead'";
    $actLeads = $bean->get_full_list('',$query);
    foreach($actLeads as $actLead){
       
       //Mark each meeting as Not Held
       $tmp = new DateTime();
       $sToday = $tmp->format("Y-m-d");
       $Today = new DateTime($sToday);
       $sEnterDate = $actLead->date_entered; 
       $tmpEnterDate = new DateTime($sEnterDate) ;
       $sEnterDate = $tmpEnterDate->format("Y-m-d");
       $EnterDate = new DateTime($sEnterDate);
       $dateDifference = $Today->diff($EnterDate);   
       $actLead->lead_age_c = $dateDifference->days;
       $actLead->save();
        
    }

    return true; // Remember to return the result of successful execution whether this be from a function or not.
    ///return my_custom_function_from_my_included_file();
}
