<?php 

$mod_strings['LBL_CASE_JOB_2DAYS'] = 'Case - Two days after contacted with customer';
$mod_strings['LBL_CASE_JOB_3DAYS'] = 'Case - Three days after Waiting for customer';
$mod_strings['LBL_CASE_JOB_5DAYS'] = 'Case - Five days after Waiting for customer';
$mod_strings['LBL_EMAIL_TO_CASE'] = 'Create cases from emails';
$mod_strings['LBL_CALCULATE_YEARLY_POINTS'] = 'Calculate yearly point values';
$mod_strings['LBL_SYSTEMWORKFLOWS'] = 'Runs System Workflows - aka timed Workflows';
$mod_strings['LBL_CUSTOM_INBOUND_EMAILS'] = 'Check Inbound Mailboxes - And replace variables in the email template';
$mod_strings['LBL_CUSTOM_INBOUND_EMAILS_RESET'] = 'Reset Inbound email job, after the time out period';
$mod_strings['LBL_CUSTOM_WORKFLOW_PROCESS'] = 'Process Workflow Tasks - And use custom order';
$mod_strings['LBL_RESET_JOB'] = 'Reset Job';
?>