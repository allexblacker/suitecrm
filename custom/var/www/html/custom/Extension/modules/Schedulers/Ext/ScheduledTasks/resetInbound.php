<?php

array_push($job_strings, 'custom_inbound_emails_reset');

function custom_inbound_emails_reset() {
	require_once("custom/include/dtbc/InboundEmailScheduler.php");
	
	$temp = new InboundEmailScheduler();
	
	return $temp->resetScheduler(true);
}

