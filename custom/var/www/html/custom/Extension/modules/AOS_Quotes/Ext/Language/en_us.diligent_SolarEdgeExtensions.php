<?php 

$mod_strings["LBL_LISTVIEW_GROUP"] = "Security Group";
$mod_strings["LBL_LISTVIEW_ENVELOPE"] = " ";

$mod_strings["LBL_LINEITEM_SUBTOTAL"] = "Subtotal";
$mod_strings["LBL_LINEITEM_DISCOUNT"] = "Discount";
$mod_strings["LBL_LINEITEM_TOTALPRICE"] = "Total Price";
$mod_strings["LBL_LINEITEM_GRANDTOTAL"] = "Grand Total";

$mod_strings["LBL_SYNC_START"] = "Start Sync";
$mod_strings["LBL_SYNC_STOP"] = "Stop Sync";
$mod_strings["LBL_SYNC_WARNING"] = "Quote Line Items will replace opportunity products.<br/><br/>After initial sync, all future updates to quote line items and opportunity products will automatically sync both ways.";
$mod_strings["LBL_SYNC_TITLE"] = "Synchronize quote and opportunity";
$mod_strings["LBL_SYNC_OK"] = "Sync";
$mod_strings['LBL_SEARCHFORM_SECURITY_GROUP'] = 'Security Group';

$mod_strings['LBL_BTN_GENDOC'] = 'Create PDF';

$mod_strings['LBL_PU_TITLE'] = 'Create PDF';
$mod_strings['LBL_PU_DOCTYPE'] = 'Select document template';
$mod_strings['LBL_PU_CREATE'] = 'Create';

?>