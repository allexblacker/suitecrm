<?php
// created: 2017-10-25 18:47:53
$dictionary["IS1_Inside_Sales"]["fields"]["accounts_is1_inside_sales_1"] = array (
  'name' => 'accounts_is1_inside_sales_1',
  'type' => 'link',
  'relationship' => 'accounts_is1_inside_sales_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_is1_inside_sales_1accounts_ida',
);
$dictionary["IS1_Inside_Sales"]["fields"]["accounts_is1_inside_sales_1_name"] = array (
  'name' => 'accounts_is1_inside_sales_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_is1_inside_sales_1accounts_ida',
  'link' => 'accounts_is1_inside_sales_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["IS1_Inside_Sales"]["fields"]["accounts_is1_inside_sales_1accounts_ida"] = array (
  'name' => 'accounts_is1_inside_sales_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_is1_inside_sales_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_IS1_INSIDE_SALES_1_FROM_IS1_INSIDE_SALES_TITLE',
);
