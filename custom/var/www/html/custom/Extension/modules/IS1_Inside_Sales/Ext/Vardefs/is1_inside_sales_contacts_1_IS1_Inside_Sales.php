<?php
// created: 2017-11-03 23:37:27
$dictionary["IS1_Inside_Sales"]["fields"]["is1_inside_sales_contacts_1"] = array (
  'name' => 'is1_inside_sales_contacts_1',
  'type' => 'link',
  'relationship' => 'is1_inside_sales_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_IS1_INSIDE_SALES_CONTACTS_1_FROM_CONTACTS_TITLE',
);
