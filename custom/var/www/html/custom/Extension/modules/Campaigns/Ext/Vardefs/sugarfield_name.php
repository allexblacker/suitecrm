<?php
 // created: 2017-11-16 14:57:43
$dictionary['Campaign']['fields']['name']['len']='255';
$dictionary['Campaign']['fields']['name']['inline_edit']=true;
$dictionary['Campaign']['fields']['name']['comments']='The name of the campaign';
$dictionary['Campaign']['fields']['name']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['name']['unified_search']=false;
$dictionary['Campaign']['fields']['name']['full_text_search']=array (
);

 ?>