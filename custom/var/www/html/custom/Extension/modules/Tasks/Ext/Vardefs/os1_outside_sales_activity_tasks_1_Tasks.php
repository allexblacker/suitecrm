<?php
// created: 2017-11-01 02:12:01
$dictionary["Task"]["fields"]["os1_outside_sales_activity_tasks_1"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1',
  'type' => 'link',
  'relationship' => 'os1_outside_sales_activity_tasks_1',
  'source' => 'non-db',
  'module' => 'OS1_Outside_Sales_Activity',
  'bean_name' => 'OS1_Outside_Sales_Activity',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
  'id_name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
);
$dictionary["Task"]["fields"]["os1_outside_sales_activity_tasks_1_name"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_OS1_OUTSIDE_SALES_ACTIVITY_TITLE',
  'save' => true,
  'id_name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
  'link' => 'os1_outside_sales_activity_tasks_1',
  'table' => 'os1_outside_sales_activity',
  'module' => 'OS1_Outside_Sales_Activity',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida"] = array (
  'name' => 'os1_outside_sales_activity_tasks_1os1_outside_sales_activity_ida',
  'type' => 'link',
  'relationship' => 'os1_outside_sales_activity_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OS1_OUTSIDE_SALES_ACTIVITY_TASKS_1_FROM_TASKS_TITLE',
);
