<?php
 
$hook_array['after_save'][] = array(
	70,
	'If the date entered is midnight, update it to current date',
	'custom/include/dtbc/hooks/70_update_date_entered.php',
	'UpdateDateEntered',
	'after_save'
);