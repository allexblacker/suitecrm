<?php

$mod_strings['LBL_AOP_CASE_ATTACHMENTS'] = 'Anlagen: ';
$mod_strings['LBL_AOP_CASE_UPDATES'] = 'Fälle Updates?';
$mod_strings['LBL_AOP_CASE_UPDATES_THREADED'] = 'Konversationen zu Fall Aktualisierungen';
$mod_strings['LBL_CASE_UPDATES_COLLAPSE_ALL'] = 'Alle zuklappen';
$mod_strings['LBL_CASE_UPDATES_EXPAND_ALL'] = 'Alle ausklappen';
$mod_strings['LBL_INTERNAL'] = 'Internes Update';
$mod_strings['LBL_NO_CASE_UPDATES'] = 'Zu diesem Fall gibt es keine Aktualisierungen';
$mod_strings['LBL_UPDATE_TEXT'] = 'Aktualisieren von Text';
