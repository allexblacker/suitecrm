<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

foreach ($layout_defs['Cases']['subpanel_setup']['activities']['top_buttons'] as $key => $value) {
	if ($value['widget_class'] == "SubPanelTopComposeEmailButton") {
		$layout_defs['Cases']['subpanel_setup']['activities']['top_buttons'][$key]['widget_class'] = 'SubPanelTopCustomComposeEmailButton';
		break;
	}
}