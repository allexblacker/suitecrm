<?php
 // created: 2017-08-29 11:10:07
$layout_defs["Cases"]["subpanel_setup"]['cases_sn1_serial_number_1'] = array (
  'order' => 100,
  'module' => 'SN1_Serial_Number',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_SN1_SERIAL_NUMBER_1_FROM_SN1_SERIAL_NUMBER_TITLE',
  'get_subpanel_data' => 'cases_sn1_serial_number_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
