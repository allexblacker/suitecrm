<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$bean = BeanFactory::getBean("Cases", $_REQUEST['record']);

$layout_defs["Cases"]["subpanel_setup"]['cases_cases_2cases_ida'] = null;

if($bean->case_record_type_c != "compensation"){
	unset($layout_defs['Cases']['subpanel_setup']['cases_cases_1cases_ida']);
}

$layout_defs["Cases"]["subpanel_setup"]['cases_dtbc_statuslog_1']['top_buttons'] = array();

