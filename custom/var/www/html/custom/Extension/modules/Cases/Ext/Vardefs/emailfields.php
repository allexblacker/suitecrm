<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['email_serial_numbers'] = array(
	'name' => 'email_serial_numbers',
	'label' => 'LBL_POINTS_TOTAL',
	'vname' => 'LBL_POINTS_TOTAL',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_email_serial_numbers',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/cases_email_serial_numbers.php',
	), 
);
