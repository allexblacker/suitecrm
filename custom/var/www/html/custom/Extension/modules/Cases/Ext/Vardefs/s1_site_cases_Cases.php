<?php
// created: 2017-08-31 12:51:23
$dictionary["Case"]["fields"]["s1_site_cases"] = array (
  'name' => 's1_site_cases',
  'type' => 'link',
  'relationship' => 's1_site_cases',
  'source' => 'non-db',
  'module' => 'S1_Site',
  'bean_name' => 'S1_Site',
  'vname' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
  'id_name' => 's1_site_casess1_site_ida',
);
$dictionary["Case"]["fields"]["s1_site_cases_name"] = array (
  'name' => 's1_site_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE',
  'save' => true,
  'id_name' => 's1_site_casess1_site_ida',
  'link' => 's1_site_cases',
  'table' => 's1_site',
  'module' => 'S1_Site',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["s1_site_casess1_site_ida"] = array (
  'name' => 's1_site_casess1_site_ida',
  'type' => 'link',
  'relationship' => 's1_site_cases',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_S1_SITE_CASES_FROM_CASES_TITLE',
);
