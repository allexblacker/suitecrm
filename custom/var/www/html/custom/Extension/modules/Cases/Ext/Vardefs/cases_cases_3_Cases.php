<?php
// created: 2017-09-15 13:18:31
$dictionary["Case"]["fields"]["cases_cases_3"] = array (
  'name' => 'cases_cases_3',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
  'id_name' => 'cases_cases_3cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_3"] = array (
  'name' => 'cases_cases_3',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_R_TITLE',
  'id_name' => 'cases_cases_3cases_ida',
);
$dictionary["Case"]["fields"]["cases_cases_3_name"] = array (
  'name' => 'cases_cases_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_R_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_3cases_ida',
  'link' => 'cases_cases_3',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_3_name"] = array (
  'name' => 'cases_cases_3_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
  'save' => true,
  'id_name' => 'cases_cases_3cases_ida',
  'link' => 'cases_cases_3',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["cases_cases_3cases_ida"] = array (
  'name' => 'cases_cases_3cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_R_TITLE',
);
$dictionary["Case"]["fields"]["cases_cases_3cases_ida"] = array (
  'name' => 'cases_cases_3cases_ida',
  'type' => 'link',
  'relationship' => 'cases_cases_3',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE',
);
