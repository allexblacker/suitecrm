<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	100,
	'Updating field engineer email field',
	'custom/include/dtbc/hooks/update_field_engineer_email.php',
	'FieldEngineerEmailUpdater',
	'before_save'
);