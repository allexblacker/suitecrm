<?php

$hook_array['after_save'][] = array(
	20,
	'When the Case Sub-Status is changed to ‘Pending Agent Call’ case owner will receive an email',
	'custom/include/dtbc/pendingagentcall.php',
	'PendingAgentCall',
	'after_save'
);