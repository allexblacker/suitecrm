<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary['Case']['fields']['aop_case_updates_threaded_custom'] = array(
	'required' => false,
	'name' => 'aop_case_updates_threaded_custom',
	'vname' => 'LBL_AOP_CASE_UPDATES_THREADED',
	'type' => 'function',
	'source' => 'non-db',
	'massupdate' => 0,
	'studio' => 'visible',
	'importable' => 'false',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => 0,
	'audited' => false,
	'reportable' => false,
	'inline_edit' => 0,
	'function' => array(
		'name' => 'display_updates_custom',
		'returns' => 'html',
		'include' => 'custom/modules/AOP_Case_Updates/Case_Updates.php',
	),
);
