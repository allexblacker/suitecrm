<?php
 // created: 2017-10-08 13:57:02
$dictionary['Case']['fields']['case_number']['comments']='Visual unique identifier';
$dictionary['Case']['fields']['case_number']['merge_filter']='disabled';
$dictionary['Case']['fields']['case_number']['enable_range_search']=false;
$dictionary['Case']['fields']['case_number']['autoinc_next']='117';
$dictionary['Case']['fields']['case_number']['min']=false;
$dictionary['Case']['fields']['case_number']['max']=false;

 ?>