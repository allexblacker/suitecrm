<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

$hook_array['before_save'][] = array(
	21,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_eu'
);

$hook_array['before_save'][] = array(
	22,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_il'
);

$hook_array['before_save'][] = array(
	23,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_asia'
);

$hook_array['before_save'][] = array(
	24,
	'Run only for service partner cases',
	'custom/include/dtbc/hooks/workflow_set_tier2_region.php',
	'Tier2RegionUpdater',
	'before_save_to_aus'
);