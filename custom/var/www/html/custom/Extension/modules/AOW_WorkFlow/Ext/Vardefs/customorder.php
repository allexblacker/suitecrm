<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$dictionary["AOW_WorkFlow"]["fields"]["custom_order_c"] = array(
	'required' => false,
	'name' => 'custom_order_c',
	'vname' => 'LBL_CUSTOM_ORDER',
	'type' => 'varchar',
	'massupdate' => 0,
	'default' => '0',
	'no_default' => false,
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'inline_edit' => true,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => '100',
	'size' => '20',
);
