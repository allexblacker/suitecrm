<?php

$mod_strings['LBL_AOP_CASE_UPDATES'] = 'Ticket Updates';
$mod_strings['LBL_CREATE_PORTAL_USER'] = 'Portal Benutzer erstellen';
$mod_strings['LBL_CREATE_PORTAL_USER_FAILED'] = 'Konnte keinen Portal Benutzer erstellen';
$mod_strings['LBL_CREATE_PORTAL_USER_SUCCESS'] = 'Portal Benutzer erstellt';
$mod_strings['LBL_DISABLE_PORTAL_USER'] = 'Portal Benutzer deaktivieren';
$mod_strings['LBL_DISABLE_PORTAL_USER_FAILED'] = 'Deaktivieren von Portal Benutzer fehlgeschlagen';
$mod_strings['LBL_DISABLE_PORTAL_USER_SUCCESS'] = 'Portal Benutzer deaktiviert';
$mod_strings['LBL_ENABLE_PORTAL_USER'] = 'Portal Benutzer aktivieren';
$mod_strings['LBL_ENABLE_PORTAL_USER_FAILED'] = 'Erstellen von Portal Benutzer fehlgeschlagen';
$mod_strings['LBL_ENABLE_PORTAL_USER_SUCCESS'] = 'Portal Benutzer aktiviert';
$mod_strings['LBL_NO_JOOMLA_URL'] = 'Keine URL für Portal definiert';
$mod_strings['LBL_PORTAL_ACCOUNT_DISABLED'] = 'Benutzerkonto deaktiviert';
$mod_strings['LBL_PORTAL_USER_TYPE'] = 'Webportal User-Typ';
