<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

	$layout_defs['Contacts']['subpanel_setup']['lms'] = array(
		'order' => 100,
		'module' => 'TLA1_Training_LMS_Absorb',
		'get_subpanel_data'=>'function:getTrainingLMS',
		'sort_order' => 'asc',
		'sort_by' => 'training_name',
		'subpanel_name' => 'default',
		'title_key' => 'LBL_LMS', 
		'generate_select' => false,
		'function_parameters' => array(
			'import_function_file' => 'custom/modules/Contacts/contactSubPanels.php',
			'contact_id' => $this->_focus->id,
			//'sex' => 'Male',
		)
	);