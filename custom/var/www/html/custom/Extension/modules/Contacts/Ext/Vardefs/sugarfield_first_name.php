<?php
 // created: 2017-10-24 16:08:09
$dictionary['Contact']['fields']['first_name']['required']=true;
$dictionary['Contact']['fields']['first_name']['inline_edit']=true;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';

 ?>