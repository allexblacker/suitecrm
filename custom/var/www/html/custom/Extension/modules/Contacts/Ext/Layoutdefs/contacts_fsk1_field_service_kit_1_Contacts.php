<?php
 // created: 2017-11-09 11:47:52
$layout_defs["Contacts"]["subpanel_setup"]['contacts_fsk1_field_service_kit_1'] = array (
  'order' => 100,
  'module' => 'FSK1_Field_Service_Kit',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_FSK1_FIELD_SERVICE_KIT_TITLE',
  'get_subpanel_data' => 'contacts_fsk1_field_service_kit_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
