<?php
 // created: 2017-09-08 20:40:19
$layout_defs["Contacts"]["subpanel_setup"]['contacts_dtbc_casessurvey_1'] = array (
  'order' => 100,
  'module' => 'dtbc_CasesSurvey',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_DTBC_CASESSURVEY_1_FROM_DTBC_CASESSURVEY_TITLE',
  'get_subpanel_data' => 'contacts_dtbc_casessurvey_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
