<?php

$dictionary["Contact"]["fields"]["add_new_contact_c"] = array(
	'name' => 'add_new_contact_c',
	'label' => 'LBL_FUNCTION_ADD_NEW_CONTACT',
	'vname' => 'LBL_FUNCTION_ADD_NEW_CONTACT',
	'type' => 'function',
	'source' => 'non-db',
	'studio' => 'visible',
	'function' => array(
		'name' => 'dtbc_AddContactButton',
		'returns' => 'html',
		'include' => 'custom/include/dtbc/contactsFunctionFields.php',
	),
);