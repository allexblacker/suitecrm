<?php

$hook_array['process_record'][] = array(
	10,
	'Display rma alert field',
	'custom/include/dtbc/hooks/SerialNumber_ProcessRecords.php',
	'ProcessRecords',
	'process_record_rma_alert'
);

$hook_array['process_record'][] = array(
	20,
	'Display checkbox field on subpanels to be able to controll the serial number check rma function',
	'custom/include/dtbc/hooks/SerialNumber_ProcessRecords.php',
	'ProcessRecords',
	'process_record_check_rma'
);