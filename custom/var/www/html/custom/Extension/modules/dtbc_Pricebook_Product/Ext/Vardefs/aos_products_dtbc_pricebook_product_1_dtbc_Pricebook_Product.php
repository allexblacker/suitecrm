<?php
// created: 2017-10-04 15:59:42
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_products_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_products_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_dtbc_pricebook_product_1aos_products_ida',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_products_dtbc_pricebook_product_1_name"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_dtbc_pricebook_product_1aos_products_ida',
  'link' => 'aos_products_dtbc_pricebook_product_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_products_dtbc_pricebook_product_1aos_products_ida"] = array (
  'name' => 'aos_products_dtbc_pricebook_product_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);
