<?php
// created: 2017-10-13 13:04:41
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_quotes_dtbc_pricebook_product_1"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_QUOTES_TITLE',
  'id_name' => 'aos_quotes_dtbc_pricebook_product_1aos_quotes_ida',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_quotes_dtbc_pricebook_product_1_name"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_AOS_QUOTES_TITLE',
  'save' => true,
  'id_name' => 'aos_quotes_dtbc_pricebook_product_1aos_quotes_ida',
  'link' => 'aos_quotes_dtbc_pricebook_product_1',
  'table' => 'aos_quotes',
  'module' => 'AOS_Quotes',
  'rname' => 'name',
);
$dictionary["dtbc_Pricebook_Product"]["fields"]["aos_quotes_dtbc_pricebook_product_1aos_quotes_ida"] = array (
  'name' => 'aos_quotes_dtbc_pricebook_product_1aos_quotes_ida',
  'type' => 'link',
  'relationship' => 'aos_quotes_dtbc_pricebook_product_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_DTBC_PRICEBOOK_PRODUCT_1_FROM_DTBC_PRICEBOOK_PRODUCT_TITLE',
);
