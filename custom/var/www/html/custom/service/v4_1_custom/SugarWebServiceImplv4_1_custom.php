<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 if(!defined('sugarEntry')){
   define('sugarEntry', true);
 }
 require_once 'service/v4_1/SugarWebServiceImplv4_1.php';
 class SugarWebServiceImplv4_1_custom extends SugarWebServiceImplv4_1
 {

    function getDepandencesTable($session)
    {
       //Here we check that $session represents a valid session
        /*if (!self::$helperObject->checkSessionAndModuleAccess(
           $session,
           'invalid_session',
           '',
           '',
           '',
           new SoapError()))
        {
           $GLOBALS['log']->info('Error - Not Valid SessonId');
           echo 'in';
           return false;
        }*/
       
        global $db;
        $sql = "SELECT module, controller, controller_value, controlled, controlled_value FROM dtbc_dependencies "; 
	$queryResult = $db->query($sql);       
        $values = array();
        while($record = $db->fetchByAssoc($queryResult)){
                //array_push($values,$record['module'] . '|' . $record['controller'] . '|' . $record['controller_value'] . '|' . $record['controlled'] . '|' . $record['controlled_value']);
            array_push($values,$record);
        }
        
        return json_encode($values); //$values;
        
    }
}