
<?php

// specify the REST web service to interact with
//$url = 'http://localhost:8080/SuiteCrm/custom/service/v4_1_custom/rest.php';
//
//$username = "admin";
//$password = "adm!n";

$url = 'http://10.0.10.6/custom/service/v4_1_custom/rest.php';

$username = "ServicePortalAPI";
$password = "TheHouse123";

//function to make cURL request
function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	//print_r($http_status);
	//print_r($result);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);

//echo "<pre>";
//print_r($login_result);
//echo "</pre>";

//get session id
$session_id = $login_result->id;

//echo $session_id;
/*
        $get_entry_list_parameters = array(

             //session id
             'session' => $session_id,

             //The name of the module from which to retrieve records
             'module_name' => "Contacts",
                     //"contacts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Contacts' AND ea.email_address_caps LIKE '%" . $email1 . "' AND eabr.deleted=0)",
                     "contacts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Contacts' AND ea.email_address_caps = '" . $email . "' AND eabr.deleted=0)",
             //'query' => "",
                     'order_by' => "",

             //The record offset from which to start.
             'offset' => "0",

             //Optional. The list of fields to be returned in the results
             'select_fields' => array('id','name', 'email1', 'email_c'),

                    //Show 10 max results
                    'max_results' => 10,
                    //Do not show deleted
                    'deleted' => 0,

        ); 
*/

        $getDepandencesTable_parameters = array(

             //session id
             'session' => $session_id,
        ); 

      //  $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
        $get_entry_list_result = call("getDepandencesTable", $getDepandencesTable_parameters, $url);
    
	//echo "<pre>";
	//echo 'contact_id = ' . $contact_id;
        //print_r($get_entry_list_result);
        //echo "</pre>";
        header('Content-Type: application/json');
        print($get_entry_list_result);
