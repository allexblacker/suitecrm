<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

include_once('include/generic/SugarWidgets/SugarWidgetSubPanelTopButton.php');

class SugarWidgetSubPanelTopCreateOpportunityButton extends SugarWidgetSubPanelTopButton
{
	function display($defines, $additionalFormFields = null, $nonbutton = false){
		
		require_once('custom/include/dtbc/router.php');
		$value = translate('LBL_CREATE_OPPORTUNITY_BUTTON', 'Opportunities');
		$url = Router::getCreateProjectWithPrefillRoute($_REQUEST['record']);
		$button = "<input type='button' class='button' value='$value' id='btnCreateOpportunity' onClick=\"window.location.replace('$url')\"/>";
		return $button;
	}
}