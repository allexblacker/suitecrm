<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

class SugarWidgetSubPanelTopCustomComposeEmailButton extends SugarWidgetSubPanelTopButton
{     
	var $form_value = '';
    
    public function getWidgetId($buttonSuffix = true) {
    	global $app_strings;
		$this->form_value = $app_strings['LBL_COMPOSE_EMAIL_BUTTON_LABEL'];
    	return parent::getWidgetId();
    }

	public function display($defines, $additionalFormFields = NULL, $nonbutton = false)	{
		if((ACLController::moduleSupportsACL($defines['module'])  && !ACLController::checkAccess($defines['module'], 'edit', true) ||
			$defines['module'] == "Activities" & !ACLController::checkAccess("Emails", 'edit', true))){
			$temp = '';
			return $temp;
		}
		
		global $app_strings,$current_user,$sugar_config,$beanList,$beanFiles;
		$title = $app_strings['LBL_COMPOSE_EMAIL_BUTTON_TITLE'];
		$value = $app_strings['LBL_COMPOSE_EMAIL_BUTTON_LABEL'];
		$parent_type = $defines['focus']->module_dir;
		$parent_id = $defines['focus']->id;

		//martin Bug 19660
		$userPref = $current_user->getPreference('email_link_type');
		$defaultPref = $sugar_config['email_default_client'];
		if($userPref != '') {
			$client = $userPref;
		} else {
			$client = $defaultPref;
		}
		if($client != 'sugar') {
			$bean = $defines['focus'];
			// awu: Not all beans have emailAddress property, we must account for this
			if (isset($bean->emailAddress)){
				$to_addrs = $bean->emailAddress->getPrimaryAddress($bean);
				$button = "<input class='button' type='button'  value='$value'  id='". $this->getWidgetId() . "'  name='".preg_replace('[ ]', '', $value)."'   title='$title' onclick=\"location.href='mailto:$to_addrs';return false;\" />";
			}
			else{
				$button = "<input class='button' type='button'  value='$value'  id='". $this->getWidgetId() ."'  name='".preg_replace('[ ]', '', $value)."'  title='$title' onclick=\"location.href='mailto:';return false;\" />";
			}
		} else {
			//Generate the compose package for the quick create options.
    		$composeData = array("parent_id" => $parent_id, "parent_type"=>$parent_type);
            require_once('modules/Emails/EmailUI.php');
            $eUi = new EmailUI();
			$j_quickComposeOptions = $eUi->generateComposePackageForQuickCreate($composeData, http_build_query($composeData), false, $defines['focus']);
			
			// Prefill contact's email address
			$objValues = json_decode($j_quickComposeOptions);
			$objValues->composePackage->to_email_addrs = $this->getContactsEmail($defines['focus']->contact_created_by_id);
			$j_quickComposeOptions = json_encode($objValues);
			
            $button = "<input title='$title'  id='". $this->getWidgetId()."'  onclick='SUGAR.quickCompose.init($j_quickComposeOptions);' class='button' type='submit' name='".preg_replace('[ ]', '', $value)."_button' value='$value' />";
		}
		return $button;
	}
	
	private function getContactsEmail($contactId) {
		global $db;

		$sql = "SELECT email_addresses.email_address
				FROM email_addr_bean_rel
				JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id
				WHERE bean_module = 'Contacts' AND
				email_addr_bean_rel.deleted = 0 AND
				bean_id = " . $db->quoted($contactId);
				
		$res = $db->fetchOne($sql);
	
		if (!empty($res) && is_array($res) && count($res) > 0)
			return $res['email_address'];
		
		return "";
	}
}
