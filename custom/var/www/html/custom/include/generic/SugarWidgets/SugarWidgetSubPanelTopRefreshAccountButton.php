<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

include_once('include/generic/SugarWidgets/SugarWidgetSubPanelTopButton.php');
require_once("custom/include/dtbc/helpers.php");

class SugarWidgetSubPanelTopRefreshAccountButton extends SugarWidgetSubPanelTopButton
{
	function display($defines, $additionalFormFields = null, $nonbutton = false){
		$value = translate('LBL_SUBPANEL_TOP_REFRESH_BTN', 'dtbc_Year_Alliance_Points');
		$button = "<input type='button' class='button' value='" . $value . "' id='btnRefreshPoints' onClick='dtbc_refreshpoints()'/>";
		return $button;
	}
}