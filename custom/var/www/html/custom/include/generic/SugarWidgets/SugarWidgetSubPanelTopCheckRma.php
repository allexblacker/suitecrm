<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

include_once('include/generic/SugarWidgets/SugarWidgetSubPanelTopButton.php');

class SugarWidgetSubPanelTopCheckRma extends SugarWidgetSubPanelTopButton
{
	function display($defines, $additionalFormFields = null, $nonbutton = false){
		
		require_once('custom/include/dtbc/router.php');
		$value = translate('LBL_CHECK_RMA', 'SN1_Serial_Number');
		$button = "<input type='button' class='button' value='$value' id='btnCreateOpportunity' onClick='checkRma()'/>";
		return $button;
	}
}