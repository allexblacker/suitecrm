<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

include_once('include/generic/SugarWidgets/SugarWidgetSubPanelTopButton.php');
require_once('custom/include/dtbc/helpers.php');

class SugarWidgetSubPanelTopSyncLineItemsButton extends SugarWidgetSubPanelTopButton
{
	function display($defines, $additionalFormFields = null, $nonbutton = false){
		global $mod_strings;
		$quote = BeanFactory::getBean("AOS_Quotes", $_REQUEST['record']);
		$label = $quote->is_synced_c == 1 ? $mod_strings['LBL_SYNC_STOP'] : $mod_strings['LBL_SYNC_START'];
		$button = "<input type='button' class='button' value='" . $label . "' id='btnSync' onClick=\"syncPopup(" . $quote->is_synced_c . ", '" . $_REQUEST['record'] . "')\"/>";
		return $button;
	}
}