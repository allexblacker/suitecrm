<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

include_once('include/generic/SugarWidgets/SugarWidgetSubPanelTopButton.php');
require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/dtbc/router.php');

class SugarWidgetSubPanelTopSelectPriceBookButton extends SugarWidgetSubPanelTopButton
{
	function display($defines, $additionalFormFields = null, $nonbutton = false) {
		$pricebookId = "";
		$focus = BeanFactory::getBean($_REQUEST['module'], $_REQUEST['record']);
		if (!empty($focus->dtbc_pricebook_id_c) && strlen($focus->dtbc_pricebook_id_c) > 0)
			$pricebookId = $focus->dtbc_pricebook_id_c;
		
		$valueTitle = translate('LBL_POPUP_TITLE', 'Opportunities');
		$valuePuBtn = translate('LBL_POPUP_BUTTON', 'Opportunities');
		$valuePuDd = translate('LBL_POPUP_DROPDOWN', 'Opportunities');
		$valuePuWarn = translate('LBL_POPUP_WARNING', 'Opportunities');
		$valueBtn = translate('LBL_SELECT_PRICE_BOOK_BTN', 'Opportunities');
		
		$clickFunction = "pricebookPopup('" . $_REQUEST['record'] . "', '" . $valueTitle . "', '" . $valuePuBtn . "', '" . $valuePuDd . "', '" . $pricebookId . "', '" . $valuePuWarn . "');";
		$button = "<input type='button' class='button' value='" . $valueBtn . "' id='btnCreateOpportunity' onClick=\"" . $clickFunction . "\"/>";
		return $button;
	}
}