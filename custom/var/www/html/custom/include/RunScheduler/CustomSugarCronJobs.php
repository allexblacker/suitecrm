<?php

require_once('include/SugarQueue/SugarCronJobs.php');
require_once('custom/include/RunScheduler/CustomSugarJobQueue.php');

class CustomSugarCronJobs extends SugarCronJobs {
	public function __construct() {
		parent::__construct();
		
		$this->queue = new CustomSugarJobQueue();
	}
	
	public function jobFailing($job = null) {
		$this->jobFailed($job);
	}
}
