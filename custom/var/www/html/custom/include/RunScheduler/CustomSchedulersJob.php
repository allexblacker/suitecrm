<?php

require_once('modules/SchedulersJobs/SchedulersJob.php');

class CustomSchedulersJob extends SchedulersJob {
	public function __construct() {
		parent::__construct();
	}
	
	protected function sudo($user) {
		$GLOBALS['current_user'] = $user;
		
		$_SESSION['is_valid_session'] = true;
		$_SESSION['user_id'] = $user->id;
		$_SESSION['type'] = 'user';
		$_SESSION['authenticated_user_id'] = $user->id;
	}
}
