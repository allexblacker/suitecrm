<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');

class DocumentGenerator {

	private $bean;
	private $templateId;
	private $fromModule;
	private $printPdf = true;

	public function __construct($templateId, $record, $module) { 
		$this->fromModule = $module;
		$this->bean = BeanFactory::getBean($module)->retrieve($record);
		$this->templateId = $templateId;
	}

	public function setGlobalVariables() {
		global $db;
		$GLOBALS['dtbc_total'] = "0";
		
		$sql = "SELECT SUM(total_price_c) AS total
				FROM aos_quotes_dtbc_line_item_1_c
				JOIN dtbc_line_item ON aos_quotes_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id AND dtbc_line_item.deleted = 0
				JOIN dtbc_line_item_cstm ON dtbc_line_item.id = dtbc_line_item_cstm.id_c
				WHERE aos_quotes_dtbc_line_item_1_c.deleted = 0 AND
				aos_quotes_dtbc_line_item_1aos_quotes_ida = " . $db->quoted($this->bean->id);

		$res = $db->fetchOne($sql);

		if (!empty($res) && is_array($res) && count($res) > 0)
			$GLOBALS['dtbc_total'] = $res['total'];
	}

	public function generateDocument() {
		global $sugar_config;
		require_once('modules/DHA_PlantillasDocumentos/MassGenerateDocument.php');
		require_once("modules/DHA_PlantillasDocumentos/Generate_Document.php");
		
		$GD = new Generate_Document($this->fromModule, $this->templateId, array($this->bean->id));
		$DHADocument = BeanFactory::getBean('DHA_PlantillasDocumentos')->retrieve_by_string_fields(array('id' => $GD->plantilla_id));
		if ($DHADocument == null)
			return false;
		$documentsRelationshipName = "aos_quotes_documents_1";
		
		$templateDir = "document_templates/";
		if (isset($sugar_config['DHA_templates_dir']))
			$templateDir = $sugar_config['DHA_templates_dir'];
		
		if(!file_exists($templateDir . $DHADocument->id . '.' . $DHADocument->file_ext)) {
			return;
		}
		
		$GD->enPDF = $this->printPdf;
		$GD->Download = false;
		$GD->CargarPlantilla();
		$GD->ObtenerDatos();
		$GD->GenerarInforme();
		
		if ($GD->Download_filename) { 
			global $app_list_strings, $current_user, $mod_strings;
			require_once('include/utils/sugar_file_utils.php');  
			
			$documentRevision = new DocumentRevision();
			$fileId = $documentRevision->save();
			
			$filePath = $GD->includeTrailingCharacter($sugar_config['upload_dir'], '/') . $fileId;
			copy($GD->Download_filename, $filePath);
			sugar_chmod($filePath);
			
			$document = new Document();
			$genDocName = $this->generateDocumentName($GD->bean_plantilla->name);
			$newDocument = true;
			
			if($this->bean->load_relationship($documentsRelationshipName)) {
				$createdDocs = $this->bean->$documentsRelationshipName->getBeans();
				
				foreach($createdDocs as $createdDoc) {
					if ($createdDoc->document_name == $genDocName) {
						$document = $createdDoc;
						$newDocument = false;
					}
				}
			}
						
			
			$fileExt = ($GD->enPDF) ? 'pdf' : $GD->bean_plantilla->file_ext;
			
			$document->modified_user_id = $current_user->id;
			$document->assigned_user_id = $current_user->id;
			$document->created_by = $current_user->id;
			$document->document_name = $genDocName;
			$document->doc_type = 'Sugar';
			$document->document_type_c = $this->docType;
			$document->document_revision_id = $fileId;
			$docId = $document->save();

			$documentRevision->change_log = 'Document Created';
			$documentRevision->document_id = $docId;
			$documentRevision->doc_type = 'Sugar';
			$documentRevision->created_by = $current_user->id;
			$documentRevision->filename = $genDocName . '.' . $fileExt;
			$documentRevision->file_mime_type = $GD->Download_mimetype;
			$documentRevision->file_ext = $fileExt;
			$documentRevision->revision = 1;
			$documentRevision->save();
			
			if ($newDocument) {
				$this->bean->load_relationship($documentsRelationshipName);
				$this->bean->$documentsRelationshipName->add($docId);
				$this->bean->save();
			}	
			return array('docId' => $document->id, 'fileId' => $fileId);
		}
	}
	
	private function generateDocumentName($originalName) {
		return $originalName;
	}
}