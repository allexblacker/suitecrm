<?php

if(!defined('sugarEntry') || !sugarEntry) 
	die('Not A Valid Entry Point');

class BatterySupplierUpdater{
	
	public function before_save($bean, $event, $arguments){
		
		if($bean->parent_type != "Cases" || $bean->type != "inbound" || $bean->parent_id == null || $bean->parent_id == '')
			return;
		
		$caseBean = BeanFactory::getBean('Cases', $bean->parent_id);
		if(($bean->from_addr == "support@solaredge.com" || strpos($bean->from_addr, "@solaredge") === false) && ($caseBean->escalation_to_tesla_c == "LG" || $caseBean->escalation_to_tesla_c == "Tesla")){
			$bean->escalation_to_tesla_c = '';
			$bean->date_time_battery_escalation_c = date('Y-m-d H:i:s');
		}
	}
}