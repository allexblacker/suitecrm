<?php

class calculatepoints_class {
	
	public function calculatepoints_function(&$bean, $event, $arguments) {
		if (!empty($_REQUEST['module']) && isset($_REQUEST['module']) && $_REQUEST['module'] == "AL1_Alliance_Transaction") {
			global $sugar_config, $db;
			
			$sql = "SELECT accounts_cstm.kw_factor__c
					FROM accounts_al1_alliance_transaction_1_c
					LEFT JOIN accounts ON accounts_al1_alliance_transaction_1accounts_ida = accounts.id AND accounts.deleted = 0
					LEFT JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id
					LEFT JOIN al1_alliance_transaction ON al1_alliance_transaction.id = accounts_al1_alliance_transaction_1al1_alliance_transaction_idb AND al1_alliance_transaction.deleted = 0
					WHERE accounts_al1_alliance_transaction_1_c.deleted = 0 AND
					al1_alliance_transaction.id = " . $db->quoted($bean->id);
					
			$res = $db->fetchOne($sql);
			$kwFactor = !empty($res) && isset($res['kw_factor__c']) ? $res['kw_factor__c'] : $sugar_config['solaredge']['default_kw_factor'];
			$calcPoints = $bean->kw * $kwFactor;
			
			if (!empty($bean->collect_redeem) && $bean->collect_redeem == 2)
				$calcPoints *= -1;
			
			$sql = "UPDATE al1_alliance_transaction 
					SET points = " . $db->quoted($calcPoints) . " 
					WHERE id = " . $db->quoted($bean->id);
					
			$db->query($sql);
		}
	}
	
}
