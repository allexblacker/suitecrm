<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
require_once("custom/include/dtbc/classes/FieldUpdater.php");

class CasesFieldUpdater{
	
	function after_save($bean, $event, $arguments){
		if(empty($bean->acase_id_c) && ($bean->fetched_row['fault_description'] != $bean->fault_description || $bean->fetched_row['family'] != $bean->family || $bean->fetched_row['rma_product'] != $bean->rma_product || $bean->fetched_row['rma'] != $bean->rma || $bean->fetched_row['product_old'] != $bean->product_old) && !empty($_REQUEST['record']))
			return;
		
		$caseBean = BeanFactory::getBean('Cases', $bean->acase_id_c);
		$fieldUpdater = new FieldUpdater();
		$caseBean->countif_portia_rs485_c = $fieldUpdater->getCountIfPortia($caseBean->id);
		$caseBean->rma_lines_in_case_c = $fieldUpdater->getRMALinesInCase($caseBean->id);
		$caseBean->save();
	}
}