<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class loginlogger_class {
	
	function loginlogger_function($event, $arguments) {	
		if (isset($_REQUEST['module']) && isset($_REQUEST['action']) && $_REQUEST['action'] == "MassUpdate") {
			$_REQUEST['dtbc_addresses'] = array();
			$this->getOldGroupsMassUpdate($_REQUEST['mass'], $_REQUEST['module'], $_REQUEST['select_entire_list']);
		}
		
		if (isset($_REQUEST['module']) && isset($_REQUEST['record']) && $_REQUEST['action'] == "Save") {
			$_REQUEST['dtbc_addresses'] = array();
			$this->getOldGroupsBecauseOfEmailSending($_REQUEST['record'], $_REQUEST['module']);
		}
		
		if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'Authenticate' && isset($_REQUEST['Login']) && $_REQUEST['Login'] == 'Log In') {
			$this->logLogins();
			$_REQUEST['alreadyLoggedIn'] = 1;
		}

	}
	
	private function getOldGroupsMassUpdate($recordIds, $recordModule, $isAll) {
		if ($isAll) {
			global $db;
			$sql = "SELECT id FROM " . strtolower($recordModule);
		
			$res = $db->query($sql);
			
			while ($row = $db->fetchByAssoc($res)) {
				$this->getOldGroupsBecauseOfEmailSending($row['id'], $recordModule);
			}
		} else {
			foreach ($recordIds as $recordId) {
				$this->getOldGroupsBecauseOfEmailSending($recordId, $recordModule);
			}
		}
	}
	
	private function getOldGroupsBecauseOfEmailSending($recordId, $recordModule) {
		global $db;
		
		$sql = "SELECT email_addresses.email_address FROM securitygroups_users
				JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = securitygroups_users.user_id AND email_addr_bean_rel.deleted = 0
				JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id AND email_addresses.deleted = 0
				JOIN securitygroups_records ON securitygroups_records.securitygroup_id = securitygroups_users.securitygroup_id AND securitygroups_records.deleted = 0
				WHERE securitygroups_records.module = " . $db->quoted($recordModule) . " AND
				securitygroups_records.record_id = " . $db->quoted($recordId) . " AND
				securitygroups_users.deleted = 0 AND
				email_addr_bean_rel.bean_module = 'Users'";
		
		$res = $db->query($sql);
		
		$_REQUEST['dtbc_addresses'][$recordId] = array();
		
		while ($row = $db->fetchByAssoc($res)) {
			$_REQUEST['dtbc_addresses'][$recordId][] = $row['email_address'];
		}
	}
		
	private function logLogins() {
		if (isset($_REQUEST['alreadyLoggedIn']) && $_REQUEST['alreadyLoggedIn'] == 1)
			return;
		
		global $db;
		$userDetails = $db->fetchOne("select id, first_name, last_name from users where user_name = " . $db->quoted($_REQUEST['user_name']));
		
		$focus = BeanFactory::newBean("dtbc_Login_Logger");
		$focus->login_date_c = date("Y-m-d H:i:s");
		$focus->ip_address_c = $_SERVER['REMOTE_ADDR'];
		$focus->userid_c = $userDetails['id'];
		$focus->user_id_c = $userDetails['id'];
		$focus->username_c = $_REQUEST['user_name'];
		$focus->name = $userDetails['first_name'] . " " . $userDetails['last_name'];
		
		$focus->save();
	}

}