<?php

require_once("custom/include/dtbc/helpers.php");

class UpdateDateEntered {
	public function after_save($bean, $event, $arguments) {
		if (strpos($bean->date_entered, "00:00") !== false) {
			$this->updateDateEnteredField($bean->id);
		}
	}
	
	private function updateDateEnteredField($recId) {
		global $db, $timedate;
		$sql = "UPDATE cases SET date_entered = " . $db->quoted($timedate->getInstance()->nowDb()) . " WHERE id = " . $db->quoted($recId);
		$db->query($sql);
	}
	
}
