<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/EmailSender.php');
require_once('custom/include/dtbc/helpers.php');

class Tier2RegionUpdater{
	
	function before_save_to_eu(&$bean, $event, $arguments){
		
		if(!$this->checkSubStatus($bean))
			return;
			
		$roles = $this->getRoles();
		if(empty($roles))
			return;
		
		if(in_array("support europe", $roles) || in_array("gmbh support manager", $roles)){
			$bean->tier_2_region_c = 1;
			global $sugar_config;
			$emailTo = $sugar_config['solaredge']['workflow']['set_tier_2_region_eu']['recipient_to'];
			$emailCc = $sugar_config['solaredge']['workflow']['set_tier_2_region_eu']['recipient_cc'];
			if(empty($emailTo))
				return;
		
			$this->sendEmail($bean, $emailTo, $emailCc);
		}
	}

	function before_save_to_il(&$bean, $event, $arguments){
		if(!$this->checkSubStatus($bean))
			return;
			
		$roles = $this->getRoles();
		if(empty($roles))
			return;
		
		if(in_array("support admin", $roles) || in_array("support tier 1- il", $roles) || in_array("support tier 2", $roles)){
			$bean->tier_2_region_c = 4;
			global $sugar_config;
			$emailTo = $sugar_config['solaredge']['workflow']['set_tier_2_region_il']['recipient_to'];
			$emailCc = $sugar_config['solaredge']['workflow']['set_tier_2_region_il']['recipient_cc'];
			if(empty($emailTo))
				return;
			
			$this->sendEmail($bean, $emailTo, $emailCc);
		}
	}

	function before_save_to_aus(&$bean, $event, $arguments){
		if(!$this->checkSubStatus($bean))
			return;
			
		$roles = $this->getRoles();
		if(empty($roles))
			return;
		
		if(in_array("support tier 1apac", $roles)){
			$bean->tier_2_region_c = 7;
			global $sugar_config;
			$emailTo = $sugar_config['solaredge']['workflow']['set_tier_2_region_aus']['recipient_to'];
			$emailCc = $sugar_config['solaredge']['workflow']['set_tier_2_region_aus']['recipient_cc'];
			if(empty($emailTo))
				return;
			
			$this->sendEmail($bean, $emailTo, $emailCc);
		}
	}

	function before_save_to_asia(&$bean, $event, $arguments){
		if(!$this->checkSubStatus($bean))
			return;
			
		$roles = $this->getRoles();
		if(empty($roles))
			return;
		
		if(in_array("support tier1japan", $roles)){
			$bean->tier_2_region_c = 8;
			global $sugar_config;
			$emailTo = $sugar_config['solaredge']['workflow']['set_tier_2_region_asia']['recipient_to'];
			$emailCc = $sugar_config['solaredge']['workflow']['set_tier_2_region_asia']['recipient_cc'];
			if(empty($emailTo))
				return;
			
			$this->sendEmail($bean, $emailTo, $emailCc);
		}
	}
	
	private function sendEmail($bean, $emailTo, $emailCc){
		$emailSender = new EmailSender();
		global $sugar_config;
		$emailTemplateId = $sugar_config['solaredge']['workflow']['set_tier_2_region']['email_template_id'];
		$emailSender->sendEmailWithCurrentUser($emailTo, $emailCc, "Cases", $bean->id, $emailTemplateId);
	}

	private function checkSubStatus($bean){
		
		if($bean->case_sub_status_c == 8)
			return true;
		return false;
	}
	
	private function getRoles(){
		global $db, $current_user;
		$sql = "SELECT 
			LOWER(acl_roles.name) AS role_name
		FROM
			acl_roles_users
				INNER JOIN
			acl_roles ON acl_roles_users.role_id = acl_roles.id
				AND acl_roles.deleted = 0
		WHERE
			acl_roles_users.deleted = 0
				AND acl_roles_users.user_id LIKE " . $db->quoted($current_user->id);
		
		$queryResult = $db->query($sql);
		$rolesArray = array();
		while($record = $db->fetchByAssoc($queryResult)){
			array_push($rolesArray, $record['role_name']);
		};
		
		return $rolesArray;
	}
}