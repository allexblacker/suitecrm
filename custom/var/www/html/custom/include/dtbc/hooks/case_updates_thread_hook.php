<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
class CaseUpdatesThreadUpdater{
	
	public function after_save($bean, $event, $argument){
		if(!isset($bean->id) || empty($bean) || empty($bean->case_id))
			return;
		
		global $db;
		$caseBean = BeanFactory::getBean('Cases', $bean->case_id);
		if($bean->internal == 1){
			$img = $this->getImageTag('Blue_envelope.png');
			$caseBean->internal_comment_c = $img;
			$caseBean->new_internal_comment_c = 1;
		}
		else
		{
			if($bean->assigned_user_id != null || !empty($bean->assigned_user_id))
				return;
			
			$img = $this->getImageTag('New_red_envelope.png');
			$sql = "UPDATE cases_cstm
					SET alert_override_c = 'red',
					change_indicator_c = \"$img\"
					WHERE id_c = " . $db->quoted($bean->case_id);
			$db->query($sql);
		}
	}
	
	private function getImageTag($imgName){
		return "<img src='custom/include/dtbc/images/$imgName' height='15' width='25' />";
	}
}