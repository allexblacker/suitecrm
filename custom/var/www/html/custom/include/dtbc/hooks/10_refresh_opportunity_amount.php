<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class RefreshOpportunity {
	public function after_save($bean, $event, $arguments) {
		global $db;
		$sql = "SELECT SUM(total_price_c) AS total_price, opportunities_dtbc_line_item_1opportunities_ida AS opportunity_id
				FROM opportunities_dtbc_line_item_1_c
				JOIN dtbc_line_item_cstm ON opportunities_dtbc_line_item_1dtbc_line_item_idb = id_c
				WHERE opportunities_dtbc_line_item_1opportunities_ida = (
					SELECT opportunities_dtbc_line_item_1opportunities_ida as id
					FROM opportunities_dtbc_line_item_1_c
					WHERE opportunities_dtbc_line_item_1dtbc_line_item_idb = " . $db->quoted($bean->id) . " AND
					deleted = 0) AND
				opportunities_dtbc_line_item_1_c.deleted = 0";
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0 && !empty($res['opportunity_id'])) {
			$opp = BeanFactory::getBean("Opportunities", $res['opportunity_id']);
			$opp->amount = $res['total_price'];
			$opp->save();
		}
	}
	
	public function after_relationship_delete($bean, $event, $arguments) {
		global $db;
		$sql = "SELECT SUM(total_price_c) AS total_price 
				FROM opportunities_dtbc_line_item_1_c
				JOIN dtbc_line_item_cstm ON opportunities_dtbc_line_item_1dtbc_line_item_idb = id_c
				WHERE deleted = 0 AND
				opportunities_dtbc_line_item_1opportunities_ida = " . $db->quoted($bean->id);
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0) {
			$bean->amount = $res['total_price'];
			$bean->save();
		}
	}
}