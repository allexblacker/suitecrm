<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class SubStatudDateUpdater{
	
	function before_save(&$bean, $event, $arguments){
		if($bean->case_sub_status_c != $bean->fetched_row['case_sub_status_c']){
			$bean->sub_status_updated_c = date('Y-m-d');
		}
	}
}