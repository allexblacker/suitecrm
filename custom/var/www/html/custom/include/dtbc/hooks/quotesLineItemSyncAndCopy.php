<?php

require_once("custom/include/dtbc/helpers.php");

class LineItemCopySync {
	
	private $idFieldName = "dtbc_line_item_id_c";
	static $already_ran = false;
	
	public function after_save_from_line_item(&$bean, $event, $arguments) {
		if (empty($_REQUEST['relate_to']))
			return;
		
		if ($_REQUEST['relate_to'] == 'aos_quotes_dtbc_line_item_1')
			$this->copyFromQuoteToOpportunity($bean);
		
		if ($_REQUEST['relate_to'] == 'opportunities_dtbc_line_item_1')
			$this->copyFromOpportunityToQuote($bean);
	}
	
	private function copyFromQuoteToOpportunity($liBean) {
		// Check if quote is syncable
		$qId = $this->getSyncedQuoteId($_REQUEST['relate_id']);
		
		if (empty($liBean->dtbc_line_item_id_c) || strlen($qId) == 0)
			return;
		
		$this->updateLineItemPair($liBean, $liBean->dtbc_line_item_id_c);
	}
	
	private function copyFromOpportunityToQuote($liBean) {
		// Check if quote is syncable
		$qId = $this->getSyncedQuoteId($_REQUEST['relate_id']);
		
		if (strlen($qId) == 0)
			return;
		
		global $db;
		
		// Get line item pair's id
		$sql = "SELECT dtbc_line_item_cstm.id_c
				FROM aos_quotes
				JOIN aos_quotes_dtbc_line_item_1_c ON aos_quotes_dtbc_line_item_1aos_quotes_ida = aos_quotes.id AND aos_quotes_dtbc_line_item_1_c.deleted = 0
				JOIN dtbc_line_item_cstm ON aos_quotes_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item_cstm.id_c
				WHERE aos_quotes.id = " . $db->quoted($qId) . " AND
				dtbc_line_item_cstm.dtbc_line_item_id_c = " . $db->quoted($liBean->id);
				
		$res = $db->fetchOne($sql);

		if (!empty($res) && is_array($res) && count($res) > 0)
			$this->updateLineItemPair($liBean, $res['id_c']);
	}
	
	private function updateLineItemPair($liBean, $pairId) {
		global $db;
		$sql = "UPDATE dtbc_line_item_cstm
				SET 
					dtbc_pricebook_product_id_c = " . $db->quoted($liBean->dtbc_pricebook_product_id_c) . ",
					discount_c = " . $liBean->discount_c . ",
					quantity_c = " . $liBean->quantity_c . ",
					shipment_date_c = " . $db->quoted($liBean->shipment_date_c) . ",
					total_price_c = " . $liBean->total_price_c . ",
					total_kw_c = " . $db->quoted($liBean->total_kw_c) . ",
					unit_price_after_discount_c = " . $db->quoted($liBean->unit_price_after_discount_c) . ",
					sale_type_c = " . $db->quoted($liBean->sale_type_c) . ",
					list_price_c = " . $liBean->list_price_c . ",
					kwforop_c = " . $liBean->kwforop_c . ",
					product_family_from_product_c = " . $db->quoted($liBean->product_family_from_product_c) . "
				WHERE id_c = " . $db->quoted($pairId);
		$db->query($sql);
		$sql = "UPDATE dtbc_line_item
				SET 
					name = " . $db->quoted($liBean->name) . ",
					description = " . $db->quoted($liBean->description) . ",
					date_modified = " . $db->quoted($liBean->date_modified) . ",
					modified_user_id = " . $db->quoted($liBean->modified_user_id) . ",
					assigned_user_id = " . $db->quoted($liBean->assigned_user_id) . "
				WHERE id = " . $db->quoted($pairId);
		$db->query($sql);
	}
	
	public function after_relationship_add_from_opportunity(&$bean, $event, $arguments) {
		if ($_REQUEST['relate_to'] != 'opportunities_dtbc_line_item_1')
			return;
		
		$qId = $this->getSyncedQuoteId($_REQUEST['opportunities_dtbc_line_item_1opportunities_ida']);

		if (strlen($qId) > 0) {
			$this->createLineItemOnQuote($qId, $arguments['related_bean']);
		}
	}
	
	private function getSyncedQuoteId($oppId) {
		global $db;
		
		// Check if there is a syncable quote
		$sql = "SELECT id
				FROM aos_quotes
				JOIN aos_quotes_cstm ON id_c = id
				WHERE deleted = 0 AND
				is_synced_c = 1 AND
				opportunity_id = " . $db->quoted($oppId);
				
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0) {
			return $res['id'];
		}
		
		return "";
	}
	
	private function createLineItemOnQuote($quoteId, $relBean) {
		global $db;
		$newBean = BeanFactory::newBean("dtbc_Line_Item");
		$skipFields = $this->getSkipFields();
		foreach ($newBean->field_defs as $fieldDef) {
			if ((!empty($fieldDef['source']) && $fieldDef['source'] == 'non-db') || in_array($fieldDef['name'], $skipFields))
				continue;
			$newBean->{$fieldDef['name']} = $relBean->{$fieldDef['name']};
		}
		$newBean->{$this->idFieldName} = $relBean->id;
		$newBean->save();
		// Create relation (quote -> line item)
		$sql = "INSERT INTO aos_quotes_dtbc_line_item_1_c
				(id, date_modified, deleted, aos_quotes_dtbc_line_item_1aos_quotes_ida, aos_quotes_dtbc_line_item_1dtbc_line_item_idb)
				VALUES (UUID(), NOW(), 0, " . $db->quoted($quoteId) . ", " . $db->quoted($newBean->id) . ")";
		$db->query($sql);
	}
	
	public function after_relationship_delete_from_opportunity(&$bean, $event, $arguments) {
		// Check if line item specific relation changed
		if ($_REQUEST['linked_field'] != 'opportunities_dtbc_line_item_1')
			return;
		
		global $db;
		
		// Check if there is a syncable quote
		$sql = "SELECT dtbc_line_item_cstm.id_c
				FROM aos_quotes
				JOIN aos_quotes_cstm ON id_c = aos_quotes.id
				LEFT JOIN aos_quotes_dtbc_line_item_1_c ON aos_quotes_dtbc_line_item_1aos_quotes_ida = aos_quotes.id AND aos_quotes_dtbc_line_item_1_c.deleted = 0
				JOIN dtbc_line_item_cstm ON dtbc_line_item_id_c = " . $db->quoted($_REQUEST['linked_id']) . "
				WHERE aos_quotes.deleted = 0 AND
				is_synced_c = 1 AND
				opportunity_id = " . $db->quoted($_REQUEST['record']);
				
		$res = $db->fetchOne($sql);

		if (!empty($res) && is_array($res) && count($res) > 0 && !empty($res['id_c'])) {
			$pair = BeanFactory::getBean("dtbc_Line_Item", $res['id_c']);
			$pair->mark_deleted($pair->id);
			$pair->save();
		}
	}
	
	public function after_relationship_delete_from_quote(&$bean, $event, $arguments) {
		// Check if line item specific relation changed
		if ($_REQUEST['linked_field'] != 'aos_quotes_dtbc_line_item_1')
			return;

		// Check if quote is syncable
		$quote = BeanFactory::getBean("AOS_Quotes", $_REQUEST['record']);
		if ($quote->is_synced_c == 0)
			return;

		// Delete opportunity's line item in case of sync enabled
		$pair = BeanFactory::getBean($arguments['related_bean']->object_name, $arguments['related_bean']->{$this->idFieldName});
		$pair->mark_deleted($pair->id);
		$pair->save();
	}
	
	public function before_save_from_quote(&$bean, $event, $arguments) {
		// Set flag to true if this is a newly created record
		$_REQUEST['isNew'] = empty($_REQUEST['record']);
	}
	
	public function after_save_from_quote(&$bean, $event, $arguments) {
		if(self::$already_ran == true) return;
			self::$already_ran = true;
		
		if ($_REQUEST['isNew'])
			$this->copyLineItemsFromOpportunity($bean->id, $bean->opportunity_id);
	}
	
	private function copyLineItemsFromOpportunity($quoteId, $oppId) {
		global $db;
		
		// Get line items
		$sql = "SELECT dtbc_line_item.name, dtbc_line_item.id, dtbc_line_item_cstm.*, opportunities_cstm.dtbc_pricebook_id_c 
				FROM dtbc_line_item
				LEFT JOIN dtbc_line_item_cstm ON dtbc_line_item_cstm.id_c = dtbc_line_item.id
				JOIN opportunities_dtbc_line_item_1_c ON opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id AND opportunities_dtbc_line_item_1_c.deleted = 0
				LEFT JOIN opportunities_cstm ON opportunities_dtbc_line_item_1opportunities_ida = opportunities_cstm.id_c
				WHERE dtbc_line_item.deleted = 0 AND
				opportunities_dtbc_line_item_1opportunities_ida = " . $db->quoted($oppId);
				
		$res = $db->query($sql);
		$skipFields = $this->getSkipFields();
		
		// Copy line items
		while ($row = $db->fetchByAssoc($res)) {
			$newBean = BeanFactory::newBean("dtbc_Line_Item");
			foreach ($newBean->field_defs as $fieldDef) {
				if ($fieldDef['source'] == 'non-db' || in_array($fieldDef['name'], $skipFields) || empty($row[$fieldDef['name']]))
					continue;
				$newBean->{$fieldDef['name']} = $row[$fieldDef['name']];
			}
			$newBean->{$this->idFieldName} = $row['id'];
			$newBean->save();
			// Create relation (quote -> line item)
			$sql = "INSERT INTO aos_quotes_dtbc_line_item_1_c
					(id, date_modified, deleted, aos_quotes_dtbc_line_item_1aos_quotes_ida, aos_quotes_dtbc_line_item_1dtbc_line_item_idb)
					VALUES (UUID(), NOW(), 0, " . $db->quoted($quoteId) . ", " . $db->quoted($newBean->id) . ")";
			$db->query($sql);
		}
	}
	
	private function getSkipFields() {
		return array(
			"id",
			"date_entered",
			"date_modified",
			"deleted",
		);
	}
}
