<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class OpportunityBackWriter {
	public function before_save_aos_products($bean, $event, $arguments) {
		global $app_list_strings;
		
		$lis = $this->getLinkedDtbcLineItemIds($bean->id);
		
		foreach ($lis as $liId) {
			$focus = BeanFactory::getBean("dtbc_Line_Item", $liId);
			$focus->total_kw_c = $bean->kw_c * $focus->quantity_c; 
			$focus->kwforop_c = $focus->total_kw_c;
			$focus->product_family_from_product_c = $app_list_strings['family_list'][$bean->family_c];
			$focus->save();
		}
		
	}
	
	private function getLinkedDtbcLineItemIds($aospId) {
		global $db;
		$retval = array();
		$sql = "SELECT dtbc_line_item_cstm.id_c
				FROM aos_products_dtbc_pricebook_product_1_c
				JOIN dtbc_pricebook_product ON aos_products_dtbc_pricebook_product_1dtbc_pricebook_product_idb = dtbc_pricebook_product.id AND dtbc_pricebook_product.deleted = 0
				JOIN dtbc_line_item_cstm ON dtbc_line_item_cstm.dtbc_pricebook_product_id_c = aos_products_dtbc_pricebook_product_1dtbc_pricebook_product_idb
				JOIN dtbc_line_item ON dtbc_line_item.id = dtbc_line_item_cstm.id_c AND dtbc_line_item.deleted
				WHERE aos_products_dtbc_pricebook_product_1aos_products_ida = " . $db->quoted($aospId) . " AND
				aos_products_dtbc_pricebook_product_1_c.deleted = 0";
		$res = $db->query($sql);
		while ($row = $db->fetchByAssoc($res)) {
			if (!empty($row['id_c']))
				$retval[] = $row['id_c'];
		}
		return $retval;
	}
	
}