<?php

if(!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

class Tier2AssigneeEmailAddressUpdater{
	
	function before_save(&$bean, $event, $arguments){
		global $app_list_strings;
		$assignee = $bean->tier_2_assignee_email_c;
		
		if(!isset($app_list_strings['tier_2_assignee_email_list'][$assignee]))
			return;
		
		$bean->tier_2_assigneee_email_c = $app_list_strings['tier_2_assignee_email_list'][$assignee];
	}
}