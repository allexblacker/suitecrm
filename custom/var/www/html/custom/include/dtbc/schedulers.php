<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/EmailSender.php");
require_once("custom/include/dtbc/helpers.php");
require_once("custom/include/dtbc/yearlypoints.php");
require_once("include/utils.php");
require_once("custom/include/dtbc/SystemWorkflows.php");

class DtbcSchedulers {
	
	public function systemWorkflows() {
		$mod = new SystemWorkflows();
		return $mod->run();
	}
	
	public function calculateYearlyPoints($customWhere = "", $withoutUpdate = false) {
		global $db;
		// Calculate expiration date, total points and gift values
		
		// Total points by Accounts by years
		$sql = "SELECT accounts_al1_alliance_transaction_1accounts_ida as a_id, 
					SUM(CASE WHEN collect_redeem = 1 THEN points END) AS sum_total, 
					SUM(CASE WHEN collect_redeem = 2 THEN points END) AS sum_gift,
                    SUM(kw) AS sum_kw,
					YEAR(date1) AS sum_year
				FROM al1_alliance_transaction
				JOIN accounts_al1_alliance_transaction_1_c ON accounts_al1_alliance_transaction_1al1_alliance_transaction_idb = al1_alliance_transaction.id AND accounts_al1_alliance_transaction_1_c.deleted = 0
				WHERE al1_alliance_transaction.deleted = 0 " . $customWhere . " 
				GROUP BY accounts_al1_alliance_transaction_1accounts_ida, YEAR(date1)
				ORDER BY accounts_al1_alliance_transaction_1accounts_ida, YEAR(date1) ASC";
		
		$res = $db->query($sql);
		$today = date("Y-m-d");
		$data = array();
		while ($row = $db->fetchByAssoc($res)) {
			// Calculate yearly values
			$expirationDate = DtbcHelpers::getYearAllianceExpirationDate($row['sum_year']);
			$availablePoints = DtbcHelpers::isFirstGreaterDbStrDate($expirationDate, $today) ? $row['sum_total'] - $row['sum_gift'] : 0;
			$expiredPoints = DtbcHelpers::isFirstGreaterDbStrDate($today, $expirationDate, true) ? $availablePoints - $row['sum_gift'] : 0;
			// Update or insert yearly values
			$giftValue = $row['sum_gift'] != null && !empty($row['sum_gift']) ? $row['sum_gift'] : 0;
			if (!$withoutUpdate)
				$this->updateOrInsertYearlyValues($row, $expirationDate, $availablePoints, $expiredPoints, $giftValue);
			
			if (empty($data[$row['a_id']]) && DtbcHelpers::isFirstGreaterDbStrDate($expirationDate, $today))
				$data[$row['a_id']] = new YearlyPoints();
			
			if (DtbcHelpers::isFirstGreaterDbStrDate($expirationDate, $today)) {
				$data[$row['a_id']]->addData($row['sum_year'], $row['sum_total'], $row['sum_gift'], $expirationDate, $row);
			}
		}

		$this->calculateValues($data, $withoutUpdate);
		
		if ($withoutUpdate)
			return $data;
		
		return true;
	}
	
	private function updateTimeStamp($recordId) {
		global $db;
		$sql = "UPDATE accounts_cstm SET timestamp_recalculate_c = NOW() WHERE id_c = " . $db->quoted($recordId);
		$db->query($sql);
	}
	
	private function calculateValues($data, $withoutUpdate) {
		foreach ($data as $a_id => $years) {
			foreach ($years->getPoints() as $calcPoints) {
				$giftValue = $calcPoints['row']['sum_total'] != null && !empty($calcPoints['row']['sum_total']) ? $calcPoints['row']['sum_total'] - $calcPoints['total'] - $calcPoints['gift'] : 0;
				if (!$withoutUpdate)
					$this->updateOrInsertYearlyValues($calcPoints['row'], $calcPoints['expirationDate'], $calcPoints['total'] - $calcPoints['gift'], 0, $giftValue);
			}
		}
	}
	
	private function updateOrInsertYearlyValues($row, $expirationDate, $availablePoints, $expiredPoints, $giftValue) {
		global $db;
		$sql = "SELECT dtbc_year_alliance_points.id AS id
				FROM dtbc_year_alliance_points
				JOIN dtbc_year_alliance_points_cstm ON dtbc_year_alliance_points.id = id_c
				JOIN accounts_dtbc_year_alliance_points_1_c ON accounts_dabb8_points_idb = dtbc_year_alliance_points.id AND accounts_dtbc_year_alliance_points_1_c.deleted = 0
				WHERE dtbc_year_alliance_points.deleted = 0 AND
				accounts_dtbc_year_alliance_points_1accounts_ida = " . $db->quoted($row['a_id']) . " AND 
				year_c = " . $db->quoted($row['sum_year']);
		
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0) {
			// Update
			$sql = "UPDATE dtbc_year_alliance_points_cstm SET 
						available_points_c = " . $db->quoted($availablePoints) . ",
						date_expiration_c = " . $db->quoted($expirationDate) . ", 
						expired_points_c = " . $db->quoted($expiredPoints) . ",
						gift_value_c = " . $giftValue . ",
						kw_installed_c = " . $db->quoted($row['sum_kw']) . ",
						total_points_c = " . $db->quoted($row['sum_total']) . " 
						WHERE id_c = " . $db->quoted($res['id']);
			$db->query($sql);
		} else {
			// Insert
			$newGuid = create_guid();
			$sql = "INSERT INTO dtbc_year_alliance_points (
							id, 
							date_entered, 
							date_modified, 
							deleted,
							modified_user_id,
							created_by,
							name,
							assigned_user_id,
							description) 
					VALUES (" . $db->quoted($newGuid) . ", 
							NOW(), 
							NOW(), 
							0,
							1,
							1,
							" . $db->quoted($row['sum_year']) . ",
							1,
							" . $db->quoted("") . ")";
			$db->query($sql);
			$sql = "INSERT INTO dtbc_year_alliance_points_cstm (
							id_c, 
							available_points_c, 
							date_expiration_c, 
							expired_points_c,
							gift_value_c,
							kw_installed_c,
							total_points_c,
							year_c) 
					VALUES (" . $db->quoted($newGuid) . ", 
							" . $db->quoted($availablePoints) . ", 
							" . $db->quoted($expirationDate) . ", 
							" . $db->quoted($expiredPoints) . ", 
							" . $db->quoted($giftValue) . ", 
							" . $db->quoted($row['sum_kw']) . ", 
							" . $db->quoted($this->getValidNumber($row['sum_total'])) . ", 
							" . $db->quoted($row['sum_year']) . ")";
			$db->query($sql);
			// Set relation
			$sql = "INSERT INTO accounts_dtbc_year_alliance_points_1_c (
							id, 
							date_modified, 
							deleted, 
							accounts_dtbc_year_alliance_points_1accounts_ida,
							accounts_dabb8_points_idb) 
					VALUES (UUID(), 
							NOW(), 
							0, 
							" . $db->quoted($row['a_id']) . ", 
							" . $db->quoted($newGuid) . ")";
			$db->query($sql);
		}
		
		$this->updateTimeStamp($row['a_id']);
	}
	
	private function getValidNumber($value) {
		return strlen($value) == 0 ? 0 : $value;
	}
	
	public function emailToCase() {
		require_once 'custom/modules/InboundEmail/CustomAOPInboundEmail.php';
		$GLOBALS['log']->info('----->Scheduler fired job of type pollMonitoredInboxesAOP()');
		global $dictionary;
		global $app_strings;
		global $sugar_config;

		require_once('modules/Configurator/Configurator.php');
		require_once('modules/Emails/EmailUI.php');

		$ie = new CustomAOPInboundEmail();
		$emailUI = new EmailUI();
		$r = $ie->db->query('SELECT id, name FROM inbound_email WHERE is_personal = 0 AND deleted=0 AND status=\'Active\' AND mailbox_type != \'bounce\'');
		$GLOBALS['log']->debug('Just got Result from get all Inbounds of Inbound Emails');

		while ($a = $ie->db->fetchByAssoc($r)) {
			$GLOBALS['log']->debug('In while loop of Inbound Emails');
			$ieX = new CustomAOPInboundEmail();
			$ieX->retrieve($a['id']);
			$mailboxes = $ieX->mailboxarray;
			foreach ($mailboxes as $mbox) {
				$ieX->mailbox = $mbox;
				$newMsgs = array();
				$msgNoToUIDL = array();
				$connectToMailServer = false;
				if ($ieX->isPop3Protocol()) {
					$msgNoToUIDL = $ieX->getPop3NewMessagesToDownloadForCron();
					// get all the keys which are msgnos;
					$newMsgs = array_keys($msgNoToUIDL);
				}
				if ($ieX->connectMailserver() == 'true') {
					$connectToMailServer = true;
				} // if

				$GLOBALS['log']->debug('Trying to connect to mailserver for [ ' . $a['name'] . ' ]');
				if ($connectToMailServer) {
					$GLOBALS['log']->debug('Connected to mailserver');
					if (!$ieX->isPop3Protocol()) {
						$newMsgs = $ieX->getNewMessageIds();
					}
					if (is_array($newMsgs)) {
						$current = 1;
						$total = count($newMsgs);
						require_once("include/SugarFolders/SugarFolders.php");
						$sugarFolder = new SugarFolder();
						$groupFolderId = $ieX->groupfolder_id;
						$isGroupFolderExists = false;
						$users = array();
						if ($groupFolderId != null && $groupFolderId != "") {
							$sugarFolder->retrieve($groupFolderId);
							$isGroupFolderExists = true;
						} // if
						$messagesToDelete = array();
						if ($ieX->isMailBoxTypeCreateCase()) {
							require_once 'modules/AOP_Case_Updates/AOPAssignManager.php';
							$assignManager = new AOPAssignManager($ieX);
						}
						foreach ($newMsgs as $k => $msgNo) {
							$uid = $msgNo;
							if ($ieX->isPop3Protocol()) {
								$uid = $msgNoToUIDL[$msgNo];
							} else {
								$uid = imap_uid($ieX->conn, $msgNo);
							} // else
							if ($isGroupFolderExists) {
								if ($ieX->importOneEmail($msgNo, $uid)) {
									// add to folder
									$sugarFolder->addBean($ieX->email);
									if ($ieX->isPop3Protocol()) {
										$messagesToDelete[] = $msgNo;
									} else {
										$messagesToDelete[] = $uid;
									}
									if ($ieX->isMailBoxTypeCreateCase()) {
										$userId = $assignManager->getNextAssignedUser();
										$GLOBALS['log']->debug('userId [ ' . $userId . ' ]');
										$ieX->handleCreateCase($ieX->email, $userId);
									} // if
								} // if
							} else {
								if ($ieX->isAutoImport()) {
									$ieX->importOneEmail($msgNo, $uid);
								} else {
									/*If the group folder doesn't exist then download only those messages
									 which has caseid in message*/

									$ieX->getMessagesInEmailCache($msgNo, $uid);
									$email = new Email();
									$header = imap_headerinfo($ieX->conn, $msgNo);
									$email->name = $ieX->handleMimeHeaderDecode($header->subject);
									$email->from_addr = $ieX->convertImapToSugarEmailAddress($header->from);
									$email->reply_to_email = $ieX->convertImapToSugarEmailAddress($header->reply_to);
									if (!empty($email->reply_to_email)) {
										$contactAddr = $email->reply_to_email;
									} else {
										$contactAddr = $email->from_addr;
									}
									$mailBoxType = $ieX->mailbox_type;
									$ieX->handleAutoresponse($email, $contactAddr);
								} // else
							} // else
							$GLOBALS['log']->debug('***** On message [ ' . $current . ' of ' . $total . ' ] *****');
							$current++;
						} // foreach
						// update Inbound Account with last robin

					} // if
					if ($isGroupFolderExists) {
						$leaveMessagesOnMailServer = $ieX->get_stored_options("leaveMessagesOnMailServer", 0);
						if (!$leaveMessagesOnMailServer) {
							if ($ieX->isPop3Protocol()) {
								$ieX->deleteMessageOnMailServerForPop3(implode(",", $messagesToDelete));
							} else {
								$ieX->deleteMessageOnMailServer(implode($app_strings['LBL_EMAIL_DELIMITER'], $messagesToDelete));
							}
						}
					}
				} else {
					$GLOBALS['log']->fatal("SCHEDULERS: could not get an IMAP connection resource for ID [ {$a['id']} ]. Skipping mailbox [ {$a['name']} ].");
					// cn: bug 9171 - continue while
				} // else
			} // foreach
			imap_expunge($ieX->conn);
			imap_close($ieX->conn, CL_EXPUNGE);
		} // while
		return true;
	}
	
	public function caseStatuses($schedulerType) {
		global $db;
	
		$sql = "SELECT cases.*, cases_cstm.* 
				FROM cases 
				JOIN cases_cstm ON cases_cstm.id_c = cases.id 
				JOIN cases_dtbc_statuslog_1_c ON cases_dtbc_statuslog_1cases_ida = cases.id AND cases_dtbc_statuslog_1_c.deleted = 0 
				JOIN dtbc_statuslog ON dtbc_statuslog.id = cases_dtbc_statuslog_1dtbc_statuslog_idb AND dtbc_statuslog.deleted = 0 
				JOIN dtbc_statuslog_cstm ON dtbc_statuslog_cstm.id_c = dtbc_statuslog.id 
				WHERE cases.deleted = 0 AND 
				" . $this->getPartialSql($schedulerType) . "
				(dtbc_statuslog_cstm.date_end_c IS NULL OR 
				dtbc_statuslog_cstm.date_end_c = '') AND 
				" . $this->getFieldName($schedulerType) . " = 0";

		$res = $db->query($sql);
		
		while ($row = $db->fetchByAssoc($res)) {
			if (empty($row['id']))
				continue;
			
			switch ($schedulerType) {
				case "case_job_3days": $this->customerNotifier3days($row['id']);
				case "case_job_5days": $this->customerNotifier5days($row['id']);
			}
			$this->updateSentMailCheckbox($this->getFieldName($schedulerType), $row['id']);
		}
		return true;
	}
	
	public function customerNotifier2days() {
		global $db;
		
		$sql = "SELECT * 
				FROM cases
				JOIN cases_cstm ON id = id_c
				WHERE cases.deleted = 0 AND
				cases.dtbc_2days_noti_sent_c = 0 AND
                cases_cstm.case_sub_status_c IN (23, 24) AND
				TIMESTAMPDIFF(DAY, cases.last_sub_status_change, NOW()) = 2";
		
		$res = $db->query($sql);
		while ($row = $db->fetchByAssoc($res)) {
			$bean = BeanFactory::getBean("Cases", $row['id']);
			$bean->case_status_c = "7"; // Closed
			$bean->do_not_send_survey_c = "0";
			$bean->resolution = "No response from Customer";
			$fieldName = $this->getFieldName("case_job_2days");
			$bean->$fieldName = 1;
			$bean->save();

			global $sugar_config;
			$emailSender = new EmailSender();
			$emailAddress = $emailSender->getEmailAddresFromCase($bean);
			$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $row['id'], $sugar_config['solaredge']['pending_2days_notifier_email_id']);
		}
		return true;
	}
	
	private function customerNotifier3days($caseId) {
		global $sugar_config;
		$bean = BeanFactory::getBean("Cases", $caseId);
		$emailSender = new EmailSender();
		$emailAddress = $emailSender->getEmailAddresFromCase($bean);
		$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $caseId, $sugar_config['solaredge']['pending_3days_notifier_email_id']);
	}
	
	private function customerNotifier5days($caseId) {
		$bean = BeanFactory::getBean("Cases", $caseId);
		$bean->case_status_c = "18"; // Customer Response Delayed
		$bean->case_sub_status_c = "22"; // Pending Agent Call
		$bean->save();
		
		global $sugar_config;
		$bean = BeanFactory::getBean("Cases", $caseId);
		$emailSender = new EmailSender();
		$emailAddress = $emailSender->getEmailAddresFromCase($bean);
		$emailSender->sendEmailWoCustomizedValues($emailAddress, "Cases", $caseId, $sugar_config['solaredge']['pending_5days_notifier_email_id']);
	}
	
	private function getPartialSql($schedulerType) {
		switch ($schedulerType) {
			case "case_job_3days": return "dtbc_statuslog_cstm.case_status_c = '3' AND TIMESTAMPDIFF(DAY, dtbc_statuslog_cstm.date_start_c, NOW()) = 3 AND ";
			case "case_job_5days": return "dtbc_statuslog_cstm.case_status_c = '3' AND TIMESTAMPDIFF(DAY, dtbc_statuslog_cstm.date_start_c, NOW()) = 5 AND ";
		}
	}
	
	private function updateSentMailCheckbox($fieldName, $caseId) {
		global $db;
		
		$sql = "UPDATE cases 
				SET " . $fieldName . " = 1 
				WHERE id = " . $db->quoted($caseId);
		
		$db->query($sql);
	}
	
	private function getFieldName($schedulerType) {
		switch ($schedulerType) {
			case "case_job_2days": return "dtbc_2days_noti_sent_c";
			case "case_job_3days": return "dtbc_3days_noti_sent_c";
			case "case_job_5days": return "dtbc_5days_noti_sent_c";
		}
	}

}