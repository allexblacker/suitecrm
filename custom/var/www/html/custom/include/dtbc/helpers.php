<?php

// require_once("custom/include/dtbc/helpers.php");
require_once("modules/AOW_WorkFlow/aow_utils.php");

class DtbcHelpers {
	public static function logger($value, $fileName = "dtbc.txt") {
		file_put_contents($fileName, "[" . date("Y-m-d H:i:s") . "] " . print_r($value, true) . "\r\n", FILE_APPEND | LOCK_EX);
	}
	
	public static function getYearAllianceExpirationDate($year, $useEmailDateFormat = false) {
		$retYear = intval($year) + 2;
		
		if ($useEmailDateFormat)
			return "01/06/" . $retYear;
		
		return $retYear . "-06-01";
	}
	
	public static function localDateTime($dbDate) {
		global $current_user;
		$timeDate = new TimeDate();
		return $timeDate->to_display_date_time($dbDate, true, true, $current_user);
	}
	
	public static function getUserTimeFormat(){
		global $timedate, $current_user;
		return $timedate->get_time_format($current_user);
	}
	
	public static function getDateInterval($dateBegin, $dateEnd, $format){
		if(empty($dateBegin) || empty($dateEnd) || empty($format))
			return;
		
		$userDateFormat = self::getUserDateFormat();
		$userTimeFormat = self::getUserTimeFormat();
		$dateBegin = DateTime::createFromFormat($userDateFormat . ' ' . $userTimeFormat, $dateBegin);
		if($dateBegin === false)
			return;
		$dateEnd = DateTime::createFromFormat($userDateFormat . ' ' . $userTimeFormat, $dateEnd);
		if($dateEnd === false)
			return;
		
		$diff = $dateBegin->diff($dateEnd);
		return $diff->format($format);
	}
	
	public static function localDate($dbDate) {
		global $current_user;
		$timeDate = new TimeDate();
		return $timeDate->to_display_date($dbDate, true, true, $current_user);
	}
	
	public static function isFirstGreaterDbStrDate($strDbDate1, $strDbDate2, $equalsIsApproved = false) {
		if ($strDbDate1 != null && $strDbDate2 != null && !empty($strDbDate1) && !empty($strDbDate2)) {
			$d1 = new DateTime($strDbDate1);
			$d2 = new DateTime($strDbDate2);
			
			if ($equalsIsApproved)
				return $d1 >= $d2;
			
			return $d1 > $d2;
		}
		
		return false;
	}
	
	public static function getDateBasedOnUserSettings($date){
		global $timedate;
		return $timedate->asUserDate(new DateTime($date));
	}
	
	public static function getDateTimeBasedOnUserSettings($dateTime){
		global $timedate;
		return self::getDateBasedOnUserSettings($dateTime) . ' ' . $timedate->asUserTime(new DateTime($dateTime));
	}
	
	public static function getUserDateFormat(){
		global $timedate, $current_user;
		return $timedate->get_date_format($current_user);
	}
	
	public static function getDatePart($dateFormat,$date,$part){
		$dateParse = date_parse_from_format($dateFormat,$date);
		return $dateParse[$part];
	}
	
	public static function getImageTagForFormulaFields($image,$alt,$format,$link = false,$height = 36, $width = 36){
		if(!$link)
		{
			if($format)
				return '<img src="custom/include/dtbc/images/' . $image. '" height="' . $height . '" width="' . $width . '" alt="' . $alt. '">';
			else
				return '<img src="custom/include/dtbc/images/' . $image . '" alt="' . $alt . '">';
		}
		
		$image = str_replace(' ','%20',$image);
		return '<img src="' . $image . '" alt="' . $alt . '">';
	}
	
	public static function getHyperLinkForFormulaFields($link,$text){
		$link = str_replace(' ','%20',$link);
		return '<a href="' . $link . '" target="_blank">' . $text. '</a>'; 
	}

	public static function getRelatedBeansForEmails($pType, $pId) {
		global $beanList;
		$object_arr = array();
		$object_arr[$pType] = $pId;
		$mod = new $beanList[$pType]();
		foreach ($mod->field_defs as $bean_arr) {
			if ($bean_arr['type'] == 'relate') {
				if (isset($bean_arr['module']) &&  $bean_arr['module'] != '' && isset($bean_arr['id_name']) &&  $bean_arr['id_name'] != '' && $bean_arr['module'] != 'EmailAddress') {
					$idName = $bean_arr['id_name'];
					if (isset($mod->field_defs[$idName]) && $mod->field_defs[$idName]['source'] != 'non-db') {
						if (!isset($object_arr[$bean_arr['module']])) 
							$object_arr[$bean_arr['module']] = $mod->$idName;
					}
				}
			}
			else if ($bean_arr['type'] == 'link') {
				if (!isset($bean_arr['module']) || $bean_arr['module'] == '') $bean_arr['module'] = getRelatedModule($mod->module_dir, $bean_arr['name']);
				if (isset($bean_arr['module']) &&  $bean_arr['module'] != ''&& !isset($object_arr[$bean_arr['module']]) && $bean_arr['module'] != 'EmailAddress') {
					$linkedBeans = $mod->get_linked_beans($bean_arr['name'], $bean_arr['module'], array(), 0, 1);
					if ($linkedBeans) {
						$linkedBean = $linkedBeans[0];
						if (!isset($object_arr[$linkedBean->module_dir])) $object_arr[$linkedBean->module_dir] = $linkedBean->id;
					}
				}
			}
		}
		return $object_arr;
	}
	
	public static function validateEmailAddressArray($emailAddressesArray) {
		if (empty($emailAddressesArray))
			return $emailAddressesArray;
		
		$retval = array();
		
		foreach ($emailAddressesArray as $email) {
			if (filter_var($email, FILTER_VALIDATE_EMAIL))
				$retval[] = $email;
        }
		
		return $retval;
	}
}
