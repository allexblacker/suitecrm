<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class TplCalculator {
	
	public static function getOneDimensionView() {
		$cacheFileName = $_REQUEST['action'] == "DetailView" ? "DetailView.tpl" : "EditView.tpl";
		$defFileName = $_REQUEST['action'] == "DetailView" ? "detailviewdefs.php" : "editviewdefs.php";

		global $sugar_config;
		
		// Remove cache
		$cacheFileName = "cache/themes/" . $GLOBALS['theme'] . "/modules/" . $_REQUEST['module'] . "/" . $cacheFileName;
		if (file_exists($cacheFileName))
			unlink($cacheFileName);
				
		// Get Custom TPL based on the custom layout logic
        return self::getMetadata($_REQUEST['module'], $sugar_config['solaredge']['layout_config'][$_REQUEST['module']], $defFileName);
	}
	
	public static function getMetadata($module, $recordType, $fileName) {
		global $db, $current_user, $sugar_config;
		$retval = null;
		
		if ($recordType == 'rmaclosecase') {
			// Close Case "mode"
			$retval = $sugar_config['solaredge']['custom_views']['rmaclosecase'] . $fileName;
		} else if ($recordType == 'closecase') {
			// Close Case "mode"
			$retval = $sugar_config['solaredge']['custom_views']['closecase'] . $fileName;
		} else if ($recordType == 'spcase') {
			// Service Partner "mode"
			$retval = $sugar_config['solaredge']['custom_views']['spcase'] . $fileName;
		} else if ($recordType == 'fieldengineer') {
			// Field engineer "mode"
			$retval = $sugar_config['solaredge']['custom_views']['fieldengineer'] . $fileName;
		} else if ($recordType == 'newrmacreation') {
			// New RMA "mode"
			$retval = $sugar_config['solaredge']['custom_views']['newrmacreation'] . $fileName;
		} else {
			// Normal "mode"
			$sql = "SELECT dtbc_layouts.id FROM dtbc_layouts
											JOIN dtbc_layouts_roles ON dtbc_layouts_roles.layout_id = dtbc_layouts.id AND dtbc_layouts_roles.deleted = 0
											WHERE dtbc_layouts.deleted = 0 AND
											dtbc_layouts.module_name = " . $db->quoted($module) . " AND 
											dtbc_layouts_roles.bean_variable_value = " . $db->quoted($recordType) . " AND 
											dtbc_layouts_roles.role_id = (SELECT role_id FROM acl_roles_users WHERE user_id = " . $db->quoted($current_user->id) . " AND deleted = 0 LIMIT 0,1)";
			
			$layoutSettings = $db->fetchOne($sql);

			if (!empty($layoutSettings) && isset($layoutSettings['id']) && strlen($layoutSettings['id']) > 0) {
				if (file_exists('custom/modules/' . $module . '/metadata/' . $layoutSettings['id'] . '/' . $fileName)) {
					$retval = 'custom/modules/' . $module . '/metadata/' . $layoutSettings['id'] . '/' . $fileName;
				}
			}
		}

		return $retval;
	}
	
	public static function getRecordType($param1, $param2) {
		if (!empty($param1) && isset($param1) && strlen($param1) > 0)
			return $param1;
		
		if (!empty($param2) && isset($param2) && strlen($param2) > 0)
			return $param2;
		
		return "";
	}
}