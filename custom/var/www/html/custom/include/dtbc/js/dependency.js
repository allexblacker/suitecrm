$(function() {

if(action_sugar_grp1 == 'EditView'){
	jQuery.getScript('custom/include/dtbc/js/addToDropDown.js');

	getDependeciesOfModule();
	var changeFun = function(that){
		$.ajax({
			method: 'POST',
			url: 'index.php?module=Administration&action=getExistingSettings',
			data: {
				fieldController: that.id,
				moduleName: module_sugar_grp1,
				controllerValue: that.value,
			} 
		}).done(function (data) {
			if(data != ''){
				var values = JSON.parse(data);
				$.each(values, function(item, element){
					var dropDown = $("#" + item);
					var dropDownDefaultValue = dropDown.val();
					var options = [];
					dropDown.find("option").each(function(key, item){
							options.push(item.value);
					});

					var deleted = [];
					$.each(element, function(key, object){
						if(object.list != '' && object.list != undefined){
							if(jQuery.inArray(object.value, options) == -1){
								addToDropDown(dropDown,object.value,SUGAR.language.languages.app_list_strings[object.list][object.value]);
							}
							var index = options.indexOf(object.value);
							if(index != -1)
								options.splice(index, 1);
						}
					});

					$.each(options, function(key, value){
						dropDown.find("option[value='" + value + "']").remove();
					});

					if(dropDownDefaultValue != dropDown.val())
						dropDown.trigger('change');
				});
			}
		}).fail(function (xhr) {
			console.log(xhr);
		});
	};

	$("select").change(function () {changeFun(this)});
}
});

function getDependeciesOfModule(){
	$.ajax({
			method: 'POST',
			url: 'index.php?module=Administration&action=getDependenciesOfModule',
			data: {
				moduleName: module_sugar_grp1,
			} 
	}).done(function (dataJSON){
		if(dataJSON == '')
			return;
		dependencies = JSON.parse(dataJSON);
		setAvailableOptions(dependencies);
	}).fail(function (xhr){
		console.log(xhr);
	});
}

function getDistinctDropDownLists(dependencies){
	var flagsController = [], flagsControlled = [], outputController = [], outputControlled = [], l = dependencies.length, i;
	for( i=0; i<l; i++) {
    	if(flagsController[dependencies[i].controller] != true && $("#" + dependencies[i].controller).length){
    		flagsController[dependencies[i].controller] = true;
    		outputController.push(dependencies[i].controller);
		}
		
		if(flagsControlled[dependencies[i].controlled] && $("#" + dependencies[i].controlled).length) 
			continue;
    	flagsControlled[dependencies[i].controlled] = true;
    	outputControlled.push(dependencies[i].controlled);
	}
	
	var output = [];
	output[0] = outputController;
	output[1] = outputControlled;
	return output;
}

function setAvailableOptions(dependencies){

	var dropDownDefaultValues = {};
	$("select").each(function(){
		dropDownDefaultValues[this.id] = this.value;
	});
	
	var distinctDropDownLists = getDistinctDropDownLists(dependencies);
	var distinctControllers = distinctDropDownLists[0];
	var distinctControlledList = distinctDropDownLists[1];
	
	var dropDown = ";"
	for(i = 0; i < distinctControlledList.length; i++){
		dropDown = $("#" + distinctControlledList[i]);
		dropDown.find('option').remove();
	}
	
	
	for(i = 0; i < distinctControllers.length; i++){
		var controller = distinctControllers[i];
		var defaultValue = dropDownDefaultValues[controller];
		
		var currentDependencies = dependencies.filter(function(item){
									return item.controller == controller &&
											item.controller_value == defaultValue;
								});
		
			
		$.each(currentDependencies, function(key, value){
			dropDown = $("#" + value.controlled);

			if(dropDown.length){
				addToDropDown(dropDown,value.controlled_value,SUGAR.language.languages.app_list_strings[value.controlled_list_name][value.controlled_value]);
			}
		});
	}
	
	$.each(dropDownDefaultValues,function(key,value){
		dropDown = $("#" + key);
		if(dropDown.length)
			$("#" + key).val(value);
	});
}