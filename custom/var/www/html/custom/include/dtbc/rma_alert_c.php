<?php

require_once("custom/include/dtbc/helpers.php");

class RmaAlertFunctionField {
	
	private $bean;
	
	public function __construct($bean) {
		$this->bean = $bean;
	}
	
	public function run() {
		if (!$this->isCasesSubPanel())
			return "";
		
		if (empty($this->bean))
			return "";
			
		$additionalQuery = "";
		
		if (strlen($this->bean->serial_number_new) > 0)
			$additionalQuery = $this->concatenate($additionalQuery, " (serial_number_new <> NULL && serial_number_new <> '' && serial_number_new = " . $this->bean->db->quoted($this->bean->serial_number_new) . ") ");
		
		if (strlen($this->bean->name) > 0)
			$additionalQuery = $this->concatenate($additionalQuery, " (sn1_serial_number.name IS NOT NULL && sn1_serial_number.name <> '' && sn1_serial_number.name = " . $this->bean->db->quoted($this->bean->name) . ") ");
		
		if (strlen($this->bean->product_old) > 0)
			$additionalQuery = $this->concatenate($additionalQuery, " (product_old IS NOT NULL && product_old <> '' && product_old = " . $this->bean->db->quoted($this->bean->product_old) . ") ");
		
		if (strlen($this->bean->product_new) > 0)
			$additionalQuery = $this->concatenate($additionalQuery, " (product_new IS NOT NULL && product_new <> '' && product_new = " . $this->bean->db->quoted($this->bean->product_new) . ") ");
		
		if (strlen($additionalQuery) == 0)
			return "";
		
		$additionalQuery = "(" . $additionalQuery . ") AND";
		
		$sql = "SELECT cases.case_number, cases.id 
				FROM cases_sn1_serial_number_1_c 
				JOIN sn1_serial_number ON cases_sn1_serial_number_1sn1_serial_number_idb = sn1_serial_number.id AND sn1_serial_number.deleted = 0 
				JOIN cases ON cases.id = cases_sn1_serial_number_1cases_ida AND cases.deleted = 0 
				WHERE cases_sn1_serial_number_1_c.deleted = 0 AND 
				 " . $additionalQuery . " 
				sn1_serial_number.id <> " . $this->bean->db->quoted($this->bean->id);
				
		$res = $this->bean->db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res['case_number']))
			return $this->getImage($res['case_number'], $res['id']);
		
		return "";
	}
	
	private function isCasesSubPanel() {
		return isset($_REQUEST['action']) && ($_REQUEST['action'] == 'DetailView' || $_REQUEST['action'] == 'SubPanelViewer') &&
			   isset($_REQUEST['module']) && $_REQUEST['module'] == 'Cases';
	}
	
	private function concatenate($oldValue, $newValue) {
		if (strlen($oldValue) > 0)
			return $oldValue . " OR " . $newValue;
		
		return $newValue;
	}
	
	private function getImage($case_number, $case_id) {
		if (strlen(trim($case_number)) == 0)
			return "";
		
		$text = 'Duplication in SN - ' . $case_number;
		
		return '<a href="index.php?module=Cases&action=DetailView&record=' . $case_id . '"><img src="custom/include/dtbc/images/error16.png" alt="' . $text . '" />&nbsp;' . $text . '</a>';
	}
}

function dtbc_display_rma_alert_image($bean) {
	$handler = new RmaAlertFunctionField($bean);
	return $handler->run();
}
