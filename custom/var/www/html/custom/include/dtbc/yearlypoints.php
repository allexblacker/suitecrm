<?php

class YearlyPoints {
	private $data;
	
	public function __construct() {
		$this->data = array();
	}
	
	public function addData($year, $total, $gift, $expirationDate, $row) {
		$this->data[] = array(
			"year" => $year,
			"total" => $total,
			"gift" => abs($gift),
			"expirationDate" => $expirationDate,
			"row" => $row,
		);
	}
	
	public function getPoints() {
		$result = array();
		for ($i = 0; $i < count($this->data); $i++) {
			if ($this->data[$i]['gift'] > 0 && $i > 0) {
				// If we have gift values we must find a way to decrease old values first
				for ($j = 0; $j < count($result); $j++) {
					if ($result[$j]['gift'] < $result[$j]['total']) {
						// Found a year where there is some space 
						if ($this->data[$i]['gift'] > $result[$j]['total'] - $result[$j]['gift']) {
							// Gift value is more than space we have
							$this->data[$i]['gift'] -= $result[$j]['total'] - $result[$j]['gift'];
							$result[$j]['gift'] = $result[$j]['total'];
						} else {
							$result[$j]['gift'] += $this->data[$i]['gift'];
							$this->data[$i]['gift'] = 0;
						}
					}
				}
			}
			
			$result[] = array(
				"year" => $this->data[$i]['year'],
				"total" => $this->data[$i]['total'],
				"gift" => $this->data[$i]['gift'] > $this->data[$i]['total'] ? $this->data[$i]['total'] : $this->data[$i]['gift'],
				"expirationDate" => $this->data[$i]['expirationDate'],
				"row" => $this->data[$i]['row'],
			);
		}
		return $result;
	}
	
	public function getAvailablePoints() {
		$retval = array(
			"total" => 0,
			"gift" => 0,
		);
		foreach ($this->data as $data) {
			$retval['total'] += $data['row']['sum_total'];
			$retval['gift'] += abs($data['row']['sum_gift']);
		}
		return $retval['total'] - $retval['gift'];
	}
}
