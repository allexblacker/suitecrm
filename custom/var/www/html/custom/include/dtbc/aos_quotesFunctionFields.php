<?php

require_once("custom/include/dtbc/helpers.php");

class AOS_QuotesFunctionFields {
	
	private $fieldType;
	private $db;
	private $currency;
	private $quoteBean;
	private $viewName;
	
	public function __construct($fieldType, $bean = null, $viewName = "") {
		global $db;
		$this->quoteBean = $this->getQuoteBean($bean);
		$this->fieldType = $fieldType;
		$this->db = $db;
		$this->currency = $this->getCurrency();
		$this->viewName = $viewName;
	}
	
	public function run() {
		switch (trim(strtolower($this->fieldType))) {
			case "subtotal":
				return $this->subtotal();
			case "discount":
				return $this->discount();
			case "totalprice":
				return $this->totalprice();
			case "grandtotal":
				return $this->grandtotal();
		}
	}
	
	private function getQuoteBean($bean) {
		if ($bean != null && $bean->object_name == 'AOS_Quotes')
			return $bean;
		
		if (isset($_REQUEST['module']) && $_REQUEST['module'] == 'AOS_Quotes' && isset($_REQUEST['record']) && strlen($_REQUEST['record']) > 0)
			return BeanFactory::getBean($_REQUEST['module'], $_REQUEST['record']);
		
		// parent_id ??
		
		return null;
	}
		
	private function getCurrency() {
		if ($this->quoteBean == null)
			return "0";
		
		$sql = "SELECT dtbc_pricebook_product_cstm.currency_c
				FROM dtbc_line_item
				JOIN dtbc_line_item_cstm on id_c = dtbc_line_item.id
				JOIN dtbc_pricebook_product ON dtbc_line_item_cstm.dtbc_pricebook_product_id_c = dtbc_pricebook_product.id AND dtbc_pricebook_product.deleted = 0
				JOIN dtbc_pricebook_product_cstm ON dtbc_pricebook_product.id = dtbc_pricebook_product_cstm.id_c
				JOIN opportunities_dtbc_line_item_1_c ON opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id AND opportunities_dtbc_line_item_1_c.deleted = 0
				WHERE dtbc_line_item.deleted = 0 AND
				opportunities_dtbc_line_item_1opportunities_ida = " . $this->db->quoted($this->quoteBean->opportunity_id);
				
		$res = $this->db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0)
			return $res['currency_c'];
		
		return "0";
	}
	
	private function subtotal($withoutFormat = false) {	
		if ($this->quoteBean == null)
			return "0";

		$sql = "SELECT SUM(dtbc_line_item_cstm.quantity_c * dtbc_line_item_cstm.list_price_c) AS subtotal
				FROM dtbc_line_item
				JOIN dtbc_line_item_cstm on dtbc_line_item_cstm.id_c = dtbc_line_item.id
				JOIN opportunities_dtbc_line_item_1_c ON opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id AND opportunities_dtbc_line_item_1_c.deleted = 0
				WHERE dtbc_line_item.deleted = 0 AND
				opportunities_dtbc_line_item_1opportunities_ida = " . $this->db->quoted($this->quoteBean->opportunity_id);
		
		$res = $this->db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0) {
			if ($withoutFormat)
				return $res['subtotal'];
			
			return $this->getFormattedCurrency($res['subtotal']);
		}
			

		return "0";
	}
	
	private function discount() {
		$subtotal = $this->subtotal(true);
		$totalprice = $this->totalprice(true);
		$discountCalc = 100.00 - ($totalprice / $subtotal) * 100.00;
		$discount = round($discountCalc, 2);
		$discount = number_format($discount, 2);
		return $discount . "%";
	}
	
	private function totalprice($withoutFormat = false) {
		if ($this->quoteBean == null)
			return "0";
		
		$sql = "SELECT sum(total_price_c) AS totalprice
				FROM dtbc_line_item
				JOIN dtbc_line_item_cstm on id_c = dtbc_line_item.id
				JOIN opportunities_dtbc_line_item_1_c ON opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id AND opportunities_dtbc_line_item_1_c.deleted = 0
				WHERE dtbc_line_item.deleted = 0 AND
				opportunities_dtbc_line_item_1opportunities_ida = " . $this->db->quoted($this->quoteBean->opportunity_id);

		$res = $this->db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0) {
			if ($withoutFormat)
				return $res['totalprice'];
			
			return $this->getFormattedCurrency($res['totalprice']);
		}
			
		
		return "0";
	}
	
	private function grandtotal() {
		if ($this->quoteBean == null)
			return "0";
		
		$sql = "SELECT sum(total_price_c) AS grandtotal
				FROM dtbc_line_item
				JOIN dtbc_line_item_cstm on id_c = dtbc_line_item.id
				JOIN opportunities_dtbc_line_item_1_c ON opportunities_dtbc_line_item_1dtbc_line_item_idb = dtbc_line_item.id AND opportunities_dtbc_line_item_1_c.deleted = 0
				WHERE dtbc_line_item.deleted = 0 AND
				opportunities_dtbc_line_item_1opportunities_ida = " . $this->db->quoted($this->quoteBean->opportunity_id);
		
		$res = $this->db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0)
			return $this->getFormattedCurrency($res['grandtotal']);
		
		return "0";
	}
	
	private function getFormattedCurrency($value) {
		switch ($this->currency) {
			case 2: return "USD " . $value;
		}
		return $value;
	}
}

function dtbc_lineitem_subtotal($focus = "", $fieldName = "", $temp1 = "", $viewName = "", $temp2 = "") {
	$obj = new AOS_QuotesFunctionFields("subtotal", $focus, $viewName);
	return $obj->run();
}

function dtbc_lineitem_discount($focus = "", $fieldName = "", $temp1 = "", $viewName = "", $temp2 = "") {
	$obj = new AOS_QuotesFunctionFields("discount", $focus, $viewName);
	return $obj->run();
}

function dtbc_lineitem_totalprice($focus = "", $fieldName = "", $temp1 = "", $viewName = "", $temp2 = "") {
	$obj = new AOS_QuotesFunctionFields("totalprice", $focus, $viewName);
	return $obj->run();
}

function dtbc_lineitem_grandtotal($focus = "", $fieldName = "", $temp1 = "", $viewName = "", $temp2 = "") {
	$obj = new AOS_QuotesFunctionFields("grandtotal", $focus, $viewName);
	return $obj->run();
}

