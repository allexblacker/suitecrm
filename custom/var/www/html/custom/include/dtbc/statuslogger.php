<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class StatusLogger {
	
	public function after_save($bean, $event, $arguments) {
		if (is_array($bean->fetched_row) && null != $bean->fetched_row['case_status_c']) {
			if ($bean->fetched_row['case_status_c'] != $bean->case_status_c) {
				$this->closeLastLog($bean->id);
				$this->createNewLog($bean->id, $bean->name, $bean->case_status_c, $bean->case_sub_status_c);
				$this->deleteMailSentField($bean->id, $bean->fetched_row['case_status_c'] != $bean->case_status_c, $bean->fetched_row['case_sub_status_c'] != $bean->case_sub_status_c);
			}
		}
	}
	
	private function deleteMailSentField($beanId, $isStatusChanged, $isSubStatusChanged) {
		global $db;
		
		if ($isStatusChanged) {
			$sql = "UPDATE cases
				SET dtbc_3days_noti_sent_c = 0,
				dtbc_5days_noti_sent_c = 0,
				dtbc_agent_status_noti_sent_c = 0
				WHERE id = " . $db->quoted($beanId);
		
			$db->query($sql);
		}
		
		if ($isSubStatusChanged) {
			$sql = "UPDATE cases
				SET dtbc_2days_noti_sent_c = 0,
				dtbc_agent_status_noti_sent_c = 0
				WHERE id = " . $db->quoted($beanId);
		
			$db->query($sql);
		}
	}
	
	private function closeLastLog($beanId) {
		global $db;
		
		$sql = "SELECT dtbc_statuslog_cstm.id_c, dtbc_statuslog_cstm.date_start_c 
				FROM dtbc_statuslog_cstm 
				JOIN cases_dtbc_statuslog_1_c ON cases_dtbc_statuslog_1dtbc_statuslog_idb = id_c 
				WHERE cases_dtbc_statuslog_1cases_ida = " . $db->quoted($beanId) . " AND 
				deleted = 0 AND
				(dtbc_statuslog_cstm.date_end_c IS NULL OR
				dtbc_statuslog_cstm.date_end_c = '')";
				
		$res = $db->fetchOne($sql);
		
		if (is_array($res) && isset($res['id_c']) && strlen($res['id_c']) > 0) {
			$sql = "UPDATE dtbc_statuslog_cstm 
					SET date_end_c = NOW(), time_spent_c = " . $db->quoted($this->getSpentTime($res['date_start_c'])) . " 
					WHERE id_c = " . $db->quoted($res['id_c']);
					
			$db->query($sql);
		}
	}
	
	private function getSpentTime($startDate) {
		global $db;
		$res = $db->fetchOne("SELECT NOW() AS dateend");
		
		$dteStart = new DateTime($startDate);
		$dteEnd   = new DateTime($res['dateend']);
		$dteDiff  = $dteStart->diff($dteEnd);
		return $dteDiff->format("%d days, %H:%I:%S");
	}
	
	private function createNewLog($beanId, $beanName, $status, $subStatus) {
		global $current_user, $db;
		$guid = $this->getGuid();
		$sql = "INSERT INTO dtbc_statuslog (
					id,
					name,
					date_entered,
					date_modified,
					modified_user_id,
					created_by,
					description,
					deleted,
					assigned_user_id
				) VALUES (
					" . $db->quoted($guid) . ",
					" . $db->quoted($beanName . " - status log") . ",
					NOW(),
					NOW(),
					" . $db->quoted($current_user->id) . ",
					" . $db->quoted($current_user->id) . ",
					" . $db->quoted("") . ",
					0,
					" . $db->quoted($current_user->id) . "
				)";
		$db->query($sql);
		
		$sql = "INSERT INTO dtbc_statuslog_cstm (
					id_c,
					userid_c,
					case_status_c,
					case_sub_status_c,
					date_start_c,
					date_end_c,
					time_spent_c
				) VALUES (
					" . $db->quoted($guid) . ",
					" . $db->quoted($current_user->id) . ",
					" . $db->quoted($status) . ",
					" . $db->quoted($subStatus) . ",
					NOW(),
					NULL,
					NULL
				)";
		$db->query($sql);
		
		// Our own relation
		$sql = "INSERT INTO cases_dtbc_statuslog_1_c (
					id,
					date_modified,
					deleted,
					cases_dtbc_statuslog_1cases_ida,
					cases_dtbc_statuslog_1dtbc_statuslog_idb
				) VALUES (
					UUID(),
					NOW(),
					0,
					" . $db->quoted($beanId) . ",
					" . $db->quoted($guid) . "
				)";
		$db->query($sql);
	}
	
	private function getGuid() {
		global $db;
		$res = $db->fetchOne("SELECT UUID() AS id");
		return $res['id'];
	}

}