<?php

if (!defined('sugarEntry') || !sugarEntry)
	die('Not A Valid Entry Point');

require_once('custom/include/dtbc/helpers.php');
	
function getToday($focus, $field, $value, $view){
	return DtbcHelpers::getDateBasedOnUserSettings(date('Y-m-d'));
}

function getCaseAge($focus, $field, $value, $view){
	if(!empty($focus))
		return DtbcHelpers::getDateInterval($focus->date_entered, DtbcHelpers::getDateTimeBasedOnUserSettings(date('Y-m-d H:i:s')), '%a');
}

function getSupportTeamForTemplate($focus, $field, $value, $view){

	$account = BeanFactory::getBean('Accounts',$focus->account_id);
	if(empty($account))
		return '';
	
	global $sugar_config, $app_list_strings;
	$container = $sugar_config['solaredge']['support_team_for_template'];
	if(in_array($account->country_c, $container))
		return $app_list_strings['country_0'][$account->country_c];
	else
		return '';
}

function getLastModifiedBy($focus, $field, $value, $view){
	return $focus->modified_by_name;
}