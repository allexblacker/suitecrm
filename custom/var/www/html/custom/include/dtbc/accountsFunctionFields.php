<?php

require_once("custom/include/dtbc/helpers.php");

class AccountsFunctionFields {
	
	private $fieldType;
	private $db;
	private $recordId;
	private $year;
	
	public function __construct($fieldType, $year = 0) {
		global $db;
		$this->fieldType = $fieldType;
		$this->db = $db;
		$this->recordId = !empty($_REQUEST['accountrecordid']) ? $_REQUEST['accountrecordid'] : "";
		if ($this->recordId == "")
			$this->recordId = !empty($_REQUEST['record']) ? $_REQUEST['record'] : "";
		$this->year = $year;
	}
	
	public function run() {
		switch (trim(strtolower($this->fieldType))) {
			case "loyaltystart":
				return $this->loyaltyStart();
			case "balance":
				return $this->balance();
			case "totalkw":
				return $this->totalKw();
			case "extra":
				return $this->extra();
			case "uniquesites":
				return $this->uniqueSites();
			case "lasttransaction":
				return $this->lastTransaction();
			case "totalyearkw":
				return $this->totalYearKw();
			case "lastpostransaction":
				return $this->lastPOSTransaction();
			case "ofopportunities":
				return $this->ofOpportunities();
			case "checkiffirstopp":
				return $this->checkIfFirstOpp();
			case "servicekit":
				return $this->serviceKit();
			case "newaccountbutton":
				return $this->newAccountButton();
			case "pointstable":
				return $this->pointsTable();
		}
	}
	
	private function pointsTable() {
		$retval = "<table border='1' style='width:400px; border-collapse:collapse;'>";
		$retval .= "<tbody>";
		// Header
		$retval .= "<tr>";
		$retval .= "<th><b>Year</b></th>";
		$retval .= "<th><b>Available points</b></th>";
		$retval .= "<th><b>Expiration date</b></th>";
		$retval .= "</tr>";
		
		// Content
		$year = intval(date("Y"));
		if (intval(date("m")) < 6 || (intval(date("m")) == 6 && intval(date("d")) == 1))
			$retval .= $this->getPointsTableYearLine($year - 2);
		$retval .= $this->getPointsTableYearLine($year - 1);
		$retval .= $this->getPointsTableYearLine($year);

		// Total line
		$retval .= "<tr>";
		$retval .= "<td><b>Total</b></td>";
		$retval .= "<td><b>" . $this->balance() . "</b></td>";
		$retval .= "<td><b></b></td>";
		$retval .= "</tr>";
		$retval .= "</tbody>";
		$retval .= "</table>";
		return $retval;
	}
	
	private function getPointsTableYearLine($year) {
		$temp = $this->year;
		$this->year = $year;
		$totalKw = $this->totalYearKw(true);
		$this->year = $temp;
		
		if (strlen($totalKw) == 0)
			$totalKw = "0";
		else if ($totalKw == "No data")
			return "";

		$yearExp = $year + 2;
		$retval .= "<tr>";
		$retval .= "<td>" . $year . "</td>";
		$retval .= "<td>" . $totalKw . "</td>";
		$retval .= "<td>" . DtbcHelpers::getYearAllianceExpirationDate($year, true) . "</td>";
		$retval .= "</tr>";

		return $retval;
	}
		
	private function getFirstLastTransactionDateSql($isFirst) {
		$temp = $isFirst ? "ASC" : "DESC";
		return "SELECT date1
				FROM accounts_al1_alliance_transaction_1_c
				JOIN al1_alliance_transaction ON al1_alliance_transaction.id = accounts_al1_alliance_transaction_1al1_alliance_transaction_idb AND al1_alliance_transaction.deleted = 0 
				WHERE accounts_al1_alliance_transaction_1_c.deleted = 0 AND
				accounts_al1_alliance_transaction_1accounts_ida = " . $this->db->quoted($this->recordId) . "
				ORDER BY date1 " . $temp;
	}
	
	private function getSumSql($sumType) {
		$temp = "points";
		$extraWhere = "";
		switch (trim(strtolower($sumType))) {
			case "kw":
				$temp = "kw";
				break;
			case "extra":
				$extraWhere = "AND type NOT IN ('Site, Fault_Site, Gift')";
				break;
		}
		
		// Calculate start date
		$year = intval(date("Y")) - 1;

		if (intval(date("m")) < 6 || (intval(date("m")) == 6 && intval(date("d")) == 1))
			$year = intval(date("Y")) - 2;
		
		$fromDate = $year . "-01-01";

		return "SELECT SUM(" . $temp . ") as " . $temp . "
				FROM accounts_al1_alliance_transaction_1_c
				JOIN al1_alliance_transaction ON al1_alliance_transaction.id = accounts_al1_alliance_transaction_1al1_alliance_transaction_idb AND al1_alliance_transaction.deleted = 0 
				WHERE accounts_al1_alliance_transaction_1_c.deleted = 0 AND
				accounts_al1_alliance_transaction_1accounts_ida = " . $this->db->quoted($this->recordId) . " AND
				date1 >= " . $this->db->quoted($fromDate) . " " . $extraWhere;
	}
	
	private function loyaltyStart() {
		$sql = $this->getFirstLastTransactionDateSql(true);
		$res = $this->db->fetchOne($sql);
		if (!empty($res) && isset($res['date1']))
			return DtbcHelpers::localDate($res['date1']);
		return "";
	}
	
	private function balance() {
		$sql = $this->getSumSql("points");
		$res = $this->db->fetchOne($sql);
		if (!empty($res) && isset($res['points']))
			return $res['points'];
		return "0";
	}
	
	private function totalKw() {
		$sql = $this->getSumSql("kw");
		$res = $this->db->fetchOne($sql);
		if (!empty($res) && isset($res['kw']))
			return $res['kw'];
		return "0";
	}
	
	private function extra() {
		$sql = $this->getSumSql("extra");
		$res = $this->db->fetchOne($sql);
		if (!empty($res) && isset($res['extra']))
			return $res['extra'];
		return "0";
	}
	
	private function uniqueSites() {
		$sql = "SELECT COUNT(DISTINCT monitoring_site_id) AS summary
				FROM accounts_al1_alliance_transaction_1_c
				JOIN al1_alliance_transaction ON al1_alliance_transaction.id = accounts_al1_alliance_transaction_1al1_alliance_transaction_idb AND al1_alliance_transaction.deleted = 0 
				WHERE accounts_al1_alliance_transaction_1_c.deleted = 0 AND
				accounts_al1_alliance_transaction_1accounts_ida = " . $this->db->quoted($this->recordId);
		$res = $this->db->fetchOne($sql);
		if (!empty($res) && isset($res['summary']))
			return $res['summary'];
		return "0";
	}
	
	private function lastTransaction() {
		$sql = $this->getFirstLastTransactionDateSql(false);
		$res = $this->db->fetchOne($sql);
		if (!empty($res) && isset($res['date1']))
			return DtbcHelpers::localDate($res['date1']);
		return "";
	}
	
	private function totalYearKw($notifyIfNoDate = false) {
		$sql = "SELECT SUM(points) AS points 
				FROM al1_alliance_transaction 
				JOIN accounts_al1_alliance_transaction_1_c ON accounts_al1_alliance_transaction_1al1_alliance_transaction_idb = al1_alliance_transaction.id AND accounts_al1_alliance_transaction_1_c.deleted = 0
				WHERE al1_alliance_transaction.deleted = 0 AND
				accounts_al1_alliance_transaction_1accounts_ida = " . $this->db->quoted($this->recordId) . " AND 
				date1 >= '" . $this->year . "-01-01' AND 
				date1 <= '" . $this->year . "-12-31'";
		$res = $this->db->fetchOne($sql);
		
		if (!empty($res) && isset($res['points']))
			return $res['points'];
		else if ($notifyIfNoDate)
			return "No data";
		
		return "";
	}
		
	private function lastPOSTransaction(){
		$sql = "SELECT MAX(transaction_date) AS max_date FROM pos1_pos_tracking
				INNER JOIN accounts_pos1_pos_tracking_1_c
				ON accounts_pos1_pos_tracking_1_c.accounts_pos1_pos_tracking_1pos1_pos_tracking_idb = pos1_pos_tracking.id 
				AND accounts_pos1_pos_tracking_1_c.deleted = 0  
				WHERE pos1_pos_tracking.deleted = 0 AND accounts_pos1_pos_tracking_1_c.accounts_pos1_pos_tracking_1accounts_ida = " . $this->db->quoted($this->recordId);
		$result = $this->db->fetchOne($sql);
		if(!empty($result['max_date']))
			return DtbcHelpers::getDateBasedOnUserSettings($result['max_date']);
		return '';
	}
	
	private function ofOpportunities(){
		$sql = "SELECT COUNT(*) AS opportunity_number FROM accounts_opportunities
				WHERE accounts_opportunities.deleted = 0 AND accounts_opportunities.account_id =" . $this->db->quoted($this->recordId);
		$result = $this->db->fetchOne($sql);
		return $result['opportunity_number'];
	}
	
	private function checkIfFirstOpp(){
		$sql = "SELECT COUNT(opportunities.id) AS opportunity_number FROM opportunities 
				INNER JOIN accounts_opportunities
				ON accounts_opportunities.opportunity_id = opportunities.id AND accounts_opportunities.deleted = 0
				WHERE opportunities.deleted = 0 AND opportunities.date_closed >= '2014-01-01' AND accounts_opportunities.account_id = " . $this->db->quoted($this->recordId);
		$result = $this->db->fetchOne($sql);
		return $result['opportunity_number'];
	}
	
	private function serviceKit(){
		$sql = "SELECT SUM(fsk1_field_service_kit.qty) AS sum_qty FROM fsk1_field_service_kit
				INNER JOIN accounts_fsk1_field_service_kit_1_c
				ON accounts_fsk1_field_service_kit_1_c.accounts_fsk1_field_service_kit_1fsk1_field_service_kit_idb AND accounts_fsk1_field_service_kit_1_c.deleted = 0
				WHERE fsk1_field_service_kit.deleted = 0 AND accounts_fsk1_field_service_kit_1_c.accounts_fsk1_field_service_kit_1accounts_ida = " . $this->db->quoted($this->recordId);
		$result = $this->db->fetchOne($sql);
		
		if(!empty($result))
			return $result['sum_qty'];
		return '';
	}
	
	private function newAccountButton(){
		global $sugar_config;
		$url = $sugar_config['site_url'] . "/index.php?module=Accounts&action=EditView&return_module=Accounts&return_action=index";
		$text = 'Create a New Account';
		return DtbcHelpers::getHyperLinkForFormulaFields($url,$text);
	}
}

function dtbc_newAccountButton(){
	$obj = new AccountsFunctionFields("newaccountbutton");
	return $obj->run();
}

function dtbc_serviceKit(){
	$obj = new AccountsFunctionFields("servicekit");
	return $obj->run();
}

function dtbc_checkIfFirstOpp(){
	$obj = new AccountsFunctionFields("checkiffirstopp");
	return $obj->run();
}

function dtbc_ofOpportunities(){
	$obj = new AccountsFunctionFields("ofopportunities");
	return $obj->run();
}

function dtbc_lastPOSTransaction(){
	$obj = new AccountsFunctionFields("lastpostransaction");
	return $obj->run();
}

function dtbc_loyaltyStart() {
	$obj = new AccountsFunctionFields("loyaltyStart");
	return $obj->run();
}

function dtbc_balance() {
	$obj = new AccountsFunctionFields("balance");
	return $obj->run();
}

function dtbc_totalKw() {
	$obj = new AccountsFunctionFields("totalKw");
	return $obj->run();
}

function dtbc_extra() {
	$obj = new AccountsFunctionFields("extra");
	return $obj->run();
}

function dtbc_uniqueSites() {
	$obj = new AccountsFunctionFields("uniqueSites");
	return $obj->run();
}

function dtbc_lastTransaction() {
	$obj = new AccountsFunctionFields("lastTransaction");
	return $obj->run();
}

function dtbc_totalYearKw() {
	$args = func_get_args();
	if (!empty($args) && is_array($args) && count($args) > 1) {
		$year = filter_var($args[1], FILTER_SANITIZE_NUMBER_INT);
		$obj = new AccountsFunctionFields("totalYearKw", $year);
		return $obj->run();
	}
	return "";
}

function dtbc_email_points_total() {
	return dtbc_balance();
}

function dtbc_email_points_table() {
	$obj = new AccountsFunctionFields("pointstable");
	return $obj->run();
}
