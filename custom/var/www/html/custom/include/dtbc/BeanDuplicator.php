<?php

class BeanDuplicator {

	public function getModuleFieldDefs($module)
	{
		global $app_strings, $beanList;
				
		$fields = array();
		$relations = array();

		if(isset($beanList[$module]) && $beanList[$module]){
			$mod = new $beanList[$module]();
			foreach($mod->field_defs as $name => $arr){				
				if($arr['type'] != 'link' && ((!isset($arr['source']) || $arr['source'] != 'non-db') || ($arr['type'] == 'relate' && isset($arr['id_name']))) && $name != 'currency_name' && $name != 'currency_symbol'){
					$fields[$name] = $arr['type'];
				} else if ($arr['type'] == 'link' && isset($arr['id_name'])) {
					$fields[$arr['id_name']] = $arr['type'];
				} else {
					if (array_key_exists("relationship", $arr))
						$relations[] = $arr['relationship'];
				}
			}
		}
		
		$field_defs = array(
			"fields" => $fields,
			"relations" => $relations
		);
		
		return $field_defs;
	}

	public function getDuplicatedBean($id, $module, $fields, $nonCopyFields) {
		$oldBean = BeanFactory::getBean($module)->retrieve($id);
		$newBean = BeanFactory::newBean($module);
		
		foreach ($fields as $field => $type) {
			if (in_array($field, $nonCopyFields))
				continue;
			if ($type != "float" && $type != "currency")
				$newBean->{$field} = $oldBean->{$field};
			else
				$newBean->{$field} =  $this->toFloat($oldBean->{$field});
		}
		return $newBean;		
	}
	
	private function toFloat($val, $decLength = 3) {
		if(is_string($val)) {
			return floatval(str_replace(',', '.', $val));
		} else {
			return number_format($val, $decLength, ',', '');
		}
	}
	
	public function duplicateRelations($relations, $oldBean, $newBean, $nonChangeLinks, $deleteOldRels = false) {
		
		foreach ($relations as $relation) {
			
			if (in_array($relation, $nonChangeLinks))
				continue;
			
			if (!$oldBean->load_relationship($relation) || !$newBean->load_relationship($relation))
				continue;
			
			$existingRelations = array();
			foreach ($oldBean->$relation->getBeans() as $entity) {
				$newBean->$relation->add($entity->id);
				$existingRelations []= $entity->id;
			}
			
			if($deleteOldRels) {
				foreach ($existingRelations as $existingRelation) {
					$oldBean->$relation->delete($oldBean->id, $existingRelation);
				}				
			}		
		}		
	}
	
	public function duplicateCustomRelation($tableName, $newBeanId, $oldBeanId, $customWhere) {
		$db = DBManagerFactory::getInstance();
		$sql = " SELECT * FROM " . $tableName . " WHERE deleted = 0 AND " . $customWhere;
		
		$response = $db->query($sql);
			
		while($result = $db->fetchByAssoc($response)) {
			$fieldList = "";
			$values = "";
			
			$result['id'] = create_guid();
			
			$oldIdField = array_search($oldBeanId,$result);
			$result[$oldIdField] = $newBeanId;
			
			foreach($result as $field => $value) {
				$fieldList .= $field . ",";
				$values .= $db->quoted($value) . ",";
			}
			
			$fieldList = rtrim($fieldList, ",");
			$values = rtrim($values, ",");
							
			$insertSql = "INSERT INTO " . $tableName . "(" . $fieldList . ") VALUES (" . $values . ")";
			
			$db->query($insertSql);
			
		}		
	}
		
}