<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

class FormulaFieldsContacts {
	
	private $accountBean;
	
	public function before_save($bean, $event, $arguments) {
		$this->setBeans($bean);
		
		if ($this->accountBean != null) {
			$bean->cont_type_acc_service_is_mi_c = $this->get_cont_type_acc_service_is_mi_c($this->accountBean->service_level_c,$bean->contact_type_c);
			$bean->allow_loyalty_plan_email_c = $this->get_allow_loyalty_plan_email_c($this->accountBean->allow_loyalty_plan_email_c);
		}
		
		$bean->certified_contact_c = $this->get_certified_contact_c($bean->certified_installer_c);
		$bean->department = $this->get_department($bean->created_by);
		$bean->google_search_c = $this->get_google_search_c($bean->first_name, $bean->last_name);
		$bean->linkedin_search_c = $this->get_linkedin_search_c($bean->first_name, $bean->last_name);
		$bean->location_c = $this->get_location_c($bean->primary_address_street, $bean->primary_address_city, $bean->primary_address_state, $bean->primary_address_country);
		$bean->total_lead_score_c = $this->get_total_lead_score_c($bean->lead_score_c, $bean->campaign_score_c);
		$bean->xing_search_c = $this->get_xing_search_c($bean->first_name, $bean->last_name);
	}
	
	private function setBeans($bean){
		if(!empty($bean->account_id))
			$this->accountBean = BeanFactory::getBean('Accounts',$bean->account_id);
	}
	
	private function get_cont_type_acc_service_is_mi_c($service_level, $contact_type){
		
		if(empty($service_level) || empty($contact_type))
			return DtbcHelpers::getImageTagForFormulaFields('Missing_information.jpg','Missing Contact Type/Account Service Level',true);
		return '';
	}
	
	private function get_allow_loyalty_plan_email_c($allow_loyalty_plan_email){
		
		if($allow_loyalty_plan_email)
			return 'True';
		else
			return 'False';
	}
	
	private function get_certified_contact_c($certified_installer){
		
		if($certified_installer)
			return DtbcHelpers::getImageTagForFormulaFields('CertifiedInstaller.jpg','Certified Contact',true);
		return '';
	}
	
	public function get_department($created_by_id){
		global $db;
		$sql = "SELECT department, division_c 
				FROM users
				JOIN users_cstm ON id = id_c
				WHERE id = " . $db->quoted($created_by_id);
		$res = $db->fetchOne($sql);
		
		if (!empty($res) && is_array($res) && count($res) > 0)
			return empty($res['department']) ? $res['division_c'] : $res['department'];
	}
	
	private function get_google_search_c($first_name, $last_name){
		$link = "http://www.google.com//#q=$first_name+$last_name";
		$text = "Search Google for $first_name";
		return DtbcHelpers::getHyperLinkForFormulaFields($link,$text);
	}
	
	private function get_linkedin_search_c($first_name, $last_name){
		$link = "http://www.linkedin.com/vsearch/f?type=people&keywords=$first_name+$last_name";
		$text = "Search Linkedin for $first_name";
		return DtbcHelpers::getHyperLinkForFormulaFields($link,$text);
	}
	
	private function get_location_c($mailing_street, $mailing_city, $mailing_state, $mailing_country){
		$link = "http://maps.google.com/maps/api/staticmap?center=$mailing_street,$mailing_city,$mailing_state,$mailing_country&zoom=14&size=200x200";
		return DtbcHelpers::getImageTagForFormulaFields($link,'error',false, true);
	}
	
	private function get_total_lead_score_c($lead_score_c, $campaign_score_c){
		return $lead_score_c + $campaign_score_c;
	}
	
	private function get_xing_search_c($first_name, $last_name){
		$link = "https://www.xing.com/profile/($first_name)_($last_name)";
		$text = "Search Xing for $first_name";
		return DtbcHelpers::getHyperLinkForFormulaFields($link,$text);
	}
	
}