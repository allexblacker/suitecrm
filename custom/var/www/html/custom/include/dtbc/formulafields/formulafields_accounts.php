<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once('custom/include/dtbc/accountsFunctionFields.php');

class FormulaFieldsAccounts{
	
	private $last_pos_transaction;
	private $last_alliance_transaction_c;
	
	public function before_save($bean, $event, $arguments) {
		$this->setField();
		
		$bean->account_target_formula_c = $this->get_account_target_formula_c($bean->account_target_c);
		$bean->service_lvl_is_missing_c = $this->get_service_lvl_is_missing_c($bean->service_level_c);
		$bean->customer_satisfaction_rating_c = $this->get_customer_satisfaction_rating_c($bean->not_currently_satisfied_c);
		$bean->days_without_alliance_transa_c = $this->get_days_without_alliance_transa_c($this->last_alliance_transaction_c);
		$bean->department_c = $this->get_department_c($bean->created_by);
		$bean->language_c = $this->get_language_c($bean->country_c);
		$bean->pos_transaction_quarter_c = $this->get_pos_transaction_quarter_c();
		$bean->pos_transaction_year_c = $this->get_pos_transaction_year_c();
		$bean->pos_last_transaction_y_and_q_c = $this->get_pos_last_transaction_y_and_q_c($bean->pos_transaction_year_c, $bean->pos_transaction_quarter_c);
		$bean->location_c = $this->get_location_c($bean->billing_address_street,$bean->billing_address_city,
																		  $bean->billing_address_state,$bean->billing_address_country);
		$bean->service_level_flag_c = $this->get_service_level_flag_c($bean->service_level_c);
		$bean->solarcity_account_flag_c = $this->get_solarcity_account_flag_c($bean->name);
		$bean->vip_customer_c = $this->get_vip_customer_c($bean->name);
		$bean->target_m_c = $this->get_target_m_c($bean->region_c);
		$bean->us_county_c = $this->get_us_county_c($bean->uzc1_usa_zip_codes_id_c);
	}
	
	private function setField(){
		$accountsFunctionfields = new AccountsFunctionFields('lastpostransaction');
		$this->last_pos_transaction = $accountsFunctionfields->run();
		$accountsFunctionfields = new AccountsFunctionFields('lastTransaction');
		$this->last_alliance_transaction_c = $accountsFunctionfields->run();
	}
	
	private function get_service_lvl_is_missing_c($service_level_c){
		
		if(empty($service_level_c))
			return DtbcHelpers::getImageTagForFormulaFields('Missing_information.jpg','Missing Account Service Level',true);
	}
	
	private function get_account_target_formula_c($account_target){
	
		$value = $account_target_c / 1000;
		return $value . "K";
	}
	
	private function get_customer_satisfaction_rating_c($not_currently_satisfied_c){
		
		if($not_currently_satisfied_c)
			return DtbcHelpers::getImageTagForFormulaFields('CustomerSatisfactionRating.png','',true);
		return '';
	}
	
	private function get_days_without_alliance_transa_c($last_alliance_transaction_c){
		
		if(!empty($last_alliance_transaction_c)){
			$today = date_create(date('Y-m-d'));
			$last_transaction = date_create($last_alliance_transaction_c);
			$diff = date_diff($last_transaction,$today)->format('%a');
			return $diff;
		}
		return '';
	}
	
	private function get_department_c($created_by_id){
		$accountsFunctionfields = new AccountsFunctionFields('ofopportunities');
		$of_opportunities = $accountsFunctionfields->run();
		
		if(intval($of_opportunities) > 0)
			return 'Sales';
		else{
			$user = BeanFactory::getBean('Users',$created_by_id);
			if(empty($user->department))
				return $user->division_c;
			else
				return $user->department;
		}
	}
	
	private function get_language_c($country_c){
		global $app_list_strings;
		$country = $app_list_strings['country_0'][$country_c];
		
		switch($country){
			
			case "Belgium":
				return "FR";
			case "France":
				return "FR";
			case "Luxembourg":
				return "FR";
			case "Morocco":
				return "FR";
			case "Austria":
				return "DE";
			case "Germany":
				return "DE";
			case "Liechtenstein":
				return "DE";
			case "Switzerland":
				return "DE";
			case "Italy":
				return "it";
			case "San Marino":
				return "it";
			default:
				return 'EN';
		}
	}
	
	private function get_pos_last_transaction_y_and_q_c($year, $quarter){
		return $quarter . ' ' . $year;
	}
	
	private function get_pos_transaction_quarter_c(){
		
		if(!empty($this->last_pos_transaction)){
			$userDateFormat = DtbcHelpers::getUserDateFormat();
			$month = DtbcHelpers::getDatePart($userDateFormat,$this->last_pos_transaction,'month');
			return 'Q' . ceil($month / 3);
		}
		return '';
	}
	
	private function get_pos_transaction_year_c(){
		
		if(!empty($this->last_pos_transaction)){
			$userDateFormat = DtbcHelpers::getUserDateFormat();
			return DtbcHelpers::getDatePart($userDateFormat,$this->last_pos_transaction,'year');
		}
		return '';
	}
	
	private function get_location_c($billing_address_street,$billing_address_city,$billing_address_state,$billing_address_country){

		$image = "http://maps.google.com/maps/api/staticmap?center=$billing_address_street,$billing_address_city,$billing_address_state,$billing_address_country&zoom=14&size=200x200&sensor=false";
		return DtbcHelpers::getImageTagForFormulaFields($image, 'error', false, true);
	}
	
	private function get_service_level_flag_c($service_level){
		
		$image = '';
		switch($service_level){
			case 'VIP':
				$image = 'exclamation.png';
				break;
			case 'Service_Hold':
				$image = 'bullet_delete.png';
				break;
			case 'Finance_Hold':
				$image = 'bullet_delete.png';
				break;
		}
		
		if(!empty($image))
			return DtbcHelpers::getImageTagForFormulaFields($image,'Service Level Flag',false);
		return '';
	}
	
	private function get_solarcity_account_flag_c($name){
		
		$alt = 'SolarCity Flag';
		if(strpos(strtolower($name),'solarcity') !== false)
			return DtbcHelpers::getImageTagForFormulaFields('entitlements24.png',$alt,false);
		else
			return DtbcHelpers::getImageTagForFormulaFields('s.gif',$alt,false);
	}
	
	private function get_vip_customer_c($name){
	
		$alt = 'SunPower Flag';
		if(strpos(strtolower($name),'sunpower') !== false)
			return DtbcHelpers::getImageTagForFormulaFields('entitlements24.png',$alt,false);
		else
			return DtbcHelpers::getImageTagForFormulaFields('s.gif',$alt,false);
	}
	
	private function get_target_m_c($region){
		global $sugar_config, $app_list_strings;
		$array = 'solaredge';
		$field = 'target_m_c';
		
		$regionValue = $app_list_strings['region_list'][$region];
		switch($regionValue){
			case 'North America':
				return $sugar_config[$array][$field]['North_America'];
			case 'DACH':
				return $sugar_config[$array][$field]['DACH'];
			case 'Italy':
				return $sugar_config[$array][$field]['Italy'];
			case 'APAC':
				return $sugar_config[$array][$field]['APAC'];
			case 'Netherlands':
				return $sugar_config[$array][$field]['Netherlands'];
			case 'Israel':
				return $sugar_config[$array][$field]['Israel'];
			case 'France+Belgium':
				return $sugar_config[$array][$field]['France_Belgium'];
			case 'UK':
				return $sugar_config[$array][$field]['UK'];
			case 'China':
				return $sugar_config[$array][$field]['China'];
			case 'Japan':
				return $sugar_config[$array][$field]['Japan'];
			case 'Australia':
				return $sugar_config[$array][$field]['Australia'];
			default:
				return 0;
		}
	}
	
	private function get_us_county_c($uzc1_usa_zip_codes_id_c){
		$zipBean = BeanFactory::getBean('UZC1_USA_ZIP_Codes',$uzc1_usa_zip_codes_id_c);
		return $zipBean->county;
	}
}
