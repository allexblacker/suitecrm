<?php

require_once("custom/include/dtbc/helpers.php");
require_once("custom/modules/AOW_WorkFlow/AOW_WorkFlow.php");

class SystemWorkflows {
	private $db;
	
	public function __construct() {
		global $db;
		$this->db = $db;
	}
	
	public function run() {
		$wfObj = $this->getWorkFlows();
		$flow = new CustomAOW_WorkFlow();
		
		while($row = $this->db->fetchByAssoc($wfObj)) {
			$bean = BeanFactory::getBean($row['modulename'], $row['beanid']);
			$flow->retrieve($row['wfid']);
			if ($flow->check_valid_bean($bean)) {
				$flow->run_actions($bean, true);
			}
			$this->inactivateScheduledSystemWorkflow($row['swfid']);
		}
		
		return true;
	}
	
	private function inactivateScheduledSystemWorkflow($sWfId) {
		$sql = "UPDATE dtbc_system_workflow_scheduling_cstm
				SET triggered_c = 1
				WHERE id_c = " . $this->db->quoted($sWfId);
		
		return $this->db->query($sql);
	}
	
	private function getWorkFlows() {
		$sql = "SELECT aow_workflow_id_c AS wfid, dtbc_system_workflow_scheduling.id AS swfid, parent_type AS modulename, parent_id AS beanid
				FROM dtbc_system_workflow_scheduling
				JOIN dtbc_system_workflow_scheduling_cstm ON dtbc_system_workflow_scheduling_cstm.id_c = dtbc_system_workflow_scheduling.id
				JOIN aow_workflow ON aow_workflow.id = dtbc_system_workflow_scheduling_cstm.aow_workflow_id_c
				WHERE dtbc_system_workflow_scheduling.deleted = 0 AND
				dtbc_system_workflow_scheduling_cstm.triggered_c = 0 AND
                STR_TO_DATE(dtbc_system_workflow_scheduling_cstm.trigger_time_c, '%Y-%m-%d %H:%i:%s') <= UTC_TIMESTAMP";
		
		return $this->db->query($sql);
	}
}
