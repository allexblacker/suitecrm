<?php

function includeJavaScriptFile($path) {
	echo '<script type="text/javascript" src="' . $path . "?v=" . trim(str_replace(".", "_", translate('LBL_VERSION_NUMBER', 'Users'))) . '"></script>';
}

function includeCssFile($path) {
	echo '<link rel="stylesheet" type="text/css" href="' . $path . "?v=" . trim(str_replace(".", "_", translate('LBL_VERSION_NUMBER', 'Users'))) .  '">';
}
