<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");

function dtbc_AddContactButton(){
	global $sugar_config;
	$url = $sugar_config['site_url'];
	$url = $sugar_config['site_url'] . "/index.php?module=Contacts&action=EditView&return_module=Contacts&return_action=index";
	$text = 'Create a New Contact';
	return DtbcHelpers::getHyperLinkForFormulaFields($url,$text);
}