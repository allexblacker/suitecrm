<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once("custom/include/dtbc/helpers.php");
require_once("modules/Configurator/Configurator.php");
require_once("modules/Emails/EmailUI.php");
require_once("custom/modules/InboundEmail/CustomAOPInboundEmail.php");

class InboundEmailScheduler {
	
	public function resetScheduler($checkTime) {
		global $db, $sugar_config;
		
		$partialQuery = "";
		
		if ($checkTime) {
			$partialQuery = " AND TIME_TO_SEC(TIMEDIFF(NOW(), date_entered)) / 60 > " . $sugar_config['solaredge']['inbound_reset_minutes'];
		}

		$sql = "DELETE
				FROM job_queue 
				WHERE status = " . $db->quoted('running') . " " .  $partialQuery;
		
		$db->query($sql);

		return true;
	}
	
	public function runScheduler() {
		return $this->core_pollMonitoredInboxesAOP();
	}

	// Not Nice? Yes, from the core...
	private function core_pollMonitoredInboxesAOP() {
		$GLOBALS['log']->info('----->Scheduler fired job of type core_pollMonitoredInboxesAOP()');
		global $dictionary;
		global $app_strings;
		global $sugar_config;

		$ie = new CustomAOPInboundEmail();
		$emailUI = new EmailUI();
		$r = $ie->db->query('SELECT id, name FROM inbound_email WHERE is_personal = 0 AND deleted=0 AND status=\'Active\' AND mailbox_type != \'bounce\'');
		$GLOBALS['log']->debug('Just got Result from get all Inbounds of Inbound Emails');
		
		while ($a = $ie->db->fetchByAssoc($r)) {
			$GLOBALS['log']->debug('In while loop of Inbound Emails');
			$ieX = new CustomAOPInboundEmail();
			$ieX->retrieve($a['id']);
			$mailboxes = $ieX->mailboxarray;
			foreach ($mailboxes as $mbox) {
				$ieX->mailbox = $mbox;
				$newMsgs = array();
				$msgNoToUIDL = array();
				$connectToMailServer = false;
				if ($ieX->isPop3Protocol()) {
					$msgNoToUIDL = $ieX->getPop3NewMessagesToDownloadForCron();
					// get all the keys which are msgnos;
					$newMsgs = array_keys($msgNoToUIDL);
				}
				if ($ieX->connectMailserver() == 'true') {
					$connectToMailServer = true;
				} // if
				$GLOBALS['log']->debug('Trying to connect to mailserver for [ ' . $a['name'] . ' ]');
				if ($connectToMailServer) {
					$GLOBALS['log']->debug('Connected to mailserver');
					if (!$ieX->isPop3Protocol()) {
						$newMsgs = $ieX->getNewMessageIds();
					}
					if (is_array($newMsgs)) {
						$current = 1;
						$total = count($newMsgs);
						require_once("include/SugarFolders/SugarFolders.php");
						$sugarFolder = new SugarFolder();
						$groupFolderId = $ieX->groupfolder_id;
						$isGroupFolderExists = false;
						$users = array();
						if ($groupFolderId != null && $groupFolderId != "") {
							$sugarFolder->retrieve($groupFolderId);
							$isGroupFolderExists = true;
						} // if
						$messagesToDelete = array();
						if ($ieX->isMailBoxTypeCreateCase()) {
							require_once 'modules/AOP_Case_Updates/AOPAssignManager.php';
							$assignManager = new AOPAssignManager($ieX);
						}
						foreach ($newMsgs as $k => $msgNo) {
							$uid = $msgNo;
							if ($ieX->isPop3Protocol()) {
								$uid = $msgNoToUIDL[$msgNo];
							} else {
								$uid = imap_uid($ieX->conn, $msgNo);
							} // else
							try {
								if ($isGroupFolderExists) {
									if ($ieX->importOneEmail($msgNo, $uid)) {
										// add to folder
										$sugarFolder->addBean($ieX->email);
										if ($ieX->isPop3Protocol()) {
											$messagesToDelete[] = $msgNo;
										} else {
											$messagesToDelete[] = $uid;
										}
										if ($ieX->isMailBoxTypeCreateCase()) {
											$userId = $assignManager->getNextAssignedUser();
											$GLOBALS['log']->debug('userId [ ' . $userId . ' ]');
		
											$ieX->handleCreateCase($ieX->email, $userId);
										} // if
									} // if
								} else {
									if ($ieX->isAutoImport()) {
										$ieX->importOneEmail($msgNo, $uid);
									} else {
										/*If the group folder doesn't exist then download only those messages
										 which has caseid in message*/

										$ieX->getMessagesInEmailCache($msgNo, $uid);
										$email = new Email();
										$header = imap_headerinfo($ieX->conn, $msgNo);
										$email->name = $ieX->handleMimeHeaderDecode($header->subject);
										$email->from_addr = $ieX->convertImapToSugarEmailAddress($header->from);
										$email->reply_to_email = $ieX->convertImapToSugarEmailAddress($header->reply_to);
										if (!empty($email->reply_to_email)) {
											$contactAddr = $email->reply_to_email;
										} else {
											$contactAddr = $email->from_addr;
										}
										$mailBoxType = $ieX->mailbox_type;
										$ieX->handleAutoresponse($email, $contactAddr);
									} // else
								} // else
							} catch (Exception $ex) {
								DtbcHelpers::logger($ex, "inbound_errors.txt");
							} catch(Throwable $th) {
								DtbcHelpers::logger($th, "inbound_errors.txt");
							}
							
							$GLOBALS['log']->debug('***** On message [ ' . $current . ' of ' . $total . ' ] *****');
							$current++;
						} // foreach
						// update Inbound Account with last robin

					} // if
					if ($isGroupFolderExists) {
						$leaveMessagesOnMailServer = $ieX->get_stored_options("leaveMessagesOnMailServer", 0);
						if (!$leaveMessagesOnMailServer) {
							if ($ieX->isPop3Protocol()) {
								$ieX->deleteMessageOnMailServerForPop3(implode(",", $messagesToDelete));
							} else {
								$ieX->deleteMessageOnMailServer(implode($app_strings['LBL_EMAIL_DELIMITER'], $messagesToDelete));
							}
						}
					}
				} else {
					$GLOBALS['log']->fatal("SCHEDULERS: could not get an IMAP connection resource for ID [ {$a['id']} ]. Skipping mailbox [ {$a['name']} ].");
					// cn: bug 9171 - continue while
				} // else
			} // foreach
			imap_expunge($ieX->conn);
			imap_close($ieX->conn, CL_EXPUNGE);
		} // while
		return true;
	}
}