<?php 
 //WARNING: The contents of this file are auto-generated

 
 //WARNING: The contents of this file are auto-generated
$beanList['OS1_Outside_Sales_Activity'] = 'OS1_Outside_Sales_Activity';
$beanFiles['OS1_Outside_Sales_Activity'] = 'modules/OS1_Outside_Sales_Activity/OS1_Outside_Sales_Activity.php';
$moduleList[] = 'OS1_Outside_Sales_Activity';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Contact_Roles'] = 'dtbc_Contact_Roles';
$beanFiles['dtbc_Contact_Roles'] = 'modules/dtbc_Contact_Roles/dtbc_Contact_Roles.php';
$moduleList[] = 'dtbc_Contact_Roles';


 
 //WARNING: The contents of this file are auto-generated
$beanList['KReports'] = 'KReport';
$beanFiles['KReport'] = 'modules/KReports/KReport.php';
$moduleList[] = 'KReports';


 
 //WARNING: The contents of this file are auto-generated
$beanList['FSK1_Field_Service_Kit'] = 'FSK1_Field_Service_Kit';
$beanFiles['FSK1_Field_Service_Kit'] = 'modules/FSK1_Field_Service_Kit/FSK1_Field_Service_Kit.php';
$moduleList[] = 'FSK1_Field_Service_Kit';


 
 //WARNING: The contents of this file are auto-generated
$beanList['DHA_PlantillasDocumentos'] = 'DHA_PlantillasDocumentos';
$beanFiles['DHA_PlantillasDocumentos'] = 'modules/DHA_PlantillasDocumentos/DHA_PlantillasDocumentos.php';
$moduleList[] = 'DHA_PlantillasDocumentos';




/* * *******************************************************************************
 * This file is part of KReporter. KReporter is an enhancement developed
 * by Christian Knoll. All rights are (c) 2014 by Christian Knoll
 *
 * This Version of the KReporter is licensed software and may only be used in
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * witten consent of Christian Knoll
 *
 * You can contact us at info@kreporter.org
 * ****************************************************************************** */

if (!isset($GLOBALS['sugar_config']['addAjaxBannedModules'])
    || !is_array($GLOBALS['sugar_config']['addAjaxBannedModules'])
) {
    $GLOBALS['sugar_config']['addAjaxBannedModules'] = array();
}


if (!array_search('KReports', $GLOBALS['sugar_config']['addAjaxBannedModules']))
    $GLOBALS['sugar_config']['addAjaxBannedModules'][] = 'KReports';

 
 //WARNING: The contents of this file are auto-generated
$beanList['emqu_dtbc_Email_Queue'] = 'emqu_dtbc_Email_Queue';
$beanFiles['emqu_dtbc_Email_Queue'] = 'modules/emqu_dtbc_Email_Queue/emqu_dtbc_Email_Queue.php';
$moduleList[] = 'emqu_dtbc_Email_Queue';


 
 //WARNING: The contents of this file are auto-generated
$beanList['ActOn_HotProspects'] = 'ActOn_HotProspects';
$beanFiles['ActOn_HotProspects'] = 'modules/ActOn_HotProspects/ActOn_HotProspects.php';
$moduleList[] = 'ActOn_HotProspects';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_System_Workflow_Scheduling'] = 'dtbc_System_Workflow_Scheduling';
$beanFiles['dtbc_System_Workflow_Scheduling'] = 'modules/dtbc_System_Workflow_Scheduling/dtbc_System_Workflow_Scheduling.php';
$moduleList[] = 'dtbc_System_Workflow_Scheduling';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Line_Item'] = 'dtbc_Line_Item';
$beanFiles['dtbc_Line_Item'] = 'modules/dtbc_Line_Item/dtbc_Line_Item.php';
$moduleList[] = 'dtbc_Line_Item';


 

global $sugar_version;
if (version_compare($sugar_version, '7.0.0', '>=')) {
   global $bwcModules;
   $bwcModules[] = 'DHA_PlantillasDocumentos';
}


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Case_Routing_Rules'] = 'dtbc_Case_Routing_Rules';
$beanFiles['dtbc_Case_Routing_Rules'] = 'modules/dtbc_Case_Routing_Rules/dtbc_Case_Routing_Rules.php';
$moduleList[] = 'dtbc_Case_Routing_Rules';


 
 //WARNING: The contents of this file are auto-generated
$beanList['AP1_Alternative_Part'] = 'AP1_Alternative_Part';
$beanFiles['AP1_Alternative_Part'] = 'modules/AP1_Alternative_Part/AP1_Alternative_Part.php';
$moduleList[] = 'AP1_Alternative_Part';


 
 //WARNING: The contents of this file are auto-generated
$beanList['SPADI_SPA_Distributor'] = 'SPADI_SPA_Distributor';
$beanFiles['SPADI_SPA_Distributor'] = 'modules/SPADI_SPA_Distributor/SPADI_SPA_Distributor.php';
$moduleList[] = 'SPADI_SPA_Distributor';


 
 //WARNING: The contents of this file are auto-generated
$beanList['ONB_Onboarding'] = 'ONB_Onboarding';
$beanFiles['ONB_Onboarding'] = 'modules/ONB_Onboarding/ONB_Onboarding.php';
$moduleList[] = 'ONB_Onboarding';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Project_Stakeholders'] = 'dtbc_Project_Stakeholders';
$beanFiles['dtbc_Project_Stakeholders'] = 'modules/dtbc_Project_Stakeholders/dtbc_Project_Stakeholders.php';
$moduleList[] = 'dtbc_Project_Stakeholders';


 
 //WARNING: The contents of this file are auto-generated
$beanList['SN1_Serial_Number'] = 'SN1_Serial_Number';
$beanFiles['SN1_Serial_Number'] = 'modules/SN1_Serial_Number/SN1_Serial_Number.php';
$moduleList[] = 'SN1_Serial_Number';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Project_Comments'] = 'dtbc_Project_Comments';
$beanFiles['dtbc_Project_Comments'] = 'modules/dtbc_Project_Comments/dtbc_Project_Comments.php';
$moduleList[] = 'dtbc_Project_Comments';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_CasesSurvey'] = 'dtbc_CasesSurvey';
$beanFiles['dtbc_CasesSurvey'] = 'modules/dtbc_CasesSurvey/dtbc_CasesSurvey.php';
$moduleList[] = 'dtbc_CasesSurvey';


 
 //WARNING: The contents of this file are auto-generated
$beanList['SPA_SPA'] = 'SPA_SPA';
$beanFiles['SPA_SPA'] = 'modules/SPA_SPA/SPA_SPA.php';
$moduleList[] = 'SPA_SPA';


 
 //WARNING: The contents of this file are auto-generated
$beanList['TLA1_Training_LMS_Absorb'] = 'TLA1_Training_LMS_Absorb';
$beanFiles['TLA1_Training_LMS_Absorb'] = 'modules/TLA1_Training_LMS_Absorb/TLA1_Training_LMS_Absorb.php';
$moduleList[] = 'TLA1_Training_LMS_Absorb';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Emails_Blacklist'] = 'dtbc_Emails_Blacklist';
$beanFiles['dtbc_Emails_Blacklist'] = 'modules/dtbc_Emails_Blacklist/dtbc_Emails_Blacklist.php';
$moduleList[] = 'dtbc_Emails_Blacklist';


 
 //WARNING: The contents of this file are auto-generated
$beanList['CI1_Competitive_Intelligence'] = 'CI1_Competitive_Intelligence';
$beanFiles['CI1_Competitive_Intelligence'] = 'modules/CI1_Competitive_Intelligence/CI1_Competitive_Intelligence.php';
$moduleList[] = 'CI1_Competitive_Intelligence';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Assignment_Rules'] = 'dtbc_Assignment_Rules';
$beanFiles['dtbc_Assignment_Rules'] = 'modules/dtbc_Assignment_Rules/dtbc_Assignment_Rules.php';
$moduleList[] = 'dtbc_Assignment_Rules';


 
 //WARNING: The contents of this file are auto-generated
$beanList['P1_Project'] = 'P1_Project';
$beanFiles['P1_Project'] = 'modules/P1_Project/P1_Project.php';
$moduleList[] = 'P1_Project';


 
 //WARNING: The contents of this file are auto-generated
$beanList['AL1_Alliance_Transaction'] = 'AL1_Alliance_Transaction';
$beanFiles['AL1_Alliance_Transaction'] = 'modules/AL1_Alliance_Transaction/AL1_Alliance_Transaction.php';
$moduleList[] = 'AL1_Alliance_Transaction';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Approvals'] = 'dtbc_Approvals';
$beanFiles['dtbc_Approvals'] = 'modules/dtbc_Approvals/dtbc_Approvals.php';
$moduleList[] = 'dtbc_Approvals';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Pricebook'] = 'dtbc_Pricebook';
$beanFiles['dtbc_Pricebook'] = 'modules/dtbc_Pricebook/dtbc_Pricebook.php';
$moduleList[] = 'dtbc_Pricebook';


 
 //WARNING: The contents of this file are auto-generated
$beanList['POS1_POS_Tracking'] = 'POS1_POS_Tracking';
$beanFiles['POS1_POS_Tracking'] = 'modules/POS1_POS_Tracking/POS1_POS_Tracking.php';
$moduleList[] = 'POS1_POS_Tracking';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_StatusLog'] = 'dtbc_StatusLog';
$beanFiles['dtbc_StatusLog'] = 'modules/dtbc_StatusLog/dtbc_StatusLog.php';
$moduleList[] = 'dtbc_StatusLog';


 
 //WARNING: The contents of this file are auto-generated
$beanList['IS1_Inside_Sales'] = 'IS1_Inside_Sales';
$beanFiles['IS1_Inside_Sales'] = 'modules/IS1_Inside_Sales/IS1_Inside_Sales.php';
$moduleList[] = 'IS1_Inside_Sales';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Pricebook_Product'] = 'dtbc_Pricebook_Product';
$beanFiles['dtbc_Pricebook_Product'] = 'modules/dtbc_Pricebook_Product/dtbc_Pricebook_Product.php';
$moduleList[] = 'dtbc_Pricebook_Product';


 
 //WARNING: The contents of this file are auto-generated
$beanList['S1_Site'] = 'S1_Site';
$beanFiles['S1_Site'] = 'modules/S1_Site/S1_Site.php';
$moduleList[] = 'S1_Site';


 
 //WARNING: The contents of this file are auto-generated
$beanList['UZC1_USA_ZIP_Codes'] = 'UZC1_USA_ZIP_Codes';
$beanFiles['UZC1_USA_ZIP_Codes'] = 'modules/UZC1_USA_ZIP_Codes/UZC1_USA_ZIP_Codes.php';
$moduleList[] = 'UZC1_USA_ZIP_Codes';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Year_Alliance_Points'] = 'dtbc_Year_Alliance_Points';
$beanFiles['dtbc_Year_Alliance_Points'] = 'modules/dtbc_Year_Alliance_Points/dtbc_Year_Alliance_Points.php';
$moduleList[] = 'dtbc_Year_Alliance_Points';


 
 //WARNING: The contents of this file are auto-generated
$beanList['dtbc_Login_Logger'] = 'dtbc_Login_Logger';
$beanFiles['dtbc_Login_Logger'] = 'modules/dtbc_Login_Logger/dtbc_Login_Logger.php';
$moduleList[] = 'dtbc_Login_Logger';


?>