<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CountryList
{
    const Afghanistan = 2;
    const Albania = 3;
    const Algeria = 4;
    const AmericanSamoa = 5;
    const Andorra = 6;
    const Angola = 7;
    const Anguilla = 8;
    const Antigua = 9;
    const Argentina = 10;
    const Armenia = 11;
    const Aruba = 12;
    const Australia = 13;
    const Austria = 14;
    const Azerbaijan = 15;
    const Bahamas = 16;
    const Bahrain = 17;
    const Bangladesh = 18;
    const Barbados = 19;
    const Barbuda = 20;
    const Belarus = 21;
    const Belgium = 22;
    const Belize = 23;
    const Benin = 24;
    const Bermuda = 25;
    const Bhutan = 26;
    const Bolivia = 27;
    const Bonaire = 28;
    const BosniaHerzegovina = 29;
    const Botswana = 30; 
    const Brazil = 31;
    const BritishVirginIsland = 32;
    const Brunei = 33;
    const Bulgaria = 34;
    const BurkinaFaso = 35;
    const Burundi = 36;
    const Cambodia = 37;
    const Cameroon = 38;
    const CapeVerde = 40;
    const CaymanIsland = 41;
    const CentralAfricanRepublic = 42;
    const Chad = 43;
    const ChannelIslands = 44;
    const Chile = 45;
    const China = 46;
    const Colombia = 47;
    const Comoros = 48;
    const Congo = 49;
    const CongoBrazzaville = 50;
    const CostaRica = 51;
    const Côted = 52;
    const Croatia = 53;
    const Cuba = 54;
    const Cyprus = 55;
    const CzechRepublic = 56;
    const Denmark = 57;
    const Djibouti = 58;
    const Dominica = 59;
    const DominicanRepublic = 60;
    const Ecuador = 61;
    const Egypt = 62;
    const ElSalvador = 63;
    const EquatorialGuinea = 64;
    const Eritrea = 65;
    const Estonia = 66;
    const Ethiopia = 67;
    const Fiji = 68;
    const Finland = 69;
    const France = 70;
    const FrenchGuinea = 71;
    const Gabon = 72;
    const Gambia = 73;
    const Georgia = 74;
    const Germany = 75;
    const Ghana = 76;
    const Gibraltar = 77;
    const Greece = 78;
    const Greenland = 80;
    const Grenada = 81;
    const Guatemala = 82;
    const Guinea = 83;
    const Guyana = 84;
    const Haiti = 85;
    const Honduras = 86;
    const HongKong = 87;
    const Hungary = 88;
    const Iceland = 90;
    const India = 91;
    const Indonesia = 92;
    const Iran = 93;
    const Iraq = 94;
    const Ireland = 95;
    const Israel = 96;
    const Italy = 97;
    const IvoryCoast = 98;
    const Jamaica = 99;
    const Japan = 100;
    const Jordan = 101;
    const Kazakhstan = 102;
    const Kenya = 103;
    const Kuwait = 104;
    const Kyrgyzstan = 105;
    const Laos = 106;
    const Latvia = 107;
    const Lebanon = 108;
    const Lesotho = 109;
    const Liberia = 110;
    const Libya = 111;
    const Liechtenstein = 112;
    const Lithuania = 113;
    const Luxembourg = 114;
    const Macedonia = 115;
    const Madagascar = 116;
    const Malawi = 117;
    const Malaysia = 118;
    const Maldives = 119;
    const Mali = 120;
    const Malta = 121;
    const MarshallIslands = 122;
    const Martinique = 123;
    const Mauritania = 124;
    const Mauritius = 125;
    const Mexico = 126;
    const Micronesia = 127;
    const Moldova = 128;
    const Monaco = 129;
    const Mongolia = 130;
    const Montenegro = 131;
    const Monsterrat = 132;
    const Morocco = 133;
    const Mozambique = 134;
    const MyanmarBurma = 135;
    const Namibia = 136;
    const Nauru = 137;
    const Nepal = 138;
    const Netherlands = 139;
    const NewZealand = 140;
    const Nicaragua = 141;
    const Niger = 142;
    const Nigeria = 143;
    const NorthKorea = 144;
    const Norway = 145;
    const Oman = 146;
    const Pakistan = 147;
    const Palau = 148;
    const Panama = 149;
    const PapuaNewGuinea = 150;
    const Paraguay = 151;
    const Peru = 152;
    const Philippines = 153;
    const Poland = 154;
    const Portugal = 155;
    const PuertoRico = 156;
    const Qatar = 157;
    const Réunion = 158;
    const Romania = 159;
    const Russia = 160;
    const Rwanda = 161;
    const SanMarino = 162;
    const SaudiArabia = 163;
    const Senegal = 164;
    const Serbia = 165;
    const Seychelles = 166;
    const SierraLeone = 167;
    const Singapore = 168;
    const SlovakRepublic = 169;
    const Slovenia = 170;
    const SolomonIslands = 171;
    const Somalia = 172;
    const SouthAfrica = 173;
    const SouthKorea = 174;
    const Spain = 175;
    const SriLanka = 176;
    const StKittisandNevis = 177;
    const StLucia = 178;
    const StMartin = 179;
    const StVincent = 180;
    const Sudan = 181;
    const Suriname = 182;
    const Swaziland = 183;
    const Sweden = 184;
    const Switzerland = 185;
    const Syria = 186;
    const Taiwan = 187;
    const Tajikistan = 188;
    const Tanzania = 189;
    const Thailand = 190;
    const Togo = 191;
    const Tonga = 192;
    const Tortola = 193;
    const TrinidadandTobago = 194;
    const Tunisia = 195;
    const Turkey = 196;
    const Turkmenistan = 197;
    const Tuvalu = 198;
    const Uganda = 199;
    const Ukraine = 200;
    const UnitedArabEmirates = 201;
    const UnitedKingdom = 202;
    const Uruguay = 203;
    const Uzbekistan = 204;
    const Vanuatu = 205;
    const VaticanCity = 206;
    const Venezuela = 207;
    const Vietnam = 208;
    const WesternSahara = 209;
    const Yemen = 210;
    const Zambia = 211;
    const Canada = 'Canada';
    
}
function getCountryId($country)
{
    if($country == "Afghanistan") $country = CountryList::Afghanistan;
    if($country == "Albania") $country = CountryList::Albania;
    if($country == "Algeria") $country = CountryList::Algeria;
    if($country == "AmericanSamoa") $country = CountryList::AmericanSamoa;
    if($country == "Andorra") $country = CountryList::Andorra;
    if($country == "Angola") $country = CountryList::Angola;
    if($country == "Anguilla") $country = CountryList::Anguilla;
    if($country == "Antigua") $country = CountryList::Antigua;
    if($country == "Argentina") $country = CountryList::Argentina;
    if($country == "Armenia") $country = CountryList::Armenia;
    if($country == "Aruba") $country = CountryList::Aruba;
    if($country == "Australia") $country = CountryList::Australia;
    if($country == "Austria") $country = CountryList::Austria;
    if($country == "Azerbaijan") $country = CountryList::Azerbaijan;
    if($country == "Bahamas") $country = CountryList::Bahamas;
    if($country == "Bahrain") $country = CountryList::Bahrain;
    if($country == "Bangladesh") $country = CountryList::Bangladesh;
    if($country == "Barbados") $country = CountryList::Barbados;
    if($country == "Barbuda") $country = CountryList::Barbuda;
    if($country == "Belarus") $country = CountryList::Belarus;
    if($country == "Belgium") $country = CountryList::Belgium;
    if($country == "Belize") $country = CountryList::Belize;
    if($country == "Benin") $country = CountryList::Benin;
    if($country == "Bermuda") $country = CountryList::Bermuda;
    if($country == "Bhutan") $country = CountryList::Bhutan;
    if($country == "Bolivia") $country = CountryList::Bolivia;
    if($country == "Bonaire") $country = CountryList::Bonaire;
    if($country == "BosniaHerzegovina") $country = CountryList::BosniaHerzegovina;
    if($country == "Botswana") $country = CountryList::Botswana;
    if($country == "Brazil") $country = CountryList::Brazil;
    if($country == "BritishVirginIsland") $country = CountryList::BritishVirginIsland;
    if($country == "Brunei") $country = CountryList::Brunei;
    if($country == "Bulgaria") $country = CountryList::Bulgaria;
    if($country == "BurkinaFaso") $country = CountryList::BurkinaFaso;
    if($country == "Burundi") $country = CountryList::Burundi;
    if($country == "Cambodia") $country = CountryList::Cambodia;
    if($country == "Cameroon") $country = CountryList::Cameroon;
    if($country == "CapeVerde") $country = CountryList::CapeVerde;
    if($country == "CaymanIsland") $country = CountryList::CaymanIsland;
    if($country == "CentralAfricanRepublic") $country = CountryList::CentralAfricanRepublic;
    if($country == "Chad") $country = CountryList::Chad;
    if($country == "ChannelIslands") $country = CountryList::ChannelIslands;
    if($country == "Chile") $country = CountryList::Chile;
    if($country == "China") $country = CountryList::China;
    if($country == "Colombia") $country = CountryList::Colombia;
    if($country == "Comoros") $country = CountryList::Comoros;
    if($country == "Congo") $country = CountryList::Congo;
    if($country == "CongoBrazzaville") $country = CountryList::CongoBrazzaville;
    if($country == "CostaRica") $country = CountryList::CostaRica;
    if($country == "Côted") $country = CountryList::Côted;
    if($country == "Croatia") $country = CountryList::Croatia;
    if($country == "Cuba") $country = CountryList::Cuba;
    if($country == "Cyprus") $country = CountryList::Cyprus;
    if($country == "CzechRepublic") $country = CountryList::CzechRepublic;
    if($country == "Denmark") $country = CountryList::Denmark;
    if($country == "Djibouti") $country = CountryList::Djibouti;
    if($country == "Dominica") $country = CountryList::Dominica;
    if($country == "DominicanRepublic") $country = CountryList::DominicanRepublic;
    if($country == "Ecuador") $country = CountryList::Ecuador;
    if($country == "Egypt") $country = CountryList::Egypt;
    if($country == "ElSalvador") $country = CountryList::ElSalvador;
    if($country == "EquatorialGuinea") $country = CountryList::EquatorialGuinea;
    if($country == "Eritrea") $country = CountryList::Eritrea;
    if($country == "Estonia") $country = CountryList::Estonia;
    if($country == "Ethiopia") $country = CountryList::Ethiopia;
    if($country == "Fiji") $country = CountryList::Fiji;
    if($country == "Finland") $country = CountryList::Finland;
    if($country == "France") $country = CountryList::France;
    if($country == "FrenchGuinea") $country = CountryList::FrenchGuinea;
    if($country == "Gabon") $country = CountryList::Gabon;
    if($country == "Gambia") $country = CountryList::Gambia;
    if($country == "Georgia") $country = CountryList::Georgia;
    if($country == "Germany") $country = CountryList::Germany;
    if($country == "Ghana") $country = CountryList::Ghana;
    if($country == "Gibraltar") $country = CountryList::Gibraltar;
    if($country == "Greece") $country = CountryList::Greece;
    if($country == "Greenland") $country = CountryList::Greenland;
    if($country == "Grenada") $country = CountryList::Grenada;
    if($country == "Guatemala") $country = CountryList::Guatemala;
    if($country == "Guinea") $country = CountryList::Guinea;
    if($country == "Guyana") $country = CountryList::Guyana;
    if($country == "Haiti") $country = CountryList::Haiti;
    if($country == "Honduras") $country = CountryList::Honduras;
    if($country == "HongKong") $country = CountryList::HongKong;
    if($country == "Hungary") $country = CountryList::Hungary;
    if($country == "Iceland") $country = CountryList::Iceland;
    if($country == "India") $country = CountryList::India;
    if($country == "Indonesia") $country = CountryList::Indonesia;
    if($country == "Iran") $country = CountryList::Iran;
    if($country == "Iraq") $country = CountryList::Iraq;
    if($country == "Ireland") $country = CountryList::Ireland;
    if($country == "Israel") $country = CountryList::Israel;
    if($country == "Italy") $country = CountryList::Italy;
    if($country == "IvoryCoast") $country = CountryList::IvoryCoast;
    if($country == "Jamaica") $country = CountryList::Jamaica;
    if($country == "Japan") $country = CountryList::Japan;
    if($country == "Jordan") $country = CountryList::Jordan;
    if($country == "Kazakhstan") $country = CountryList::Kazakhstan;
    if($country == "Kenya") $country = CountryList::Kenya;
    if($country == "Kuwait") $country = CountryList::Kuwait;
    if($country == "Kyrgyzstan") $country = CountryList::Kyrgyzstan;
    if($country == "Laos") $country = CountryList::Laos;
    if($country == "Latvia") $country = CountryList::Latvia;
    if($country == "Lebanon") $country = CountryList::Lebanon;
    if($country == "Lesotho") $country = CountryList::Lesotho;
    if($country == "Liberia") $country = CountryList::Liberia;
    if($country == "Libya") $country = CountryList::Libya;
    if($country == "Liechtenstein") $country = CountryList::Liechtenstein;
    if($country == "Lithuania") $country = CountryList::Lithuania;
    if($country == "Luxembourg") $country = CountryList::Luxembourg;
    if($country == "Macedonia") $country = CountryList::Macedonia;
    if($country == "Madagascar") $country = CountryList::Madagascar;
    if($country == "Malawi") $country = CountryList::Malawi;
    if($country == "Malaysia") $country = CountryList::Malaysia;
    if($country == "Maldives") $country = CountryList::Maldives;
    if($country == "Mali") $country = CountryList::Mali;
    if($country == "Malta") $country = CountryList::Malta;
    if($country == "MarshallIslands") $country = CountryList::MarshallIslands;
    if($country == "Martinique") $country = CountryList::Martinique;



    if($country == "Mauritania") $country = CountryList::Mauritania;
    if($country == "Mauritius") $country = CountryList::Mauritius;
    if($country == "Mexico") $country = CountryList::Mexico;
    if($country == "Micronesia") $country = CountryList::Micronesia;
    if($country == "Moldova") $country = CountryList::Moldova;
    if($country == "Monaco") $country = CountryList::Monaco;
    if($country == "Mongolia") $country = CountryList::Mongolia;
    if($country == "Montenegro") $country = CountryList::Montenegro;
    if($country == "Monsterrat") $country = CountryList::Monsterrat;
    if($country == "Morocco") $country = CountryList::Morocco;
    if($country == "Mozambique") $country = CountryList::Mozambique;
    if($country == "MyanmarBurma") $country = CountryList::MyanmarBurma;
    if($country == "Namibia") $country = CountryList::Namibia;
    if($country == "Nauru") $country = CountryList::Nauru;
    if($country == "Nepal") $country = CountryList::Nepal;
    if($country == "Netherlands") $country = CountryList::Netherlands;
    if($country == "Niger") $country = CountryList::Niger;
    if($country == "Nigeria") $country = CountryList::Nigeria;
    if($country == "NorthKorea") $country = CountryList::NorthKorea;
    if($country == "Norway") $country = CountryList::Norway;
    if($country == "Oman") $country = CountryList::Oman;
    if($country == "Pakistan") $country = CountryList::Pakistan;
    if($country == "Palau") $country = CountryList::Palau;
    if($country == "Panama") $country = CountryList::Panama;
    if($country == "PapuaNewGuinea") $country = CountryList::PapuaNewGuinea;
    if($country == "Paraguay") $country = CountryList::Paraguay;
    if($country == "Peru") $country = CountryList::Peru;
    if($country == "Philippines") $country = CountryList::Philippines;
    if($country == "Poland") $country = CountryList::Poland;
    if($country == "Portugal") $country = CountryList::Portugal;
    if($country == "PuertoRico") $country = CountryList::PuertoRico;
    if($country == "Qatar") $country = CountryList::Qatar;
    if($country == "Réunion") $country = CountryList::Réunion;
    if($country == "Romania") $country = CountryList::Romania;
    if($country == "Russia") $country = CountryList::Russia;

    if($country == "Rwanda") $country = CountryList::Rwanda;
    if($country == "SanMarino") $country = CountryList::SanMarino;
    if($country == "SaudiArabia") $country = CountryList::SaudiArabia;
    if($country == "Senegal") $country = CountryList::Senegal;
    if($country == "Serbia") $country = CountryList::Serbia;
    if($country == "Seychelles") $country = CountryList::Seychelles;
    if($country == "SierraLeone") $country = CountryList::SierraLeone;
    if($country == "Singapore") $country = CountryList::Singapore;
    if($country == "SlovakRepublic") $country = CountryList::SlovakRepublic;
    if($country == "Slovenia") $country = CountryList::Slovenia;
    if($country == "SolomonIslands") $country = CountryList::SolomonIslands;
    if($country == "Somalia") $country = CountryList::Somalia;
    if($country == "SouthAfrica") $country = CountryList::SouthAfrica;
    if($country == "SouthKorea") $country = CountryList::SouthKorea;
    if($country == "Spain") $country = CountryList::Spain;
    if($country == "SriLanka") $country = CountryList::SriLanka;
    if($country == "StKittisandNevis") $country = CountryList::StKittisandNevis;
    if($country == "StLucia") $country = CountryList::StLucia;
    if($country == "StMartin") $country = CountryList::StMartin;
    if($country == "StVincent") $country = CountryList::StVincent;
    if($country == "Sudan") $country = CountryList::Sudan;
    if($country == "Suriname") $country = CountryList::Suriname;
    if($country == "Swaziland") $country = CountryList::Swaziland;
    if($country == "Sweden") $country = CountryList::Sweden;
    if($country == "Switzerland") $country = CountryList::Switzerland;
    if($country == "Syria") $country = CountryList::Syria;
    if($country == "Taiwan") $country = CountryList::Taiwan;
    if($country == "Tajikistan") $country = CountryList::Tajikistan;
    if($country == "Tanzania") $country = CountryList::Tanzania;
    if($country == "Thailand") $country = CountryList::Thailand;
    if($country == "Togo") $country = CountryList::Togo;
    if($country == "Tonga") $country = CountryList::Tonga;
    if($country == "Tortola") $country = CountryList::Tortola;
    if($country == "TrinidadandTobago") $country = CountryList::TrinidadandTobago;
    if($country == "Tunisia") $country = CountryList::Tunisia;
    if($country == "Turkey") $country = CountryList::Turkey;
    if($country == "Turkmenistan") $country = CountryList::Turkmenistan;
    if($country == "Tuvalu") $country = CountryList::Tuvalu;
    if($country == "Uganda") $country = CountryList::Uganda;
    if($country == "Ukraine") $country = CountryList::Ukraine;
    if($country == "UnitedArabEmirates") $country = CountryList::UnitedArabEmirates;
    if($country == "UnitedKingdom") $country = CountryList::UnitedKingdom;
    if($country == "Uruguay") $country = CountryList::Uruguay;
    if($country == "Uzbekistan") $country = CountryList::Uzbekistan;
    if($country == "Vanuatu") $country = CountryList::Vanuatu;
    if($country == "VaticanCity") $country = CountryList::VaticanCity;
    if($country == "Vietnam") $country = CountryList::Vietnam;
    if($country == "Venezuela") $country = CountryList::Venezuela;
    if($country == "WesternSahara") $country = CountryList::WesternSahara;
    if($country == "Yemen") $country = CountryList::Yemen;
    if($country == "Zambia") $country = CountryList::Zambia;
    if($country == "Canada") $country = CountryList::Canada;

    return $country;
}