<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getCaseTypeById($name, $session_id, $url)
{
    $get_module_fields_parameters = array(

         //session id
         'session' => $session_id,

         //The name of the module from which to retrieve records
         'module_name' => 'Cases',

         //Optional. Returns vardefs for the specified fields. An empty array will return all fields.
         'fields' => array(
             'case_type_c',
         ),
    );
    $get_module_fields_result = call("get_module_fields", $get_module_fields_parameters, $url);
    $caseTypeCode = $get_module_fields_result->module_fields->case_type_c->options;
    foreach($caseTypeCode as $typeCode)
    {
        if($typeCode->value == $name)
        {
            $key = $typeCode->name;
        }
    }
    return $key;
}

function getCaseCategoryByName($name, $session_id, $url)
{
    $get_module_fields_parameters = array(

         //session id
         'session' => $session_id,

         //The name of the module from which to retrieve records
         'module_name' => 'Cases',

         //Optional. Returns vardefs for the specified fields. An empty array will return all fields.
         'fields' => array(
             'symptom_c',
         ),
    );
    $get_module_fields_result = call("get_module_fields", $get_module_fields_parameters, $url);
    $caseCategories = $get_module_fields_result->module_fields->symptom_c->options;
    foreach($caseCategories as $category)
    {
        if($category->value == $name)
        {
            $key = $category->name;
        }
    }
    return $key;
}

function getCaseSubCategoryByName($name, $session_id, $url)
{
    $get_module_fields_parameters = array(

         //session id
         'session' => $session_id,

         //The name of the module from which to retrieve records
         'module_name' => 'Cases',

         //Optional. Returns vardefs for the specified fields. An empty array will return all fields.
         'fields' => array(
             'sub_symptom_c',
         ),
    );
    $get_module_fields_result = call("get_module_fields", $get_module_fields_parameters, $url);
    $caseSubCategories = $get_module_fields_result->module_fields->sub_symptom_c->options;
    foreach($caseSubCategories as $subCategory)
    {
        if($subCategory->value == $name)
        {
            $key = $subCategory->name;
        }
    }
    return $key;
}
