
<?php

// specify the REST web service to interact with

//$url = 'http://172.20.101.229/custom/service/v4_1_custom/rest.php';
//$url = 'http://10.0.10.6/custom/service/v4_1_custom/rest.php';
$url = 'http://credge/service/v4_1/rest.php';

$username = "ServicePortalAPI";
$password = "TheHouse123";

//function to make cURL request
function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	//print_r($http_status);
	//print_r($result);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);

//get session id
$session_id = $login_result->id;
//echo $session_id;

$getDepandencesTable_parameters = array(

     //session id
     'session' => $session_id,
); 

//  $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
$get_entry_list_result = call("getDepandencesTable", $getDepandencesTable_parameters, $url);

//echo "<pre>";
//echo 'contact_id = ' . $contact_id;
//print_r($get_entry_list_result);
//echo "</pre>";
header('Content-Type: application/json');
print($get_entry_list_result);
