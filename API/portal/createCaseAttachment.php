<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$url = 'http://172.20.101.229/service/v4_1/rest.php';
//$url = 'http://10.0.10.6/service/v4_1/rest.php';
$url = 'http://credge/service/v4_1/rest.php';

    $username = "ServicePortalAPI";
    $password = "TheHouse123";

    //function to make cURL request
    function call($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //login ----------------------------------------------------- 
    $login_parameters = array(
         "user_auth" => array(
              "user_name" => $username,
              "password" => md5($password),
              "version" => "1"
         ),
         "application_name" => "RestTest",
         "name_value_list" => array(),
    );

    $login_result = call("login", $login_parameters, $url);

    /*
    echo "<pre>";
    print_r($login_result);
    echo "</pre>";
    */

    //get session id
    $session_id = $login_result->id;

if (isset($_GET['FileName'])) {
    $fileName = $_GET['FileName'];
}
if (isset($_GET['FilePath'])) {
    $filePath = $_GET['FilePath'];
}
if (isset($_GET['CaseId'])) {
    $caseId = $_GET['CaseId'];
}


//echo $DocId;
if($filePath != '' && $caseId != '')
{
    
      //create document -------------------------------------------- 
    $set_entry_parameters = array(
        //session id
        "session" => $session_id,

        //The name of the module
        "module_name" => "Documents",

        //Record attributes
        "name_value_list" => array(
            //to update a record, pass in a record id as commented below
            //array("name" => "id", "value" => "9b170af9-3080-e22b-fbc1-4fea74def88f"),
            array("name" => "document_name", "value" => $fileName),
            array("name" => "revision", "value" => "1"),
        ),
    );

    $set_entry_result = call("set_entry", $set_entry_parameters, $url);

    //echo "<pre>";
    //print_r($set_entry_result);
    //echo "</pre>";
    header('Content-Type: application/json');
    print(json_encode($set_entry_result));
    $document_id = $set_entry_result->id;

    //create document revision ------------------------------------ 
    //$contents = file_get_contents ("/path/to/example_document.txt");
    $contents = file_get_contents ($filePath);

    $set_document_revision_parameters = array(
        //session id
        "session" => $session_id,

        //The attachment details
        "note" => array(
            //The ID of the parent document.
            'id' => $document_id,

            //The binary contents of the file.
            'file' => base64_encode($contents),

            //The name of the file
            'filename' => $fileName,

            //The revision number
            'revision' => '1',
            
            //'case_id' => $CaseId,
        ),
    );

    $set_document_revision_result = call("set_document_revision", $set_document_revision_parameters, $url);

    //echo "<pre>";
    //print_r($set_document_revision_result);
    //echo "</pre>";
    //echo 'CaseId=' . $caseId;
    print(json_encode($set_document_revision_result));
/*********************************************************************/
    // Connect Document to Case
    $set_relationship_parameters = array(
        //session id
        'session' => $session_id,
        //The name of the module.
        'module_name' => 'Documents',
        //The ID of the specified module bean.
        'module_id' => $document_id,
        //The relationship name of the linked field from which to relate records.
        'link_field_name' => 'cases',
        //The list of record ids to relate
        'related_ids' => array(
            $caseId,
        ),
        //Whether or not to delete the relationship. 0:create, 1:delete
        'delete'=> 0,
    );
                
    $result = call("set_relationship", $set_relationship_parameters, $url);
    //echo "<pre>";
    //print_r($result);
    //echo "</pre>";
    print(json_encode($result));
/***************************************************************************/    
    
    
}