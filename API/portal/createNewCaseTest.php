<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../getDropDownValue.php';

// specify the REST web service to interact with
//$url = 'http://172.20.101.229/service/v4_1/rest.php';
$url = 'http://credge/custom/service/v4_1_custom/rest.php';
//$url = 'http://10.0.10.6/service/v4_1/rest.php';
//$url = 'http://localhost:8080/SuiteCRM/service/v4_1/rest.php';


$status = false;
$username = "ServicePortalAPI";
$password = "TheHouse123";
//$username = "admin";
//$password = "adm!n";

//function to make cURL request
function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	//print_r($http_status);
	//print_r($result);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);

//echo "<pre>";
//print_r($login_result);
//echo "</pre>";

//get session id
$session_id = $login_result->id;

//echo $session_id;

		// URL Parameters
if (isset($_GET['AccountId'])) {
    $accountId = $_GET['AccountId'];
}
if (isset($_GET['ContactId'])) {
    $contactId = $_GET['ContactId'];
}
if (isset($_GET['Subject'])) {
    $subject = $_GET['Subject'];
}
if (isset($_GET['Description'])) {
    $desc = $_GET['Description'];
}
if (isset($_GET['CaseType'])) {
    $caseType = $_GET['CaseType'];
}
if (isset($_GET['Category'])) {
    $category = $_GET['Category'];
}
if (isset($_GET['SubCategory'])) {
    $subCategory = $_GET['SubCategory'];
}
if (isset($_GET['Severity'])) {
    $severity = $_GET['Severity'];
    $severity = $severity[0];
}
if (isset($_GET['MonitoringSiteName'])) {
    $mSiteName = $_GET['MonitoringSiteName'];
}
if (isset($_GET['ErrorCodeNumber'])) {
    $errCodeNo = $_GET['ErrorCodeNumber'];
}
if (isset($_GET['InverterSerial'])) {
    $inverterSerial = $_GET['InverterSerial'];
}
if (isset($_GET['SerialNumbers'])) {
    $serialNumbers = $_GET['SerialNumbers'];
}

//$caseTypeId = getCaseTypeById($caseType, $session_id, $url);
//$categoryId = getCaseCategoryByName($category, $session_id, $url);
//$subCategoryId = getCaseSubCategoryByName($subCategory, $session_id, $url);

$caseTypeId = $caseType;
$categoryId = $category;
$subCategoryId = $subCategory;


//echo 'Account Id = ' . $accountId . '<BR/>';
//echo 'Subject = ' . $subject . '<BR/> CaseTypeID' . $caseTypeId;

//echo 'Description = ' . $desc . '<BR/>';
//echo 'CaseType = ' . $caseType . '<BR/>';
//echo 'Category = ' . $category . '<BR/>';
//echo 'SubCategory = ' . $subCategory . '<BR/>';
//echo 'Severity = ' . $severity . '<BR/>';
//echo 'MonitoringSiteName = ' . $mSiteName . '<BR/>';
//echo 'ErrorCodeNumber = ' . $errCodeNo . '<BR/>';
//echo 'InverterSerial = ' . $inverterSerial . '<BR/>';
//echo 'SerialNumbers = ' . $serialNumbers . '<BR/>';

$serialNumbersArr = explode(",", $serialNumbers);
//echo 'ARR[0]' . $serialNumbersArr[0] . '<BR/>';
//echo 'ARR[2]' . $serialNumbersArr[2] ;

if($accountId != '' && $subject != '' && $caseTypeId != '')
{
    //Create Case
    $set_entry_parameters = array(
        "session" => $session_id,
        "module_name" => "Cases",

        //Record attributes
        "name_value_list" => array(
            array("name" => "name", "value" => $subject),
            array("name" => "description", "value" => $desc),
            array("name" => "case_type_c", "value" => $caseTypeId),
            array("name" => "symptom_c", "value" => $categoryId),
            array("name" => "sub_symptom_c", "value" => $subCategoryId),
            array("name" => "severity_c", "value" => $severity),
            array("name" => "case_site_c", "value" => $mSiteName),
            array("name" => "error_code_number_c", "value" => $errCodeNo),
            array("name" => "inverter_serial_c", "value" => $inverterSerial),
            array("name" => "case_origin_c", "value" => "4" ),
            array("name" => "doa_c", "value" => "2" ),
            array("name" => "account_id", "value" => $accountId ),
            array("name" => "contact_created_by_id", "value" => $contactId ),
        ),
    );
    $set_entry_result = call("set_entry", $set_entry_parameters, $url);
    //echo "<pre>";
    //print_r($set_entry_result);
    //echo "</pre>";
    $caseid = $set_entry_result->id;
    
    echo "Serial" . $caseid;
    
    /*'sn1_serial_number'*/
    //echo "<pre>";
    //print_r($serialNumbersArr);
    //echo "</pre>";
   
    foreach($serialNumbersArr as $sn)    
    {
        $set_entry_parameters_sn = array(
            "session" => $session_id,
            "module_name" => "SN1_Serial_Number",

            //Record attributes
            "name_value_list" => array(
                array("name" => "name", "value" => $sn),
                array("name" => "acase_id_c", "value" => $caseid),
            ),
        );
        $set_entry_result_sn = call("set_entry", $set_entry_parameters_sn, $url);
        echo "<pre>";
        print_r($set_entry_result_sn);
        echo "</pre>";
        $snid = $set_entry_result_sn->id;

        $insertCaseSerial_parameters = array(

           //session id
           'session' => $session_id,
           'caseNo'  => $caseid,
           'serialNo' => $snid,
        ); 

        //  $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
        $get_case_sn_list_result = call("insertNewCaseSerial", $insertCaseSerial_parameters, $url);
        echo "<pre>";
        print_r($get_case_sn_list_result);
        echo "</pre>";
    }
    
    
    //header('Content-Type: application/json');
    //print(json_encode($set_entry_result));
    // Set Related Serials 
    /*
        $set_relationship_parameters = array(
        //session id
        'session' => $session_id,
        //The name of the module.
        'module_name' => 'Documents',
        //The ID of the specified module bean.
        'module_id' => $document_id,
        //The relationship name of the linked field from which to relate records.
        'link_field_name' => 'cases',
        //The list of record ids to relate
        'related_ids' => array(
            $caseId,
        ),
        //Whether or not to delete the relationship. 0:create, 1:delete
        'delete'=> 0,
    );
                
    $result = call("set_relationship", $set_relationship_parameters, $url);
    echo "<pre>";
    print_r($result);
    echo "</pre>";
*/

    
}

