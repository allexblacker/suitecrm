<?php

// specify the REST web service to interact with
//$url = 'http://localhost:8080/SuiteCRM/service/v4_1/rest.php';
//$url = 'http://172.20.101.229/service/v4_1/rest.php';
//$url = 'http://10.0.10.6/service/v4_1/rest.php';
$url = 'http://credge/service/v4_1/rest.php';

$username = "ServicePortalAPI";
$password = "TheHouse123";

//function to make cURL request
function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	//print_r($http_status);
	//print_r($result);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);

//echo "<pre>";
//print_r($login_result);
//echo "</pre>";

//get session id
$session_id = $login_result->id;

//echo $session_id;
if (isset($_GET['DocId'])) {
    $DocId = strtolower($_GET['DocId']);
}
//echo $DocId;
if($DocId != '')
{
    //retrieve documents by Id ------------------------------------------------------ 
    $get_entry_list_parameters = array(

         //session id
         'session' => $session_id,

         //The name of the module from which to retrieve records
         'module_name' => "Documents",
         'query' => "documents.id = '" . $DocId . "'",
         'order_by' => "",

         //The record offset from which to start.
         'offset' => "0",

         //Optional. The list of fields to be returned in the results
         'select_fields' => array('id','name','filename', 'document_revision_id', 'doc_type', 'doc_url'),
		//Show 10 max results
		'max_results' => 10,
		//Do not show deleted
		'deleted' => 0,

    ); 
    $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
    //echo "<pre>";
    //print_r($get_entry_list_result);
    //echo "</pre>";
    //$account_id = $get_entry_list_result->entry_list[0]->name_value_list->account_id->value; 
    header('Content-Type: application/json');
    print(json_encode($get_entry_list_result));

    
}
    //------------------------------------------------------------------------------------------



