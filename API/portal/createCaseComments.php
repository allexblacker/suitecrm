<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$url = 'http://172.20.101.229/service/v4_1/rest.php';
//$url = 'http://10.0.10.6/service/v4_1/rest.php';
$url = 'http://credge/service/v4_1/rest.php';

$username = "ServicePortalAPI";
$password = "TheHouse123";

    //function to make cURL request
    function call($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //login ----------------------------------------------------     
    $login_parameters = array(
         "user_auth" => array(
              "user_name" => $username,
              "password" => md5($password),
              "version" => "1"
         ),
         "application_name" => "RestTest",
         "name_value_list" => array(),
    );

    $login_result = call("login", $login_parameters, $url);

    /*
    echo "<pre>";
    print_r($login_result);
    echo "</pre>";
    */

    //get session id
    $session_id = $login_result->id;
    
if (isset($_GET['CaseId'])) {
    $caseId = $_GET['CaseId'];
}
if (isset($_GET['Comments'])) {
    $cmmt = $_GET['Comments'];
}
if (isset($_GET['FileName'])) {
    $fileName = $_GET['FileName'];
}
if (isset($_GET['FilePath'])) {
    $filePath = $_GET['FilePath'];
}

if($caseId != '')
{
    if($cmmt != '')
    {
            $set_entry_parameters = array(
                 //session id
                 "session" => $session_id,

                 //The name of the module
                 "module_name" => "AOP_Case_Updates",

                 //Record attributes
                 "name_value_list" => array(
                      //to update a record, you will nee to pass in a record id as commented below
                      //array("name" => "id", "value" => "9b170af9-3080-e22b-fbc1-4fea74def88f"),
                      array("name" => "name", "value" => $cmmt),
                      array("name" => "description", "value" => $cmmt),
                      array("name" => "internal", "value" => '0'),
                      array("name" => "case_id", "value" => $caseId),
                      array("name" => "assigned_user_id", "value" => '3d467c31-1f72-48ae-3462-59e7867f8ce5'),
                 ),
            );
            $set_entry_result = call("set_entry", $set_entry_parameters, $url);
            /************************************************/
            /************** Check Case Status ***************/
		$get_case_status_entry_parameters = array(

                    //session id
                    'session' => $session_id,

                    //The name of the module from which to retrieve records
                    'module_name' => "Cases",
                    'query' => " cases.id = '" . $caseId . "'",
                    'order_by' => "",
                    //The record offset from which to start.
                    'offset' => "0",

                    //Optional. The list of fields to be returned in the results
                    'select_fields' => array('id','name', 'case_status_c'),

                    //Show 10 max results
                    'max_results' => 1,
                    //Do not show deleted
                    'deleted' => 0,

		); 

                $get_case_status_entry_result = call("get_entry_list", $get_case_status_entry_parameters, $url);
		//10 - RMA shipped - Waiting for return
		//12 - RMA approved - Waiting for return
                // 8 - Reopen 
		$ststus = $get_case_status_entry_result->entry_list[0]->name_value_list->case_status_c->value;
                if($ststus == '7') //close
                {
                   $sat_val = '8';
                }
                else {
                    if($ststus != 4)
                    {
                        $sat_val = '2';
                    }
                }
            /************************************************/
                    // Update Case Comment
                     $set_case_entry_parameters = array(
                          //session id
                          "session" => $session_id,

                          //The name of the module
                          "module_name" => "Cases",

                          //Record attributes
                          "name_value_list" => array(
                               //to update a record, you will nee to pass in a record id as commented below
                               //array("name" => "id", "value" => "9b170af9-3080-e22b-fbc1-4fea74def88f"),
                               array("name" => "id", "value" => $caseId),
                               array("name" => "alert_override_c", "value" => 'red'),
                               array("name" => "case_status_c", "value" => $sat_val),
                               //array("name" => "new_internal_comment_c", "value" => '1'),
                          ),
                     );
                    if($ststus == '10' || $ststus == '12') 
		    {
                        unset ($set_case_entry_parameters['name_value_list'][2]);
                    }
                    $set_case_entry_result = call("set_entry", $set_case_entry_parameters, $url);

            //echo "<pre>";
            //print_r($set_entry_result);
            //echo "</pre>";
            header('Content-Type: application/json');
            print(json_encode($set_entry_result));
        //echo "3";
            $note_id = $set_entry_result->id;

            
            
            if($filePath != '')
            {
                //create note attachment --------------------------------------     
                $contents = file_get_contents ($filePath);

                $set_note_attachment_parameters = array(
                    //session id
                    "session" => $session_id,

                    //The attachment details
                    "note" => array(
                        //The ID of the note containing the attachment.
                        'id' => $note_id,

                        //The file name of the attachment.
                        'filename' => $fileName,

                        //The binary contents of the file.
                        'file' => base64_encode($contents),
                    ),
                );

                $set_note_attachment_result = call("set_note_attachment", $set_note_attachment_parameters, $url);

                //echo "<pre>";
                //print_r($set_note_attachment_result);
                //echo "</pre>";
                print(json_encode($set_note_attachment_result));
            }
    }
}