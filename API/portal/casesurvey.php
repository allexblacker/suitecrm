<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$url = 'http://credge/service/v4_1/rest.php';
//$url = 'http://localhost:8080/SuiteCRM/service/v4_1/rest.php';
$username = "MonitoringAPI";
$password = "P@ssw0rd";
//$username = "solaredge";
//$password = "vHjo98bH";

function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	//print_r($http_status);
	//print_r($result);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);
//echo "<pre>";
//print_r($login_result);
//echo "</pre>";

//get session id
$session_id = $login_result->id;

$caseId = '';
$lang = 'en';
$subject = '';
$caseNo = '';
if (isset($_GET['caseId'])) {
    $caseId = $_GET['caseId'];
}
if (isset($_GET['lang'])) {
    $lang = $_GET['lang'];
}
if($caseId != '')
{
    /*** ***/
        $get_entry_list_parameters = array(

         //session id
         'session' => $session_id,

         //The name of the module from which to retrieve records
         'module_name' => "Cases",
         'query' => "cases.id = '" . $caseId . "'",
         'order_by' => "",

         //The record offset from which to start.
         'offset' => "0",
        
         //Optional. The list of fields to be returned in the results
         'select_fields' => array('id','name', 'date_entered', 'case_number',),
		//Show 10 max results
		'max_results' => 30,
		//Do not show deleted
		'deleted' => 0,
    ); 
    $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
    
//    echo "<pre>";
//    print_r($get_entry_list_result);
//    echo "</pre>";
    $caseNo = $get_entry_list_result->entry_list[0]->name_value_list->case_number->value;
    $subject = $get_entry_list_result->entry_list[0]->name_value_list->name->value;
//    echo $subject;
}

//if(is_set($_POST['saveSurvey']))
//{
  //  echo "in";
//}
if(isset($_POST['saveSurvey'])){
        if(isset($_POST['caseid'])){
        $caseid_val = $_POST['caseid'];  
        }
        if(isset($_POST['subject'])){
        $sub_val = $_POST['subject'];  
        }
        //echo $caseid_val . $sub_val;
        if(isset($_POST['description'])){
        $desc_val = $_POST['description'];  
        }
        if(isset($_POST['initial_response_time_c'])){
        $resp_val = $_POST['initial_response_time_c'];  
        }
        if(isset($_POST['professionalism_and_service_c'])){
        $prof_val = $_POST['professionalism_and_service_c'];  
        }
        if(isset($_POST['technical_knowledge_c'])){
        $tech_val = $_POST['technical_knowledge_c'];  
        }
        if(isset($_POST['time_until_issue_resolved_c'])){
        $time_val = $_POST['time_until_issue_resolved_c'];  
        }
        if(isset($_POST['time_until_issue_resolved_c'])){
        $time_val = $_POST['time_until_issue_resolved_c'];  
        }
        $set_entry_parameters = array(
                         //session id
                         "session" => $session_id,

                         //The name of the module
                         "module_name" => "dtbc_CasesSurvey",

                         //Record attributes
                         "name_value_list" => array(
                              //to update a record, you will nee to pass in a record id as commented below
                              array("name" => "name", "value" => $sub_val),
                              array("name" => "description", "value" => $desc_val),
                              array("name" => "initial_response_time_c", "value" => $resp_val),
                              array("name" => "professionalism_and_service_c", "value" => $prof_val),
                              array("name" => "time_until_issue_resolved_c", "value" => $time_val),
                              array("name" => "technical_knowledge_c", "value" => $tech_val),
                         ),
                    );
                    $set_entry_result = call("set_entry", $set_entry_parameters, $url);
                    //echo "<pre>";
                    //print_r ($set_entry_parameters);
                    //echo "</pre>";
//                    echo "<pre>";
//                    print_r ($set_entry_result);
//                    echo "</pre>";
                    $surveyid = $set_entry_result->id;

                    // Connect Survey to Case
            $set_relationship_parameters = array(
                //session id
                'session' => $session_id,
                //The name of the module.
                'module_name' => 'dtbc_CasesSurvey',
                //The ID of the specified module bean.
                'module_id' => $surveyid,
                //The relationship name of the linked field from which to relate records.
                'link_field_name' => 'cases_dtbc_casessurvey_1',
                //The list of record ids to relate
                'related_ids' => array(
                    $caseid_val,
                ),
                //Whether or not to delete the relationship. 0:create, 1:delete
                'delete'=> 0,
            );

            $result = call("set_relationship", $set_relationship_parameters, $url);
//                    echo "<pre>";
//                    print_r ($result);
//                    echo "</pre>";
}
?>

<script type="text/javascript">
    function completeSurvey()
    {
       var surveyDiv = document.getElementById("headerSurvey");
       surveyDiv.style.display = 'none';
       var compDiv = document.getElementById("complete");
       compDiv.style.display = '';
       window.setTimeout(function(){window.location.href = "https://www.solaredge.com/service/support";	}, 2000);
    }
</script>

<style>
    
        body {
        width: 50%;
        margin: 0 auto;
        font-family: 'Lato', Lato, Arial, sans-serif;
        font-size: 15px;
        background: #f5f5f5;
    }
    hr{
        height: 3px;
        background-color: black;
    }
    h3{
        color: gray;
            font-size: 18px;
    }
    td { 
        padding: 10px;
    }
    .panel-heading{
            display: none !important;
    }
    .tab-pane{
            display: none !important;
    }
    .dcQuickEdit{
            display: none !important;
    }
    #EditView_tabs div{
            margin-bottom: 10px;
            padding: 0px;
    }
    input[type="radio"] {
      margin-right: 5px;
    }
    #EditView_tabs div label{
            margin-right: 15px;
    }
    #description{
            width: 70% !important;
    }
    div.clear{
        clear:both;
        height:0;
        visibility:hidden
    }
    .edit-view-row .label {
        display: inline;
        font-size: 16px;
        font-weight: 700;
        color: #534d64;
        padding: 0 0 0 8px;
        line-height: 48px;
        text-align: left;
        vertical-align: baseline;
        white-space: nowrap;
      
    }
    .label {
       font-size: 16px;
       color: #534d64;
    }
    
    input[type=radio]:checked {
    background-position: -18px 0;
    }
    input[type="radio"] {
        margin-right: 5px;
    }
    input[type="radio"] {
        background-position: 0 0;
    }
    input[type="radio"] {
        display: inline-block;
        background: url(radio.png) no-repeat;
        padding: 0;
        margin: 0;
        width: 18px;
        height: 18px;
        vertical-align: middle;
        visibility: visible;
    }
    input[type=submit] {
        font-size: 13px;
        font-weight: 500;
        background: #f08377;
        color: #f5f5f5;
        cursor: pointer;
        padding: 0 20px 0 20px;
        margin: 0 0 0 0;
        border: none;
        border-radius: 3px;
        letter-spacing: 1px;
        line-height: 40px;
        height: 40px;
        text-transform: uppercase;
    }
    .edit-view-field textarea {
        padding-top: 14px;
        line-height: 24px;
    }    
    textarea {
        background: #d8f5ee;
        padding: 5px;
        border: 1px solid #a5e8d6;
        border-radius: 4px;
    }
    textarea {
        font-family: 'Lato', Lato, Arial, sans-serif;
    }   
    .col-sm-12 .col-sm-8.edit-view-field{
        margin-left:0
    }
</style>
<div>
    <br/>
    <img src="logo.png" style="line-height:125px; color:#f5f5f5;top:10px;" >
    <br/>
</div>
<div id="headerSurvey">
    <hr/>
    <h3><?php if($lang == 'en'){ echo "Case Number:"; } else { echo "Befragung:"; } ?> <?php echo $caseNo ?></h3> 
    <h3><?php if($lang == 'en'){ echo "Subject:"; } else { echo "Thema:"; } ?> <?php echo $subject ?></h3>
    <hr/>
    <div class="clear"></div>
    <!-- saveSurvey.php -->
    <form action="#" method="POST" name="EditView" id="EditView" >
            <input type="hidden" name="caseid" value="<?php echo $caseId ?>" />
            <input type="hidden" name="subject" value="<?php echo $subject ?>" />
            <div id="EditView_tabs">
               <div class="clearfix"></div>
               <div class="tab-content" style="padding: 0; border: 0;">
               <div class="tab-pane panel-collapse">&nbsp;</div>
            </div>
               <div class="panel-content">
               <div>&nbsp;</div>
               <div class="panel panel-default">
                <div class="panel-heading ">
                <a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
                <div class="col-xs-10 col-sm-11 col-md-11">
                BASIC
                </div>
                </a>
                </div>
                <div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
                  <div class="tab-content">
                    <!-- tab_panel_content.tpl -->
                    <div class="row edit-view-row">
                    <div class="col-xs-12 col-sm-12 edit-view-row-item">
                    <div class="col-xs-12 col-sm-2 label" data-label="LBL_INITIAL_RESPONSE_TIME" dtbc-data="initial_response_time_c_label">
                    <?php if($lang == 'en')
                    {
                       echo "Initial response time and overall availability:";
                    }
                    else
                    {
                        echo "Reaktionszeit und generelle Verfügbarkeit:";
                    }
                    ?>
                     </div>
                    <div class="col-xs-12 col-sm-8 edit-view-field " type="radioenum" field="initial_response_time_c" colspan='3' >
                    <label><input type="radio" name="initial_response_time_c" value="" checked="checked" id="initial_response_time_c" title="" /></label>
                    <label><input type="radio" name="initial_response_time_c" value="1" id="initial_response_time_c" title="" /><?php if($lang == 'en'){ echo "Poor"; } else { echo "Sehr schlecht"; } ?></label>
                    <label><input type="radio" name="initial_response_time_c" value="2" id="initial_response_time_c" title="" /><?php if($lang == 'en'){ echo "Below average"; } else { echo "Schlech"; } ?></label>
                    <label><input type="radio" name="initial_response_time_c" value="3" id="initial_response_time_c" title="" /><?php if($lang == 'en'){ echo "Average"; } else { echo "Durchschnitt"; } ?></label>
                    <label><input type="radio" name="initial_response_time_c" value="4" id="initial_response_time_c" title="" /><?php if($lang == 'en'){ echo "Very good"; } else { echo "Gutt"; } ?></label>
                    <label><input type="radio" name="initial_response_time_c" value="5" id="initial_response_time_c" title="" /><?php if($lang == 'en'){ echo "Excellent"; } else { echo "Sehr gut"; } ?></label>
                    </div>
                    <!-- [/hide] -->
                    </div>
                    <div class="clear"></div>
                    <div class="col-xs-12 col-sm-12 edit-view-row-item">
                    <div class="col-xs-12 col-sm-2 label" data-label="LBL_PROFESSIONALISM_AND_SERVICE" dtbc-data="professionalism_and_service_c_label">
                    
                    <?php if($lang == 'en')
                    {
                       echo "Professionalism and service-oriented conduct of the staff:";
                    }
                    else
                    {
                        echo "Professionalität und service-orientiertes Verhalten der Mitarbeiter";
                    }
                    ?>
                    </div>
                    <div class="col-xs-12 col-sm-8 edit-view-field " type="radioenum" field="professionalism_and_service_c" colspan='3' >
                    <label><input type="radio" name="professionalism_and_service_c" value="" checked="checked" id="professionalism_and_service_c" title="" /></label>
                    <label><input type="radio" name="professionalism_and_service_c" value="1" id="professionalism_and_service_c" title="" /><?php if($lang == 'en'){ echo "Poor"; } else { echo "Sehr schlecht"; } ?></label>
                    <label><input type="radio" name="professionalism_and_service_c" value="2" id="professionalism_and_service_c" title="" /><?php if($lang == 'en'){ echo "Below average"; } else { echo "Schlech"; } ?></label>
                    <label><input type="radio" name="professionalism_and_service_c" value="3" id="professionalism_and_service_c" title="" /><?php if($lang == 'en'){ echo "Average"; } else { echo "Durchschnitt"; } ?></label>
                    <label><input type="radio" name="professionalism_and_service_c" value="4" id="professionalism_and_service_c" title="" /><?php if($lang == 'en'){ echo "Very good"; } else { echo "Gutt"; } ?></label>
                    <label><input type="radio" name="professionalism_and_service_c" value="5" id="professionalism_and_service_c" title="" /><?php if($lang == 'en'){ echo "Excellent"; } else { echo "Sehr gut"; } ?>
                    </div>
                    <!-- [/hide] -->
                    </div>
                    <div class="clear"></div>
                    <div class="col-xs-12 col-sm-12 edit-view-row-item">
                    <div class="col-xs-12 col-sm-2 label" data-label="LBL_TECHNICAL_KNOWLEDGE" dtbc-data="technical_knowledge_c_label">
                    
                    <?php if($lang == 'en')
                    {
                       echo "Technical knowledge of the staff:";
                    }
                    else
                    {
                        echo "Technisches Fachwissen der Mitarbeiter";
                    }
                    ?>
                    </div>
                    <div class="col-xs-12 col-sm-8 edit-view-field " type="radioenum" field="technical_knowledge_c" colspan='3' >
                    <label><input type="radio" name="technical_knowledge_c" value="" checked="checked" id="technical_knowledge_c" title="" /></label>
                    <label><input type="radio" name="technical_knowledge_c" value="1" id="technical_knowledge_c" title="" /><?php if($lang == 'en'){ echo "Poor"; } else { echo "Sehr schlecht"; } ?></label>
                    <label><input type="radio" name="technical_knowledge_c" value="2" id="technical_knowledge_c" title="" /><?php if($lang == 'en'){ echo "Below average"; } else { echo "Schlech"; } ?></label>
                    <label><input type="radio" name="technical_knowledge_c" value="3" id="technical_knowledge_c" title="" /><?php if($lang == 'en'){ echo "Average"; } else { echo "Durchschnitt"; } ?></label>
                    <label><input type="radio" name="technical_knowledge_c" value="4" id="technical_knowledge_c" title="" /><?php if($lang == 'en'){ echo "Very good"; } else { echo "Gutt"; } ?></label>
                    <label><input type="radio" name="technical_knowledge_c" value="5" id="technical_knowledge_c" title="" /><?php if($lang == 'en'){ echo "Excellent"; } else { echo "Sehr gut"; } ?>
                    </div>
                    <!-- [/hide] -->
                    </div>
                    <div class="clear"></div>
                    <div class="col-xs-12 col-sm-12 edit-view-row-item">
                    <div class="col-xs-12 col-sm-2 label" data-label="LBL_TIME_UNTIL_ISSUE_RESOLVED" dtbc-data="time_until_issue_resolved_c_label">
                    
                    <?php if($lang == 'en')
                    {
                       echo "Time until issue was resolved:";
                    }
                    else
                    {
                        echo "Dauer bis zur Lösung des Problems";
                    }
                    ?>
                    </div>
                    <div class="col-xs-12 col-sm-8 edit-view-field " type="radioenum" field="time_until_issue_resolved_c" colspan='3' >
                    <label><input type="radio" name="time_until_issue_resolved_c" value="" checked="checked" id="time_until_issue_resolved_c" title="" /></label>
                    <label><input type="radio" name="time_until_issue_resolved_c" value="1" id="time_until_issue_resolved_c" title="" /><?php if($lang == 'en'){ echo "Poor"; } else { echo "Sehr schlecht"; } ?></label>
                    <label><input type="radio" name="time_until_issue_resolved_c" value="2" id="time_until_issue_resolved_c" title="" /><?php if($lang == 'en'){ echo "Below average"; } else { echo "Schlech"; } ?></label>
                    <label><input type="radio" name="time_until_issue_resolved_c" value="3" id="time_until_issue_resolved_c" title="" /><?php if($lang == 'en'){ echo "Average"; } else { echo "Durchschnitt"; } ?></label>
                    <label><input type="radio" name="time_until_issue_resolved_c" value="4" id="time_until_issue_resolved_c" title="" /><?php if($lang == 'en'){ echo "Very good"; } else { echo "Gutt"; } ?></label>
                    <label><input type="radio" name="time_until_issue_resolved_c" value="5" id="time_until_issue_resolved_c" title="" /><?php if($lang == 'en'){ echo "Excellent"; } else { echo "Sehr gut"; } ?>
                    </div>
                    <!-- [/hide] -->
                    </div>
                    <div class="clear"></div>
                    <div class="col-xs-12 col-sm-12 edit-view-row-item">
                    <div class="col-xs-12 col-sm-2 label" data-label="LBL_DESCRIPTION" dtbc-data="description_label">
                    
                    <?php if($lang == 'en')
                    {
                       echo "<b>Additional comments</b>:";
                    }
                    else
                    {
                        echo "<b>Weitere Kommentare</b>:";
                    }
                    ?>
                    </div>
                    <div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="description" colspan='3' >
                      <textarea  id='description' name='description' rows="6" cols="80" title='' tabindex="" ></textarea>
                    </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
               <input type='submit' name="saveSurvey" value='Save' id='saveButton' onclick="completeSurvey();" />
            </div>
            </div>
    </form>
</div>
<div id="complete" style='text-align: center;display: none'>
    <h1>Thank you for your response</h1>
</div>    