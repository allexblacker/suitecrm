
<?php

require_once 'countryList.php';
require_once 'getListKey.php';

// specify the REST web service to interact with
//$url = 'http://172.20.101.229/service/v4_1/rest.php';
$url = 'http://credge/service/v4_1/rest.php';

$status = false;
$username = "MonitoringAPI";
$password = "P@ssw0rd";

//function to make cURL request
function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	//print_r($http_status);
	//print_r($result);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);

//echo "<pre>";
//print_r($login_result);
//echo "</pre>";

//get session id
$session_id = $login_result->id;

//echo $session_id;

		// URL Parameters
if (isset($_GET['Mode'])) {
    $apiMode = $_GET['Mode'];
}
if($apiMode == 'Site')
{
    $CRMSiteId = '';
    $SiteId = '';
    $fName = '';
    $fStatus = '';
    $fPeakPower = '';
    $fCurrency = '';
    $fInstallationDate = '';
    $fType = '';
    $fAddress1 = '';
    $fNotes = '';
    $fPublicAccess = '';
    $fLastReportingTime = '';
    $fCity = '';
    $fZipCode = '';
    $fMagnitude = '';
    $fDefaultPanelModel = '';
    $fMonitoringCreationTime = '';
    $fGUID = '';
    $fLongitude = '';
    $fLatitude = '';
    $fAccountGUID = '';
    $fCountry ='';
    $fState = '';
    $CRMAccountId = '';
    // URL Parameters
    if (isset($_GET['Field_ID'])) {
        $SiteId = $_GET['Field_ID'];
    }

    if (isset($_GET['Field_Name'])) {
        $fName = $_GET['Field_Name'];
    }

    if (isset($_GET['Field_Status'])) {
        $fStatus = $_GET['Field_Status'];
    }

    if (isset($_GET['Field_Peak_Power'])) {
        $fPeakPower = $_GET['Field_Peak_Power'];
    }

    if (isset($_GET['Field_Currency'])) {
        $fCurrency = $_GET['Field_Currency'];
    }

    if (isset($_GET['Field_Installation_Date'])) {
        $fInstallationDate = $_GET['Field_Installation_Date'];
        $fInstallationDate = date('Y-m-d', strtotime($fInstallationDate));
    }

    if (isset($_GET['Field_Type'])) {
        $fType = $_GET['Field_Type'];
    }

    if (isset($_GET['Field_Address_1'])) {
        $fAddress = $_GET['Field_Address_1'];
    }

    if (isset($_GET['Field_Country'])) {
        $fCountry = $_GET['Field_Country'];
        if(strtolower($fCountry) == "united states") { 
           $fCountry = "Unitedstates";
        }
        else {
            $fCountry = str_replace(' ','',$fCountry);
            $fCountry = str_replace('.','',$fCountry);
            $fCountry = str_replace('/','',$fCountry);
            $fCountry = getCountryId($fCountry);
        }
    }

    if (isset($_GET['Field_State'])) {
        $fState = $_GET['Field_State'];
        $fState = str_replace(' ','',$fState);
        //echo 'State=' . $fState;
        $fState = getListKey('state_0', $fState, $session_id, $url);
        //echo 'State=' . $fState;
    }

    if (isset($_GET['Field_City'])) {
        $fCity = $_GET['Field_City'];
    }

    if (isset($_GET['Field_Zip_Code'])) {
        $fZipCode = $_GET['Field_Zip_Code'];
    }

    if (isset($_GET['Field_GUID'])) {
        $fGUID = $_GET['Field_GUID'];
    }

    if (isset($_GET['Account_GUID'])) {
        $fAccountGUID = $_GET['Account_GUID'];
    }

    if (isset($_GET['Field_Notes'])) {
        $fNotes = $_GET['Field_Notes'];
    }

    if (isset($_GET['Field_Public_Access'])) {
        $fPublicAccess = $_GET['Field_Public_Access'];
        if( strtolower($fPublicAccess) == 'false'){
            $fPublicAccess = '0';
        }
        if(strtolower($fPublicAccess) == 'true'){
            $fPublicAccess = '1';
        }
    }

    if (isset($_GET['Field_Magnitude'])) {
        $fMagnitude = $_GET['Field_Magnitude'];
    }

    if (isset($_GET['Field_Longitude'])) {
        $fLongitude = $_GET['Field_Longitude'];
    }

    if (isset($_GET['Field_Latitude'])) {
        $fLatitude = $_GET['Field_Latitude'];
    }

    if (isset($_GET['Field_Last_Reporting_Time'])) {
        $fLastReportingTime = $_GET['Field_Last_Reporting_Time'];
        $fLastReportingTime = date('Y-m-d', strtotime($fLastReportingTime));
    }

    if (isset($_GET['Field_Default_Panel_Model'])) {
        $fDefaultPanelModel = $_GET['Field_Default_Panel_Model'];
    }
    if (isset($_GET['Field_Creation_Date'])) {
        $fMonitoringCreationTime = $_GET['Field_Creation_Date'];
        $fMonitoringCreationTime = date('Y-m-d', strtotime($fMonitoringCreationTime));
    }
    if($SiteId != '') {
        //retrieve Contact Id -------------------------------------
        $get_entry_list_parameters = array(

            //session id
            'session' => $session_id,

            //The name of the module from which to retrieve records
            'module_name' => "S1_Site",
            'query' => "monitoring_site_id = '" . $SiteId . "'",
            'order_by' => "",

            //The record offset from which to start.
            'offset' => "0",

            //Optional. The list of fields to be returned in the results
            'select_fields' => array('id', 'name','monitoring_site_id'),

            //Show 10 max results
            'max_results' => 10,
            //Do not show deleted
            'deleted' => 0,

        );

        $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
        $CRMSiteId = $get_entry_list_result->entry_list[0]->name_value_list->id->value;
        //echo $CRMSiteId . "<br/>";

        if($fAccountGUID != ''){
            $get_entry_list_parameters = array(

                //session id
                'session' => $session_id,

                //The name of the module from which to retrieve records
                'module_name' => "Accounts",
                'query' => "account_monitoring_id_c = '" . $fAccountGUID . "'",
                'order_by' => "",

                //The record offset from which to start.
                'offset' => "0",

                //Optional. The list of fields to be returned in the results
                'select_fields' => array('id', 'name'),

                //Show 10 max results
                'max_results' => 10,
                //Do not show deleted
                'deleted' => 0,

            );

            $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
            $CRMAccountId = $get_entry_list_result->entry_list[0]->name_value_list->id->value;
            //echo $CRMAccountId . "<br/>";

        }
        //echo $CRMAccountId;
        //echo "<pre>";
        //print_r($get_entry_list_result);
        //echo 'phone = ' . $phone . " id" . $CRMSiteId;
        //echo "</pre>";
        if ($CRMSiteId != '')  // Existing Customer
        {
            //Update Site
            $set_entry_parameters2 = array(
                "session" => $session_id,
                "module_name" => "S1_Site",
                //Record attributes
                "name_value_list" => array(
                    array("name" => "id", "value" => $CRMSiteId),
                    array("name" => "monitoring_site_id_c", "value" => $SiteId),
                    array("name" => "status", "value" => $fStatus),
                    array("name" => "name", "value" => $fName),
                    array("name" => "peak_power_kw_h_peak", "value" => $fPeakPower),
                    array("name" => "currency", "value" => $fCurrency),
                    array("name" => "installation_date", "value" => $fInstallationDate),
                    array("name" => "field_type", "value" => $fType),
                    array("name" => "site_address", "value" => $fAddress),
                    array("name" => "notes", "value" => $fNotes),
                    array("name" => "is_public", "value" => $fPublicAccess ),
                    array("name" => "last_reporting_time", "value" => $fLastReportingTime),
                    array("name" => "site_city", "value" => $fCity),
                    array("name" => "site_zip", "value" => $fZipCode),
                    array("name" => "field_magnitude", "value" => $fMagnitude),
                    array("name" => "default_panel_model", "value" => $fDefaultPanelModel),
                    array("name" => "monitoring_creation_time", "value" => $fMonitoringCreationTime),
                    array("name" => "guid", "value" => $fGUID),
                    array("name" => "longitude", "value" => $fLongitude),
                    array("name" => "latitude", "value" => $fLatitude),
                    array("name" => "monitoring_account_id", "value" => $fAccountGUID),
                    array("name" => "site_country", "value" => $fCountry),
                    array("name" => "site_state", "value" => $fState),
                    array("name" => "account_id_c", "value" => $CRMAccountId),
                ),
            );
            if($fStatus == '') { unset ($set_entry_parameters2['name_value_list'][2]); }
            if($fName == '') { unset ($set_entry_parameters2['name_value_list'][3]); }
            if($fPeakPower == '') { unset ($set_entry_parameters2['name_value_list'][4]); }
            if($fCurrency == '') { unset ($set_entry_parameters2['name_value_list'][5]); }
            if($fInstallationDate == '') { unset ($set_entry_parameters2['name_value_list'][6]); }
            if($fType == '') { unset ($set_entry_parameters2['name_value_list'][7]); }
            if($fAddress == '') { unset ($set_entry_parameters2['name_value_list'][8]); }
            if($fNotes == '') { unset ($set_entry_parameters2['name_value_list'][9]); }
            if($fPublicAccess == '') { unset ($set_entry_parameters2['name_value_list'][10]); }
            if($fLastReportingTime == '') { unset ($set_entry_parameters2['name_value_list'][11]); }
            if($fCity == '') { unset ($set_entry_parameters2['name_value_list'][12]); }
            if($fZipCode == '') { unset ($set_entry_parameters2['name_value_list'][13]); }
            if($fMagnitude == '') { unset ($set_entry_parameters2['name_value_list'][14]); }
            if($fDefaultPanelModel == '') { unset ($set_entry_parameters2['name_value_list'][15]); }
            if($fMonitoringCreationTime == '') { unset ($set_entry_parameters2['name_value_list'][16]); }
            if($fGUID == '') { unset ($set_entry_parameters2['name_value_list'][17]); }
            if($fLongitude == '') { unset ($set_entry_parameters2['name_value_list'][18]); }
            if($fLatitude == '') { unset ($set_entry_parameters2['name_value_list'][19]); }
            if($fAccountGUID == '') { unset ($set_entry_parameters2['name_value_list'][20]); }
            if($fCountry == '') { unset ($set_entry_parameters2['name_value_list'][21]); }
            if($fState == '') { unset ($set_entry_parameters2['name_value_list'][22]); }
            if($CRMAccountId == '') { unset ($set_entry_parameters2['name_value_list'][23]); }
            //echo "<pre>";
            //print_r($set_entry_parameters2);
            //echo "</pre>";

            $get_entry_list_result2 = call("set_entry", $set_entry_parameters2, $url);

            //echo "<pre>";
            //print_r($get_entry_list_result2);
            //echo "</pre>";
        }
        else{
            //Create Site
            $set_entry_parameters_cr = array(
                "session" => $session_id,
                "module_name" => "S1_Site",
                //Record attributes
                "name_value_list" => array(
                    array("name" => "monitoring_site_id", "value" => $SiteId),
                    array("name" => "status", "value" =>$fStatus ),
                    array("name" => "name", "value" =>$fName ),
                    array("name" => "peak_power_kw_h_peak", "value" => $fPeakPower),
                    array("name" => "currency", "value" => $fCurrency),
                    array("name" => "installation_date", "value" => $fInstallationDate),
                    array("name" => "field_type", "value" => $fType),
                    array("name" => "site_address", "value" => $fAddress),
                    array("name" => "notes", "value" => $fNotes),
                    array("name" => "is_public", "value" => $fPublicAccess ),
                    array("name" => "last_reporting_time", "value" => $fLastReportingTime),
                    array("name" => "site_city", "value" => $fCity),
                    array("name" => "site_zip", "value" => $fZipCode),
                    array("name" => "field_magnitude", "value" => $fMagnitude),
                    array("name" => "default_panel_model", "value" => $fDefaultPanelModel),
                    array("name" => "monitoring_creation_time", "value" => $fMonitoringCreationTime),
                    array("name" => "guid", "value" => $fGUID),
                    array("name" => "longitude", "value" => $fLongitude),
                    array("name" => "latitude", "value" => $fLatitude),
                    array("name" => "monitoring_account_id", "value" => $fAccountGUID),
                    array("name" => "site_country", "value" => $fCountry),
                    array("name" => "site_state", "value" => $fState),
                    array("name" => "account_id_c", "value" => $CRMAccountId),
                ),
            );
            if($fStatus == '') { unset ($set_entry_parameters_cr['name_value_list'][1]); }
            if($fName == '') { unset ($set_entry_parameters_cr['name_value_list'][2]); }
            if($fPeakPower == '') { unset ($set_entry_parameters_cr['name_value_list'][3]); }
            if($fCurrency == '') { unset ($set_entry_parameters_cr['name_value_list'][4]); }
            if($fInstallationDate == '') { unset ($set_entry_parameters_cr['name_value_list'][5]); }
            if($fType == '') { unset ($set_entry_parameters_cr['name_value_list'][6]); }
            if($fAddress == '') { unset ($set_entry_parameters_cr['name_value_list'][7]); }
            if($fNotes == '') { unset ($set_entry_parameters_cr['name_value_list'][8]); }
            if($fPublicAccess == '') { unset ($set_entry_parameters_cr['name_value_list'][9]); }
            if($fLastReportingTime == '') { unset ($set_entry_parameters_cr['name_value_list'][10]); }
            if($fCity == '') { unset ($set_entry_parameters_cr['name_value_list'][11]); }
            if($fZipCode == '') { unset ($set_entry_parameters_cr['name_value_list'][12]); }
            if($fMagnitude == '') { unset ($set_entry_parameters_cr['name_value_list'][13]); }
            if($fDefaultPanelModel == '') { unset ($set_entry_parameters_cr['name_value_list'][14]); }
            if($fMonitoringCreationTime == '') { unset ($set_entry_parameters_cr['name_value_list'][15]); }
            if($fGUID == '') { unset ($set_entry_parameters_cr['name_value_list'][16]); }
            if($fLongitude == '') { unset ($set_entry_parameters_cr['name_value_list'][17]); }
            if($fLatitude == '') { unset ($set_entry_parameters_cr['name_value_list'][18]); }
            if($fAccountGUID == '') { unset ($set_entry_parameters_cr['name_value_list'][19]); }
            if($fCountry == '') { unset ($set_entry_parameters_cr['name_value_list'][20]); }
            if($fState == '') { unset ($set_entry_parameters_cr['name_value_list'][21]); }
            if($CRMAccountId == '') { unset ($set_entry_parameters_cr['name_value_list'][22]); }
            $get_entry_list_result_cr = call("set_entry", $set_entry_parameters_cr, $url);

            //echo "<pre>";
            //print_r($get_entry_list_result2);
            //print_r($get_entry_list_result_cr);
            //echo "</pre>";
        }
    }
    $status = true;
}
 else {
    if (isset($_GET['First_name'])) {
        $firstName = $_GET['First_name'];
    }

    if (isset($_GET['Last_name'])) {
        $lastName = $_GET['Last_name'];
    }
    if (isset($_GET['Email'])) {
        $email = strtolower($_GET['Email']);
    }
    if (isset($_GET['Phone'])) {
        $phone = $_GET['Phone'];
    }
    if (isset($_GET['Receives_Newsletter'])) {
        $newsletter = $_GET['Receives_Newsletter'];
    }
    if (isset($_GET['Receives_Product_Definitions'])) {
        $product_def = $_GET['Receives_Product_Definitions'];
    }
    if (isset($_GET['Receives_Blog_Updates'])) {
        $blog_upd = $_GET['Receives_Blog_Updates'];
    }
    if (isset($_GET['GUID'])) {
        $guid = $_GET['GUID'];
    }
    if (isset($_GET['Account_Name'])) {
        $accName = $_GET['Account_Name'];
    }
    if (isset($_GET['Account_Address_1'])) {
        $street = $_GET['Account_Address_1'];
    }
    if (isset($_GET['Account_Address_2'])) {
        $acct_addr = $_GET['Account_Address_2'];
    }
    if (isset($_GET['Account_Country'])) {
        $country = $_GET['Account_Country'];
    }
    if (isset($_GET['Account_City'])) {
        $city = $_GET['Account_City'];
    }
    if (isset($_GET['Account_State'])) {
        $stat = $_GET['Account_State'];
    }
    if (isset($_GET['Account_Zip_Code'])) {
        $zip = $_GET['Account_Zip_Code'];
    }
    if (isset($_GET['Account_Phone'])) {
        $custPhone = $_GET['Account_Phone'];
    }
    if (isset($_GET['Account_GUID'])) {
        $custid = $_GET['Account_GUID'];
    }
    if (isset($_GET['Locale'])) {
        $local = $_GET['Locale'];
    }

        if(strtolower($country) == "united states") { 
           $country = "Unitedstates";
        }
        else {
            $country = str_replace(' ','',$country);
            $country = str_replace('.','',$country);
            $country = str_replace('/','',$country);
            $country = getCountryId($country);
        }

       $stat = str_replace(' ','',$stat);
       $contact_id = '';
        if($email != '')
       {
            //retrieve Contact Id ------------------------------------- 
            $get_entry_list_parameters = array(

                 //session id
                 'session' => $session_id,

                 //The name of the module from which to retrieve records
                 'module_name' => "Contacts",
                         //"contacts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Contacts' AND ea.email_address_caps LIKE '%" . $email1 . "' AND eabr.deleted=0)",
                         "contacts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Contacts' AND ea.email_address_caps = '" . $email . "' AND eabr.deleted=0)",
                 //'query' => "",
                         'order_by' => "",

                 //The record offset from which to start.
                 'offset' => "0",

                 //Optional. The list of fields to be returned in the results
                 'select_fields' => array('id','name', 'email1', 'email_c'),

                        //Show 10 max results
                        'max_results' => 10,
                        //Do not show deleted
                        'deleted' => 0,

            ); 

            $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
            $contact_id = $get_entry_list_result->entry_list[0]->name_value_list->id->value; 

            //echo "<pre>";
            //echo 'contact_id = ' . $contact_id;
        //print_r($get_entry_list_result);
            //echo "</pre>";
            $account_id = '';
            if($custid != '')
            {
                    //retrieve Account Id ------------------------------------- 
                    $get_entry_list_parameters = array(

                             //session id
                             'session' => $session_id,

                             //The name of the module from which to retrieve records
                             'module_name' => "Accounts",
                             //'query' => "account_monitoring_id_c = '1ad98ae2-a145-4666-ac83-608ff30ec500'",
                             'query' => "account_monitoring_id_c = '" . $custid . "'",
                             //'query' => "",
                             'order_by' => "",

                             //The record offset from which to start.
                             'offset' => "0",

                             //Optional. The list of fields to be returned in the results
                             'select_fields' => array('id','name', 'account_monitoring_id_c'),

                            //Show 10 max results
                            'max_results' => 10,
                            //Do not show deleted
                            'deleted' => 0,

                    ); 

                    $get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
                    $account_id = $get_entry_list_result->entry_list[0]->name_value_list->id->value; 
            }
            //echo "<pre>";
            //echo 'account_id = ' . $account_id;
            //echo "</pre>";
            if($account_id != '')  // Existing Customer
            {
                //Update Account
                $set_entry_parameters = array(
                         "session" => $session_id,
                         "module_name" => "Accounts",
                            //Record attributes
                             "name_value_list" => array(
                                array("name" => "id", "value" => $account_id),
                                array("name" => "name", "value" => $accName),
                                array("name" => "jjwg_maps_address_c", "value" => $acct_addr),
                                array("name" => "street_c", "value" => $street),
                                array("name" => "country_c", "value" => $country),
                                array("name" => "state_c", "value" => "Unitedstates_" . $stat),
                                array("name" => "city_c", "value" => $city),
                                array("name" => "zip_postal_code_c", "value" => $zip),
                                array("name" => "phone_c", "value" => $custPhone),
                                array("name" => "account_monitoring_id_c", "value" => $custid),
                             ),
                      );
                if($accName == '') { unset ($set_entry_parameters['name_value_list'][1]); }
                if($acct_addr == '') { unset ($set_entry_parameters['name_value_list'][2]); }
                if($street == '') { unset ($set_entry_parameters['name_value_list'][3]); }
                if($country == '') { unset ($set_entry_parameters['name_value_list'][4]); }
                if($stat == '') { unset ($set_entry_parameters['name_value_list'][5]); }
                if($city == '') { unset ($set_entry_parameters['name_value_list'][6]); }
                if($zip == '') { unset ($set_entry_parameters['name_value_list'][7]); }
                if($custPhone == '') { unset ($set_entry_parameters['name_value_list'][8]); }
                if($custid == '') { unset ($set_entry_parameters['name_value_list'][9]); }
                $set_entry_result = call("set_entry", $set_entry_parameters, $url); 
                      //echo "<pre>";
                      //print_r($set_entry_result);
                      //echo "</pre>";

                if($contact_id == '')
                {
                         //Create Contact
                        $set_entry_parameters = array(
                                "session" => $session_id,
                                "module_name" => "Contacts",

                                //Record attributes
                                "name_value_list" => array(
                                            array("name" => "first_name", "value" => $firstName),
                                            array("name" => "last_name", "value" => $lastName),
                                            array("name" => "email1", "value" => $email),
                                            array("name" => "phone_c", "value" => $phone),
                                            array("name" => "newsletter_c", "value" => $newsletter),
                                            array("name" => "product_definition_c", "value" => $product_def),
                                            array("name" => "receive_blog_update_c", "value" => $blog_upd),
                                            array("name" => "guid_c", "value" => $guid),
                                            array("name" => "locale_c", "value" => $local),
                                            array("name" => "account_id", "value" => $account_id ),
                               ),
                           );
                        $set_entry_result = call("set_entry", $set_entry_parameters, $url);
                                    //echo "<pre>";
                                    //print_r($set_entry_result);
                                    //echo "</pre>";
                    }
                    else
                    { 
                         //Update Contact
                        $set_entry_parameters = array(
                                    "session" => $session_id,
                                    "module_name" => "Contacts",

                                    //Record attributes
                                    "name_value_list" => array(
                                                    array("name" => "id", "value" => $contact_id),
                                                    array("name" => "first_name", "value" => $firstName),
                                                    array("name" => "last_name", "value" => $lastName),
                                                    array("name" => "email1", "value" => $email),
                                                    array("name" => "phone_c", "value" => $phone),
                                                    array("name" => "newsletter_c", "value" => $newsletter),
                                                    array("name" => "product_definition_c", "value" => $product_def),
                                                    array("name" => "receive_blog_update_c", "value" => $blog_upd),
                                                    array("name" => "guid_c", "value" => $guid),
                                                    array("name" => "account_id", "value" => $account_id ),
                                                    array("name" => "local_c", "value" => $local),
                               ),
                           );
                        if($firstName == '') { unset ($set_entry_parameters['name_value_list'][1]); }
                        if($lastName == '') { unset ($set_entry_parameters['name_value_list'][2]); }
                        if($email == '') { unset ($set_entry_parameters['name_value_list'][3]); }
                        if($phone == '') { unset ($set_entry_parameters['name_value_list'][4]); }
                        if($newsletter == '') { unset ($set_entry_parameters['name_value_list'][5]); }
                        if($product_def == '') { unset ($set_entry_parameters['name_value_list'][6]); }
                        if($blog_upd == '') { unset ($set_entry_parameters['name_value_list'][7]); }
                        if($guid == '') { unset ($set_entry_parameters['name_value_list'][8]); }
                        if($account_id == '') { unset ($set_entry_parameters['name_value_list'][9]); }

                        $set_entry_result = call("set_entry", $set_entry_parameters, $url);
                                    //echo "<pre>";
                                    //print_r($set_entry_result);
                                    //echo "</pre>";
                    }
                    $status = true;
                }
                else
                {    
                    if($custid != '')
                    {
                         //Create Account
                        $set_entry_parameters = array(
                                "session" => $session_id,
                                "module_name" => "Accounts",
                                    //Record attributes
                                     "name_value_list" => array(
                                        array("name" => "name", "value" => $accName),
                                        array("name" => "jjwg_maps_address_c", "value" => $acct_addr),
                                        array("name" => "street_c", "value" => $street),
                                        array("name" => "country_c", "value" => $country),
                                        array("name" => "state_c", "value" => "Unitedstates_" . $stat),
                                        array("name" => "city_c", "value" => $city),
                                        array("name" => "zip_postal_code_c", "value" => $zip),
                                        array("name" => "phone_c", "value" => $custPhone),
                                        array("name" => "account_monitoring_id_c", "value" => $custid),
                                        array("name" => "account_type", "value" => "Installer"),
                                        //array("name" => "account_owner_c", "value" => "569a46e5-bc87-8471-5220-59d0e11beb66" /*$username*/ ),
                                        //array("name" => "assign_user_id", "value" => "569a46e5-bc87-8471-5220-59d0e11beb66" /*$username*/ ),
                                     ),
                              );
                        $set_entry_result = call("set_entry", $set_entry_parameters, $url);
                        $account_id = $set_entry_result->id; 
                          //echo "<pre>";
                          //print_r($set_entry_result);
                          //echo "</pre>";
                          //echo "new account id = " . $account_id;
                    }
                    if($contact_id == '')
                    {
                        if($account_id != '')
                        {
                                //Create Contact
                           $set_entry_parameters = array(
                                    "session" => $session_id,
                                    "module_name" => "Contacts",

                                    //Record attributes
                                    "name_value_list" => array(
                                                    array("name" => "first_name", "value" => $firstName),
                                                    array("name" => "last_name", "value" => $lastName),
                                                    array("name" => "email1", "value" => $email),
                                                    array("name" => "phone_c", "value" => $phone),
                                                    array("name" => "newsletter_c", "value" => $newsletter),
                                                    array("name" => "product_definition_c", "value" => $product_def),
                                                    array("name" => "receive_blog_update_c", "value" => $blog_upd),
                                                    array("name" => "guid_c", "value" => $guid),
                                                    array("name" => "account_id", "value" => $account_id ),
                                                    array("name" => "locale_c", "value" => $local),
                               ),
                           );
                           $set_entry_result = call("set_entry", $set_entry_parameters, $url);
                                           //echo "<pre>";
                                           //print_r($set_entry_result);
                                           //echo "</pre>";
                        }
                    }
                    else
                    {
                             //Update Contact
                        $set_entry_parameters = array(
                                 "session" => $session_id,
                                 "module_name" => "Contacts",

                                 //Record attributes
                                 "name_value_list" => array(
                                                 array("name" => "id", "value" => $contact_id),
                                                 array("name" => "first_name", "value" => $firstName),
                                                 array("name" => "last_name", "value" => $lastName),
                                                 array("name" => "email1", "value" => $email),
                                                 array("name" => "phone_c", "value" => $phone),
                                                 array("name" => "newsletter_c", "value" => $newsletter),
                                                 array("name" => "product_definition_c", "value" => $product_def),
                                                 array("name" => "receive_blog_update_c", "value" => $blog_upd),
                                                 array("name" => "guid_c", "value" => $guid),
                                                 array("name" => "account_id", "value" => $account_id ),
                                                 array("name" => "locale_c", "value" => $local),
                                ),
                            );
                        if($firstName == '') { unset ($set_entry_parameters['name_value_list'][1]); }
                        if($lastName == '') { unset ($set_entry_parameters['name_value_list'][2]); }
                        if($email == '') { unset ($set_entry_parameters['name_value_list'][3]); }
                        if($phone == '') { unset ($set_entry_parameters['name_value_list'][4]); }
                        if($newsletter == '') { unset ($set_entry_parameters['name_value_list'][5]); }
                        if($product_def == '') { unset ($set_entry_parameters['name_value_list'][6]); }
                        if($blog_upd == '') { unset ($set_entry_parameters['name_value_list'][7]); }
                        if($guid == '') { unset ($set_entry_parameters['name_value_list'][8]); }
                        if($account_id == '') { unset ($set_entry_parameters['name_value_list'][9]); }

                        $set_entry_result = call("set_entry", $set_entry_parameters, $url);
                                        //echo "<pre>";
                                        //print_r($set_entry_result);
                                        //echo "</pre>";
                    }
                    $status = true;
                }
       }    
}

   if (!isset($resultObj)) 
   { $resultObj = new stdClass(); }

    header('Content-Type: application/json');
    if($status == true)
    {    
        header('Status: 200',true,200);
       //http_response_code(200);
        $resultObj->status = "200";
        $resultObj->status_message = "Success";
        $resultObj = json_encode($resultObj);
        echo $resultObj;
    }
    else
    {
        //header(':', true, 400);
       header('400 Bad Request',true,400);
       //http_response_code(400);
       $resultObj->status = "400";
       $resultObj->status_message = "Failed";
       $resultObj = json_encode($resultObj);
       echo $resultObj;
    }