<?php
		header("Content-type: text/xml; charset=utf-8");

		// specify the REST web service to interact with
//$url = 'http://172.20.101.229/service/v4_1/rest.php';
$url = 'http://credge/service/v4_1/rest.php';

$username = "CiscoAPI";
$password = "P@ssw0rd";

//function to make cURL request
function call($method, $parameters, $url)
{
	ob_start();
	$curl_request = curl_init();

	curl_setopt($curl_request, CURLOPT_URL, $url);
	curl_setopt($curl_request, CURLOPT_POST, 1);
	curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($curl_request, CURLOPT_HEADER, 1);
	curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	$jsonEncodedData = json_encode($parameters);

	$post = array(
	"method" => $method,
	"input_type" => "JSON",
	"response_type" => "JSON",
	"rest_data" => $jsonEncodedData
	);

	curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	$result = curl_exec($curl_request);
	$http_status = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
	curl_close($curl_request);

	$result = explode("\r\n\r\n", $result, 2);
	$response = json_decode($result[1]);
	ob_end_flush();

	return $response;
}

//login 
$login_parameters = array(
"user_auth" => array(
"user_name" => $username,
"password" => md5($password),
"version" => "4"
),
"application_name" => "RestTest",
"name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);

//get session id
$session_id = $login_result->id;

		$contact_id = "";
		$contact_type = "";
		$service_lvl = "";

		// URL Parameters
		if (isset($_GET['action'])) {
             $action = $_GET['action'];
        }
		if($action = 'getContacts')
		{
			if (isset($_GET['callerID'])) {
				 $phone = $_GET['callerID'];
			}
			if($phone != "")
			{
			     $phone = substr($phone,-9) ;
				//retrieve records ------------------------------------- 
					$get_entry_list_parameters = array(

					 //session id
					 'session' => $session_id,

					 //The name of the module from which to retrieve records
					 'module_name' => "Contacts",

					 //The SQL WHERE clause without the word "where".
					 //'query' => "phone = 'SJ0616-07F147A70-7D'",
					'query' => " phone_c LIKE '%" . $phone . "' || home_phone_c LIKE '%" . $phone . "' || mobile_c LIKE '%" . $phone . "' || contacts.phone_work LIKE '%" . $phone . "' || contacts.phone_home LIKE '%" . $phone . "' || contacts.phone_mobile LIKE '%" . $phone . "' || contacts.phone_other LIKE '%" . $phone . "'",
					//'query' => "contacts.phone_work = '" . $phone . "' || contacts.phone_home = '" . $phone . "' || contacts.phone_mobile = '" . $phone . "' || contacts.phone_other '" . $phone . "'",
					//'query' => "",
					 'order_by' => "",

					 //The record offset from which to start.
					 'offset' => "0",

					 //Optional. The list of fields to be returned in the results
					 'select_fields' => array('id','name', 'contact_type_c', 'phone_home', 'phone_work', 'phone_mobile', 'phone_other'),

				 //Link to the "contacts" relationship and retrieve the
				 //First and last names.
				 'link_name_to_fields_array' => array(
					 array(
						 'name' => 'accounts',
						 'value' => array('id', 'name', 'service_level_c'),
						 ),
					 ),

					//Show 10 max results
					'max_results' => 10,
					//Do not show deleted
					'deleted' => 0,

				); 

				$get_entry_list_result = call("get_entry_list", $get_entry_list_parameters, $url);
				$contact_id = $get_entry_list_result->entry_list[0]->name_value_list->id->value;
				$contact_type = $get_entry_list_result->entry_list[0]->name_value_list->contact_type_c->value;
				$service_lvl = $get_entry_list_result->relationship_list[0]->link_list[0]->records[0]->link_value->service_level_c->value; 

			}

		}

$output = "<response><success>true</success><message>null</message><RecordID>" . $contact_id . "</RecordID><contactType>" . $contact_type . "</contactType><accountServiceLevel>" . $service_lvl . "</accountServiceLevel></response>";
echo $output;


