<?php
// created: 2018-03-11 22:19:15
$dashletsFiles = array (
  'MyAccountsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.php',
    'class' => 'MyAccountsDashlet',
    'meta' => 'modules/var/www/html/modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.meta.php',
    'module' => 'Accounts',
  ),
  'ActOn_HotProspectsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/ActOn_HotProspects/Dashlets/ActOn_HotProspectsDashlet/ActOn_HotProspectsDashlet.php',
    'class' => 'ActOn_HotProspectsDashlet',
    'meta' => 'modules/var/www/html/modules/ActOn_HotProspects/Dashlets/ActOn_HotProspectsDashlet/ActOn_HotProspectsDashlet.meta.php',
  ),
  'AL1_Alliance_TransactionDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AL1_Alliance_Transaction/Dashlets/AL1_Alliance_TransactionDashlet/AL1_Alliance_TransactionDashlet.php',
    'class' => 'AL1_Alliance_TransactionDashlet',
    'meta' => 'modules/var/www/html/modules/AL1_Alliance_Transaction/Dashlets/AL1_Alliance_TransactionDashlet/AL1_Alliance_TransactionDashlet.meta.php',
    'module' => 'AL1_Alliance_Transaction',
  ),
  'AM_ProjectTemplatesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AM_ProjectTemplates/Dashlets/AM_ProjectTemplatesDashlet/AM_ProjectTemplatesDashlet.php',
    'class' => 'AM_ProjectTemplatesDashlet',
    'meta' => 'modules/var/www/html/modules/AM_ProjectTemplates/Dashlets/AM_ProjectTemplatesDashlet/AM_ProjectTemplatesDashlet.meta.php',
    'module' => 'AM_ProjectTemplates',
  ),
  'AM_TaskTemplatesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AM_TaskTemplates/Dashlets/AM_TaskTemplatesDashlet/AM_TaskTemplatesDashlet.php',
    'class' => 'AM_TaskTemplatesDashlet',
    'meta' => 'modules/var/www/html/modules/AM_TaskTemplates/Dashlets/AM_TaskTemplatesDashlet/AM_TaskTemplatesDashlet.meta.php',
    'module' => 'AM_TaskTemplates',
  ),
  'AOK_KnowledgeBaseDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOK_KnowledgeBase/Dashlets/AOK_KnowledgeBaseDashlet/AOK_KnowledgeBaseDashlet.php',
    'class' => 'AOK_KnowledgeBaseDashlet',
    'meta' => 'modules/var/www/html/modules/AOK_KnowledgeBase/Dashlets/AOK_KnowledgeBaseDashlet/AOK_KnowledgeBaseDashlet.meta.php',
    'module' => 'AOK_KnowledgeBase',
  ),
  'AOK_Knowledge_Base_CategoriesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOK_Knowledge_Base_Categories/Dashlets/AOK_Knowledge_Base_CategoriesDashlet/AOK_Knowledge_Base_CategoriesDashlet.php',
    'class' => 'AOK_Knowledge_Base_CategoriesDashlet',
    'meta' => 'modules/var/www/html/modules/AOK_Knowledge_Base_Categories/Dashlets/AOK_Knowledge_Base_CategoriesDashlet/AOK_Knowledge_Base_CategoriesDashlet.meta.php',
    'module' => 'AOK_Knowledge_Base_Categories',
  ),
  'AORReportsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOR_Reports/Dashlets/AORReportsDashlet/AORReportsDashlet.php',
    'class' => 'AORReportsDashlet',
    'meta' => 'modules/var/www/html/modules/AOR_Reports/Dashlets/AORReportsDashlet/AORReportsDashlet.meta.php',
    'module' => 'AOR_Reports',
  ),
  'AOS_ContractsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOS_Contracts/Dashlets/AOS_ContractsDashlet/AOS_ContractsDashlet.php',
    'class' => 'AOS_ContractsDashlet',
    'meta' => 'modules/var/www/html/modules/AOS_Contracts/Dashlets/AOS_ContractsDashlet/AOS_ContractsDashlet.meta.php',
    'module' => 'AOS_Contracts',
  ),
  'AOS_InvoicesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOS_Invoices/Dashlets/AOS_InvoicesDashlet/AOS_InvoicesDashlet.php',
    'class' => 'AOS_InvoicesDashlet',
    'meta' => 'modules/var/www/html/modules/AOS_Invoices/Dashlets/AOS_InvoicesDashlet/AOS_InvoicesDashlet.meta.php',
    'module' => 'AOS_Invoices',
  ),
  'AOS_PDF_TemplatesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOS_PDF_Templates/Dashlets/AOS_PDF_TemplatesDashlet/AOS_PDF_TemplatesDashlet.php',
    'class' => 'AOS_PDF_TemplatesDashlet',
    'meta' => 'modules/var/www/html/modules/AOS_PDF_Templates/Dashlets/AOS_PDF_TemplatesDashlet/AOS_PDF_TemplatesDashlet.meta.php',
    'module' => 'AOS_PDF_Templates',
  ),
  'AOS_ProductsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOS_Products/Dashlets/AOS_ProductsDashlet/AOS_ProductsDashlet.php',
    'class' => 'AOS_ProductsDashlet',
    'meta' => 'modules/var/www/html/modules/AOS_Products/Dashlets/AOS_ProductsDashlet/AOS_ProductsDashlet.meta.php',
    'module' => 'AOS_Products',
  ),
  'AOS_Product_CategoriesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOS_Product_Categories/Dashlets/AOS_Product_CategoriesDashlet/AOS_Product_CategoriesDashlet.php',
    'class' => 'AOS_Product_CategoriesDashlet',
    'meta' => 'modules/var/www/html/modules/AOS_Product_Categories/Dashlets/AOS_Product_CategoriesDashlet/AOS_Product_CategoriesDashlet.meta.php',
    'module' => 'AOS_Product_Categories',
  ),
  'AOS_QuotesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOS_Quotes/Dashlets/AOS_QuotesDashlet/AOS_QuotesDashlet.php',
    'class' => 'AOS_QuotesDashlet',
    'meta' => 'modules/var/www/html/modules/AOS_Quotes/Dashlets/AOS_QuotesDashlet/AOS_QuotesDashlet.meta.php',
    'module' => 'AOS_Quotes',
  ),
  'AOW_ProcessedDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOW_Processed/Dashlets/AOW_ProcessedDashlet/AOW_ProcessedDashlet.php',
    'class' => 'AOW_ProcessedDashlet',
    'meta' => 'modules/var/www/html/modules/AOW_Processed/Dashlets/AOW_ProcessedDashlet/AOW_ProcessedDashlet.meta.php',
    'module' => 'AOW_Processed',
  ),
  'AOW_WorkFlowDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AOW_WorkFlow/Dashlets/AOW_WorkFlowDashlet/AOW_WorkFlowDashlet.php',
    'class' => 'AOW_WorkFlowDashlet',
    'meta' => 'modules/var/www/html/modules/AOW_WorkFlow/Dashlets/AOW_WorkFlowDashlet/AOW_WorkFlowDashlet.meta.php',
    'module' => 'AOW_WorkFlow',
  ),
  'AP1_Alternative_PartDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/AP1_Alternative_Part/Dashlets/AP1_Alternative_PartDashlet/AP1_Alternative_PartDashlet.php',
    'class' => 'AP1_Alternative_PartDashlet',
    'meta' => 'modules/var/www/html/modules/AP1_Alternative_Part/Dashlets/AP1_Alternative_PartDashlet/AP1_Alternative_PartDashlet.meta.php',
    'module' => 'AP1_Alternative_Part',
  ),
  'MyBugsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.php',
    'class' => 'MyBugsDashlet',
    'meta' => 'modules/var/www/html/modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.meta.php',
    'module' => 'Bugs',
  ),
  'CalendarDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.php',
    'class' => 'CalendarDashlet',
    'meta' => 'modules/var/www/html/modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.meta.php',
    'module' => 'Calendar',
  ),
  'MyCallsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.php',
    'class' => 'MyCallsDashlet',
    'meta' => 'modules/var/www/html/modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.meta.php',
    'module' => 'Calls',
  ),
  'TopCampaignsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.php',
    'class' => 'TopCampaignsDashlet',
    'meta' => 'modules/var/www/html/modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'MyCasesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.php',
    'class' => 'MyCasesDashlet',
    'meta' => 'modules/var/www/html/modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.meta.php',
    'module' => 'Cases',
  ),
  'CampaignROIChartDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.php',
    'class' => 'CampaignROIChartDashlet',
    'meta' => 'modules/var/www/html/modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'MyPipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.php',
    'class' => 'MyPipelineBySalesStageDashlet',
    'meta' => 'modules/var/www/html/modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceByOutcomeDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.php',
    'class' => 'OpportunitiesByLeadSourceByOutcomeDashlet',
    'meta' => 'modules/var/www/html/modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.php',
    'class' => 'OpportunitiesByLeadSourceDashlet',
    'meta' => 'modules/var/www/html/modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OutcomeByMonthDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.php',
    'class' => 'OutcomeByMonthDashlet',
    'meta' => 'modules/var/www/html/modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'PipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.php',
    'class' => 'PipelineBySalesStageDashlet',
    'meta' => 'modules/var/www/html/modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'CI1_Competitive_IntelligenceDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/CI1_Competitive_Intelligence/Dashlets/CI1_Competitive_IntelligenceDashlet/CI1_Competitive_IntelligenceDashlet.php',
    'class' => 'CI1_Competitive_IntelligenceDashlet',
    'meta' => 'modules/var/www/html/modules/CI1_Competitive_Intelligence/Dashlets/CI1_Competitive_IntelligenceDashlet/CI1_Competitive_IntelligenceDashlet.meta.php',
    'module' => 'CI1_Competitive_Intelligence',
  ),
  'MyContactsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.php',
    'class' => 'MyContactsDashlet',
    'meta' => 'modules/var/www/html/modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.meta.php',
    'module' => 'Contacts',
  ),
  'MyDocumentsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.php',
    'class' => 'MyDocumentsDashlet',
    'meta' => 'modules/var/www/html/modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.meta.php',
    'module' => 'Documents',
  ),
  'dtbc_ApprovalsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Approvals/Dashlets/dtbc_ApprovalsDashlet/dtbc_ApprovalsDashlet.php',
    'class' => 'dtbc_ApprovalsDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Approvals/Dashlets/dtbc_ApprovalsDashlet/dtbc_ApprovalsDashlet.meta.php',
    'module' => 'dtbc_Approvals',
  ),
  'dtbc_CasesSurveyDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_CasesSurvey/Dashlets/dtbc_CasesSurveyDashlet/dtbc_CasesSurveyDashlet.php',
    'class' => 'dtbc_CasesSurveyDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_CasesSurvey/Dashlets/dtbc_CasesSurveyDashlet/dtbc_CasesSurveyDashlet.meta.php',
    'module' => 'dtbc_CasesSurvey',
  ),
  'dtbc_Contact_RolesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Contact_Roles/Dashlets/dtbc_Contact_RolesDashlet/dtbc_Contact_RolesDashlet.php',
    'class' => 'dtbc_Contact_RolesDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Contact_Roles/Dashlets/dtbc_Contact_RolesDashlet/dtbc_Contact_RolesDashlet.meta.php',
    'module' => 'dtbc_Contact_Roles',
  ),
  'dtbc_Line_ItemDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Line_Item/Dashlets/dtbc_Line_ItemDashlet/dtbc_Line_ItemDashlet.php',
    'class' => 'dtbc_Line_ItemDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Line_Item/Dashlets/dtbc_Line_ItemDashlet/dtbc_Line_ItemDashlet.meta.php',
    'module' => 'dtbc_Line_Item',
  ),
  'dtbc_Login_LoggerDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Login_Logger/Dashlets/dtbc_Login_LoggerDashlet/dtbc_Login_LoggerDashlet.php',
    'class' => 'dtbc_Login_LoggerDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Login_Logger/Dashlets/dtbc_Login_LoggerDashlet/dtbc_Login_LoggerDashlet.meta.php',
    'module' => 'dtbc_Login_Logger',
  ),
  'dtbc_PricebookDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Pricebook/Dashlets/dtbc_PricebookDashlet/dtbc_PricebookDashlet.php',
    'class' => 'dtbc_PricebookDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Pricebook/Dashlets/dtbc_PricebookDashlet/dtbc_PricebookDashlet.meta.php',
    'module' => 'dtbc_Pricebook',
  ),
  'dtbc_Pricebook_ProductDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Pricebook_Product/Dashlets/dtbc_Pricebook_ProductDashlet/dtbc_Pricebook_ProductDashlet.php',
    'class' => 'dtbc_Pricebook_ProductDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Pricebook_Product/Dashlets/dtbc_Pricebook_ProductDashlet/dtbc_Pricebook_ProductDashlet.meta.php',
    'module' => 'dtbc_Pricebook_Product',
  ),
  'dtbc_Project_CommentsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Project_Comments/Dashlets/dtbc_Project_CommentsDashlet/dtbc_Project_CommentsDashlet.php',
    'class' => 'dtbc_Project_CommentsDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Project_Comments/Dashlets/dtbc_Project_CommentsDashlet/dtbc_Project_CommentsDashlet.meta.php',
    'module' => 'dtbc_Project_Comments',
  ),
  'dtbc_Project_StakeholdersDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Project_Stakeholders/Dashlets/dtbc_Project_StakeholdersDashlet/dtbc_Project_StakeholdersDashlet.php',
    'class' => 'dtbc_Project_StakeholdersDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Project_Stakeholders/Dashlets/dtbc_Project_StakeholdersDashlet/dtbc_Project_StakeholdersDashlet.meta.php',
    'module' => 'dtbc_Project_Stakeholders',
  ),
  'dtbc_StatusLogDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_StatusLog/Dashlets/dtbc_StatusLogDashlet/dtbc_StatusLogDashlet.php',
    'class' => 'dtbc_StatusLogDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_StatusLog/Dashlets/dtbc_StatusLogDashlet/dtbc_StatusLogDashlet.meta.php',
    'module' => 'dtbc_StatusLog',
  ),
  'dtbc_System_Workflow_SchedulingDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_System_Workflow_Scheduling/Dashlets/dtbc_System_Workflow_SchedulingDashlet/dtbc_System_Workflow_SchedulingDashlet.php',
    'class' => 'dtbc_System_Workflow_SchedulingDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_System_Workflow_Scheduling/Dashlets/dtbc_System_Workflow_SchedulingDashlet/dtbc_System_Workflow_SchedulingDashlet.meta.php',
    'module' => 'dtbc_System_Workflow_Scheduling',
  ),
  'dtbc_Year_Alliance_PointsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/dtbc_Year_Alliance_Points/Dashlets/dtbc_Year_Alliance_PointsDashlet/dtbc_Year_Alliance_PointsDashlet.php',
    'class' => 'dtbc_Year_Alliance_PointsDashlet',
    'meta' => 'modules/var/www/html/modules/dtbc_Year_Alliance_Points/Dashlets/dtbc_Year_Alliance_PointsDashlet/dtbc_Year_Alliance_PointsDashlet.meta.php',
    'module' => 'dtbc_Year_Alliance_Points',
  ),
  'MyEmailsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.php',
    'class' => 'MyEmailsDashlet',
    'meta' => 'modules/var/www/html/modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.meta.php',
    'module' => 'Emails',
  ),
  'FavoritesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Favorites/Dashlets/Favorites/FavoritesDashlet.php',
    'class' => 'FavoritesDashlet',
  ),
  'FP_eventsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/FP_events/Dashlets/FP_eventsDashlet/FP_eventsDashlet.php',
    'class' => 'FP_eventsDashlet',
    'meta' => 'modules/var/www/html/modules/FP_events/Dashlets/FP_eventsDashlet/FP_eventsDashlet.meta.php',
    'module' => 'FP_events',
  ),
  'FP_Event_LocationsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/FP_Event_Locations/Dashlets/FP_Event_LocationsDashlet/FP_Event_LocationsDashlet.php',
    'class' => 'FP_Event_LocationsDashlet',
    'meta' => 'modules/var/www/html/modules/FP_Event_Locations/Dashlets/FP_Event_LocationsDashlet/FP_Event_LocationsDashlet.meta.php',
    'module' => 'FP_Event_Locations',
  ),
  'FSK1_Field_Service_KitDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/FSK1_Field_Service_Kit/Dashlets/FSK1_Field_Service_KitDashlet/FSK1_Field_Service_KitDashlet.php',
    'class' => 'FSK1_Field_Service_KitDashlet',
    'meta' => 'modules/var/www/html/modules/FSK1_Field_Service_Kit/Dashlets/FSK1_Field_Service_KitDashlet/FSK1_Field_Service_KitDashlet.meta.php',
    'module' => 'FSK1_Field_Service_Kit',
  ),
  'ChartsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.php',
    'class' => 'ChartsDashlet',
    'meta' => 'modules/var/www/html/modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.meta.php',
  ),
  'iFrameDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.php',
    'class' => 'iFrameDashlet',
    'meta' => 'modules/var/www/html/modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.meta.php',
    'module' => 'Home',
  ),
  'InvadersDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.php',
    'class' => 'InvadersDashlet',
    'meta' => 'modules/var/www/html/modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.meta.php',
    'icon' => 'modules/var/www/html/modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.icon.jpg',
  ),
  'JotPadDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.php',
    'class' => 'JotPadDashlet',
    'meta' => 'modules/var/www/html/modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.meta.php',
  ),
  'RSSDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Home/Dashlets/RSSDashlet/RSSDashlet.php',
    'class' => 'RSSDashlet',
    'meta' => 'modules/var/www/html/modules/Home/Dashlets/RSSDashlet/RSSDashlet.meta.php',
    'icon' => 'modules/var/www/html/modules/Home/Dashlets/RSSDashlet/RSSDashlet.icon.jpg',
  ),
  'SugarNewsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.php',
    'class' => 'SugarNewsDashlet',
    'meta' => 'modules/var/www/html/modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.meta.php',
    'module' => 'Home',
  ),
  'IS1_Inside_SalesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/IS1_Inside_Sales/Dashlets/IS1_Inside_SalesDashlet/IS1_Inside_SalesDashlet.php',
    'class' => 'IS1_Inside_SalesDashlet',
    'meta' => 'modules/var/www/html/modules/IS1_Inside_Sales/Dashlets/IS1_Inside_SalesDashlet/IS1_Inside_SalesDashlet.meta.php',
    'module' => 'IS1_Inside_Sales',
  ),
  'jjwg_AreasDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/jjwg_Areas/Dashlets/jjwg_AreasDashlet/jjwg_AreasDashlet.php',
    'class' => 'jjwg_AreasDashlet',
    'meta' => 'modules/var/www/html/modules/jjwg_Areas/Dashlets/jjwg_AreasDashlet/jjwg_AreasDashlet.meta.php',
    'module' => 'jjwg_Areas',
  ),
  'jjwg_MapsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/jjwg_Maps/Dashlets/jjwg_MapsDashlet/jjwg_MapsDashlet.php',
    'class' => 'jjwg_MapsDashlet',
    'meta' => 'modules/var/www/html/modules/jjwg_Maps/Dashlets/jjwg_MapsDashlet/jjwg_MapsDashlet.meta.php',
    'module' => 'jjwg_Maps',
  ),
  'jjwg_MarkersDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/jjwg_Markers/Dashlets/jjwg_MarkersDashlet/jjwg_MarkersDashlet.php',
    'class' => 'jjwg_MarkersDashlet',
    'meta' => 'modules/var/www/html/modules/jjwg_Markers/Dashlets/jjwg_MarkersDashlet/jjwg_MarkersDashlet.meta.php',
    'module' => 'jjwg_Markers',
  ),
  'MyLeadsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.php',
    'class' => 'MyLeadsDashlet',
    'meta' => 'modules/var/www/html/modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.meta.php',
    'module' => 'Leads',
  ),
  'MyMeetingsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.php',
    'class' => 'MyMeetingsDashlet',
    'meta' => 'modules/var/www/html/modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.meta.php',
    'module' => 'Meetings',
  ),
  'MyNotesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.php',
    'class' => 'MyNotesDashlet',
    'meta' => 'modules/var/www/html/modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.meta.php',
    'module' => 'Notes',
  ),
  'MyClosedOpportunitiesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.php',
    'class' => 'MyClosedOpportunitiesDashlet',
    'meta' => 'modules/var/www/html/modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'MyOpportunitiesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.php',
    'class' => 'MyOpportunitiesDashlet',
    'meta' => 'modules/var/www/html/modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OS1_Outside_Sales_ActivityDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/OS1_Outside_Sales_Activity/Dashlets/OS1_Outside_Sales_ActivityDashlet/OS1_Outside_Sales_ActivityDashlet.php',
    'class' => 'OS1_Outside_Sales_ActivityDashlet',
    'meta' => 'modules/var/www/html/modules/OS1_Outside_Sales_Activity/Dashlets/OS1_Outside_Sales_ActivityDashlet/OS1_Outside_Sales_ActivityDashlet.meta.php',
    'module' => 'OS1_Outside_Sales_Activity',
  ),
  'OutboundEmailAccountsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/OutboundEmailAccounts/Dashlets/OutboundEmailAccountsDashlet/OutboundEmailAccountsDashlet.php',
    'class' => 'OutboundEmailAccountsDashlet',
    'meta' => 'modules/var/www/html/modules/OutboundEmailAccounts/Dashlets/OutboundEmailAccountsDashlet/OutboundEmailAccountsDashlet.meta.php',
    'module' => 'OutboundEmailAccounts',
  ),
  'P1_ProjectDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/P1_Project/Dashlets/P1_ProjectDashlet/P1_ProjectDashlet.php',
    'class' => 'P1_ProjectDashlet',
    'meta' => 'modules/var/www/html/modules/P1_Project/Dashlets/P1_ProjectDashlet/P1_ProjectDashlet.meta.php',
    'module' => 'P1_Project',
  ),
  'POS1_POS_TrackingDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/POS1_POS_Tracking/Dashlets/POS1_POS_TrackingDashlet/POS1_POS_TrackingDashlet.php',
    'class' => 'POS1_POS_TrackingDashlet',
    'meta' => 'modules/var/www/html/modules/POS1_POS_Tracking/Dashlets/POS1_POS_TrackingDashlet/POS1_POS_TrackingDashlet.meta.php',
    'module' => 'POS1_POS_Tracking',
  ),
  'MyProjectDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Project/Dashlets/MyProjectDashlet/MyProjectDashlet.php',
    'class' => 'MyProjectDashlet',
    'meta' => 'modules/var/www/html/modules/Project/Dashlets/MyProjectDashlet/MyProjectDashlet.meta.php',
    'module' => 'Project',
  ),
  'MyProjectTaskDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.php',
    'class' => 'MyProjectTaskDashlet',
    'meta' => 'modules/var/www/html/modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.meta.php',
    'module' => 'ProjectTask',
  ),
  'S1_SiteDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/S1_Site/Dashlets/S1_SiteDashlet/S1_SiteDashlet.php',
    'class' => 'S1_SiteDashlet',
    'meta' => 'modules/var/www/html/modules/S1_Site/Dashlets/S1_SiteDashlet/S1_SiteDashlet.meta.php',
    'module' => 'S1_Site',
  ),
  'SN1_Serial_NumberDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/SN1_Serial_Number/Dashlets/SN1_Serial_NumberDashlet/SN1_Serial_NumberDashlet.php',
    'class' => 'SN1_Serial_NumberDashlet',
    'meta' => 'modules/var/www/html/modules/SN1_Serial_Number/Dashlets/SN1_Serial_NumberDashlet/SN1_Serial_NumberDashlet.meta.php',
    'module' => 'SN1_Serial_Number',
  ),
  'SpotsDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Spots/Dashlets/SpotsDashlet/SpotsDashlet.php',
    'class' => 'SpotsDashlet',
    'meta' => 'modules/var/www/html/modules/Spots/Dashlets/SpotsDashlet/SpotsDashlet.meta.php',
  ),
  'SugarFeedDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.php',
    'class' => 'SugarFeedDashlet',
    'meta' => 'modules/var/www/html/modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.meta.php',
    'module' => 'SugarFeed',
  ),
  'MyTasksDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.php',
    'class' => 'MyTasksDashlet',
    'meta' => 'modules/var/www/html/modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.meta.php',
    'module' => 'Tasks',
  ),
  'TLA1_Training_LMS_AbsorbDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/TLA1_Training_LMS_Absorb/Dashlets/TLA1_Training_LMS_AbsorbDashlet/TLA1_Training_LMS_AbsorbDashlet.php',
    'class' => 'TLA1_Training_LMS_AbsorbDashlet',
    'meta' => 'modules/var/www/html/modules/TLA1_Training_LMS_Absorb/Dashlets/TLA1_Training_LMS_AbsorbDashlet/TLA1_Training_LMS_AbsorbDashlet.meta.php',
    'module' => 'TLA1_Training_LMS_Absorb',
  ),
  'UZC1_USA_ZIP_CodesDashlet' => 
  array (
    'file' => 'modules/var/www/html/modules/UZC1_USA_ZIP_Codes/Dashlets/UZC1_USA_ZIP_CodesDashlet/UZC1_USA_ZIP_CodesDashlet.php',
    'class' => 'UZC1_USA_ZIP_CodesDashlet',
    'meta' => 'modules/var/www/html/modules/UZC1_USA_ZIP_Codes/Dashlets/UZC1_USA_ZIP_CodesDashlet/UZC1_USA_ZIP_CodesDashlet.meta.php',
    'module' => 'UZC1_USA_ZIP_Codes',
  ),
);