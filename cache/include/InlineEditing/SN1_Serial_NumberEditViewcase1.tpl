
<input type="text" name="{$fields.case1.name}" class="sqsEnabled" tabindex="1" id="{$fields.case1.name}" size="" value="{$fields.case1.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.case1.id_name}" 
	id="{$fields.case1.id_name}" 
	value="{$fields.acase_id_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.case1.name}" id="btn_{$fields.case1.name}" tabindex="1" title="{sugar_translate label="LBL_SELECT_BUTTON_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_SELECT_BUTTON_LABEL"}"
onclick='open_popup(
    "{$fields.case1.module}", 
	600, 
	400, 
	"", 
	true, 
	false, 
	{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":{/literal}"{$fields.case1.id_name}"{literal},"case_number":{/literal}"{$fields.case1.name}"{literal}}}{/literal}, 
	"single", 
	true
);' ><img src="{sugar_getimagepath file="id-ff-select.png"}"></button><button type="button" name="btn_clr_{$fields.case1.name}" id="btn_clr_{$fields.case1.name}" tabindex="1" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.case1.name}', '{$fields.case1.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_LABEL"}" ><img src="{sugar_getimagepath file="id-ff-clear.png"}"></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.case1.name}']) != 'undefined'",
		enableQS
);
</script>