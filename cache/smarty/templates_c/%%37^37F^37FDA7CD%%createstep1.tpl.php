<?php /* Smarty version 2.6.29, created on 2018-03-14 17:57:05
         compiled from custom/modules/Cases/tpl/createstep1.tpl */ ?>
<?php echo '
<style>
	#container {
		padding-left: 20px;
	}
	
	#buttons {
		padding-top: 10px;
		text-align: center;
		width: 50%;
	}
</style>

<script>
	function btnRedirect(btnType) {
		// Default link is cancel
		var link = "index.php?module=Cases&action=index";
		
		// If additional things has set
		if ($(\'#return_module\').length && $(\'#return_action\').length && $(\'#return_id\').length)
			link = "index.php?module=" + $(\'#return_module\').val() + "&action=" + $(\'#return_action\').val() + "&record=" + $(\'#return_id\').val();
		
		if (btnType == 1) {
			// Continue
			var parentType = "";
			if ($(\'#parent_type\').length)
				parentType = $(\'#parent_type\').val();
			
			var parentId = "";
			if ($(\'#parent_id\').length)
				parentId = $(\'#parent_id\').val();
				
			link = "index.php?module=Cases&action=EditView&return_module=Cases&return_action=DetailView&caseType=" + $(\'#caseType\').val() + "&parent_type=" + parentType + "&parent_id=" + parentId;
			
			if ($(\'#return_module\').length && $(\'#return_action\').length && $(\'#return_id\').length) {
				link = "index.php?module=Cases&action=EditView&return_module=" + $(\'#return_module\').val() + "&return_action=" + $(\'#return_action\').val() + "&return_id=" + $(\'#return_id\').val() + "&caseType=" + $(\'#caseType\').val() + "&parent_type=" + $(\'#parent_type\').val() + "&parent_id=" + $(\'#parent_id\').val();
				if ($(\'#return_relationship\').length)
					link += "&return_relationship=" + $(\'#return_relationship\').val();
				if ($(\'#return_name\').length)
					link += "&return_name=" + $(\'#return_name\').val();
				if ($(\'#contact_id\').length)
					link += "&contact_id=" + $(\'#contact_id\').val();
				if ($(\'#account_id\').length)
					link += "&account_id=" + $(\'#account_id\').val();
			}
				
		}
		
		// Create link
		var tIndex = document.location.href.indexOf("index.php");
		var url = document.location.href.substr(0, tIndex);
		window.location = url + link;
		return false;
	}
</script>
'; ?>


<h2><?php echo $this->_tpl_vars['title']; ?>
</h2>
<div id="container">
	<?php $_from = $this->_tpl_vars['requests']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
	   <input type="hidden" id="<?php echo $this->_tpl_vars['k']; ?>
" value="<?php echo $this->_tpl_vars['v']; ?>
"/>
	<?php endforeach; endif; unset($_from); ?>
	<div id="fields">
		<label><?php echo $this->_tpl_vars['label']; ?>
</label>
		<select id="caseType">
			<?php $_from = $this->_tpl_vars['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['label']):
?>
				<option value='<?php echo $this->_tpl_vars['key']; ?>
'><?php echo $this->_tpl_vars['label']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>		
		</select>
	</div>
	<div id="buttons">
		<input type="submit" onclick="return btnRedirect(1);" value="<?php echo $this->_tpl_vars['btnContinue']; ?>
" />
		<input type="submit"  onclick="return btnRedirect(2);" value="<?php echo $this->_tpl_vars['btnCancel']; ?>
" />
	</div>
</div>