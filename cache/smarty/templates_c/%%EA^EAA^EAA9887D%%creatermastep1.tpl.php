<?php /* Smarty version 2.6.29, created on 2018-03-14 18:00:31
         compiled from custom/modules/Cases/tpl/creatermastep1.tpl */ ?>
<?php echo '
<style>
	#container {
		padding-left: 20px;
	}
	
	#buttons {
		padding-top: 10px;
		text-align: center;
		width: 50%;
	}
	
	.valuelabel {
		font-weight: bold;
	}
	
	h3 {
		margin-top: 10px;
	}
</style>

<script>
	function btnRedirect(btnType) {
		// Default link is cancel
		var link = "index.php?module=Cases&action=DetailView&record='; ?>
<?php echo $this->_tpl_vars['recordid']; ?>
<?php echo '";
		var confValue = $("input:radio[name =\'confirmation\']:checked").val();
		if (btnType == 1) {
			if (confValue == undefined) {
				// Confirmation answer is mandatory
				alert(\''; ?>
<?php echo $this->_tpl_vars['alertmessage']; ?>
<?php echo '\');
				return false;
			}
			// Continue
			link = "index.php?module=Cases&action=EditView&return_module=Cases&return_action=DetailView&record='; ?>
<?php echo $this->_tpl_vars['recordid']; ?>
<?php echo '&newrma=2&confValue=" + confValue;
		}
		// Create link
		var tIndex = document.location.href.indexOf("index.php");
		var url = document.location.href.substr(0, tIndex);
		window.location = url + link;
		return false;
	}
</script>
'; ?>


<h2><?php echo $this->_tpl_vars['title']; ?>
</h2>
<div id="container">
	<h3><?php echo $this->_tpl_vars['label_cdetails_title']; ?>
</h3>
	<div id="fields1">
		<table>
			<tbody>
				<tr>
					<td><span class="valuelabel"><?php echo $this->_tpl_vars['label_cname']; ?>
</span></td>
					<td width="10"></td>
					<td><span class="value"><?php echo $this->_tpl_vars['v_cname']; ?>
</span></td>
				</tr>
				
				<tr>
					<td><span class="valuelabel"><?php echo $this->_tpl_vars['label_scountry']; ?>
</span></td>
					<td width="10"></td>
					<td><span class="value"><?php echo $this->_tpl_vars['v_scountry']; ?>
</span></td>
				</tr>
				<tr>
					<td><span class="valuelabel"><?php echo $this->_tpl_vars['label_scity']; ?>
</span></td>
					<td width="10"></td>
					<td><span class="value"><?php echo $this->_tpl_vars['v_scity']; ?>
</span></td>
				</tr>
				<tr>
					<td><span class="valuelabel"><?php echo $this->_tpl_vars['label_saddress']; ?>
</span></td>
					<td width="10"></td>
					<td><span class="value"><?php echo $this->_tpl_vars['v_saddress']; ?>
</span></td>
				</tr>
				<tr>
					<td><span class="valuelabel"><?php echo $this->_tpl_vars['label_szip']; ?>
</span></td>
					<td width="10"></td>
					<td><span class="value"><?php echo $this->_tpl_vars['v_szip']; ?>
</span></td>
				</tr>
			</tbody>
		</table>
	</div>
	<h3><?php echo $this->_tpl_vars['label_confirmed_title']; ?>
</h3>
	<div id="fields2">
		<input type="radio" name="confirmation" value="1">&nbsp;<?php echo $this->_tpl_vars['label_yes']; ?>
<br/>
		<input type="radio" name="confirmation" value="0">&nbsp;<?php echo $this->_tpl_vars['label_no']; ?>

	</div>
	<div id="buttons">
		<input type="submit" onclick="return btnRedirect(1);" value="<?php echo $this->_tpl_vars['btnContinue']; ?>
" />
		<input type="submit"  onclick="return btnRedirect(2);" value="<?php echo $this->_tpl_vars['btnCancel']; ?>
" />
	</div>
</div>