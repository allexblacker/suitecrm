<?php /* Smarty version 2.6.29, created on 2018-03-15 09:41:52
         compiled from cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 46, false),array('modifier', 'strip_semicolon', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 99, false),array('modifier', 'lookup', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 137, false),array('modifier', 'count', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 239, false),array('modifier', 'default', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 1123, false),array('function', 'sugar_include', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 61, false),array('function', 'sugar_translate', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 82, false),array('function', 'counter', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 104, false),array('function', 'html_options', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 112, false),array('function', 'sugar_getimagepath', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 140, false),array('function', 'sugar_getimage', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 1085, false),array('function', 'sugar_getjspath', 'cache/themes/SolarEdgeP/modules/Cases/field_engineer_id_EditView.tpl', 1093, false),)), $this); ?>


<script>
    <?php echo '
    $(document).ready(function(){
	    $("ul.clickMenu").each(function(index, node){
	        $(node).sugarActionMenu();
	    });

        if($(\'.edit-view-pagination\').children().length == 0) $(\'.saveAndContinue\').remove();
    });
    '; ?>

</script>
<div class="clear"></div>
<form action="index.php" method="POST" name="<?php echo $this->_tpl_vars['form_name']; ?>
" id="<?php echo $this->_tpl_vars['form_id']; ?>
" <?php echo $this->_tpl_vars['enctype']; ?>
>
<div class="edit-view-pagination-mobile-container">
<div class="edit-view-pagination edit-view-mobile-pagination">
</div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
<input type="hidden" name="module" value="<?php echo $this->_tpl_vars['module']; ?>
">
<?php if (isset ( $_REQUEST['isDuplicate'] ) && $_REQUEST['isDuplicate'] == 'true'): ?>
<input type="hidden" name="record" value="">
<input type="hidden" name="duplicateSave" value="true">
<input type="hidden" name="duplicateId" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<?php else: ?>
<input type="hidden" name="record" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<?php endif; ?>
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="action">
<input type="hidden" name="return_module" value="<?php echo $_REQUEST['return_module']; ?>
">
<input type="hidden" name="return_action" value="<?php echo $_REQUEST['return_action']; ?>
">
<input type="hidden" name="return_id" value="<?php echo $_REQUEST['return_id']; ?>
">
<input type="hidden" name="module_tab"> 
<input type="hidden" name="contact_role">
<?php if (( ! empty ( $_REQUEST['return_module'] ) || ! empty ( $_REQUEST['relate_to'] ) ) && ! ( isset ( $_REQUEST['isDuplicate'] ) && $_REQUEST['isDuplicate'] == 'true' )): ?>
<input type="hidden" name="relate_to" value="<?php if ($_REQUEST['return_relationship']): ?><?php echo $_REQUEST['return_relationship']; ?>
<?php elseif ($_REQUEST['relate_to'] && empty ( $_REQUEST['from_dcmenu'] )): ?><?php echo $_REQUEST['relate_to']; ?>
<?php elseif (empty ( $this->_tpl_vars['isDCForm'] ) && empty ( $_REQUEST['from_dcmenu'] )): ?><?php echo $_REQUEST['return_module']; ?>
<?php endif; ?>">
<input type="hidden" name="relate_id" value="<?php echo $_REQUEST['return_id']; ?>
">
<?php endif; ?>
<input type="hidden" name="offset" value="<?php echo $this->_tpl_vars['offset']; ?>
">
<?php $this->assign('place', '_HEADER'); ?> <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
<?php if ($this->_tpl_vars['bean']->aclAccess('save')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_SAVE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_SAVE_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('EditView'); <?php if ($this->_tpl_vars['isDuplicate']): ?>_form.return_id.value=''; <?php endif; ?>_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_SAVE_BUTTON_LABEL']; ?>
" id="SAVE"><?php endif; ?> 
<?php if (! empty ( $_REQUEST['return_action'] ) && ( $_REQUEST['return_action'] == 'DetailView' && ! empty ( $_REQUEST['return_id'] ) )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo ((is_array($_tmp=$_REQUEST['return_id'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'); return false;" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" type="button" id="CANCEL"> <?php elseif (! empty ( $_REQUEST['return_action'] ) && ( $_REQUEST['return_action'] == 'DetailView' && ! empty ( $this->_tpl_vars['fields']['id']['value'] ) )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php elseif (! empty ( $_REQUEST['return_action'] ) && ( $_REQUEST['return_action'] == 'DetailView' && empty ( $this->_tpl_vars['fields']['id']['value'] ) ) && empty ( $_REQUEST['return_id'] )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php elseif (! empty ( $_REQUEST['return_action'] ) && ! empty ( $_REQUEST['return_module'] )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=<?php echo $_REQUEST['return_action']; ?>
&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php elseif (empty ( $_REQUEST['return_action'] ) || empty ( $_REQUEST['return_id'] ) && ! empty ( $this->_tpl_vars['fields']['id']['value'] )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=Cases'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php else: ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo ((is_array($_tmp=$_REQUEST['return_id'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php endif; ?>
<?php if ($this->_tpl_vars['showVCRControl']): ?>
<button type="button" id="save_and_continue" class="button saveAndContinue" title="<?php echo $this->_tpl_vars['app_strings']['LBL_SAVE_AND_CONTINUE']; ?>
" onClick="SUGAR.saveAndContinue(this);">
<?php echo $this->_tpl_vars['APP']['LBL_SAVE_AND_CONTINUE']; ?>

</button>
<?php endif; ?>
<?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=Cases", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?>
</div>
</td>
<td align='right' class="edit-view-pagination-desktop-container">
<div class="edit-view-pagination edit-view-pagination-desktop">
</div>
</td>
</tr>
</table>
<?php echo smarty_function_sugar_include(array('include' => $this->_tpl_vars['includes']), $this);?>

<div id="EditView_tabs">

<ul class="nav nav-tabs">
</ul>
<div class="clearfix"></div>
<div class="tab-content" style="padding: 0; border: 0;">

<div class="tab-pane panel-collapse">&nbsp;</div>
</div>

<div class="panel-content">
<div>&nbsp;</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIELD_ENGINEER_CASE','module' => 'Cases'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="FIELD_ENGINEER" dtbc-data="field_engineer_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'FIELD_ENGINEER','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="field_engineer_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! isset ( $this->_tpl_vars['config']['enable_autocomplete'] ) || $this->_tpl_vars['config']['enable_autocomplete'] == false): ?>
<select name="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
" 
title=''       
>
<?php if (isset ( $this->_tpl_vars['fields']['field_engineer_c']['value'] ) && $this->_tpl_vars['fields']['field_engineer_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_c']['default']), $this);?>

<?php endif; ?>
</select>
<?php else: ?>
<?php $this->assign('field_options', $this->_tpl_vars['fields']['field_engineer_c']['options']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['field_engineer_c']['value']; ?>
<?php $this->_smarty_vars['capture']['field_val'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('field_val', $this->_smarty_vars['capture']['field_val']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php $this->_smarty_vars['capture']['ac_key'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('ac_key', $this->_smarty_vars['capture']['ac_key']); ?>
<select style='display:none' name="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
" 
title=''          
>
<?php if (isset ( $this->_tpl_vars['fields']['field_engineer_c']['value'] ) && $this->_tpl_vars['fields']['field_engineer_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_c']['default']), $this);?>

<?php endif; ?>
</select>
<input
id="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-input"
name="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-input"
size="30"
value="<?php echo ((is_array($_tmp=$this->_tpl_vars['field_val'])) ? $this->_run_mod_handler('lookup', true, $_tmp, $this->_tpl_vars['field_options']) : smarty_modifier_lookup($_tmp, $this->_tpl_vars['field_options'])); ?>
"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-down.png"), $this);?>
" id="<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-image"></button><button type="button"
id="btn-clear-<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-input', '<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
');sync_<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
()"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-clear.png"), $this);?>
"></button>
</span>
<?php echo '
<script>
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo ' = [];
	'; ?>


			<?php echo '
		(function (){
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php echo '");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:\'\'};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use(\'datasource\', \'datasource-jsonschema\',function (Y) {
				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value==\'\' && selectElem.options[i].innerHTML==\'\'))
				    			ret.push({\'key\':selectElem.options[i].value,\'text\':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		'; ?>

	
	<?php echo '
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	'; ?>

			
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputNode = Y.one('#<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-input');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputImage = Y.one('#<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-image');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputHidden = Y.one('#<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
');
	
			<?php echo '
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php echo '");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'change\');
			}

			//global variable 
			sync_'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php echo ' = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php echo '");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\');

				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.simulate(\'keyup\');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById(\''; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
-input<?php echo '\'))
						SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php echo '", syncFromHiddenToWidget);
		'; ?>


		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions = <?php echo count($this->_tpl_vars['field_options']); ?>
;
		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 300) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 200;
			<?php echo '
		}
		'; ?>

		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 3000) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 500;
			<?php echo '
		}
		'; ?>

		
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.optionsVisible = false;
	
	<?php echo '
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		'; ?>

		minQueryLength: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen,
		queryDelay: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay,
		zIndex: 99999,

				
		<?php echo '
		source: SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds,
		
		resultTextLocator: \'text\',
		resultHighlighter: \'phraseMatch\',
		resultFilters: \'phraseMatch\',
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName(\'dccontent\');
		if(hover[0] != null){
			if (ex) {
				var h = \'1000px\';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = \'\';
			}
		}
	}
		
	if('; ?>
SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen<?php echo ' == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function () {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.sendRequest(\'\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'click\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'click\');
		});
		
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'dblclick\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'dblclick\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'focus\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mouseup\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mouseup\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mousedown\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mousedown\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'blur\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'blur\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = false;
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_c']['name']; ?>
<?php echo '");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputImage.ancestor().on(\'click\', function () {
		if (SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.blur();
		} else {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'query\', function () {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.set(\'value\', \'\');
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'visibleChange\', function (e) {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'select\', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
'; ?>

<?php endif; ?>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="CASE_CITY" dtbc-data="case_city_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'CASE_CITY','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="case_city_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['case_city_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_city_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_city_c']['value']); ?>
<?php endif; ?>  
<input type='text' name='<?php echo $this->_tpl_vars['fields']['case_city_c']['name']; ?>
' 
id='<?php echo $this->_tpl_vars['fields']['case_city_c']['name']; ?>
' size='30' 
maxlength='255' 
value='<?php echo $this->_tpl_vars['value']; ?>
' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="FIELD_ENGINEER_NAME" dtbc-data="field_engineer_name_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'FIELD_ENGINEER_NAME','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="field_engineer_name_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! isset ( $this->_tpl_vars['config']['enable_autocomplete'] ) || $this->_tpl_vars['config']['enable_autocomplete'] == false): ?>
<select name="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
" 
title=''       
>
<?php if (isset ( $this->_tpl_vars['fields']['field_engineer_name_c']['value'] ) && $this->_tpl_vars['fields']['field_engineer_name_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_name_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_name_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_name_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_name_c']['default']), $this);?>

<?php endif; ?>
</select>
<?php else: ?>
<?php $this->assign('field_options', $this->_tpl_vars['fields']['field_engineer_name_c']['options']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['value']; ?>
<?php $this->_smarty_vars['capture']['field_val'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('field_val', $this->_smarty_vars['capture']['field_val']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php $this->_smarty_vars['capture']['ac_key'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('ac_key', $this->_smarty_vars['capture']['ac_key']); ?>
<select style='display:none' name="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
" 
title=''          
>
<?php if (isset ( $this->_tpl_vars['fields']['field_engineer_name_c']['value'] ) && $this->_tpl_vars['fields']['field_engineer_name_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_name_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_name_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['field_engineer_name_c']['options'],'selected' => $this->_tpl_vars['fields']['field_engineer_name_c']['default']), $this);?>

<?php endif; ?>
</select>
<input
id="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-input"
name="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-input"
size="30"
value="<?php echo ((is_array($_tmp=$this->_tpl_vars['field_val'])) ? $this->_run_mod_handler('lookup', true, $_tmp, $this->_tpl_vars['field_options']) : smarty_modifier_lookup($_tmp, $this->_tpl_vars['field_options'])); ?>
"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-down.png"), $this);?>
" id="<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-image"></button><button type="button"
id="btn-clear-<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-input', '<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
');sync_<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
()"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-clear.png"), $this);?>
"></button>
</span>
<?php echo '
<script>
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo ' = [];
	'; ?>


			<?php echo '
		(function (){
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php echo '");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:\'\'};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use(\'datasource\', \'datasource-jsonschema\',function (Y) {
				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value==\'\' && selectElem.options[i].innerHTML==\'\'))
				    			ret.push({\'key\':selectElem.options[i].value,\'text\':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		'; ?>

	
	<?php echo '
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	'; ?>

			
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputNode = Y.one('#<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-input');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputImage = Y.one('#<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-image');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputHidden = Y.one('#<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
');
	
			<?php echo '
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php echo '");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'change\');
			}

			//global variable 
			sync_'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php echo ' = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php echo '");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\');

				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.simulate(\'keyup\');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById(\''; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
-input<?php echo '\'))
						SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php echo '", syncFromHiddenToWidget);
		'; ?>


		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions = <?php echo count($this->_tpl_vars['field_options']); ?>
;
		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 300) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 200;
			<?php echo '
		}
		'; ?>

		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 3000) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 500;
			<?php echo '
		}
		'; ?>

		
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.optionsVisible = false;
	
	<?php echo '
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		'; ?>

		minQueryLength: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen,
		queryDelay: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay,
		zIndex: 99999,

				
		<?php echo '
		source: SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds,
		
		resultTextLocator: \'text\',
		resultHighlighter: \'phraseMatch\',
		resultFilters: \'phraseMatch\',
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName(\'dccontent\');
		if(hover[0] != null){
			if (ex) {
				var h = \'1000px\';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = \'\';
			}
		}
	}
		
	if('; ?>
SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen<?php echo ' == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function () {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.sendRequest(\'\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'click\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'click\');
		});
		
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'dblclick\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'dblclick\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'focus\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mouseup\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mouseup\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mousedown\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mousedown\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'blur\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'blur\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = false;
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['field_engineer_name_c']['name']; ?>
<?php echo '");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputImage.ancestor().on(\'click\', function () {
		if (SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.blur();
		} else {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'query\', function () {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.set(\'value\', \'\');
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'visibleChange\', function (e) {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'select\', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
'; ?>

<?php endif; ?>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="CASE_ZIP" dtbc-data="case_zip_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'CASE_ZIP','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="case_zip_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['case_zip_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_zip_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_zip_c']['value']); ?>
<?php endif; ?>  
<input type='text' name='<?php echo $this->_tpl_vars['fields']['case_zip_c']['name']; ?>
' 
id='<?php echo $this->_tpl_vars['fields']['case_zip_c']['name']; ?>
' size='30' 
maxlength='255' 
value='<?php echo $this->_tpl_vars['value']; ?>
' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE" dtbc-data="s1_site_cases_name_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="s1_site_cases_name"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<input type="text" name="<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
" class="sqsEnabled" tabindex="" id="<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
" size="" value="<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['value']; ?>
" title='' autocomplete="off"  	 >
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['id_name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['id_name']; ?>
" 
value="<?php echo $this->_tpl_vars['fields']['s1_site_casess1_site_ida']['value']; ?>
">
<span class="id-ff multiple">
<button type="button" name="btn_<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
" id="btn_<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
" tabindex="" title="<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SELECT_BUTTON_TITLE'), $this);?>
" class="button firstChild" value="<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SELECT_BUTTON_LABEL'), $this);?>
"
onclick='open_popup(
"<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['module']; ?>
", 
600, 
400, 
"", 
true, 
false, 
<?php echo '{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"s1_site_casess1_site_ida","name":"s1_site_cases_name"}}'; ?>
, 
"single", 
true
);' ><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-select.png"), $this);?>
"></button><button type="button" name="btn_clr_<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
" id="btn_clr_<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
" tabindex="" title="<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCESSKEY_CLEAR_RELATE_TITLE'), $this);?>
"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
', '<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['id_name']; ?>
');"  value="<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCESSKEY_CLEAR_RELATE_LABEL'), $this);?>
" ><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-clear.png"), $this);?>
"></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['<?php echo $this->_tpl_vars['form_name']; ?>
_<?php echo $this->_tpl_vars['fields']['s1_site_cases_name']['name']; ?>
']) != 'undefined'",
		enableQS
);
</script>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TILE_ROOF" dtbc-data="tile_roof_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_TILE_ROOF','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="tile_roof_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! isset ( $this->_tpl_vars['config']['enable_autocomplete'] ) || $this->_tpl_vars['config']['enable_autocomplete'] == false): ?>
<select name="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
" 
title=''       
>
<?php if (isset ( $this->_tpl_vars['fields']['tile_roof_c']['value'] ) && $this->_tpl_vars['fields']['tile_roof_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['tile_roof_c']['options'],'selected' => $this->_tpl_vars['fields']['tile_roof_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['tile_roof_c']['options'],'selected' => $this->_tpl_vars['fields']['tile_roof_c']['default']), $this);?>

<?php endif; ?>
</select>
<?php else: ?>
<?php $this->assign('field_options', $this->_tpl_vars['fields']['tile_roof_c']['options']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['tile_roof_c']['value']; ?>
<?php $this->_smarty_vars['capture']['field_val'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('field_val', $this->_smarty_vars['capture']['field_val']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php $this->_smarty_vars['capture']['ac_key'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('ac_key', $this->_smarty_vars['capture']['ac_key']); ?>
<select style='display:none' name="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
" 
title=''          
>
<?php if (isset ( $this->_tpl_vars['fields']['tile_roof_c']['value'] ) && $this->_tpl_vars['fields']['tile_roof_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['tile_roof_c']['options'],'selected' => $this->_tpl_vars['fields']['tile_roof_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['tile_roof_c']['options'],'selected' => $this->_tpl_vars['fields']['tile_roof_c']['default']), $this);?>

<?php endif; ?>
</select>
<input
id="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-input"
name="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-input"
size="30"
value="<?php echo ((is_array($_tmp=$this->_tpl_vars['field_val'])) ? $this->_run_mod_handler('lookup', true, $_tmp, $this->_tpl_vars['field_options']) : smarty_modifier_lookup($_tmp, $this->_tpl_vars['field_options'])); ?>
"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-down.png"), $this);?>
" id="<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-image"></button><button type="button"
id="btn-clear-<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-input', '<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
');sync_<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
()"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-clear.png"), $this);?>
"></button>
</span>
<?php echo '
<script>
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo ' = [];
	'; ?>


			<?php echo '
		(function (){
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php echo '");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:\'\'};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use(\'datasource\', \'datasource-jsonschema\',function (Y) {
				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value==\'\' && selectElem.options[i].innerHTML==\'\'))
				    			ret.push({\'key\':selectElem.options[i].value,\'text\':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		'; ?>

	
	<?php echo '
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	'; ?>

			
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputNode = Y.one('#<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-input');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputImage = Y.one('#<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-image');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputHidden = Y.one('#<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
');
	
			<?php echo '
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php echo '");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'change\');
			}

			//global variable 
			sync_'; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php echo ' = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php echo '");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\');

				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.simulate(\'keyup\');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById(\''; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
-input<?php echo '\'))
						SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("'; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php echo '", syncFromHiddenToWidget);
		'; ?>


		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions = <?php echo count($this->_tpl_vars['field_options']); ?>
;
		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 300) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 200;
			<?php echo '
		}
		'; ?>

		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 3000) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 500;
			<?php echo '
		}
		'; ?>

		
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.optionsVisible = false;
	
	<?php echo '
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		'; ?>

		minQueryLength: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen,
		queryDelay: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay,
		zIndex: 99999,

				
		<?php echo '
		source: SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds,
		
		resultTextLocator: \'text\',
		resultHighlighter: \'phraseMatch\',
		resultFilters: \'phraseMatch\',
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName(\'dccontent\');
		if(hover[0] != null){
			if (ex) {
				var h = \'1000px\';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = \'\';
			}
		}
	}
		
	if('; ?>
SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen<?php echo ' == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function () {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.sendRequest(\'\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'click\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'click\');
		});
		
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'dblclick\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'dblclick\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'focus\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mouseup\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mouseup\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mousedown\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mousedown\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'blur\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'blur\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = false;
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['tile_roof_c']['name']; ?>
<?php echo '");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputImage.ancestor().on(\'click\', function () {
		if (SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.blur();
		} else {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'query\', function () {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.set(\'value\', \'\');
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'visibleChange\', function (e) {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'select\', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
'; ?>

<?php endif; ?>
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="CASE_SITE" dtbc-data="case_site_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'CASE_SITE','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="case_site_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['case_site_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_site_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_site_c']['value']); ?>
<?php endif; ?>  
<input type='text' name='<?php echo $this->_tpl_vars['fields']['case_site_c']['name']; ?>
' 
id='<?php echo $this->_tpl_vars['fields']['case_site_c']['name']; ?>
' size='30' 
maxlength='255' 
value='<?php echo $this->_tpl_vars['value']; ?>
' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="WORKPLAN_REMARKS" dtbc-data="workplan_remarks_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'WORKPLAN_REMARKS','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="workplan_remarks_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (empty ( $this->_tpl_vars['fields']['workplan_remarks_c']['value'] )): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['workplan_remarks_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['workplan_remarks_c']['value']); ?>
<?php endif; ?>
<textarea  id='<?php echo $this->_tpl_vars['fields']['workplan_remarks_c']['name']; ?>
' name='<?php echo $this->_tpl_vars['fields']['workplan_remarks_c']['name']; ?>
'
rows="4"
cols="20"
title='' tabindex="" 
 ><?php echo $this->_tpl_vars['value']; ?>
</textarea>
<?php echo ''; ?>

</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="TIME_OF_VISIT" dtbc-data="time_of_visit_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'TIME_OF_VISIT','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="datetimecombo" field="time_of_visit_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<table border="0" cellpadding="0" cellspacing="0" class="dateTime">
<tr valign="middle">
<td nowrap>
<input autocomplete="off" type="text" id="<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_date" class="datetimecombo_date" value="<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['fields']['time_of_visit_c']['name']]['value']; ?>
" size="11" maxlength="10" title='' tabindex="" onblur="combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
.update();" onchange="combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
.update(); "    >
<?php ob_start(); ?>alt="<?php echo $this->_tpl_vars['APP']['LBL_ENTER_DATE']; ?>
" style="position:relative; top:6px" border="0" id="<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_trigger"<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('other_attributes', ob_get_contents());ob_end_clean(); ?>
<?php echo smarty_function_sugar_getimage(array('name' => 'jscalendar','ext' => ".gif",'other_attributes' => ($this->_tpl_vars['other_attributes'])), $this);?>
&nbsp;
</td>
<td nowrap>
<div id="<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_time_section" class="datetimecombo_time_section"></div>
</td>
</tr>
</table>
<input type="hidden" class="DateTimeCombo" id="<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
" name="<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['fields']['time_of_visit_c']['name']]['value']; ?>
">
<script type="text/javascript" src="<?php echo smarty_function_sugar_getjspath(array('file' => "include/SugarFields/Fields/Datetimecombo/Datetimecombo.js"), $this);?>
"></script>
<script type="text/javascript">
var combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
 = new Datetimecombo("<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['fields']['time_of_visit_c']['name']]['value']; ?>
", "<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
", "<?php echo $this->_tpl_vars['TIME_FORMAT']; ?>
", "", '', false, true);
//Render the remaining widget fields
text = combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
.html('');
document.getElementById('<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_time_section').innerHTML = text;

//Call eval on the update function to handle updates to calendar picker object
eval(combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
.jsscript(''));

//bug 47718: this causes too many addToValidates to be called, resulting in the error messages being displayed multiple times
//    removing it here to mirror the Datetime SugarField, where the validation is not added at this level
//addToValidate('<?php echo $this->_tpl_vars['form_name']; ?>
',"<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_date",'date',false,"<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
");
addToValidateBinaryDependency('<?php echo $this->_tpl_vars['form_name']; ?>
',"<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_hours", 'alpha', false, "<?php echo $this->_tpl_vars['APP']['ERR_MISSING_REQUIRED_FIELDS']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_HOURS']; ?>
" ,"<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_date");
addToValidateBinaryDependency('<?php echo $this->_tpl_vars['form_name']; ?>
', "<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_minutes", 'alpha', false, "<?php echo $this->_tpl_vars['APP']['ERR_MISSING_REQUIRED_FIELDS']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_MINUTES']; ?>
" ,"<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_date");
addToValidateBinaryDependency('<?php echo $this->_tpl_vars['form_name']; ?>
', "<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_meridiem", 'alpha', false, "<?php echo $this->_tpl_vars['APP']['ERR_MISSING_REQUIRED_FIELDS']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_MERIDIEM']; ?>
","<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_date");

YAHOO.util.Event.onDOMReady(function()
{

	Calendar.setup ({
	onClose : update_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
,
	inputField : "<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_date",
    form : "EditView",
	ifFormat : "<?php echo $this->_tpl_vars['CALENDAR_FORMAT']; ?>
",
	daFormat : "<?php echo $this->_tpl_vars['CALENDAR_FORMAT']; ?>
",
	button : "<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
_trigger",
	singleClick : true,
	step : 1,
	weekNumbers: false,
        startWeekday: <?php echo ((is_array($_tmp=@$this->_tpl_vars['CALENDAR_FDOW'])) ? $this->_run_mod_handler('default', true, $_tmp, '0') : smarty_modifier_default($_tmp, '0')); ?>
,
	comboObject: combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>

	});

	//Call update for first time to round hours and minute values
	combo_<?php echo $this->_tpl_vars['fields']['time_of_visit_c']['name']; ?>
.update(false);

}); 
</script>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="SITE_CONTACT_NAME" dtbc-data="site_contact_name_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_NAME','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="site_contact_name_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['site_contact_name_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['site_contact_name_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['site_contact_name_c']['value']); ?>
<?php endif; ?>  
<input type='text' name='<?php echo $this->_tpl_vars['fields']['site_contact_name_c']['name']; ?>
' 
id='<?php echo $this->_tpl_vars['fields']['site_contact_name_c']['name']; ?>
' size='30' 
maxlength='100' 
value='<?php echo $this->_tpl_vars['value']; ?>
' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="CASE_COUNTRY" dtbc-data="case_country_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'CASE_COUNTRY','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="case_country_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! isset ( $this->_tpl_vars['config']['enable_autocomplete'] ) || $this->_tpl_vars['config']['enable_autocomplete'] == false): ?>
<select name="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
" 
title=''       
>
<?php if (isset ( $this->_tpl_vars['fields']['case_country_c']['value'] ) && $this->_tpl_vars['fields']['case_country_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_country_c']['options'],'selected' => $this->_tpl_vars['fields']['case_country_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_country_c']['options'],'selected' => $this->_tpl_vars['fields']['case_country_c']['default']), $this);?>

<?php endif; ?>
</select>
<?php else: ?>
<?php $this->assign('field_options', $this->_tpl_vars['fields']['case_country_c']['options']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['case_country_c']['value']; ?>
<?php $this->_smarty_vars['capture']['field_val'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('field_val', $this->_smarty_vars['capture']['field_val']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php $this->_smarty_vars['capture']['ac_key'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('ac_key', $this->_smarty_vars['capture']['ac_key']); ?>
<select style='display:none' name="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
" 
title=''          
>
<?php if (isset ( $this->_tpl_vars['fields']['case_country_c']['value'] ) && $this->_tpl_vars['fields']['case_country_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_country_c']['options'],'selected' => $this->_tpl_vars['fields']['case_country_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_country_c']['options'],'selected' => $this->_tpl_vars['fields']['case_country_c']['default']), $this);?>

<?php endif; ?>
</select>
<input
id="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-input"
name="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-input"
size="30"
value="<?php echo ((is_array($_tmp=$this->_tpl_vars['field_val'])) ? $this->_run_mod_handler('lookup', true, $_tmp, $this->_tpl_vars['field_options']) : smarty_modifier_lookup($_tmp, $this->_tpl_vars['field_options'])); ?>
"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-down.png"), $this);?>
" id="<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-image"></button><button type="button"
id="btn-clear-<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-input', '<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
');sync_<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
()"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-clear.png"), $this);?>
"></button>
</span>
<?php echo '
<script>
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo ' = [];
	'; ?>


			<?php echo '
		(function (){
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php echo '");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:\'\'};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use(\'datasource\', \'datasource-jsonschema\',function (Y) {
				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value==\'\' && selectElem.options[i].innerHTML==\'\'))
				    			ret.push({\'key\':selectElem.options[i].value,\'text\':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		'; ?>

	
	<?php echo '
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	'; ?>

			
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputNode = Y.one('#<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-input');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputImage = Y.one('#<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-image');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputHidden = Y.one('#<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
');
	
			<?php echo '
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php echo '");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'change\');
			}

			//global variable 
			sync_'; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php echo ' = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php echo '");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\');

				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.simulate(\'keyup\');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById(\''; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
-input<?php echo '\'))
						SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("'; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php echo '", syncFromHiddenToWidget);
		'; ?>


		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions = <?php echo count($this->_tpl_vars['field_options']); ?>
;
		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 300) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 200;
			<?php echo '
		}
		'; ?>

		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 3000) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 500;
			<?php echo '
		}
		'; ?>

		
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.optionsVisible = false;
	
	<?php echo '
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		'; ?>

		minQueryLength: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen,
		queryDelay: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay,
		zIndex: 99999,

				
		<?php echo '
		source: SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds,
		
		resultTextLocator: \'text\',
		resultHighlighter: \'phraseMatch\',
		resultFilters: \'phraseMatch\',
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName(\'dccontent\');
		if(hover[0] != null){
			if (ex) {
				var h = \'1000px\';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = \'\';
			}
		}
	}
		
	if('; ?>
SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen<?php echo ' == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function () {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.sendRequest(\'\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'click\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'click\');
		});
		
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'dblclick\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'dblclick\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'focus\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mouseup\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mouseup\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mousedown\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mousedown\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'blur\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'blur\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = false;
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_country_c']['name']; ?>
<?php echo '");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputImage.ancestor().on(\'click\', function () {
		if (SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.blur();
		} else {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'query\', function () {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.set(\'value\', \'\');
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'visibleChange\', function (e) {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'select\', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
'; ?>

<?php endif; ?>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="SITE_CONTACT_EMAIL" dtbc-data="site_contact_email_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_EMAIL','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="site_contact_email_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['site_contact_email_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['site_contact_email_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['site_contact_email_c']['value']); ?>
<?php endif; ?>  
<input type='text' name='<?php echo $this->_tpl_vars['fields']['site_contact_email_c']['name']; ?>
' 
id='<?php echo $this->_tpl_vars['fields']['site_contact_email_c']['name']; ?>
' size='30' 
maxlength='50' 
value='<?php echo $this->_tpl_vars['value']; ?>
' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CASE_STATE" dtbc-data="case_state_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_STATE','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="case_state_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! isset ( $this->_tpl_vars['config']['enable_autocomplete'] ) || $this->_tpl_vars['config']['enable_autocomplete'] == false): ?>
<select name="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
" 
title=''       
>
<?php if (isset ( $this->_tpl_vars['fields']['case_state_c']['value'] ) && $this->_tpl_vars['fields']['case_state_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_state_c']['options'],'selected' => $this->_tpl_vars['fields']['case_state_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_state_c']['options'],'selected' => $this->_tpl_vars['fields']['case_state_c']['default']), $this);?>

<?php endif; ?>
</select>
<?php else: ?>
<?php $this->assign('field_options', $this->_tpl_vars['fields']['case_state_c']['options']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['case_state_c']['value']; ?>
<?php $this->_smarty_vars['capture']['field_val'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('field_val', $this->_smarty_vars['capture']['field_val']); ?>
<?php ob_start(); ?><?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php $this->_smarty_vars['capture']['ac_key'] = ob_get_contents(); ob_end_clean(); ?>
<?php $this->assign('ac_key', $this->_smarty_vars['capture']['ac_key']); ?>
<select style='display:none' name="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
" 
id="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
" 
title=''          
>
<?php if (isset ( $this->_tpl_vars['fields']['case_state_c']['value'] ) && $this->_tpl_vars['fields']['case_state_c']['value'] != ''): ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_state_c']['options'],'selected' => $this->_tpl_vars['fields']['case_state_c']['value']), $this);?>

<?php else: ?>
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['fields']['case_state_c']['options'],'selected' => $this->_tpl_vars['fields']['case_state_c']['default']), $this);?>

<?php endif; ?>
</select>
<input
id="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-input"
name="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-input"
size="30"
value="<?php echo ((is_array($_tmp=$this->_tpl_vars['field_val'])) ? $this->_run_mod_handler('lookup', true, $_tmp, $this->_tpl_vars['field_options']) : smarty_modifier_lookup($_tmp, $this->_tpl_vars['field_options'])); ?>
"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-down.png"), $this);?>
" id="<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-image"></button><button type="button"
id="btn-clear-<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-input', '<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
');sync_<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
()"><img src="<?php echo smarty_function_sugar_getimagepath(array('file' => "id-ff-clear.png"), $this);?>
"></button>
</span>
<?php echo '
<script>
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo ' = [];
	'; ?>


			<?php echo '
		(function (){
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php echo '");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:\'\'};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use(\'datasource\', \'datasource-jsonschema\',function (Y) {
				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value==\'\' && selectElem.options[i].innerHTML==\'\'))
				    			ret.push({\'key\':selectElem.options[i].value,\'text\':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		'; ?>

	
	<?php echo '
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	'; ?>

			
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputNode = Y.one('#<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-input');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputImage = Y.one('#<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-image');
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.inputHidden = Y.one('#<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
');
	
			<?php echo '
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php echo '");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'change\');
			}

			//global variable 
			sync_'; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php echo ' = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php echo '");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\');

				SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.simulate(\'keyup\');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById(\''; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
-input<?php echo '\'))
						SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("'; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php echo '", syncFromHiddenToWidget);
		'; ?>


		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 0;
		SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions = <?php echo count($this->_tpl_vars['field_options']); ?>
;
		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 300) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 200;
			<?php echo '
		}
		'; ?>

		if(SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.numOptions >= 3000) <?php echo '{
			'; ?>

			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen = 1;
			SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay = 500;
			<?php echo '
		}
		'; ?>

		
	SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.optionsVisible = false;
	
	<?php echo '
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		'; ?>

		minQueryLength: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen,
		queryDelay: SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.queryDelay,
		zIndex: 99999,

				
		<?php echo '
		source: SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.ds,
		
		resultTextLocator: \'text\',
		resultHighlighter: \'phraseMatch\',
		resultFilters: \'phraseMatch\',
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName(\'dccontent\');
		if(hover[0] != null){
			if (ex) {
				var h = \'1000px\';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = \'\';
			}
		}
	}
		
	if('; ?>
SUGAR.AutoComplete.<?php echo $this->_tpl_vars['ac_key']; ?>
.minQLen<?php echo ' == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function () {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.sendRequest(\'\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'click\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'click\');
		});
		
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'dblclick\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'dblclick\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'focus\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'focus\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mouseup\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mouseup\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'mousedown\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'mousedown\');
		});

		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.on(\'blur\', function(e) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.simulate(\'blur\');
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible = false;
			var selectElem = document.getElementById("'; ?>
<?php echo $this->_tpl_vars['fields']['case_state_c']['name']; ?>
<?php echo '");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.get(\'value\'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.set(\'value\', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputImage.ancestor().on(\'click\', function () {
		if (SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.optionsVisible) {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.blur();
		} else {
			SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'query\', function () {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputHidden.set(\'value\', \'\');
	});

	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'visibleChange\', function (e) {
		SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.'; ?>
<?php echo $this->_tpl_vars['ac_key']; ?>
<?php echo '.inputNode.ac.on(\'select\', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
'; ?>

<?php endif; ?>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="SITE_CONTACT_PHONE" dtbc-data="site_contact_phone_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_PHONE','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="site_contact_phone_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['site_contact_phone_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['site_contact_phone_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['site_contact_phone_c']['value']); ?>
<?php endif; ?>  
<input type='text' name='<?php echo $this->_tpl_vars['fields']['site_contact_phone_c']['name']; ?>
' 
id='<?php echo $this->_tpl_vars['fields']['site_contact_phone_c']['name']; ?>
' size='30' 
maxlength='30' 
value='<?php echo $this->_tpl_vars['value']; ?>
' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CASE_ADDRESS" dtbc-data="case_address_c_label">

<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'CASE_ADDRESS','module' => 'Cases'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="case_address_c"  >
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (empty ( $this->_tpl_vars['fields']['case_address_c']['value'] )): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_address_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['case_address_c']['value']); ?>
<?php endif; ?>
<textarea  id='<?php echo $this->_tpl_vars['fields']['case_address_c']['name']; ?>
' name='<?php echo $this->_tpl_vars['fields']['case_address_c']['name']; ?>
'
rows="4"
cols="20"
title='' tabindex="" 
 ><?php echo $this->_tpl_vars['value']; ?>
</textarea>
<?php echo ''; ?>

</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>
</div>
</div>

<script language="javascript">
    var _form_id = '<?php echo $this->_tpl_vars['form_id']; ?>
';
    <?php echo '
    SUGAR.util.doWhen(function(){
        _form_id = (_form_id == \'\') ? \'EditView\' : _form_id;
        return document.getElementById(_form_id) != null;
    }, SUGAR.themes.actionMenu);
    '; ?>

</script>
<?php $this->assign('place', '_FOOTER'); ?> <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
<?php if ($this->_tpl_vars['bean']->aclAccess('save')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_SAVE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_SAVE_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('EditView'); <?php if ($this->_tpl_vars['isDuplicate']): ?>_form.return_id.value=''; <?php endif; ?>_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_SAVE_BUTTON_LABEL']; ?>
" id="SAVE"><?php endif; ?> 
<?php if (! empty ( $_REQUEST['return_action'] ) && ( $_REQUEST['return_action'] == 'DetailView' && ! empty ( $_REQUEST['return_id'] ) )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo ((is_array($_tmp=$_REQUEST['return_id'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'); return false;" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" type="button" id="CANCEL"> <?php elseif (! empty ( $_REQUEST['return_action'] ) && ( $_REQUEST['return_action'] == 'DetailView' && ! empty ( $this->_tpl_vars['fields']['id']['value'] ) )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php elseif (! empty ( $_REQUEST['return_action'] ) && ( $_REQUEST['return_action'] == 'DetailView' && empty ( $this->_tpl_vars['fields']['id']['value'] ) ) && empty ( $_REQUEST['return_id'] )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php elseif (! empty ( $_REQUEST['return_action'] ) && ! empty ( $_REQUEST['return_module'] )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=<?php echo $_REQUEST['return_action']; ?>
&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php elseif (empty ( $_REQUEST['return_action'] ) || empty ( $_REQUEST['return_id'] ) && ! empty ( $this->_tpl_vars['fields']['id']['value'] )): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=Cases'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php else: ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_KEY']; ?>
" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=<?php echo ((is_array($_tmp=$_REQUEST['return_module'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
&record=<?php echo ((is_array($_tmp=$_REQUEST['return_id'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'); return false;" type="button" name="button" value="<?php echo $this->_tpl_vars['APP']['LBL_CANCEL_BUTTON_LABEL']; ?>
" id="CANCEL"> <?php endif; ?>
<?php if ($this->_tpl_vars['showVCRControl']): ?>
<button type="button" id="save_and_continue" class="button saveAndContinue" title="<?php echo $this->_tpl_vars['app_strings']['LBL_SAVE_AND_CONTINUE']; ?>
" onClick="SUGAR.saveAndContinue(this);">
<?php echo $this->_tpl_vars['APP']['LBL_SAVE_AND_CONTINUE']; ?>

</button>
<?php endif; ?>
<?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=Cases", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?>
</div>
</form>
<?php echo $this->_tpl_vars['set_focus_block']; ?>

<script>SUGAR.util.doWhen("document.getElementById('EditView') != null",
        function(){SUGAR.util.buildAccessKeyLabels();});
</script>
<script type="text/javascript">
YAHOO.util.Event.onContentReady("EditView",
    function () { initEditView(document.forms.EditView) });
//window.setTimeout(, 100);
window.onbeforeunload = function () { return onUnloadEditView(); };
// bug 55468 -- IE is too aggressive with onUnload event
if ($.browser.msie) {
$(document).ready(function() {
    $(".collapseLink,.expandLink").click(function (e) { e.preventDefault(); });
  });
}
</script>
<?php echo '
<script type="text/javascript">

    var selectTab = function(tab) {
        $(\'#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER\').hide();
        $(\'#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER\').eq(tab).show().addClass(\'active\').addClass(\'in\');
    };

    var selectTabOnError = function(tab) {
        selectTab(tab);
        $(\'#EditView_tabs ul.nav.nav-tabs li\').removeClass(\'active\');
        $(\'#EditView_tabs ul.nav.nav-tabs li a\').css(\'color\', \'\');

        $(\'#EditView_tabs ul.nav.nav-tabs li\').eq(tab).find(\'a\').first().css(\'color\', \'red\');
        $(\'#EditView_tabs ul.nav.nav-tabs li\').eq(tab).addClass(\'active\');

    };

    var selectTabOnErrorInputHandle = function(inputHandle) {
        var tab = $(inputHandle).closest(\'.tab-pane-NOBOOTSTRAPTOGGLER\').attr(\'id\').match(/^detailpanel_(.*)$/)[1];
        selectTabOnError(tab);
    };


    $(function(){
        $(\'#EditView_tabs ul.nav.nav-tabs li > a[data-toggle="tab"]\').click(function(e){
            if(typeof $(this).parent().find(\'a\').first().attr(\'id\') != \'undefined\') {
                var tab = parseInt($(this).parent().find(\'a\').first().attr(\'id\').match(/^tab(.)*$/)[1]);
                selectTab(tab);
            }
        });

        $(\'a[data-toggle="collapse-edit"]\').click(function(e){
            if($(this).hasClass(\'collapsed\')) {
              // Expand panel
                // Change style of .panel-header
                $(this).removeClass(\'collapsed\');
                // Expand .panel-body
                $(this).parents(\'.panel\').find(\'.panel-body\').removeClass(\'in\').addClass(\'in\');
            } else {
              // Collapse panel
                // Change style of .panel-header
                $(this).addClass(\'collapsed\');
                // Collapse .panel-body
                $(this).parents(\'.panel\').find(\'.panel-body\').removeClass(\'in\').removeClass(\'in\');
            }
        });
    });

    </script>
'; ?>
<?php echo '
<script type="text/javascript">
addForm(\'EditView\');addToValidate(\'EditView\', \'name\', \'name\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SUBJECT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_entered_date\', \'date\', false,\'Date Created\' );
addToValidate(\'EditView\', \'date_modified_date\', \'date\', false,\'Date Modified\' );
addToValidate(\'EditView\', \'modified_user_id\', \'assigned_user_name\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_MODIFIED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'modified_by_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_MODIFIED_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'created_by\', \'assigned_user_name\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CREATED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'created_by_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CREATED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'description\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DESCRIPTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'deleted\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DELETED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'assigned_user_id\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'assigned_user_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_number\', \'int\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'type\', \'enum\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'status\', \'dynamicenum\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'priority\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRIORITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'resolution\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RESOLUTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'work_log\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_WORK_LOG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'suggestion_box\', \'readonly\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SUGGESTION_BOX','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'account_name\', \'relate\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCOUNT_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'account_id\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCOUNT_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'state\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_STATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_attachments_display\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_ATTACHMENTS_DISPLAY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_update_form\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_UPDATE_FORM','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_created_by_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_CREATED_BY_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_created_by_id\', \'id\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_CREATED_BY_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'update_text\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_UPDATE_TEXT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'internal\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INTERNAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'aop_case_updates_threaded\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_AOP_CASE_UPDATES_THREADED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_name_c\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_rma_unit_shipped_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_RMA_UNIT_SHIPPED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'vendor_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_VENDOR_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'optimizer_inverter_pn_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_OPTIMIZER_INVERTER_PN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'reason_for_not_in_warranty_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REASON_FOR_NOT_IN_WARRANTY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_origin_c\', \'enum\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_ORIGIN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'last_telemetry_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LAST_TELEMETRY_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_state_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_STATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'installer_s_grade_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'INSTALLER_S_GRADE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DOA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_contact_person_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_PERSON_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_sub_status_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CASE_SUB_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'receiving_tracking_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RECEIVING_TRACKING_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'emailtocase_notificationsent_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_EMAILTOCASE_NOTIFICATIONSENT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_warranty_still_valid_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_WARRANTY_STILL_VALID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'county_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_COUNTY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_contant_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RMA_CONTANT_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'support_comments_cc_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SUPPORT_COMMENTS_CC','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'visit_order_c[]\', \'multienum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'VISIT_ORDER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'inverter_model_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'INVERTER_MODEL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tesla_expiration_date_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TESLA_EXPIRATION_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'waiting_for_tier_1_timestamp_c_date\', \'date\', false,\'Waiting for Tier 1 timestamp\' );
addToValidate(\'EditView\', \'sub_symptom_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SUB_SYMPTOM','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'acase_id_c\', \'id\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PARENT_CASE_ACASE_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'transferred_to_finance_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TRANSFERRED_TO_FINANCE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'cpu_firmware_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CPU_FIRMWARE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'link_to_monitoring_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINK_TO_MONITORING','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'severity_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SEVERITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'any_battery_serial_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ANY_BATTERY_SERIAL_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'pr_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PR_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'comments_for_sunpower_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'COMMENTS_FOR_SUNPOWER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'any_inverter_serial_req_for_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ANY_INVERTER_SERIAL_REQ_FOR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rca_need_imm_return_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RCA_NEED_IMM_RETURN_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_reason_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_REASON','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'state_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_STATE_C','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'send_email_close_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SEND_EMAIL_CLOSE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_approved_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_APPROVED_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'ship_to_name_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SHIP_TO_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_site_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CASE_SITE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'auto_assigned_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_AUTO_ASSIGNED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'cause_for_not_approved_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CAUSE_FOR_NOT_APPROVED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tesla_case_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TESLA_CASE_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_rma_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOA_RMA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'change_details_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CHANGE_DETAILS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dtbc_notify_customer_date_date\', \'date\', false,\'Notify Customer Date\' );
addToValidate(\'EditView\', \'return_asked_by_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RETURN_ASKED_BY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_valid_for_service_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_VALID_FOR_SERVICE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'address_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ADDRESS_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'distributer_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DISTRIBUTER_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_part_description_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_PART_DESCRIPTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'estimated_pay_date_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ESTIMATED_PAY_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_early_mortality_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOA_EARLY_MORTALITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'operating_period_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_OPERATING_PERIOD','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'escalated_to_t2_timestamp_c_date\', \'date\', false,\'escalated to tier 2 timestamp\' );
addToValidate(\'EditView\', \'site_address_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_ADDRESS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_sunpower_partner_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DATE_SUNPOWER_PARTNER_CONTACTED_BY_SEDG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'new_internal_comment_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_NEW_INTERNAL_COMMENT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'lg_tesla_for_email_template_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LG_TESLA_FOR_EMAIL_TEMPLATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_1_germany_timestamp_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER_1_GERMANY_TIMESTAMP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'activation_code_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ACTIVATION_CODE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'return_label_awb_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RETURN_LABEL_AWB','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'alert_override_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ALERT_OVERRIDE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'inverter_s_size_s_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'INVERTER_S_SIZE_S','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'link_to_ups_2_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINK_TO_UPS_2','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'shipping_service_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SHIPPING_SERVICE_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'vip_flag_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_VIP_FLAG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'uzc1_usa_zip_codes_cases_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_UZC1_USA_ZIP_CODES_CASES_FROM_UZC1_USA_ZIP_CODES_TITLE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_3_flag_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER_3_FLAG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tracking_number_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TRACKING_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'distributor_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DISTRIBUTOR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'reason_for_not_eligible_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REASON_FOR_NOT_ELIGIBLE_CERTIFICATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tracking_number_link_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TRACKING_NUMBER_LINK','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'certification_level_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CERTIFICATION_LEVEL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'inverter_type_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INVERTER_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tile_roof_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TILE_ROOF','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'if_eligible_was_true_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'IF_ELIGIBLE_WAS_TRUE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_phone_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_PHONE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'web_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_WEB_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'web_name_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_WEB_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'field_engineer_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIELD_ENGINEER_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'origin_email_address_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIELD_ORIGIN_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'is_email_to_case\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIELD_IS_EMAIL_TO_CASE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'last_sub_status_change\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIELD_LAST_SUBSTATUSCHANGE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'taken_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TAKEN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_record_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_RECORD_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_status_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'region_for_email_template_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_REGION_FOR_EMAIL_TEMPLATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'certification_data_updated_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CERTIFICATION_DATA_UPDATED__C','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_2_assignee_email_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TIER_2_ASSIGNEE_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_is_monitored_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_IS_MONITORED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'service_level_flag_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SERVICE_LEVEL_FLAG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'inverter_id_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INVERTER_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'alliance_region_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ALLIANCE_REGION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'warranty_end_date_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'WARRANTY_END_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_country_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_COUNTRY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'field_engineer_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'FIELD_ENGINEER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_type_new_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOA_TYPE_NEW','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'lg_battery_min_soe_new_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LG_BATTERY_MIN_SOE_NEW','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'waiting_customer_check_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_WAITING_CUSTOMER_CHECK','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'name_of_person_who_approves_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'NAME_OF_PERSON_WHO_APPROVES_RMA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'escalated_to_tesla_timestamp_c_date\', \'date\', false,\'escalated to tesla timestamp\' );
addToValidate(\'EditView\', \'fault_category_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FAULT_CATEGORY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'communication_board_was_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'COMMUNICATION_BOARD_WAS_REPLACED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'change_checkbox_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CHANGE_CHECKBOX','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'panel_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PANEL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_time_closed_c_date\', \'date\', false,\'date time closed\' );
addToValidate(\'EditView\', \'paid_amount_c\', \'decimal\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PAID_AMOUNT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'new_email_comment_for_wf_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_NEW_EMAIL_COMMENT_FOR_WF','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DOA_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'customer_complaint_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CUSTOMER_COMPLAINT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rca_and_need_immediate_ret_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RCA_AND_NEED_IMMEDIATE_RET','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'mass_update_admin_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'MASS_UPDATE_ADMIN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_id_c\', \'id\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_NAME_CONTACT_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'account_contact_s_comments_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCOUNT_CONTACT_S_COMMENTS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dsp2_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DSP2','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'optimizer_inverter_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_OPTIMIZER_INVERTER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_fault_description_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_FAULT_DESCRIPTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'solaredge_code_for_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SOLAREDGE_CODE_FOR_CERTIFICATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'refurbish_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REFURBISH','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_contact_person_phone_c\', \'phone\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_PERSON_PHONE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'email_serial_numbers\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_POINTS_TOTAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_email_template_table_c\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOA_TEMPLATE_TOTAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_street_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_STREET','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'points_balance_on_case_open_c\', \'decimal\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'POINTS_BALANCE_ON_CASE_OPEN_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'first_telemetry_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'FIRST_TELEMETRY_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'internal_comments_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INTERNAL_COMMENTS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tracking_number_fa_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TRACKING_NUMBER_FA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'urgency_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_URGENCY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'lg_battery_min_soe_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LG_BATTERY_MIN_SOE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_taken_from_tier_1_us_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DATE_TAKEN_FROM_TIER_1_US','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_type_c\', \'enum\', true,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'notes_rma_team_us_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'NOTES_RMA_TEAM_US','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'cases_cases_2_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASES_CASES_2_FROM_CASES_L_TITLE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'aop_case_updates_threaded_custom\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_AOP_CASE_UPDATES_THREADED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'need_fa_immediate_return_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_NEED_FA_IMMEDIATE_RETURN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'chat_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CHAT_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_reason_for_not_in_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_REASON_FOR_NOT_IN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'immediate_escalation_to_batt_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_IMMEDIATE_ESCALATION_TO_BATT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'account_support_comments_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCOUNT_SUPPORT_COMMENTS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_distributor_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_DISTRIBUTOR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_software_version_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_SOFTWARE_VERSION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'return_label_created_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RETURN_LABEL_CREATED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'resolution1_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RESOLUTION1','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'compensation_po_n_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'COMPENSATION_PO_N','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_shipment_date_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_SHIPMENT_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'fault_sub_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'FAULT_SUB_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_zip_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_ZIP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'eligible_for_compensation_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ELIGIBLE_FOR_COMPENSATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_in_erp_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_IN_ERP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_installer_id_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_INSTALLER_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'paid_by_ltd_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PAID_BY_LTD','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'current_software_version_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CURRENT_SOFTWARE_VERSION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'serial_numbers_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SERIAL_NUMBERS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'roof_type_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ROOF_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'latest_validation_error_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LATEST_VALIDATION_ERROR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'certification_expiry_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CERTIFICATION_EXPIRY_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'timestamp_of_approval_c_date\', \'date\', false,\'Timestamp of Approval\' );
addToValidate(\'EditView\', \'internal_comment_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INTERNAL_COMMENT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'reason_for_not_eligible_cert_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_REASON_FOR_NOT_ELIGIBLE_CERT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'workplan_remarks_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'WORKPLAN_REMARKS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'parent_case_c\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PARENT_CASE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'cases_cases_3_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASES_CASES_3_FROM_CASES_L_TITLE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'symptom_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SYMPTOM','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'whole_shipping_address_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_WHOLE_SHIPPING_ADDRESS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_2_assigneee_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER_2_ASSIGNEEE_EMAIL_ADDRESS_FOR_WF','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'region_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_REGION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_1_germany_start_c_date\', \'date\', false,\'Tier 1 Germany start timestamp\' );
addToValidate(\'EditView\', \'site_contact_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_time_battery_escalation_c_date\', \'date\', false,\'Date Time Battery escalation canceled\' );
addToValidate(\'EditView\', \'rma_state_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_STATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'product_group_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PRODUCT_GROUP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_2_region_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER_2_REGION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'warrant_extension_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_WARRANT_EXTENSION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'doa_creation_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOA_CREATION_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'ack_service_instruction_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ACK_SERVICE_INSTRUCTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'certified_installer_tesla_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CERTIFIED_INSTALLER_TESLA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'listview_envelope\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LISTVIEW_ENVELOPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'listview_security_group\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LISTVIEW_GROUP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'se_battery_support_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SE_BATTERY_SUPPORT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'receiving_tracking_link_c\', \'html\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RECEIVING_TRACKING_LINK','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_buyer_id_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_BUYER_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_pending_approval_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_PENDING_APPROVAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'inverter_serial_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INVERTER_SERIAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_2_escalation_completed_c_date\', \'date\', false,\'Tier 2 escalation completed\' );
addToValidate(\'EditView\', \'entered_tier_2_us_c_date\', \'date\', false,\'Entered to tier 2 US\' );
addToValidate(\'EditView\', \'sub_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SUB_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'first_taken_1_us_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIRST_TAKEN_1_US','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'special_service_instruction_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SPECIAL_SERVICE_INSTRUCTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'receiving_forwarder_url_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RECEIVING_FORWARDER_URL_C','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_distributor_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_DISTRIBUTOR_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'field_service_kit_holder_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIELD_SERVICE_KIT_HOLDER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'escalation_to_tesla_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ESCALATION_TO_TESLA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'monitored_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'MONITORED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'time_of_visit_c_date\', \'date\', false,\'Time of Visit\' );
addToValidate(\'EditView\', \'first_taken_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIRST_TAKEN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_taken_from_tier_2_us_c_date\', \'date\', false,\'Date Taken from Tier 2 US\' );
addToValidate(\'EditView\', \'traceability_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TRACEABILITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'immediate_escalation_to_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_IMMEDIATE_ESCALATION_TO','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tracking_number_f_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_TRACKING_NUMBER_F','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'handling_time_eu_assignee_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'HANDLING_TIME_EU_ASSIGNEE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_contact_phone_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_PHONE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'axiplus_indicator_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_AXIPLUS_INDICATOR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_contact_person_name_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_PERSON_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'searchform_security_group\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SEARCHFORM_SECURITY_GROUP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dtbc_2days_noti_sent_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_HIDDEN_2DAYS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dtbc_3days_noti_sent_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_HIDDEN_3DAYS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dtbc_5days_noti_sent_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_HIDDEN_5DAYS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dtbc_agent_status_noti_sent_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_HIDDEN_AFTERSAVE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rca_and_need_imm_ret_email_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RCA_AND_NEED_IMM_RET_EMAIL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'link_to_ups_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINK_TO_UPS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'cases_cases_1_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASES_CASES_1_FROM_CASES_L_TITLE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'details_for_fault_sub_type_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DETAILS_FOR_FAULT_SUB_TYPE_OTHER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'video_chat_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'VIDEO_CHAT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'new_case_notification_sent_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_NEW_CASE_NOTIFICATION_SENT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'return_label_pickup_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RETURN_LABEL_PICKUP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'service_instruction_flag_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SERVICE_INSTRUCTION_FLAG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_zip_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CASE_ZIP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'sub_status_updated_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SUB_STATUS_UPDATED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'today_c\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_TODAY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_age_c\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_AGE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'support_team_for_template_c\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_SUPPORT_TEAM','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'last_modified_by_c\', \'function\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_LAST_MODIFIED_BY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_city_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CASE_CITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'solution_statement__c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SOLUTION_STATEMENT_','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'solaredge_service_kit_holder_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SOLAREDGE_SERVICE_KIT_HOLDER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'error_event_code_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ERROR_EVENT_CODE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_installation_date_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_INSTALLATION_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rca_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RCA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_country_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CASE_COUNTRY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'sunpower_partner_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SUNPOWER_PARTNER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'in_process_timestamp_eu_c_date\', \'date\', false,\'In Process timestamp EU\' );
addToValidate(\'EditView\', \'forwarder_url_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FORWARDER_URL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_city_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_CITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'as_part_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'AS_PART_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'in_process_timestamp_eu_step_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'IN_PROCESS_TIMESTAMP_EU_STEP_1','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'digital_board_was_replaced_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DIGITAL_BOARD_WAS_REPLACED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'alternative_ibolt_status_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ALTERNATIVE_IBOLT_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'valid_for_service_indication_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_VALID_FOR_SERVICE_INDICATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'thread_id_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_THREAD_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_lines_in_case_c\', \'int\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RMA_LINES_IN_CASE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'link_to_tesla_rma_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINK_TO_TESLA_RMA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'jira_fa_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'JIRA_FA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tesla_lg_escalation_timestam_c_date\', \'date\', false,\'Tesla/LG escalation timestamp\' );
addToValidate(\'EditView\', \'usa_county_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_USA_COUNTY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tracking_number_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TRACKING_NUMBER_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_2_assignee_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER_2_ASSIGNEE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'error_code_number_c\', \'decimal\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ERROR_CODE_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_country_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_COUNTRY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'solarcity_flag_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SOLARCITY_FLAG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'reason_for_additional_rma_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REASON_FOR_ADDITIONAL_RMA','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'replenishment_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REPLENISHMENT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'installation_rules_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'INSTALLATION_RULES','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'power_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_POWER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'receiving_forwarder_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RECEIVING_FORWARDER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'part_description_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PART_DESCRIPTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'do_not_send_survey_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DO_NOT_SEND_SURVEY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'peak_power_kw_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PEAK_POWER_KW','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_zip_postal_code_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_ZIP_POSTAL_CODE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'taken_from_tier_2_us_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TAKEN_FROM_TIER_2_US','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'customer_case_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CUSTOMER_CASE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'actions_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACTIONS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'alliance_case_status_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ALLIANCE_CASE_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'case_address_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CASE_ADDRESS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'state_zip_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_STATE_ZIP','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'customer_should_go_to_the_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CUSTOMER_SHOULD_GO_TO_THE_SITE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'shipping_tracking_number_2_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SHIPPING_TRACKING_NUMBER_2','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_total_time_c\', \'decimal\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_TOTAL_TIME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'field_engineer_name_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'FIELD_ENGINEER_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'ibolt_status_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_IBOLT_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'canadian_province_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CANADIAN_PROVINCE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'taken_from_tier_1_us_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TAKEN_FROM_TIER_1_US','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'need_pre_configuration_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'NEED_PRE_CONFIGURATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'internal_notes_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_INTERNAL_NOTES','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'countif_portia_rs485_c\', \'int\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_COUNTIF_PORTIA_RS485','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_part_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_PART_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'entered_tier_1_us_c_date\', \'date\', false,\'Entered to tier 1 US\' );
addToValidate(\'EditView\', \'compensation_approved_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'COMPENSATION_APPROVED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_1_germany_start_timesta_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER_1_GERMANY_START_TIMESTAMP_STEP_1','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'s1_site_cases_name\', \'relate\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'part_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'PART_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_state_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_STATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_comments_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'RMA_COMMENTS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rca_location_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RCA_LOCATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'get_address_confirmation_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'GET_ADDRESS_CONFIRMATION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'contact_support_comments_c\', \'text\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT_SUPPORT_COMMENTS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'change_indicator_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_CHANGE_INDICATOR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'first_taken_2_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_FIRST_TAKEN_2','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'job_number_us_only_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'JOB_NUMBER_US_ONLY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_ibolt_status_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_IBOLT_STATUS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'action_log_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACTION_LOG','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'waiting_for_tier_2_timestamp_c_date\', \'date\', false,\'Waiting for Tier 2 timestamp\' );
addToValidate(\'EditView\', \'record_type_is_rma_timestamp_c_date\', \'date\', false,\'record type is rma timestamp\' );
addToValidate(\'EditView\', \'place_name_canada_only_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PLACE_NAME_CANADA_ONLY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_yes_no_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RMA_YES_NO','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'service_level_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SERVICE_LEVEL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_circuit_breaker_stat_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_CIRCUIT_BREAKER_STAT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'public_modules_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PUBLIC_MODULES','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'compensation_exceptional_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'COMPENSATION_EXCEPTIONAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'priority_indicator_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRIORITY_INDICATOR','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier_3_escalation_completed_c_date\', \'date\', false,\'Tier 3 escalation completed\' );
addToValidate(\'EditView\', \'send_to_priority_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SEND_TO_PRIORITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'created_by_address_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CREATED_BY_ADDRESS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'shipping_tracking_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SHIPPING_TRACKING_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'date_rma_approval_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DATE_RMA_APPROVAL','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_tesla_authorized_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'BATTERY_TESLA_AUTHORIZED_RESELLER_ID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'send_zach_vm_notification_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SEND_ZACH_VM_NOTIFICATION_MAIL_WF','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'proactive_case_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PROACTIVE_CASE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'battery_end_warranty_date_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_BATTERY_END_WARRANTY_DATE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'dsp1_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'DSP1','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'family_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'FAMILY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'warranty_extention_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'WARRANTY_EXTENTION','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'reason_for_not_approved_list_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REASON_FOR_NOT_APPROVED_LIST','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'tier3_escalation_owner_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIER3_ESCALATION_OWNER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'time_stamp_rma_approved_c\', \'date\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'TIME_STAMP_RMA_APPROVED','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'rma_pr_number_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_RMA_PR_NUMBER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_city_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_CITY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'reason_for_delay_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_REASON_FOR_DELAY','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'grid_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'GRID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'valid_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'VALID','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'guidelines_deviation_reason_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'GUIDELINES_DEVIATION_REASON','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'escalated_to_t3_timestamp_c_date\', \'date\', false,\'escalated to tier 3 timestamp\' );
addToValidate(\'EditView\', \'gen_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'GEN','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'replacement_type_c\', \'enum\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'REPLACEMENT_TYPE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'country_of_contact_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_COUNTRY_OF_CONTACT','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'site_contact_name_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'SITE_CONTACT_NAME','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'faulty_inverter_c\', \'varchar\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'FAULTY_INVERTER','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidate(\'EditView\', \'confirm_contact_address_c\', \'bool\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'CONFIRM_CONTACT_ADDRESS','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\' );
addToValidateBinaryDependency(\'EditView\', \'assigned_user_name\', \'alpha\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ERR_SQS_NO_MATCH_FIELD','module' => 'Cases','for_js' => true), $this);?>
<?php echo ': '; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\', \'assigned_user_id\' );
addToValidateBinaryDependency(\'EditView\', \'s1_site_cases_name\', \'alpha\', false,\''; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'ERR_SQS_NO_MATCH_FIELD','module' => 'Cases','for_js' => true), $this);?>
<?php echo ': '; ?>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_S1_SITE_CASES_FROM_S1_SITE_TITLE','module' => 'Cases','for_js' => true), $this);?>
<?php echo '\', \'s1_site_casess1_site_ida\' );
</script><script language="javascript">if(typeof sqs_objects == \'undefined\'){var sqs_objects = new Array;}sqs_objects[\'EditView_s1_site_cases_name\']={"form":"EditView","method":"query","modules":["S1_Site"],"group":"or","field_list":["name","id"],"populate_list":["s1_site_cases_name","s1_site_casess1_site_ida"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};</script>'; ?>
