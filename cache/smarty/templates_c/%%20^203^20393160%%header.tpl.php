<?php /* Smarty version 2.6.29, created on 2018-03-14 17:55:29
         compiled from themes/SolarEdgeP/tpls/header.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "themes/SolarEdgeP/tpls/_head.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<body onMouseOut="closeMenus();">
<?php echo '
<script>
	/**
	 * JavaScript Get URL Parameter
	 * 
	 * @param String prop The specific URL parameter you want to retreive the value for
	 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
	 */
	function getUrlParams( prop ) {
		var params = {};
		var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( \'?\' ) + 1 ) );
		if (search.includes("ajaxui")) {
			search = search.substring(search.indexOf("?") + 1);
		}
		var definitions = search.split( \'&\' );

		definitions.forEach( function( val, key ) {
			var parts = val.split( \'=\', 2 );
			params[ parts[ 0 ] ] = parts[ 1 ];
		} );

		return ( prop && prop in params ) ? params[ prop ] : "";
	}

	if (window.location.hostname != "localhost" && window.location.hostname != "10.0.10.6" && window.location.hostname != "10.0.10.7") {
		newrelic.setPageViewName(getUrlParams(\'module\') + "/" + getUrlParams(\'action\') + "/" + getUrlParams(\'record\'));
	}		
</script>
'; ?>


<?php echo $this->_tpl_vars['DCSCRIPT']; ?>

<?php if ($this->_tpl_vars['AUTHENTICATED']): ?>
    <div id="ajaxHeader">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "themes/SolarEdgeP/tpls/_headerModuleList.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </div>
<?php endif; ?>
<?php echo '
    <iframe id=\'ajaxUI-history-iframe\' src=\'index.php?entryPoint=getImage&imageName=blank.png\' title=\'empty\'
            style=\'display:none\'></iframe>
<input id=\'ajaxUI-history-field\' type=\'hidden\'>
<script type=\'text/javascript\'>
    if (SUGAR.ajaxUI && !SUGAR.ajaxUI.hist_loaded) {
        YAHOO.util.History.register(\'ajaxUILoc\', "", SUGAR.ajaxUI.go);
        '; ?>
<?php if ($_REQUEST['module'] != 'ModuleBuilder'): ?>        YAHOO.util.History.initialize("ajaxUI-history-field", "ajaxUI-history-iframe");
        <?php endif; ?><?php echo '
    }
</script>
'; ?>

<!-- Start of page content -->
<?php if ($this->_tpl_vars['AUTHENTICATED']): ?>
<div id="bootstrap-container"
     class="<?php if ($this->_tpl_vars['THEME_CONFIG']['display_sidebar'] && $_COOKIE['sidebartoggle'] != 'collapsed'): ?>col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2<?php endif; ?> main bootstrap-container">
    <div id="content" class="content">
        <div id="pagecontent" class=".pagecontent">
<?php endif; ?>