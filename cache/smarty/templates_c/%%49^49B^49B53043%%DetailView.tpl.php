<?php /* Smarty version 2.6.29, created on 2018-03-14 17:55:36
         compiled from custom/include/SugarFields/Fields/Datetime/DetailView.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'sugarvar', 'custom/include/SugarFields/Fields/Datetime/DetailView.tpl', 2, false),array('function', 'sugarvar_connector', 'custom/include/SugarFields/Fields/Datetime/DetailView.tpl', 7, false),)), $this); ?>

{assign var="value" value=<?php echo smarty_function_sugarvar(array('key' => 'value','string' => true), $this);?>
 }

<span class="sugar_field" id="<?php echo smarty_function_sugarvar(array('key' => 'name'), $this);?>
">{$value}</span>
<?php if (! empty ( $this->_tpl_vars['displayParams']['enableConnectors'] )): ?>
{if !empty($value)}
<?php echo smarty_function_sugarvar_connector(array('view' => 'DetailView'), $this);?>

{/if}
<?php endif; ?>