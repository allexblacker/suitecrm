<?php
// created: 2018-03-14 17:55:29
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_LIST_FORM_TITLE' => 'Alternative Part List',
  'LBL_MODULE_NAME' => 'Alternative Part',
  'LBL_MODULE_TITLE' => 'Alternative Part',
  'LBL_HOMEPAGE_TITLE' => 'My Alternative Part',
  'LNK_NEW_RECORD' => 'Create Alternative Part',
  'LNK_LIST' => 'View Alternative Part',
  'LNK_IMPORT_AP1_ALTERNATIVE_PART' => 'Import Alternative Part',
  'LBL_SEARCH_FORM_TITLE' => ' Alternative Part',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_AP1_ALTERNATIVE_PART_SUBPANEL_TITLE' => 'Alternative Part',
  'LBL_NEW_FORM_TITLE' => 'New Alternative Part',
  'Name' => 'Alternative Part Name',
  'Bin' => 'Bin',
  'Priority' => 'Priority',
  'Stock_Level' => 'Stock Level',
  'Warehouse' => 'Warehouse',
  'LBL_DETAILVIEW_PANEL1' => 'Alternative Part Detail',
  'LBL_DETAILVIEW_PANEL2' => 'System Information',
  'LBL_EDITVIEW_PANEL2' => 'Alternative Part Name',
  'LBL_EDITVIEW_PANEL3' => 'System Information',
  'LBL_SERIAL_NUMBER_SN1_SERIAL_NUMBER_ID' => 'serial number (related  ID)',
  'LBL_SERIAL_NUMBER' => 'serial number',
);