<?php
// created: 2018-03-14 17:55:29
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_LIST_FORM_TITLE' => 'Field Service Kit List',
  'LBL_MODULE_NAME' => 'Field Service Kit',
  'LBL_MODULE_TITLE' => 'Field Service Kit',
  'LBL_HOMEPAGE_TITLE' => 'My Field Service Kit',
  'LNK_NEW_RECORD' => 'Create Field Service Kit',
  'LNK_LIST' => 'View Field Service Kit',
  'LNK_IMPORT_FSK1_FIELD_SERVICE_KIT' => 'Import Field Service Kit',
  'LBL_SEARCH_FORM_TITLE' => ' Field Service Kit',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_FSK1_FIELD_SERVICE_KIT_SUBPANEL_TITLE' => 'Field Service Kit',
  'LBL_NEW_FORM_TITLE' => 'New Field Service Kit',
  'QTY' => 'QTY',
  'Shipping_Address' => 'Shipping Address',
  'Shipping_Date' => 'Shipping Date',
  'Tracking_Number' => 'Tracking Number',
  'LBL_DETAILVIEW_PANEL1' => 'Field Service Kit Detail',
  'LBL_DETAILVIEW_PANEL2' => 'System Information',
  'LBL_EDITVIEW_PANEL2' => 'Field Service Kit Detail',
  'LBL_EDITVIEW_PANEL1' => 'System Information',
  'LBL_ACCOUNTS_FSK1_FIELD_SERVICE_KIT_1_FROM_ACCOUNTS_TITLE' => 'Accounts',
  'LBL_CONTACTS_FSK1_FIELD_SERVICE_KIT_1_FROM_CONTACTS_TITLE' => 'Contacts',
);