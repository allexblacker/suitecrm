<?php
// created: 2018-03-20 17:34:00
$mod_strings = array (
  'LBL_SPA_SPA_SPADI_SPA_DISTRIBUTOR_1_FROM_SPADI_SPA_DISTRIBUTOR_TITLE' => 'SPA Distributor(s)',
  'LBL_Q4_18_TARGET_KW' => 'Q4&#039;18 Target KW',
  'LBL_SPA_REGION' => 'SPA Region',
  'LBL_SPA_STATUS' => 'SPA Status',
  'LBL_SPA_TYPE' => 'SPA Type',
  'LBL_Q2_18_MARKETING_FUND' => 'Q2&#039;18 Marketing Fund',
  'LBL_Q1_19_TARGET_KW_C' => 'Q1&#039;19 Target KW',
  'LBL_Q2_19_TARGET_KW_C' => 'Q2&#039;19 Target KW',
  'LBL_Q3_19_TARGET_KW_C' => 'Q3&#039;19 Target KW',
  'LBL_Q4_19_TARGET_KW_C' => 'Q4&#039;19 Target KW',
  'LBL_Q1_19_MARKETING_FUND_C' => 'Q1&#039;19 Marketing Fund',
  'LBL_Q2_19_MARKETING_FUND_C' => 'Q2&#039;19 Marketing Fund',
  'LBL_Q3_19_MARKETING_FUND_C' => 'Q3&#039;19 Marketing Fund',
  'LBL_Q4_19_MARKETING_FUND_C' => 'Q4&#039;19 Marketing Fund',
  'LBL_ACCOUNTS_SPA_SPA_1_FROM_ACCOUNTS_TITLE' => 'Accounts',
  'LBL_SPA_SPA_DOCUMENTS_1_FROM_DOCUMENTS_TITLE' => 'Attachments',
);