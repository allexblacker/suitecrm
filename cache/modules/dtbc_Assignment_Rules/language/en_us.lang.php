<?php
// created: 2018-03-20 17:34:00
$mod_strings = array (
  'LBL_VALIDATION_ERR_FILTER_LOGIC' => 'Filter logic must contain only the followings: curly brackets (), phrase and, phrase or, numbers and spaces. Please make sure that before and after the phrases has a space!',
  'LBL_VALIDATION_ERR_FILTER_LOGIC_BRACKETS' => 'Open and close brackets number not matched',
  'LBL_VALIDATION_ERR_USERGROUP' => 'You must set a user or a group (or check the "Do Not Reassign Owner" option)',
  'LBL_SAVEBUTTON' => 'Save',
  'LBL_ASSIGN_TO_GROUP_SECURITYGROUP_ID' => 'Group (related  ID)',
  'LBL_ASSIGN_TO_GROUP' => 'Group',
  'LBL_ASSIGN_TO_USER_USER_ID' => 'User (related User ID)',
  'LBL_ASSIGN_TO_USER' => 'User',
  'LBL_CRITERIA' => 'Criteria',
  'LBL_DO_NOT_REASSIGN_OWNER' => 'Do Not Reassign Owner',
  'LBL_FILTER_LOGIC' => 'Filter Logic',
  'LBL_MODULE' => 'Module',
  'LBL_RULE_ORDER' => 'Order',
  'LBL_EDITVIEW_PANEL1' => 'Assignment',
  'LBL_EDITVIEW_PANEL2' => 'New Panel 2',
  'LBL_EDITVIEW_PANEL3' => 'Criteria',
  'LBL_EDITVIEW_PANEL4' => 'Order',
  'LBL_MODULE_CODE' => 'Module Code',
);