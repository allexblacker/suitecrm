<?php
// created: 2018-03-20 17:34:00
$mod_strings = array (
  'LBL_EMAIL_ADDRESS' => 'Email Address',
  'LBL_CASE_PRIORITY' => 'Case Priority',
  'LBL_CASE_ORIGIN' => 'Case Origin',
  'LBL_NAME' => 'Routing Name',
  'LBL_EDITVIEW_PANEL1' => 'Routing Information',
  'LBL_EDITVIEW_PANEL2' => 'Case Settings',
);