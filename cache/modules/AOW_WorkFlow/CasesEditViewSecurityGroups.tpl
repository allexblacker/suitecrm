
<input type="text" name="{$fields.SecurityGroups.name}" class="sqsEnabled" tabindex="1" id="{$fields.SecurityGroups.name}" size="" value="{$fields.SecurityGroups.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.SecurityGroups.id_name}" 
	id="{$fields.SecurityGroups.id_name}" 
	value="{$fields.SecurityGroups_id.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.SecurityGroups.name}" id="btn_{$fields.SecurityGroups.name}" tabindex="1" title="{sugar_translate label="LBL_SELECT_BUTTON_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_SELECT_BUTTON_LABEL"}"
onclick='open_popup(
    "{$fields.SecurityGroups.module}", 
	600, 
	400, 
	"", 
	true, 
	false, 
	{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":{/literal}"{$fields.SecurityGroups.id_name}"{literal},"name":{/literal}"{$fields.SecurityGroups.name}"{literal}}}{/literal}, 
	"single", 
	true
);' ><img src="{sugar_getimagepath file="id-ff-select.png"}"></button><button type="button" name="btn_clr_{$fields.SecurityGroups.name}" id="btn_clr_{$fields.SecurityGroups.name}" tabindex="1" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.SecurityGroups.name}', '{$fields.SecurityGroups.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_LABEL"}" ><img src="{sugar_getimagepath file="id-ff-clear.png"}"></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.SecurityGroups.name}']) != 'undefined'",
		enableQS
);
</script>